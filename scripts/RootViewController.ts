/// <reference path="./UICore/UIViewController.ts" />
/// <reference path="./UICore/UIDialogView.ts" />
/// <reference path="./UICore/UILinkButton.ts" />
///<reference path="ArticleViewController.ts"/>
///<reference path="TableView.ts"/>





class RootViewController extends UIViewController {
    
    private articleViewController: ArticleViewController
    private mainViewController: any
    private _contentViewController: UIViewController
    private topBarView: CellView
    private _tableView: TableView
    private backToMainButton: CBFlatButton
    private _retrieveData = GraphQL(
        `query ($skip: Int!, $limit: Int!) {
            newsList(skip: $skip, limit: $limit) {
                rows {
                  id
                  title
                  img
                }
              }
            }`
    )
    private _data: any
    
    constructor(view) {
        
        // Calling super
        super(view)
        
        // Here are some suggested conventions that are used in UICore
        
        // Instance variables, it is good to initialize to nil or empty function, not leave as undefined to avoid
        // if blocks
        // this._firstView = nil;
        // this._secondView = nil;
        // this._testView = nil;
        // this._button = nil;
        
        // The nil object avoids unneccessary crashes by allowing you to call any function or access any variable on it, returning nil
        
        // Define properties with get and set functions so they can be accessed and set like variables
        
        // Name variables that should be private, like property variables, with a _ sign, this also holds for private functions
        // Avoid accessing variables and functions named with _ from outside as this creates strong coupling and hinders stability
        
        // Code for further setup if necessary
        
    }
    
    
    loadIntrospectionVariables() {
        
        super.loadIntrospectionVariables()
        this.superclass = UIViewController
        
    }
    
    
    loadSubviews() {
        
        this.topBarView = new CellView("TopBarView")
        this.topBarView.titleLabel.text = "NEWS READER"
        this.topBarView.colors.background.normal = new UIColor("#282c34")
        this.topBarView.colors.titleLabel.normal = UIColor.whiteColor
        this.topBarView.titleLabel.textAlignment = UITextView.textAlignment.center
        this.view.addSubview(this.topBarView)
        
        this.backToMainButton = new CBFlatButton()
        this.backToMainButton.titleLabel.text = "&#8592;"
        this.backToMainButton.colors = {
            titleLabel: {
                normal: UIColor.whiteColor,
                highlighted: UIColor.whiteColor.colorWithAlpha(0.75),
                selected: UIColor.whiteColor.colorWithAlpha(0.5)
            },
            background: {
                normal: UIColor.transparentColor,
                highlighted: UIColor.transparentColor,
                selected: UIColor.transparentColor
            }
        }
        
        this.backToMainButton.calculateAndSetViewFrame = function (this: CBFlatButton) {
            this.setPosition(0, nil, 0, 0, nil, 50)
        }
        
        this.topBarView.addSubview(this.backToMainButton)
        
        this.backToMainButton.addControlEventTarget.EnterDown.PointerUpInside = (
            sender,
            event
        ) => UIRoute.currentRoute.routeByRemovingComponentNamed(ArticleViewController.routeComponentName).apply()
        
    }
    
    
    async handleRoute(route: UIRoute) {
        
        super.handleRoute(route)
        
        this.backToMainButton.hidden = NO
        
        if (IS(route.componentWithName(ArticleViewController.routeComponentName))) {
            
            // Show article view
            if (!IS(this.articleViewController)) {
                this.articleViewController = new ArticleViewController(new UIView("ArticleView"))
            }
            
            this.contentViewController = this.articleViewController
            
        }
        else {
            
            // Main view controller
            if (!IS(this.mainViewController)) {
                this._tableView = new TableView("mainView")
                this.mainViewController = new UIViewController(this._tableView)
            }
            
            this.contentViewController = this.mainViewController
            this.backToMainButton.hidden = YES
            
            // Update _tableView with data
            this._data = this._data || await FIRST_OR_NIL(this._retrieveData)({ skip: 0, limit: 200 })
            this._tableView.items = this._data.newsList.rows
            
        }
        
    }
    
    
    set contentViewController(controller: UIViewController) {
        
        if (this.contentViewController == controller) {
            return
        }
        
        if (this.contentViewController) {
            this.removeChildViewController(this.contentViewController)
        }
        
        this._contentViewController = controller
        this.addChildViewControllerInContainer(controller, this.view)
        this._layoutViewSubviews()
        
        this.view.setNeedsLayout()
        
    }
    
    get contentViewController(): UIViewController {
        return this._contentViewController || nil
    }
    
    
    static get paddingLength() {
        return 20
    }
    
    get paddingLength() {
        return this.class.paddingLength
    }
    
    
    layoutViewsManually() {
        
        super.layoutViewsManually()
        
        // View bounds
        var bounds = this.view.bounds
        
        this.topBarView.frame = bounds.rectangleWithHeight(50)
        
        this.contentViewController.view.frame = this.topBarView.frame.rectangleForNextRow(
            0,
            this.contentViewController.view.intrinsicContentHeight(bounds.width)
        )
        
    }
    
    
}







































