///<reference path="UICore/UIButton.ts"/>


class ItemView extends UIButton {
    
    
    constructor(elementID: string) {
        
        super(elementID, nil, UITextView.type.header3)
        
        this._class = ItemView
        this.superclass = UIButton
        
    }
    
    
    updateContentForNormalState() {
        
        this.backgroundColor = UIColor.transparentColor
        this.titleLabel.textColor = UIColor.blackColor
        
        this.titleLabel.alpha = 1
        
        
    }
    
    updateContentForHoveredState() {
        
        this.titleLabel.alpha = 0.75
        
    }
    
    updateContentForHighlightedState() {
        
        this.titleLabel.alpha = 0.5
        
    }
    
    
    layoutSubviews() {
        
        // Not calling super on purpose
        //super.layoutSubviews();
        
        const padding = RootViewController.paddingLength
        const labelHeight = padding * 1.5
        
        const bounds = this.bounds
        
        this.titleLabel.isSingleLine = NO
        
        this.imageView.fillMode = UIImageView.fillMode.aspectFill
        this.titleLabel.textAlignment = UITextView.textAlignment.left
        
        this.imageView.frame = bounds.rectangleWithHeightRelativeToWidth(9 / 16)
        this.titleLabel.frame = this.imageView.frame.rectangleForNextRow(
            padding,
            this.titleLabel.intrinsicContentHeight(bounds.width)
        )
        
    }
    
    
    intrinsicContentHeight(constrainingWidth: number = 0): number {
        
        const padding = RootViewController.paddingLength
        const labelHeight = padding * 1.5
        
        const result = constrainingWidth * (9 / 16) + padding * 2 +
            this.titleLabel.intrinsicContentHeight(constrainingWidth)
        
        return result
        
        
    }
    
    
}





























