/// <reference path="./UICore/UIViewController.ts" />
///<reference path="UICore/UITextView.ts"/>





class ArticleViewController extends UIViewController {
    
    contentTextView: UITextView
    private imageView: UIImageView
    private titleLabel: UITextView
    private linkButton: CBLinkButton
    
    private _retrieveData = GraphQL(
        `query($id: ID!) {
          newsItem(id: $id) {
            id
            title
            content
            url
            img
          }
        }`
    )
    
    
    constructor(view) {
        
        // Calling super
        super(view)
        
        // Code for further setup if necessary
        
    }
    
    
    loadIntrospectionVariables() {
        
        super.loadIntrospectionVariables()
        this.superclass = UIViewController
        
    }
    
    
    static readonly routeComponentName = "article"
    
    static readonly ParameterIdentifierName = { "ID": "ID" }
    
    
    loadSubviews() {
        
        this.view.backgroundColor = UIColor.whiteColor
        
        this.imageView = new UIImageView(this.view.elementID + "ImageView")
        this.imageView.hidden = YES
        this.view.addSubview(this.imageView)
        
        this.imageView.fillMode = UIImageView.fillMode.aspectFill
        
        
        this.titleLabel = new UITextView(this.view.elementID + "TitleLabel", UITextView.type.header3)
        this.titleLabel.textAlignment = UITextView.textAlignment.left
        this.titleLabel.nativeSelectionEnabled = NO
        this.titleLabel.isSingleLine = NO
        this.view.addSubview(this.titleLabel)
        
        
        this.contentTextView = new UITextView(this.view.elementID + "TitleLabel")
        this.contentTextView.text = "Some content"
        this.view.addSubview(this.contentTextView)
        
        
        this.linkButton = new CBLinkButton()
        this.linkButton.text = "Read more"
        this.view.addSubview(this.linkButton)
        
    }
    
    
    async handleRoute(route: UIRoute) {
        
        super.handleRoute(route)
        const inquiryComponent = route.componentWithViewController(ArticleViewController)
        
        this.view.subviews.everyElement.hidden = YES
        
        this.titleLabel.hidden = NO
        
        this.titleLabel.text = "Loading"
        
        try {
            
            let data: any = (await this._retrieveData({ id: inquiryComponent.parameters.ID })).newsItem
            
            this.imageView.imageSource = FIRST(data.img, "images/logo192.png")
            this.titleLabel.text = data.title
            this.contentTextView.text = data.content
            this.linkButton.target = data.url
            
            this.view.subviews.everyElement.hidden = NO
            
        } catch (exception) {
            
            this.titleLabel.text = "Retrieving article data failed."
            
        }
        
        route.didcompleteComponent(inquiryComponent)
        
    }
    
    
    layoutViewsManually() {
        
        super.layoutViewsManually()
        
        const padding = RootViewController.paddingLength
        const labelHeight = padding
        
        // View bounds
        var bounds = this.view.bounds
        
        this.imageView.frame = bounds.rectangleWithHeightRelativeToWidth(9 / 16)
        
        this.titleLabel.frame = this.imageView.frame.rectangleWithInsets(padding * 0.5, padding * 0.5, 0, 0)
        .rectangleForNextRow(
            padding,
            this.titleLabel.intrinsicContentHeight(this.imageView.frame.width)
        )
        
        this.contentTextView.frame = this.titleLabel.frame.rectangleForNextRow(
            padding,
            this.contentTextView.intrinsicContentHeight(this.titleLabel.frame.width)
        )
        
        this.linkButton.frame = this.contentTextView.frame.rectangleForNextRow(
            padding,
            this.linkButton.intrinsicContentHeight(this.contentTextView.frame.width)
        )
        
    }
    
    
    intrinsicContentHeight(constrainingWidth: number = 0): number {
        
        const padding = RootViewController.paddingLength
        const labelHeight = padding * 1.5
        
        const result = constrainingWidth * (9 / 16) + padding * 4 +
            this.titleLabel.intrinsicContentHeight(constrainingWidth) +
            this.contentTextView.intrinsicContentHeight(constrainingWidth) +
            this.linkButton.intrinsicContentHeight(constrainingWidth)
        
        return result
        
        
    }
    
    
}








































































