///<reference path="UICore/UIView.ts"/>
///<reference path="RootViewController.ts"/>
///<reference path="ItemView.ts"/>
///<reference path="UICore/UICoreExtensions.ts"/>


class TableView extends UIView {
    
    private itemViews: ItemView[] = []
    private _items: { id: string; title: string; img: string }[] = []
    
    constructor(elementID: string) {
        
        super(elementID)
        
        this._class = TableView
        this.superclass = UIView
        
    }
    
    
    get items() {
        return this._items
    }
    
    set items(items: {
        id: string
        title: string, img: string
    }[]) {
        
        this._items = items
        
        this.itemViews.everyElement.removeFromSuperview()
        
        this.itemViews = []
        
        items.forEach((value, index, array) => {
            
            const itemView = new ItemView("ItemView" + index)
            
            itemView.titleLabel.text = value.title
            itemView.imageView.imageSource = FIRST(value.img, "images/logo192.png")
            itemView.addControlEventTarget.EnterDown.PointerUpInside = (
                sender,
                event
            ) => UIRoute.currentRoute.routeWithViewControllerComponent(ArticleViewController, { ID: value.id }).apply()
            
            this.itemViews.push(itemView)
            this.addSubview(itemView)
            
        })
        
        
    }
    
    
    layoutSubviews() {
        
        super.layoutSubviews()
        
        const padding = RootViewController.paddingLength
        
        const bounds = this.bounds
        
        // This solution is obviously not very dynamic, but I decided to not make the page have more features than the
        // React version
        
        var frame = bounds.rectangleWithHeight(0)
        
        for (let i = 0; i < this.itemViews.length; i = i + 3) {
            
            const itemView = FIRST_OR_NIL(this.itemViews[i])
            
            frame = frame.rectangleForNextRow(0, itemView.intrinsicContentHeight(frame.width))
            itemView.frame = frame
            
            const secondItemView = FIRST_OR_NIL(this.itemViews[i + 1])
            const thirdItemView = FIRST_OR_NIL(this.itemViews[i + 2])
            
            
            const constrainingWidth = (frame.width - padding * 0.5) / 2;
            const height = Math.max(
                secondItemView.intrinsicContentHeight(constrainingWidth),
                thirdItemView.intrinsicContentHeight(constrainingWidth)
            )
            
            frame = frame.rectangleForNextRow(0, height)
            
            frame.distributeViewsEquallyAlongWidth([secondItemView, thirdItemView], padding * 0.5)
            
        }
        
    }
    
    
}




























