var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function NilFunction() {
    return nil;
}
var nil = new Proxy(Object.assign(NilFunction, { "class": nil, "className": "Nil" }), {
    get(target, name) {
        if (name == Symbol.toPrimitive) {
            return function (hint) {
                if (hint == "number") {
                    return 0;
                }
                if (hint == "string") {
                    return "";
                }
                return false;
            };
        }
        if (name == "toString") {
            return function toString() {
                return "";
            };
        }
        return NilFunction();
    },
    set(target, name, value) {
        return NilFunction();
    }
});
function wrapInNil(object) {
    var result = FIRST_OR_NIL(object);
    if (object instanceof Object && !(object instanceof Function)) {
        result = new Proxy(object, {
            get(target, name) {
                if (name == "wrapped_nil_target") {
                    return target;
                }
                const value = Reflect.get(target, name);
                if (typeof value === "object") {
                    return wrapInNil(value);
                }
                if (IS_NOT_LIKE_NULL(value)) {
                    return value;
                }
                return nil;
            }
        });
    }
    return result;
}
const YES = true;
const NO = false;
function IS(object) {
    if (object && object !== nil) {
        return YES;
    }
    return NO;
    //return (object != nil && object);
}
function IS_NOT(object) {
    return !IS(object);
}
function IS_DEFINED(object) {
    if (object != undefined) {
        return YES;
    }
    return NO;
}
function IS_UNDEFINED(object) {
    return !IS_DEFINED(object);
}
function IS_NIL(object) {
    if (object === nil) {
        return YES;
    }
    return NO;
}
function IS_NOT_NIL(object) {
    return !IS_NIL(object);
}
function IS_LIKE_NULL(object) {
    return (IS_UNDEFINED(object) || IS_NIL(object) || object == null);
}
function IS_NOT_LIKE_NULL(object) {
    return !IS_LIKE_NULL(object);
}
function IS_AN_EMAIL_ADDRESS(email) {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
}
function FIRST_OR_NIL(...objects) {
    const result = objects.find(function (object, index, array) {
        return IS(object);
    });
    return result || nil;
}
function FIRST(...objects) {
    const result = objects.find(function (object, index, array) {
        return IS(object);
    });
    return result || IF(IS_DEFINED(objects.lastElement))(RETURNER(objects.lastElement))();
}
function MAKE_ID(randomPartLength = 15) {
    var result = "";
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < randomPartLength; i++) {
        result = result + characters.charAt(Math.floor(Math.random() * characters.length));
    }
    result = result + Date.now();
    return result;
}
function RETURNER(value) {
    return function (...objects) {
        return value;
    };
}
function IF(value) {
    var thenFunction = nil;
    var elseFunction = nil;
    const result = function (functionToCall) {
        thenFunction = functionToCall;
        return result.evaluateConditions;
    };
    result.evaluateConditions = function () {
        if (IS(value)) {
            return thenFunction();
        }
        return elseFunction();
    };
    result.evaluateConditions.ELSE_IF = function (otherValue) {
        const functionResult = IF(otherValue);
        elseFunction = functionResult.evaluateConditions;
        const functionResultEvaluateConditionsFunction = function () {
            return result.evaluateConditions();
        };
        functionResultEvaluateConditionsFunction.ELSE_IF = functionResult.evaluateConditions.ELSE_IF;
        functionResultEvaluateConditionsFunction.ELSE = functionResult.evaluateConditions.ELSE;
        functionResult.evaluateConditions = functionResultEvaluateConditionsFunction;
        return functionResult;
    };
    result.evaluateConditions.ELSE = function (functionToCall) {
        elseFunction = functionToCall;
        return result.evaluateConditions();
    };
    return result;
}
// @ts-ignore
if (!window.AutoLayout) {
    // @ts-ignore
    window.AutoLayout = nil;
}
class UIObject {
    constructor() {
        this._class = UIObject;
        this.superclass = nil.class;
    }
    get class() {
        return this.constructor;
    }
    get superclass() {
        return this.constructor.superclass;
    }
    set superclass(superclass) {
        this.constructor.superclass = superclass;
    }
    static wrapObject(object) {
        if (IS_NOT(object)) {
            return nil;
        }
        if (object instanceof UIObject) {
            return object;
        }
        const result = Object.assign(new UIObject(), object);
        return result;
    }
    isKindOfClass(classObject) {
        if (this.isMemberOfClass(classObject)) {
            return YES;
        }
        for (var superclassObject = this.superclass; IS(superclassObject); superclassObject = superclassObject.superclass) {
            if (superclassObject == classObject) {
                return YES;
            }
        }
        return NO;
    }
    isMemberOfClass(classObject) {
        return (this.class == classObject);
    }
    valueForKey(key) {
        return this[key];
    }
    valueForKeyPath(keyPath) {
        return UIObject.valueForKeyPath(keyPath, this);
    }
    static valueForKeyPath(keyPath, object) {
        if (IS_NOT(keyPath)) {
            return object;
        }
        const keys = keyPath.split(".");
        var currentObject = object;
        for (var i = 0; i < keys.length; i++) {
            const key = keys[i];
            if (key.substring(0, 2) == "[]") {
                // This next object will be an array and the rest of the keys need to be run for each of the elements
                currentObject = currentObject[key.substring(2)];
                // CurrentObject is now an array
                const remainingKeyPath = keys.slice(i + 1).join(".");
                const currentArray = currentObject;
                currentObject = currentArray.map(function (subObject, index, array) {
                    const result = UIObject.valueForKeyPath(remainingKeyPath, subObject);
                    return result;
                });
                break;
            }
            currentObject = currentObject[key];
            if (IS_NOT(currentObject)) {
                currentObject = nil;
            }
        }
        return currentObject;
    }
    setValueForKeyPath(keyPath, value, createPath = YES) {
        return UIObject.setValueForKeyPath(keyPath, value, this, createPath);
    }
    static setValueForKeyPath(keyPath, value, currentObject, createPath) {
        const keys = keyPath.split(".");
        var didSetValue = NO;
        keys.forEach(function (key, index, array) {
            if (index == array.length - 1 && IS_NOT_LIKE_NULL(currentObject)) {
                currentObject[key] = value;
                didSetValue = YES;
                return;
            }
            else if (IS_NOT(currentObject)) {
                return;
            }
            const currentObjectValue = currentObject[key];
            if (IS_LIKE_NULL(currentObjectValue) && createPath) {
                currentObject[key] = {};
            }
            currentObject = currentObject[key];
        });
        return didSetValue;
    }
    performFunctionWithSelf(functionToPerform) {
        return functionToPerform(this);
    }
    performFunctionWithDelay(delay, functionToCall) {
        new UITimer(delay, NO, functionToCall);
    }
}
class UIColor extends UIObject {
    constructor(stringValue) {
        super();
        this.stringValue = stringValue;
        this._class = UIColor;
        this.superclass = UIObject;
    }
    toString() {
        return this.stringValue;
    }
    static get redColor() {
        return new UIColor("red");
    }
    static get blueColor() {
        return new UIColor("blue");
    }
    static get greenColor() {
        return new UIColor("green");
    }
    static get yellowColor() {
        return new UIColor("yellow");
    }
    static get blackColor() {
        return new UIColor("black");
    }
    static get brownColor() {
        return new UIColor("brown");
    }
    static get whiteColor() {
        return new UIColor("white");
    }
    static get greyColor() {
        return new UIColor("grey");
    }
    static get lightGreyColor() {
        return new UIColor("lightgrey");
    }
    static get transparentColor() {
        return new UIColor("transparent");
    }
    static get undefinedColor() {
        return new UIColor("");
    }
    static get nilColor() {
        return new UIColor("");
    }
    static nameToHex(name) {
        return {
            "aliceblue": "#f0f8ff",
            "antiquewhite": "#faebd7",
            "aqua": "#00ffff",
            "aquamarine": "#7fffd4",
            "azure": "#f0ffff",
            "beige": "#f5f5dc",
            "bisque": "#ffe4c4",
            "black": "#000000",
            "blanchedalmond": "#ffebcd",
            "blue": "#0000ff",
            "blueviolet": "#8a2be2",
            "brown": "#a52a2a",
            "burlywood": "#deb887",
            "cadetblue": "#5f9ea0",
            "chartreuse": "#7fff00",
            "chocolate": "#d2691e",
            "coral": "#ff7f50",
            "cornflowerblue": "#6495ed",
            "cornsilk": "#fff8dc",
            "crimson": "#dc143c",
            "cyan": "#00ffff",
            "darkblue": "#00008b",
            "darkcyan": "#008b8b",
            "darkgoldenrod": "#b8860b",
            "darkgray": "#a9a9a9",
            "darkgreen": "#006400",
            "darkkhaki": "#bdb76b",
            "darkmagenta": "#8b008b",
            "darkolivegreen": "#556b2f",
            "darkorange": "#ff8c00",
            "darkorchid": "#9932cc",
            "darkred": "#8b0000",
            "darksalmon": "#e9967a",
            "darkseagreen": "#8fbc8f",
            "darkslateblue": "#483d8b",
            "darkslategray": "#2f4f4f",
            "darkturquoise": "#00ced1",
            "darkviolet": "#9400d3",
            "deeppink": "#ff1493",
            "deepskyblue": "#00bfff",
            "dimgray": "#696969",
            "dodgerblue": "#1e90ff",
            "firebrick": "#b22222",
            "floralwhite": "#fffaf0",
            "forestgreen": "#228b22",
            "fuchsia": "#ff00ff",
            "gainsboro": "#dcdcdc",
            "ghostwhite": "#f8f8ff",
            "gold": "#ffd700",
            "goldenrod": "#daa520",
            "gray": "#808080",
            "green": "#008000",
            "greenyellow": "#adff2f",
            "honeydew": "#f0fff0",
            "hotpink": "#ff69b4",
            "indianred ": "#cd5c5c",
            "indigo": "#4b0082",
            "ivory": "#fffff0",
            "khaki": "#f0e68c",
            "lavender": "#e6e6fa",
            "lavenderblush": "#fff0f5",
            "lawngreen": "#7cfc00",
            "lemonchiffon": "#fffacd",
            "lightblue": "#add8e6",
            "lightcoral": "#f08080",
            "lightcyan": "#e0ffff",
            "lightgoldenrodyellow": "#fafad2",
            "lightgrey": "#d3d3d3",
            "lightgreen": "#90ee90",
            "lightpink": "#ffb6c1",
            "lightsalmon": "#ffa07a",
            "lightseagreen": "#20b2aa",
            "lightskyblue": "#87cefa",
            "lightslategray": "#778899",
            "lightsteelblue": "#b0c4de",
            "lightyellow": "#ffffe0",
            "lime": "#00ff00",
            "limegreen": "#32cd32",
            "linen": "#faf0e6",
            "magenta": "#ff00ff",
            "maroon": "#800000",
            "mediumaquamarine": "#66cdaa",
            "mediumblue": "#0000cd",
            "mediumorchid": "#ba55d3",
            "mediumpurple": "#9370d8",
            "mediumseagreen": "#3cb371",
            "mediumslateblue": "#7b68ee",
            "mediumspringgreen": "#00fa9a",
            "mediumturquoise": "#48d1cc",
            "mediumvioletred": "#c71585",
            "midnightblue": "#191970",
            "mintcream": "#f5fffa",
            "mistyrose": "#ffe4e1",
            "moccasin": "#ffe4b5",
            "navajowhite": "#ffdead",
            "navy": "#000080",
            "oldlace": "#fdf5e6",
            "olive": "#808000",
            "olivedrab": "#6b8e23",
            "orange": "#ffa500",
            "orangered": "#ff4500",
            "orchid": "#da70d6",
            "palegoldenrod": "#eee8aa",
            "palegreen": "#98fb98",
            "paleturquoise": "#afeeee",
            "palevioletred": "#d87093",
            "papayawhip": "#ffefd5",
            "peachpuff": "#ffdab9",
            "peru": "#cd853f",
            "pink": "#ffc0cb",
            "plum": "#dda0dd",
            "powderblue": "#b0e0e6",
            "purple": "#800080",
            "red": "#ff0000",
            "rosybrown": "#bc8f8f",
            "royalblue": "#4169e1",
            "saddlebrown": "#8b4513",
            "salmon": "#fa8072",
            "sandybrown": "#f4a460",
            "seagreen": "#2e8b57",
            "seashell": "#fff5ee",
            "sienna": "#a0522d",
            "silver": "#c0c0c0",
            "skyblue": "#87ceeb",
            "slateblue": "#6a5acd",
            "slategray": "#708090",
            "snow": "#fffafa",
            "springgreen": "#00ff7f",
            "steelblue": "#4682b4",
            "tan": "#d2b48c",
            "teal": "#008080",
            "thistle": "#d8bfd8",
            "tomato": "#ff6347",
            "turquoise": "#40e0d0",
            "violet": "#ee82ee",
            "wheat": "#f5deb3",
            "white": "#ffffff",
            "whitesmoke": "#f5f5f5",
            "yellow": "#ffff00",
            "yellowgreen": "#9acd32"
        }[name.toLowerCase()];
    }
    static hexToDescriptor(c) {
        if (c[0] === "#") {
            c = c.substr(1);
        }
        const r = parseInt(c.slice(0, 2), 16);
        const g = parseInt(c.slice(2, 4), 16);
        const b = parseInt(c.slice(4, 6), 16);
        const a = parseInt(c.slice(6, 8), 16);
        const result = { "red": r, "green": g, "blue": b, "alpha": a };
        return result;
        //return 'rgb(' + r + ',' + g + ',' + b + ')';
    }
    static rgbToDescriptor(colorString) {
        if (colorString.startsWith("rgba(")) {
            colorString = colorString.slice(5, colorString.length - 1);
        }
        if (colorString.startsWith("rgb(")) {
            colorString = colorString.slice(4, colorString.length - 1) + ", 0";
        }
        const components = colorString.split(",");
        const result = {
            "red": Number(components[0]),
            "green": Number(components[1]),
            "blue": Number(components[2]),
            "alpha": Number(components[3])
        };
        return result;
    }
    get colorDescriptor() {
        var descriptor;
        const colorHEXFromName = UIColor.nameToHex(this.stringValue);
        if (this.stringValue.startsWith("rgb")) {
            descriptor = UIColor.rgbToDescriptor(this.stringValue);
        }
        else if (colorHEXFromName) {
            descriptor = UIColor.hexToDescriptor(colorHEXFromName);
        }
        else {
            descriptor = UIColor.hexToDescriptor(this.stringValue);
        }
        return descriptor;
    }
    colorWithRed(red) {
        const descriptor = this.colorDescriptor;
        const result = new UIColor("rgba(" + red + "," + descriptor.green + "," + descriptor.blue + "," +
            descriptor.alpha + ")");
        return result;
    }
    colorWithGreen(green) {
        const descriptor = this.colorDescriptor;
        const result = new UIColor("rgba(" + descriptor.red + "," + green + "," + descriptor.blue + "," +
            descriptor.alpha + ")");
        return result;
    }
    colorWithBlue(blue) {
        const descriptor = this.colorDescriptor;
        const result = new UIColor("rgba(" + descriptor.red + "," + descriptor.green + "," + blue + "," +
            descriptor.alpha + ")");
        return result;
    }
    colorWithAlpha(alpha) {
        const descriptor = this.colorDescriptor;
        const result = new UIColor("rgba(" + descriptor.red + "," + descriptor.green + "," + descriptor.blue + "," +
            alpha + ")");
        return result;
    }
    static colorWithRGBA(red, green, blue, alpha = 1) {
        const result = new UIColor("rgba(" + red + "," + green + "," + blue + "," + alpha + ")");
        return result;
    }
    static colorWithDescriptor(descriptor) {
        const result = new UIColor("rgba(" + descriptor.red.toFixed(0) + "," + descriptor.green.toFixed(0) + "," +
            descriptor.blue.toFixed(0) + "," + this.defaultAlphaToOne(descriptor.alpha) + ")");
        return result;
    }
    static defaultAlphaToOne(value = 1) {
        if (value != value) {
            value = 1;
        }
        return value;
    }
    colorByMultiplyingRGB(multiplier) {
        const descriptor = this.colorDescriptor;
        descriptor.red = descriptor.red * multiplier;
        descriptor.green = descriptor.green * multiplier;
        descriptor.blue = descriptor.blue * multiplier;
        const result = UIColor.colorWithDescriptor(descriptor);
        return result;
    }
}
/// <reference path="./UIObject.ts" />
class UIPoint extends UIObject {
    constructor(x, y) {
        super();
        this.x = x;
        this.y = y;
        this._class = UIPoint;
        this.superclass = UIObject;
    }
    copy() {
        return new UIPoint(this.x, this.y);
    }
    isEqualTo(point) {
        const result = (this.x == point.x && this.y == point.y);
        return result;
    }
    scale(zoom) {
        const x = this.x;
        const y = this.y;
        this.x = x * zoom;
        this.y = y * zoom;
        return this;
    }
    add(v) {
        this.x = this.x + v.x;
        this.y = this.y + v.y;
        return this;
    }
    subtract(v) {
        this.x = this.x - v.x;
        this.y = this.y - v.y;
        return this;
    }
    to(b) {
        const a = this;
        const ab = b.copy().add(a.copy().scale(-1));
        return ab;
    }
    pointWithX(x) {
        const result = this.copy();
        result.x = x;
        return result;
    }
    pointWithY(y) {
        const result = this.copy();
        result.y = y;
        return result;
    }
    pointByAddingX(x) {
        return this.pointWithX(this.x + x);
    }
    pointByAddingY(y) {
        return this.pointWithY(this.y + y);
    }
    get length() {
        var result = this.x * this.x + this.y * this.y;
        result = Math.sqrt(result);
        return result;
    }
    didChange(b) {
        // Callback to be set by delegate
    }
}
/// <reference path="./UIPoint.ts" />
class UIRectangle extends UIObject {
    constructor(x = 0, y = 0, height = 0, width = 0) {
        super();
        this._class = UIRectangle;
        this.superclass = UIObject;
        this.min = new UIPoint(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY);
        this.max = new UIPoint(Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY);
        this.min.didChange = this.rectanglePointDidChange;
        this.max.didChange = this.rectanglePointDidChange;
        this._isBeingUpdated = NO;
        this.min = new UIPoint(x, y);
        this.max = new UIPoint(x + width, y + height);
        if (IS_NIL(height)) {
            this.max.y = height;
        }
        if (IS_NIL(width)) {
            this.max.x = width;
        }
    }
    copy() {
        const result = new UIRectangle(this.x, this.y, this.height, this.width);
        return result;
    }
    isEqualTo(rectangle) {
        const result = (IS(rectangle) && this.min.isEqualTo(rectangle.min) && this.max.isEqualTo(rectangle.max));
        return result;
    }
    static zero() {
        const result = new UIRectangle(0, 0, 0, 0);
        return result;
    }
    containsPoint(point) {
        return this.min.x <= point.x && this.min.y <= point.y &&
            point.x <= this.max.x && point.y <= this.max.y;
    }
    updateByAddingPoint(point) {
        if (!point) {
            point = new UIPoint(0, 0);
        }
        this.beginUpdates();
        const min = this.min.copy();
        if (min.x === nil) {
            min.x = this.max.x;
        }
        if (min.y === nil) {
            min.y = this.max.y;
        }
        const max = this.max.copy();
        if (max.x === nil) {
            max.x = this.min.x;
        }
        if (max.y === nil) {
            max.y = this.min.y;
        }
        this.min.x = Math.min(min.x, point.x);
        this.min.y = Math.min(min.y, point.y);
        this.max.x = Math.max(max.x, point.x);
        this.max.y = Math.max(max.y, point.y);
        this.finishUpdates();
    }
    get height() {
        if (this.max.y === nil) {
            return nil;
        }
        return this.max.y - this.min.y;
    }
    set height(height) {
        this.max.y = this.min.y + height;
    }
    get width() {
        if (this.max.x === nil) {
            return nil;
        }
        return this.max.x - this.min.x;
    }
    set width(width) {
        this.max.x = this.min.x + width;
    }
    get x() {
        return this.min.x;
    }
    set x(x) {
        this.beginUpdates();
        const width = this.width;
        this.min.x = x;
        this.max.x = this.min.x + width;
        this.finishUpdates();
    }
    get y() {
        return this.min.y;
    }
    set y(y) {
        this.beginUpdates();
        const height = this.height;
        this.min.y = y;
        this.max.y = this.min.y + height;
        this.finishUpdates();
    }
    get topLeft() {
        return this.min.copy();
    }
    get topRight() {
        return new UIPoint(this.max.x, this.y);
    }
    get bottomLeft() {
        return new UIPoint(this.x, this.max.y);
    }
    get bottomRight() {
        return this.max.copy();
    }
    get center() {
        const result = this.min.copy().add(this.min.to(this.max).scale(0.5));
        return result;
    }
    set center(center) {
        const offset = this.center.to(center);
        this.offsetByPoint(offset);
    }
    offsetByPoint(offset) {
        this.min.add(offset);
        this.max.add(offset);
        return this;
    }
    concatenateWithRectangle(rectangle) {
        this.updateByAddingPoint(rectangle.bottomRight);
        this.updateByAddingPoint(rectangle.topLeft);
        return this;
    }
    intersectionRectangleWithRectangle(rectangle) {
        const result = this.copy();
        result.beginUpdates();
        const min = result.min;
        if (min.x === nil) {
            min.x = rectangle.max.x - Math.min(result.width, rectangle.width);
        }
        if (min.y === nil) {
            min.y = rectangle.max.y - Math.min(result.height, rectangle.height);
        }
        const max = result.max;
        if (max.x === nil) {
            max.x = rectangle.min.x + Math.min(result.width, rectangle.width);
        }
        if (max.y === nil) {
            max.y = rectangle.min.y + Math.min(result.height, rectangle.height);
        }
        result.min.x = Math.max(result.min.x, rectangle.min.x);
        result.min.y = Math.max(result.min.y, rectangle.min.y);
        result.max.x = Math.min(result.max.x, rectangle.max.x);
        result.max.y = Math.min(result.max.y, rectangle.max.y);
        if (result.height < 0) {
            const averageY = (this.center.y + rectangle.center.y) * 0.5;
            result.min.y = averageY;
            result.max.y = averageY;
        }
        if (result.width < 0) {
            const averageX = (this.center.x + rectangle.center.x) * 0.5;
            result.min.x = averageX;
            result.max.x = averageX;
        }
        result.finishUpdates();
        return result;
    }
    get area() {
        const result = this.height * this.width;
        return result;
    }
    intersectsWithRectangle(rectangle) {
        return (this.intersectionRectangleWithRectangle(rectangle).area != 0);
    }
    // add some space around the rectangle
    rectangleWithInsets(left, right, bottom, top) {
        const result = this.copy();
        result.min.x = this.min.x + left;
        result.max.x = this.max.x - right;
        result.min.y = this.min.y + top;
        result.max.y = this.max.y - bottom;
        return result;
    }
    rectangleWithInset(inset) {
        const result = this.rectangleWithInsets(inset, inset, inset, inset);
        return result;
    }
    rectangleWithHeight(height, centeredOnPosition = nil) {
        if (isNaN(centeredOnPosition)) {
            centeredOnPosition = nil;
        }
        const result = this.copy();
        result.height = height;
        if (centeredOnPosition != nil) {
            const change = height - this.height;
            result.offsetByPoint(new UIPoint(0, change * centeredOnPosition).scale(-1));
        }
        return result;
    }
    rectangleWithWidth(width, centeredOnPosition = nil) {
        if (isNaN(centeredOnPosition)) {
            centeredOnPosition = nil;
        }
        const result = this.copy();
        result.width = width;
        if (centeredOnPosition != nil) {
            const change = width - this.width;
            result.offsetByPoint(new UIPoint(change * centeredOnPosition, 0).scale(-1));
        }
        return result;
    }
    rectangleWithHeightRelativeToWidth(heightRatio = 1, centeredOnPosition = nil) {
        const result = this.rectangleWithHeight(this.width * heightRatio, centeredOnPosition);
        return result;
    }
    rectangleWithWidthRelativeToHeight(widthRatio = 1, centeredOnPosition = nil) {
        const result = this.rectangleWithWidth(this.height * widthRatio, centeredOnPosition);
        return result;
    }
    rectangleWithX(x, centeredOnPosition = 0) {
        const result = this.copy();
        result.x = x - result.width * centeredOnPosition;
        return result;
    }
    rectangleWithY(y, centeredOnPosition = 0) {
        const result = this.copy();
        result.y = y - result.height * centeredOnPosition;
        return result;
    }
    rectangleByAddingX(x) {
        const result = this.copy();
        result.x = this.x + x;
        return result;
    }
    rectangleByAddingY(y) {
        const result = this.copy();
        result.y = this.y + y;
        return result;
    }
    rectanglesBySplittingWidth(weights, paddings = 0, absoluteWidths = nil) {
        if (IS_NIL(paddings)) {
            paddings = 1;
        }
        if (!(paddings instanceof Array)) {
            paddings = [paddings].arrayByRepeating(weights.length - 1);
        }
        paddings = paddings.arrayByTrimmingToLengthIfLonger(weights.length - 1);
        if (!(absoluteWidths instanceof Array) && IS_NOT_NIL(absoluteWidths)) {
            absoluteWidths = [absoluteWidths].arrayByRepeating(weights.length);
        }
        const result = [];
        const sumOfWeights = weights.reduce(function (a, b, index) {
            if (IS_NOT_NIL(absoluteWidths[index])) {
                b = 0;
            }
            return a + b;
        }, 0);
        const sumOfPaddings = paddings.summedValue;
        const sumOfAbsoluteWidths = absoluteWidths.summedValue;
        const totalRelativeWidth = this.width - sumOfPaddings - sumOfAbsoluteWidths;
        var previousCellMaxX = this.x;
        for (var i = 0; i < weights.length; i++) {
            var resultWidth;
            if (IS_NOT_NIL(absoluteWidths[i])) {
                resultWidth = absoluteWidths[i] || 0;
            }
            else {
                resultWidth = totalRelativeWidth * (weights[i] / sumOfWeights);
            }
            const rectangle = this.rectangleWithWidth(resultWidth);
            var padding = 0;
            if (paddings.length > i && paddings[i]) {
                padding = paddings[i];
            }
            rectangle.x = previousCellMaxX;
            previousCellMaxX = rectangle.max.x + padding;
            //rectangle = rectangle.rectangleWithInsets(0, padding, 0, 0);
            result.push(rectangle);
        }
        return result;
    }
    rectanglesBySplittingHeight(weights, paddings = 0, absoluteHeights = nil) {
        if (IS_NIL(paddings)) {
            paddings = 1;
        }
        if (!(paddings instanceof Array)) {
            paddings = [paddings].arrayByRepeating(weights.length - 1);
        }
        paddings = paddings.arrayByTrimmingToLengthIfLonger(weights.length - 1);
        if (!(absoluteHeights instanceof Array) && IS_NOT_NIL(absoluteHeights)) {
            absoluteHeights = [absoluteHeights].arrayByRepeating(weights.length);
        }
        const result = [];
        const sumOfWeights = weights.reduce(function (a, b, index) {
            if (IS_NOT_NIL(absoluteHeights[index])) {
                b = 0;
            }
            return a + b;
        }, 0);
        const sumOfPaddings = paddings.summedValue;
        const sumOfAbsoluteHeights = absoluteHeights.summedValue;
        const totalRelativeHeight = this.height - sumOfPaddings - sumOfAbsoluteHeights;
        var previousCellMaxY = this.y;
        for (var i = 0; i < weights.length; i++) {
            var resultHeight;
            if (IS_NOT_NIL(absoluteHeights[i])) {
                resultHeight = absoluteHeights[i] || 0;
            }
            else {
                resultHeight = totalRelativeHeight * (weights[i] / sumOfWeights);
            }
            const rectangle = this.rectangleWithHeight(resultHeight);
            var padding = 0;
            if (paddings.length > i && paddings[i]) {
                padding = paddings[i];
            }
            rectangle.y = previousCellMaxY;
            previousCellMaxY = rectangle.max.y + padding;
            //rectangle = rectangle.rectangleWithInsets(0, 0, padding, 0);
            result.push(rectangle);
        }
        return result;
    }
    rectanglesByEquallySplittingWidth(numberOfFrames, padding = 0) {
        const result = [];
        const totalPadding = padding * (numberOfFrames - 1);
        const resultWidth = (this.width - totalPadding) / numberOfFrames;
        for (var i = 0; i < numberOfFrames; i++) {
            const rectangle = this.rectangleWithWidth(resultWidth, i / (numberOfFrames - 1));
            result.push(rectangle);
        }
        return result;
    }
    rectanglesByEquallySplittingHeight(numberOfFrames, padding = 0) {
        const result = [];
        const totalPadding = padding * (numberOfFrames - 1);
        const resultHeight = (this.height - totalPadding) / numberOfFrames;
        for (var i = 0; i < numberOfFrames; i++) {
            const rectangle = this.rectangleWithHeight(resultHeight, i / (numberOfFrames - 1));
            result.push(rectangle);
        }
        return result;
    }
    distributeViewsAlongWidth(views, weights = 1, paddings, absoluteWidths) {
        if (!(weights instanceof Array)) {
            weights = [weights].arrayByRepeating(views.length);
        }
        const frames = this.rectanglesBySplittingWidth(weights, paddings, absoluteWidths);
        frames.forEach(function (frame, index, array) {
            views[index].frame = frame;
        });
        return this;
    }
    distributeViewsAlongHeight(views, weights = 1, paddings, absoluteHeights) {
        if (!(weights instanceof Array)) {
            weights = [weights].arrayByRepeating(views.length);
        }
        const frames = this.rectanglesBySplittingHeight(weights, paddings, absoluteHeights);
        frames.forEach(function (frame, index, array) {
            views[index].frame = frame;
        });
        return this;
    }
    distributeViewsEquallyAlongWidth(views, padding) {
        const frames = this.rectanglesByEquallySplittingWidth(views.length, padding);
        frames.forEach(function (frame, index, array) {
            views[index].frame = frame;
        });
        return this;
    }
    distributeViewsEquallyAlongHeight(views, padding) {
        const frames = this.rectanglesByEquallySplittingHeight(views.length, padding);
        frames.forEach(function (frame, index, array) {
            views[index].frame = frame;
        });
        return this;
    }
    rectangleForNextRow(padding = 0, height = this.height) {
        const result = this.rectangleWithY(this.max.y + padding);
        if (height != this.height) {
            result.height = height;
        }
        return result;
    }
    rectangleForNextColumn(padding = 0, width = this.width) {
        const result = this.rectangleWithX(this.max.x + padding);
        if (width != this.width) {
            result.width = width;
        }
        return result;
    }
    rectangleForPreviousRow(padding = 0) {
        const result = this.rectangleWithY(this.min.y - this.height - padding);
        return result;
    }
    rectangleForPreviousColumn(padding = 0) {
        const result = this.rectangleWithX(this.min.x - this.width - padding);
        return result;
    }
    // Bounding box
    static boundingBoxForPoints(points) {
        const result = new UIRectangle();
        for (var i = 0; i < points.length; i++) {
            result.updateByAddingPoint(points[i]);
        }
        return result;
    }
    beginUpdates() {
        this._isBeingUpdated = YES;
    }
    finishUpdates() {
        this._isBeingUpdated = NO;
        this.didChange();
    }
    didChange() {
        // Callback to be set by delegate
    }
    _rectanglePointDidChange() {
        if (!this._isBeingUpdated) {
            this.didChange();
        }
    }
}
///// <reference path="../autolayout/src/AutoLayout.js" />
/// <reference path="./UIObject.ts" />
/// <reference path="./UIColor.ts" />
/// <reference path="./UIRectangle.ts" />
class UIView extends UIObject {
    constructor(elementID = ("UIView" +
        UIView.nextIndex), viewHTMLElement = null, elementType = null, initViewData) {
        super();
        this._nativeSelectionEnabled = YES;
        this._enabled = YES;
        this._backgroundColor = UIColor.transparentColor;
        this._localizedTextObject = nil;
        this._controlEventTargets = {}; //{ "PointerDown": Function[]; "PointerMove": Function[]; "PointerLeave": Function[]; "PointerEnter": Function[]; "PointerUpInside": Function[]; "PointerUp": Function[]; "PointerHover": Function[]; };
        this._viewControllerLayoutFunction = nil;
        this._isHidden = NO;
        this.pausesPointerEvents = NO;
        this.stopsPointerEventPropagation = YES;
        this._pointerDragThreshold = 2;
        this.ignoresTouches = NO;
        this.ignoresMouse = NO;
        this.forceIntrinsicSizeZero = NO;
        this.controlEvent = UIView.controlEvent;
        this._class = UIView;
        this.superclass = UIObject;
        // Instance variables
        UIView._UIViewIndex = UIView.nextIndex;
        this._UIViewIndex = UIView._UIViewIndex;
        this._styleClasses = [];
        // Object.defineProperty(this, "styleClasses", { get: this.styleClasses, set: this.setStyleClasses });
        // Object.defineProperty(this, "styleClassName", { get: this.styleClassName });
        this._initViewHTMLElement(elementID, viewHTMLElement, elementType);
        this.subviews = [];
        this.superview = nil;
        // Object.defineProperty(this, "elementID", { get: this.elementID });
        // Object.defineProperty(this, "constraints", { get: this.constraints, set: this.setConstraints });
        this._constraints = [];
        this._updateLayoutFunction = nil;
        //Object.defineProperty(this, "backgroundColor", { get: this.backgroundColor, set: this.setBackgroundColor });
        //this.backgroundColor = "transparent";
        // Object.defineProperty(this, "alpha", { get: this.alpha, set: this.setAlpha });
        // Object.defineProperty(this, "frame", { get: this.frame, set: this.setFrame });
        // Object.defineProperty(this, "bounds", { get: this.bounds, set: this.setBounds });
        // Object.defineProperty(this, "userInteractionEnabled", { get: this.userInteractionEnabled, set: this.setUserInteractionEnabled });
        // this._controlEventTargets = {
        //     "PointerDown": [],
        //     "PointerMove": [],
        //     "PointerLeave": [],
        //     "PointerEnter": [],
        //     "PointerUpInside": [],
        //     "PointerUp": [],
        //     "PointerHover": []
        // }
        this._didLayoutSubviewsDelegateFunction = function () {
        };
        this._frameTransform = "";
        this.initView(this.viewHTMLElement.id, this.viewHTMLElement, initViewData);
        this._initViewCSSSelectorsIfNeeded();
        this._loadUIEvents();
        this.setNeedsLayout();
    }
    static get nextIndex() {
        return UIView._UIViewIndex + 1;
    }
    static get pageHeight() {
        const body = document.body;
        const html = document.documentElement;
        const height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        return height;
    }
    static get pageWidth() {
        const body = document.body;
        const html = document.documentElement;
        const width = Math.max(body.scrollWidth, body.offsetWidth, html.clientWidth, html.scrollWidth, html.offsetWidth);
        return width;
    }
    initView(elementID, viewHTMLElement, initViewData) {
    }
    centerInContainer() {
        this.style.left = "50%";
        this.style.top = "50%";
        this.style.transform = "translateX(-50%) translateY(-50%)";
    }
    centerXInContainer() {
        this.style.left = "50%";
        this.style.transform = "translateX(-50%)";
    }
    centerYInContainer() {
        this.style.top = "50%";
        this.style.transform = "translateY(-50%)";
    }
    _initViewHTMLElement(elementID, viewHTMLElement, elementType = "div") {
        if (!IS(elementType)) {
            elementType = "div";
        }
        if (!IS(viewHTMLElement)) {
            this._viewHTMLElement = this.createElement(elementID, elementType);
            this.style.position = "absolute";
            this.style.margin = "0";
        }
        else {
            this._viewHTMLElement = viewHTMLElement;
        }
        if (IS(elementID)) {
            this.viewHTMLElement.id = elementID;
        }
        this.viewHTMLElement.obeyAutolayout = YES;
        this.viewHTMLElement.UIView = this;
        this.addStyleClass(this.styleClassName);
    }
    set nativeSelectionEnabled(selectable) {
        this._nativeSelectionEnabled = selectable;
        if (!selectable) {
            this.style.cssText = this.style.cssText +
                " -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;";
        }
        else {
            this.style.cssText = this.style.cssText +
                " -webkit-touch-callout: text; -webkit-user-select: text; -khtml-user-select: text; -moz-user-select: text; -ms-user-select: text; user-select: text;";
        }
    }
    get nativeSelectionEnabled() {
        return this._nativeSelectionEnabled;
    }
    get styleClassName() {
        const result = "UICore_UIView_" + this.class.name;
        return result;
    }
    _initViewCSSSelectorsIfNeeded() {
        if (!this.class._areViewCSSSelectorsInitialized) {
            this.initViewStyleSelectors();
            this.class._areViewCSSSelectorsInitialized = YES;
        }
    }
    initViewStyleSelectors() {
        // Override this in a subclass
    }
    initStyleSelector(selector, style) {
        const styleRules = UIView.getStyleRules(selector);
        if (!styleRules) {
            UIView.createStyleSelector(selector, style);
        }
    }
    createElement(elementID, elementType) {
        var result = document.getElementById(elementID);
        if (!result) {
            result = document.createElement(elementType);
        }
        return result;
    }
    get viewHTMLElement() {
        return this._viewHTMLElement;
    }
    get elementID() {
        return this.viewHTMLElement.id;
    }
    setInnerHTML(key, defaultString, parameters) {
        this._innerHTMLKey = key;
        this._defaultInnerHTML = defaultString;
        this._parameters = parameters;
        const languageName = UICore.languageService.currentLanguageKey;
        const result = UICore.languageService.stringForKey(key, languageName, defaultString, parameters);
        this.innerHTML = result;
    }
    _setInnerHTMLFromKeyIfPossible() {
        if (this._innerHTMLKey && this._defaultInnerHTML) {
            this.setInnerHTML(this._innerHTMLKey, this._defaultInnerHTML, this._parameters);
        }
    }
    _setInnerHTMLFromLocalizedTextObjectIfPossible() {
        if (IS(this._localizedTextObject)) {
            this.innerHTML = UICore.languageService.stringForCurrentLanguage(this._localizedTextObject);
        }
    }
    get localizedTextObject() {
        return this._localizedTextObject;
    }
    set localizedTextObject(localizedTextObject) {
        this._localizedTextObject = localizedTextObject;
        this._setInnerHTMLFromLocalizedTextObjectIfPossible();
    }
    get innerHTML() {
        return this.viewHTMLElement.innerHTML;
    }
    set innerHTML(innerHTML) {
        if (this.innerHTML != innerHTML) {
            this.viewHTMLElement.innerHTML = FIRST(innerHTML, "");
        }
    }
    set hoverText(hoverText) {
        this.viewHTMLElement.setAttribute("title", hoverText);
    }
    get hoverText() {
        return this.viewHTMLElement.getAttribute("title");
    }
    get scrollSize() {
        const result = new UIRectangle(0, 0, this.viewHTMLElement.scrollHeight, this.viewHTMLElement.scrollWidth);
        return result;
    }
    get dialogView() {
        if (!IS(this.superview)) {
            return nil;
        }
        if (!(this instanceof UIDialogView)) {
            return this.superview.dialogView;
        }
        return this;
    }
    get rootView() {
        if (IS(this.superview)) {
            return this.superview.rootView;
        }
        return this;
    }
    set enabled(enabled) {
        this._enabled = enabled;
        this.updateContentForCurrentEnabledState();
    }
    get enabled() {
        return this._enabled;
    }
    updateContentForCurrentEnabledState() {
        this.hidden = !this.enabled;
        this.userInteractionEnabled = this.enabled;
    }
    get tabIndex() {
        return Number(this.viewHTMLElement.getAttribute("tabindex"));
    }
    set tabIndex(index) {
        this.viewHTMLElement.setAttribute("tabindex", "" + index);
    }
    get styleClasses() {
        return this._styleClasses;
    }
    set styleClasses(styleClasses) {
        this._styleClasses = styleClasses;
    }
    hasStyleClass(styleClass) {
        // This is for performance reasons
        if (!IS(styleClass)) {
            return NO;
        }
        const index = this.styleClasses.indexOf(styleClass);
        if (index > -1) {
            return YES;
        }
        return NO;
    }
    addStyleClass(styleClass) {
        if (!IS(styleClass)) {
            return;
        }
        if (!this.hasStyleClass(styleClass)) {
            this._styleClasses.push(styleClass);
        }
    }
    removeStyleClass(styleClass) {
        // This is for performance reasons
        if (!IS(styleClass)) {
            return;
        }
        const index = this.styleClasses.indexOf(styleClass);
        if (index > -1) {
            this.styleClasses.splice(index, 1);
        }
    }
    static findViewWithElementID(elementID) {
        const viewHTMLElement = document.getElementById(elementID);
        if (IS_NOT(viewHTMLElement)) {
            return nil;
        }
        // @ts-ignore
        const result = viewHTMLElement.UIView;
        return result;
    }
    static createStyleSelector(selector, style) {
        return;
        // @ts-ignore
        if (!document.styleSheets) {
            return;
        }
        if (document.getElementsByTagName("head").length == 0) {
            return;
        }
        var styleSheet;
        var mediaType;
        if (document.styleSheets.length > 0) {
            for (var i = 0, l = document.styleSheets.length; i < l; i++) {
                if (document.styleSheets[i].disabled) {
                    continue;
                }
                const media = document.styleSheets[i].media;
                mediaType = typeof media;
                if (mediaType === "string") {
                    if (media === "" || (media.indexOf("screen") !== -1)) {
                        styleSheet = document.styleSheets[i];
                    }
                }
                else if (mediaType == "object") {
                    if (media.mediaText === "" || (media.mediaText.indexOf("screen") !== -1)) {
                        styleSheet = document.styleSheets[i];
                    }
                }
                if (typeof styleSheet !== "undefined") {
                    break;
                }
            }
        }
        if (typeof styleSheet === "undefined") {
            const styleSheetElement = document.createElement("style");
            styleSheetElement.type = "text/css";
            document.getElementsByTagName("head")[0].appendChild(styleSheetElement);
            for (i = 0; i < document.styleSheets.length; i++) {
                if (document.styleSheets[i].disabled) {
                    continue;
                }
                styleSheet = document.styleSheets[i];
            }
            mediaType = typeof styleSheet.media;
        }
        if (mediaType === "string") {
            for (var i = 0, l = styleSheet.rules.length; i < l; i++) {
                if (styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase() ==
                    selector.toLowerCase()) {
                    styleSheet.rules[i].style.cssText = style;
                    return;
                }
            }
            styleSheet.addRule(selector, style);
        }
        else if (mediaType === "object") {
            var styleSheetLength = 0;
            try {
                styleSheetLength = (styleSheet.cssRules) ? styleSheet.cssRules.length : 0;
            }
            catch (error) {
            }
            for (var i = 0; i < styleSheetLength; i++) {
                if (styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() ==
                    selector.toLowerCase()) {
                    styleSheet.cssRules[i].style.cssText = style;
                    return;
                }
            }
            styleSheet.insertRule(selector + "{" + style + "}", styleSheetLength);
        }
    }
    static getStyleRules(selector) {
        var selector = selector.toLowerCase();
        for (var i = 0; i < document.styleSheets.length; i++) {
            const styleSheet = document.styleSheets[i];
            var styleRules;
            try {
                styleRules = styleSheet.cssRules ? styleSheet.cssRules : styleSheet.rules;
            }
            catch (error) {
            }
            return styleRules;
        }
    }
    get style() {
        return this.viewHTMLElement.style;
    }
    get computedStyle() {
        return getComputedStyle(this.viewHTMLElement);
    }
    get hidden() {
        return this._isHidden;
    }
    set hidden(v) {
        this._isHidden = v;
        if (this._isHidden) {
            this.style.visibility = "hidden";
        }
        else {
            this.style.visibility = "visible";
        }
    }
    static set pageScale(scale) {
        UIView._pageScale = scale;
        const zoom = scale;
        const width = 100 / zoom;
        const viewHTMLElement = UICore.main.rootViewController.view.viewHTMLElement;
        viewHTMLElement.style.transformOrigin = "left top";
        viewHTMLElement.style.transform = "scale(" + zoom + ")";
        viewHTMLElement.style.width = width + "%";
    }
    static get pageScale() {
        return UIView._pageScale;
    }
    calculateAndSetViewFrame() {
        // Use this method to calculate the frame for the view itself
        // This can be used when adding subviews to existing views like buttons
    }
    get frame() {
        // var result = new UIRectangle(1 * this.viewHTMLElement.offsetLeft, 1 * this.viewHTMLElement.offsetTop, 1 * this.viewHTMLElement.offsetHeight, 1 * this.viewHTMLElement.offsetWidth);
        // result.zIndex = 1 * this.style.zIndex;
        var result = this._frame;
        if (!result) {
            result = new UIRectangle(1 * this.viewHTMLElement.offsetLeft, 1 * this.viewHTMLElement.offsetTop, 1 *
                this.viewHTMLElement.offsetHeight, 1 * this.viewHTMLElement.offsetWidth);
            result.zIndex = 0;
        }
        return result.copy();
    }
    set frame(rectangle) {
        if (IS(rectangle)) {
            this.setFrame(rectangle);
        }
    }
    setFrame(rectangle, zIndex = 0, performUncheckedLayout = NO) {
        const frame = this._frame || new UIRectangle(nil, nil, nil, nil);
        if (zIndex != undefined) {
            rectangle.zIndex = zIndex;
        }
        this._frame = rectangle;
        // This is useless because frames are copied
        // frame.didChange = function () {
        //     // Do nothing
        // }
        // rectangle.didChange = function () {
        //     this.frame = rectangle;
        // }.bind(this);
        if (frame && frame.isEqualTo(rectangle) && !performUncheckedLayout) {
            return;
        }
        UIView._setAbsoluteSizeAndPosition(this.viewHTMLElement, rectangle.topLeft.x, rectangle.topLeft.y, rectangle.width, rectangle.height, rectangle.zIndex);
        if (frame.height != rectangle.height || frame.width != rectangle.width || performUncheckedLayout) {
            this.setNeedsLayout();
            this.boundsDidChange();
            //this.layoutSubviews();
        }
    }
    get bounds() {
        var result;
        // if (IS_NOT(this._frame) && this.style.height == "" && this.style.width  == "" && this.style.left == "" && this.style.right == "" && this.style.bottom == "" && this.style.top == "") {
        //     result = new UIRectangle(0, 0, 0, 0)
        // }
        // else
        if (IS_NOT(this._frame)) {
            result = new UIRectangle(0, 0, 1 * this.viewHTMLElement.offsetHeight, 1 * this.viewHTMLElement.offsetWidth);
        }
        else {
            result = this.frame.copy();
            result.x = 0;
            result.y = 0;
        }
        return result;
    }
    set bounds(rectangle) {
        const frame = this.frame;
        this.frame = new UIRectangle(frame.topLeft.x, frame.topLeft.y, rectangle.height, rectangle.width);
    }
    boundsDidChange() {
    }
    setPosition(left = nil, right = nil, bottom = nil, top = nil, height = nil, width = nil) {
        const previousBounds = this.bounds;
        this.setStyleProperty("left", left);
        this.setStyleProperty("right", right);
        this.setStyleProperty("bottom", bottom);
        this.setStyleProperty("top", top);
        this.setStyleProperty("height", height);
        this.setStyleProperty("width", width);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setSizes(height, width) {
        const previousBounds = this.bounds;
        this.setStyleProperty("height", height);
        this.setStyleProperty("width", width);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setMinSizes(height, width) {
        const previousBounds = this.bounds;
        this.setStyleProperty("minHeight", height);
        this.setStyleProperty("minWidth", width);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setMaxSizes(height, width) {
        const previousBounds = this.bounds;
        this.setStyleProperty("maxHeight", height);
        this.setStyleProperty("maxWidth", width);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setMargin(margin) {
        const previousBounds = this.bounds;
        this.setStyleProperty("margin", margin);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setMargins(left, right, bottom, top) {
        const previousBounds = this.bounds;
        this.setStyleProperty("marginLeft", left);
        this.setStyleProperty("marginRight", right);
        this.setStyleProperty("marginBottom", bottom);
        this.setStyleProperty("marginTop", top);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setPadding(padding) {
        const previousBounds = this.bounds;
        this.setStyleProperty("padding", padding);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setPaddings(left, right, bottom, top) {
        const previousBounds = this.bounds;
        this.setStyleProperty("paddingLeft", left);
        this.setStyleProperty("paddingRight", right);
        this.setStyleProperty("paddingBottom", bottom);
        this.setStyleProperty("paddingTop", top);
        const bounds = this.bounds;
        if (bounds.height != previousBounds.height || bounds.width != previousBounds.width) {
            this.setNeedsLayout();
            this.boundsDidChange();
        }
    }
    setBorder(radius = nil, width = 1, color = UIColor.blackColor, style = "solid") {
        this.setStyleProperty("borderStyle", style);
        this.setStyleProperty("borderRadius", radius);
        this.setStyleProperty("borderColor", color.stringValue);
        this.setStyleProperty("borderWidth", width);
    }
    setStyleProperty(propertyName, value) {
        try {
            if (IS_NIL(value)) {
                return;
            }
            if (IS_DEFINED(value) && value.isANumber) {
                value = "" + value.integerValue + "px";
            }
            this.style[propertyName] = value;
        }
        catch (exception) {
            console.log(exception);
        }
    }
    get userInteractionEnabled() {
        const result = (this.style.pointerEvents != "none");
        return result;
    }
    set userInteractionEnabled(userInteractionEnabled) {
        if (userInteractionEnabled) {
            this.style.pointerEvents = "";
        }
        else {
            this.style.pointerEvents = "none";
        }
    }
    get backgroundColor() {
        return this._backgroundColor;
    }
    set backgroundColor(backgroundColor) {
        this._backgroundColor = backgroundColor;
        this.style.backgroundColor = backgroundColor.stringValue;
    }
    get alpha() {
        return 1 * this.style.opacity;
    }
    set alpha(alpha) {
        this.style.opacity = "" + alpha;
    }
    static animateViewOrViewsWithDurationDelayAndFunction(viewOrViews, duration, delay, timingStyle = "cubic-bezier(0.25,0.1,0.25,1)", transformFunction, transitioncompletionFunction) {
        function callTransitioncompletionFunction() {
            (transitioncompletionFunction || nil)();
            viewOrViews.forEach(function (view, index, array) {
                view.animationDidFinish();
            });
        }
        if (IS_FIREFOX) {
            // Firefox does not fire the transition completion event properly
            new UIObject().performFunctionWithDelay(delay + duration, callTransitioncompletionFunction);
        }
        if (!(viewOrViews instanceof Array)) {
            viewOrViews = [viewOrViews];
        }
        const transitionStyles = [];
        const transitionDurations = [];
        const transitionDelays = [];
        const transitionTimings = [];
        for (var i = 0; i < viewOrViews.length; i++) {
            var view = viewOrViews[i];
            if (view.viewHTMLElement) {
                view = view.viewHTMLElement;
            }
            view.addEventListener("transitionend", transitionDidFinish, true);
            transitionStyles.push(view.style.transition);
            transitionDurations.push(view.style.transitionDuration);
            transitionDelays.push(view.style.transitionDelay);
            transitionTimings.push(view.style.transitionTimingFunction);
            view.style.transition = "all";
            view.style.transitionDuration = "" + duration + "s";
            view.style.transitionDelay = "" + delay + "s";
            view.style.transitionTimingFunction = timingStyle;
        }
        transformFunction();
        const transitionObject = {
            "finishImmediately": finishTransitionImmediately,
            "didFinish": transitionDidFinishManually,
            "views": viewOrViews,
            "registrationTime": Date.now()
        };
        function finishTransitionImmediately() {
            for (var i = 0; i < viewOrViews.length; i++) {
                var view = viewOrViews[i];
                if (view.viewHTMLElement) {
                    view = view.viewHTMLElement;
                }
                view.style.transition = "all";
                view.style.transitionDuration = "" + duration + "s";
                view.style.transitionDelay = "" + delay + "s";
                view.style.transition = transitionStyles[i];
                view.style.transitionDuration = transitionDurations[i];
                view.style.transitionDelay = transitionDelays[i];
                view.style.transitionTimingFunction = transitionTimings[i];
            }
        }
        function transitionDidFinish(event) {
            var view = event.srcElement;
            if (!view) {
                return;
            }
            if (view.viewHTMLElement) {
                view = view.viewHTMLElement;
            }
            view.style.transition = transitionStyles[i];
            view.style.transitionDuration = transitionDurations[i];
            view.style.transitionDelay = transitionDelays[i];
            view.style.transitionTimingFunction = transitionTimings[i];
            callTransitioncompletionFunction();
            view.removeEventListener("transitionend", transitionDidFinish, true);
        }
        function transitionDidFinishManually() {
            for (var i = 0; i < viewOrViews.length; i++) {
                var view = viewOrViews[i];
                if (view.viewHTMLElement) {
                    view = view.viewHTMLElement;
                }
                view.style.transition = transitionStyles[i];
                view.style.transitionDuration = transitionDurations[i];
                view.style.transitionDelay = transitionDelays[i];
                view.style.transitionTimingFunction = transitionTimings[i];
                view.removeEventListener("transitionend", transitionDidFinish, true);
            }
        }
        return transitionObject;
    }
    animationDidFinish() {
    }
    static _setAbsoluteSizeAndPosition(element, left, top, width, height, zIndex = 0) {
        // if (!UIView._transformAttribute) {
        //     UIView._transformAttribute = (('transform' in document.documentElement.style) ? 'transform' : undefined);
        //     UIView._transformAttribute = UIView._transformAttribute || (('-webkit-transform' in document.documentElement.style) ? '-webkit-transform' : 'undefined');
        //     UIView._transformAttribute = UIView._transformAttribute || (('-moz-transform' in document.documentElement.style) ? '-moz-transform' : 'undefined');
        //     UIView._transformAttribute = UIView._transformAttribute || (('-ms-transform' in document.documentElement.style) ? '-ms-transform' : 'undefined');
        //     UIView._transformAttribute = UIView._transformAttribute || (('-o-transform' in document.documentElement.style) ? '-o-transform' : 'undefined');
        // }
        if (!IS(element) || !element.obeyAutolayout && !element.getAttribute("obeyAutolayout")) {
            return;
        }
        if (element.id == "mainView") {
            var asd = 1;
        }
        if (IS(height)) {
            height = height.integerValue + "px";
        }
        if (IS(width)) {
            width = width.integerValue + "px";
        }
        var str = element.style.cssText;
        const frameTransform = UIView._transformAttribute + ": translate3d(" + (1 * left).integerValue + "px, " +
            (1 * top).integerValue + "px, " + zIndex.integerValue + "px)";
        if (element.UIView) {
            str = str + frameTransform + ";";
        }
        else {
            element.UIView._frameTransform = frameTransform;
        }
        if (height == nil) {
            str = str + " height: unset;";
        }
        else {
            str = str + " height:" + height + ";";
        }
        if (width == nil) {
            str = str + " width: unset;";
        }
        else {
            str = str + " width:" + width + ";";
        }
        if (element.id == "mainView") {
            var asd = 1;
        }
        element.style.cssText = element.style.cssText + str;
    }
    static performAutoLayout(parentElement, visualFormatArray, constraintsArray) {
        const view = new AutoLayout.View();
        if (IS(visualFormatArray) && IS(visualFormatArray.length)) {
            view.addConstraints(AutoLayout.VisualFormat.parse(visualFormatArray, { extended: true }));
        }
        if (IS(constraintsArray) && IS(constraintsArray.length)) {
            view.addConstraints(constraintsArray);
        }
        const elements = {};
        for (var key in view.subViews) {
            if (!view.subViews.hasOwnProperty(key)) {
                continue;
            }
            var element = nil;
            try {
                element = parentElement.querySelector("#" + key);
            }
            catch (error) {
                //console.log("Error occurred " + error);
            }
            if (element && !element.obeyAutolayout && !element.getAttribute("obeyAutolayout")) {
            }
            else if (element) {
                element.className += element.className ? " abs" : "abs";
                elements[key] = element;
            }
        }
        var parentUIView = nil;
        if (parentElement.UIView) {
            parentUIView = parentElement.UIView;
        }
        const updateLayout = function () {
            view.setSize(parentElement ? parentElement.clientWidth : window.innerWidth, parentElement ? parentElement.clientHeight : window.innerHeight);
            for (key in view.subViews) {
                if (!view.subViews.hasOwnProperty(key)) {
                    continue;
                }
                const subView = view.subViews[key];
                if (elements[key]) {
                    UIView._setAbsoluteSizeAndPosition(elements[key], subView.left, subView.top, subView.width, subView.height);
                }
            }
            parentUIView.didLayoutSubviews();
        };
        updateLayout();
        return updateLayout;
    }
    static runFunctionBeforeNextFrame(step) {
        if (IS_SAFARI) {
            // This creates a microtask
            Promise.resolve().then(step);
        }
        else {
            window.requestAnimationFrame(step);
        }
    }
    static scheduleLayoutViewsIfNeeded() {
        UIView.runFunctionBeforeNextFrame(UIView.layoutViewsIfNeeded);
    }
    static layoutViewsIfNeeded() {
        for (var i = 0; i < UIView._viewsToLayout.length; i++) {
            const view = UIView._viewsToLayout[i];
            view.layoutIfNeeded();
        }
        UIView._viewsToLayout = [];
    }
    setNeedsLayout() {
        if (this._shouldLayout) {
            return;
        }
        this._shouldLayout = YES;
        // Register view for layout before next frame
        UIView._viewsToLayout.push(this);
        if (UIView._viewsToLayout.length == 1) {
            UIView.scheduleLayoutViewsIfNeeded();
        }
    }
    get needsLayout() {
        return this._shouldLayout;
    }
    layoutIfNeeded() {
        if (!this._shouldLayout) {
            return;
        }
        this._shouldLayout = NO;
        try {
            this.layoutSubviews();
        }
        catch (exception) {
            console.log(exception);
        }
    }
    layoutSubviews() {
        this._shouldLayout = NO;
        // Autolayout
        //window.removeEventListener('resize', this._updateLayoutFunction);
        if (this.constraints.length) {
            this._updateLayoutFunction = UIView.performAutoLayout(this.viewHTMLElement, null, this.constraints);
        }
        //this._updateLayoutFunction = this.layoutSubviews.bind(this);
        //window.addEventListener('resize', this._updateLayoutFunction);
        this._viewControllerLayoutFunction();
        this.applyClassesAndStyles();
        for (var i = 0; i < this.subviews.length; i++) {
            const subview = this.subviews[i];
            subview.calculateAndSetViewFrame();
            //subview.layoutSubviews();
        }
        this.didLayoutSubviews();
    }
    applyClassesAndStyles() {
        //var classesString = "";
        for (var i = 0; i < this.styleClasses.length; i++) {
            const styleClass = this.styleClasses[i];
            if (styleClass) {
                this.viewHTMLElement.classList.add(styleClass);
            }
            //classesString = classesString + " " + styleClass;
        }
        //this.viewHTMLElement.className = classesString;
    }
    didLayoutSubviews() {
        this._didLayoutSubviewsDelegateFunction();
    }
    get constraints() {
        return this._constraints;
    }
    set constraints(constraints) {
        this._constraints = constraints;
    }
    addConstraint(constraint) {
        this.constraints.push(constraint);
    }
    addConstraintsWithVisualFormat(visualFormatArray) {
        this.constraints = this.constraints.concat(AutoLayout.VisualFormat.parse(visualFormatArray, { extended: true }));
    }
    static constraintWithView(view, attribute, relation, toView, toAttribute, multiplier, constant, priority) {
        var UIViewObject = nil;
        var viewID = null;
        if (view) {
            if (view.isKindOfClass && view.isKindOfClass(UIView)) {
                UIViewObject = view;
                view = view.viewHTMLElement;
            }
            viewID = view.id;
        }
        var toUIViewObject = nil;
        var toViewID = null;
        if (toView) {
            if (toView.isKindOfClass && view.isKindOfClass(UIView)) {
                toUIViewObject = toView;
                toView = toView.viewHTMLElement;
            }
            toViewID = toView.id;
        }
        const constraint = {
            view1: viewID,
            attr1: attribute,
            relation: relation,
            view2: toViewID,
            attr2: toAttribute,
            multiplier: multiplier,
            constant: constant,
            priority: priority
        };
        return constraint;
    }
    subviewWithID(viewID) {
        var resultHTMLElement = nil;
        try {
            resultHTMLElement = this.viewHTMLElement.querySelector("#" + viewID);
        }
        catch (error) {
        }
        if (resultHTMLElement && resultHTMLElement.UIView) {
            return resultHTMLElement.UIView;
        }
        return nil;
    }
    rectangleContainingSubviews() {
        const center = this.bounds.center;
        var result = new UIRectangle(center.x, center.y, 0, 0);
        for (var i = 0; i < this.subviews.length; i++) {
            const subview = this.subviews[i];
            var frame = subview.frame;
            const rectangleContainingSubviews = subview.rectangleContainingSubviews();
            frame = frame.concatenateWithRectangle(rectangleContainingSubviews);
            result = result.concatenateWithRectangle(frame);
        }
        return result;
    }
    hasSubview(view) {
        // This is for performance reasons
        if (!IS(view)) {
            return NO;
        }
        for (var i = 0; i < this.subviews.length; i++) {
            const subview = this.subviews[i];
            if (subview == view) {
                return YES;
            }
        }
        return NO;
    }
    get viewBelowThisView() {
        const result = (this.viewHTMLElement.previousElementSibling || {}).UIView;
        return result;
    }
    get viewAboveThisView() {
        const result = (this.viewHTMLElement.nextElementSibling || {}).UIView;
        return result;
    }
    addSubview(view, aboveView) {
        if (!this.hasSubview(view) && IS(view)) {
            view.willMoveToSuperview(this);
            if (IS(aboveView)) {
                this.viewHTMLElement.insertBefore(view.viewHTMLElement, aboveView.viewHTMLElement.nextSibling);
                this.subviews.insertElementAtIndex(this.subviews.indexOf(aboveView), view);
            }
            else {
                this.viewHTMLElement.appendChild(view.viewHTMLElement);
                this.subviews.push(view);
            }
            view.didMoveToSuperview(this);
            if (this.superview && this.isMemberOfViewTree) {
                view.broadcastEventInSubtree({
                    name: UIView.broadcastEventName.AddedToViewTree,
                    parameters: nil
                });
            }
            this.setNeedsLayout();
        }
    }
    addSubviews(views) {
        views.forEach(function (view, index, array) {
            this.addSubview(view);
        }, this);
    }
    moveToBottomOfSuperview() {
        if (IS(this.superview)) {
            const bottomView = this.superview.subviews.firstElement;
            if (bottomView == this) {
                return;
            }
            this.superview.subviews.removeElement(this);
            this.superview.subviews.insertElementAtIndex(0, this);
            this.superview.viewHTMLElement.insertBefore(this.viewHTMLElement, bottomView.viewHTMLElement);
        }
    }
    moveToTopOfSuperview() {
        if (IS(this.superview)) {
            const topView = this.superview.subviews.lastElement;
            if (topView == this) {
                return;
            }
            this.superview.subviews.removeElement(this);
            this.superview.subviews.push(this);
            this.superview.viewHTMLElement.appendChild(this.viewHTMLElement);
        }
    }
    removeFromSuperview() {
        if (IS(this.superview)) {
            this.forEachViewInSubtree(function (view) {
                view.blur();
            });
            const index = this.superview.subviews.indexOf(this);
            if (index > -1) {
                this.superview.subviews.splice(index, 1);
                this.superview.viewHTMLElement.removeChild(this.viewHTMLElement);
                this.superview = nil;
                this.broadcastEventInSubtree({
                    name: UIView.broadcastEventName.RemovedFromViewTree,
                    parameters: nil
                });
            }
        }
    }
    willAppear() {
    }
    willMoveToSuperview(superview) {
        this._setInnerHTMLFromKeyIfPossible();
        this._setInnerHTMLFromLocalizedTextObjectIfPossible();
    }
    didMoveToSuperview(superview) {
        this.superview = superview;
    }
    wasAddedToViewTree() {
    }
    wasRemovedFromViewTree() {
    }
    get isMemberOfViewTree() {
        var element = this.viewHTMLElement;
        for (var i = 0; element; i = i) {
            if (element.parentElement && element.parentElement == document.body) {
                return YES;
            }
            element = element.parentElement;
        }
        return NO;
    }
    get allSuperviews() {
        const result = [];
        var view = this;
        for (var i = 0; IS(view); i = i) {
            result.push(view);
            view = view.superview;
        }
        return result;
    }
    setNeedsLayoutOnAllSuperviews() {
        this.allSuperviews.reverse().forEach(function (view, index, array) {
            view.setNeedsLayout();
        });
    }
    setNeedsLayoutUpToRootView() {
        this.setNeedsLayoutOnAllSuperviews();
        this.setNeedsLayout();
    }
    focus() {
        this.viewHTMLElement.focus();
    }
    blur() {
        this.viewHTMLElement.blur();
    }
    _loadUIEvents() {
        //this.viewHTMLElement = nil;
        const isTouchEventClassDefined = NO || window.TouchEvent;
        const pauseEvent = (event, forced = NO) => {
            if (this.pausesPointerEvents || forced) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
                if (event.preventDefault) {
                    event.preventDefault();
                }
                event.cancelBubble = true;
                event.returnValue = false;
                return false;
            }
            if (event.stopPropagation && this.stopsPointerEventPropagation) {
                event.stopPropagation();
            }
        };
        const onMouseDown = (event) => {
            if ((this.ignoresTouches && isTouchEventClassDefined && event instanceof TouchEvent) ||
                ((this.ignoresMouse || (IS(this._touchEventTime) && (Date.now() - this._touchEventTime) > 500)) &&
                    event instanceof MouseEvent)) {
                return;
            }
            this.sendControlEventForKey(UIView.controlEvent.PointerDown, event);
            this._isPointerInside = YES;
            this._isPointerValid = YES;
            this._initialPointerPosition = new UIPoint(event.clientX, event.clientY);
            if (isTouchEventClassDefined && event instanceof TouchEvent) {
                this._touchEventTime = Date.now();
                this._initialPointerPosition = new UIPoint(event.touches[0].clientX, event.touches[0].clientY);
                if (event.touches.length > 1) {
                    onTouchCancel(event);
                    return;
                }
            }
            else {
                this._touchEventTime = nil;
                pauseEvent(event);
            }
            this._hasPointerDragged = NO;
        };
        const onTouchStart = onMouseDown;
        const onmouseup = (event) => {
            if (!this._isPointerValid) {
                return;
            }
            if ((this.ignoresTouches && isTouchEventClassDefined && event instanceof TouchEvent) ||
                (this.ignoresMouse && event instanceof MouseEvent)) {
                return;
            }
            if (this._isPointerInside) {
                onPointerUpInside(event);
                if (!this._hasPointerDragged) {
                    this.sendControlEventForKey(UIView.controlEvent.PointerTap, event);
                }
            }
            // This has to be sent after the more specific event so that UIButton can ignore it when not highlighted
            this.sendControlEventForKey(UIView.controlEvent.PointerUp, event);
            pauseEvent(event);
        };
        const onTouchEnd = onmouseup;
        // function onMouseEnter(event) {
        //     this.sendControlEventForKey(UIView.controlEvent.PointerEnter, event);
        //     this._isPointerInside = YES;
        //     pauseEvent(event);
        // }
        const onmouseout = (event) => {
            if ((this.ignoresTouches && isTouchEventClassDefined && event instanceof TouchEvent) ||
                (this.ignoresMouse && event instanceof MouseEvent)) {
                return;
            }
            this.sendControlEventForKey(UIView.controlEvent.PointerLeave, event);
            this._isPointerInside = NO;
            pauseEvent(event);
        };
        const onTouchLeave = onmouseout;
        var onTouchCancel = function (event) {
            if (!this._isPointerValid) {
                return;
            }
            if ((this.ignoresTouches && isTouchEventClassDefined && event instanceof TouchEvent) ||
                (this.ignoresMouse && event instanceof MouseEvent)) {
                return;
            }
            this._isPointerValid = NO;
            this.sendControlEventForKey(UIView.controlEvent.PointerCancel, event);
        }.bind(this);
        const onmouseover = (event) => {
            if ((this.ignoresTouches && isTouchEventClassDefined && event instanceof TouchEvent) ||
                (this.ignoresMouse && event instanceof MouseEvent)) {
                return;
            }
            this.sendControlEventForKey(UIView.controlEvent.PointerHover, event);
            this._isPointerInside = YES;
            this._isPointerValid = YES;
            pauseEvent(event);
        };
        const onMouseMove = (event) => {
            if (!this._isPointerValid) {
                return;
            }
            if ((this.ignoresTouches && isTouchEventClassDefined && event instanceof TouchEvent) ||
                (this.ignoresMouse && event instanceof MouseEvent)) {
                return;
            }
            if (IS_NOT(this._initialPointerPosition)) {
                this._initialPointerPosition = new UIPoint(event.clientX, event.clientY);
            }
            if (new UIPoint(event.clientX, event.clientY).to(this._initialPointerPosition).length >
                this._pointerDragThreshold) {
                this._hasPointerDragged = YES;
            }
            this.sendControlEventForKey(UIView.controlEvent.PointerMove, event);
            pauseEvent(event);
        };
        const onTouchMove = function (event) {
            if (!this._isPointerValid) {
                return;
            }
            if ((this.ignoresTouches && isTouchEventClassDefined && event instanceof TouchEvent) ||
                (this.ignoresMouse && event instanceof MouseEvent)) {
                return;
            }
            if (event.touches.length > 1) {
                onTouchZoom(event);
                return;
            }
            const touch = event.touches[0];
            if (new UIPoint(touch.clientX, touch.clientY).to(this._initialPointerPosition).length >
                this._pointerDragThreshold) {
                this._hasPointerDragged = YES;
            }
            if (this._isPointerInside && this.viewHTMLElement !=
                document.elementFromPoint(touch.clientX, touch.clientY)) {
                this._isPointerInside = NO;
                this.sendControlEventForKey(UIView.controlEvent.PointerLeave, event);
            }
            this.sendControlEventForKey(UIView.controlEvent.PointerMove, event);
            //pauseEvent(event);
        };
        var onTouchZoom = function onTouchZoom(event) {
            this.sendControlEventForKey(UIView.controlEvent.MultipleTouches, event);
        }.bind(this);
        var onPointerUpInside = (event) => {
            pauseEvent(event);
            this.sendControlEventForKey(UIView.controlEvent.PointerUpInside, event);
        };
        function eventKeyIsEnter(event) {
            if (event.keyCode !== 13) {
                return NO;
            }
            return YES;
        }
        function eventKeyIsTab(event) {
            if (event.keyCode !== 9) {
                return NO;
            }
            return YES;
        }
        function eventKeyIsEsc(event) {
            var result = false;
            if ("key" in event) {
                result = (event.key == "Escape" || event.key == "Esc");
            }
            else {
                result = (event.keyCode == 27);
            }
            return result;
        }
        function eventKeyIsLeft(event) {
            if (event.keyCode != "37") {
                return NO;
            }
            return YES;
        }
        function eventKeyIsRight(event) {
            if (event.keyCode != "39") {
                return NO;
            }
            return YES;
        }
        function eventKeyIsDown(event) {
            if (event.keyCode != "40") {
                return NO;
            }
            return YES;
        }
        function eventKeyIsUp(event) {
            if (event.keyCode != "38") {
                return NO;
            }
            return YES;
        }
        const onKeyDown = function (event) {
            if (eventKeyIsEnter(event)) {
                this.sendControlEventForKey(UIView.controlEvent.EnterDown, event);
            }
            if (eventKeyIsEsc(event)) {
                this.sendControlEventForKey(UIView.controlEvent.EscDown, event);
            }
            if (eventKeyIsTab(event) && this._controlEventTargets.TabDown && this._controlEventTargets.TabDown.length) {
                this.sendControlEventForKey(UIView.controlEvent.TabDown, event);
                pauseEvent(event, YES);
            }
            if (eventKeyIsLeft(event)) {
                this.sendControlEventForKey(UIView.controlEvent.LeftArrowDown, event);
            }
            if (eventKeyIsRight(event)) {
                this.sendControlEventForKey(UIView.controlEvent.RightArrowDown, event);
            }
            if (eventKeyIsDown(event)) {
                this.sendControlEventForKey(UIView.controlEvent.DownArrowDown, event);
            }
            if (eventKeyIsUp(event)) {
                this.sendControlEventForKey(UIView.controlEvent.UpArrowDown, event);
            }
        }.bind(this);
        const onKeyUp = function (event) {
            if (eventKeyIsEnter(event)) {
                this.sendControlEventForKey(UIView.controlEvent.EnterUp, event);
            }
        }.bind(this);
        const onfocus = function (event) {
            this.sendControlEventForKey(UIView.controlEvent.Focus, event);
        }.bind(this);
        const onblur = function (event) {
            this.sendControlEventForKey(UIView.controlEvent.Blur, event);
        }.bind(this);
        // Mouse and touch start events
        this._viewHTMLElement.onmousedown = onMouseDown.bind(this);
        this._viewHTMLElement.ontouchstart = onTouchStart.bind(this);
        // this.viewHTMLElement.addEventListener("mousedown", onMouseDown.bind(this), false)
        // this.viewHTMLElement.addEventListener('touchstart', onTouchStart.bind(this), false)
        // //this.viewHTMLElement.addEventListener("mouseenter", onMouseEnter.bind(this), false);
        // Mouse and touch move events
        this._viewHTMLElement.onmousemove = onMouseMove.bind(this);
        this._viewHTMLElement.ontouchmove = onTouchMove.bind(this);
        // this.viewHTMLElement.addEventListener("mousemove", onMouseMove.bind(this), false)
        // this.viewHTMLElement.addEventListener('touchmove', onTouchMove.bind(this), false)
        //this.viewHTMLElement.addEventListener("mousewheel", onmousewheel.bind(this), false)
        this._viewHTMLElement.onmouseover = onmouseover.bind(this);
        // this.viewHTMLElement.addEventListener("mouseover", onmouseover.bind(this), false)
        // Mouse and touch end events
        this._viewHTMLElement.onmouseup = onmouseup.bind(this);
        this._viewHTMLElement.ontouchend = onTouchEnd.bind(this);
        this._viewHTMLElement.ontouchcancel = onTouchCancel.bind(this);
        // this.viewHTMLElement.addEventListener("mouseup", onmouseup.bind(this), false)
        // this.viewHTMLElement.addEventListener('touchend', onTouchEnd.bind(this), false)
        // this.viewHTMLElement.addEventListener('touchcancel', onTouchCancel.bind(this), false)
        this._viewHTMLElement.onmouseout = onmouseout.bind(this);
        // this.viewHTMLElement.addEventListener("mouseout", onmouseout.bind(this), false)
        this._viewHTMLElement.addEventListener("touchleave", onTouchLeave.bind(this), false);
        // this.viewHTMLElement.onkeydown = onkeydown
        // this.viewHTMLElement.onkeyup = onkeyup
        this._viewHTMLElement.addEventListener("keydown", onKeyDown, false);
        this._viewHTMLElement.addEventListener("keyup", onKeyUp, false);
        // Focus events
        this._viewHTMLElement.onfocus = onfocus;
        this._viewHTMLElement.onblur = onblur;
        // this.viewHTMLElement.addEventListener("focus", onfocus, true)
        // this.viewHTMLElement.addEventListener("blur", onblur, true)
    }
    get addControlEventTarget() {
        const eventKeys = [];
        const result = new Proxy(this.constructor.controlEvent, {
            get: (target, key, receiver) => {
                eventKeys.push(key);
                return result;
            },
            set: (target, key, value, receiver) => {
                eventKeys.push(key);
                this.addTargetForControlEvents(eventKeys, value);
                return true;
            }
        });
        return result;
    }
    addTargetForControlEvents(eventKeys, targetFunction) {
        eventKeys.forEach(function (key, index, array) {
            this.addTargetForControlEvent(key, targetFunction);
        }, this);
    }
    addTargetForControlEvent(eventKey, targetFunction) {
        var targets = this._controlEventTargets[eventKey];
        if (!targets) {
            // @ts-ignore
            targets = [];
            this._controlEventTargets[eventKey] = targets;
        }
        if (targets.indexOf(targetFunction) == -1) {
            targets.push(targetFunction);
        }
    }
    removeTargetForControlEvent(eventKey, targetFunction) {
        const targets = this._controlEventTargets[eventKey];
        if (!targets) {
            return;
        }
        const index = targets.indexOf(targetFunction);
        if (index != -1) {
            targets.splice(index, 1);
        }
    }
    removeTargetForControlEvents(eventKeys, targetFunction) {
        eventKeys.forEach(function (key, index, array) {
            this.removeTargetForControlEvent(key, targetFunction);
        }, this);
    }
    sendControlEventForKey(eventKey, nativeEvent) {
        var targets = this._controlEventTargets[eventKey];
        if (!targets) {
            return;
        }
        targets = targets.copy();
        for (var i = 0; i < targets.length; i++) {
            const target = targets[i];
            target(this, nativeEvent);
        }
    }
    broadcastEventInSubtree(event) {
        this.forEachViewInSubtree(function (view) {
            view.didReceiveBroadcastEvent(event);
            if (view._didReceiveBroadcastEventDelegateFunction) {
                view._didReceiveBroadcastEventDelegateFunction(event);
            }
        });
    }
    didReceiveBroadcastEvent(event) {
        if (event.name == UIView.broadcastEventName.PageDidScroll) {
            this._isPointerValid = NO;
        }
        if (event.name == UIView.broadcastEventName.AddedToViewTree) {
            this.wasAddedToViewTree();
        }
        if (event.name == UIView.broadcastEventName.RemovedFromViewTree) {
            this.wasRemovedFromViewTree();
        }
        if (event.name == UIView.broadcastEventName.LanguageChanged || event.name ==
            UIView.broadcastEventName.AddedToViewTree) {
            this._setInnerHTMLFromKeyIfPossible();
            this._setInnerHTMLFromLocalizedTextObjectIfPossible();
        }
    }
    forEachViewInSubtree(functionToCall) {
        functionToCall(this);
        this.subviews.forEach(function (subview, index, array) {
            subview.forEachViewInSubtree(functionToCall);
        });
    }
    rectangleInView(rectangle, view) {
        if (!view.isMemberOfViewTree || !this.isMemberOfViewTree) {
            return nil;
        }
        const viewClientRectangle = view.viewHTMLElement.getBoundingClientRect();
        const viewLocation = new UIPoint(viewClientRectangle.left, viewClientRectangle.top);
        const selfClientRectangle = this.viewHTMLElement.getBoundingClientRect();
        const selfLocation = new UIPoint(selfClientRectangle.left, selfClientRectangle.top);
        const offsetPoint = selfLocation.subtract(viewLocation);
        return rectangle.copy().offsetByPoint(offsetPoint);
    }
    rectangleFromView(rectangle, view) {
        return view.rectangleInView(rectangle, this);
    }
    intrinsicContentSizeWithConstraints(constrainingHeight = 0, constrainingWidth = 0) {
        // This works but is slow
        const result = new UIRectangle(0, 0, 0, 0);
        if (this.rootView.forceIntrinsicSizeZero) {
            return result;
        }
        var temporarilyInViewTree = NO;
        var nodeAboveThisView;
        if (!this.isMemberOfViewTree) {
            document.body.appendChild(this.viewHTMLElement);
            temporarilyInViewTree = YES;
            nodeAboveThisView = this.viewHTMLElement.nextSibling;
        }
        const height = this.style.height;
        const width = this.style.width;
        this.style.height = "" + constrainingHeight;
        this.style.width = "" + constrainingWidth;
        const left = this.style.left;
        const right = this.style.right;
        const bottom = this.style.bottom;
        const top = this.style.top;
        this.style.left = "";
        this.style.right = "";
        this.style.bottom = "";
        this.style.top = "";
        const resultHeight = this.viewHTMLElement.scrollHeight;
        const whiteSpace = this.style.whiteSpace;
        this.style.whiteSpace = "nowrap";
        const resultWidth = this.viewHTMLElement.scrollWidth;
        this.style.whiteSpace = whiteSpace;
        this.style.height = height;
        this.style.width = width;
        this.style.left = left;
        this.style.right = right;
        this.style.bottom = bottom;
        this.style.top = top;
        if (temporarilyInViewTree) {
            document.body.removeChild(this.viewHTMLElement);
            if (this.superview) {
                if (nodeAboveThisView) {
                    this.superview.viewHTMLElement.insertBefore(this.viewHTMLElement, nodeAboveThisView);
                }
                else {
                    this.superview.viewHTMLElement.appendChild(this.viewHTMLElement);
                }
            }
        }
        result.height = resultHeight;
        result.width = resultWidth;
        return result;
    }
    intrinsicContentWidth(constrainingHeight = 0) {
        const result = this.intrinsicContentSizeWithConstraints(constrainingHeight).width;
        return result;
    }
    intrinsicContentHeight(constrainingWidth = 0) {
        const result = this.intrinsicContentSizeWithConstraints(undefined, constrainingWidth).height;
        return result;
    }
    intrinsicContentSize() {
        return nil;
    }
}
UIView._UIViewIndex = -1;
UIView._viewsToLayout = [];
UIView._pageScale = 1;
UIView._transformAttribute = (("transform" in document.documentElement.style) ? "transform" : undefined) ||
    (("-webkit-transform" in document.documentElement.style) ? "-webkit-transform" : "undefined") ||
    (("-moz-transform" in document.documentElement.style) ? "-moz-transform" : "undefined") ||
    (("-ms-transform" in document.documentElement.style) ? "-ms-transform" : "undefined") ||
    (("-o-transform" in document.documentElement.style) ? "-o-transform" : "undefined");
UIView.constraintAttribute = {
    "left": AutoLayout.Attribute.LEFT,
    "right": AutoLayout.Attribute.RIGHT,
    "bottom": AutoLayout.Attribute.BOTTOM,
    "top": AutoLayout.Attribute.TOP,
    "centerX": AutoLayout.Attribute.CENTERX,
    "centerY": AutoLayout.Attribute.CENTERY,
    "height": AutoLayout.Attribute.HEIGHT,
    "width": AutoLayout.Attribute.WIDTH,
    "zIndex": AutoLayout.Attribute.ZINDEX,
    // Not sure what these are for
    "constant": AutoLayout.Attribute.NOTANATTRIBUTE,
    "variable": AutoLayout.Attribute.VARIABLE
};
UIView.constraintRelation = {
    "equal": AutoLayout.Relation.EQU,
    "lessThanOrEqual": AutoLayout.Relation.LEQ,
    "greaterThanOrEqual": AutoLayout.Relation.GEQ
};
UIView.controlEvent = {
    "PointerDown": "PointerDown",
    "PointerMove": "PointerMove",
    "PointerLeave": "PointerLeave",
    "PointerEnter": "PointerEnter",
    "PointerUpInside": "PointerUpInside",
    "PointerTap": "PointerTap",
    "PointerUp": "PointerUp",
    "MultipleTouches": "PointerZoom",
    "PointerCancel": "PointerCancel",
    "PointerHover": "PointerHover",
    "EnterDown": "EnterDown",
    "EnterUp": "EnterUp",
    "EscDown": "EscDown",
    "TabDown": "TabDown",
    "LeftArrowDown": "LeftArrowDown",
    "RightArrowDown": "RightArrowDown",
    "DownArrowDown": "DownArrowDown",
    "UpArrowDown": "UpArrowDown",
    "Focus": "Focus",
    "Blur": "Blur"
};
UIView.broadcastEventName = {
    "LanguageChanged": "LanguageChanged",
    "RemovedFromViewTree": "RemovedFromViewTree",
    "AddedToViewTree": "AddedToViewTree",
    "PageDidScroll": "PageDidScroll"
};
/// <reference path="./UIView.ts" />
/// <reference path="./UIObject.ts" />
class UIViewController extends UIObject {
    constructor(view) {
        super();
        this.view = view;
        this.loadIntrospectionVariables();
        this._UIViewController_constructorArguments = { "view": view };
        this._initInstanceVariables();
        this.loadSubviews();
        this.updateViewConstraints();
        this.updateViewStyles();
        this._layoutViewSubviews();
    }
    loadIntrospectionVariables() {
        this._class = UIViewController;
        this.superclass = UIObject;
    }
    _initInstanceVariables() {
        this.view = this._UIViewController_constructorArguments.view;
        this.view._viewControllerLayoutFunction = this.layoutViewsManually.bind(this);
        this.view._didLayoutSubviewsDelegateFunction = this.viewDidLayoutSubviews.bind(this);
        this.view._didReceiveBroadcastEventDelegateFunction = this.viewDidReceiveBroadcastEvent.bind(this);
        this.childViewControllers = [];
        this.parentViewController = nil;
    }
    handleRouteRecursively(route) {
        this.handleRoute(route);
        this.childViewControllers.forEach(function (controller, index, array) {
            if (!route.isHandled) {
                controller.handleRouteRecursively(route);
            }
        });
    }
    handleRoute(route) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    loadSubviews() {
    }
    viewWillAppear() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    viewDidAppear() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    viewWillDisappear() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    viewDidDisappear() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    updateViewConstraints() {
    }
    updateViewStyles() {
    }
    layoutViewsManually() {
    }
    _layoutViewSubviews() {
        this.view.layoutSubviews();
        this.viewDidLayoutSubviews();
    }
    viewDidLayoutSubviews() {
        // this.childViewControllers.forEach(function (controller, index, controllers) {
        //     controller._layoutViewSubviews();
        // })
    }
    viewDidReceiveBroadcastEvent(event) {
    }
    hasChildViewController(viewController) {
        // This is for performance reasons
        if (!IS(viewController)) {
            return NO;
        }
        for (var i = 0; i < this.childViewControllers.length; i++) {
            const childViewController = this.childViewControllers[i];
            if (childViewController == viewController) {
                return YES;
            }
        }
        return NO;
    }
    addChildViewController(viewController) {
        if (!this.hasChildViewController(viewController)) {
            viewController.willMoveToParentViewController(this);
            this.childViewControllers.push(viewController);
            //this.view.addSubview(viewController.view);
            //viewController.didMoveToParentViewController(this);
        }
    }
    removeFromParentViewController() {
        const index = this.parentViewController.childViewControllers.indexOf(this);
        if (index > -1) {
            this.parentViewController.childViewControllers.splice(index, 1);
            //this.view.removeFromSuperview();
            this.parentViewController = nil;
        }
    }
    willMoveToParentViewController(parentViewController) {
    }
    didMoveToParentViewController(parentViewController) {
        this.parentViewController = parentViewController;
    }
    removeChildViewController(controller) {
        controller.viewWillDisappear();
        if (IS(controller.parentViewController)) {
            controller.removeFromParentViewController();
        }
        if (IS(controller.view)) {
            controller.view.removeFromSuperview();
        }
        controller.viewDidDisappear();
    }
    addChildViewControllerInContainer(controller, containerView) {
        controller.viewWillAppear();
        this.addChildViewController(controller);
        containerView.addSubview(controller.view);
        controller.didMoveToParentViewController(this);
        controller.viewDidAppear();
    }
    addChildViewControllerInDialogView(controller, dialogView) {
        controller.viewWillAppear();
        this.addChildViewController(controller);
        dialogView.view = controller.view;
        var originalDismissFunction = dialogView.dismiss.bind(dialogView);
        dialogView.dismiss = animated => {
            originalDismissFunction(animated);
            this.removeChildViewController(controller);
        };
        controller.didMoveToParentViewController(this);
        controller.viewDidAppear();
    }
}
class UIDialogView extends UIView {
    constructor(elementID, viewHTMLElement) {
        super(elementID, viewHTMLElement);
        this._view = nil;
        this.animationDuration = 0.25;
        this._zIndex = 100;
        this.isVisible = NO;
        this.dismissesOnTapOutside = YES;
        this.addTargetForControlEvent(UIView.controlEvent.PointerTap, function (sender, event) {
            this.didDetectTapOutside(sender, event);
        }.bind(this));
        this.backgroundColor = UIColor.colorWithRGBA(0, 10, 25).colorWithAlpha(0.75); //CBColor.primaryContentColor.colorWithAlpha(0.75)
        this.zIndex = this._zIndex;
    }
    didDetectTapOutside(sender, event) {
        if (event.target == this.viewHTMLElement && this.dismissesOnTapOutside) {
            this.dismiss(this._appearedAnimated);
        }
    }
    set zIndex(zIndex) {
        this._zIndex = zIndex;
        this.style.zIndex = "" + zIndex;
    }
    get zIndex() {
        return this._zIndex;
    }
    set view(view) {
        this._view.removeFromSuperview();
        this._view = view;
        this.addSubview(view);
    }
    get view() {
        return this._view;
    }
    willAppear(animated = NO) {
        if (animated) {
            this.style.opacity = "0";
        }
        this.style.height = "";
        this._frame = null;
    }
    animateAppearing() {
        this.style.opacity = "1";
    }
    animateDisappearing() {
        this.style.opacity = "0";
    }
    showInView(containerView, animated) {
        animated = (animated && !IS_FIREFOX);
        this._appearedAnimated = animated;
        this.willAppear(animated);
        containerView.addSubview(this);
        if (animated) {
            this.layoutSubviews();
            UIView.animateViewOrViewsWithDurationDelayAndFunction(this, this.animationDuration, 0, undefined, function () {
                this.animateAppearing();
            }.bind(this), nil);
        }
        else {
            this.setNeedsLayout();
        }
        this.isVisible = YES;
    }
    showInRootView(animated) {
        this.showInView(UICore.main.rootViewController.view, animated);
    }
    dismiss(animated) {
        animated = (animated && !IS_FIREFOX);
        if (animated == undefined) {
            animated = this._appearedAnimated;
        }
        if (animated) {
            UIView.animateViewOrViewsWithDurationDelayAndFunction(this, this.animationDuration, 0, undefined, function () {
                this.animateDisappearing();
            }.bind(this), function () {
                if (this.isVisible == NO) {
                    this.removeFromSuperview();
                }
            }.bind(this));
        }
        else {
            this.removeFromSuperview();
        }
        this.isVisible = NO;
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
        if (event.name == UICore.broadcastEventName.WindowDidResize) {
            this.setNeedsLayout();
        }
    }
    layoutSubviews() {
        if (!IS(this.view)) {
            return;
        }
        //this.frame = this.superview.bounds;
        this.setPosition(0, 0, 0, 0, 0, "100%");
        this.setPosition(0, 0, 0, 0, UIView.pageHeight, "100%");
        const bounds = this.bounds;
        const margin = 20;
        //this.view.centerInContainer();
        this.view.style.position = "relative";
        // this.view.style.maxHeight = "" + (bounds.height - margin * 2).integerValue + "px";
        // this.view.style.maxWidth = "" + (bounds.width - margin * 2).integerValue + "px";
        // var viewIntrinsicRectangle = this.view.intrinsicContentSize();
        // this.view.frame = new UIRectangle((bounds.width - viewIntrinsicRectangle.width)*0.5,  )
        super.layoutSubviews();
    }
}
/// <reference path="./UIView.ts" />
class UIBaseButton extends UIView {
    constructor(elementID, elementType, initViewData) {
        super(elementID, nil, elementType, initViewData);
        this._selected = NO;
        this._highlighted = NO;
        this._isToggleable = NO;
        this._class = UIButton;
        this.superclass = UIView;
        this.initViewStateControl();
    }
    initViewStateControl() {
        this.class.superclass = UIView;
        // Instance variables
        this._isPointerInside = NO;
        const setHovered = function () {
            this.hovered = YES;
        }.bind(this);
        this.addTargetForControlEvent(UIView.controlEvent.PointerHover, setHovered);
        const setNotHovered = function () {
            this.hovered = NO;
        }.bind(this);
        this.addTargetForControlEvents([
            UIView.controlEvent.PointerLeave, UIView.controlEvent.PointerCancel, UIView.controlEvent.MultipleTouches
        ], setNotHovered);
        var highlightingTime;
        const setHighlighted = function () {
            this.highlighted = YES;
            highlightingTime = Date.now();
        }.bind(this);
        this.addTargetForControlEvent(UIView.controlEvent.PointerDown, setHighlighted);
        this.addTargetForControlEvent(UIView.controlEvent.PointerEnter, setHighlighted);
        const setNotHighlighted = function () {
            this.highlighted = NO;
        }.bind(this);
        const setNotHighlightedWithMinimumDuration = function () {
            const minimumDurationInMilliseconds = 50;
            const elapsedTime = Date.now() - highlightingTime;
            if (minimumDurationInMilliseconds < elapsedTime) {
                this.highlighted = NO;
            }
            else {
                setTimeout(function () {
                    this.highlighted = NO;
                }.bind(this), minimumDurationInMilliseconds - elapsedTime);
            }
        }.bind(this);
        this.addTargetForControlEvents([
            UIView.controlEvent.PointerLeave, UIView.controlEvent.PointerCancel, UIView.controlEvent.MultipleTouches
        ], setNotHighlighted);
        this.addTargetForControlEvent(UIView.controlEvent.PointerUp, setNotHighlightedWithMinimumDuration);
        // Handle enter key press
        this.addTargetForControlEvent(UIView.controlEvent.EnterDown, function () {
            setHighlighted();
            setNotHighlightedWithMinimumDuration();
        });
        this.addTargetForControlEvent(UIView.controlEvent.Focus, function (sender, event) {
            this.focused = YES;
        }.bind(this));
        this.addTargetForControlEvent(UIView.controlEvent.Blur, function (sender, event) {
            this.focused = NO;
        }.bind(this));
        this.updateContentForCurrentState();
        this.pausesPointerEvents = YES;
        this.tabIndex = 1;
        this.style.cursor = "pointer";
        //this.style.outline = "none";
        this.nativeSelectionEnabled = NO;
        this.addTargetForControlEvents([
            UIView.controlEvent.EnterDown, UIView.controlEvent.PointerUpInside
        ], function (sender, event) {
            if (this.isToggleable) {
                this.toggleSelectedState();
            }
        }.bind(this));
    }
    set hovered(hovered) {
        this._hovered = hovered;
        this.updateContentForCurrentState();
    }
    get hovered() {
        return this._hovered;
    }
    set highlighted(highlighted) {
        this._highlighted = highlighted;
        this.updateContentForCurrentState();
    }
    get highlighted() {
        return this._highlighted;
    }
    set focused(focused) {
        this._focused = focused;
        if (focused) {
            this.focus();
        }
        else {
            this.blur();
        }
        this.updateContentForCurrentState();
    }
    get focused() {
        return this._focused;
    }
    set selected(selected) {
        this._selected = selected;
        this.updateContentForCurrentState();
    }
    get selected() {
        return this._selected;
    }
    updateContentForCurrentState() {
        var updateFunction = this.updateContentForNormalState;
        if (this.selected && this.highlighted) {
            updateFunction = this.updateContentForSelectedAndHighlightedState;
        }
        else if (this.selected) {
            updateFunction = this.updateContentForSelectedState;
        }
        else if (this.focused) {
            updateFunction = this.updateContentForFocusedState;
        }
        else if (this.highlighted) {
            updateFunction = this.updateContentForHighlightedState;
        }
        else if (this.hovered) {
            updateFunction = this.updateContentForHoveredState;
        }
        if (!IS(updateFunction)) {
            this.backgroundColor = UIColor.nilColor;
        }
        else {
            updateFunction.call(this);
        }
    }
    updateContentForNormalState() {
    }
    updateContentForHoveredState() {
        this.updateContentForNormalState();
    }
    updateContentForFocusedState() {
        this.updateContentForHoveredState();
    }
    updateContentForHighlightedState() {
    }
    updateContentForSelectedState() {
    }
    updateContentForSelectedAndHighlightedState() {
        this.updateContentForSelectedState();
    }
    set enabled(enabled) {
        super.enabled = enabled;
        this.updateContentForCurrentEnabledState();
    }
    get enabled() {
        return super.enabled;
    }
    updateContentForCurrentEnabledState() {
        if (this.enabled) {
            this.alpha = 1;
        }
        else {
            this.alpha = 0.5;
        }
        this.userInteractionEnabled = this.enabled;
    }
    addStyleClass(styleClassName) {
        super.addStyleClass(styleClassName);
        if (this.styleClassName != styleClassName) {
            this.updateContentForCurrentState.call(this);
        }
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
        if (event.name == UIView.broadcastEventName.PageDidScroll || event.name ==
            UIView.broadcastEventName.AddedToViewTree) {
            this.hovered = NO;
            this.highlighted = NO;
        }
    }
    toggleSelectedState() {
        this.selected = !this.selected;
    }
    set isToggleable(isToggleable) {
        this._isToggleable = isToggleable;
    }
    get isToggleable() {
        return this._isToggleable;
    }
    layoutSubviews() {
        super.layoutSubviews();
        const bounds = this.bounds;
    }
    sendControlEventForKey(eventKey, nativeEvent) {
        if (eventKey == UIView.controlEvent.PointerUpInside && !this.highlighted) {
            // Do not send the event in this case
            //super.sendControlEventForKey(eventKey, nativeEvent);
            const asd = 1;
        }
        else {
            super.sendControlEventForKey(eventKey, nativeEvent);
        }
    }
    static getEventCoordinatesInDocument(touchOrMouseEvent) {
        // http://www.quirksmode.org/js/events_properties.html
        var posx = 0;
        var posy = 0;
        var e = touchOrMouseEvent;
        if (!e) {
            e = window.event;
        }
        if (e.pageX || e.pageY) {
            posx = e.pageX;
            posy = e.pageY;
        }
        else if (e.clientX || e.clientY) {
            posx = e.clientX + document.body.scrollLeft
                + document.documentElement.scrollLeft;
            posy = e.clientY + document.body.scrollTop
                + document.documentElement.scrollTop;
        }
        // posx and posy contain the mouse position relative to the document
        const coordinates = { "x": posx, "y": posy };
        return coordinates;
    }
    static getElementPositionInDocument(el) {
        //https://www.kirupa.com/html5/getting_mouse_click_position.htm
        var xPosition = 0;
        var yPosition = 0;
        while (el) {
            if (el.tagName == "BODY") {
                // Coordinates in document are coordinates in body, therefore subtracting the scroll position of the body is not needed
                //      // deal with browser quirks with body/window/document and page scroll
                //      var xScrollPos = el.scrollLeft || document.documentElement.scrollLeft;
                //      var yScrollPos = el.scrollTop || document.documentElement.scrollTop;
                //
                //      xPosition += (el.offsetLeft - xScrollPos + el.clientLeft);
                //      yPosition += (el.offsetTop - yScrollPos + el.clientTop);
            }
            else {
                xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
                yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
            }
            el = el.offsetParent;
        }
        return {
            x: xPosition,
            y: yPosition
        };
    }
    static convertCoordinatesFromDocumentToElement(x, y, element) {
        const elementPositionInDocument = this.getElementPositionInDocument(element);
        const coordinatesInElement = { "x": x - elementPositionInDocument.x, "y": y - elementPositionInDocument.y };
        return coordinatesInElement;
    }
    static getEventCoordinatesInElement(touchOrMouseEvent, element) {
        const coordinatesInDocument = this.getEventCoordinatesInDocument(touchOrMouseEvent);
        const coordinatesInElement = this.convertCoordinatesFromDocumentToElement(coordinatesInDocument.x, coordinatesInDocument.y, element);
        return coordinatesInElement;
    }
}
/// <reference path="./UIBaseButton.ts" />
class UILink extends UIBaseButton {
    constructor(elementID, initViewData = nil) {
        super(elementID, "a", initViewData);
        this._class = UILink;
        this.superclass = UIBaseButton;
        this.stopsPointerEventPropagation = NO;
        this.pausesPointerEvents = NO;
    }
    initView(elementID, viewHTMLElement, initViewData) {
        super.initView(elementID, viewHTMLElement, initViewData);
        this.class.superclass = UIBaseButton;
        // Instance variables
        //this.style.position = "relative"
        viewHTMLElement.onclick = this.blur.bind(this);
    }
    get viewHTMLElement() {
        return super.viewHTMLElement;
    }
    set text(text) {
        this.viewHTMLElement.textContent = text;
    }
    get text() {
        return this.viewHTMLElement.textContent;
    }
    set target(target) {
        this.viewHTMLElement.setAttribute("href", target);
    }
    get target() {
        const result = this.viewHTMLElement.getAttribute("href");
        return result;
    }
    set targetRouteForCurrentState(targetRouteForCurrentState) {
        this._targetRouteForCurrentState = targetRouteForCurrentState;
        this.updateTarget();
    }
    get targetRouteForCurrentState() {
        return this._targetRouteForCurrentState;
    }
    _targetRouteForCurrentState() {
        const result = UIRoute.currentRoute.routeByRemovingComponentsOtherThanOnesNamed(["settings"]);
        return result;
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
        if (event.name == UICore.broadcastEventName.RouteDidChange) {
            this.updateTarget();
        }
    }
    wasAddedToViewTree() {
        super.wasAddedToViewTree();
        this.updateTarget();
    }
    updateTarget() {
        const route = this.targetRouteForCurrentState();
        if (route instanceof UIRoute) {
            this.target = route.linkRepresentation;
            return;
        }
        this.target = route;
    }
    layoutSubviews() {
        super.layoutSubviews();
        const bounds = this.bounds;
    }
}
/// <reference path="./UILink.ts" />
class UILinkButton extends UILink {
    //link = new UILink(this.elementID + "Link");
    constructor(elementID, elementType, titleType) {
        super(elementID, { "elementType": elementType, "titleType": titleType });
        this._class = UILinkButton;
        this.superclass = UILink;
        this.button.addTargetForControlEvents([
            UIButton.controlEvent.EnterDown, UIButton.controlEvent.PointerUpInside
        ], function (sender, event) {
            const asd = 1;
            window.location = this.target;
        }.bind(this));
        // this.link.hidden = YES;
        // this.addSubview(this.link);
    }
    initView(elementID, viewHTMLElement, initViewData) {
        super.initView(elementID, viewHTMLElement, initViewData);
        this.class.superclass = UILink;
        // Instance variables
        this.button = new UIButton(this.elementID + "Button", initViewData.elementType, initViewData.titleType);
        this.addSubview(this.button);
        this.style.position = "absolute";
    }
    get titleLabel() {
        return this.button.titleLabel;
    }
    get imageView() {
        return this.button.imageView;
    }
    set colors(colors) {
        this.button.colors = colors;
    }
    get colors() {
        return this.button.colors;
    }
    get viewHTMLElement() {
        return super.viewHTMLElement;
    }
    set target(target) {
        this.viewHTMLElement.setAttribute("href", target);
    }
    get target() {
        const result = this.viewHTMLElement.getAttribute("href");
        return result;
    }
    layoutSubviews() {
        super.layoutSubviews();
        const bounds = this.bounds;
        this.button.frame = bounds;
        this.button.layoutSubviews();
    }
}
/// <reference path="./UIBaseButton.ts" />
class UIButton extends UIBaseButton {
    constructor(elementID, elementType, titleType = UITextView.type.span) {
        super(elementID, elementType, { "titleType": titleType });
        this.usesAutomaticTitleFontSize = NO;
        this.minAutomaticFontSize = nil;
        this.maxAutomaticFontSize = 25;
        this._class = UIButton;
        this.superclass = UIBaseButton;
    }
    initView(elementID, viewHTMLElement, initViewData) {
        this.class.superclass = UIBaseButton;
        // Instance variables
        this.colors = {
            titleLabel: {
                normal: UIColor.whiteColor,
                highlighted: UIColor.whiteColor,
                selected: UIColor.whiteColor
            },
            background: {
                normal: UIColor.blueColor,
                highlighted: UIColor.greenColor,
                selected: UIColor.redColor
            }
        };
        this._imageView = new UIImageView(elementID + "ImageView");
        this._imageView.hidden = YES;
        this.addSubview(this.imageView);
        this.imageView.fillMode = UIImageView.fillMode.aspectFitIfLarger;
        if (IS_NOT_NIL(initViewData.titleType)) {
            this._titleLabel = new UITextView(elementID + "TitleLabel", initViewData.titleType);
            this.titleLabel.style.whiteSpace = "nowrap";
            this.addSubview(this.titleLabel);
            this.titleLabel.userInteractionEnabled = NO;
        }
        this.contentPadding = 10;
        this.imageView.userInteractionEnabled = NO;
        this.titleLabel.textAlignment = UITextView.textAlignment.center;
        this.titleLabel.nativeSelectionEnabled = NO;
    }
    get contentPadding() {
        return this._contentPadding.integerValue;
    }
    set contentPadding(contentPadding) {
        this._contentPadding = contentPadding;
        this.setNeedsLayout();
    }
    set hovered(hovered) {
        this._hovered = hovered;
        this.updateContentForCurrentState();
    }
    get hovered() {
        return this._hovered;
    }
    set highlighted(highlighted) {
        this._highlighted = highlighted;
        this.updateContentForCurrentState();
    }
    get highlighted() {
        return this._highlighted;
    }
    set focused(focused) {
        this._focused = focused;
        if (focused) {
            this.focus();
        }
        else {
            this.blur();
        }
        this.updateContentForCurrentState();
    }
    get focused() {
        return this._focused;
    }
    set selected(selected) {
        this._selected = selected;
        this.updateContentForCurrentState();
    }
    get selected() {
        return this._selected;
    }
    updateContentForCurrentState() {
        var updateFunction = this.updateContentForNormalState;
        if (this.selected && this.highlighted) {
            updateFunction = this.updateContentForSelectedAndHighlightedState;
        }
        else if (this.selected) {
            updateFunction = this.updateContentForSelectedState;
        }
        else if (this.focused) {
            updateFunction = this.updateContentForFocusedState;
        }
        else if (this.highlighted) {
            updateFunction = this.updateContentForHighlightedState;
        }
        else if (this.hovered) {
            updateFunction = this.updateContentForHoveredState;
        }
        if (!IS(updateFunction)) {
            this.titleLabel.textColor = UIColor.nilColor;
            this.backgroundColor = UIColor.nilColor;
        }
        else {
            updateFunction.call(this);
        }
        this.updateContentForCurrentEnabledState();
    }
    updateContentForNormalState() {
        this.backgroundColor = this.colors.background.normal;
        this.titleLabel.textColor = this.colors.titleLabel.normal;
    }
    updateContentForHoveredState() {
        this.updateContentForNormalState();
        if (this.colors.background.hovered) {
            this.backgroundColor = this.colors.background.hovered;
        }
        if (this.colors.titleLabel.hovered) {
            this.titleLabel.textColor = this.colors.titleLabel.hovered;
        }
    }
    updateContentForFocusedState() {
        this.updateContentForHoveredState();
        if (this.colors.background.focused) {
            this.backgroundColor = this.colors.background.focused;
        }
        if (this.colors.titleLabel.focused) {
            this.titleLabel.textColor = this.colors.titleLabel.focused;
        }
    }
    updateContentForHighlightedState() {
        this.backgroundColor = this.colors.background.highlighted;
        this.titleLabel.textColor = this.colors.titleLabel.highlighted;
    }
    updateContentForSelectedState() {
        this.backgroundColor = this.colors.background.selected;
        this.titleLabel.textColor = this.colors.titleLabel.selected;
    }
    updateContentForSelectedAndHighlightedState() {
        this.updateContentForSelectedState();
        if (this.colors.background.selectedAndHighlighted) {
            this.backgroundColor = this.colors.background.selectedAndHighlighted;
        }
        if (this.colors.titleLabel.selectedAndHighlighted) {
            this.titleLabel.textColor = this.colors.titleLabel.selectedAndHighlighted;
        }
    }
    set enabled(enabled) {
        super.enabled = enabled;
        this.updateContentForCurrentState();
    }
    get enabled() {
        return super.enabled;
    }
    updateContentForCurrentEnabledState() {
        if (this.enabled) {
            this.alpha = 1;
        }
        else {
            this.alpha = 0.5;
        }
        this.userInteractionEnabled = this.enabled;
    }
    addStyleClass(styleClassName) {
        super.addStyleClass(styleClassName);
        if (this.styleClassName != styleClassName) {
            this.updateContentForCurrentState.call(this);
        }
    }
    get titleLabel() {
        return this._titleLabel;
    }
    get imageView() {
        return this._imageView;
    }
    layoutSubviews() {
        super.layoutSubviews();
        var bounds = this.bounds;
        this.hoverText = this.titleLabel.text;
        // Image only if text is not present
        if (IS_NOT(this.imageView.hidden) && !IS(this.titleLabel.text)) {
            this.imageView.frame = bounds;
        }
        // Text only if image is not present
        if (IS(this.imageView.hidden) && IS(this.titleLabel.text)) {
            var titleElement = this.titleLabel.viewHTMLElement;
            this.titleLabel.style.left = this.contentPadding;
            this.titleLabel.style.right = this.contentPadding;
            // this.titleLabel.style.marginLeft = ""
            // this.titleLabel.style.right = this.contentPadding
            this.titleLabel.style.top = "50%";
            this.titleLabel.style.transform = "translateY(-50%)";
            this.titleLabel.frame = new UIRectangle(nil, nil, nil, nil);
            if (this.usesAutomaticTitleFontSize) {
                var hidden = this.titleLabel.hidden;
                this.titleLabel.hidden = YES;
                this.titleLabel.fontSize = 15;
                this.titleLabel.fontSize = UITextView.automaticallyCalculatedFontSize(new UIRectangle(0, 0, this.bounds.height, 1 *
                    this.titleLabel.viewHTMLElement.offsetWidth), this.titleLabel.intrinsicContentSize(), this.titleLabel.fontSize, this.minAutomaticFontSize, this.maxAutomaticFontSize);
                this.titleLabel.hidden = hidden;
            }
        }
        // Image and text both present
        if (IS_NOT(this.imageView.hidden) && IS(this.titleLabel.text)) {
            const imageShareOfWidth = 0.25;
            bounds = bounds.rectangleWithInset(this.contentPadding);
            const imageFrame = bounds.copy();
            imageFrame.width = bounds.height - this.contentPadding * 0.5;
            this.imageView.frame = imageFrame;
            var titleElement = this.titleLabel.viewHTMLElement;
            this.titleLabel.style.left = imageFrame.max.x + this.contentPadding;
            this.titleLabel.style.right = this.contentPadding;
            this.titleLabel.style.top = "50%";
            this.titleLabel.style.transform = "translateY(-50%)";
            if (this.usesAutomaticTitleFontSize) {
                var hidden = this.titleLabel.hidden;
                this.titleLabel.hidden = YES;
                this.titleLabel.fontSize = 15;
                this.titleLabel.fontSize = UITextView.automaticallyCalculatedFontSize(new UIRectangle(0, 0, this.bounds.height, 1 *
                    this.titleLabel.viewHTMLElement.offsetWidth), this.titleLabel.intrinsicContentSize(), this.titleLabel.fontSize, this.minAutomaticFontSize, this.maxAutomaticFontSize);
                this.titleLabel.hidden = hidden;
            }
        }
        this.applyClassesAndStyles();
    }
    initViewStyleSelectors() {
        this.initStyleSelector("." + this.styleClassName, "background-color: lightblue;");
        // var selectorWithoutImage = "." + this.styleClassName + " ." + this.imageView.styleClassName + " + ." + this.titleLabel.styleClassName;
        // this.initStyleSelector(
        //     selectorWithoutImage,
        //     "left: " + this.contentPadding + ";" +
        //     "right: " + this.contentPadding + ";" +
        //     "top: 50%;" +
        //     "transform: translateY(-50%);");
    }
}
///<reference path="UICore/UIButton.ts"/>
class ItemView extends UIButton {
    constructor(elementID) {
        super(elementID, nil, UITextView.type.header3);
        this._class = ItemView;
        this.superclass = UIButton;
    }
    updateContentForNormalState() {
        this.backgroundColor = UIColor.transparentColor;
        this.titleLabel.textColor = UIColor.blackColor;
        this.titleLabel.alpha = 1;
    }
    updateContentForHoveredState() {
        this.titleLabel.alpha = 0.75;
    }
    updateContentForHighlightedState() {
        this.titleLabel.alpha = 0.5;
    }
    layoutSubviews() {
        // Not calling super on purpose
        //super.layoutSubviews();
        const padding = RootViewController.paddingLength;
        const labelHeight = padding * 1.5;
        const bounds = this.bounds;
        this.titleLabel.isSingleLine = NO;
        this.imageView.fillMode = UIImageView.fillMode.aspectFill;
        this.titleLabel.textAlignment = UITextView.textAlignment.left;
        this.imageView.frame = bounds.rectangleWithHeightRelativeToWidth(9 / 16);
        this.titleLabel.frame = this.imageView.frame.rectangleForNextRow(padding, this.titleLabel.intrinsicContentHeight(bounds.width));
    }
    intrinsicContentHeight(constrainingWidth = 0) {
        const padding = RootViewController.paddingLength;
        const labelHeight = padding * 1.5;
        const result = constrainingWidth * (9 / 16) + padding * 2 +
            this.titleLabel.intrinsicContentHeight(constrainingWidth);
        return result;
    }
}
if ("removeElementAtIndex" in Array.prototype == NO) {
    Array.prototype.removeElementAtIndex = function (index) {
        if (index >= 0 && index < this.length) {
            this.splice(index, 1);
        }
    };
}
if ("removeElement" in Array.prototype == NO) {
    Array.prototype.removeElement = function (element) {
        this.removeElementAtIndex(this.indexOf(element));
    };
}
if ("insertElementAtIndex" in Array.prototype == NO) {
    Array.prototype.insertElementAtIndex = function (index, element) {
        if (index >= 0 && index <= this.length) {
            this.splice(index, 0, element);
        }
    };
}
if ("replaceElementAtIndex" in Array.prototype == NO) {
    Array.prototype.replaceElementAtIndex = function (index, element) {
        this.removeElementAtIndex(index);
        this.insertElementAtIndex(index, element);
    };
}
if ("contains" in Array.prototype == NO) {
    Array.prototype.contains = function (element) {
        const result = (this.indexOf(element) != -1);
        return result;
    };
}
if ("containsAny" in Array.prototype == NO) {
    Array.prototype.containsAny = function (elements) {
        const result = this.anyMatch(function (element, index, array) {
            return elements.contains(element);
        });
        return result;
    };
}
if ("anyMatch" in Array.prototype == NO) {
    Array.prototype.anyMatch = function (functionToCall) {
        const result = (this.findIndex(functionToCall) > -1);
        return result;
    };
}
if ("noneMatch" in Array.prototype == NO) {
    Array.prototype.noneMatch = function (functionToCall) {
        const result = (this.findIndex(functionToCall) == -1);
        return result;
    };
}
if ("allMatch" in Array.prototype == NO) {
    Array.prototype.allMatch = function (functionToCall) {
        function reversedFunction(value, index, array) {
            return !functionToCall(value, index, array);
        }
        const result = (this.findIndex(reversedFunction) == -1);
        return result;
    };
}
if ("groupedBy" in Array.prototype == NO) {
    Array.prototype.groupedBy = function (funcProp) {
        return this.reduce(function (acc, val) {
            (acc[funcProp(val)] = acc[funcProp(val)] || []).push(val);
            return acc;
        }, {});
    };
}
if ("firstElement" in Array.prototype == NO) {
    Object.defineProperty(Array.prototype, "firstElement", {
        get: function firstElement() {
            const result = this[0];
            return result;
        },
        set: function (element) {
            if (this.length == 0) {
                this.push(element);
                return;
            }
            this[0] = element;
        }
    });
}
if ("lastElement" in Array.prototype == NO) {
    Object.defineProperty(Array.prototype, "lastElement", {
        get: function lastElement() {
            const result = this[this.length - 1];
            return result;
        },
        set: function (element) {
            if (this.length == 0) {
                this.push(element);
                return;
            }
            this[this.length - 1] = element;
        }
    });
}
if ("everyElement" in Array.prototype == NO) {
    Object.defineProperty(Array.prototype, "everyElement", {
        get: function everyElement() {
            var valueKeys = [];
            const targetFunction = (objects) => {
                return this.map((element, index, array) => {
                    var elementFunction = UIObject.valueForKeyPath(valueKeys.join("."), element).bind(element, objects);
                    return elementFunction();
                });
            };
            const result = new Proxy(targetFunction, {
                get: (target, key, receiver) => {
                    if (key == "UI_elementValues") {
                        return this.map((element, index, array) => UIObject.valueForKeyPath(valueKeys.join("."), element));
                    }
                    valueKeys.push(key);
                    return result;
                },
                set: (target, key, value, receiver) => {
                    valueKeys.push(key);
                    this.forEach((element, index, array) => {
                        UIObject.setValueForKeyPath(valueKeys.join("."), value, element, YES);
                    });
                    return true;
                }
            });
            return result;
        },
        set: function (element) {
            for (var i = 0; i < this.length; ++i) {
                this[i] = element;
            }
        }
    });
}
if ("copy" in Array.prototype == NO) {
    Array.prototype.copy = function () {
        const result = this.slice(0);
        return result;
    };
}
if ("arrayByRepeating" in Array.prototype == NO) {
    Array.prototype.arrayByRepeating = function (numberOfRepetitions) {
        const result = [];
        for (var i = 0; i < numberOfRepetitions; i++) {
            this.forEach(function (element, index, array) {
                result.push(element);
            });
        }
        return result;
    };
}
if ("arrayByTrimmingToLengthIfLonger" in Array.prototype == NO) {
    Array.prototype.arrayByTrimmingToLengthIfLonger = function (maxLength) {
        const result = [];
        for (var i = 0; i < maxLength && i < this.length; i++) {
            result.push(this[i]);
        }
        return result;
    };
}
if ("summedValue" in Array.prototype == NO) {
    Object.defineProperty(Array.prototype, "summedValue", {
        get: function summedValue() {
            const result = this.reduce(function (a, b) {
                return a + b;
            }, 0);
            return result;
        }
    });
}
// Warn if overriding existing method
if ("isEqualToArray" in Array.prototype == YES) {
    console.warn("Overriding existing Array.prototype.isEqualToArray. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
}
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.isEqualToArray = function (array, keyPath) {
    // if the other array is a falsy value, return
    if (!array) {
        return false;
    }
    // compare lengths - can save a lot of time 
    if (this.length != array.length) {
        return false;
    }
    var i = 0;
    const l = this.length;
    for (; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array && !keyPath) {
            // recurse into the nested arrays
            if (!this[i].isEqualToArray(array[i])) {
                return false;
            }
        }
        else if (keyPath && UIObject.valueForKeyPath(keyPath, this[i]) != UIObject.valueForKeyPath(keyPath, array[i])) {
            return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "isEqualToArray", { enumerable: false });
if ("forEach" in Object.prototype == NO) {
    Object.prototype.forEach = function (callbackFunction) {
        const keys = Object.keys(this);
        keys.forEach(function (key, index, array) {
            callbackFunction(this[key], key);
        }.bind(this));
    };
    // Hide method from for-in loops
    Object.defineProperty(Object.prototype, "forEach", { enumerable: false });
}
if ("allValues" in Object.prototype == NO) {
    Object.defineProperty(Object.prototype, "allValues", {
        get: function () {
            const values = [];
            this.forEach(function (value) {
                values.push(value);
            });
            return values;
        }
    });
}
if ("allKeys" in Object.prototype == NO) {
    Object.defineProperty(Object.prototype, "allKeys", {
        get: function () {
            const values = Object.keys(this);
            return values;
        }
    });
}
if ("objectByCopyingValuesRecursivelyFromObject" in Object.prototype == NO) {
    Object.prototype.objectByCopyingValuesRecursivelyFromObject = function (object) {
        function isAnObject(item) {
            return (item && typeof item === "object" && !Array.isArray(item));
        }
        function mergeRecursively(target, source) {
            const output = Object.assign({}, target);
            if (isAnObject(target) && isAnObject(source)) {
                Object.keys(source).forEach(function (key) {
                    if (isAnObject(source[key])) {
                        // if (!(key in target)) {
                        //     Object.assign(output, { [key]: source[key] });
                        // }
                        // else {
                        output[key] = mergeRecursively(target[key], source[key]);
                        //}
                    }
                    else {
                        Object.assign(output, { [key]: source[key] });
                    }
                });
            }
            return output;
        }
        const result = mergeRecursively(this, object);
        return result;
    };
    // Hide method from for-in loops
    Object.defineProperty(Object.prototype, "objectByCopyingValuesRecursivelyFromObject", { enumerable: false });
}
if ("contains" in String.prototype == NO) {
    String.prototype.contains = function (string) {
        const result = (this.indexOf(string) != -1);
        return result;
    };
}
if ("capitalizedString" in String.prototype == NO) {
    Object.defineProperty(Object.prototype, "capitalizedString", {
        get: function () {
            const result = this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
            return result;
        }
    });
}
if ("numericalValue" in String.prototype == NO) {
    Object.defineProperty(String.prototype, "numericalValue", {
        get: function numericalValue() {
            const result = Number(this);
            return result;
        }
    });
}
if ("isAString" in String.prototype == NO) {
    String.prototype.isAString = YES;
}
if ("isANumber" in Number.prototype == NO) {
    Number.prototype.isANumber = YES;
}
if ("integerValue" in Number.prototype == NO) {
    Object.defineProperty(Number.prototype, "integerValue", {
        get: function () {
            const result = parseInt("" + (Math.round(this) + 0.5));
            return result;
        }
    });
}
class PrimitiveNumber {
    // @ts-ignore
    static [Symbol.hasInstance](x) {
        return;
    }
}
if ("integerValue" in Boolean.prototype == NO) {
    Object.defineProperty(Boolean.prototype, "integerValue", {
        get: function () {
            if (this == true) {
                return 1;
            }
            return 0;
        }
    });
}
if ("dateString" in Date.prototype == NO) {
    Object.defineProperty(Date.prototype, "dateString", {
        get: function dateString() {
            const result = ("0" + this.getDate()).slice(-2) + "-" + ("0" + (this.getMonth() + 1)).slice(-2) + "-" +
                this.getFullYear() + " " + ("0" + this.getHours()).slice(-2) + ":" +
                ("0" + this.getMinutes()).slice(-2);
            return result;
        }
    });
}
///<reference path="UICore/UIView.ts"/>
///<reference path="RootViewController.ts"/>
///<reference path="ItemView.ts"/>
///<reference path="UICore/UICoreExtensions.ts"/>
class TableView extends UIView {
    constructor(elementID) {
        super(elementID);
        this.itemViews = [];
        this._items = [];
        this._class = TableView;
        this.superclass = UIView;
    }
    get items() {
        return this._items;
    }
    set items(items) {
        this._items = items;
        this.itemViews.everyElement.removeFromSuperview();
        this.itemViews = [];
        items.forEach((value, index, array) => {
            const itemView = new ItemView("ItemView" + index);
            itemView.titleLabel.text = value.title;
            itemView.imageView.imageSource = FIRST(value.img, "images/logo192.png");
            itemView.addControlEventTarget.EnterDown.PointerUpInside = (sender, event) => UIRoute.currentRoute.routeWithViewControllerComponent(ArticleViewController, { ID: value.id }).apply();
            this.itemViews.push(itemView);
            this.addSubview(itemView);
        });
    }
    layoutSubviews() {
        super.layoutSubviews();
        const padding = RootViewController.paddingLength;
        const bounds = this.bounds;
        // This solution is obviously not very dynamic, but I decided to not make the page have more features than the
        // React version
        var frame = bounds.rectangleWithHeight(0);
        for (let i = 0; i < this.itemViews.length; i = i + 3) {
            const itemView = FIRST_OR_NIL(this.itemViews[i]);
            frame = frame.rectangleForNextRow(0, itemView.intrinsicContentHeight(frame.width));
            itemView.frame = frame;
            const secondItemView = FIRST_OR_NIL(this.itemViews[i + 1]);
            const thirdItemView = FIRST_OR_NIL(this.itemViews[i + 2]);
            const constrainingWidth = (frame.width - padding * 0.5) / 2;
            const height = Math.max(secondItemView.intrinsicContentHeight(constrainingWidth), thirdItemView.intrinsicContentHeight(constrainingWidth));
            frame = frame.rectangleForNextRow(0, height);
            frame.distributeViewsEquallyAlongWidth([secondItemView, thirdItemView], padding * 0.5);
        }
    }
}
/// <reference path="./UICore/UIViewController.ts" />
/// <reference path="./UICore/UIDialogView.ts" />
/// <reference path="./UICore/UILinkButton.ts" />
///<reference path="ArticleViewController.ts"/>
///<reference path="TableView.ts"/>
class RootViewController extends UIViewController {
    constructor(view) {
        // Calling super
        super(view);
        this._retrieveData = GraphQL(`query ($skip: Int!, $limit: Int!) {
            newsList(skip: $skip, limit: $limit) {
                rows {
                  id
                  title
                  img
                }
              }
            }`);
        // Here are some suggested conventions that are used in UICore
        // Instance variables, it is good to initialize to nil or empty function, not leave as undefined to avoid
        // if blocks
        // this._firstView = nil;
        // this._secondView = nil;
        // this._testView = nil;
        // this._button = nil;
        // The nil object avoids unneccessary crashes by allowing you to call any function or access any variable on it, returning nil
        // Define properties with get and set functions so they can be accessed and set like variables
        // Name variables that should be private, like property variables, with a _ sign, this also holds for private functions
        // Avoid accessing variables and functions named with _ from outside as this creates strong coupling and hinders stability
        // Code for further setup if necessary
    }
    loadIntrospectionVariables() {
        super.loadIntrospectionVariables();
        this.superclass = UIViewController;
    }
    loadSubviews() {
        this.topBarView = new CellView("TopBarView");
        this.topBarView.titleLabel.text = "NEWS READER";
        this.topBarView.colors.background.normal = new UIColor("#282c34");
        this.topBarView.colors.titleLabel.normal = UIColor.whiteColor;
        this.topBarView.titleLabel.textAlignment = UITextView.textAlignment.center;
        this.view.addSubview(this.topBarView);
        this.backToMainButton = new CBFlatButton();
        this.backToMainButton.titleLabel.text = "&#8592;";
        this.backToMainButton.colors = {
            titleLabel: {
                normal: UIColor.whiteColor,
                highlighted: UIColor.whiteColor.colorWithAlpha(0.75),
                selected: UIColor.whiteColor.colorWithAlpha(0.5)
            },
            background: {
                normal: UIColor.transparentColor,
                highlighted: UIColor.transparentColor,
                selected: UIColor.transparentColor
            }
        };
        this.backToMainButton.calculateAndSetViewFrame = function () {
            this.setPosition(0, nil, 0, 0, nil, 50);
        };
        this.topBarView.addSubview(this.backToMainButton);
        this.backToMainButton.addControlEventTarget.EnterDown.PointerUpInside = (sender, event) => UIRoute.currentRoute.routeByRemovingComponentNamed(ArticleViewController.routeComponentName).apply();
    }
    handleRoute(route) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            _super("handleRoute").call(this, route);
            this.backToMainButton.hidden = NO;
            if (IS(route.componentWithName(ArticleViewController.routeComponentName))) {
                // Show article view
                if (!IS(this.articleViewController)) {
                    this.articleViewController = new ArticleViewController(new UIView("ArticleView"));
                }
                this.contentViewController = this.articleViewController;
            }
            else {
                // Main view controller
                if (!IS(this.mainViewController)) {
                    this._tableView = new TableView("mainView");
                    this.mainViewController = new UIViewController(this._tableView);
                }
                this.contentViewController = this.mainViewController;
                this.backToMainButton.hidden = YES;
                // Update _tableView with data
                this._data = this._data || (yield FIRST_OR_NIL(this._retrieveData)({ skip: 0, limit: 200 }));
                this._tableView.items = this._data.newsList.rows;
            }
        });
    }
    set contentViewController(controller) {
        if (this.contentViewController == controller) {
            return;
        }
        if (this.contentViewController) {
            this.removeChildViewController(this.contentViewController);
        }
        this._contentViewController = controller;
        this.addChildViewControllerInContainer(controller, this.view);
        this._layoutViewSubviews();
        this.view.setNeedsLayout();
    }
    get contentViewController() {
        return this._contentViewController || nil;
    }
    static get paddingLength() {
        return 20;
    }
    get paddingLength() {
        return this.class.paddingLength;
    }
    layoutViewsManually() {
        super.layoutViewsManually();
        // View bounds
        var bounds = this.view.bounds;
        this.topBarView.frame = bounds.rectangleWithHeight(50);
        this.contentViewController.view.frame = this.topBarView.frame.rectangleForNextRow(0, this.contentViewController.view.intrinsicContentHeight(bounds.width));
    }
}
// @ts-ignore
class UIRoute extends Array {
    constructor(hash) {
        super();
        this._isHandled = NO;
        this.completedComponents = [];
        if (!hash || !hash.startsWith) {
            return;
        }
        if (hash.startsWith("#")) {
            hash = hash.slice(1);
        }
        hash = decodeURIComponent(hash);
        const components = hash.split("]");
        components.forEach(function (component, index, array) {
            const componentName = component.split("[")[0];
            const parameters = {};
            if (!componentName) {
                return;
            }
            const parametersString = component.split("[")[1] || "";
            const parameterPairStrings = parametersString.split(",") || [];
            parameterPairStrings.forEach(function (pairString, index, array) {
                const keyAndValueArray = pairString.split(":");
                const key = decodeURIComponent(keyAndValueArray[0]);
                const value = decodeURIComponent(keyAndValueArray[1]);
                if (key) {
                    parameters[key] = value;
                }
            });
            this.push({
                name: componentName,
                parameters: parameters
            });
        }, this);
    }
    static get currentRoute() {
        return new UIRoute(window.location.hash);
    }
    apply() {
        window.location.hash = this.stringRepresentation;
    }
    applyByReplacingCurrentRouteInHistory() {
        window.location.replace(this.linkRepresentation);
    }
    copy() {
        var result = new UIRoute();
        result = Object.assign(result, this);
        return result;
    }
    childRoute() {
        var result = this.copy();
        result.completedComponents.forEach(function (component, index, completedComponents) {
            var indexInResult = result.indexOf(component);
            if (indexInResult > -1) {
                result.splice(indexInResult, 1);
            }
        });
        result.completedComponents = [];
        result.parentRoute = this;
        return result;
    }
    routeByRemovingComponentsOtherThanOnesNamed(componentNames) {
        const result = this.copy();
        const indexesToRemove = [];
        result.forEach(function (component, index, array) {
            if (!componentNames.contains(component.name)) {
                indexesToRemove.push(index);
            }
        });
        indexesToRemove.forEach(function (indexToRemove, index, array) {
            result.removeElementAtIndex(indexToRemove);
        });
        return result;
    }
    routeByRemovingComponentNamed(componentName) {
        const result = this.copy();
        const componentIndex = result.findIndex(function (component, index) {
            return (component.name == componentName);
        });
        if (componentIndex != -1) {
            result.splice(componentIndex, 1);
        }
        return result;
    }
    routeByRemovingParameterInComponent(componentName, parameterName, removeComponentIfEmpty = NO) {
        var result = this.copy();
        var parameters = result.componentWithName(componentName).parameters;
        if (IS_NOT(parameters)) {
            parameters = {};
        }
        delete parameters[parameterName];
        result = result.routeWithComponent(componentName, parameters);
        if (removeComponentIfEmpty && Object.keys(parameters).length == 0) {
            result = result.routeByRemovingComponentNamed(componentName);
        }
        return result;
    }
    routeBySettingParameterInComponent(componentName, parameterName, valueToSet) {
        var result = this.copy();
        if (IS_NIL(valueToSet) || IS_NIL(parameterName)) {
            return result;
        }
        var parameters = result.componentWithName(componentName).parameters;
        if (IS_NOT(parameters)) {
            parameters = {};
        }
        parameters[parameterName] = valueToSet;
        result = result.routeWithComponent(componentName, parameters);
        return result;
    }
    routeWithViewControllerComponent(viewController, parameters, extendParameters = NO) {
        return this.routeWithComponent(viewController.routeComponentName, parameters, extendParameters);
    }
    routeWithComponent(name, parameters, extendParameters = NO) {
        const result = this.copy();
        var component = result.componentWithName(name);
        if (IS_NOT(component)) {
            component = {
                name: name,
                parameters: {}
            };
            result.push(component);
        }
        if (IS_NOT(parameters)) {
            parameters = {};
        }
        if (extendParameters) {
            component.parameters = Object.assign(component.parameters, parameters);
        }
        else {
            component.parameters = parameters;
        }
        return result;
    }
    navigateBySettingComponent(name, parameters, extendParameters = NO) {
        this.routeWithComponent(name, parameters, extendParameters).apply();
    }
    componentWithViewController(viewController) {
        return this.componentWithName(viewController.routeComponentName);
    }
    componentWithName(name) {
        var result = nil;
        this.forEach(function (component, index, self) {
            if (component.name == name) {
                result = component;
            }
        });
        return result;
    }
    didcompleteComponent(component) {
        const self = this;
        const index = self.indexOf(component, 0);
        if (index > -1) {
            self.completedComponents.push(self.splice(index, 1)[0]);
            //self.completedComponents.push(component);
        }
    }
    set isHandled(isHandled) {
        this._isHandled = isHandled;
    }
    get isHandled() {
        return (this._isHandled || (this.length == 0 && this.completedComponents.length != 0));
    }
    get linkRepresentation() {
        return "#" + this.stringRepresentation;
    }
    get stringRepresentation() {
        var result = "";
        this.completedComponents.forEach(function (component, index, self) {
            result = result + component.name;
            const parameters = component.parameters;
            result = result + "[";
            Object.keys(parameters).forEach(function (key, index, keys) {
                if (index) {
                    result = result + ",";
                }
                result = result + encodeURIComponent(key) + ":" + encodeURIComponent(parameters[key]);
            });
            result = result + "]";
        });
        this.forEach(function (component, index, self) {
            result = result + component.name;
            const parameters = component.parameters;
            result = result + "[";
            Object.keys(parameters).forEach(function (key, index, keys) {
                if (index) {
                    result = result + ",";
                }
                result = result + encodeURIComponent(key) + ":" + encodeURIComponent(parameters[key]);
            });
            result = result + "]";
        });
        return result;
    }
}
/// <reference path="UIView.ts" />
/// <reference path="UIViewController.ts" />
/// <reference path="../RootViewController.ts" />
/// <reference path="UIRoute.ts" />
class UICore extends UIObject {
    constructor(rootDivElementID, rootViewControllerClass) {
        super();
        this.rootViewController = nil;
        this._class = UICore;
        this.superclass = UIObject;
        UICore.RootViewControllerClass = rootViewControllerClass;
        UICore.main = this;
        const rootViewElement = document.getElementById(rootDivElementID);
        const rootView = new UIView(rootDivElementID, rootViewElement);
        rootView.pausesPointerEvents = NO; //YES;
        if (UICore.RootViewControllerClass) {
            if (!(UICore.RootViewControllerClass.prototype instanceof UIViewController) ||
                UICore.RootViewControllerClass === UIViewController) {
                console.log("Error, UICore.RootViewControllerClass must be a or a subclass of UIViewController, falling back to UIViewController.");
                UICore.RootViewControllerClass = UIViewController;
            }
            this.rootViewController = new UICore.RootViewControllerClass(rootView);
        }
        else {
            this.rootViewController = new UIViewController(rootView);
        }
        this.rootViewController.viewWillAppear();
        this.rootViewController.viewDidAppear();
        this.rootViewController.view.addTargetForControlEvent(UIView.controlEvent.PointerUpInside, function (sender, event) {
            document.activeElement.blur();
        });
        const windowDidResize = function () {
            // Doing layout two times to prevent page scrollbars from confusing the layout
            this.rootViewController._layoutViewSubviews();
            UIView.layoutViewsIfNeeded();
            this.rootViewController._layoutViewSubviews();
            //UIView.layoutViewsIfNeeded()
            this.rootViewController.view.broadcastEventInSubtree({
                name: UICore.broadcastEventName.WindowDidResize,
                parameters: nil
            });
        };
        window.addEventListener("resize", windowDidResize.bind(this));
        const didScroll = function () {
            //code
            this.rootViewController.view.broadcastEventInSubtree({
                name: UIView.broadcastEventName.PageDidScroll,
                parameters: nil
            });
        }.bind(this);
        window.addEventListener("scroll", didScroll, false);
        const hashDidChange = function () {
            //code
            this.rootViewController.handleRouteRecursively(UIRoute.currentRoute);
            this.rootViewController.view.broadcastEventInSubtree({
                name: UICore.broadcastEventName.RouteDidChange,
                parameters: nil
            });
        }.bind(this);
        window.addEventListener("hashchange", hashDidChange.bind(this), false);
        hashDidChange();
    }
    static loadClass(className) {
        if (window[className]) {
            return;
        }
        document.writeln("<script type='text/javascript' src='dist/UICore/" + className + ".js'></script>");
    }
}
UICore.languageService = nil;
UICore.broadcastEventName = {
    "RouteDidChange": "RouteDidChange",
    "WindowDidResize": "WindowDidResize"
};
UICore.RootViewControllerClass = nil;
const IS_FIREFOX = navigator.userAgent.toLowerCase().indexOf("firefox") > -1;
const IS_SAFARI = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
Array.prototype.indexOf || (Array.prototype.indexOf = function (d, e) {
    var a;
    if (null == this) {
        throw new TypeError("\"this\" is null or not defined");
    }
    const c = Object(this), b = c.length >>> 0;
    if (0 === b) {
        return -1;
    }
    a = +e || 0;
    Infinity === Math.abs(a) && (a = 0);
    if (a >= b) {
        return -1;
    }
    for (a = Math.max(0 <= a ? a : b - Math.abs(a), 0); a < b;) {
        if (a in c && c[a] === d) {
            return a;
        }
        a++;
    }
    return -1;
});
/// <reference path="UIView.ts" />
/// <reference path="UICore.ts" />
class UITextView extends UIView {
    constructor(elementID, textViewType = UITextView.type.paragraph, viewHTMLElement = null) {
        super(elementID, viewHTMLElement, textViewType);
        this._textColor = UITextView.defaultTextColor;
        this._isSingleLine = YES;
        this.textPrefix = "";
        this.textSuffix = "";
        this._notificationAmount = 0;
        this._minFontSize = nil;
        this._maxFontSize = nil;
        this._automaticFontSizeSelection = NO;
        this.changesOften = NO;
        this._class = UITextView;
        this.superclass = UIView;
        this.text = "";
        this.style.overflow = "hidden";
        this.style.textOverflow = "ellipsis";
        this.isSingleLine = YES;
        this.textColor = this.textColor;
        this.userInteractionEnabled = YES;
        if (textViewType == UITextView.type.textArea) {
            this.pausesPointerEvents = YES;
            this.addTargetForControlEvent(UIView.controlEvent.PointerUpInside, function (sender, event) {
                sender.focus();
            });
        }
    }
    static _determinePXAndPTRatios() {
        const o = document.createElement("div");
        o.style.width = "1000pt";
        document.body.appendChild(o);
        UITextView._ptToPx = o.clientWidth / 1000;
        document.body.removeChild(o);
        UITextView._pxToPt = 1 / UITextView._ptToPx;
    }
    get textAlignment() {
        const result = this.style.textAlign;
        return result;
    }
    set textAlignment(textAlignment) {
        this._textAlignment = textAlignment;
        this.style.textAlign = textAlignment;
    }
    get textColor() {
        const result = this._textColor;
        return result;
    }
    set textColor(color) {
        this._textColor = color || UITextView.defaultTextColor;
        this.style.color = this._textColor.stringValue;
    }
    get isSingleLine() {
        return this._isSingleLine;
    }
    set isSingleLine(isSingleLine) {
        this._isSingleLine = isSingleLine;
        if (isSingleLine) {
            this.style.whiteSpace = "pre";
            return;
        }
        this.style.whiteSpace = "pre-wrap";
    }
    get notificationAmount() {
        return this._notificationAmount;
    }
    set notificationAmount(notificationAmount) {
        if (this._notificationAmount == notificationAmount) {
            return;
        }
        this._notificationAmount = notificationAmount;
        this.text = this.text;
        this.setNeedsLayoutUpToRootView();
        this.notificationAmountDidChange(notificationAmount);
    }
    notificationAmountDidChange(notificationAmount) {
    }
    get text() {
        return (this._text || this.viewHTMLElement.innerHTML);
    }
    set text(text) {
        this._text = text;
        var notificationText = "";
        if (this.notificationAmount) {
            notificationText = "<span style=\"color: " + UITextView.notificationTextColor.stringValue + ";\">" +
                (" (" + this.notificationAmount + ")").bold() + "</span>";
        }
        if (this.viewHTMLElement.innerHTML != this.textPrefix + text + this.textSuffix + notificationText) {
            this.viewHTMLElement.innerHTML = this.textPrefix + FIRST(text, "") + this.textSuffix + notificationText;
        }
        this.setNeedsLayout();
    }
    set innerHTML(innerHTML) {
        this.text = innerHTML;
    }
    get innerHTML() {
        return this.viewHTMLElement.innerHTML;
    }
    setText(key, defaultString, parameters) {
        this.setInnerHTML(key, defaultString, parameters);
    }
    get fontSize() {
        const style = window.getComputedStyle(this.viewHTMLElement, null).fontSize;
        const result = (parseFloat(style) * UITextView._pxToPt);
        return result;
    }
    set fontSize(fontSize) {
        this.style.fontSize = "" + fontSize + "pt";
    }
    useAutomaticFontSize(minFontSize = nil, maxFontSize = nil) {
        this._automaticFontSizeSelection = YES;
        this._minFontSize = minFontSize;
        this._maxFontSize = maxFontSize;
        this.setNeedsLayout();
    }
    static automaticallyCalculatedFontSize(bounds, currentSize, currentFontSize, minFontSize, maxFontSize) {
        minFontSize = FIRST(minFontSize, 1);
        maxFontSize = FIRST(maxFontSize, 100000000000);
        const heightMultiplier = bounds.height / (currentSize.height + 1);
        const widthMultiplier = bounds.width / (currentSize.width + 1);
        var multiplier = heightMultiplier;
        if (heightMultiplier > widthMultiplier) {
            multiplier = widthMultiplier;
        }
        const maxFittingFontSize = currentFontSize * multiplier;
        if (maxFittingFontSize > maxFontSize) {
            return maxFontSize;
        }
        if (minFontSize > maxFittingFontSize) {
            return minFontSize;
        }
        return maxFittingFontSize;
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
    }
    willMoveToSuperview(superview) {
        super.willMoveToSuperview(superview);
    }
    layoutSubviews() {
        super.layoutSubviews();
        if (this._automaticFontSizeSelection) {
            this.fontSize = UITextView.automaticallyCalculatedFontSize(new UIRectangle(0, 0, 1 *
                this.viewHTMLElement.offsetHeight, 1 *
                this.viewHTMLElement.offsetWidth), this.intrinsicContentSize(), this.fontSize, this._minFontSize, this._maxFontSize);
        }
    }
    intrinsicContentHeight(constrainingWidth = 0) {
        if (this.changesOften) {
            return super.intrinsicContentHeight(constrainingWidth);
        }
        const keyPath = (this.viewHTMLElement.innerHTML + "_csf_" + this.computedStyle.font).replace(new RegExp("\\.", "g"), "_") + "." +
            ("" + constrainingWidth).replace(new RegExp("\\.", "g"), "_");
        var result = UITextView._intrinsicHeightCache.valueForKeyPath(keyPath);
        if (IS_LIKE_NULL(result)) {
            result = super.intrinsicContentHeight(constrainingWidth);
            UITextView._intrinsicHeightCache.setValueForKeyPath(keyPath, result);
        }
        return result;
    }
    intrinsicContentWidth(constrainingHeight = 0) {
        if (this.changesOften) {
            return super.intrinsicContentWidth(constrainingHeight);
        }
        const keyPath = (this.viewHTMLElement.innerHTML + "_csf_" + this.computedStyle.font).replace(new RegExp("\\.", "g"), "_") + "." +
            ("" + constrainingHeight).replace(new RegExp("\\.", "g"), "_");
        var result = UITextView._intrinsicWidthCache.valueForKeyPath(keyPath);
        if (IS_LIKE_NULL(result)) {
            result = super.intrinsicContentWidth(constrainingHeight);
            UITextView._intrinsicWidthCache.setValueForKeyPath(keyPath, result);
        }
        return result;
    }
    intrinsicContentSize() {
        // This works but is slow
        const result = this.intrinsicContentSizeWithConstraints(nil, nil);
        return result;
    }
}
UITextView.defaultTextColor = UIColor.blackColor;
UITextView.notificationTextColor = UIColor.redColor;
UITextView._intrinsicHeightCache = new UIObject();
UITextView._intrinsicWidthCache = new UIObject();
UITextView.type = {
    "paragraph": "p",
    "header1": "h1",
    "header2": "h2",
    "header3": "h3",
    "header4": "h4",
    "header5": "h5",
    "header6": "h6",
    "textArea": "textarea",
    "textField": "input",
    "span": "span",
    "label": "label"
};
UITextView.textAlignment = {
    "left": "left",
    "center": "center",
    "right": "right",
    "justify": "justify"
};
UITextView._determinePXAndPTRatios();
// /**
//  * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
//  * 
//  * @param {String} text The text to be rendered.
//  * @param {String} font The css font descriptor that text is to be rendered with (e.g. "bold 14px verdana").
//  * 
//  * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
//  */
// function getTextMetrics(text, font) {
//     // re-use canvas object for better performance
//     var canvas = getTextMetrics.canvas || (getTextMetrics.canvas = document.createElement("canvas"));
//     var context = canvas.getContext("2d");
//     context.font = font;
//     var metrics = context.measureText(text);
//     return metrics;
// }
/// <reference path="./UICore/UIViewController.ts" />
///<reference path="UICore/UITextView.ts"/>
class ArticleViewController extends UIViewController {
    constructor(view) {
        // Calling super
        super(view);
        this._retrieveData = GraphQL(`query($id: ID!) {
          newsItem(id: $id) {
            id
            title
            content
            url
            img
          }
        }`);
        // Code for further setup if necessary
    }
    loadIntrospectionVariables() {
        super.loadIntrospectionVariables();
        this.superclass = UIViewController;
    }
    loadSubviews() {
        this.view.backgroundColor = UIColor.whiteColor;
        this.imageView = new UIImageView(this.view.elementID + "ImageView");
        this.imageView.hidden = YES;
        this.view.addSubview(this.imageView);
        this.imageView.fillMode = UIImageView.fillMode.aspectFill;
        this.titleLabel = new UITextView(this.view.elementID + "TitleLabel", UITextView.type.header3);
        this.titleLabel.textAlignment = UITextView.textAlignment.left;
        this.titleLabel.nativeSelectionEnabled = NO;
        this.titleLabel.isSingleLine = NO;
        this.view.addSubview(this.titleLabel);
        this.contentTextView = new UITextView(this.view.elementID + "TitleLabel");
        this.contentTextView.text = "Some content";
        this.view.addSubview(this.contentTextView);
        this.linkButton = new CBLinkButton();
        this.linkButton.text = "Read more";
        this.view.addSubview(this.linkButton);
    }
    handleRoute(route) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            _super("handleRoute").call(this, route);
            const inquiryComponent = route.componentWithViewController(ArticleViewController);
            this.view.subviews.everyElement.hidden = YES;
            this.titleLabel.hidden = NO;
            this.titleLabel.text = "Loading";
            try {
                let data = (yield this._retrieveData({ id: inquiryComponent.parameters.ID })).newsItem;
                this.imageView.imageSource = FIRST(data.img, "images/logo192.png");
                this.titleLabel.text = data.title;
                this.contentTextView.text = data.content;
                this.linkButton.target = data.url;
                this.view.subviews.everyElement.hidden = NO;
            }
            catch (exception) {
                this.titleLabel.text = "Retrieving article data failed.";
            }
            route.didcompleteComponent(inquiryComponent);
        });
    }
    layoutViewsManually() {
        super.layoutViewsManually();
        const padding = RootViewController.paddingLength;
        const labelHeight = padding;
        // View bounds
        var bounds = this.view.bounds;
        this.imageView.frame = bounds.rectangleWithHeightRelativeToWidth(9 / 16);
        this.titleLabel.frame = this.imageView.frame.rectangleWithInsets(padding * 0.5, padding * 0.5, 0, 0)
            .rectangleForNextRow(padding, this.titleLabel.intrinsicContentHeight(this.imageView.frame.width));
        this.contentTextView.frame = this.titleLabel.frame.rectangleForNextRow(padding, this.contentTextView.intrinsicContentHeight(this.titleLabel.frame.width));
        this.linkButton.frame = this.contentTextView.frame.rectangleForNextRow(padding, this.linkButton.intrinsicContentHeight(this.contentTextView.frame.width));
    }
    intrinsicContentHeight(constrainingWidth = 0) {
        const padding = RootViewController.paddingLength;
        const labelHeight = padding * 1.5;
        const result = constrainingWidth * (9 / 16) + padding * 4 +
            this.titleLabel.intrinsicContentHeight(constrainingWidth) +
            this.contentTextView.intrinsicContentHeight(constrainingWidth) +
            this.linkButton.intrinsicContentHeight(constrainingWidth);
        return result;
    }
}
ArticleViewController.routeComponentName = "article";
ArticleViewController.ParameterIdentifierName = { "ID": "ID" };
// https://github.com/f/graphql.js/
// Connect to server
// @ts-ignore
const GraphQL = graphql("https://news-reader.stagnationlab.dev/graphql", {
    method: "POST",
    //debug: true,
    asJSON: true
    // headers: {
    //     // headers
    //     "Access-Token": "some-access-token"
    //     // OR "Access-Token": () => "some-access-token"
    // },
    // fragments: {
    //     // fragments, you don't need to say `fragment name`.
    //     auth: "on User { token }",
    //     error: "on Error { messages }"
    // }
});
/// <reference path="../UICore/UIButton.ts" />
class CBButton extends UIButton {
    constructor(elementID, elementType) {
        super(elementID, elementType);
        this._class = CBButton;
        this.superclass = UIButton;
    }
    initView(elementID, viewHTMLElement, initViewData) {
        super.initView(elementID, viewHTMLElement, initViewData);
        //this.style.borderRadius = "2px"
        this.style.outline = "none";
        this.colors.titleLabel.normal = UIColor.whiteColor;
        this.setBackgroundColorsWithNormalColor(UIColor.blueColor);
        this.colors.titleLabel.selected = UIColor.blueColor;
    }
    setBackgroundColorsWithNormalColor(normalBackgroundColor) {
        this.colors.background.normal = normalBackgroundColor;
        this.colors.background.hovered = UIColor.colorWithRGBA(40, 168, 183); // normalBackgroundColor.colorByMultiplyingRGB(0.85)
        this.colors.background.focused = normalBackgroundColor; // normalBackgroundColor.colorByMultiplyingRGB(0.5)
        this.colors.background.highlighted = UIColor.colorWithRGBA(48, 196, 212); // normalBackgroundColor.colorByMultiplyingRGB(0.7)
        this.colors.background.selected = UIColor.whiteColor;
        this.updateContentForCurrentState();
    }
    updateContentForNormalState() {
        super.updateContentForNormalState();
        this.setBorder(0, 0);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.24)"
    }
    updateContentForHoveredState() {
        super.updateContentForHoveredState();
        this.setBorder(0, 0);
        //this.titleLabel.textColor = UIColor.whiteColor.colorByMultiplyingRGB(0.85);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.18)"
    }
    updateContentForFocusedState() {
        super.updateContentForFocusedState();
        this.setBorder(0, 1, UIColor.blueColor);
        //this.titleLabel.textColor = UIColor.whiteColor.colorByMultiplyingRGB(0.85);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.18)"
    }
    updateContentForHighlightedState() {
        super.updateContentForHighlightedState();
        this.setBorder(0, 0);
        //this.titleLabel.textColor = UIColor.whiteColor.colorByMultiplyingRGB(0.7);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.12)"
    }
    updateContentForCurrentEnabledState() {
        super.updateContentForCurrentEnabledState();
        if (IS_NOT(this.enabled)) {
            this.titleLabel.textColor = new UIColor("#adadad");
            this.backgroundColor = new UIColor("#e5e5e5");
            this.alpha = 1;
        }
    }
}
/// <reference path="./CBButton.ts" />
class CBFlatButton extends CBButton {
    constructor(elementID, elementType) {
        super(elementID, elementType);
        this._class = CBFlatButton;
        this.superclass = CBButton;
    }
    initView(elementID, viewHTMLElement, initViewData) {
        super.initView(elementID, viewHTMLElement, initViewData);
        this.colors = {
            titleLabel: {
                normal: UIColor.blueColor,
                highlighted: UIColor.blueColor,
                selected: UIColor.blueColor
            },
            background: {
                normal: UIColor.transparentColor,
                hovered: new UIColor("#F8F8F8"),
                highlighted: new UIColor("#ebebeb"),
                selected: new UIColor("#ebebeb")
            }
        };
    }
    set titleLabelColor(titleLabelColor) {
        this.colors.titleLabel.normal = titleLabelColor;
        this.colors.titleLabel.highlighted = titleLabelColor;
        this.colors.titleLabel.selected = titleLabelColor;
        this.updateContentForCurrentState();
    }
    get titleLabelColor() {
        const result = this.colors.titleLabel.normal;
        return result;
    }
    updateContentForNormalState() {
        UIButton.prototype.updateContentForNormalState.call(this);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.24)"
    }
    updateContentForHoveredState() {
        UIButton.prototype.updateContentForHoveredState.call(this);
        //this.titleLabel.textColor = UIColor.whiteColor.colorByMultiplyingRGB(0.85);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.18)";
    }
    updateContentForFocusedState() {
        UIButton.prototype.updateContentForFocusedState.call(this);
        //this.titleLabel.textColor = UIColor.whiteColor.colorByMultiplyingRGB(0.85);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.18)"
    }
    updateContentForHighlightedState() {
        UIButton.prototype.updateContentForHighlightedState.call(this);
        //this.titleLabel.textColor = UIColor.whiteColor.colorByMultiplyingRGB(0.7);
        //this.style.boxShadow = "0 2px 2px 0 rgba(0,0,0,0.12)"
    }
}
CBFlatButton.colors = {
    titleLabel: {
        normal: UIColor.blueColor,
        highlighted: UIColor.blueColor,
        selected: UIColor.blueColor
    },
    background: {
        normal: UIColor.transparentColor,
        hovered: new UIColor("#F8F8F8"),
        highlighted: new UIColor("#ebebeb"),
        selected: new UIColor("#ebebeb")
    }
};
/// <reference path="../UICore/UILinkButton.ts" />
class CBLinkButton extends UILinkButton {
    constructor(elementID, elementType) {
        super(elementID, elementType);
        this._class = CBLinkButton;
        this.superclass = UILinkButton;
    }
    initView(elementID, viewHTMLElement, initViewData) {
        super.initView(elementID, viewHTMLElement, initViewData);
        this.button.removeFromSuperview();
        this.button = new CBButton(this.elementID + "Button", initViewData.elementType);
        this.addSubview(this.button);
    }
}
/// <reference path="../UICore/UIView.ts" />
class CellView extends UIButton {
    //titleLabel: UITextView;
    constructor(elementID, titleLabelType = UITextView.type.span) {
        super(elementID, undefined, titleLabelType);
        this._isAButton = NO;
        this.leftInset = 0;
        this.rightInset = 0;
        this._class = CellView;
        this.superclass = UIView;
        // this.titleLabel = new UITextView(this.elementID + "TitleLabel", titleLabelType);
        // this.addSubview(this.titleLabel);
        this.updateForCurrentIsAButtonState();
    }
    set isAButton(isAButton) {
        this._isAButton = isAButton;
        this.updateForCurrentIsAButtonState();
    }
    get isAButton() {
        return this._isAButton;
    }
    updateForCurrentIsAButtonState() {
        if (this._isAButton) {
            this.style.cursor = "pointer";
            this.titleLabel.userInteractionEnabled = NO;
            this.titleLabel.nativeSelectionEnabled = NO;
            this.titleLabel.textAlignment = UITextView.textAlignment.center;
            this.nativeSelectionEnabled = NO;
            this.style.outline = "";
            this.colors = {
                titleLabel: {
                    normal: UIColor.blueColor,
                    highlighted: UIColor.blueColor,
                    selected: UIColor.blueColor
                },
                background: {
                    normal: UIColor.transparentColor,
                    hovered: new UIColor("#F8F8F8"),
                    highlighted: new UIColor("#ebebeb"),
                    selected: new UIColor("#ebebeb")
                }
            };
        }
        else {
            this.style.cursor = "";
            this.titleLabel.userInteractionEnabled = YES;
            this.titleLabel.nativeSelectionEnabled = YES;
            this.titleLabel.textAlignment = UITextView.textAlignment.left;
            this.nativeSelectionEnabled = YES;
            this.style.outline = "none";
            this.colors = {
                titleLabel: {
                    normal: UIColor.blackColor,
                    highlighted: UIColor.blackColor,
                    selected: UIColor.blackColor
                },
                background: {
                    normal: UIColor.transparentColor,
                    highlighted: UIColor.transparentColor,
                    selected: UIColor.transparentColor
                }
            };
        }
        this.updateContentForCurrentState();
    }
    initRightImageViewIfNeeded() {
        if (this._rightImageView) {
            return;
        }
        this._rightImageView = new UIImageView(this.elementID + "RightImageView");
        this._rightImageView.userInteractionEnabled = NO;
    }
    set rightImageSource(imageSource) {
        if (IS(imageSource)) {
            this.initRightImageViewIfNeeded();
            this._rightImageView.imageSource = imageSource;
            this.addSubview(this._rightImageView);
        }
        else {
            this._rightImageView.removeFromSuperview();
        }
    }
    get rightImageSource() {
        var result = nil;
        if (this._rightImageView) {
            result = this._rightImageView.imageSource;
        }
        return result;
    }
    layoutSubviews() {
        super.layoutSubviews();
        const padding = RootViewController.paddingLength;
        const labelHeight = padding;
        const bounds = this.bounds;
        this.titleLabel.centerYInContainer();
        this.titleLabel.style.left = "" + (padding * 0.5 + this.leftInset).integerValue + "px";
        this.titleLabel.style.right = "" + (padding * 0.5 + this.rightInset).integerValue + "px";
        this.titleLabel.style.maxHeight = "100%";
        this.titleLabel.style.overflow = "hidden";
        //this.titleLabel.style.whiteSpace = "nowrap";
        if (this._rightImageView && this._rightImageView.superview == this) {
            // var imageHeight = bounds.height - padding;
            // this._rightImageView.frame = new UIRectangle(bounds.width - imageHeight - padding * 0.5, padding * 0.5, imageHeight, imageHeight);
            this._rightImageView.frame = bounds.rectangleWithInsets(this.leftInset, padding * 0.5 +
                this.rightInset, 0, 0).rectangleWithWidth(24, 1).rectangleWithHeight(24, 0.5);
            this.titleLabel.style.right = "" +
                (padding * 0.5 + this.rightInset + this._rightImageView.frame.width).integerValue + "px";
        }
    }
}
class UIActionIndicator extends UIView {
    constructor(elementID) {
        super(elementID);
        this._size = 50;
        this._class = UIActionIndicator;
        this.superclass = UIView;
        this.indicatorView = new UIView(this.elementID + "IndicatorView");
        this.addSubview(this.indicatorView);
        this.indicatorView.viewHTMLElement.classList.add("LukeHaasLoader");
        this.hidden = YES;
    }
    set size(size) {
        this._size = size;
        this.setNeedsLayoutUpToRootView();
    }
    get size() {
        return this._size;
    }
    set hidden(hidden) {
        super.hidden = hidden;
        if (hidden) {
            this.indicatorView.removeFromSuperview();
        }
        else {
            this.addSubview(this.indicatorView);
        }
    }
    start() {
        this.hidden = NO;
    }
    stop() {
        this.hidden = YES;
    }
    layoutSubviews() {
        super.layoutSubviews();
        const bounds = this.bounds;
        //this.indicatorView.centerInContainer();
        this.indicatorView.style.height = "" + this._size.integerValue + "px";
        this.indicatorView.style.width = "" + this._size.integerValue + "px";
        const minSize = Math.min(this.bounds.height, this.bounds.width);
        this.indicatorView.style.maxHeight = "" + minSize.integerValue + "px";
        this.indicatorView.style.maxWidth = "" + minSize.integerValue + "px";
        const size = Math.min(this._size, minSize);
        this.indicatorView.style.left = "" + ((bounds.width - size) * 0.5 - 11).integerValue + "px";
        this.indicatorView.style.top = "" + ((bounds.height - size) * 0.5 - 11).integerValue + "px";
    }
}
class UIDateTimeInput extends UIView {
    constructor(elementID, type = UIDateTimeInput.type.DateTime) {
        super(elementID, nil, "input");
        this._class = UIDateTimeInput;
        this.superclass = UIView;
        this.viewHTMLElement.setAttribute("type", type);
        this.viewHTMLElement.onchange = (event) => {
            this.sendControlEventForKey(UIDateTimeInput.controlEvent.ValueChange, event);
        };
    }
    get addControlEventTarget() {
        return super.addControlEventTarget;
    }
    get date() {
        const result = new Date(this.viewHTMLElement.value);
        return result;
    }
}
UIDateTimeInput.controlEvent = Object.assign({}, UIView.controlEvent, {
    "ValueChange": "ValueChange"
});
UIDateTimeInput.type = {
    "Date": "date",
    "Time": "time",
    "DateTime": "datetime"
};
UIDateTimeInput.format = {
    "European": "DD-MM-YYYY",
    "ISOComputer": "YYYY-MM-DD",
    "American": "MM/DD/YYYY"
};
class UIImageView extends UIView {
    constructor(elementID, viewHTMLElement = null) {
        super(elementID, viewHTMLElement, "img");
        this._hiddenWhenEmpty = NO;
        this._class = UIImageView;
        this.superclass = UIView;
        //this.actionIndicator = new UIActionIndicator(elementID + "ActionIndicator");
    }
    get viewHTMLElement() {
        return super.viewHTMLElement;
    }
    static dataURL(url, callback) {
        const xhr = new XMLHttpRequest();
        xhr.open("get", url);
        xhr.responseType = "blob";
        xhr.onload = function () {
            const fr = new FileReader();
            fr.onload = function () {
                callback(this.result);
            };
            fr.readAsDataURL(xhr.response); // async call
        };
        xhr.send();
    }
    static dataURLWithMaxSize(URLString, maxSize, completion) {
        const imageView = new UIImageView();
        imageView.imageSource = URLString;
        imageView.viewHTMLElement.onload = function () {
            const originalSize = imageView.intrinsicContentSize();
            var multiplier = maxSize / Math.max(originalSize.height, originalSize.width);
            multiplier = Math.min(1, multiplier);
            const result = imageView.getDataURL((originalSize.height * multiplier).integerValue, (originalSize.width *
                multiplier).integerValue);
            completion(result);
        };
    }
    static dataURLWithSizes(URLString, height, width, completion) {
        const imageView = new UIImageView();
        imageView.imageSource = URLString;
        imageView.viewHTMLElement.onload = function () {
            const result = imageView.getDataURL(height, width);
            completion(result);
        };
    }
    getDataURL(height, width) {
        const img = this.viewHTMLElement;
        // Create an empty canvas element
        const canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        // Copy the image contents to the canvas
        const ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        // Get the data-URL formatted image
        // Firefox supports PNG and JPEG. You could check img.src to
        // guess the original format, but be aware the using "image/jpg"
        // will re-encode the image.
        const dataURL = canvas.toDataURL("image/png");
        return dataURL;
        //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }
    get imageSource() {
        return this.viewHTMLElement.src;
    }
    set imageSource(sourceString) {
        if (IS_NOT(sourceString)) {
            sourceString = "";
        }
        this.viewHTMLElement.src = sourceString;
        if (this.hiddenWhenEmpty) {
            this.hidden = IS_NOT(this.imageSource);
        }
        if (!sourceString || !sourceString.length) {
            //this.actionIndicator.stop();
            this.hidden = YES;
            return;
        }
        else {
            this.hidden = NO;
        }
        // this.superview.addSubview(this.actionIndicator);
        // this.actionIndicator.frame = this.frame;
        // this.actionIndicator.start();
        // this.actionIndicator.backgroundColor = UIColor.redColor
        // @ts-ignore
        this.viewHTMLElement.onload = function (event) {
            this.superview.setNeedsLayout();
            //this.actionIndicator.removeFromSuperview();
        }.bind(this);
    }
    setImageSource(key, defaultString) {
        const languageName = UICore.languageService.currentLanguageKey;
        this.imageSource = UICore.languageService.stringForKey(key, languageName, defaultString, nil);
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
        if (event.name == UIView.broadcastEventName.LanguageChanged || event.name ==
            UIView.broadcastEventName.AddedToViewTree) {
            this._setImageSourceFromKeyIfPossible();
        }
    }
    willMoveToSuperview(superview) {
        super.willMoveToSuperview(superview);
        this._setImageSourceFromKeyIfPossible();
    }
    _setImageSourceFromKeyIfPossible() {
        if (this._sourceKey && this._defaultSource) {
            this.setImageSource(this._sourceKey, this._defaultSource);
        }
    }
    get fillMode() {
        return this._fillMode;
    }
    set fillMode(fillMode) {
        this._fillMode = fillMode;
        this.style.objectFit = fillMode;
    }
    get hiddenWhenEmpty() {
        return this._hiddenWhenEmpty;
    }
    set hiddenWhenEmpty(hiddenWhenEmpty) {
        this._hiddenWhenEmpty = hiddenWhenEmpty;
        if (hiddenWhenEmpty) {
            this.hidden = IS_NOT(this.imageSource);
        }
    }
    didMoveToSuperview(superview) {
        super.didMoveToSuperview(superview);
    }
    layoutSubviews() {
        super.layoutSubviews();
    }
    intrinsicContentSize() {
        const result = new UIRectangle(0, 0, this.viewHTMLElement.naturalHeight, this.viewHTMLElement.naturalWidth);
        return result;
    }
    intrinsicContentSizeWithConstraints(constrainingHeight = 0, constrainingWidth = 0) {
        const heightRatio = constrainingHeight / this.viewHTMLElement.naturalHeight;
        const widthRatio = constrainingWidth / this.viewHTMLElement.naturalWidth;
        const multiplier = Math.max(heightRatio, widthRatio);
        const result = new UIRectangle(0, 0, this.viewHTMLElement.naturalHeight *
            multiplier, this.viewHTMLElement.naturalWidth * multiplier);
        return result;
    }
}
UIImageView.fillMode = {
    "stretchToFill": "fill",
    "aspectFit": "contain",
    "aspectFill": "cover",
    "center": "none",
    "aspectFitIfLarger": "scale-down"
};
class UIKeyValueStringFilter extends UIObject {
    constructor(useSeparateWebWorker = NO) {
        super();
        this._isThreadClosed = NO;
        this._webWorker = UIKeyValueStringFilter._sharedWebWorker;
        this._class = UIKeyValueStringFilter;
        this.superclass = UIObject;
        if (useSeparateWebWorker) {
            this._webWorker = new Worker("compiledScripts//UIKeyValueStringFilterWebWorker.js");
        }
        UIKeyValueStringFilter._instanceNumber = UIKeyValueStringFilter._instanceNumber + 1;
        this._instanceNumber = UIKeyValueStringFilter._instanceNumber;
    }
    get instanceIdentifier() {
        return this._instanceNumber;
    }
    filterData(filteringString, data, excludedData, dataKeyPath, identifier, completion) {
        if (this._isThreadClosed) {
            return;
        }
        const startTime = Date.now();
        const instanceIdentifier = this.instanceIdentifier;
        this._webWorker.onmessage = function (message) {
            if (message.data.instanceIdentifier == instanceIdentifier) {
                console.log("Filtering took " + (Date.now() - startTime) + " ms.");
                completion(message.data.filteredData, message.data.filteredIndexes, message.data.identifier);
            }
        };
        try {
            this._webWorker.postMessage({
                "filteringString": filteringString,
                "data": data,
                "excludedData": excludedData,
                "dataKeyPath": dataKeyPath,
                "identifier": identifier,
                "instanceIdentifier": instanceIdentifier
            });
        }
        catch (exception) {
            completion([], [], identifier);
        }
    }
    closeThread() {
        this._isThreadClosed = YES;
        if (this._webWorker != UIKeyValueStringFilter._sharedWebWorker) {
            this._webWorker.terminate();
        }
    }
}
UIKeyValueStringFilter._sharedWebWorker = new Worker("compiledScripts//UIKeyValueStringFilterWebWorker.js");
UIKeyValueStringFilter._instanceNumber = -1;
class UIKeyValueStringSorter extends UIObject {
    constructor(useSeparateWebWorker = NO) {
        super();
        this._isThreadClosed = NO;
        this._webWorker = UIKeyValueStringSorter._sharedWebWorker;
        this._class = UIKeyValueStringSorter;
        this.superclass = UIObject;
        if (useSeparateWebWorker) {
            this._webWorker = new Worker("compiledScripts//UIKeyValueStringSorterWebWorker.js");
        }
        UIKeyValueStringSorter._instanceNumber = UIKeyValueStringSorter._instanceNumber + 1;
        this._instanceNumber = UIKeyValueStringSorter._instanceNumber;
    }
    get instanceIdentifier() {
        return this._instanceNumber;
    }
    sortData(data, sortingInstructions, identifier, completion) {
        if (this._isThreadClosed) {
            return;
        }
        const startTime = Date.now();
        const instanceIdentifier = this.instanceIdentifier;
        this._webWorker.onmessage = function (message) {
            if (message.data.instanceIdentifier == instanceIdentifier) {
                console.log("Sorting " + data.length + " items took " + (Date.now() - startTime) + " ms.");
                completion(message.data.sortedData, message.data.sortedIndexes, message.data.identifier);
            }
        };
        try {
            this._webWorker.postMessage({
                "data": data,
                "sortingInstructions": sortingInstructions,
                "identifier": identifier,
                "instanceIdentifier": instanceIdentifier
            });
        }
        catch (exception) {
            completion([], [], identifier);
        }
    }
    sortedData(data, sortingInstructions, identifier = MAKE_ID()) {
        const result = new Promise((resolve, reject) => {
            this.sortData(data, sortingInstructions, identifier, (sortedData, sortedIndexes, sortedIdentifier) => {
                if (sortedIdentifier == identifier) {
                    resolve({
                        sortedData: sortedData,
                        sortedIndexes: sortedIndexes,
                        identifier: sortedIdentifier
                    });
                }
            });
        });
        return result;
    }
    closeThread() {
        this._isThreadClosed = YES;
        if (this._webWorker != UIKeyValueStringSorter._sharedWebWorker) {
            this._webWorker.terminate();
        }
    }
}
UIKeyValueStringSorter._sharedWebWorker = new Worker("compiledScripts//UIKeyValueStringSorterWebWorker.js");
UIKeyValueStringSorter._instanceNumber = -1;
UIKeyValueStringSorter.dataType = {
    "string": "string"
};
UIKeyValueStringSorter.direction = {
    "descending": "descending",
    "ascending": "ascending"
};
class UILayoutGrid extends UIObject {
    constructor(frame) {
        super();
        this._subframes = [];
        this._class = UILayoutGrid;
        this.superclass = UIObject;
        this._frame = frame;
    }
    splitXInto(numberOfFrames) {
        if (this._subframes.length == 0) {
            for (var i = 0; i < numberOfFrames; i++) {
                const asd = 1;
            }
        }
    }
}
/// <reference path="./UIView.ts" />
class UINativeScrollView extends UIView {
    constructor(elementID, viewHTMLElement) {
        super(elementID, viewHTMLElement);
        this.animationDuration = 0;
        this._class = UINativeScrollView;
        this.superclass = UIView;
        this.style.cssText = this.style.cssText + "-webkit-overflow-scrolling: touch;";
        this.style.overflow = "auto";
        // this.scrollsX = YES;
        // this.scrollsY = YES;
        this.viewHTMLElement.addEventListener("scroll", function (event) {
            this.didScrollToPosition(new UIPoint(this.viewHTMLElement.scrollLeft, this.viewHTMLElement.scrollTop));
            this.broadcastEventInSubtree({
                name: UIView.broadcastEventName.PageDidScroll,
                parameters: nil
            });
        }.bind(this));
    }
    didScrollToPosition(offsetPosition) {
    }
    get scrollsX() {
        const result = (this.style.overflowX == "scroll");
        return result;
    }
    set scrollsX(scrolls) {
        if (scrolls) {
            this.style.overflowX = "scroll";
        }
        else {
            this.style.overflowX = "hidden";
        }
    }
    get scrollsY() {
        const result = (this.style.overflowY == "scroll");
        return result;
    }
    set scrollsY(scrolls) {
        if (scrolls) {
            this.style.overflowY = "scroll";
        }
        else {
            this.style.overflowY = "hidden";
        }
    }
    get contentOffset() {
        const result = new UIPoint(this.viewHTMLElement.scrollLeft, this.viewHTMLElement.scrollTop);
        return result;
    }
    set contentOffset(offsetPoint) {
        if (this.animationDuration) {
            this.scrollXTo(this.viewHTMLElement, offsetPoint.x, this.animationDuration);
            this.scrollYTo(this.viewHTMLElement, offsetPoint.y, this.animationDuration);
            return;
        }
        this.viewHTMLElement.scrollLeft = offsetPoint.x;
        this.viewHTMLElement.scrollTop = offsetPoint.y;
    }
    scrollToBottom() {
        this.contentOffset = new UIPoint(this.contentOffset.x, this.scrollSize.height - this.frame.height);
    }
    scrollToTop() {
        this.contentOffset = new UIPoint(this.contentOffset.x, 0);
    }
    get isScrolledToBottom() {
        return this.contentOffset.isEqualTo(new UIPoint(this.contentOffset.x, this.scrollSize.height -
            this.frame.height));
    }
    get isScrolledToTop() {
        return this.contentOffset.isEqualTo(new UIPoint(this.contentOffset.x, 0));
    }
    scrollYTo(element, to, duration) {
        duration = duration * 1000;
        const start = element.scrollTop;
        const change = to - start;
        const increment = 10;
        const animateScroll = function (elapsedTime) {
            elapsedTime += increment;
            const position = this.easeInOut(elapsedTime, start, change, duration);
            element.scrollTop = position;
            if (elapsedTime < duration) {
                setTimeout(function () {
                    animateScroll(elapsedTime);
                }, increment);
            }
        }.bind(this);
        animateScroll(0);
    }
    scrollXTo(element, to, duration) {
        duration = duration * 1000;
        const start = element.scrollTop;
        const change = to - start;
        const increment = 10;
        const animateScroll = function (elapsedTime) {
            elapsedTime += increment;
            const position = this.easeInOut(elapsedTime, start, change, duration);
            element.scrollLeft = position;
            if (elapsedTime < duration) {
                setTimeout(function () {
                    animateScroll(elapsedTime);
                }, increment);
            }
        }.bind(this);
        animateScroll(0);
    }
    easeInOut(currentTime, start, change, duration) {
        currentTime /= duration / 2;
        if (currentTime < 1) {
            return change / 2 * currentTime * currentTime + start;
        }
        currentTime -= 1;
        return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
    }
}
/// <reference path="./UIView.ts" />
class UIScrollView extends UIView {
    constructor(elementID, viewHTMLElement) {
        super(elementID, viewHTMLElement);
        this._contentOffset = new UIPoint(0, 0);
        this._contentScale = 1;
        this._scrollEnabled = YES;
        this._class = UIScrollView;
        this.superclass = UIView;
        this.containerView = new UIView(elementID + "ContainerView");
        super.addSubview(this.containerView);
        this.style.overflow = "hidden";
        this.pausesPointerEvents = NO; //YES;
        this.addTargetForControlEvent(UIView.controlEvent.PointerDown, function () {
            this._pointerDown = YES;
        }.bind(this));
        this.addTargetForControlEvent(UIView.controlEvent.PointerUp, function () {
            this._pointerDown = NO;
            this._previousClientPoint = null;
            scrollStopped();
        }.bind(this));
        function scrollStopped() {
            // Handle paging if needed
        }
        this.addTargetForControlEvent(UIView.controlEvent.PointerMove, function (sender, event) {
            if (!(this._pointerDown && this._scrollEnabled && this._enabled)) {
                return;
            }
            const currentClientPoint = new UIPoint(nil, nil);
            if (window.MouseEvent && event instanceof MouseEvent) {
                currentClientPoint.x = event.clientX;
                currentClientPoint.y = event.clientY;
            }
            if (window.TouchEvent && event instanceof TouchEvent) {
                const touchEvent = event;
                if (touchEvent.touches.length != 1) {
                    this._pointerDown = NO;
                    this._previousClientPoint = null;
                    scrollStopped();
                    return;
                }
                currentClientPoint.x = touchEvent.touches[0].clientX;
                currentClientPoint.y = touchEvent.touches[0].clientY;
            }
            if (!this._previousClientPoint) {
                this._previousClientPoint = currentClientPoint;
                return;
            }
            const changePoint = currentClientPoint.copy().subtract(this._previousClientPoint);
            if (this.containerView.bounds.width <= this.bounds.width) {
                changePoint.x = 0;
            }
            if (0 < this.contentOffset.x + changePoint.x) {
                changePoint.x = -this.contentOffset.x;
            }
            if (this.contentOffset.x + changePoint.x < -this.bounds.width) {
                changePoint.x = -this.bounds.width - this.contentOffset.x;
            }
            if (this.containerView.bounds.height <= this.bounds.height) {
                changePoint.y = 0;
            }
            if (0 < this.contentOffset.y + changePoint.y) {
                changePoint.y = -this.contentOffset.y;
            }
            if (this.contentOffset.y + changePoint.y < -this.bounds.height) {
                changePoint.y = -this.bounds.height - this.contentOffset.y;
            }
            this.contentOffset = this.contentOffset.add(changePoint);
            this._previousClientPoint = currentClientPoint;
        }.bind(this));
    }
    invalidateIntrinsicContentFrame() {
        this._intrinsicContentFrame = nil;
    }
    get contentOffset() {
        return this._contentOffset;
    }
    set contentOffset(offset) {
        this._contentOffset = offset;
        this.setNeedsLayout();
    }
    layoutSubviews() {
        super.layoutSubviews();
        // var intrinsicContentFrame = this._intrinsicContentFrame;
        // if (!IS(intrinsicContentFrame)) {
        //     intrinsicContentFrame = this.containerView.intrinsicContentSizeWithConstraints();   
        // }
        // intrinsicContentFrame.offsetByPoint(this.contentOffset);
        // intrinsicContentFrame.height = this.containerView.viewHTMLElement.scrollHeight;
        // intrinsicContentFrame.width = this.containerView.viewHTMLElement.scrollWidth;
        // this.containerView.frame = intrinsicContentFrame;
        this.containerView.frame = this.containerView.bounds.offsetByPoint(this.contentOffset);
    }
    // get _subviews() {
    //     return super.subviews;
    // }
    // set _subviews(subviews: UIView[]) {
    //     super.subviews = subviews;
    // }
    // get subviews() {
    //     return this.containerView.subviews;
    // }
    // set subviews(subviews: UIView[]) {
    //     this.containerView.subviews = subviews;
    //     this.invalidateIntrinsicContentFrame();
    // }
    hasSubview(view) {
        return this.containerView.hasSubview(view);
    }
    addSubview(view) {
        this.containerView.addSubview(view);
        this.invalidateIntrinsicContentFrame();
    }
}
class UISlideScrollerView extends UIView {
    constructor(elementID, viewHTMLElement) {
        super(elementID, viewHTMLElement);
        this._targetIndex = 0;
        this._isAnimating = NO;
        this._isAnimationOngoing = NO;
        this._animationTimer = nil;
        this._slideViews = [];
        this.wrapAround = YES;
        this.animationDuration = 0.35;
        this.animationDelay = 2;
        this._currentPageIndex = 0;
        this._class = UIScrollView;
        this.superclass = UIView;
        this._scrollView = new UIScrollView(elementID + "ScrollView");
        this.addSubview(this._scrollView);
        this._scrollView._scrollEnabled = NO;
        this._scrollView.addTargetForControlEvent(UIView.controlEvent.PointerMove, function (sender, event) {
            if (event instanceof MouseEvent) {
                this._animationTimer.invalidate();
            }
        }.bind(this));
        this._scrollView.addTargetForControlEvent(UIView.controlEvent.PointerLeave, function () {
            if (this._isAnimating && event instanceof MouseEvent) {
                this.startAnimating();
            }
        }.bind(this));
        // Touch events
        this._scrollView.addTargetForControlEvent(UIView.controlEvent.PointerDown, function (sender, event) {
            if (event instanceof TouchEvent) {
                this._animationTimer.invalidate();
            }
        }.bind(this));
        this._scrollView.addTargetForControlEvents([
            UIView.controlEvent.PointerUp, UIView.controlEvent.PointerCancel
        ], function (sender, event) {
            if (event instanceof TouchEvent && this._isAnimating) {
                this.startAnimating();
            }
        }.bind(this));
        // Page indicator
        this.pageIndicatorsView = new UIView(elementID + "PageIndicatorsView");
        this.addSubview(this.pageIndicatorsView);
    }
    buttonForPageIndicatorWithIndex(index) {
        const result = new UIButton(this.viewHTMLElement.id + "PageIndicatorButton" + index);
        result.addTargetForControlEvents([
            UIView.controlEvent.PointerUpInside, UIView.controlEvent.EnterUp
        ], function (sender, event) {
            this.scrollToPageWithIndex(index, YES);
            if (this._isAnimating) {
                this.startAnimating();
            }
        }.bind(this));
        result.addTargetForControlEvent(UIView.controlEvent.PointerMove, function () {
            this._animationTimer.invalidate();
        }.bind(this));
        result.updateContentForNormalState = function () {
            result.backgroundColor = UIColor.blueColor;
            result.titleLabel.textColor = UIColor.whiteColor;
        };
        result.frame = new UIRectangle(nil, nil, 20, 50);
        // result.style.height = "20px";
        // result.style.width = "50px";
        result.style.display = "table-cell";
        result.style.position = "relative";
        // var resultContent = new UIView(result.viewHTMLElement.id + "Content");
        // resultContent.backgroundColor = UIColor.whiteColor;
        // resultContent.centerYInContainer();
        // resultContent.style.height = "10px";
        // resultContent.style.height = "100%";
        // resultContent.style.borderRadius = "5px";
        // result.addSubview(resultContent);
        return result;
    }
    addSlideView(view) {
        this.slideViews.push(view);
        this.updateSlideViews();
    }
    set slideViews(views) {
        this._slideViews = views;
        this.updateSlideViews();
    }
    get slideViews() {
        return this._slideViews;
    }
    get currentPageIndex() {
        const result = this._currentPageIndex;
        return result;
    }
    set currentPageIndex(index) {
        this._currentPageIndex = index;
        this._slideViews[index].willAppear();
        //this._scrollView.contentOffset.x = -this._slideViews[index].frame.min.x; //-this.bounds.width * index;
        //this._scrollView.contentOffset.x = Math.round(this._scrollView.contentOffset.x);
        this._scrollView.contentOffset = this._scrollView.contentOffset.pointWithX(-this._slideViews[index].frame.min.x);
        this.pageIndicatorsView.subviews.forEach(function (button, index, array) {
            button.selected = NO;
        });
        this.pageIndicatorsView.subviews[index].selected = YES;
    }
    scrollToPreviousPage(animated) {
        if (this.slideViews.length == 0) {
            return;
        }
        var targetIndex = this.currentPageIndex;
        if (this.wrapAround) {
            targetIndex = (this.currentPageIndex - 1) % (this.slideViews.length);
        }
        else if (this.currentPageIndex - 1 < this.slideViews.length) {
            targetIndex = this.currentPageIndex - 1;
        }
        else {
            return;
        }
        this.scrollToPageWithIndex(targetIndex, animated);
    }
    scrollToNextPage(animated) {
        if (this.slideViews.length == 0) {
            return;
        }
        var targetIndex = this.currentPageIndex;
        if (this.wrapAround) {
            targetIndex = (this.currentPageIndex + 1) % (this.slideViews.length);
        }
        else if (this.currentPageIndex + 1 < this.slideViews.length) {
            targetIndex = this.currentPageIndex + 1;
        }
        else {
            return;
        }
        this.scrollToPageWithIndex(targetIndex, animated);
    }
    scrollToPageWithIndex(targetIndex, animated = YES) {
        this._targetIndex = targetIndex;
        // this._slideViews[this.currentPageIndex]._shouldLayout = NO;
        // this._slideViews[this._targetIndex]._shouldLayout = YES;
        //this._slideViews[this._targetIndex].hidden = NO;
        this.willScrollToPageWithIndex(targetIndex);
        this._isAnimationOngoing = YES;
        //var previousView = this._slideViews[this.currentPageIndex];
        if (animated) {
            UIView.animateViewOrViewsWithDurationDelayAndFunction(this._scrollView.containerView, this.animationDuration, 0, undefined, function () {
                this.currentPageIndex = targetIndex;
            }.bind(this), function () {
                this.didScrollToPageWithIndex(targetIndex);
                this._isAnimationOngoing = NO;
                //previousView.hidden = YES;
            }.bind(this));
        }
        else {
            this.currentPageIndex = targetIndex;
            this.didScrollToPageWithIndex(targetIndex);
            //previousView.hidden = YES;
        }
    }
    willScrollToPageWithIndex(index) {
        const targetView = this.slideViews[index];
        if (IS(targetView) && targetView.willAppear && targetView.willAppear instanceof Function) {
            targetView.willAppear();
        }
    }
    didScrollToPageWithIndex(index) {
    }
    startAnimating() {
        this._isAnimating = YES;
        this._animationTimer.invalidate();
        this._animationTimer = new UITimer(this.animationDelay + this.animationDuration, YES, function () {
            this.scrollToNextPage(YES);
        }.bind(this));
    }
    stopAnimating() {
        this._isAnimating = NO;
        this._animationTimer.invalidate();
    }
    updateSlideViews() {
        this._scrollView.containerView.subviews.slice().forEach(function (subview, index, array) {
            subview.removeFromSuperview();
        });
        this.pageIndicatorsView.subviews.slice().forEach(function (subview, index, array) {
            subview.removeFromSuperview();
        });
        this._slideViews.forEach(function (view, index, array) {
            this._scrollView.addSubview(view);
            this.pageIndicatorsView.addSubview(this.buttonForPageIndicatorWithIndex(index));
        }.bind(this));
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
        if (event.name == UICore.broadcastEventName.WindowDidResize) {
            this.currentPageIndex = this.currentPageIndex;
        }
    }
    set frame(frame) {
        super.frame = frame;
        this.currentPageIndex = this.currentPageIndex;
    }
    get frame() {
        return super.frame;
    }
    layoutSubviews() {
        super.layoutSubviews();
        if (this.bounds.isEqualTo(this._previousLayoutBounds)) {
            return;
        }
        const bounds = this.bounds;
        this._previousLayoutBounds = bounds;
        this._scrollView.frame = bounds;
        this._scrollView.containerView.frame = bounds.rectangleWithWidth(bounds.width *
            this.slideViews.length).performFunctionWithSelf(function (self) {
            self.offsetByPoint(this._scrollView.contentOffset);
            return self;
        }.bind(this));
        this._slideViews.forEach(function (view, index, array) {
            view.frame = bounds.rectangleWithX((this.bounds.width + 1) * index);
        }.bind(this));
        this.layoutPageIndicators();
    }
    layoutPageIndicators() {
        this.pageIndicatorsView.centerXInContainer();
        this.pageIndicatorsView.style.bottom = "20px";
        this.pageIndicatorsView.style.height = "20px";
        this.pageIndicatorsView.style.display = "table-row";
    }
    removeFromSuperview() {
        super.removeFromSuperview();
        this.stopAnimating();
    }
}
class UIStringFilter extends UIObject {
    constructor(useSeparateWebWorker = NO) {
        super();
        this._isThreadClosed = NO;
        this._webWorker = UIStringFilter._sharedWebWorker;
        this._class = UIStringFilter;
        this.superclass = UIObject;
        if (useSeparateWebWorker) {
            this._webWorker = new Worker("compiledScripts//UIStringFilterWebWorker.js");
        }
        UIStringFilter._instanceNumber = UIStringFilter._instanceNumber + 1;
        this._instanceNumber = UIStringFilter._instanceNumber;
    }
    get instanceIdentifier() {
        return this._instanceNumber;
    }
    filterData(filteringString, data, excludedData, identifier, completion) {
        if (this._isThreadClosed) {
            return;
        }
        //var startTime = Date.now();
        const instanceIdentifier = this.instanceIdentifier;
        this._webWorker.onmessage = function (message) {
            if (message.data.instanceIdentifier == instanceIdentifier) {
                //console.log("Filtering took " + (Date.now() - startTime) + " ms");
                completion(message.data.filteredData, message.data.filteredIndexes, message.data.identifier);
            }
        };
        this._webWorker.postMessage({
            "filteringString": filteringString,
            "data": data,
            "excludedData": excludedData,
            "identifier": identifier,
            "instanceIdentifier": instanceIdentifier
        });
    }
    filteredData(filteringString, data, excludedData = [], identifier = MAKE_ID()) {
        const result = new Promise((resolve, reject) => {
            this.filterData(filteringString, data, excludedData, identifier, (filteredData, filteredIndexes, filteredIdentifier) => {
                if (filteredIdentifier == identifier) {
                    resolve({
                        filteredData: filteredData,
                        filteredIndexes: filteredIndexes,
                        identifier: filteredIdentifier
                    });
                }
            });
        });
        return result;
    }
    closeThread() {
        this._isThreadClosed = YES;
        if (this._webWorker != UIStringFilter._sharedWebWorker) {
            this._webWorker.terminate();
        }
    }
}
UIStringFilter._sharedWebWorker = new Worker("compiledScripts//UIStringFilterWebWorker.js");
UIStringFilter._instanceNumber = -1;
/// <reference path="./UINativeScrollView.ts" />
class UITableView extends UINativeScrollView {
    constructor(elementID) {
        super(elementID);
        this.allRowsHaveEqualHeight = NO;
        this._visibleRows = [];
        this._firstLayoutVisibleRows = [];
        this._rowPositions = [];
        this._highestValidRowPositionIndex = 0;
        this._reusableViews = {};
        this._removedReusableViews = {};
        this._rowIDIndex = 0;
        this.reloadsOnLanguageChange = YES;
        this.sidePadding = 0;
        this._persistedData = [];
        this._needsDrawingOfVisibleRowsBeforeLayout = NO;
        this._isDrawVisibleRowsScheduled = NO;
        this.animationDuration = 0.25;
        this._class = UITableView;
        this.superclass = UINativeScrollView;
        this.scrollsX = NO;
    }
    initView(elementID, viewHTMLElement) {
        super.initView(elementID, viewHTMLElement);
        this._fullHeightView = new UIView();
        this._fullHeightView.hidden = YES;
        this._fullHeightView.userInteractionEnabled = NO;
        this.addSubview(this._fullHeightView);
    }
    loadData() {
        this._persistedData = [];
        this._calculatePositionsUntilIndex(this.numberOfRows() - 1);
        this._needsDrawingOfVisibleRowsBeforeLayout = YES;
        this.setNeedsLayout();
        // this.forEachViewInSubtree(function(view) {
        //     view.setNeedsLayout();
        // })
    }
    reloadData() {
        this._removeVisibleRows();
        this._removeAllReusableRows();
        this._rowPositions = [];
        this._highestValidRowPositionIndex = 0;
        this.loadData();
    }
    highlightChanges(previousData, newData) {
        previousData = previousData.map(function (dataPoint, index, array) {
            return JSON.stringify(dataPoint);
        });
        newData = newData.map(function (dataPoint, index, array) {
            return JSON.stringify(dataPoint);
        });
        const newIndexes = [];
        newData.forEach(function (value, index, array) {
            if (!previousData.contains(value)) {
                newIndexes.push(index);
            }
        });
        newIndexes.forEach(function (index) {
            if (this.isRowWithIndexVisible(index)) {
                this.highlightRowAsNew(this.viewForRowWithIndex(index));
            }
        }.bind(this));
    }
    highlightRowAsNew(row) {
    }
    invalidateSizeOfRowWithIndex(index, animateChange = NO) {
        if (this._rowPositions[index]) {
            this._rowPositions[index].isValid = NO;
        }
        this._highestValidRowPositionIndex = Math.min(this._highestValidRowPositionIndex, index - 1);
        // if (index == 0) {
        //     this._highestValidRowPositionIndex = 0;
        //     this._rowPositions = [];
        // }
        this._needsDrawingOfVisibleRowsBeforeLayout = YES;
        this._shouldAnimateNextLayout = animateChange;
    }
    _calculateAllPositions() {
        this._calculatePositionsUntilIndex(this.numberOfRows() - 1);
    }
    _calculatePositionsUntilIndex(maxIndex) {
        var validPositionObject = this._rowPositions[this._highestValidRowPositionIndex];
        if (!IS(validPositionObject)) {
            validPositionObject = {
                bottomY: 0,
                topY: 0,
                isValid: YES
            };
        }
        var previousBottomY = validPositionObject.bottomY;
        if (!this._rowPositions.length) {
            this._highestValidRowPositionIndex = -1;
        }
        for (var i = this._highestValidRowPositionIndex + 1; i <= maxIndex; i++) {
            var height;
            const rowPositionObject = this._rowPositions[i];
            if (IS((rowPositionObject || nil).isValid)) {
                height = rowPositionObject.bottomY - rowPositionObject.topY;
            }
            else {
                height = this.heightForRowWithIndex(i);
            }
            const positionObject = {
                bottomY: previousBottomY + height,
                topY: previousBottomY,
                isValid: YES
            };
            if (i < this._rowPositions.length) {
                this._rowPositions[i] = positionObject;
            }
            else {
                this._rowPositions.push(positionObject);
            }
            this._highestValidRowPositionIndex = i;
            previousBottomY = previousBottomY + height;
        }
    }
    indexesForVisibleRows(paddingRatio = 0.5) {
        const firstVisibleY = this.contentOffset.y - this.bounds.height * paddingRatio;
        const lastVisibleY = firstVisibleY + this.bounds.height * (1 + paddingRatio);
        const numberOfRows = this.numberOfRows();
        if (this.allRowsHaveEqualHeight) {
            const rowHeight = this.heightForRowWithIndex(0);
            var firstIndex = firstVisibleY / rowHeight;
            var lastIndex = lastVisibleY / rowHeight;
            firstIndex = Math.trunc(firstIndex);
            lastIndex = Math.trunc(lastIndex) + 1;
            firstIndex = Math.max(firstIndex, 0);
            lastIndex = Math.min(lastIndex, numberOfRows - 1);
            var result = [];
            for (var i = firstIndex; i < lastIndex + 1; i++) {
                result.push(i);
            }
            return result;
        }
        var accumulatedHeight = 0;
        var result = [];
        this._calculateAllPositions();
        const rowPositions = this._rowPositions;
        for (var i = 0; i < numberOfRows; i++) {
            const height = rowPositions[i].bottomY - rowPositions[i].topY; // this.heightForRowWithIndex(i)
            accumulatedHeight = accumulatedHeight + height;
            if (accumulatedHeight >= firstVisibleY) {
                result.push(i);
            }
            if (accumulatedHeight >= lastVisibleY) {
                break;
            }
        }
        return result;
    }
    _removeVisibleRows() {
        const visibleRows = [];
        this._visibleRows.forEach(function (row, index, array) {
            this._persistedData[row._UITableViewRowIndex] = this.persistenceDataItemForRowWithIndex(row._UITableViewRowIndex, row);
            row.removeFromSuperview();
            this._removedReusableViews[row._UITableViewReusabilityIdentifier].push(row);
        }, this);
        this._visibleRows = visibleRows;
    }
    _removeAllReusableRows() {
        this._reusableViews.forEach(function (rows) {
            rows.forEach(function (row, index, array) {
                this._persistedData[row._UITableViewRowIndex] = this.persistenceDataItemForRowWithIndex(row._UITableViewRowIndex, row);
                row.removeFromSuperview();
                this._markReusableViewAsUnused(row);
            }.bind(this));
        }.bind(this));
    }
    _markReusableViewAsUnused(row) {
        if (!this._removedReusableViews[row._UITableViewReusabilityIdentifier].contains(row)) {
            this._removedReusableViews[row._UITableViewReusabilityIdentifier].push(row);
        }
    }
    _drawVisibleRows() {
        if (!this.isMemberOfViewTree) {
            return;
        }
        const visibleIndexes = this.indexesForVisibleRows();
        const minIndex = visibleIndexes[0];
        const maxIndex = visibleIndexes[visibleIndexes.length - 1];
        const removedViews = [];
        const visibleRows = [];
        this._visibleRows.forEach(function (row, index, array) {
            if (row._UITableViewRowIndex < minIndex || row._UITableViewRowIndex > maxIndex) {
                //row.removeFromSuperview();
                this._persistedData[row._UITableViewRowIndex] = this.persistenceDataItemForRowWithIndex(row._UITableViewRowIndex, row);
                this._removedReusableViews[row._UITableViewReusabilityIdentifier].push(row);
                removedViews.push(row);
            }
            else {
                visibleRows.push(row);
            }
        }, this);
        this._visibleRows = visibleRows;
        visibleIndexes.forEach(function (rowIndex, index, array) {
            if (this.isRowWithIndexVisible(rowIndex)) {
                return;
            }
            const view = this.viewForRowWithIndex(rowIndex);
            //view._UITableViewRowIndex = rowIndex;
            this._firstLayoutVisibleRows.push(view);
            this._visibleRows.push(view);
            this.addSubview(view);
        }, this);
        for (var i = 0; i < removedViews.length; i++) {
            var view = removedViews[i];
            if (this._visibleRows.indexOf(view) == -1) {
                //this._persistedData[view._UITableViewRowIndex] = this.persistenceDataItemForRowWithIndex(view._UITableViewRowIndex, view);
                view.removeFromSuperview();
                //this._removedReusableViews[view._UITableViewReusabilityIdentifier].push(view);
            }
        }
        //this.setNeedsLayout();
    }
    visibleRowWithIndex(rowIndex) {
        for (var i = 0; i < this._visibleRows.length; i++) {
            const row = this._visibleRows[i];
            if (row._UITableViewRowIndex == rowIndex) {
                return row;
            }
        }
        return nil;
    }
    isRowWithIndexVisible(rowIndex) {
        return IS(this.visibleRowWithIndex(rowIndex));
    }
    reusableViewForIdentifier(identifier, rowIndex) {
        if (!this._removedReusableViews[identifier]) {
            this._removedReusableViews[identifier] = [];
        }
        if (this._removedReusableViews[identifier] && this._removedReusableViews[identifier].length) {
            const view = this._removedReusableViews[identifier].pop();
            view._UITableViewRowIndex = rowIndex;
            Object.assign(view, this._persistedData[rowIndex] || this.defaultRowPersistenceDataItem());
            return view;
        }
        if (!this._reusableViews[identifier]) {
            this._reusableViews[identifier] = [];
        }
        const newView = this.newReusableViewForIdentifier(identifier, this._rowIDIndex);
        this._rowIDIndex = this._rowIDIndex + 1;
        if (this._rowIDIndex > 40) {
            const asd = 1;
        }
        newView._UITableViewReusabilityIdentifier = identifier;
        newView._UITableViewRowIndex = rowIndex;
        Object.assign(newView, this._persistedData[rowIndex] || this.defaultRowPersistenceDataItem());
        this._reusableViews[identifier].push(newView);
        return newView;
    }
    // Functions that should be overridden to draw the correct content START
    newReusableViewForIdentifier(identifier, rowIDIndex) {
        const view = new UIButton(this.elementID + "Row" + rowIDIndex);
        view.stopsPointerEventPropagation = NO;
        view.pausesPointerEvents = NO;
        return view;
    }
    heightForRowWithIndex(index) {
        return 50;
    }
    numberOfRows() {
        return 10000;
    }
    defaultRowPersistenceDataItem() {
    }
    persistenceDataItemForRowWithIndex(rowIndex, row) {
    }
    viewForRowWithIndex(rowIndex) {
        const row = this.reusableViewForIdentifier("Row", rowIndex);
        row.titleLabel.text = "Row " + rowIndex;
        return row;
    }
    // Functions that should be overridden to draw the correct content END
    // Functions that trigger redrawing of the content
    didScrollToPosition(offsetPosition) {
        super.didScrollToPosition(offsetPosition);
        this.forEachViewInSubtree(function (view) {
            view._isPointerValid = NO;
        });
        if (!this._isDrawVisibleRowsScheduled) {
            this._isDrawVisibleRowsScheduled = YES;
            UIView.runFunctionBeforeNextFrame(function () {
                this._calculateAllPositions();
                this._drawVisibleRows();
                this.setNeedsLayout();
                this._isDrawVisibleRowsScheduled = NO;
            }.bind(this));
        }
    }
    wasAddedToViewTree() {
        this.loadData();
    }
    setFrame(rectangle, zIndex, performUncheckedLayout) {
        const frame = this.frame;
        super.setFrame(rectangle, zIndex, performUncheckedLayout);
        if (frame.isEqualTo(rectangle) && !performUncheckedLayout) {
            return;
        }
        this._needsDrawingOfVisibleRowsBeforeLayout = YES;
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
        if (event.name == UIView.broadcastEventName.LanguageChanged && this.reloadsOnLanguageChange) {
            this.reloadData();
        }
    }
    _layoutAllRows(positions = this._rowPositions) {
        const bounds = this.bounds;
        this._visibleRows.forEach(function (row, index, array) {
            const frame = bounds.copy();
            const positionObject = positions[row._UITableViewRowIndex];
            frame.min.y = positionObject.topY;
            frame.max.y = positionObject.bottomY;
            row.frame = frame;
            row.style.width = "" + (bounds.width - this.sidePadding * 2).integerValue + "px";
            row.style.left = "" + this.sidePadding.integerValue + "px";
        }, this);
        this._fullHeightView.frame = bounds.rectangleWithHeight((positions.lastElement ||
            nil).bottomY).rectangleWithWidth(bounds.width * 0.5);
        this._firstLayoutVisibleRows = [];
    }
    _animateLayoutAllRows() {
        UIView.animateViewOrViewsWithDurationDelayAndFunction(this._visibleRows, this.animationDuration, 0, undefined, function () {
            this._layoutAllRows();
        }.bind(this), function () {
            // this._calculateAllPositions()
            // this._layoutAllRows()
        }.bind(this));
    }
    layoutSubviews() {
        const previousPositions = JSON.parse(JSON.stringify(this._rowPositions));
        const previousVisibleRowsLength = this._visibleRows.length;
        if (this._needsDrawingOfVisibleRowsBeforeLayout) {
            //this._calculateAllPositions()
            this._drawVisibleRows();
            this._needsDrawingOfVisibleRowsBeforeLayout = NO;
        }
        super.layoutSubviews();
        if (!this.numberOfRows() || !this.isMemberOfViewTree) {
            return;
        }
        if (this._shouldAnimateNextLayout) {
            // Need to do layout with the previous positions
            this._layoutAllRows(previousPositions);
            if (previousVisibleRowsLength < this._visibleRows.length) {
                UIView.runFunctionBeforeNextFrame(function () {
                    this._animateLayoutAllRows();
                }.bind(this));
            }
            else {
                this._animateLayoutAllRows();
            }
            this._shouldAnimateNextLayout = NO;
        }
        else {
            // if (this._needsDrawingOfVisibleRowsBeforeLayout) {
            //     this._drawVisibleRows();
            //     this._needsDrawingOfVisibleRowsBeforeLayout = NO;
            // }
            this._calculateAllPositions();
            this._layoutAllRows();
        }
    }
    intrinsicContentHeight(constrainingWidth = 0) {
        var result = 10000000000000000000000000;
        if (this._rowPositions.length) {
            result = this._rowPositions[this._rowPositions.length - 1].bottomY;
        }
        return result;
    }
}
/// <reference path="UITextView.ts" />
// @ts-ignore
class UITextField extends UITextView {
    constructor(elementID, viewHTMLElement = null, type = UITextView.type.textField) {
        super(elementID, type, viewHTMLElement);
        this._class = UITextField;
        this.superclass = UITextView;
        this.viewHTMLElement.setAttribute("type", "text");
        this.backgroundColor = UIColor.whiteColor;
        this.addTargetForControlEvent(UIView.controlEvent.PointerUpInside, function (sender, event) {
            sender.focus();
        });
        this.viewHTMLElement.oninput = (event) => {
            this.sendControlEventForKey(UITextField.controlEvent.TextChange, event);
        };
        this.style.webkitUserSelect = "text";
        this.nativeSelectionEnabled = YES;
        this.pausesPointerEvents = NO;
    }
    get addControlEventTarget() {
        return super.addControlEventTarget;
    }
    get viewHTMLElement() {
        return this._viewHTMLElement;
    }
    set text(text) {
        this.viewHTMLElement.value = text;
    }
    get text() {
        return this.viewHTMLElement.value;
    }
    set placeholderText(text) {
        this.viewHTMLElement.placeholder = text;
    }
    get placeholderText() {
        return this.viewHTMLElement.placeholder;
    }
    setPlaceholderText(key, defaultString) {
        this._placeholderTextKey = key;
        this._defaultPlaceholderText = defaultString;
        const languageName = UICore.languageService.currentLanguageKey;
        this.placeholderText = UICore.languageService.stringForKey(key, languageName, defaultString, nil);
    }
    didReceiveBroadcastEvent(event) {
        super.didReceiveBroadcastEvent(event);
        if (event.name == UIView.broadcastEventName.LanguageChanged || event.name ==
            UIView.broadcastEventName.AddedToViewTree) {
            this._setPlaceholderFromKeyIfPossible();
        }
    }
    willMoveToSuperview(superview) {
        super.willMoveToSuperview(superview);
        this._setPlaceholderFromKeyIfPossible();
    }
    _setPlaceholderFromKeyIfPossible() {
        if (this._placeholderTextKey && this._defaultPlaceholderText) {
            this.setPlaceholderText(this._placeholderTextKey, this._defaultPlaceholderText);
        }
    }
    get isSecure() {
        const result = (this.viewHTMLElement.type == "password");
        return result;
    }
    set isSecure(secure) {
        var type = "text";
        if (secure) {
            type = "password";
        }
        this.viewHTMLElement.type = type;
    }
}
UITextField.controlEvent = Object.assign({}, UIView.controlEvent, {
    "TextChange": "TextChange"
});
/// <reference path="UITextField.ts" />
class UITextArea extends UITextField {
    constructor(elementID, viewHTMLElement = null) {
        super(elementID, viewHTMLElement, UITextView.type.textArea);
        this._class = UITextArea;
        this.superclass = UITextField;
        this.viewHTMLElement.removeAttribute("type");
        this.style.overflow = "auto";
        this.style.webkitUserSelect = "text";
        this.pausesPointerEvents = NO;
    }
    get addControlEventTarget() {
        return super.addControlEventTarget;
    }
    // @ts-ignore
    get viewHTMLElement() {
        // @ts-ignore
        return super.viewHTMLElement;
    }
}
class UITimer extends UIObject {
    constructor(interval, repeats, target) {
        super();
        this.interval = interval;
        this.repeats = repeats;
        this.target = target;
        this.isValid = YES;
        this.superclass = UIObject;
        this.schedule();
    }
    schedule() {
        const callback = function () {
            if (this.repeats == NO) {
                this.invalidate();
            }
            this.target();
        }.bind(this);
        this._intervalID = window.setInterval(callback, this.interval * 1000);
    }
    reschedule() {
        this.invalidate();
        this.schedule();
    }
    fire() {
        if (this.repeats == NO) {
            this.invalidate();
        }
        else {
            this.reschedule();
        }
        this.target();
    }
    invalidate() {
        if (this.isValid) {
            clearInterval(this._intervalID);
            this.isValid = NO;
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVGVzdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NjcmlwdHMvVUlDb3JlL1VJT2JqZWN0LnRzIiwiLi4vc2NyaXB0cy9VSUNvcmUvVUlDb2xvci50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJUG9pbnQudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSVJlY3RhbmdsZS50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJVmlldy50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJVmlld0NvbnRyb2xsZXIudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSURpYWxvZ1ZpZXcudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSUJhc2VCdXR0b24udHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSUxpbmsudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSUxpbmtCdXR0b24udHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSUJ1dHRvbi50cyIsIi4uL3NjcmlwdHMvSXRlbVZpZXcudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSUNvcmVFeHRlbnNpb25zLnRzIiwiLi4vc2NyaXB0cy9UYWJsZVZpZXcudHMiLCIuLi9zY3JpcHRzL1Jvb3RWaWV3Q29udHJvbGxlci50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJUm91dGUudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSUNvcmUudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSVRleHRWaWV3LnRzIiwiLi4vc2NyaXB0cy9BcnRpY2xlVmlld0NvbnRyb2xsZXIudHMiLCIuLi9zY3JpcHRzL0dRTC50cyIsIi4uL3NjcmlwdHMvQ3VzdG9tIGNvbXBvbmVudHMvQ0JCdXR0b24udHMiLCIuLi9zY3JpcHRzL0N1c3RvbSBjb21wb25lbnRzL0NCRmxhdEJ1dHRvbi50cyIsIi4uL3NjcmlwdHMvQ3VzdG9tIGNvbXBvbmVudHMvQ0JMaW5rQnV0dG9uLnRzIiwiLi4vc2NyaXB0cy9DdXN0b20gY29tcG9uZW50cy9DZWxsVmlldy50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJQWN0aW9uSW5kaWNhdG9yLnRzIiwiLi4vc2NyaXB0cy9VSUNvcmUvVUlEYXRlVGltZUlucHV0LnRzIiwiLi4vc2NyaXB0cy9VSUNvcmUvVUlJbWFnZVZpZXcudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSUtleVZhbHVlU3RyaW5nRmlsdGVyLnRzIiwiLi4vc2NyaXB0cy9VSUNvcmUvVUlLZXlWYWx1ZVN0cmluZ1NvcnRlci50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJTGF5b3V0R3JpZC50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJTmF0aXZlU2Nyb2xsVmlldy50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJU2Nyb2xsVmlldy50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJU2xpZGVTY3JvbGxlclZpZXcudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSVN0cmluZ0ZpbHRlci50cyIsIi4uL3NjcmlwdHMvVUlDb3JlL1VJVGFibGVWaWV3LnRzIiwiLi4vc2NyaXB0cy9VSUNvcmUvVUlUZXh0RmllbGQudHMiLCIuLi9zY3JpcHRzL1VJQ29yZS9VSVRleHRBcmVhLnRzIiwiLi4vc2NyaXB0cy9VSUNvcmUvVUlUaW1lci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLFNBQVMsV0FBVztJQUNoQixPQUFPLEdBQUcsQ0FBQTtBQUNkLENBQUM7QUFFRCxJQUFJLEdBQUcsR0FBUSxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUU7SUFFdkYsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJO1FBRVosSUFBSSxJQUFJLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRTtZQUU1QixPQUFPLFVBQVUsSUFBSTtnQkFDakIsSUFBSSxJQUFJLElBQUksUUFBUSxFQUFFO29CQUNsQixPQUFPLENBQUMsQ0FBQTtpQkFDWDtnQkFDRCxJQUFJLElBQUksSUFBSSxRQUFRLEVBQUU7b0JBQ2xCLE9BQU8sRUFBRSxDQUFBO2lCQUNaO2dCQUNELE9BQU8sS0FBSyxDQUFBO1lBQ2hCLENBQUMsQ0FBQTtTQUVKO1FBRUQsSUFBSSxJQUFJLElBQUksVUFBVSxFQUFFO1lBRXBCLE9BQU8sU0FBUyxRQUFRO2dCQUNwQixPQUFPLEVBQUUsQ0FBQTtZQUNiLENBQUMsQ0FBQTtTQUVKO1FBRUQsT0FBTyxXQUFXLEVBQUUsQ0FBQTtJQUN4QixDQUFDO0lBRUQsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSztRQUNuQixPQUFPLFdBQVcsRUFBRSxDQUFBO0lBQ3hCLENBQUM7Q0FFSixDQUFDLENBQUE7QUFHRixTQUFTLFNBQVMsQ0FBSSxNQUFVO0lBRzVCLElBQUksTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQTtJQUVqQyxJQUFJLE1BQU0sWUFBWSxNQUFNLElBQUksQ0FBQyxDQUFDLE1BQU0sWUFBWSxRQUFRLENBQUMsRUFBRTtRQUUzRCxNQUFNLEdBQUcsSUFBSSxLQUFLLENBQUMsTUFBb0IsRUFBRTtZQUVyQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUk7Z0JBRVosSUFBSSxJQUFJLElBQUksb0JBQW9CLEVBQUU7b0JBRTlCLE9BQU8sTUFBTSxDQUFBO2lCQUVoQjtnQkFHRCxNQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQTtnQkFFdkMsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7b0JBRTNCLE9BQU8sU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFBO2lCQUUxQjtnQkFFRCxJQUFJLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUV6QixPQUFPLEtBQUssQ0FBQTtpQkFFZjtnQkFFRCxPQUFPLEdBQUcsQ0FBQTtZQUVkLENBQUM7U0FFSixDQUFDLENBQUE7S0FFTDtJQUVELE9BQU8sTUFBTSxDQUFBO0FBRWpCLENBQUM7QUFHRCxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUE7QUFDaEIsTUFBTSxFQUFFLEdBQUcsS0FBSyxDQUFBO0FBRWhCLFNBQVMsRUFBRSxDQUFDLE1BQU07SUFFZCxJQUFJLE1BQU0sSUFBSSxNQUFNLEtBQUssR0FBRyxFQUFFO1FBRTFCLE9BQU8sR0FBRyxDQUFBO0tBRWI7SUFFRCxPQUFPLEVBQUUsQ0FBQTtJQUVULG1DQUFtQztBQUV2QyxDQUFDO0FBRUQsU0FBUyxNQUFNLENBQUMsTUFBTTtJQUVsQixPQUFPLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBRXRCLENBQUM7QUFFRCxTQUFTLFVBQVUsQ0FBQyxNQUFNO0lBRXRCLElBQUksTUFBTSxJQUFJLFNBQVMsRUFBRTtRQUVyQixPQUFPLEdBQUcsQ0FBQTtLQUViO0lBRUQsT0FBTyxFQUFFLENBQUE7QUFFYixDQUFDO0FBRUQsU0FBUyxZQUFZLENBQUMsTUFBTTtJQUV4QixPQUFPLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBRTlCLENBQUM7QUFFRCxTQUFTLE1BQU0sQ0FBQyxNQUFNO0lBRWxCLElBQUksTUFBTSxLQUFLLEdBQUcsRUFBRTtRQUVoQixPQUFPLEdBQUcsQ0FBQTtLQUViO0lBRUQsT0FBTyxFQUFFLENBQUE7QUFFYixDQUFDO0FBRUQsU0FBUyxVQUFVLENBQUMsTUFBTTtJQUV0QixPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBRTFCLENBQUM7QUFHRCxTQUFTLFlBQVksQ0FBQyxNQUFNO0lBRXhCLE9BQU8sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQTtBQUVyRSxDQUFDO0FBRUQsU0FBUyxnQkFBZ0IsQ0FBQyxNQUFNO0lBRTVCLE9BQU8sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUE7QUFFaEMsQ0FBQztBQUdELFNBQVMsbUJBQW1CLENBQUMsS0FBYTtJQUN0QyxNQUFNLEVBQUUsR0FBRyxjQUFjLENBQUE7SUFDekIsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ3pCLENBQUM7QUFHRCxTQUFTLFlBQVksQ0FBSSxHQUFHLE9BQVk7SUFFcEMsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsS0FBSztRQUV0RCxPQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQTtJQUVyQixDQUFDLENBQUMsQ0FBQTtJQUVGLE9BQU8sTUFBTSxJQUFJLEdBQUcsQ0FBQTtBQUV4QixDQUFDO0FBRUQsU0FBUyxLQUFLLENBQUksR0FBRyxPQUFZO0lBRTdCLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLEtBQUs7UUFFdEQsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUE7SUFFckIsQ0FBQyxDQUFDLENBQUE7SUFFRixPQUFPLE1BQU0sSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFBO0FBRXpGLENBQUM7QUFHRCxTQUFTLE9BQU8sQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFO0lBRWxDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQTtJQUNmLE1BQU0sVUFBVSxHQUFHLGdFQUFnRSxDQUFBO0lBRW5GLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxnQkFBZ0IsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUV2QyxNQUFNLEdBQUcsTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUE7S0FFckY7SUFFRCxNQUFNLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQTtJQUU1QixPQUFPLE1BQU0sQ0FBQTtBQUVqQixDQUFDO0FBR0QsU0FBUyxRQUFRLENBQUksS0FBUTtJQUV6QixPQUFPLFVBQVUsR0FBRyxPQUFjO1FBRTlCLE9BQU8sS0FBSyxDQUFBO0lBRWhCLENBQUMsQ0FBQTtBQUVMLENBQUM7QUFtQkQsU0FBUyxFQUFFLENBQVUsS0FBVTtJQUUzQixJQUFJLFlBQVksR0FBRyxHQUFHLENBQUE7SUFDdEIsSUFBSSxZQUFZLEdBQUcsR0FBRyxDQUFBO0lBRXRCLE1BQU0sTUFBTSxHQUFRLFVBQVUsY0FBdUI7UUFDakQsWUFBWSxHQUFHLGNBQWMsQ0FBQTtRQUM3QixPQUFPLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQTtJQUNwQyxDQUFDLENBQUE7SUFHRCxNQUFNLENBQUMsa0JBQWtCLEdBQUc7UUFDeEIsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDWCxPQUFPLFlBQVksRUFBRSxDQUFBO1NBQ3hCO1FBQ0QsT0FBTyxZQUFZLEVBQUUsQ0FBQTtJQUN6QixDQUFDLENBQUE7SUFHRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsT0FBTyxHQUFHLFVBQVUsVUFBZTtRQUV6RCxNQUFNLGNBQWMsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFzRSxDQUFBO1FBQzFHLFlBQVksR0FBRyxjQUFjLENBQUMsa0JBQWtCLENBQUE7UUFFaEQsTUFBTSx3Q0FBd0MsR0FBUTtZQUNsRCxPQUFPLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFBO1FBQ3RDLENBQUMsQ0FBQTtRQUNELHdDQUF3QyxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFBO1FBQzVGLHdDQUF3QyxDQUFDLElBQUksR0FBRyxjQUFjLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFBO1FBRXRGLGNBQWMsQ0FBQyxrQkFBa0IsR0FBRyx3Q0FBd0MsQ0FBQTtRQUU1RSxPQUFPLGNBQWMsQ0FBQTtJQUV6QixDQUFDLENBQUE7SUFHRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsSUFBSSxHQUFHLFVBQVUsY0FBdUI7UUFDOUQsWUFBWSxHQUFHLGNBQWMsQ0FBQTtRQUM3QixPQUFPLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFBO0lBQ3RDLENBQUMsQ0FBQTtJQUdELE9BQU8sTUFBTSxDQUFBO0FBQ2pCLENBQUM7QUFJRCxhQUFhO0FBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUU7SUFFcEIsYUFBYTtJQUNiLE1BQU0sQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFBO0NBRTFCO0FBR0QsTUFBTSxRQUFRO0lBSVY7UUFFSSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQTtRQUN0QixJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUE7SUFHL0IsQ0FBQztJQUVELElBQVcsS0FBSztRQUNaLE9BQVEsSUFBSSxDQUFDLFdBQW1CLENBQUE7SUFDcEMsQ0FBQztJQUdELElBQVcsVUFBVTtRQUVqQixPQUFRLElBQUksQ0FBQyxXQUFtQixDQUFDLFVBQVUsQ0FBQTtJQUUvQyxDQUFDO0lBRUQsSUFBVyxVQUFVLENBQUMsVUFBZTtRQUNoQyxJQUFJLENBQUMsV0FBbUIsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO0lBQ3JELENBQUM7SUFNTSxNQUFNLENBQUMsVUFBVSxDQUFJLE1BQVM7UUFFakMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDaEIsT0FBTyxHQUFHLENBQUE7U0FDYjtRQUVELElBQUksTUFBTSxZQUFZLFFBQVEsRUFBRTtZQUM1QixPQUFPLE1BQU0sQ0FBQTtTQUNoQjtRQUVELE1BQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxRQUFRLEVBQUUsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUVwRCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBR0QsYUFBYSxDQUFDLFdBQVc7UUFDckIsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ25DLE9BQU8sR0FBRyxDQUFBO1NBQ2I7UUFDRCxLQUFLLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUU7WUFDL0csSUFBSSxnQkFBZ0IsSUFBSSxXQUFXLEVBQUU7Z0JBQ2pDLE9BQU8sR0FBRyxDQUFBO2FBQ2I7U0FDSjtRQUNELE9BQU8sRUFBRSxDQUFBO0lBQ2IsQ0FBQztJQUdELGVBQWUsQ0FBQyxXQUFnQjtRQUM1QixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxXQUFXLENBQUMsQ0FBQTtJQUN0QyxDQUFDO0lBSUQsV0FBVyxDQUFDLEdBQVc7UUFDbkIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7SUFDcEIsQ0FBQztJQUVELGVBQWUsQ0FBQyxPQUFlO1FBQzNCLE9BQU8sUUFBUSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUE7SUFDbEQsQ0FBQztJQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsT0FBZSxFQUFFLE1BQVc7UUFFL0MsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFFakIsT0FBTyxNQUFNLENBQUM7U0FFakI7UUFFRCxNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQy9CLElBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQTtRQUUxQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUVsQyxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFFbkIsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUU7Z0JBRTdCLHFHQUFxRztnQkFFckcsYUFBYSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBRS9DLGdDQUFnQztnQkFFaEMsTUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7Z0JBRXBELE1BQU0sWUFBWSxHQUFHLGFBQWlDLENBQUE7Z0JBRXRELGFBQWEsR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQVUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLO29CQUU5RCxNQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDLGdCQUFnQixFQUFFLFNBQVMsQ0FBQyxDQUFBO29CQUVwRSxPQUFPLE1BQU0sQ0FBQTtnQkFFakIsQ0FBQyxDQUFDLENBQUE7Z0JBRUYsTUFBSzthQUVSO1lBRUQsYUFBYSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNsQyxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBRTtnQkFDdkIsYUFBYSxHQUFHLEdBQUcsQ0FBQTthQUN0QjtTQUVKO1FBRUQsT0FBTyxhQUFhLENBQUE7SUFFeEIsQ0FBQztJQUVELGtCQUFrQixDQUFDLE9BQWUsRUFBRSxLQUFVLEVBQUUsVUFBVSxHQUFHLEdBQUc7UUFFNUQsT0FBTyxRQUFRLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUE7SUFFeEUsQ0FBQztJQUVELE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxPQUFlLEVBQUUsS0FBVSxFQUFFLGFBQWtCLEVBQUUsVUFBVTtRQUVqRixNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQy9CLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQTtRQUVwQixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLO1lBQ3BDLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxFQUFFO2dCQUM5RCxhQUFhLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFBO2dCQUMxQixXQUFXLEdBQUcsR0FBRyxDQUFBO2dCQUNqQixPQUFNO2FBQ1Q7aUJBQ0ksSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0JBQzVCLE9BQU07YUFDVDtZQUVELE1BQU0sa0JBQWtCLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1lBQzdDLElBQUksWUFBWSxDQUFDLGtCQUFrQixDQUFDLElBQUksVUFBVSxFQUFFO2dCQUNoRCxhQUFhLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFBO2FBQzFCO1lBQ0QsYUFBYSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUN0QyxDQUFDLENBQUMsQ0FBQTtRQUVGLE9BQU8sV0FBVyxDQUFBO0lBRXRCLENBQUM7SUFNRCx1QkFBdUIsQ0FBQyxpQkFBc0M7UUFFMUQsT0FBTyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUVsQyxDQUFDO0lBRUQsd0JBQXdCLENBQUMsS0FBYSxFQUFFLGNBQXdCO1FBSTVELElBQUksT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLEVBQUUsY0FBYyxDQUFDLENBQUE7SUFJMUMsQ0FBQztDQU1KO0FDM2NELE1BQU0sT0FBUSxTQUFRLFFBQVE7SUFHMUIsWUFBbUIsV0FBbUI7UUFFbEMsS0FBSyxFQUFFLENBQUE7UUFGUSxnQkFBVyxHQUFYLFdBQVcsQ0FBUTtRQUlsQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQTtRQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtJQUk5QixDQUFDO0lBSUQsUUFBUTtRQUNKLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQTtJQUMzQixDQUFDO0lBRUQsTUFBTSxLQUFLLFFBQVE7UUFDZixPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO0lBQzdCLENBQUM7SUFFRCxNQUFNLEtBQUssU0FBUztRQUNoQixPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0lBQzlCLENBQUM7SUFFRCxNQUFNLEtBQUssVUFBVTtRQUNqQixPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQy9CLENBQUM7SUFFRCxNQUFNLEtBQUssV0FBVztRQUNsQixPQUFPLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBQ2hDLENBQUM7SUFFRCxNQUFNLEtBQUssVUFBVTtRQUNqQixPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQy9CLENBQUM7SUFFRCxNQUFNLEtBQUssVUFBVTtRQUNqQixPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQy9CLENBQUM7SUFFRCxNQUFNLEtBQUssVUFBVTtRQUNqQixPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQy9CLENBQUM7SUFFRCxNQUFNLEtBQUssU0FBUztRQUNoQixPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0lBQzlCLENBQUM7SUFFRCxNQUFNLEtBQUssY0FBYztRQUNyQixPQUFPLElBQUksT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0lBQ25DLENBQUM7SUFFRCxNQUFNLEtBQUssZ0JBQWdCO1FBQ3ZCLE9BQU8sSUFBSSxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUE7SUFDckMsQ0FBQztJQUVELE1BQU0sS0FBSyxjQUFjO1FBQ3JCLE9BQU8sSUFBSSxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUE7SUFDMUIsQ0FBQztJQUVELE1BQU0sS0FBSyxRQUFRO1FBQ2YsT0FBTyxJQUFJLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUMxQixDQUFDO0lBSUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFZO1FBQ3pCLE9BQU87WUFDSCxXQUFXLEVBQUUsU0FBUztZQUN0QixjQUFjLEVBQUUsU0FBUztZQUN6QixNQUFNLEVBQUUsU0FBUztZQUNqQixZQUFZLEVBQUUsU0FBUztZQUN2QixPQUFPLEVBQUUsU0FBUztZQUNsQixPQUFPLEVBQUUsU0FBUztZQUNsQixRQUFRLEVBQUUsU0FBUztZQUNuQixPQUFPLEVBQUUsU0FBUztZQUNsQixnQkFBZ0IsRUFBRSxTQUFTO1lBQzNCLE1BQU0sRUFBRSxTQUFTO1lBQ2pCLFlBQVksRUFBRSxTQUFTO1lBQ3ZCLE9BQU8sRUFBRSxTQUFTO1lBQ2xCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLFlBQVksRUFBRSxTQUFTO1lBQ3ZCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLE9BQU8sRUFBRSxTQUFTO1lBQ2xCLGdCQUFnQixFQUFFLFNBQVM7WUFDM0IsVUFBVSxFQUFFLFNBQVM7WUFDckIsU0FBUyxFQUFFLFNBQVM7WUFDcEIsTUFBTSxFQUFFLFNBQVM7WUFDakIsVUFBVSxFQUFFLFNBQVM7WUFDckIsVUFBVSxFQUFFLFNBQVM7WUFDckIsZUFBZSxFQUFFLFNBQVM7WUFDMUIsVUFBVSxFQUFFLFNBQVM7WUFDckIsV0FBVyxFQUFFLFNBQVM7WUFDdEIsV0FBVyxFQUFFLFNBQVM7WUFDdEIsYUFBYSxFQUFFLFNBQVM7WUFDeEIsZ0JBQWdCLEVBQUUsU0FBUztZQUMzQixZQUFZLEVBQUUsU0FBUztZQUN2QixZQUFZLEVBQUUsU0FBUztZQUN2QixTQUFTLEVBQUUsU0FBUztZQUNwQixZQUFZLEVBQUUsU0FBUztZQUN2QixjQUFjLEVBQUUsU0FBUztZQUN6QixlQUFlLEVBQUUsU0FBUztZQUMxQixlQUFlLEVBQUUsU0FBUztZQUMxQixlQUFlLEVBQUUsU0FBUztZQUMxQixZQUFZLEVBQUUsU0FBUztZQUN2QixVQUFVLEVBQUUsU0FBUztZQUNyQixhQUFhLEVBQUUsU0FBUztZQUN4QixTQUFTLEVBQUUsU0FBUztZQUNwQixZQUFZLEVBQUUsU0FBUztZQUN2QixXQUFXLEVBQUUsU0FBUztZQUN0QixhQUFhLEVBQUUsU0FBUztZQUN4QixhQUFhLEVBQUUsU0FBUztZQUN4QixTQUFTLEVBQUUsU0FBUztZQUNwQixXQUFXLEVBQUUsU0FBUztZQUN0QixZQUFZLEVBQUUsU0FBUztZQUN2QixNQUFNLEVBQUUsU0FBUztZQUNqQixXQUFXLEVBQUUsU0FBUztZQUN0QixNQUFNLEVBQUUsU0FBUztZQUNqQixPQUFPLEVBQUUsU0FBUztZQUNsQixhQUFhLEVBQUUsU0FBUztZQUN4QixVQUFVLEVBQUUsU0FBUztZQUNyQixTQUFTLEVBQUUsU0FBUztZQUNwQixZQUFZLEVBQUUsU0FBUztZQUN2QixRQUFRLEVBQUUsU0FBUztZQUNuQixPQUFPLEVBQUUsU0FBUztZQUNsQixPQUFPLEVBQUUsU0FBUztZQUNsQixVQUFVLEVBQUUsU0FBUztZQUNyQixlQUFlLEVBQUUsU0FBUztZQUMxQixXQUFXLEVBQUUsU0FBUztZQUN0QixjQUFjLEVBQUUsU0FBUztZQUN6QixXQUFXLEVBQUUsU0FBUztZQUN0QixZQUFZLEVBQUUsU0FBUztZQUN2QixXQUFXLEVBQUUsU0FBUztZQUN0QixzQkFBc0IsRUFBRSxTQUFTO1lBQ2pDLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLFlBQVksRUFBRSxTQUFTO1lBQ3ZCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLGFBQWEsRUFBRSxTQUFTO1lBQ3hCLGVBQWUsRUFBRSxTQUFTO1lBQzFCLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLGdCQUFnQixFQUFFLFNBQVM7WUFDM0IsZ0JBQWdCLEVBQUUsU0FBUztZQUMzQixhQUFhLEVBQUUsU0FBUztZQUN4QixNQUFNLEVBQUUsU0FBUztZQUNqQixXQUFXLEVBQUUsU0FBUztZQUN0QixPQUFPLEVBQUUsU0FBUztZQUNsQixTQUFTLEVBQUUsU0FBUztZQUNwQixRQUFRLEVBQUUsU0FBUztZQUNuQixrQkFBa0IsRUFBRSxTQUFTO1lBQzdCLFlBQVksRUFBRSxTQUFTO1lBQ3ZCLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLGdCQUFnQixFQUFFLFNBQVM7WUFDM0IsaUJBQWlCLEVBQUUsU0FBUztZQUM1QixtQkFBbUIsRUFBRSxTQUFTO1lBQzlCLGlCQUFpQixFQUFFLFNBQVM7WUFDNUIsaUJBQWlCLEVBQUUsU0FBUztZQUM1QixjQUFjLEVBQUUsU0FBUztZQUN6QixXQUFXLEVBQUUsU0FBUztZQUN0QixXQUFXLEVBQUUsU0FBUztZQUN0QixVQUFVLEVBQUUsU0FBUztZQUNyQixhQUFhLEVBQUUsU0FBUztZQUN4QixNQUFNLEVBQUUsU0FBUztZQUNqQixTQUFTLEVBQUUsU0FBUztZQUNwQixPQUFPLEVBQUUsU0FBUztZQUNsQixXQUFXLEVBQUUsU0FBUztZQUN0QixRQUFRLEVBQUUsU0FBUztZQUNuQixXQUFXLEVBQUUsU0FBUztZQUN0QixRQUFRLEVBQUUsU0FBUztZQUNuQixlQUFlLEVBQUUsU0FBUztZQUMxQixXQUFXLEVBQUUsU0FBUztZQUN0QixlQUFlLEVBQUUsU0FBUztZQUMxQixlQUFlLEVBQUUsU0FBUztZQUMxQixZQUFZLEVBQUUsU0FBUztZQUN2QixXQUFXLEVBQUUsU0FBUztZQUN0QixNQUFNLEVBQUUsU0FBUztZQUNqQixNQUFNLEVBQUUsU0FBUztZQUNqQixNQUFNLEVBQUUsU0FBUztZQUNqQixZQUFZLEVBQUUsU0FBUztZQUN2QixRQUFRLEVBQUUsU0FBUztZQUNuQixLQUFLLEVBQUUsU0FBUztZQUNoQixXQUFXLEVBQUUsU0FBUztZQUN0QixXQUFXLEVBQUUsU0FBUztZQUN0QixhQUFhLEVBQUUsU0FBUztZQUN4QixRQUFRLEVBQUUsU0FBUztZQUNuQixZQUFZLEVBQUUsU0FBUztZQUN2QixVQUFVLEVBQUUsU0FBUztZQUNyQixVQUFVLEVBQUUsU0FBUztZQUNyQixRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsU0FBUztZQUNuQixTQUFTLEVBQUUsU0FBUztZQUNwQixXQUFXLEVBQUUsU0FBUztZQUN0QixXQUFXLEVBQUUsU0FBUztZQUN0QixNQUFNLEVBQUUsU0FBUztZQUNqQixhQUFhLEVBQUUsU0FBUztZQUN4QixXQUFXLEVBQUUsU0FBUztZQUN0QixLQUFLLEVBQUUsU0FBUztZQUNoQixNQUFNLEVBQUUsU0FBUztZQUNqQixTQUFTLEVBQUUsU0FBUztZQUNwQixRQUFRLEVBQUUsU0FBUztZQUNuQixXQUFXLEVBQUUsU0FBUztZQUN0QixRQUFRLEVBQUUsU0FBUztZQUNuQixPQUFPLEVBQUUsU0FBUztZQUNsQixPQUFPLEVBQUUsU0FBUztZQUNsQixZQUFZLEVBQUUsU0FBUztZQUN2QixRQUFRLEVBQUUsU0FBUztZQUNuQixhQUFhLEVBQUUsU0FBUztTQUMzQixDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFBO0lBQ3pCLENBQUM7SUFJRCxNQUFNLENBQUMsZUFBZSxDQUFDLENBQVM7UUFDNUIsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQ2QsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDbEI7UUFDRCxNQUFNLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUE7UUFDckMsTUFBTSxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBQ3JDLE1BQU0sQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQTtRQUNyQyxNQUFNLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUE7UUFFckMsTUFBTSxNQUFNLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUE7UUFFOUQsT0FBTyxNQUFNLENBQUE7UUFFYiw4Q0FBOEM7SUFFbEQsQ0FBQztJQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsV0FBbUI7UUFHdEMsSUFBSSxXQUFXLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBRWpDLFdBQVcsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFBO1NBRTdEO1FBRUQsSUFBSSxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBRWhDLFdBQVcsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQTtTQUVyRTtRQUdELE1BQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUE7UUFJekMsTUFBTSxNQUFNLEdBQUc7WUFDWCxLQUFLLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1QixPQUFPLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5QixNQUFNLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixPQUFPLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNqQyxDQUFBO1FBR0QsT0FBTyxNQUFNLENBQUE7SUFHakIsQ0FBQztJQUlELElBQUksZUFBZTtRQUVmLElBQUksVUFBVSxDQUFBO1FBRWQsTUFBTSxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUU1RCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBRXBDLFVBQVUsR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtTQUV6RDthQUNJLElBQUksZ0JBQWdCLEVBQUU7WUFFdkIsVUFBVSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtTQUV6RDthQUNJO1lBRUQsVUFBVSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1NBRXpEO1FBRUQsT0FBTyxVQUFVLENBQUE7SUFFckIsQ0FBQztJQUlELFlBQVksQ0FBQyxHQUFXO1FBR3BCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7UUFFdkMsTUFBTSxNQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHO1lBQzNGLFVBQVUsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUE7UUFFM0IsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELGNBQWMsQ0FBQyxLQUFhO1FBR3hCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7UUFFdkMsTUFBTSxNQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEtBQUssR0FBRyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHO1lBQzNGLFVBQVUsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUE7UUFFM0IsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFZO1FBR3RCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7UUFFdkMsTUFBTSxNQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLElBQUksR0FBRyxHQUFHO1lBQzNGLFVBQVUsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUE7UUFFM0IsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUdELGNBQWMsQ0FBQyxLQUFhO1FBR3hCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7UUFFdkMsTUFBTSxNQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxJQUFJLEdBQUcsR0FBRztZQUN0RyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUE7UUFFaEIsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUlELE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBVyxFQUFFLEtBQWEsRUFBRSxJQUFZLEVBQUUsUUFBZ0IsQ0FBQztRQUc1RSxNQUFNLE1BQU0sR0FBRyxJQUFJLE9BQU8sQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxLQUFLLEdBQUcsR0FBRyxHQUFHLElBQUksR0FBRyxHQUFHLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFBO1FBRXhGLE9BQU8sTUFBTSxDQUFBO0lBR2pCLENBQUM7SUFFRCxNQUFNLENBQUMsbUJBQW1CLENBQUMsVUFBNkI7UUFHcEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHO1lBQ3BHLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFBO1FBRXRGLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFHTyxNQUFNLENBQUMsaUJBQWlCLENBQUMsS0FBSyxHQUFHLENBQUM7UUFDdEMsSUFBSSxLQUFLLElBQUksS0FBSyxFQUFFO1lBQ2hCLEtBQUssR0FBRyxDQUFDLENBQUE7U0FDWjtRQUNELE9BQU8sS0FBSyxDQUFBO0lBQ2hCLENBQUM7SUFJRCxxQkFBcUIsQ0FBQyxVQUFrQjtRQUVwQyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFBO1FBRXZDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUE7UUFDNUMsVUFBVSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQTtRQUNoRCxVQUFVLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFBO1FBRTlDLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUV0RCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0NBTUo7QUN6WkQsc0NBQXNDO0FBS3RDLE1BQU0sT0FBUSxTQUFRLFFBQVE7SUFFMUIsWUFBbUIsQ0FBUyxFQUFTLENBQVM7UUFFMUMsS0FBSyxFQUFFLENBQUE7UUFGUSxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQVMsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUkxQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQTtRQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtJQUU5QixDQUFDO0lBTUQsSUFBSTtRQUNBLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDdEMsQ0FBQztJQUdELFNBQVMsQ0FBQyxLQUFjO1FBRXBCLE1BQU0sTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBRXZELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFFRCxLQUFLLENBQUMsSUFBWTtRQUNkLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUE7UUFDaEIsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUNoQixJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUE7UUFDakIsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFBO1FBQ2pCLE9BQU8sSUFBSSxDQUFBO0lBQ2YsQ0FBQztJQUlELEdBQUcsQ0FBQyxDQUFVO1FBQ1YsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDckIsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDckIsT0FBTyxJQUFJLENBQUE7SUFDZixDQUFDO0lBRUQsUUFBUSxDQUFDLENBQVU7UUFDZixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNyQixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNyQixPQUFPLElBQUksQ0FBQTtJQUNmLENBQUM7SUFFRCxFQUFFLENBQUMsQ0FBVTtRQUNULE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQTtRQUNkLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDM0MsT0FBTyxFQUFFLENBQUE7SUFDYixDQUFDO0lBRUQsVUFBVSxDQUFDLENBQVM7UUFDaEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQzFCLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ1osT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELFVBQVUsQ0FBQyxDQUFTO1FBQ2hCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTtRQUMxQixNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNaLE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFFRCxjQUFjLENBQUMsQ0FBUztRQUNwQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtJQUN0QyxDQUFDO0lBRUQsY0FBYyxDQUFDLENBQVM7UUFDcEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7SUFDdEMsQ0FBQztJQUdELElBQUksTUFBTTtRQUNOLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUE7UUFDOUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7UUFDMUIsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUdELFNBQVMsQ0FBQyxDQUFDO1FBRVAsaUNBQWlDO0lBRXJDLENBQUM7Q0FNSjtBQ25HRCxxQ0FBcUM7QUFNckMsTUFBTSxXQUFZLFNBQVEsUUFBUTtJQU85QixZQUFZLElBQVksQ0FBQyxFQUFFLElBQVksQ0FBQyxFQUFFLFNBQWlCLENBQUMsRUFBRSxRQUFnQixDQUFDO1FBRTNFLEtBQUssRUFBRSxDQUFBO1FBRVAsSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUE7UUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7UUFHMUIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUE7UUFDMUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUE7UUFFMUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFBO1FBQ2pELElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQTtRQUVqRCxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQTtRQUV6QixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUM1QixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksT0FBTyxDQUFDLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFBO1FBRTdDLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQTtTQUN0QjtRQUVELElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFBO1NBQ3JCO0lBR0wsQ0FBQztJQU1ELElBQUk7UUFDQSxNQUFNLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDdkUsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELFNBQVMsQ0FBQyxTQUFzQjtRQUc1QixNQUFNLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFeEcsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELE1BQU0sQ0FBQyxJQUFJO1FBRVAsTUFBTSxNQUFNLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7UUFFMUMsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELGFBQWEsQ0FBQyxLQUFjO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQztZQUNqRCxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7SUFDdEQsQ0FBQztJQUVELG1CQUFtQixDQUFDLEtBQWM7UUFFOUIsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNSLEtBQUssR0FBRyxJQUFJLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7U0FDNUI7UUFFRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7UUFFbkIsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtRQUMzQixJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQ2YsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtTQUNyQjtRQUNELElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7WUFDZixHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1NBQ3JCO1FBRUQsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtRQUMzQixJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQ2YsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtTQUNyQjtRQUNELElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7WUFDZixHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1NBQ3JCO1FBRUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNyQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3JDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDckMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUVyQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7SUFFeEIsQ0FBQztJQUVELElBQUksTUFBTTtRQUNOLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQ3BCLE9BQU8sR0FBRyxDQUFBO1NBQ2I7UUFDRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0lBQ2xDLENBQUM7SUFFRCxJQUFJLE1BQU0sQ0FBQyxNQUFjO1FBQ3JCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQTtJQUNwQyxDQUFDO0lBSUQsSUFBSSxLQUFLO1FBQ0wsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7WUFDcEIsT0FBTyxHQUFHLENBQUE7U0FDYjtRQUNELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7SUFDbEMsQ0FBQztJQUVELElBQUksS0FBSyxDQUFDLEtBQWE7UUFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFBO0lBQ25DLENBQUM7SUFJRCxJQUFJLENBQUM7UUFDRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0lBQ3JCLENBQUM7SUFFRCxJQUFJLENBQUMsQ0FBQyxDQUFTO1FBRVgsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFBO1FBRW5CLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7UUFDeEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ2QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFBO1FBRS9CLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtJQUV4QixDQUFDO0lBR0QsSUFBSSxDQUFDO1FBQ0QsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtJQUNyQixDQUFDO0lBR0QsSUFBSSxDQUFDLENBQUMsQ0FBUztRQUVYLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQUVuQixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBQzFCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNkLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQTtRQUVoQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7SUFFeEIsQ0FBQztJQUtELElBQUksT0FBTztRQUVQLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtJQUUxQixDQUFDO0lBRUQsSUFBSSxRQUFRO1FBQ1IsT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDMUMsQ0FBQztJQUVELElBQUksVUFBVTtRQUNWLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQzFDLENBQUM7SUFFRCxJQUFJLFdBQVc7UUFFWCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUE7SUFFMUIsQ0FBQztJQUdELElBQUksTUFBTTtRQUVOLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtRQUVwRSxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBRUQsSUFBSSxNQUFNLENBQUMsTUFBZTtRQUV0QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUNyQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0lBRTlCLENBQUM7SUFFRCxhQUFhLENBQUMsTUFBZTtRQUV6QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUVwQixPQUFPLElBQUksQ0FBQTtJQUVmLENBQUM7SUFJRCx3QkFBd0IsQ0FBQyxTQUFzQjtRQUUzQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBQy9DLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7UUFFM0MsT0FBTyxJQUFJLENBQUE7SUFFZixDQUFDO0lBTUQsa0NBQWtDLENBQUMsU0FBc0I7UUFHckQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBRTFCLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQUVyQixNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFBO1FBQ3RCLElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7WUFDZixHQUFHLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEU7UUFDRCxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQ2YsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1NBQ3RFO1FBRUQsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQTtRQUN0QixJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQ2YsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ3BFO1FBQ0QsSUFBSSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtZQUNmLEdBQUcsQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUN0RTtRQUVELE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUN0RCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDdEQsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3RELE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUd0RCxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBRW5CLE1BQU0sUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUE7WUFDM0QsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFBO1lBQ3ZCLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQTtTQUUxQjtRQUVELElBQUksTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7WUFFbEIsTUFBTSxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQTtZQUMzRCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUE7WUFDdkIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFBO1NBRTFCO1FBRUQsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFBO1FBRXRCLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFJRCxJQUFJLElBQUk7UUFDSixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7UUFDdkMsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUdELHVCQUF1QixDQUFDLFNBQXNCO1FBRTFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsa0NBQWtDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFBO0lBRXpFLENBQUM7SUFNRCxzQ0FBc0M7SUFDdEMsbUJBQW1CLENBQUMsSUFBWSxFQUFFLEtBQWEsRUFBRSxNQUFjLEVBQUUsR0FBVztRQUN4RSxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDMUIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFBO1FBQ2hDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQTtRQUNqQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUE7UUFDL0IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFBO1FBQ2xDLE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxLQUFhO1FBQzVCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUNuRSxPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBRUQsbUJBQW1CLENBQUMsTUFBYyxFQUFFLHFCQUE2QixHQUFHO1FBRWhFLElBQUksS0FBSyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7WUFDM0Isa0JBQWtCLEdBQUcsR0FBRyxDQUFBO1NBQzNCO1FBRUQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQzFCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO1FBRXRCLElBQUksa0JBQWtCLElBQUksR0FBRyxFQUFFO1lBQzNCLE1BQU0sTUFBTSxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1lBQ25DLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxFQUFFLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDOUU7UUFFRCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBYSxFQUFFLHFCQUE2QixHQUFHO1FBRTlELElBQUksS0FBSyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7WUFDM0Isa0JBQWtCLEdBQUcsR0FBRyxDQUFBO1NBQzNCO1FBRUQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQzFCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1FBRXBCLElBQUksa0JBQWtCLElBQUksR0FBRyxFQUFFO1lBQzNCLE1BQU0sTUFBTSxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFBO1lBQ2pDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDOUU7UUFFRCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBRUQsa0NBQWtDLENBQUMsY0FBc0IsQ0FBQyxFQUFFLHFCQUE2QixHQUFHO1FBRXhGLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFBO1FBRXJGLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFFRCxrQ0FBa0MsQ0FBQyxhQUFxQixDQUFDLEVBQUUscUJBQTZCLEdBQUc7UUFFdkYsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsVUFBVSxFQUFFLGtCQUFrQixDQUFDLENBQUE7UUFFcEYsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELGNBQWMsQ0FBQyxDQUFTLEVBQUUscUJBQTZCLENBQUM7UUFFcEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQzFCLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxLQUFLLEdBQUcsa0JBQWtCLENBQUE7UUFFaEQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELGNBQWMsQ0FBQyxDQUFTLEVBQUUscUJBQTZCLENBQUM7UUFFcEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQzFCLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUE7UUFFakQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUlELGtCQUFrQixDQUFDLENBQVM7UUFFeEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQzFCLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUE7UUFFckIsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELGtCQUFrQixDQUFDLENBQVM7UUFFeEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQzFCLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUE7UUFFckIsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQU1ELDBCQUEwQixDQUN0QixPQUFpQixFQUNqQixXQUE4QixDQUFDLEVBQy9CLGlCQUFvQyxHQUFHO1FBR3ZDLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBRWxCLFFBQVEsR0FBRyxDQUFDLENBQUE7U0FFZjtRQUVELElBQUksQ0FBQyxDQUFDLFFBQVEsWUFBWSxLQUFLLENBQUMsRUFBRTtZQUU5QixRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFBO1NBRTdEO1FBRUQsUUFBUSxHQUFHLFFBQVEsQ0FBQywrQkFBK0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFBO1FBRXZFLElBQUksQ0FBQyxDQUFDLGNBQWMsWUFBWSxLQUFLLENBQUMsSUFBSSxVQUFVLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDbEUsY0FBYyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1NBQ3JFO1FBRUQsTUFBTSxNQUFNLEdBQWtCLEVBQUUsQ0FBQTtRQUNoQyxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLO1lBQ3JELElBQUksVUFBVSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUNuQyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQ1I7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDaEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBQ0wsTUFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQTtRQUMxQyxNQUFNLG1CQUFtQixHQUFJLGNBQTJCLENBQUMsV0FBVyxDQUFBO1FBQ3BFLE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLEdBQUcsbUJBQW1CLENBQUE7UUFDM0UsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFBO1FBRTdCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBRXJDLElBQUksV0FBbUIsQ0FBQTtZQUN2QixJQUFJLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFFL0IsV0FBVyxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUE7YUFFdkM7aUJBQ0k7Z0JBRUQsV0FBVyxHQUFHLGtCQUFrQixHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksQ0FBQyxDQUFBO2FBRWpFO1lBRUQsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxDQUFBO1lBRXRELElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQTtZQUNmLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNwQyxPQUFPLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQ3hCO1lBRUQsU0FBUyxDQUFDLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQTtZQUM5QixnQkFBZ0IsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUE7WUFDNUMsOERBQThEO1lBQzlELE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7U0FFekI7UUFFRCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBRUQsMkJBQTJCLENBQ3ZCLE9BQWlCLEVBQ2pCLFdBQThCLENBQUMsRUFDL0Isa0JBQXFDLEdBQUc7UUFHeEMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFFbEIsUUFBUSxHQUFHLENBQUMsQ0FBQTtTQUVmO1FBRUQsSUFBSSxDQUFDLENBQUMsUUFBUSxZQUFZLEtBQUssQ0FBQyxFQUFFO1lBRTlCLFFBQVEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7U0FFN0Q7UUFFRCxRQUFRLEdBQUcsUUFBUSxDQUFDLCtCQUErQixDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFdkUsSUFBSSxDQUFDLENBQUMsZUFBZSxZQUFZLEtBQUssQ0FBQyxJQUFJLFVBQVUsQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUNwRSxlQUFlLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDdkU7UUFFRCxNQUFNLE1BQU0sR0FBa0IsRUFBRSxDQUFBO1FBQ2hDLE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUs7WUFDckQsSUFBSSxVQUFVLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ3BDLENBQUMsR0FBRyxDQUFDLENBQUE7YUFDUjtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNoQixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7UUFDTCxNQUFNLGFBQWEsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFBO1FBQzFDLE1BQU0sb0JBQW9CLEdBQUksZUFBNEIsQ0FBQyxXQUFXLENBQUE7UUFDdEUsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLGFBQWEsR0FBRyxvQkFBb0IsQ0FBQTtRQUM5RSxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxDQUFDLENBQUE7UUFFN0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckMsSUFBSSxZQUFvQixDQUFBO1lBQ3hCLElBQUksVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUVoQyxZQUFZLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUV6QztpQkFDSTtnQkFFRCxZQUFZLEdBQUcsbUJBQW1CLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxDQUFDLENBQUE7YUFFbkU7WUFFRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLENBQUE7WUFFeEQsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFBO1lBQ2YsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3BDLE9BQU8sR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDeEI7WUFFRCxTQUFTLENBQUMsQ0FBQyxHQUFHLGdCQUFnQixDQUFBO1lBQzlCLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQTtZQUM1Qyw4REFBOEQ7WUFDOUQsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtTQUN6QjtRQUVELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFNRCxpQ0FBaUMsQ0FBQyxjQUFzQixFQUFFLFVBQWtCLENBQUM7UUFDekUsTUFBTSxNQUFNLEdBQWtCLEVBQUUsQ0FBQTtRQUNoQyxNQUFNLFlBQVksR0FBRyxPQUFPLEdBQUcsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDbkQsTUFBTSxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxHQUFHLGNBQWMsQ0FBQTtRQUNoRSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JDLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDaEYsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtTQUN6QjtRQUNELE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFFRCxrQ0FBa0MsQ0FBQyxjQUFzQixFQUFFLFVBQWtCLENBQUM7UUFDMUUsTUFBTSxNQUFNLEdBQWtCLEVBQUUsQ0FBQTtRQUNoQyxNQUFNLFlBQVksR0FBRyxPQUFPLEdBQUcsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDbkQsTUFBTSxZQUFZLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQyxHQUFHLGNBQWMsQ0FBQTtRQUNsRSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JDLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDbEYsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtTQUN6QjtRQUNELE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFJRCx5QkFBeUIsQ0FDckIsS0FBZSxFQUNmLFVBQTZCLENBQUMsRUFDOUIsUUFBNEIsRUFDNUIsY0FBa0M7UUFHbEMsSUFBSSxDQUFDLENBQUMsT0FBTyxZQUFZLEtBQUssQ0FBQyxFQUFFO1lBQzdCLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUNyRDtRQUVELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFBO1FBRWpGLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUs7WUFDeEMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUE7UUFDOUIsQ0FBQyxDQUFDLENBQUE7UUFFRixPQUFPLElBQUksQ0FBQTtJQUVmLENBQUM7SUFFRCwwQkFBMEIsQ0FDdEIsS0FBZSxFQUNmLFVBQTZCLENBQUMsRUFDOUIsUUFBNEIsRUFDNUIsZUFBbUM7UUFHbkMsSUFBSSxDQUFDLENBQUMsT0FBTyxZQUFZLEtBQUssQ0FBQyxFQUFFO1lBQzdCLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUNyRDtRQUVELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsQ0FBQyxDQUFBO1FBRW5GLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUs7WUFDeEMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUE7UUFDOUIsQ0FBQyxDQUFDLENBQUE7UUFFRixPQUFPLElBQUksQ0FBQTtJQUVmLENBQUM7SUFHRCxnQ0FBZ0MsQ0FBQyxLQUFlLEVBQUUsT0FBZTtRQUU3RCxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsaUNBQWlDLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUU1RSxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLO1lBQ3hDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1FBQzlCLENBQUMsQ0FBQyxDQUFBO1FBRUYsT0FBTyxJQUFJLENBQUE7SUFFZixDQUFDO0lBRUQsaUNBQWlDLENBQUMsS0FBZSxFQUFFLE9BQWU7UUFFOUQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFFN0UsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSztZQUN4QyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQTtRQUM5QixDQUFDLENBQUMsQ0FBQTtRQUVGLE9BQU8sSUFBSSxDQUFBO0lBRWYsQ0FBQztJQUlELG1CQUFtQixDQUFDLFVBQWtCLENBQUMsRUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU07UUFDekQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQTtRQUN4RCxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3ZCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO1NBQ3pCO1FBQ0QsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELHNCQUFzQixDQUFDLFVBQWtCLENBQUMsRUFBRSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUs7UUFDMUQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQTtRQUN4RCxJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ3JCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1NBQ3ZCO1FBQ0QsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELHVCQUF1QixDQUFDLFVBQWtCLENBQUM7UUFDdkMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxDQUFBO1FBQ3RFLE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFFRCwwQkFBMEIsQ0FBQyxVQUFrQixDQUFDO1FBQzFDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsQ0FBQTtRQUNyRSxPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBSUQsZUFBZTtJQUNmLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNO1FBQzlCLE1BQU0sTUFBTSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUE7UUFDaEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1NBQ3hDO1FBQ0QsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUdELFlBQVk7UUFDUixJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQTtJQUM5QixDQUFDO0lBRUQsYUFBYTtRQUNULElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFBO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtJQUNwQixDQUFDO0lBR0QsU0FBUztRQUVMLGlDQUFpQztJQUVyQyxDQUFDO0lBRUQsd0JBQXdCO1FBRXBCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBRXZCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtTQUVuQjtJQUVMLENBQUM7Q0FNSjtBQ25zQkQsMERBQTBEO0FBQzFELHNDQUFzQztBQUN0QyxxQ0FBcUM7QUFDckMseUNBQXlDO0FBeUZ6QyxNQUFNLE1BQU8sU0FBUSxRQUFRO0lBdUR6QixZQUNJLFlBQW9CLENBQUMsUUFBUTtRQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLEVBQ3JCLGtCQUE2QyxJQUFJLEVBQ2pELGNBQXNCLElBQUksRUFDMUIsWUFBa0I7UUFHbEIsS0FBSyxFQUFFLENBQUE7UUE3RFgsNEJBQXVCLEdBQVksR0FBRyxDQUFBO1FBTXRDLGFBQVEsR0FBWSxHQUFHLENBQUE7UUFFdkIscUJBQWdCLEdBQVksT0FBTyxDQUFDLGdCQUFnQixDQUFBO1FBUXBELHlCQUFvQixHQUEwQixHQUFHLENBQUE7UUFFakQseUJBQW9CLEdBQThCLEVBQUUsQ0FBQSxDQUFDLHdNQUF3TTtRQUU3UCxrQ0FBNkIsR0FBZSxHQUFHLENBQUE7UUFTL0MsY0FBUyxHQUFZLEVBQUUsQ0FBQTtRQUV2Qix3QkFBbUIsR0FBWSxFQUFFLENBQUE7UUFDakMsaUNBQTRCLEdBQVksR0FBRyxDQUFBO1FBSzNDLDBCQUFxQixHQUFHLENBQUMsQ0FBQTtRQUV6QixtQkFBYyxHQUFZLEVBQUUsQ0FBQTtRQUM1QixpQkFBWSxHQUFZLEVBQUUsQ0FBQTtRQVExQiwyQkFBc0IsR0FBWSxFQUFFLENBQUE7UUF3dkVwQyxpQkFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUE7UUF6dUU5QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtRQUNwQixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtRQUUxQixxQkFBcUI7UUFFckIsTUFBTSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFBO1FBQ3RDLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQTtRQUV2QyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQTtRQUN2QixzR0FBc0c7UUFDdEcsK0VBQStFO1FBRS9FLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLFdBQVcsQ0FBQyxDQUFBO1FBRWxFLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFBO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFBO1FBRXBCLHFFQUFxRTtRQUVyRSxtR0FBbUc7UUFDbkcsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUE7UUFFdEIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQTtRQUVoQyw4R0FBOEc7UUFDOUcsdUNBQXVDO1FBRXZDLGlGQUFpRjtRQUVqRixpRkFBaUY7UUFDakYsb0ZBQW9GO1FBRXBGLG9JQUFvSTtRQUVwSSxnQ0FBZ0M7UUFDaEMseUJBQXlCO1FBQ3pCLHlCQUF5QjtRQUN6QiwwQkFBMEI7UUFDMUIsMEJBQTBCO1FBQzFCLDZCQUE2QjtRQUM3Qix1QkFBdUI7UUFDdkIseUJBQXlCO1FBQ3pCLElBQUk7UUFFSixJQUFJLENBQUMsa0NBQWtDLEdBQUc7UUFDMUMsQ0FBQyxDQUFBO1FBRUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUE7UUFFekIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLFlBQVksQ0FBQyxDQUFBO1FBRTFFLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFBO1FBRXBDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtRQUdwQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7SUFHekIsQ0FBQztJQUdELE1BQU0sS0FBSyxTQUFTO1FBRWhCLE9BQU8sTUFBTSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUE7SUFFbEMsQ0FBQztJQUVELE1BQU0sS0FBSyxVQUFVO1FBRWpCLE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUE7UUFDMUIsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQTtRQUVyQyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUNuQixJQUFJLENBQUMsWUFBWSxFQUNqQixJQUFJLENBQUMsWUFBWSxFQUNqQixJQUFJLENBQUMsWUFBWSxFQUNqQixJQUFJLENBQUMsWUFBWSxFQUNqQixJQUFJLENBQUMsWUFBWSxDQUNwQixDQUFBO1FBRUQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELE1BQU0sS0FBSyxTQUFTO1FBRWhCLE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUE7UUFDMUIsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQTtRQUVyQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRWhILE9BQU8sS0FBSyxDQUFBO0lBRWhCLENBQUM7SUFNRCxRQUFRLENBQUMsU0FBaUIsRUFBRSxlQUE0QixFQUFFLFlBQWtCO0lBSTVFLENBQUM7SUFNRCxpQkFBaUI7UUFDYixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUE7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFBO1FBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLG1DQUFtQyxDQUFBO0lBQzlELENBQUM7SUFFRCxrQkFBa0I7UUFDZCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUE7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsa0JBQWtCLENBQUE7SUFDN0MsQ0FBQztJQUVELGtCQUFrQjtRQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQTtRQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQTtJQUM3QyxDQUFDO0lBSUQsb0JBQW9CLENBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxXQUFXLEdBQUcsS0FBSztRQUdoRSxJQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBRWxCLFdBQVcsR0FBRyxLQUFLLENBQUE7U0FFdEI7UUFFRCxJQUFJLENBQUMsRUFBRSxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBRXRCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsQ0FBQTtZQUVsRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUE7WUFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFBO1NBRTFCO2FBQ0k7WUFFRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFBO1NBRTFDO1FBRUQsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFHZixJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUE7U0FHdEM7UUFHRCxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsR0FBRyxHQUFHLENBQUE7UUFFekMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFBO1FBRWxDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO0lBRTNDLENBQUM7SUFJRCxJQUFJLHNCQUFzQixDQUFDLFVBQW1CO1FBQzFDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxVQUFVLENBQUE7UUFDekMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTztnQkFDbkMsc0pBQXNKLENBQUE7U0FDN0o7YUFDSTtZQUNELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTztnQkFDbkMsc0pBQXNKLENBQUE7U0FDN0o7SUFDTCxDQUFDO0lBR0QsSUFBSSxzQkFBc0I7UUFDdEIsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUE7SUFDdkMsQ0FBQztJQUlELElBQUksY0FBYztRQUVkLE1BQU0sTUFBTSxHQUFHLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFBO1FBRWpELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFFRCw2QkFBNkI7UUFFekIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsK0JBQStCLEVBQUU7WUFFN0MsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUE7WUFFN0IsSUFBSSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsR0FBRyxHQUFHLENBQUE7U0FFbkQ7SUFFTCxDQUFDO0lBRUQsc0JBQXNCO1FBRWxCLDhCQUE4QjtJQUVsQyxDQUFDO0lBRUQsaUJBQWlCLENBQUMsUUFBUSxFQUFFLEtBQUs7UUFFN0IsTUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUVqRCxJQUFJLENBQUMsVUFBVSxFQUFFO1lBRWIsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQTtTQUU5QztJQUVMLENBQUM7SUFHRCxhQUFhLENBQUMsU0FBUyxFQUFFLFdBQVc7UUFDaEMsSUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUMvQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1QsTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUE7U0FDL0M7UUFDRCxPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBRUQsSUFBVyxlQUFlO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFBO0lBQ2hDLENBQUM7SUFFRCxJQUFXLFNBQVM7UUFFaEIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQTtJQUVsQyxDQUFDO0lBR0QsWUFBWSxDQUFDLEdBQVcsRUFBRSxhQUFxQixFQUFFLFVBQTREO1FBRXpHLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFBO1FBQ3hCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxhQUFhLENBQUE7UUFDdEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUE7UUFFN0IsTUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQTtRQUM5RCxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxVQUFVLENBQUMsQ0FBQTtRQUVoRyxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQTtJQUUzQixDQUFDO0lBR0QsOEJBQThCO1FBRTFCLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFFOUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7U0FFbEY7SUFFTCxDQUFDO0lBRUQsOENBQThDO1FBRTFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO1lBRS9CLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtTQUU5RjtJQUVMLENBQUM7SUFHRCxJQUFJLG1CQUFtQjtRQUVuQixPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQTtJQUVwQyxDQUFDO0lBRUQsSUFBSSxtQkFBbUIsQ0FBQyxtQkFBMEM7UUFFOUQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLG1CQUFtQixDQUFBO1FBRS9DLElBQUksQ0FBQyw4Q0FBOEMsRUFBRSxDQUFBO0lBRXpELENBQUM7SUFHRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFBO0lBQ3pDLENBQUM7SUFHRCxJQUFJLFNBQVMsQ0FBQyxTQUFTO1FBRW5CLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxTQUFTLEVBQUU7WUFFN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUV4RDtJQUVMLENBQUM7SUFJRCxJQUFJLFNBQVMsQ0FBQyxTQUFpQjtRQUMzQixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUE7SUFDekQsQ0FBQztJQUVELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDckQsQ0FBQztJQUdELElBQUksVUFBVTtRQUVWLE1BQU0sTUFBTSxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUV6RyxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBSUQsSUFBSSxVQUFVO1FBQ1YsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDckIsT0FBTyxHQUFHLENBQUE7U0FDYjtRQUNELElBQUksQ0FBQyxDQUFDLElBQUksWUFBWSxZQUFZLENBQUMsRUFBRTtZQUNqQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFBO1NBQ25DO1FBQ0QsT0FBTyxJQUFJLENBQUE7SUFDZixDQUFDO0lBR0QsSUFBSSxRQUFRO1FBQ1IsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ3BCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUE7U0FDakM7UUFDRCxPQUFPLElBQUksQ0FBQTtJQUNmLENBQUM7SUFHRCxJQUFXLE9BQU8sQ0FBQyxPQUFnQjtRQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQTtRQUN2QixJQUFJLENBQUMsbUNBQW1DLEVBQUUsQ0FBQTtJQUM5QyxDQUFDO0lBRUQsSUFBVyxPQUFPO1FBQ2QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFBO0lBQ3hCLENBQUM7SUFFRCxtQ0FBbUM7UUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUE7UUFDM0IsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUE7SUFDOUMsQ0FBQztJQUtELElBQVcsUUFBUTtRQUVmLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUE7SUFFaEUsQ0FBQztJQUdELElBQVcsUUFBUSxDQUFDLEtBQWE7UUFFN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQTtJQUU3RCxDQUFDO0lBTUQsSUFBSSxZQUFZO1FBRVosT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFBO0lBRTdCLENBQUM7SUFFRCxJQUFJLFlBQVksQ0FBQyxZQUFZO1FBRXpCLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFBO0lBRXJDLENBQUM7SUFFRCxhQUFhLENBQUMsVUFBVTtRQUVwQixrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUNqQixPQUFPLEVBQUUsQ0FBQTtTQUNaO1FBRUQsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDbkQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDWixPQUFPLEdBQUcsQ0FBQTtTQUNiO1FBQ0QsT0FBTyxFQUFFLENBQUE7SUFFYixDQUFDO0lBSUQsYUFBYSxDQUFDLFVBQWtCO1FBRTVCLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDakIsT0FBTTtTQUNUO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7U0FDdEM7SUFFTCxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsVUFBa0I7UUFFL0Isa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDakIsT0FBTTtTQUNUO1FBRUQsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDbkQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFFWixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUE7U0FFckM7SUFHTCxDQUFDO0lBSUQsTUFBTSxDQUFDLHFCQUFxQixDQUFDLFNBQWlCO1FBQzFDLE1BQU0sZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDMUQsSUFBSSxNQUFNLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDekIsT0FBTyxHQUFHLENBQUE7U0FDYjtRQUNELGFBQWE7UUFDYixNQUFNLE1BQU0sR0FBRyxlQUFlLENBQUMsTUFBTSxDQUFBO1FBQ3JDLE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFLRCxNQUFNLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLEtBQUs7UUFFdEMsT0FBTTtRQUVOLGFBQWE7UUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRTtZQUN2QixPQUFNO1NBQ1Q7UUFDRCxJQUFJLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQ25ELE9BQU07U0FDVDtRQUVELElBQUksVUFBVSxDQUFBO1FBQ2QsSUFBSSxTQUFTLENBQUE7UUFFYixJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQVEsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDOUQsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtvQkFDbEMsU0FBUTtpQkFDWDtnQkFDRCxNQUFNLEtBQUssR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtnQkFDM0MsU0FBUyxHQUFHLE9BQU8sS0FBSyxDQUFBO2dCQUV4QixJQUFJLFNBQVMsS0FBSyxRQUFRLEVBQUU7b0JBQ3hCLElBQUksS0FBWSxLQUFLLEVBQUUsSUFBSSxDQUFFLEtBQWEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDbEUsVUFBVSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUE7cUJBQ3ZDO2lCQUNKO3FCQUNJLElBQUksU0FBUyxJQUFJLFFBQVEsRUFBRTtvQkFDNUIsSUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3RFLFVBQVUsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFBO3FCQUN2QztpQkFDSjtnQkFFRCxJQUFJLE9BQU8sVUFBVSxLQUFLLFdBQVcsRUFBRTtvQkFDbkMsTUFBSztpQkFDUjthQUNKO1NBQ0o7UUFFRCxJQUFJLE9BQU8sVUFBVSxLQUFLLFdBQVcsRUFBRTtZQUNuQyxNQUFNLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDekQsaUJBQWlCLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQTtZQUNuQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUE7WUFFdkUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDOUMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtvQkFDbEMsU0FBUTtpQkFDWDtnQkFDRCxVQUFVLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUN2QztZQUVELFNBQVMsR0FBRyxPQUFPLFVBQVUsQ0FBQyxLQUFLLENBQUE7U0FDdEM7UUFFRCxJQUFJLFNBQVMsS0FBSyxRQUFRLEVBQUU7WUFDeEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3JELElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFO29CQUNsRixRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUU7b0JBQ3hCLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7b0JBQ3pDLE9BQU07aUJBQ1Q7YUFDSjtZQUNELFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFBO1NBQ3RDO2FBQ0ksSUFBSSxTQUFTLEtBQUssUUFBUSxFQUFFO1lBRTdCLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFBO1lBRXhCLElBQUk7Z0JBRUEsZ0JBQWdCLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFFNUU7WUFBQyxPQUFPLEtBQUssRUFBRTthQUVmO1lBR0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGdCQUFnQixFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN2QyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtvQkFDeEYsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUFFO29CQUN4QixVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO29CQUM1QyxPQUFNO2lCQUNUO2FBQ0o7WUFDRCxVQUFVLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxHQUFHLEdBQUcsS0FBSyxHQUFHLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFBO1NBQ3hFO0lBQ0wsQ0FBQztJQUVELE1BQU0sQ0FBQyxhQUFhLENBQUMsUUFBUTtRQUN6QixJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUE7UUFDckMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xELE1BQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFRLENBQUE7WUFDakQsSUFBSSxVQUFVLENBQUE7WUFFZCxJQUFJO2dCQUVBLFVBQVUsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFBO2FBRTVFO1lBQUMsT0FBTyxLQUFLLEVBQUU7YUFFZjtZQUVELE9BQU8sVUFBVSxDQUFBO1NBQ3BCO0lBQ0wsQ0FBQztJQUlELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUE7SUFDckMsQ0FBQztJQUVELElBQUksYUFBYTtRQUNiLE9BQU8sZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO0lBQ2pELENBQUM7SUFFRCxJQUFXLE1BQU07UUFDYixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUE7SUFDekIsQ0FBQztJQUdELElBQVcsTUFBTSxDQUFDLENBQVU7UUFFeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUE7UUFFbEIsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtTQUNuQzthQUNJO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFBO1NBQ3BDO0lBR0wsQ0FBQztJQUVELE1BQU0sS0FBSyxTQUFTLENBQUMsS0FBYTtRQUU5QixNQUFNLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQTtRQUV6QixNQUFNLElBQUksR0FBRyxLQUFLLENBQUE7UUFDbEIsTUFBTSxLQUFLLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQTtRQUN4QixNQUFNLGVBQWUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUE7UUFDM0UsZUFBZSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFBO1FBQ2xELGVBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLFFBQVEsR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFBO1FBQ3ZELGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUE7SUFFN0MsQ0FBQztJQUVELE1BQU0sS0FBSyxTQUFTO1FBRWhCLE9BQU8sTUFBTSxDQUFDLFVBQVUsQ0FBQTtJQUU1QixDQUFDO0lBTUQsd0JBQXdCO1FBRXBCLDZEQUE2RDtRQUU3RCx1RUFBdUU7SUFFM0UsQ0FBQztJQU1ELElBQVcsS0FBSztRQUVaLHNMQUFzTDtRQUV0TCx5Q0FBeUM7UUFFekMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUV4QixJQUFJLENBQUMsTUFBTSxFQUFFO1lBRVQsTUFBTSxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDL0YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUE7WUFDNUUsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUE7U0FFcEI7UUFFRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtJQUV4QixDQUFDO0lBRUQsSUFBVyxLQUFLLENBQUMsU0FBc0I7UUFFbkMsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDZixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1NBQzNCO0lBRUwsQ0FBQztJQUVELFFBQVEsQ0FBQyxTQUFTLEVBQUUsTUFBTSxHQUFHLENBQUMsRUFBRSxzQkFBc0IsR0FBRyxFQUFFO1FBR3ZELE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxXQUFXLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFFaEUsSUFBSSxNQUFNLElBQUksU0FBUyxFQUFFO1lBQ3JCLFNBQVMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO1NBQzVCO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUE7UUFFdkIsNENBQTRDO1FBQzVDLGtDQUFrQztRQUNsQyxvQkFBb0I7UUFDcEIsSUFBSTtRQUNKLHNDQUFzQztRQUN0Qyw4QkFBOEI7UUFDOUIsZ0JBQWdCO1FBRWhCLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUNoRSxPQUFNO1NBQ1Q7UUFHRCxNQUFNLENBQUMsMkJBQTJCLENBQzlCLElBQUksQ0FBQyxlQUFlLEVBQ3BCLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUNuQixTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFDbkIsU0FBUyxDQUFDLEtBQUssRUFDZixTQUFTLENBQUMsTUFBTSxFQUNoQixTQUFTLENBQUMsTUFBTSxDQUNuQixDQUFBO1FBR0QsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxTQUFTLENBQUMsS0FBSyxJQUFJLHNCQUFzQixFQUFFO1lBRTlGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUVyQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7WUFFdEIsd0JBQXdCO1NBRTNCO0lBSUwsQ0FBQztJQUlELElBQUksTUFBTTtRQUVOLElBQUksTUFBbUIsQ0FBQTtRQUV2Qix5TEFBeUw7UUFFekwsMkNBQTJDO1FBRTNDLElBQUk7UUFDSixPQUFPO1FBQ1AsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBRXJCLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQTtTQUU5RzthQUNJO1lBRUQsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUE7WUFFMUIsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDWixNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQTtTQUVmO1FBRUQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELElBQUksTUFBTSxDQUFDLFNBQVM7UUFFaEIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQTtRQUV4QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFBO0lBRXJHLENBQUM7SUFHRCxlQUFlO0lBSWYsQ0FBQztJQUdELFdBQVcsQ0FDUCxPQUF3QixHQUFHLEVBQzNCLFFBQXlCLEdBQUcsRUFDNUIsU0FBMEIsR0FBRyxFQUM3QixNQUF1QixHQUFHLEVBQzFCLFNBQTBCLEdBQUcsRUFDN0IsUUFBeUIsR0FBRztRQUc1QixNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRWxDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDbkMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUNyQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFBO1FBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFDakMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUN2QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBRXJDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFDMUIsSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLGNBQWMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsS0FBSyxFQUFFO1lBQ2hGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUNyQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7U0FDekI7SUFFTCxDQUFDO0lBRUQsUUFBUSxDQUFDLE1BQXdCLEVBQUUsS0FBdUI7UUFFdEQsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUVsQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFBO1FBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUE7UUFFckMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUMxQixJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksY0FBYyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxLQUFLLEVBQUU7WUFDaEYsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1lBQ3JCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtTQUN6QjtJQUVMLENBQUM7SUFFRCxXQUFXLENBQUMsTUFBd0IsRUFBRSxLQUF1QjtRQUV6RCxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRWxDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFDMUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUV4QyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBQzFCLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBSSxjQUFjLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLEtBQUssRUFBRTtZQUNoRixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7WUFDckIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO1NBQ3pCO0lBRUwsQ0FBQztJQUVELFdBQVcsQ0FBQyxNQUF3QixFQUFFLEtBQXVCO1FBRXpELE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFFbEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUMxQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBRXhDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFDMUIsSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLGNBQWMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsS0FBSyxFQUFFO1lBQ2hGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUNyQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7U0FDekI7SUFFTCxDQUFDO0lBRUQsU0FBUyxDQUFDLE1BQXdCO1FBRTlCLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFFbEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUV2QyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBQzFCLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBSSxjQUFjLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLEtBQUssRUFBRTtZQUNoRixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7WUFDckIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO1NBQ3pCO0lBRUwsQ0FBQztJQUVELFVBQVUsQ0FBQyxJQUFzQixFQUFFLEtBQXVCLEVBQUUsTUFBd0IsRUFBRSxHQUFxQjtRQUV2RyxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRWxDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDekMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUMzQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLE1BQU0sQ0FBQyxDQUFBO1FBQzdDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFFdkMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUMxQixJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksY0FBYyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxLQUFLLEVBQUU7WUFDaEYsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1lBQ3JCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtTQUN6QjtJQUVMLENBQUM7SUFFRCxVQUFVLENBQUMsT0FBeUI7UUFFaEMsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUVsQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFBO1FBRXpDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFDMUIsSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLGNBQWMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsS0FBSyxFQUFFO1lBQ2hGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUNyQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7U0FDekI7SUFFTCxDQUFDO0lBRUQsV0FBVyxDQUFDLElBQXNCLEVBQUUsS0FBdUIsRUFBRSxNQUF3QixFQUFFLEdBQXFCO1FBRXhHLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFFbEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUMxQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQzVDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFDOUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxHQUFHLENBQUMsQ0FBQTtRQUV4QyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBQzFCLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBSSxjQUFjLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLEtBQUssRUFBRTtZQUNoRixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7WUFDckIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO1NBQ3pCO0lBRUwsQ0FBQztJQU1ELFNBQVMsQ0FDTCxTQUEwQixHQUFHLEVBQzdCLFFBQXlCLENBQUMsRUFDMUIsUUFBaUIsT0FBTyxDQUFDLFVBQVUsRUFDbkMsUUFBZ0IsT0FBTztRQUd2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBRTNDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFN0MsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFdkQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQTtJQUUvQyxDQUFDO0lBTUQsZ0JBQWdCLENBQUMsWUFBb0IsRUFBRSxLQUF1QjtRQUcxRCxJQUFJO1lBRUEsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2YsT0FBTTthQUNUO1lBQ0QsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUssS0FBZ0IsQ0FBQyxTQUFTLEVBQUU7Z0JBQ2xELEtBQUssR0FBRyxFQUFFLEdBQUksS0FBZ0IsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFBO2FBQ3JEO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsR0FBRyxLQUFLLENBQUE7U0FFbkM7UUFBQyxPQUFPLFNBQVMsRUFBRTtZQUVoQixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1NBRXpCO0lBR0wsQ0FBQztJQUlELElBQUksc0JBQXNCO1FBRXRCLE1BQU0sTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLElBQUksTUFBTSxDQUFDLENBQUE7UUFFbkQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUVELElBQUksc0JBQXNCLENBQUMsc0JBQXNCO1FBRTdDLElBQUksc0JBQXNCLEVBQUU7WUFFeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFBO1NBRWhDO2FBQ0k7WUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUE7U0FFcEM7SUFFTCxDQUFDO0lBSUQsSUFBSSxlQUFlO1FBQ2YsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUE7SUFDaEMsQ0FBQztJQUVELElBQUksZUFBZSxDQUFDLGVBQXdCO1FBRXhDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxlQUFlLENBQUE7UUFFdkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDLFdBQVcsQ0FBQTtJQUU1RCxDQUFDO0lBSUQsSUFBSSxLQUFLO1FBQ0wsT0FBTyxDQUFDLEdBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFlLENBQUE7SUFDMUMsQ0FBQztJQUVELElBQUksS0FBSyxDQUFDLEtBQUs7UUFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxFQUFFLEdBQUcsS0FBSyxDQUFBO0lBQ25DLENBQUM7SUFNRCxNQUFNLENBQUMsOENBQThDLENBQ2pELFdBQTRELEVBQzVELFFBQWdCLEVBQ2hCLEtBQWEsRUFDYixXQUFXLEdBQUcsK0JBQStCLEVBQzdDLGlCQUEyQixFQUMzQiw0QkFBc0M7UUFJdEMsU0FBUyxnQ0FBZ0M7WUFFckMsQ0FBQyw0QkFBNEIsSUFBSSxHQUFHLENBQUMsRUFBRSxDQUFDO1lBRXZDLFdBQXdCLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLO2dCQUUxRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtZQUU3QixDQUFDLENBQUMsQ0FBQTtRQUVOLENBQUM7UUFHRCxJQUFJLFVBQVUsRUFBRTtZQUVaLGlFQUFpRTtZQUNqRSxJQUFJLFFBQVEsRUFBRSxDQUFDLHdCQUF3QixDQUFDLEtBQUssR0FBRyxRQUFRLEVBQUUsZ0NBQWdDLENBQUMsQ0FBQTtTQUk5RjtRQUdELElBQUksQ0FBQyxDQUFDLFdBQVcsWUFBWSxLQUFLLENBQUMsRUFBRTtZQUNqQyxXQUFXLEdBQUcsQ0FBQyxXQUFXLENBQVEsQ0FBQTtTQUNyQztRQUVELE1BQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFBO1FBQzNCLE1BQU0sbUJBQW1CLEdBQUcsRUFBRSxDQUFBO1FBQzlCLE1BQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFBO1FBQzNCLE1BQU0saUJBQWlCLEdBQUcsRUFBRSxDQUFBO1FBRTVCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBSSxXQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUVsRCxJQUFJLElBQUksR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFFekIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUV0QixJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQTthQUU5QjtZQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUUsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLENBQUE7WUFFakUsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUE7WUFDNUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtZQUN2RCxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQTtZQUNqRCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBO1lBRTNELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQTtZQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixHQUFHLEVBQUUsR0FBRyxRQUFRLEdBQUcsR0FBRyxDQUFBO1lBQ25ELElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLEVBQUUsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFBO1lBQzdDLElBQUksQ0FBQyxLQUFLLENBQUMsd0JBQXdCLEdBQUcsV0FBVyxDQUFBO1NBRXBEO1FBSUQsaUJBQWlCLEVBQUUsQ0FBQTtRQUduQixNQUFNLGdCQUFnQixHQUFHO1lBRXJCLG1CQUFtQixFQUFFLDJCQUEyQjtZQUNoRCxXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLE9BQU8sRUFBRSxXQUFXO1lBQ3BCLGtCQUFrQixFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUU7U0FFakMsQ0FBQTtRQUVELFNBQVMsMkJBQTJCO1lBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBSSxXQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbEQsSUFBSSxJQUFJLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUN6QixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7b0JBQ3RCLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFBO2lCQUM5QjtnQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUE7Z0JBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxHQUFHLFFBQVEsR0FBRyxHQUFHLENBQUE7Z0JBQ25ELElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLEVBQUUsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFBO2dCQUM3QyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsR0FBRyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDdEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQ2hELElBQUksQ0FBQyxLQUFLLENBQUMsd0JBQXdCLEdBQUcsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDN0Q7UUFDTCxDQUFDO1FBRUQsU0FBUyxtQkFBbUIsQ0FBQyxLQUFLO1lBQzlCLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUE7WUFDM0IsSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDUCxPQUFNO2FBQ1Q7WUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3RCLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFBO2FBQzlCO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsR0FBRyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN0RCxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNoRCxJQUFJLENBQUMsS0FBSyxDQUFDLHdCQUF3QixHQUFHLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRTFELGdDQUFnQyxFQUFFLENBQUE7WUFFbEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsRUFBRSxtQkFBbUIsRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUV4RSxDQUFDO1FBRUQsU0FBUywyQkFBMkI7WUFDaEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFJLFdBQW1CLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUVsRCxJQUFJLElBQUksR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBRXpCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtvQkFDdEIsSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7aUJBQzlCO2dCQUVELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUMzQyxJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixHQUFHLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUN0RCxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDaEQsSUFBSSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsR0FBRyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFFMUQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsRUFBRSxtQkFBbUIsRUFBRSxJQUFJLENBQUMsQ0FBQTthQUV2RTtRQUlMLENBQUM7UUFFRCxPQUFPLGdCQUFnQixDQUFBO0lBRTNCLENBQUM7SUFNRCxrQkFBa0I7SUFJbEIsQ0FBQztJQVlELE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sR0FBRyxDQUFDO1FBRTVFLHFDQUFxQztRQUVyQyxnSEFBZ0g7UUFDaEgsZ0tBQWdLO1FBQ2hLLDBKQUEwSjtRQUMxSix3SkFBd0o7UUFDeEosc0pBQXNKO1FBRXRKLElBQUk7UUFFSixJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtZQUNwRixPQUFNO1NBQ1Q7UUFFRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLElBQUksVUFBVSxFQUFFO1lBRzFCLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQTtTQUVkO1FBRUQsSUFBSSxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDWixNQUFNLEdBQUcsTUFBTSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7U0FDdEM7UUFFRCxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNYLEtBQUssR0FBRyxLQUFLLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQTtTQUNwQztRQUVELElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFBO1FBRS9CLE1BQU0sY0FBYyxHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsR0FBRyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxZQUFZLEdBQUcsTUFBTTtZQUNuRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxZQUFZLEdBQUcsTUFBTSxHQUFHLE1BQU0sQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFBO1FBRWpFLElBQUksT0FBTyxDQUFDLE1BQU0sRUFBRTtZQUNoQixHQUFHLEdBQUcsR0FBRyxHQUFHLGNBQWMsR0FBRyxHQUFHLENBQUE7U0FDbkM7YUFDSTtZQUNELE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQTtTQUNsRDtRQUVELElBQUksTUFBTSxJQUFJLEdBQUcsRUFBRTtZQUNmLEdBQUcsR0FBRyxHQUFHLEdBQUcsaUJBQWlCLENBQUE7U0FDaEM7YUFDSTtZQUNELEdBQUcsR0FBRyxHQUFHLEdBQUcsVUFBVSxHQUFHLE1BQU0sR0FBRyxHQUFHLENBQUE7U0FDeEM7UUFFRCxJQUFJLEtBQUssSUFBSSxHQUFHLEVBQUU7WUFDZCxHQUFHLEdBQUcsR0FBRyxHQUFHLGdCQUFnQixDQUFBO1NBQy9CO2FBQ0k7WUFDRCxHQUFHLEdBQUcsR0FBRyxHQUFHLFNBQVMsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFBO1NBQ3RDO1FBRUQsSUFBSSxPQUFPLENBQUMsRUFBRSxJQUFJLFVBQVUsRUFBRTtZQUcxQixJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUE7U0FFZDtRQUVELE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQTtJQUV2RCxDQUFDO0lBSUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0I7UUFHdkUsTUFBTSxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUE7UUFFbEMsSUFBSSxFQUFFLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDdkQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUE7U0FDNUY7UUFFRCxJQUFJLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNyRCxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUE7U0FDeEM7UUFFRCxNQUFNLFFBQVEsR0FBRyxFQUFFLENBQUE7UUFDbkIsS0FBSyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBRTNCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDcEMsU0FBUTthQUNYO1lBRUQsSUFBSSxPQUFPLEdBQUcsR0FBRyxDQUFBO1lBRWpCLElBQUk7Z0JBRUEsT0FBTyxHQUFHLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFBO2FBRW5EO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBRVoseUNBQXlDO2FBRTVDO1lBRUQsSUFBSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO2FBSWxGO2lCQUNJLElBQUksT0FBTyxFQUFFO2dCQUVkLE9BQU8sQ0FBQyxTQUFTLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7Z0JBQ3ZELFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUE7YUFFMUI7U0FFSjtRQUVELElBQUksWUFBWSxHQUFHLEdBQUcsQ0FBQTtRQUV0QixJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFFdEIsWUFBWSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUE7U0FFdEM7UUFFRCxNQUFNLFlBQVksR0FBRztZQUNqQixJQUFJLENBQUMsT0FBTyxDQUNSLGFBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFDN0QsYUFBYSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUNsRSxDQUFBO1lBQ0QsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFFdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNwQyxTQUFRO2lCQUNYO2dCQUVELE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUE7Z0JBRWxDLElBQUksUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNmLE1BQU0sQ0FBQywyQkFBMkIsQ0FDOUIsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUNiLE9BQU8sQ0FBQyxJQUFJLEVBQ1osT0FBTyxDQUFDLEdBQUcsRUFDWCxPQUFPLENBQUMsS0FBSyxFQUNiLE9BQU8sQ0FBQyxNQUFNLENBQ2pCLENBQUE7aUJBQ0o7YUFDSjtZQUVELFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO1FBRXBDLENBQUMsQ0FBQTtRQUVELFlBQVksRUFBRSxDQUFBO1FBQ2QsT0FBTyxZQUFZLENBQUE7SUFFdkIsQ0FBQztJQUdELE1BQU0sQ0FBQywwQkFBMEIsQ0FBQyxJQUFnQjtRQUU5QyxJQUFJLFNBQVMsRUFBRTtZQUVYLDJCQUEyQjtZQUMzQixPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1NBRS9CO2FBQ0k7WUFFRCxNQUFNLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUE7U0FFckM7SUFFTCxDQUFDO0lBR0QsTUFBTSxDQUFDLDJCQUEyQjtRQUU5QixNQUFNLENBQUMsMEJBQTBCLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUE7SUFFakUsQ0FBQztJQUdELE1BQU0sQ0FBQyxtQkFBbUI7UUFDdEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25ELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDckMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1NBQ3hCO1FBQ0QsTUFBTSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUE7SUFDOUIsQ0FBQztJQUdELGNBQWM7UUFFVixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDcEIsT0FBTTtTQUNUO1FBRUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUE7UUFFeEIsNkNBQTZDO1FBQzdDLE1BQU0sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBRWhDLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQ25DLE1BQU0sQ0FBQywyQkFBMkIsRUFBRSxDQUFBO1NBQ3ZDO0lBRUwsQ0FBQztJQUdELElBQUksV0FBVztRQUVYLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQTtJQUU3QixDQUFDO0lBR0QsY0FBYztRQUVWLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3JCLE9BQU07U0FDVDtRQUVELElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFBO1FBRXZCLElBQUk7WUFFQSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7U0FFeEI7UUFBQyxPQUFPLFNBQVMsRUFBRTtZQUVoQixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1NBRXpCO0lBRUwsQ0FBQztJQUdELGNBQWM7UUFHVixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQTtRQUV2QixhQUFhO1FBQ2IsbUVBQW1FO1FBQ25FLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7WUFFekIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7U0FFdEc7UUFFRCw4REFBOEQ7UUFFOUQsZ0VBQWdFO1FBRWhFLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFBO1FBRXBDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFBO1FBRTVCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUUzQyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRWhDLE9BQU8sQ0FBQyx3QkFBd0IsRUFBRSxDQUFBO1lBRWxDLDJCQUEyQjtTQUU5QjtRQUVELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO0lBRTVCLENBQUM7SUFJRCxxQkFBcUI7UUFJakIseUJBQXlCO1FBRXpCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUUvQyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRXZDLElBQUksVUFBVSxFQUFFO2dCQUVaLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTthQUVqRDtZQUlELG1EQUFtRDtTQUV0RDtRQUdELGlEQUFpRDtJQUlyRCxDQUFDO0lBRUQsaUJBQWlCO1FBRWIsSUFBSSxDQUFDLGtDQUFrQyxFQUFFLENBQUE7SUFFN0MsQ0FBQztJQUVELElBQUksV0FBVztRQUNYLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQTtJQUM1QixDQUFDO0lBRUQsSUFBSSxXQUFXLENBQUMsV0FBVztRQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQTtJQUNuQyxDQUFDO0lBRUQsYUFBYSxDQUFDLFVBQVU7UUFFcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7SUFFckMsQ0FBQztJQUlELDhCQUE4QixDQUFDLGlCQUFpQjtRQUU1QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUNwRSxpQkFBaUIsRUFDakIsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQ3JCLENBQUMsQ0FBQTtJQUVOLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFFBQVE7UUFFcEcsSUFBSSxZQUFZLEdBQUcsR0FBRyxDQUFBO1FBQ3RCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQTtRQUNqQixJQUFJLElBQUksRUFBRTtZQUNOLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNsRCxZQUFZLEdBQUcsSUFBSSxDQUFBO2dCQUNuQixJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQTthQUM5QjtZQUNELE1BQU0sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFBO1NBQ25CO1FBRUQsSUFBSSxjQUFjLEdBQUcsR0FBRyxDQUFBO1FBQ3hCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQTtRQUNuQixJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksTUFBTSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNwRCxjQUFjLEdBQUcsTUFBTSxDQUFBO2dCQUN2QixNQUFNLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQTthQUNsQztZQUNELFFBQVEsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFBO1NBQ3ZCO1FBRUQsTUFBTSxVQUFVLEdBQUc7WUFFZixLQUFLLEVBQUUsTUFBTTtZQUNiLEtBQUssRUFBRSxTQUFTO1lBQ2hCLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLEtBQUssRUFBRSxRQUFRO1lBQ2YsS0FBSyxFQUFFLFdBQVc7WUFDbEIsVUFBVSxFQUFFLFVBQVU7WUFDdEIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsUUFBUSxFQUFFLFFBQVE7U0FFckIsQ0FBQTtRQUVELE9BQU8sVUFBVSxDQUFBO0lBRXJCLENBQUM7SUE2QkQsYUFBYSxDQUFDLE1BQU07UUFHaEIsSUFBSSxpQkFBaUIsR0FBRyxHQUFHLENBQUE7UUFFM0IsSUFBSTtZQUVBLGlCQUFpQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQTtTQUV2RTtRQUFDLE9BQU8sS0FBSyxFQUFFO1NBRWY7UUFFRCxJQUFJLGlCQUFpQixJQUFJLGlCQUFpQixDQUFDLE1BQU0sRUFBRTtZQUMvQyxPQUFPLGlCQUFpQixDQUFDLE1BQU0sQ0FBQTtTQUNsQztRQUNELE9BQU8sR0FBRyxDQUFBO0lBQ2QsQ0FBQztJQUlELDJCQUEyQjtRQUV2QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQTtRQUVqQyxJQUFJLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBRXRELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUUzQyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRWhDLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUE7WUFFekIsTUFBTSwyQkFBMkIsR0FBRyxPQUFPLENBQUMsMkJBQTJCLEVBQUUsQ0FBQTtZQUV6RSxLQUFLLEdBQUcsS0FBSyxDQUFDLHdCQUF3QixDQUFDLDJCQUEyQixDQUFDLENBQUE7WUFFbkUsTUFBTSxHQUFHLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUVsRDtRQUVELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFLRCxVQUFVLENBQUMsSUFBWTtRQUVuQixrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNYLE9BQU8sRUFBRSxDQUFBO1NBQ1o7UUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDM0MsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNoQyxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQ2pCLE9BQU8sR0FBRyxDQUFBO2FBQ2I7U0FDSjtRQUNELE9BQU8sRUFBRSxDQUFBO0lBQ2IsQ0FBQztJQUVELElBQUksaUJBQWlCO1FBQ2pCLE1BQU0sTUFBTSxHQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxzQkFBNkIsSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUE7UUFDeEYsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELElBQUksaUJBQWlCO1FBQ2pCLE1BQU0sTUFBTSxHQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBeUIsSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUE7UUFDcEYsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELFVBQVUsQ0FBQyxJQUFZLEVBQUUsU0FBa0I7UUFFdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBRXBDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUU5QixJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBRTtnQkFDZixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUE7Z0JBQzlGLElBQUksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7YUFDN0U7aUJBQ0k7Z0JBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO2dCQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUMzQjtZQUNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUU3QixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUUzQyxJQUFJLENBQUMsdUJBQXVCLENBQUM7b0JBRXpCLElBQUksRUFBRSxNQUFNLENBQUMsa0JBQWtCLENBQUMsZUFBZTtvQkFDL0MsVUFBVSxFQUFFLEdBQUc7aUJBRWxCLENBQUMsQ0FBQTthQUVMO1lBRUQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1NBRXhCO0lBRUwsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFlO1FBQ3ZCLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBd0IsSUFBWSxFQUFFLEtBQUssRUFBRSxLQUFLO1lBQzVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDekIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO0lBQ1osQ0FBQztJQUdELHVCQUF1QjtRQUVuQixJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFFcEIsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFBO1lBRXZELElBQUksVUFBVSxJQUFJLElBQUksRUFBRTtnQkFFcEIsT0FBTTthQUVUO1lBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFBO1lBRTNDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQTtZQUVyRCxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUE7U0FHaEc7SUFJTCxDQUFDO0lBRUQsb0JBQW9CO1FBRWhCLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUVwQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUE7WUFFbkQsSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUVqQixPQUFNO2FBRVQ7WUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUE7WUFFM0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1lBRWxDLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7U0FHbkU7SUFJTCxDQUFDO0lBR0QsbUJBQW1CO1FBQ2YsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBRXBCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLElBQUk7Z0JBRXBDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTtZQUVmLENBQUMsQ0FBQyxDQUFBO1lBRUYsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQ25ELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNaLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUE7Z0JBQ3hDLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7Z0JBQ2hFLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFBO2dCQUVwQixJQUFJLENBQUMsdUJBQXVCLENBQUM7b0JBRXpCLElBQUksRUFBRSxNQUFNLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CO29CQUNuRCxVQUFVLEVBQUUsR0FBRztpQkFFbEIsQ0FBQyxDQUFBO2FBRUw7U0FDSjtJQUNMLENBQUM7SUFHRCxVQUFVO0lBSVYsQ0FBQztJQUlELG1CQUFtQixDQUFDLFNBQWlCO1FBRWpDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFBO1FBRXJDLElBQUksQ0FBQyw4Q0FBOEMsRUFBRSxDQUFBO0lBRXpELENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxTQUFpQjtRQUVoQyxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQTtJQUU5QixDQUFDO0lBRUQsa0JBQWtCO0lBRWxCLENBQUM7SUFFRCxzQkFBc0I7SUFFdEIsQ0FBQztJQUVELElBQUksa0JBQWtCO1FBQ2xCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7UUFDbEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDNUIsSUFBSSxPQUFPLENBQUMsYUFBYSxJQUFJLE9BQU8sQ0FBQyxhQUFhLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTtnQkFDakUsT0FBTyxHQUFHLENBQUE7YUFDYjtZQUNELE9BQU8sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFBO1NBQ2xDO1FBQ0QsT0FBTyxFQUFFLENBQUE7SUFDYixDQUFDO0lBR0QsSUFBSSxhQUFhO1FBQ2IsTUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFBO1FBQ2pCLElBQUksSUFBSSxHQUFXLElBQUksQ0FBQTtRQUN2QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQ2pCLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFBO1NBQ3hCO1FBQ0QsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUdELDZCQUE2QjtRQUV6QixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQVksRUFBRSxLQUFLLEVBQUUsS0FBSztZQUVyRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFekIsQ0FBQyxDQUFDLENBQUE7SUFFTixDQUFDO0lBR0QsMEJBQTBCO1FBRXRCLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFBO1FBRXBDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtJQUV6QixDQUFDO0lBR0QsS0FBSztRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUE7SUFFaEMsQ0FBQztJQUdELElBQUk7UUFFQSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFBO0lBRS9CLENBQUM7SUFNRCxhQUFhO1FBRVQsNkJBQTZCO1FBRTdCLE1BQU0sd0JBQXdCLEdBQVksRUFBRSxJQUFLLE1BQWMsQ0FBQyxVQUFVLENBQUE7UUFFMUUsTUFBTSxVQUFVLEdBQUcsQ0FBQyxLQUFZLEVBQUUsTUFBTSxHQUFHLEVBQUUsRUFBRSxFQUFFO1lBRTdDLElBQUksSUFBSSxDQUFDLG1CQUFtQixJQUFJLE1BQU0sRUFBRTtnQkFFcEMsSUFBSSxLQUFLLENBQUMsZUFBZSxFQUFFO29CQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUE7aUJBQzFCO2dCQUNELElBQUksS0FBSyxDQUFDLGNBQWMsRUFBRTtvQkFDdEIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFBO2lCQUN6QjtnQkFDRCxLQUFLLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQTtnQkFDekIsS0FBSyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUE7Z0JBQ3pCLE9BQU8sS0FBSyxDQUFBO2FBRWY7WUFFRCxJQUFJLEtBQUssQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLDRCQUE0QixFQUFFO2dCQUM1RCxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUE7YUFDMUI7UUFFTCxDQUFDLENBQUE7UUFFRCxNQUFNLFdBQVcsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBRTFCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLHdCQUF3QixJQUFJLEtBQUssWUFBWSxVQUFVLENBQUM7Z0JBQ2hGLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7b0JBQzNGLEtBQUssWUFBWSxVQUFVLENBQUMsRUFBRTtnQkFDbEMsT0FBTTthQUNUO1lBRUQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFBO1lBRW5FLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUE7WUFDM0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUE7WUFDMUIsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1lBQ3hFLElBQUksd0JBQXdCLElBQUksS0FBSyxZQUFZLFVBQVUsRUFBRTtnQkFFekQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUE7Z0JBRWpDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFBO2dCQUU5RixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFFMUIsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFBO29CQUVwQixPQUFNO2lCQUVUO2FBR0o7aUJBQ0k7Z0JBRUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUE7Z0JBRTFCLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTthQUVwQjtZQUdELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUE7UUFFaEMsQ0FBQyxDQUFBO1FBRUQsTUFBTSxZQUFZLEdBQUcsV0FBa0IsQ0FBQTtRQUV2QyxNQUFNLFNBQVMsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBRXhCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN2QixPQUFNO2FBQ1Q7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSx3QkFBd0IsSUFBSSxLQUFLLFlBQVksVUFBVSxDQUFDO2dCQUNoRixDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxZQUFZLFVBQVUsQ0FBQyxFQUFFO2dCQUNwRCxPQUFNO2FBQ1Q7WUFFRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFFdkIsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUE7Z0JBRXhCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7b0JBRTFCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQTtpQkFFckU7YUFHSjtZQUVELHdHQUF3RztZQUN4RyxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUE7WUFFakUsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRXJCLENBQUMsQ0FBQTtRQUVELE1BQU0sVUFBVSxHQUFHLFNBQVMsQ0FBQTtRQUU1QixpQ0FBaUM7UUFFakMsNEVBQTRFO1FBRTVFLG1DQUFtQztRQUVuQyx5QkFBeUI7UUFFekIsSUFBSTtRQUVKLE1BQU0sVUFBVSxHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFFekIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksd0JBQXdCLElBQUksS0FBSyxZQUFZLFVBQVUsQ0FBQztnQkFDaEYsQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssWUFBWSxVQUFVLENBQUMsRUFBRTtnQkFDcEQsT0FBTTthQUNUO1lBRUQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFBO1lBRXBFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUE7WUFFMUIsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRXJCLENBQUMsQ0FBQTtRQUVELE1BQU0sWUFBWSxHQUFHLFVBQVUsQ0FBQTtRQUUvQixJQUFJLGFBQWEsR0FBRyxVQUFVLEtBQUs7WUFFL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3ZCLE9BQU07YUFDVDtZQUVELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLHdCQUF3QixJQUFJLEtBQUssWUFBWSxVQUFVLENBQUM7Z0JBQ2hGLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxLQUFLLFlBQVksVUFBVSxDQUFDLEVBQUU7Z0JBQ3BELE9BQU07YUFDVDtZQUVELElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFBO1lBRXpCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUV6RSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBRVosTUFBTSxXQUFXLEdBQUcsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUUxQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSx3QkFBd0IsSUFBSSxLQUFLLFlBQVksVUFBVSxDQUFDO2dCQUNoRixDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxZQUFZLFVBQVUsQ0FBQyxFQUFFO2dCQUNwRCxPQUFNO2FBQ1Q7WUFFRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUE7WUFFcEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQTtZQUUzQixJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQTtZQUUxQixVQUFVLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFckIsQ0FBQyxDQUFBO1FBRUQsTUFBTSxXQUFXLEdBQUcsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUUxQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDdkIsT0FBTTthQUNUO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksd0JBQXdCLElBQUksS0FBSyxZQUFZLFVBQVUsQ0FBQztnQkFDaEYsQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssWUFBWSxVQUFVLENBQUMsRUFBRTtnQkFDcEQsT0FBTTthQUNUO1lBRUQsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUU7Z0JBRXRDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQTthQUUzRTtZQUVELElBQUksSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLE1BQU07Z0JBQ2pGLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtnQkFFNUIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEdBQUcsQ0FBQTthQUVoQztZQUdELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQTtZQUVuRSxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFckIsQ0FBQyxDQUFBO1FBRUQsTUFBTSxXQUFXLEdBQUcsVUFBVSxLQUFpQjtZQUUzQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDdkIsT0FBTTthQUNUO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksd0JBQXdCLElBQUksS0FBSyxZQUFZLFVBQVUsQ0FBQztnQkFDaEYsQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssWUFBWSxVQUFVLENBQUMsRUFBRTtnQkFDcEQsT0FBTTthQUNUO1lBRUQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBRTFCLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtnQkFFbEIsT0FBTTthQUVUO1lBRUQsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUU5QixJQUFJLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxNQUFNO2dCQUNqRixJQUFJLENBQUMscUJBQXFCLEVBQUU7Z0JBRTVCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxHQUFHLENBQUE7YUFFaEM7WUFHRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZUFBZTtnQkFDN0MsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUV6RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFBO2dCQUUxQixJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUE7YUFFdkU7WUFHRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUE7WUFFbkUsb0JBQW9CO1FBR3hCLENBQUMsQ0FBQTtRQUVELElBQUksV0FBVyxHQUFHLFNBQVMsV0FBVyxDQUFDLEtBQWlCO1lBRXBELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUUzRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBR1osSUFBSSxpQkFBaUIsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQzlCLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUVqQixJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUE7UUFDM0UsQ0FBQyxDQUFBO1FBRUQsU0FBUyxlQUFlLENBQUMsS0FBSztZQUMxQixJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssRUFBRSxFQUFFO2dCQUN0QixPQUFPLEVBQUUsQ0FBQTthQUNaO1lBQ0QsT0FBTyxHQUFHLENBQUE7UUFDZCxDQUFDO1FBRUQsU0FBUyxhQUFhLENBQUMsS0FBSztZQUN4QixJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssQ0FBQyxFQUFFO2dCQUNyQixPQUFPLEVBQUUsQ0FBQTthQUNaO1lBQ0QsT0FBTyxHQUFHLENBQUE7UUFDZCxDQUFDO1FBRUQsU0FBUyxhQUFhLENBQUMsS0FBSztZQUN4QixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUE7WUFDbEIsSUFBSSxLQUFLLElBQUksS0FBSyxFQUFFO2dCQUNoQixNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxDQUFBO2FBQ3pEO2lCQUNJO2dCQUNELE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLENBQUE7YUFDakM7WUFDRCxPQUFPLE1BQU0sQ0FBQTtRQUNqQixDQUFDO1FBRUQsU0FBUyxjQUFjLENBQUMsS0FBSztZQUN6QixJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUN2QixPQUFPLEVBQUUsQ0FBQTthQUNaO1lBQ0QsT0FBTyxHQUFHLENBQUE7UUFDZCxDQUFDO1FBRUQsU0FBUyxlQUFlLENBQUMsS0FBSztZQUMxQixJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUN2QixPQUFPLEVBQUUsQ0FBQTthQUNaO1lBQ0QsT0FBTyxHQUFHLENBQUE7UUFDZCxDQUFDO1FBRUQsU0FBUyxjQUFjLENBQUMsS0FBSztZQUN6QixJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUN2QixPQUFPLEVBQUUsQ0FBQTthQUNaO1lBQ0QsT0FBTyxHQUFHLENBQUE7UUFDZCxDQUFDO1FBRUQsU0FBUyxZQUFZLENBQUMsS0FBSztZQUN2QixJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUN2QixPQUFPLEVBQUUsQ0FBQTthQUNaO1lBQ0QsT0FBTyxHQUFHLENBQUE7UUFDZCxDQUFDO1FBRUQsTUFBTSxTQUFTLEdBQUcsVUFBVSxLQUFLO1lBRTdCLElBQUksZUFBZSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUV4QixJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUE7YUFFcEU7WUFFRCxJQUFJLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFFdEIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO2FBRWxFO1lBRUQsSUFBSSxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtnQkFFdkcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO2dCQUUvRCxVQUFVLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFBO2FBRXpCO1lBRUQsSUFBSSxjQUFjLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBRXZCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQTthQUV4RTtZQUVELElBQUksZUFBZSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUV4QixJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUE7YUFFekU7WUFFRCxJQUFJLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFFdkIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFBO2FBRXhFO1lBRUQsSUFBSSxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBRXJCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQTthQUV0RTtRQUVMLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFWixNQUFNLE9BQU8sR0FBRyxVQUFVLEtBQUs7WUFFM0IsSUFBSSxlQUFlLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBRXhCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQTthQUVsRTtRQUVMLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFHWixNQUFNLE9BQU8sR0FBRyxVQUFVLEtBQVk7WUFFbEMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBRWpFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFWixNQUFNLE1BQU0sR0FBRyxVQUFVLEtBQVk7WUFFakMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBRWhFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFHWiwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzFELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUM1RCxvRkFBb0Y7UUFDcEYsc0ZBQXNGO1FBQ3RGLHlGQUF5RjtRQUV6Riw4QkFBOEI7UUFDOUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzFELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUMxRCxvRkFBb0Y7UUFDcEYsb0ZBQW9GO1FBRXBGLHFGQUFxRjtRQUVyRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDMUQsb0ZBQW9GO1FBRXBGLDZCQUE2QjtRQUM3QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDdEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ3hELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUM5RCxnRkFBZ0Y7UUFDaEYsa0ZBQWtGO1FBQ2xGLHdGQUF3RjtRQUV4RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDeEQsa0ZBQWtGO1FBQ2xGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUVwRiw2Q0FBNkM7UUFDN0MseUNBQXlDO1FBQ3pDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ25FLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBRS9ELGVBQWU7UUFDZixJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQTtRQUN2QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtRQUNyQyxnRUFBZ0U7UUFDaEUsOERBQThEO0lBR2xFLENBQUM7SUFtQ0QsSUFBSSxxQkFBcUI7UUFFckIsTUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFBO1FBR3BCLE1BQU0sTUFBTSxHQUFrRSxJQUFJLEtBQUssQ0FDbEYsSUFBSSxDQUFDLFdBQW1CLENBQUMsWUFBWSxFQUN0QztZQUVJLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEVBQUU7Z0JBRTNCLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7Z0JBRW5CLE9BQU8sTUFBTSxDQUFBO1lBRWpCLENBQUM7WUFDRCxHQUFHLEVBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsRUFBRTtnQkFFbEMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtnQkFFbkIsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQTtnQkFFaEQsT0FBTyxJQUFJLENBQUE7WUFFZixDQUFDO1NBRUosQ0FDSixDQUFBO1FBRUQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQU1ELHlCQUF5QixDQUFDLFNBQW1CLEVBQUUsY0FBc0Q7UUFFakcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUF3QixHQUFXLEVBQUUsS0FBYSxFQUFFLEtBQWU7WUFFakYsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsRUFBRSxjQUFjLENBQUMsQ0FBQTtRQUV0RCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7SUFFWixDQUFDO0lBSUQsd0JBQXdCLENBQUMsUUFBZ0IsRUFBRSxjQUFzRDtRQUU3RixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUE7UUFFakQsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNWLGFBQWE7WUFDYixPQUFPLEdBQUcsRUFBRSxDQUFBO1lBQ1osSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxHQUFHLE9BQU8sQ0FBQTtTQUNoRDtRQUVELElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtZQUN2QyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1NBQy9CO0lBRUwsQ0FBQztJQUVELDJCQUEyQixDQUFDLFFBQWdCLEVBQUUsY0FBc0Q7UUFDaEcsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQ25ELElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDVixPQUFNO1NBQ1Q7UUFDRCxNQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQzdDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxFQUFFO1lBQ2IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUE7U0FDM0I7SUFDTCxDQUFDO0lBRUQsNEJBQTRCLENBQUMsU0FBbUIsRUFBRSxjQUFzRDtRQUVwRyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLO1lBRXpDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxHQUFHLEVBQUUsY0FBYyxDQUFDLENBQUE7UUFFekQsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO0lBRVosQ0FBQztJQUVELHNCQUFzQixDQUFDLFFBQWdCLEVBQUUsV0FBa0I7UUFDdkQsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQ2pELElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDVixPQUFNO1NBQ1Q7UUFDRCxPQUFPLEdBQUcsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFBO1FBQ3hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JDLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN6QixNQUFNLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFBO1NBQzVCO0lBQ0wsQ0FBQztJQWdCRCx1QkFBdUIsQ0FBQyxLQUEyQjtRQUUvQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsVUFBVSxJQUFJO1lBRXBDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUVwQyxJQUFJLElBQUksQ0FBQyx5Q0FBeUMsRUFBRTtnQkFFaEQsSUFBSSxDQUFDLHlDQUF5QyxDQUFDLEtBQUssQ0FBQyxDQUFBO2FBRXhEO1FBRUwsQ0FBQyxDQUFDLENBQUE7SUFHTixDQUFDO0lBRUQsd0JBQXdCLENBQUMsS0FBMkI7UUFFaEQsSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEVBQUU7WUFFdkQsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUE7U0FFNUI7UUFFRCxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRTtZQUV6RCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtTQUU1QjtRQUVELElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLEVBQUU7WUFFN0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUE7U0FFaEM7UUFFRCxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsSUFBSTtZQUNyRSxNQUFNLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFO1lBRTNDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFBO1lBRXJDLElBQUksQ0FBQyw4Q0FBOEMsRUFBRSxDQUFBO1NBRXhEO0lBSUwsQ0FBQztJQU1ELG9CQUFvQixDQUFDLGNBQXNDO1FBRXZELGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUVwQixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSztZQUVqRCxPQUFPLENBQUMsb0JBQW9CLENBQUMsY0FBYyxDQUFDLENBQUE7UUFFaEQsQ0FBQyxDQUFDLENBQUE7SUFFTixDQUFDO0lBTUQsZUFBZSxDQUFDLFNBQXNCLEVBQUUsSUFBWTtRQUNoRCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3RELE9BQU8sR0FBRyxDQUFBO1NBQ2I7UUFFRCxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLEVBQUUsQ0FBQTtRQUN4RSxNQUFNLFlBQVksR0FBRyxJQUFJLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUE7UUFFbkYsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixFQUFFLENBQUE7UUFDeEUsTUFBTSxZQUFZLEdBQUcsSUFBSSxPQUFPLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBRW5GLE1BQU0sV0FBVyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUE7UUFFdkQsT0FBTyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0lBQ3RELENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxTQUFzQixFQUFFLElBQVk7UUFDbEQsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQTtJQUNoRCxDQUFDO0lBTUQsbUNBQW1DLENBQUMscUJBQTZCLENBQUMsRUFBRSxvQkFBNEIsQ0FBQztRQUU3Rix5QkFBeUI7UUFFekIsTUFBTSxNQUFNLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7UUFDMUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixFQUFFO1lBQ3RDLE9BQU8sTUFBTSxDQUFBO1NBQ2hCO1FBRUQsSUFBSSxxQkFBcUIsR0FBRyxFQUFFLENBQUE7UUFDOUIsSUFBSSxpQkFBdUIsQ0FBQTtRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtZQUMvQyxxQkFBcUIsR0FBRyxHQUFHLENBQUE7WUFDM0IsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUE7U0FDdkQ7UUFFRCxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQTtRQUNoQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQTtRQUU5QixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsa0JBQWtCLENBQUE7UUFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsRUFBRSxHQUFHLGlCQUFpQixDQUFBO1FBR3pDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFBO1FBQzVCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFBO1FBQzlCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFBO1FBQ2hDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFBO1FBRTFCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQTtRQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUE7UUFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO1FBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQTtRQUduQixNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQTtRQUd0RCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQTtRQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7UUFFaEMsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUE7UUFFcEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO1FBSWxDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtRQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUE7UUFFeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFBO1FBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQTtRQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUE7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFBO1FBRXBCLElBQUkscUJBQXFCLEVBQUU7WUFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1lBQy9DLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDaEIsSUFBSSxpQkFBaUIsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsaUJBQWlCLENBQUMsQ0FBQTtpQkFDdkY7cUJBQ0k7b0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtpQkFDbkU7YUFDSjtTQUNKO1FBRUQsTUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUE7UUFDNUIsTUFBTSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUE7UUFHMUIsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQU1ELHFCQUFxQixDQUFDLHFCQUE2QixDQUFDO1FBRWhELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEtBQUssQ0FBQTtRQUVqRixPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBRUQsc0JBQXNCLENBQUMsb0JBQTRCLENBQUM7UUFFaEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLG1DQUFtQyxDQUFDLFNBQVMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQTtRQUU1RixPQUFPLE1BQU0sQ0FBQTtJQUdqQixDQUFDO0lBRUQsb0JBQW9CO1FBRWhCLE9BQU8sR0FBRyxDQUFBO0lBRWQsQ0FBQzs7QUFuakZNLG1CQUFZLEdBQVcsQ0FBQyxDQUFDLENBQUE7QUFHekIscUJBQWMsR0FBYSxFQUFFLENBQUE7QUFLN0IsaUJBQVUsR0FBRyxDQUFDLENBQUE7QUErbkNkLDBCQUFtQixHQUFHLENBQUMsQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7SUFDcEcsQ0FBQyxDQUFDLG1CQUFtQixJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7SUFDN0YsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7SUFDdkYsQ0FBQyxDQUFDLGVBQWUsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztJQUNyRixDQUFDLENBQUMsY0FBYyxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUE7QUF1WGhGLDBCQUFtQixHQUFHO0lBRXpCLE1BQU0sRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUk7SUFDakMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsS0FBSztJQUNuQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNO0lBQ3JDLEtBQUssRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUc7SUFDL0IsU0FBUyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsT0FBTztJQUN2QyxTQUFTLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxPQUFPO0lBQ3ZDLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLE1BQU07SUFDckMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsS0FBSztJQUNuQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNO0lBQ3JDLDhCQUE4QjtJQUM5QixVQUFVLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFjO0lBQy9DLFVBQVUsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVE7Q0FFNUMsQ0FBQTtBQUVNLHlCQUFrQixHQUFHO0lBRXhCLE9BQU8sRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUc7SUFDaEMsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHO0lBQzFDLG9CQUFvQixFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRztDQUVoRCxDQUFBO0FBMnNCYSxtQkFBWSxHQUFHO0lBRXpCLGFBQWEsRUFBRSxhQUFhO0lBQzVCLGFBQWEsRUFBRSxhQUFhO0lBQzVCLGNBQWMsRUFBRSxjQUFjO0lBQzlCLGNBQWMsRUFBRSxjQUFjO0lBQzlCLGlCQUFpQixFQUFFLGlCQUFpQjtJQUNwQyxZQUFZLEVBQUUsWUFBWTtJQUMxQixXQUFXLEVBQUUsV0FBVztJQUN4QixpQkFBaUIsRUFBRSxhQUFhO0lBQ2hDLGVBQWUsRUFBRSxlQUFlO0lBQ2hDLGNBQWMsRUFBRSxjQUFjO0lBQzlCLFdBQVcsRUFBRSxXQUFXO0lBQ3hCLFNBQVMsRUFBRSxTQUFTO0lBQ3BCLFNBQVMsRUFBRSxTQUFTO0lBQ3BCLFNBQVMsRUFBRSxTQUFTO0lBQ3BCLGVBQWUsRUFBRSxlQUFlO0lBQ2hDLGdCQUFnQixFQUFFLGdCQUFnQjtJQUNsQyxlQUFlLEVBQUUsZUFBZTtJQUNoQyxhQUFhLEVBQUUsYUFBYTtJQUM1QixPQUFPLEVBQUUsT0FBTztJQUNoQixNQUFNLEVBQUUsTUFBTTtDQUVqQixDQUFBO0FBNEdNLHlCQUFrQixHQUFHO0lBRXhCLGlCQUFpQixFQUFFLGlCQUFpQjtJQUNwQyxxQkFBcUIsRUFBRSxxQkFBcUI7SUFDNUMsaUJBQWlCLEVBQUUsaUJBQWlCO0lBQ3BDLGVBQWUsRUFBRSxlQUFlO0NBRW5DLENBQUE7QUN2L0VMLG9DQUFvQztBQUNwQyxzQ0FBc0M7QUFNdEMsTUFBTSxnQkFBaUIsU0FBUSxRQUFRO0lBV25DLFlBQW1CLElBQVk7UUFFM0IsS0FBSyxFQUFFLENBQUE7UUFGUSxTQUFJLEdBQUosSUFBSSxDQUFRO1FBSzNCLElBQUksQ0FBQywwQkFBMEIsRUFBRSxDQUFBO1FBQ2pDLElBQUksQ0FBQyxzQ0FBc0MsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQTtRQUM5RCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQTtRQUU3QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7UUFDbkIsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUE7UUFDNUIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUE7UUFDdkIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUE7SUFJOUIsQ0FBQztJQUlELDBCQUEwQjtRQUV0QixJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFBO1FBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFBO0lBRzlCLENBQUM7SUFFRCxzQkFBc0I7UUFJbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsc0NBQXNDLENBQUMsSUFBSSxDQUFBO1FBRTVELElBQUksQ0FBQyxJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUM3RSxJQUFJLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDcEYsSUFBSSxDQUFDLElBQUksQ0FBQyx5Q0FBeUMsR0FBRyxJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBRWxHLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLENBQUE7UUFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQTtJQUluQyxDQUFDO0lBTUQsc0JBQXNCLENBQUMsS0FBYztRQUVqQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRXZCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsVUFBVSxVQUFVLEVBQUUsS0FBSyxFQUFFLEtBQUs7WUFFaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUU7Z0JBQ2xCLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTthQUMzQztRQUVMLENBQUMsQ0FBQyxDQUFBO0lBRU4sQ0FBQztJQUVLLFdBQVcsQ0FBQyxLQUFjOztRQUloQyxDQUFDO0tBQUE7SUFJRCxZQUFZO0lBTVosQ0FBQztJQUVLLGNBQWM7O1FBSXBCLENBQUM7S0FBQTtJQUdLLGFBQWE7O1FBSW5CLENBQUM7S0FBQTtJQUdLLGlCQUFpQjs7UUFJdkIsQ0FBQztLQUFBO0lBRUssZ0JBQWdCOztRQUl0QixDQUFDO0tBQUE7SUFHRCxxQkFBcUI7SUFNckIsQ0FBQztJQUVELGdCQUFnQjtJQU1oQixDQUFDO0lBRUQsbUJBQW1CO0lBSW5CLENBQUM7SUFFRCxtQkFBbUI7UUFFZixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBRTFCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFBO0lBRWhDLENBQUM7SUFJRCxxQkFBcUI7UUFFakIsZ0ZBQWdGO1FBRWhGLHdDQUF3QztRQUV4QyxLQUFLO0lBSVQsQ0FBQztJQUlELDRCQUE0QixDQUFDLEtBQTJCO0lBSXhELENBQUM7SUFLRCxzQkFBc0IsQ0FBQyxjQUFjO1FBRWpDLGtDQUFrQztRQUNsQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQ3JCLE9BQU8sRUFBRSxDQUFBO1NBQ1o7UUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUV2RCxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUV4RCxJQUFJLG1CQUFtQixJQUFJLGNBQWMsRUFBRTtnQkFDdkMsT0FBTyxHQUFHLENBQUE7YUFDYjtTQUVKO1FBRUQsT0FBTyxFQUFFLENBQUE7SUFFYixDQUFDO0lBRUQsc0JBQXNCLENBQUMsY0FBYztRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQzlDLGNBQWMsQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUNuRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1lBQzlDLDRDQUE0QztZQUM1QyxxREFBcUQ7U0FDeEQ7SUFDTCxDQUFDO0lBR0QsOEJBQThCO1FBQzFCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDMUUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDWixJQUFJLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQTtZQUMvRCxrQ0FBa0M7WUFDbEMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQTtTQUNsQztJQUNMLENBQUM7SUFFRCw4QkFBOEIsQ0FBQyxvQkFBb0I7SUFFbkQsQ0FBQztJQUdELDZCQUE2QixDQUFDLG9CQUFvQjtRQUU5QyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsb0JBQW9CLENBQUE7SUFFcEQsQ0FBQztJQUVELHlCQUF5QixDQUFDLFVBQTRCO1FBRWxELFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO1FBQzlCLElBQUksRUFBRSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO1lBQ3JDLFVBQVUsQ0FBQyw4QkFBOEIsRUFBRSxDQUFBO1NBQzlDO1FBQ0QsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JCLFVBQVUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtTQUN4QztRQUNELFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO0lBRWpDLENBQUM7SUFFRCxpQ0FBaUMsQ0FBQyxVQUE0QixFQUFFLGFBQXFCO1FBRWpGLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUMzQixJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDdkMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDekMsVUFBVSxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzlDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQTtJQUU5QixDQUFDO0lBRUQsa0NBQWtDLENBQUMsVUFBNEIsRUFBRSxVQUF3QjtRQUVyRixVQUFVLENBQUMsY0FBYyxFQUFFLENBQUE7UUFDM0IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBQ3ZDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQTtRQUVqQyxJQUFJLHVCQUF1QixHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBRWpFLFVBQVUsQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLEVBQUU7WUFFNUIsdUJBQXVCLENBQUMsUUFBUSxDQUFDLENBQUE7WUFFakMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBRTlDLENBQUMsQ0FBQTtRQUVELFVBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUM5QyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUE7SUFFOUIsQ0FBQztDQU1KO0FDdFJELE1BQU0sWUFBK0MsU0FBUSxNQUFNO0lBZ0IvRCxZQUFZLFNBQWtCLEVBQUUsZUFBNkI7UUFHekQsS0FBSyxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQTtRQWZyQyxVQUFLLEdBQWEsR0FBRyxDQUFBO1FBSXJCLHNCQUFpQixHQUFXLElBQUksQ0FBQTtRQUVoQyxZQUFPLEdBQVcsR0FBRyxDQUFBO1FBRXJCLGNBQVMsR0FBWSxFQUFFLENBQUE7UUFFdkIsMEJBQXFCLEdBQUcsR0FBRyxDQUFBO1FBT3ZCLElBQUksQ0FBQyx3QkFBd0IsQ0FDekIsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQzlCLFVBQThCLE1BQWMsRUFBRSxLQUFZO1lBRXRELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUE7UUFFM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDZixDQUFBO1FBRUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFBLENBQUMsa0RBQWtEO1FBRS9ILElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtJQUU5QixDQUFDO0lBSUQsbUJBQW1CLENBQUMsTUFBYyxFQUFFLEtBQVk7UUFFNUMsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQ3BFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUE7U0FDdkM7SUFFTCxDQUFDO0lBR0QsSUFBSSxNQUFNLENBQUMsTUFBYztRQUVyQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQTtRQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsTUFBTSxDQUFBO0lBRW5DLENBQUM7SUFFRCxJQUFJLE1BQU07UUFFTixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUE7SUFFdkIsQ0FBQztJQUlELElBQUksSUFBSSxDQUFDLElBQWM7UUFFbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO1FBRWhDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO1FBRWpCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7SUFFekIsQ0FBQztJQUdELElBQUksSUFBSTtRQUVKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQTtJQUVyQixDQUFDO0lBSUQsVUFBVSxDQUFDLFdBQW9CLEVBQUU7UUFFN0IsSUFBSSxRQUFRLEVBQUU7WUFFVixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUE7U0FFM0I7UUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFFdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUE7SUFFdEIsQ0FBQztJQUdELGdCQUFnQjtRQUVaLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQTtJQUU1QixDQUFDO0lBRUQsbUJBQW1CO1FBRWYsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFBO0lBRTVCLENBQUM7SUFJRCxVQUFVLENBQUMsYUFBcUIsRUFBRSxRQUFpQjtRQUcvQyxRQUFRLEdBQUcsQ0FBQyxRQUFRLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUVwQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFBO1FBRWpDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUE7UUFHekIsYUFBYSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUU5QixJQUFJLFFBQVEsRUFBRTtZQUVWLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUVyQixNQUFNLENBQUMsOENBQThDLENBQ2pELElBQUksRUFDSixJQUFJLENBQUMsaUJBQWlCLEVBQ3RCLENBQUMsRUFDRCxTQUFTLEVBQ1Q7Z0JBR0ksSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUE7WUFHM0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDWixHQUFHLENBQ04sQ0FBQTtTQUdKO2FBQ0k7WUFFRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7U0FFeEI7UUFFRCxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQTtJQUV4QixDQUFDO0lBSUQsY0FBYyxDQUFDLFFBQWlCO1FBRTVCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUE7SUFFbEUsQ0FBQztJQUlELE9BQU8sQ0FBQyxRQUFrQjtRQUd0QixRQUFRLEdBQUcsQ0FBQyxRQUFRLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUVwQyxJQUFJLFFBQVEsSUFBSSxTQUFTLEVBQUU7WUFFdkIsUUFBUSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQTtTQUVwQztRQUVELElBQUksUUFBUSxFQUFFO1lBRVYsTUFBTSxDQUFDLDhDQUE4QyxDQUNqRCxJQUFJLEVBQ0osSUFBSSxDQUFDLGlCQUFpQixFQUN0QixDQUFDLEVBQ0QsU0FBUyxFQUNUO2dCQUVJLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO1lBRTlCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQ1o7Z0JBRUksSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtvQkFFdEIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUE7aUJBRTdCO1lBRUwsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDZixDQUFBO1NBRUo7YUFDSTtZQUVELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO1NBRTdCO1FBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUE7SUFFdkIsQ0FBQztJQU1ELHdCQUF3QixDQUFDLEtBQTJCO1FBRWhELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUVyQyxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRTtZQUV6RCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7U0FFeEI7SUFFTCxDQUFDO0lBTUQsY0FBYztRQUdWLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBRWhCLE9BQU07U0FFVDtRQUVELHFDQUFxQztRQUVyQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsTUFBTSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUV2RCxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRTFCLE1BQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQTtRQUVqQixnQ0FBZ0M7UUFFaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQTtRQUVyQyxxRkFBcUY7UUFDckYsbUZBQW1GO1FBS25GLGlFQUFpRTtRQUNqRSwwRkFBMEY7UUFFMUYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFBO0lBRTFCLENBQUM7Q0FNSjtBQzNRRCxvQ0FBb0M7QUFNcEMsTUFBTSxZQUFhLFNBQVEsTUFBTTtJQWdCN0IsWUFBWSxTQUFpQixFQUFFLFdBQW9CLEVBQUUsWUFBa0I7UUFFbkUsS0FBSyxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFBO1FBaEJwRCxjQUFTLEdBQVksRUFBRSxDQUFBO1FBQ3ZCLGlCQUFZLEdBQVksRUFBRSxDQUFBO1FBSzFCLGtCQUFhLEdBQVksRUFBRSxDQUFBO1FBWXZCLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFBO1FBQ3RCLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFBO1FBRXhCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFBO0lBRS9CLENBQUM7SUFNRCxvQkFBb0I7UUFFaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFBO1FBRTlCLHFCQUFxQjtRQUdyQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFBO1FBRzFCLE1BQU0sVUFBVSxHQUFHO1lBQ2YsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUE7UUFDdEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUNaLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQTtRQUUzRSxNQUFNLGFBQWEsR0FBRztZQUVsQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQTtRQUVyQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBRVosSUFBSSxDQUFDLHlCQUF5QixDQUFDO1lBQzNCLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZTtTQUMzRyxFQUFFLGFBQWEsQ0FBQyxDQUFBO1FBR2pCLElBQUksZ0JBQWdCLENBQUE7UUFDcEIsTUFBTSxjQUFjLEdBQUc7WUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUE7WUFDdEIsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFBO1FBQ2pDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDWixJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsY0FBYyxDQUFDLENBQUE7UUFDOUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLGNBQWMsQ0FBQyxDQUFBO1FBRS9FLE1BQU0saUJBQWlCLEdBQUc7WUFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUE7UUFDekIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUNaLE1BQU0sb0NBQW9DLEdBQUc7WUFDekMsTUFBTSw2QkFBNkIsR0FBRyxFQUFFLENBQUE7WUFDeEMsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLGdCQUFnQixDQUFBO1lBQ2pELElBQUksNkJBQTZCLEdBQUcsV0FBVyxFQUFFO2dCQUM3QyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQTthQUN4QjtpQkFDSTtnQkFDRCxVQUFVLENBQUM7b0JBQ1AsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUE7Z0JBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsNkJBQTZCLEdBQUcsV0FBVyxDQUFDLENBQUE7YUFDN0Q7UUFDTCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ1osSUFBSSxDQUFDLHlCQUF5QixDQUFDO1lBQzNCLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZTtTQUMzRyxFQUFFLGlCQUFpQixDQUFDLENBQUE7UUFDckIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLG9DQUFvQyxDQUFDLENBQUE7UUFFbEcseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRTtZQUV6RCxjQUFjLEVBQUUsQ0FBQTtZQUNoQixvQ0FBb0MsRUFBRSxDQUFBO1FBRTFDLENBQUMsQ0FBQyxDQUFBO1FBR0YsSUFBSSxDQUFDLHdCQUF3QixDQUN6QixNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFDekIsVUFBOEIsTUFBYyxFQUFFLEtBQVk7WUFFdEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUE7UUFFdEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDZixDQUFBO1FBRUQsSUFBSSxDQUFDLHdCQUF3QixDQUN6QixNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFDeEIsVUFBOEIsTUFBYyxFQUFFLEtBQVk7WUFFdEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUE7UUFFckIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDZixDQUFBO1FBR0QsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7UUFFbkMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEdBQUcsQ0FBQTtRQUM5QixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQTtRQUVqQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUE7UUFFN0IsOEJBQThCO1FBRzlCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxFQUFFLENBQUE7UUFHaEMsSUFBSSxDQUFDLHlCQUF5QixDQUFDO1lBQzNCLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZTtTQUNyRSxFQUFFLFVBQThCLE1BQU0sRUFBRSxLQUFLO1lBRTFDLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFFbkIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUE7YUFFN0I7UUFFTCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7SUFHakIsQ0FBQztJQU1ELElBQVcsT0FBTyxDQUFDLE9BQWdCO1FBQy9CLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFBO1FBQ3ZCLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFBO0lBQ3ZDLENBQUM7SUFFRCxJQUFXLE9BQU87UUFDZCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUE7SUFDeEIsQ0FBQztJQUVELElBQVcsV0FBVyxDQUFDLFdBQW9CO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFBO1FBQy9CLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFBO0lBQ3ZDLENBQUM7SUFFRCxJQUFXLFdBQVc7UUFDbEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFBO0lBQzVCLENBQUM7SUFFRCxJQUFXLE9BQU8sQ0FBQyxPQUFnQjtRQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQTtRQUN2QixJQUFJLE9BQU8sRUFBRTtZQUNULElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtTQUNmO2FBQ0k7WUFDRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7U0FDZDtRQUNELElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFBO0lBQ3ZDLENBQUM7SUFFRCxJQUFXLE9BQU87UUFDZCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUE7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUSxDQUFDLFFBQWlCO1FBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFBO1FBQ3pCLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFBO0lBQ3ZDLENBQUM7SUFFRCxJQUFXLFFBQVE7UUFDZixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUE7SUFDekIsQ0FBQztJQU1ELDRCQUE0QjtRQUV4QixJQUFJLGNBQWMsR0FBYSxJQUFJLENBQUMsMkJBQTJCLENBQUE7UUFDL0QsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbkMsY0FBYyxHQUFHLElBQUksQ0FBQywyQ0FBMkMsQ0FBQTtTQUNwRTthQUNJLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNwQixjQUFjLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFBO1NBQ3REO2FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ25CLGNBQWMsR0FBRyxJQUFJLENBQUMsNEJBQTRCLENBQUE7U0FDckQ7YUFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDdkIsY0FBYyxHQUFHLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQTtTQUN6RDthQUNJLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNuQixjQUFjLEdBQUcsSUFBSSxDQUFDLDRCQUE0QixDQUFBO1NBQ3JEO1FBRUQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsRUFBRTtZQUNyQixJQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUE7U0FDMUM7YUFDSTtZQUNELGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7U0FDNUI7SUFFTCxDQUFDO0lBRUQsMkJBQTJCO0lBSTNCLENBQUM7SUFFRCw0QkFBNEI7UUFFeEIsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUE7SUFFdEMsQ0FBQztJQUVELDRCQUE0QjtRQUV4QixJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQTtJQUV2QyxDQUFDO0lBRUQsZ0NBQWdDO0lBSWhDLENBQUM7SUFFRCw2QkFBNkI7SUFJN0IsQ0FBQztJQUVELDJDQUEyQztRQUV2QyxJQUFJLENBQUMsNkJBQTZCLEVBQUUsQ0FBQTtJQUV4QyxDQUFDO0lBR0QsSUFBSSxPQUFPLENBQUMsT0FBZ0I7UUFFeEIsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUE7UUFFdkIsSUFBSSxDQUFDLG1DQUFtQyxFQUFFLENBQUE7SUFFOUMsQ0FBQztJQUVELElBQUksT0FBTztRQUVQLE9BQU8sS0FBSyxDQUFDLE9BQU8sQ0FBQTtJQUV4QixDQUFDO0lBRUQsbUNBQW1DO1FBRS9CLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFBO1NBQ2pCO2FBQ0k7WUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQTtTQUNuQjtRQUVELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFBO0lBRTlDLENBQUM7SUFJRCxhQUFhLENBQUMsY0FBc0I7UUFFaEMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQTtRQUVuQyxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksY0FBYyxFQUFFO1lBRXZDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7U0FFL0M7SUFFTCxDQUFDO0lBTUQsd0JBQXdCLENBQUMsS0FBMkI7UUFFaEQsS0FBSyxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRXJDLElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsa0JBQWtCLENBQUMsYUFBYSxJQUFJLEtBQUssQ0FBQyxJQUFJO1lBQ25FLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLEVBQUU7WUFFM0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUE7WUFFakIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUE7U0FHeEI7SUFHTCxDQUFDO0lBTUQsbUJBQW1CO1FBR2YsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUE7SUFHbEMsQ0FBQztJQUVELElBQUksWUFBWSxDQUFDLFlBQXFCO1FBRWxDLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFBO0lBRXJDLENBQUM7SUFFRCxJQUFJLFlBQVk7UUFFWixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUE7SUFFN0IsQ0FBQztJQUtELGNBQWM7UUFFVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFdEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtJQUs5QixDQUFDO0lBTUQsc0JBQXNCLENBQUMsUUFBZ0IsRUFBRSxXQUFrQjtRQUV2RCxJQUFJLFFBQVEsSUFBSSxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFFdEUscUNBQXFDO1lBQ3JDLHNEQUFzRDtZQUV0RCxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUE7U0FFaEI7YUFDSTtZQUVELEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUE7U0FFdEQ7SUFFTCxDQUFDO0lBTUQsTUFBTSxDQUFDLDZCQUE2QixDQUFDLGlCQUFpQjtRQUNsRCxzREFBc0Q7UUFDdEQsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFBO1FBQ1osSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFBO1FBQ1osSUFBSSxDQUFDLEdBQUcsaUJBQWlCLENBQUE7UUFDekIsSUFBSSxDQUFDLENBQUMsRUFBRTtZQUNKLENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFBO1NBQ25CO1FBQ0QsSUFBSSxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUU7WUFDcEIsSUFBSSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUE7WUFDZCxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQTtTQUNqQjthQUNJLElBQUksQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFO1lBQzdCLElBQUksR0FBRyxDQUFDLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVTtrQkFDckMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUE7WUFDekMsSUFBSSxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTO2tCQUNwQyxRQUFRLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQTtTQUMzQztRQUNELG9FQUFvRTtRQUVwRSxNQUFNLFdBQVcsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFBO1FBRTVDLE9BQU8sV0FBVyxDQUFBO0lBRXRCLENBQUM7SUFJRCxNQUFNLENBQUMsNEJBQTRCLENBQUMsRUFBRTtRQUNsQywrREFBK0Q7UUFDL0QsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFBO1FBQ2pCLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQTtRQUVqQixPQUFPLEVBQUUsRUFBRTtZQUNQLElBQUksRUFBRSxDQUFDLE9BQU8sSUFBSSxNQUFNLEVBQUU7Z0JBRXRCLHVIQUF1SDtnQkFFdkgsNkVBQTZFO2dCQUM3RSw4RUFBOEU7Z0JBQzlFLDRFQUE0RTtnQkFDNUUsRUFBRTtnQkFDRixrRUFBa0U7Z0JBQ2xFLGdFQUFnRTthQUNuRTtpQkFDSTtnQkFDRCxTQUFTLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFBO2dCQUM1RCxTQUFTLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFBO2FBQzVEO1lBRUQsRUFBRSxHQUFHLEVBQUUsQ0FBQyxZQUFZLENBQUE7U0FDdkI7UUFDRCxPQUFPO1lBQ0gsQ0FBQyxFQUFFLFNBQVM7WUFDWixDQUFDLEVBQUUsU0FBUztTQUNmLENBQUE7SUFDTCxDQUFDO0lBRUQsTUFBTSxDQUFDLHVDQUF1QyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsT0FBTztRQUN4RCxNQUFNLHlCQUF5QixHQUFHLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUM1RSxNQUFNLG9CQUFvQixHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyx5QkFBeUIsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyx5QkFBeUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQTtRQUMzRyxPQUFPLG9CQUFvQixDQUFBO0lBQy9CLENBQUM7SUFFRCxNQUFNLENBQUMsNEJBQTRCLENBQUMsaUJBQWlCLEVBQUUsT0FBTztRQUMxRCxNQUFNLHFCQUFxQixHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBQ25GLE1BQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLHVDQUF1QyxDQUNyRSxxQkFBcUIsQ0FBQyxDQUFDLEVBQ3ZCLHFCQUFxQixDQUFDLENBQUMsRUFDdkIsT0FBTyxDQUNWLENBQUE7UUFDRCxPQUFPLG9CQUFvQixDQUFBO0lBQy9CLENBQUM7Q0FNSjtBQ2xkRCwwQ0FBMEM7QUFJMUMsTUFBTSxNQUFPLFNBQVEsWUFBWTtJQUs3QixZQUFZLFNBQWlCLEVBQUUsWUFBWSxHQUFHLEdBQUc7UUFFN0MsS0FBSyxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUE7UUFFbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUE7UUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxZQUFZLENBQUE7UUFFOUIsSUFBSSxDQUFDLDRCQUE0QixHQUFHLEVBQUUsQ0FBQTtRQUV0QyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFBO0lBSWpDLENBQUM7SUFNRCxRQUFRLENBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxZQUFtQztRQUVwRSxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxlQUFlLEVBQUUsWUFBWSxDQUFDLENBQUE7UUFFeEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsWUFBWSxDQUFBO1FBRXBDLHFCQUFxQjtRQUdyQixrQ0FBa0M7UUFHbEMsZUFBZSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUtsRCxDQUFDO0lBTUQsSUFBSSxlQUFlO1FBRWYsT0FBTyxLQUFLLENBQUMsZUFBa0MsQ0FBQTtJQUVuRCxDQUFDO0lBRUQsSUFBSSxJQUFJLENBQUMsSUFBWTtRQUVqQixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUE7SUFFM0MsQ0FBQztJQUVELElBQUksSUFBSTtRQUVKLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUE7SUFFM0MsQ0FBQztJQUdELElBQUksTUFBTSxDQUFDLE1BQWM7UUFFckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0lBRXJELENBQUM7SUFFRCxJQUFJLE1BQU07UUFFTixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUV4RCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBTUQsSUFBSSwwQkFBMEIsQ0FBQywwQkFBb0Q7UUFFL0UsSUFBSSxDQUFDLDJCQUEyQixHQUFHLDBCQUEwQixDQUFBO1FBRTdELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtJQUV2QixDQUFDO0lBRUQsSUFBSSwwQkFBMEI7UUFFMUIsT0FBTyxJQUFJLENBQUMsMkJBQTJCLENBQUE7SUFFM0MsQ0FBQztJQUdELDJCQUEyQjtRQUV2QixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLDJDQUEyQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQXVCLENBQUE7UUFFbkgsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQU1ELHdCQUF3QixDQUFDLEtBQTJCO1FBRWhELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUVyQyxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsRUFBRTtZQUV4RCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7U0FFdEI7SUFFTCxDQUFDO0lBTUQsa0JBQWtCO1FBRWQsS0FBSyxDQUFDLGtCQUFrQixFQUFFLENBQUE7UUFFMUIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFBO0lBR3ZCLENBQUM7SUFNRCxZQUFZO1FBRVIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUE7UUFFL0MsSUFBSSxLQUFLLFlBQVksT0FBTyxFQUFFO1lBRTFCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLGtCQUFrQixDQUFBO1lBRXRDLE9BQU07U0FFVDtRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFBO0lBRXZCLENBQUM7SUFNRCxjQUFjO1FBRVYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBRXRCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7SUFFOUIsQ0FBQztDQU1KO0FDaExELG9DQUFvQztBQUlwQyxNQUFNLFlBQWEsU0FBUSxNQUFNO0lBSzdCLDZDQUE2QztJQU03QyxZQUFZLFNBQWlCLEVBQUUsV0FBb0IsRUFBRSxTQUFrQjtRQUVuRSxLQUFLLENBQUMsU0FBUyxFQUFFLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQTtRQUV4RSxJQUFJLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQTtRQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQTtRQUd4QixJQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDO1lBQ2xDLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxZQUFZLENBQUMsZUFBZTtTQUN6RSxFQUFFLFVBQThCLE1BQWdCLEVBQUUsS0FBWTtZQUUzRCxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUE7WUFFYixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFhLENBQUE7UUFHeEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO1FBRWIsMEJBQTBCO1FBRTFCLDhCQUE4QjtJQUdsQyxDQUFDO0lBTUQsUUFBUSxDQUFDLFNBQVMsRUFBRSxlQUFlLEVBQUUsWUFBd0Q7UUFFekYsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLFlBQVksQ0FBQyxDQUFBO1FBRXhELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQTtRQUU5QixxQkFBcUI7UUFFckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsRUFBRSxZQUFZLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUV2RyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUU1QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUE7SUFNcEMsQ0FBQztJQU1ELElBQUksVUFBVTtRQUVWLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUE7SUFFakMsQ0FBQztJQUVELElBQUksU0FBUztRQUVULE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUE7SUFFaEMsQ0FBQztJQUdELElBQUksTUFBTSxDQUFDLE1BQThCO1FBRXJDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtJQUUvQixDQUFDO0lBR0QsSUFBSSxNQUFNO1FBRU4sT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQTtJQUU3QixDQUFDO0lBTUQsSUFBSSxlQUFlO1FBRWYsT0FBTyxLQUFLLENBQUMsZUFBa0MsQ0FBQTtJQUVuRCxDQUFDO0lBR0QsSUFBSSxNQUFNLENBQUMsTUFBYztRQUVyQixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFFckQsQ0FBQztJQUVELElBQUksTUFBTTtRQUVOLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBRXhELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFNRCxjQUFjO1FBRVYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBRXRCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFFMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFBO1FBRTFCLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUE7SUFHaEMsQ0FBQztDQU1KO0FDN0lELDBDQUEwQztBQXlCMUMsTUFBTSxRQUFTLFNBQVEsWUFBWTtJQWEvQixZQUFZLFNBQWlCLEVBQUUsV0FBb0IsRUFBRSxTQUFTLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJO1FBRWpGLEtBQUssQ0FBQyxTQUFTLEVBQUUsV0FBVyxFQUFFLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUE7UUFUN0QsK0JBQTBCLEdBQUcsRUFBRSxDQUFBO1FBQy9CLHlCQUFvQixHQUFXLEdBQUcsQ0FBQTtRQUNsQyx5QkFBb0IsR0FBVyxFQUFFLENBQUE7UUFTN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUE7UUFDdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxZQUFZLENBQUE7SUFHbEMsQ0FBQztJQU1ELFFBQVEsQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLFlBQW1DO1FBRXBFLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFlBQVksQ0FBQTtRQUVwQyxxQkFBcUI7UUFFckIsSUFBSSxDQUFDLE1BQU0sR0FBRztZQUVWLFVBQVUsRUFBRTtnQkFFUixNQUFNLEVBQUUsT0FBTyxDQUFDLFVBQVU7Z0JBQzFCLFdBQVcsRUFBRSxPQUFPLENBQUMsVUFBVTtnQkFDL0IsUUFBUSxFQUFFLE9BQU8sQ0FBQyxVQUFVO2FBRS9CO1lBRUQsVUFBVSxFQUFFO2dCQUVSLE1BQU0sRUFBRSxPQUFPLENBQUMsU0FBUztnQkFDekIsV0FBVyxFQUFFLE9BQU8sQ0FBQyxVQUFVO2dCQUMvQixRQUFRLEVBQUUsT0FBTyxDQUFDLFFBQVE7YUFFN0I7U0FFSixDQUFBO1FBR0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLFdBQVcsQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDLENBQUE7UUFDMUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFBO1FBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBRS9CLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUE7UUFHaEUsSUFBSSxVQUFVLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBRXBDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxVQUFVLENBQUMsU0FBUyxHQUFHLFlBQVksRUFBRSxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUE7WUFDbkYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtZQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQTtZQUVoQyxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQTtTQUU5QztRQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFBO1FBRXhCLElBQUksQ0FBQyxTQUFTLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFBO1FBQzFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFBO1FBQy9ELElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFBO0lBRS9DLENBQUM7SUFJRCxJQUFJLGNBQWM7UUFFZCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFBO0lBRTVDLENBQUM7SUFFRCxJQUFJLGNBQWMsQ0FBQyxjQUFjO1FBRTdCLElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFBO1FBRXJDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtJQUV6QixDQUFDO0lBS0QsSUFBVyxPQUFPLENBQUMsT0FBZ0I7UUFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUE7UUFDdkIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7SUFDdkMsQ0FBQztJQUVELElBQVcsT0FBTztRQUNkLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQTtJQUN4QixDQUFDO0lBRUQsSUFBVyxXQUFXLENBQUMsV0FBb0I7UUFDdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUE7UUFDL0IsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7SUFDdkMsQ0FBQztJQUVELElBQVcsV0FBVztRQUNsQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUE7SUFDNUIsQ0FBQztJQUVELElBQVcsT0FBTyxDQUFDLE9BQWdCO1FBQy9CLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFBO1FBQ3ZCLElBQUksT0FBTyxFQUFFO1lBQ1QsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1NBQ2Y7YUFDSTtZQUNELElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTtTQUNkO1FBQ0QsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7SUFDdkMsQ0FBQztJQUVELElBQVcsT0FBTztRQUNkLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQTtJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRLENBQUMsUUFBaUI7UUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUE7UUFDekIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7SUFDdkMsQ0FBQztJQUVELElBQVcsUUFBUTtRQUNmLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQTtJQUN6QixDQUFDO0lBTUQsNEJBQTRCO1FBRXhCLElBQUksY0FBYyxHQUFhLElBQUksQ0FBQywyQkFBMkIsQ0FBQTtRQUMvRCxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNuQyxjQUFjLEdBQUcsSUFBSSxDQUFDLDJDQUEyQyxDQUFBO1NBQ3BFO2FBQ0ksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ3BCLGNBQWMsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUE7U0FDdEQ7YUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDbkIsY0FBYyxHQUFHLElBQUksQ0FBQyw0QkFBNEIsQ0FBQTtTQUNyRDthQUNJLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUN2QixjQUFjLEdBQUcsSUFBSSxDQUFDLGdDQUFnQyxDQUFBO1NBQ3pEO2FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ25CLGNBQWMsR0FBRyxJQUFJLENBQUMsNEJBQTRCLENBQUE7U0FDckQ7UUFFRCxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUE7WUFDNUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFBO1NBQzFDO2FBQ0k7WUFDRCxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1NBQzVCO1FBRUQsSUFBSSxDQUFDLG1DQUFtQyxFQUFFLENBQUM7SUFFL0MsQ0FBQztJQUVELDJCQUEyQjtRQUV2QixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQTtRQUNwRCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUE7SUFFN0QsQ0FBQztJQUVELDRCQUE0QjtRQUV4QixJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQTtRQUVsQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRTtZQUNoQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQTtTQUN4RDtRQUVELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQTtTQUM3RDtJQUVMLENBQUM7SUFFRCw0QkFBNEI7UUFFeEIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7UUFFbkMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7WUFDaEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUE7U0FDeEQ7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRTtZQUNoQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUE7U0FDN0Q7SUFFTCxDQUFDO0lBRUQsZ0NBQWdDO1FBRTVCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFBO1FBQ3pELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQTtJQUVsRSxDQUFDO0lBRUQsNkJBQTZCO1FBRXpCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFBO1FBQ3RELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQTtJQUUvRCxDQUFDO0lBRUQsMkNBQTJDO1FBRXZDLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFBO1FBRXBDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEVBQUU7WUFDL0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQTtTQUN2RTtRQUVELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEVBQUU7WUFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUE7U0FDNUU7SUFFTCxDQUFDO0lBR0QsSUFBSSxPQUFPLENBQUMsT0FBZ0I7UUFFeEIsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUE7UUFFdkIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7SUFFdkMsQ0FBQztJQUVELElBQUksT0FBTztRQUVQLE9BQU8sS0FBSyxDQUFDLE9BQU8sQ0FBQTtJQUV4QixDQUFDO0lBRUQsbUNBQW1DO1FBRS9CLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFBO1NBQ2pCO2FBQ0k7WUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQTtTQUNuQjtRQUVELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFBO0lBRTlDLENBQUM7SUFJRCxhQUFhLENBQUMsY0FBc0I7UUFFaEMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQTtRQUVuQyxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksY0FBYyxFQUFFO1lBRXZDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7U0FFL0M7SUFFTCxDQUFDO0lBR0QsSUFBSSxVQUFVO1FBRVYsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFBO0lBRTNCLENBQUM7SUFFRCxJQUFJLFNBQVM7UUFFVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUE7SUFFMUIsQ0FBQztJQU1ELGNBQWM7UUFFVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFdEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUV4QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFBO1FBRXJDLG9DQUFvQztRQUNwQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFHNUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFBO1NBR2hDO1FBRUQsb0NBQW9DO1FBQ3BDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFFdkQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUE7WUFHbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUE7WUFDaEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUE7WUFDakQsd0NBQXdDO1lBQ3hDLG9EQUFvRDtZQUNwRCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFBO1lBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQTtZQUNwRCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQTtZQUUzRCxJQUFJLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFFakMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUE7Z0JBRW5DLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQTtnQkFFNUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFBO2dCQUU3QixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsK0JBQStCLENBQ2pFLElBQUksV0FBVyxDQUNYLENBQUMsRUFDRCxDQUFDLEVBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQ2xCLENBQUM7b0JBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUM5QyxFQUNELElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLEVBQUUsRUFDdEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQ3hCLElBQUksQ0FBQyxvQkFBb0IsRUFDekIsSUFBSSxDQUFDLG9CQUFvQixDQUM1QixDQUFBO2dCQUVELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTthQUVsQztTQUdKO1FBRUQsOEJBQThCO1FBQzlCLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFFM0QsTUFBTSxpQkFBaUIsR0FBRyxJQUFJLENBQUE7WUFFOUIsTUFBTSxHQUFHLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7WUFFdkQsTUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFBO1lBQ2hDLFVBQVUsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQTtZQUM1RCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUE7WUFFakMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUE7WUFFbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUE7WUFDbkUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUE7WUFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQTtZQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsa0JBQWtCLENBQUE7WUFFcEQsSUFBSSxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBRWpDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFBO2dCQUVuQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUE7Z0JBRTVCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQTtnQkFFN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLCtCQUErQixDQUNqRSxJQUFJLFdBQVcsQ0FDWCxDQUFDLEVBQ0QsQ0FBQyxFQUNELElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUNsQixDQUFDO29CQUNELElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FDOUMsRUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixFQUFFLEVBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUN4QixJQUFJLENBQUMsb0JBQW9CLEVBQ3pCLElBQUksQ0FBQyxvQkFBb0IsQ0FDNUIsQ0FBQTtnQkFFRCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUE7YUFFbEM7U0FFSjtRQUVELElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFBO0lBRWhDLENBQUM7SUFFRCxzQkFBc0I7UUFFbEIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLDhCQUE4QixDQUFDLENBQUE7UUFFakYseUlBQXlJO1FBRXpJLDBCQUEwQjtRQUMxQiw0QkFBNEI7UUFDNUIsNkNBQTZDO1FBQzdDLDhDQUE4QztRQUM5QyxvQkFBb0I7UUFDcEIsdUNBQXVDO0lBRTNDLENBQUM7Q0FNSjtBQ25jRCx5Q0FBeUM7QUFHekMsTUFBTSxRQUFTLFNBQVEsUUFBUTtJQUczQixZQUFZLFNBQWlCO1FBRXpCLEtBQUssQ0FBQyxTQUFTLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7UUFFOUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUE7UUFDdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7SUFFOUIsQ0FBQztJQUdELDJCQUEyQjtRQUV2QixJQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQTtRQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFBO1FBRTlDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQTtJQUc3QixDQUFDO0lBRUQsNEJBQTRCO1FBRXhCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTtJQUVoQyxDQUFDO0lBRUQsZ0NBQWdDO1FBRTVCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQTtJQUUvQixDQUFDO0lBR0QsY0FBYztRQUVWLCtCQUErQjtRQUMvQix5QkFBeUI7UUFFekIsTUFBTSxPQUFPLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFBO1FBQ2hELE1BQU0sV0FBVyxHQUFHLE9BQU8sR0FBRyxHQUFHLENBQUE7UUFFakMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUUxQixJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUE7UUFFakMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUE7UUFDekQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUE7UUFFN0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQTtRQUN4RSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FDNUQsT0FBTyxFQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUN2RCxDQUFBO0lBRUwsQ0FBQztJQUdELHNCQUFzQixDQUFDLG9CQUE0QixDQUFDO1FBRWhELE1BQU0sT0FBTyxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQTtRQUNoRCxNQUFNLFdBQVcsR0FBRyxPQUFPLEdBQUcsR0FBRyxDQUFBO1FBRWpDLE1BQU0sTUFBTSxHQUFHLGlCQUFpQixHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxHQUFHLE9BQU8sR0FBRyxDQUFDO1lBQ3JELElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtRQUU3RCxPQUFPLE1BQU0sQ0FBQTtJQUdqQixDQUFDO0NBR0o7QUM3RUQsSUFBSSxzQkFBc0IsSUFBSSxLQUFLLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUVoRCxLQUFLLENBQUMsU0FBaUIsQ0FBQyxvQkFBb0IsR0FBRyxVQUE0QixLQUFhO1FBRXJGLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUVuQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQTtTQUV4QjtJQUVMLENBQUMsQ0FBQTtDQUVKO0FBVUQsSUFBSSxlQUFlLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFekMsS0FBSyxDQUFDLFNBQWlCLENBQUMsYUFBYSxHQUFHLFVBQTRCLE9BQU87UUFFeEUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQTtJQUVwRCxDQUFDLENBQUE7Q0FFSjtBQVVELElBQUksc0JBQXNCLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFaEQsS0FBSyxDQUFDLFNBQWlCLENBQUMsb0JBQW9CLEdBQUcsVUFBNEIsS0FBYSxFQUFFLE9BQVk7UUFFbkcsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBRXBDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQTtTQUVqQztJQUVMLENBQUMsQ0FBQTtDQUVKO0FBVUQsSUFBSSx1QkFBdUIsSUFBSSxLQUFLLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUVqRCxLQUFLLENBQUMsU0FBaUIsQ0FBQyxxQkFBcUIsR0FBRyxVQUE0QixLQUFhLEVBQUUsT0FBWTtRQUVwRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDaEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQTtJQUU3QyxDQUFDLENBQUE7Q0FFSjtBQVVELElBQUksVUFBVSxJQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBRXBDLEtBQUssQ0FBQyxTQUFpQixDQUFDLFFBQVEsR0FBRyxVQUE0QixPQUFPO1FBRW5FLE1BQU0sTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQzVDLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUMsQ0FBQTtDQUVKO0FBRUQsSUFBSSxhQUFhLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFdkMsS0FBSyxDQUFDLFNBQWlCLENBQUMsV0FBVyxHQUFHLFVBQTRCLFFBQWU7UUFFOUUsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSztZQUN4RCxPQUFPLFFBQVEsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDckMsQ0FBQyxDQUFDLENBQUE7UUFFRixPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDLENBQUE7Q0FFSjtBQVlELElBQUksVUFBVSxJQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBRXBDLEtBQUssQ0FBQyxTQUFpQixDQUFDLFFBQVEsR0FBRyxVQUVoQyxjQUFvRTtRQUdwRSxNQUFNLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUVwRCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDLENBQUE7Q0FFSjtBQUVELElBQUksV0FBVyxJQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBRXJDLEtBQUssQ0FBQyxTQUFpQixDQUFDLFNBQVMsR0FBRyxVQUVqQyxjQUFvRTtRQUdwRSxNQUFNLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUVyRCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDLENBQUE7Q0FFSjtBQUVELElBQUksVUFBVSxJQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBRXBDLEtBQUssQ0FBQyxTQUFpQixDQUFDLFFBQVEsR0FBRyxVQUVoQyxjQUFvRTtRQUdwRSxTQUFTLGdCQUFnQixDQUFDLEtBQVUsRUFBRSxLQUFhLEVBQUUsS0FBWTtZQUM3RCxPQUFPLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUE7UUFDL0MsQ0FBQztRQUVELE1BQU0sTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFFdkQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQyxDQUFBO0NBRUo7QUFjRCxJQUFJLFdBQVcsSUFBSSxLQUFLLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUV0QyxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxVQUE0QixRQUFRO1FBQzVELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsRUFBRSxHQUFHO1lBQ2pDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDekQsT0FBTyxHQUFHLENBQUE7UUFDZCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUE7SUFDVixDQUFDLENBQUE7Q0FFSjtBQVVELElBQUksY0FBYyxJQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBQ3pDLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxjQUFjLEVBQUU7UUFDbkQsR0FBRyxFQUFFLFNBQVMsWUFBWTtZQUN0QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDdEIsT0FBTyxNQUFNLENBQUE7UUFDakIsQ0FBQztRQUNELEdBQUcsRUFBRSxVQUE0QixPQUFZO1lBQ3pDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7Z0JBQ2xCLE9BQU07YUFDVDtZQUNELElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUE7UUFDckIsQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUNMO0FBRUQsSUFBSSxhQUFhLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFDeEMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLGFBQWEsRUFBRTtRQUNsRCxHQUFHLEVBQUUsU0FBUyxXQUFXO1lBQ3JCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFBO1lBQ3BDLE9BQU8sTUFBTSxDQUFBO1FBQ2pCLENBQUM7UUFDRCxHQUFHLEVBQUUsVUFBNEIsT0FBWTtZQUN6QyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO2dCQUNsQixPQUFNO2FBQ1Q7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUE7UUFDbkMsQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUNMO0FBRUQsSUFBSSxjQUFjLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFekMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLGNBQWMsRUFBRTtRQUVuRCxHQUFHLEVBQUUsU0FBUyxZQUFZO1lBRXRCLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQTtZQUVsQixNQUFNLGNBQWMsR0FBRyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUUvQixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUV0QyxJQUFJLGVBQWUsR0FBSSxRQUFRLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsT0FBTyxDQUFjLENBQUMsSUFBSSxDQUMzRixPQUFPLEVBQ1AsT0FBTyxDQUNWLENBQUE7b0JBRUQsT0FBTyxlQUFlLEVBQUUsQ0FBQTtnQkFFNUIsQ0FBQyxDQUFDLENBQUM7WUFFUCxDQUFDLENBQUE7WUFFRCxNQUFNLE1BQU0sR0FBRyxJQUFJLEtBQUssQ0FDcEIsY0FBYyxFQUNkO2dCQUVJLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEVBQUU7b0JBRTNCLElBQUksR0FBRyxJQUFJLGtCQUFrQixFQUFFO3dCQUUzQixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FDL0QsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFDbkIsT0FBTyxDQUNWLENBQUMsQ0FBQztxQkFFTjtvQkFFRCxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO29CQUVuQixPQUFPLE1BQU0sQ0FBQTtnQkFFakIsQ0FBQztnQkFDRCxHQUFHLEVBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsRUFBRTtvQkFFbEMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtvQkFFbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEVBQUU7d0JBRW5DLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUE7b0JBRXpFLENBQUMsQ0FBQyxDQUFBO29CQUVGLE9BQU8sSUFBSSxDQUFBO2dCQUVmLENBQUM7YUFFSixDQUNKLENBQUE7WUFFRCxPQUFPLE1BQU0sQ0FBQTtRQUVqQixDQUFDO1FBQ0QsR0FBRyxFQUFFLFVBQTRCLE9BQVk7WUFFekMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUU7Z0JBRWxDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUE7YUFFcEI7UUFFTCxDQUFDO0tBRUosQ0FBQyxDQUFBO0NBRUw7QUF1QkQsSUFBSSxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFaEMsS0FBSyxDQUFDLFNBQWlCLENBQUMsSUFBSSxHQUFHO1FBRTVCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDNUIsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQyxDQUFBO0NBRUo7QUFVRCxJQUFJLGtCQUFrQixJQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBRTVDLEtBQUssQ0FBQyxTQUFpQixDQUFDLGdCQUFnQixHQUFHLFVBQTRCLG1CQUEyQjtRQUMvRixNQUFNLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFDakIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLG1CQUFtQixFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUs7Z0JBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDeEIsQ0FBQyxDQUFDLENBQUE7U0FDTDtRQUNELE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUMsQ0FBQTtDQUVKO0FBVUQsSUFBSSxpQ0FBaUMsSUFBSSxLQUFLLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUMzRCxLQUFLLENBQUMsU0FBaUIsQ0FBQywrQkFBK0IsR0FBRyxVQUE0QixTQUFpQjtRQUNwRyxNQUFNLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFDakIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1NBQ3ZCO1FBQ0QsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQyxDQUFBO0NBQ0o7QUFVRCxJQUFJLGFBQWEsSUFBSSxLQUFLLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUV4QyxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsYUFBYSxFQUFFO1FBQ2xELEdBQUcsRUFBRSxTQUFTLFdBQVc7WUFDckIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO2dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDaEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1lBQ0wsT0FBTyxNQUFNLENBQUE7UUFDakIsQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUVMO0FBVUQscUNBQXFDO0FBQ3JDLElBQUksZ0JBQWdCLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxHQUFHLEVBQUU7SUFDNUMsT0FBTyxDQUFDLElBQUksQ0FDUiw2S0FBNkssQ0FBQyxDQUFBO0NBQ3JMO0FBQ0QseUVBQXlFO0FBQ3pFLEtBQUssQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLFVBQVUsS0FBWSxFQUFFLE9BQWdCO0lBRXJFLDhDQUE4QztJQUM5QyxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQ1IsT0FBTyxLQUFLLENBQUE7S0FDZjtJQUVELDRDQUE0QztJQUM1QyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtRQUM3QixPQUFPLEtBQUssQ0FBQTtLQUNmO0lBRUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQ1QsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtJQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7UUFFZixpQ0FBaUM7UUFDakMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVksS0FBSyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFFbkUsaUNBQWlDO1lBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNuQyxPQUFPLEtBQUssQ0FBQTthQUNmO1NBRUo7YUFDSSxJQUFJLE9BQU8sSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUUzRyxPQUFPLEtBQUssQ0FBQTtTQUVmO2FBQ0ksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBRTFCLGlGQUFpRjtZQUNqRixPQUFPLEtBQUssQ0FBQTtTQUVmO0tBRUo7SUFFRCxPQUFPLElBQUksQ0FBQTtBQUVmLENBQUMsQ0FBQTtBQUVELGdDQUFnQztBQUNoQyxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQTtBQVcvRSxJQUFJLFNBQVMsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUVwQyxNQUFNLENBQUMsU0FBaUIsQ0FBQyxPQUFPLEdBQUcsVUFBd0IsZ0JBQW1EO1FBQzNHLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSztZQUNwQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFDcEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO0lBQ2pCLENBQUMsQ0FBQTtJQUVELGdDQUFnQztJQUNoQyxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUE7Q0FFNUU7QUFVRCxJQUFJLFdBQVcsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUN2QyxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsV0FBVyxFQUFFO1FBQ2pELEdBQUcsRUFBRTtZQUNELE1BQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQTtZQUNqQixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBVTtnQkFDN0IsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUN0QixDQUFDLENBQUMsQ0FBQTtZQUNGLE9BQU8sTUFBTSxDQUFBO1FBQ2pCLENBQUM7S0FDSixDQUFDLENBQUE7Q0FDTDtBQVVELElBQUksU0FBUyxJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBQ3JDLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUU7UUFDL0MsR0FBRyxFQUFFO1lBQ0QsTUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUNoQyxPQUFPLE1BQU0sQ0FBQTtRQUNqQixDQUFDO0tBQ0osQ0FBQyxDQUFBO0NBQ0w7QUFVRCxJQUFJLDRDQUE0QyxJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBRXZFLE1BQU0sQ0FBQyxTQUFpQixDQUFDLDBDQUEwQyxHQUFHLFVBQXdCLE1BQVc7UUFHdEcsU0FBUyxVQUFVLENBQUMsSUFBUztZQUN6QixPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUNyRSxDQUFDO1FBRUQsU0FBUyxnQkFBZ0IsQ0FBQyxNQUFXLEVBQUUsTUFBVztZQUU5QyxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsQ0FBQTtZQUV4QyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBRTFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRztvQkFFckMsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7d0JBRXpCLDBCQUEwQjt3QkFFMUIscURBQXFEO3dCQUVyRCxJQUFJO3dCQUNKLFNBQVM7d0JBRVQsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTt3QkFFeEQsR0FBRztxQkFFTjt5QkFDSTt3QkFFRCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQTtxQkFFaEQ7Z0JBRUwsQ0FBQyxDQUFDLENBQUE7YUFFTDtZQUVELE9BQU8sTUFBTSxDQUFBO1FBRWpCLENBQUM7UUFFRCxNQUFNLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFN0MsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQyxDQUFBO0lBRUQsZ0NBQWdDO0lBQ2hDLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSw0Q0FBNEMsRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFBO0NBRS9HO0FBV0QsSUFBSSxVQUFVLElBQUksTUFBTSxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFckMsTUFBTSxDQUFDLFNBQWlCLENBQUMsUUFBUSxHQUFHLFVBQXdCLE1BQU07UUFFL0QsTUFBTSxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDM0MsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQyxDQUFBO0NBRUo7QUFVRCxJQUFJLG1CQUFtQixJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBQy9DLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxtQkFBbUIsRUFBRTtRQUN6RCxHQUFHLEVBQUU7WUFDRCxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUE7WUFDekUsT0FBTyxNQUFNLENBQUE7UUFDakIsQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUNMO0FBVUQsSUFBSSxnQkFBZ0IsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUM1QyxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsZ0JBQWdCLEVBQUU7UUFDdEQsR0FBRyxFQUFFLFNBQVMsY0FBYztZQUN4QixNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDM0IsT0FBTyxNQUFNLENBQUE7UUFDakIsQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUNMO0FBVUQsSUFBSSxXQUFXLElBQUksTUFBTSxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFdEMsTUFBTSxDQUFDLFNBQWlCLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQTtDQUU1QztBQVVELElBQUksV0FBVyxJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO0lBRXRDLE1BQU0sQ0FBQyxTQUFpQixDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUE7Q0FFNUM7QUFhRCxJQUFJLGNBQWMsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtJQUMxQyxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsY0FBYyxFQUFFO1FBQ3BELEdBQUcsRUFBRTtZQUNELE1BQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUE7WUFDdEQsT0FBTyxNQUFNLENBQUE7UUFDakIsQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUNMO0FBV0QsTUFBTSxlQUFlO0lBRWpCLGFBQWE7SUFDYixNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUN6QixPQUFNO0lBQ1YsQ0FBQztDQUVKO0FBR0QsSUFBSSxjQUFjLElBQUksT0FBTyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFM0MsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLGNBQWMsRUFBRTtRQUNyRCxHQUFHLEVBQUU7WUFFRCxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBRWQsT0FBTyxDQUFDLENBQUE7YUFFWDtZQUVELE9BQU8sQ0FBQyxDQUFBO1FBRVosQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUVMO0FBVUQsSUFBSSxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7SUFFdEMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFlBQVksRUFBRTtRQUNoRCxHQUFHLEVBQUUsU0FBUyxVQUFVO1lBRXBCLE1BQU0sTUFBTSxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUc7Z0JBQ2pHLElBQUksQ0FBQyxXQUFXLEVBQUUsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRztnQkFDbEUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFFdkMsT0FBTyxNQUFNLENBQUE7UUFFakIsQ0FBQztLQUNKLENBQUMsQ0FBQTtDQUlMO0FDenVCRCx1Q0FBdUM7QUFDdkMsNENBQTRDO0FBQzVDLGtDQUFrQztBQUNsQyxpREFBaUQ7QUFHakQsTUFBTSxTQUFVLFNBQVEsTUFBTTtJQUsxQixZQUFZLFNBQWlCO1FBRXpCLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUxaLGNBQVMsR0FBZSxFQUFFLENBQUE7UUFDMUIsV0FBTSxHQUFpRCxFQUFFLENBQUE7UUFNN0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUE7UUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUE7SUFFNUIsQ0FBQztJQUdELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQTtJQUN0QixDQUFDO0lBRUQsSUFBSSxLQUFLLENBQUMsS0FHUDtRQUVDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFBO1FBRW5CLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFLENBQUE7UUFFakQsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUE7UUFFbkIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFFbEMsTUFBTSxRQUFRLEdBQUcsSUFBSSxRQUFRLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxDQUFBO1lBRWpELFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUE7WUFDdEMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsb0JBQW9CLENBQUMsQ0FBQTtZQUN2RSxRQUFRLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLGVBQWUsR0FBRyxDQUN2RCxNQUFNLEVBQ04sS0FBSyxFQUNQLEVBQUUsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGdDQUFnQyxDQUFDLHFCQUFxQixFQUFFLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFBO1lBRTNHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUE7UUFFN0IsQ0FBQyxDQUFDLENBQUE7SUFHTixDQUFDO0lBR0QsY0FBYztRQUVWLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUV0QixNQUFNLE9BQU8sR0FBRyxrQkFBa0IsQ0FBQyxhQUFhLENBQUE7UUFFaEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUUxQiw4R0FBOEc7UUFDOUcsZ0JBQWdCO1FBRWhCLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUV6QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFFbEQsTUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUVoRCxLQUFLLEdBQUcsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7WUFDbEYsUUFBUSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUE7WUFFdEIsTUFBTSxjQUFjLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDMUQsTUFBTSxhQUFhLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFHekQsTUFBTSxpQkFBaUIsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsT0FBTyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1RCxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUNuQixjQUFjLENBQUMsc0JBQXNCLENBQUMsaUJBQWlCLENBQUMsRUFDeEQsYUFBYSxDQUFDLHNCQUFzQixDQUFDLGlCQUFpQixDQUFDLENBQzFELENBQUE7WUFFRCxLQUFLLEdBQUcsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQTtZQUU1QyxLQUFLLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxjQUFjLEVBQUUsYUFBYSxDQUFDLEVBQUUsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFBO1NBRXpGO0lBRUwsQ0FBQztDQUdKO0FDL0ZELHFEQUFxRDtBQUNyRCxpREFBaUQ7QUFDakQsaURBQWlEO0FBQ2pELCtDQUErQztBQUMvQyxtQ0FBbUM7QUFNbkMsTUFBTSxrQkFBbUIsU0FBUSxnQkFBZ0I7SUFxQjdDLFlBQVksSUFBSTtRQUVaLGdCQUFnQjtRQUNoQixLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7UUFoQlAsa0JBQWEsR0FBRyxPQUFPLENBQzNCOzs7Ozs7OztjQVFNLENBQ1QsQ0FBQTtRQVFHLDhEQUE4RDtRQUU5RCx5R0FBeUc7UUFDekcsWUFBWTtRQUNaLHlCQUF5QjtRQUN6QiwwQkFBMEI7UUFDMUIsd0JBQXdCO1FBQ3hCLHNCQUFzQjtRQUV0Qiw4SEFBOEg7UUFFOUgsOEZBQThGO1FBRTlGLHVIQUF1SDtRQUN2SCwwSEFBMEg7UUFFMUgsc0NBQXNDO0lBRTFDLENBQUM7SUFHRCwwQkFBMEI7UUFFdEIsS0FBSyxDQUFDLDBCQUEwQixFQUFFLENBQUE7UUFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQTtJQUV0QyxDQUFDO0lBR0QsWUFBWTtRQUVSLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDNUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQTtRQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ2pFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQTtRQUM3RCxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUE7UUFDMUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBRXJDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFBO1FBQzFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQTtRQUNqRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHO1lBQzNCLFVBQVUsRUFBRTtnQkFDUixNQUFNLEVBQUUsT0FBTyxDQUFDLFVBQVU7Z0JBQzFCLFdBQVcsRUFBRSxPQUFPLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3BELFFBQVEsRUFBRSxPQUFPLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUM7YUFDbkQ7WUFDRCxVQUFVLEVBQUU7Z0JBQ1IsTUFBTSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0I7Z0JBQ2hDLFdBQVcsRUFBRSxPQUFPLENBQUMsZ0JBQWdCO2dCQUNyQyxRQUFRLEVBQUUsT0FBTyxDQUFDLGdCQUFnQjthQUNyQztTQUNKLENBQUE7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLEdBQUc7WUFDN0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBQzNDLENBQUMsQ0FBQTtRQUVELElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1FBRWpELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsZUFBZSxHQUFHLENBQ3BFLE1BQU0sRUFDTixLQUFLLEVBQ1AsRUFBRSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsNkJBQTZCLENBQUMscUJBQXFCLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUU3RyxDQUFDO0lBR0ssV0FBVyxDQUFDLEtBQWM7OztZQUU1QixxQkFBaUIsWUFBQyxLQUFLLEVBQUM7WUFFeEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7WUFFakMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLENBQUMsRUFBRTtnQkFFdkUsb0JBQW9CO2dCQUNwQixJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO29CQUNqQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBO2lCQUNwRjtnQkFFRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFBO2FBRTFEO2lCQUNJO2dCQUVELHVCQUF1QjtnQkFDdkIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsRUFBRTtvQkFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQTtvQkFDM0MsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO2lCQUNsRTtnQkFFRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFBO2dCQUNwRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQTtnQkFFbEMsOEJBQThCO2dCQUM5QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEtBQUksTUFBTSxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQSxDQUFBO2dCQUMxRixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUE7YUFFbkQ7UUFFTCxDQUFDO0tBQUE7SUFHRCxJQUFJLHFCQUFxQixDQUFDLFVBQTRCO1FBRWxELElBQUksSUFBSSxDQUFDLHFCQUFxQixJQUFJLFVBQVUsRUFBRTtZQUMxQyxPQUFNO1NBQ1Q7UUFFRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM1QixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUE7U0FDN0Q7UUFFRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsVUFBVSxDQUFBO1FBQ3hDLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzdELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO1FBRTFCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7SUFFOUIsQ0FBQztJQUVELElBQUkscUJBQXFCO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixJQUFJLEdBQUcsQ0FBQTtJQUM3QyxDQUFDO0lBR0QsTUFBTSxLQUFLLGFBQWE7UUFDcEIsT0FBTyxFQUFFLENBQUE7SUFDYixDQUFDO0lBRUQsSUFBSSxhQUFhO1FBQ2IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQTtJQUNuQyxDQUFDO0lBR0QsbUJBQW1CO1FBRWYsS0FBSyxDQUFDLG1CQUFtQixFQUFFLENBQUE7UUFFM0IsY0FBYztRQUNkLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRTdCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUV0RCxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FDN0UsQ0FBQyxFQUNELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUN2RSxDQUFBO0lBRUwsQ0FBQztDQUdKO0FDMUtELGFBQWE7QUFDYixNQUFNLE9BQVEsU0FBUSxLQUF1QjtJQVF6QyxZQUFZLElBQWE7UUFFckIsS0FBSyxFQUFFLENBQUE7UUFSWCxlQUFVLEdBQVksRUFBRSxDQUFBO1FBQ3hCLHdCQUFtQixHQUF1QixFQUFFLENBQUE7UUFTeEMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFFM0IsT0FBTTtTQUVUO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3RCLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO1NBQ3ZCO1FBRUQsSUFBSSxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFBO1FBRS9CLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDbEMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxVQUF5QixTQUFpQixFQUFFLEtBQWEsRUFBRSxLQUFlO1lBRXpGLE1BQU0sYUFBYSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDN0MsTUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFBO1lBRXJCLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBRWhCLE9BQU07YUFFVDtZQUVELE1BQU0sZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUE7WUFDdEQsTUFBTSxvQkFBb0IsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFBO1lBRTlELG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxVQUFVLFVBQVUsRUFBRSxLQUFLLEVBQUUsS0FBSztnQkFFM0QsTUFBTSxnQkFBZ0IsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2dCQUM5QyxNQUFNLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUNuRCxNQUFNLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUVyRCxJQUFJLEdBQUcsRUFBRTtvQkFFTCxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFBO2lCQUUxQjtZQUlMLENBQUMsQ0FBQyxDQUFBO1lBSUYsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDTixJQUFJLEVBQUUsYUFBYTtnQkFDbkIsVUFBVSxFQUFFLFVBQVU7YUFDekIsQ0FBQyxDQUFBO1FBTU4sQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO0lBR1osQ0FBQztJQUlELE1BQU0sS0FBSyxZQUFZO1FBRW5CLE9BQU8sSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUU1QyxDQUFDO0lBTUQsS0FBSztRQUVELE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQTtJQUVwRCxDQUFDO0lBR0QscUNBQXFDO1FBRWpDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO0lBRXBELENBQUM7SUFJRCxJQUFJO1FBQ0EsSUFBSSxNQUFNLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQTtRQUMxQixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDcEMsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUdELFVBQVU7UUFFTixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7UUFFeEIsTUFBTSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxVQUFVLFNBQVMsRUFBRSxLQUFLLEVBQUUsbUJBQW1CO1lBRTlFLElBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUE7WUFFN0MsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBRXBCLE1BQU0sQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFBO2FBRWxDO1FBRUwsQ0FBQyxDQUFDLENBQUE7UUFFRixNQUFNLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFBO1FBRS9CLE1BQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFBO1FBQ3pCLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFJRCwyQ0FBMkMsQ0FBQyxjQUF3QjtRQUNoRSxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDMUIsTUFBTSxlQUFlLEdBQWEsRUFBRSxDQUFBO1FBQ3BDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxTQUFTLEVBQUUsS0FBSyxFQUFFLEtBQUs7WUFDNUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMxQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO2FBQzlCO1FBQ0wsQ0FBQyxDQUFDLENBQUE7UUFDRixlQUFlLENBQUMsT0FBTyxDQUFDLFVBQVUsYUFBYSxFQUFFLEtBQUssRUFBRSxLQUFLO1lBQ3pELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUM5QyxDQUFDLENBQUMsQ0FBQTtRQUNGLE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFJRCw2QkFBNkIsQ0FBQyxhQUFxQjtRQUMvQyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDMUIsTUFBTSxjQUFjLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFVLFNBQVMsRUFBRSxLQUFLO1lBQzlELE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLGFBQWEsQ0FBQyxDQUFBO1FBQzVDLENBQUMsQ0FBQyxDQUFBO1FBQ0YsSUFBSSxjQUFjLElBQUksQ0FBQyxDQUFDLEVBQUU7WUFDdEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLENBQUE7U0FDbkM7UUFDRCxPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBR0QsbUNBQW1DLENBQUMsYUFBcUIsRUFBRSxhQUFxQixFQUFFLHNCQUFzQixHQUFHLEVBQUU7UUFDekcsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1FBQ3hCLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUE7UUFDbkUsSUFBSSxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDcEIsVUFBVSxHQUFHLEVBQUUsQ0FBQTtTQUNsQjtRQUNELE9BQU8sVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQ2hDLE1BQU0sR0FBRyxNQUFNLENBQUMsa0JBQWtCLENBQUMsYUFBYSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQzdELElBQUksc0JBQXNCLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQy9ELE1BQU0sR0FBRyxNQUFNLENBQUMsNkJBQTZCLENBQUMsYUFBYSxDQUFDLENBQUE7U0FDL0Q7UUFDRCxPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBRUQsa0NBQWtDLENBQUMsYUFBcUIsRUFBRSxhQUFxQixFQUFFLFVBQWtCO1FBQy9GLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTtRQUN4QixJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDN0MsT0FBTyxNQUFNLENBQUE7U0FDaEI7UUFDRCxJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxDQUFBO1FBQ25FLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ3BCLFVBQVUsR0FBRyxFQUFFLENBQUE7U0FDbEI7UUFDRCxVQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsVUFBVSxDQUFBO1FBQ3RDLE1BQU0sR0FBRyxNQUFNLENBQUMsa0JBQWtCLENBQUMsYUFBYSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQzdELE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFHRCxnQ0FBZ0MsQ0FDNUIsY0FBaUIsRUFDakIsVUFBb0YsRUFDcEYsbUJBQTRCLEVBQUU7UUFHOUIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLGtCQUFrQixFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFBO0lBRW5HLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxJQUFZLEVBQUUsVUFBNkIsRUFBRSxtQkFBNEIsRUFBRTtRQUUxRixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDMUIsSUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzlDLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ25CLFNBQVMsR0FBRztnQkFDUixJQUFJLEVBQUUsSUFBSTtnQkFDVixVQUFVLEVBQUUsRUFBRTthQUNqQixDQUFBO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtTQUN6QjtRQUVELElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBRXBCLFVBQVUsR0FBRyxFQUFFLENBQUE7U0FFbEI7UUFFRCxJQUFJLGdCQUFnQixFQUFFO1lBQ2xCLFNBQVMsQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1NBQ3pFO2FBQ0k7WUFDRCxTQUFTLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQTtTQUNwQztRQUVELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFFRCwwQkFBMEIsQ0FBQyxJQUFZLEVBQUUsVUFBNkIsRUFBRSxtQkFBNEIsRUFBRTtRQUVsRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFBO0lBRXZFLENBQUM7SUFJRCwyQkFBMkIsQ0FBb0MsY0FBaUI7UUFFNUUsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLENBQUE7SUFFcEUsQ0FBQztJQUVELGlCQUFpQixDQUFDLElBQVk7UUFDMUIsSUFBSSxNQUFNLEdBQUcsR0FBRyxDQUFBO1FBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxTQUFTLEVBQUUsS0FBSyxFQUFFLElBQUk7WUFDekMsSUFBSSxTQUFTLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDeEIsTUFBTSxHQUFHLFNBQVMsQ0FBQTthQUNyQjtRQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ0YsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUlELG9CQUFvQixDQUFDLFNBQTJCO1FBRTVDLE1BQU0sSUFBSSxHQUFZLElBQUksQ0FBQTtRQUMxQixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUN4QyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBRTtZQUdaLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUV2RCwyQ0FBMkM7U0FFOUM7SUFFTCxDQUFDO0lBRUQsSUFBSSxTQUFTLENBQUMsU0FBa0I7UUFFNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUE7SUFFL0IsQ0FBQztJQUVELElBQUksU0FBUztRQUVULE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBRTFGLENBQUM7SUFHRCxJQUFJLGtCQUFrQjtRQUNsQixPQUFPLEdBQUcsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUE7SUFDMUMsQ0FBQztJQUdELElBQUksb0JBQW9CO1FBQ3BCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQTtRQUNmLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxTQUFTLEVBQUUsS0FBSyxFQUFFLElBQUk7WUFDN0QsTUFBTSxHQUFHLE1BQU0sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFBO1lBQ2hDLE1BQU0sVUFBVSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUE7WUFDdkMsTUFBTSxHQUFHLE1BQU0sR0FBRyxHQUFHLENBQUE7WUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUk7Z0JBQ3RELElBQUksS0FBSyxFQUFFO29CQUNQLE1BQU0sR0FBRyxNQUFNLEdBQUcsR0FBRyxDQUFBO2lCQUN4QjtnQkFDRCxNQUFNLEdBQUcsTUFBTSxHQUFHLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtZQUN6RixDQUFDLENBQUMsQ0FBQTtZQUNGLE1BQU0sR0FBRyxNQUFNLEdBQUcsR0FBRyxDQUFBO1FBQ3pCLENBQUMsQ0FBQyxDQUFBO1FBQ0YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLFNBQVMsRUFBRSxLQUFLLEVBQUUsSUFBSTtZQUN6QyxNQUFNLEdBQUcsTUFBTSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUE7WUFDaEMsTUFBTSxVQUFVLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQTtZQUN2QyxNQUFNLEdBQUcsTUFBTSxHQUFHLEdBQUcsQ0FBQTtZQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsRUFBRSxLQUFLLEVBQUUsSUFBSTtnQkFDdEQsSUFBSSxLQUFLLEVBQUU7b0JBQ1AsTUFBTSxHQUFHLE1BQU0sR0FBRyxHQUFHLENBQUE7aUJBQ3hCO2dCQUNELE1BQU0sR0FBRyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1lBQ3pGLENBQUMsQ0FBQyxDQUFBO1lBQ0YsTUFBTSxHQUFHLE1BQU0sR0FBRyxHQUFHLENBQUE7UUFDekIsQ0FBQyxDQUFDLENBQUE7UUFDRixPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0NBTUo7QUNqVkQsa0NBQWtDO0FBQ2xDLDRDQUE0QztBQUM1QyxpREFBaUQ7QUFDakQsbUNBQW1DO0FBaUNuQyxNQUFNLE1BQU8sU0FBUSxRQUFRO0lBUXpCLFlBQVksZ0JBQXdCLEVBQUUsdUJBQWdEO1FBRWxGLEtBQUssRUFBRSxDQUFBO1FBUlgsdUJBQWtCLEdBQXFCLEdBQUcsQ0FBQTtRQVV0QyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtRQUNwQixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtRQUUxQixNQUFNLENBQUMsdUJBQXVCLEdBQUcsdUJBQXVCLENBQUE7UUFFeEQsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7UUFFbEIsTUFBTSxlQUFlLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1FBRWpFLE1BQU0sUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLGdCQUFnQixFQUFFLGVBQWUsQ0FBQyxDQUFBO1FBRTlELFFBQVEsQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUEsQ0FBQyxNQUFNO1FBRXhDLElBQUksTUFBTSxDQUFDLHVCQUF1QixFQUFFO1lBRWhDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLFlBQVksZ0JBQWdCLENBQUM7Z0JBQ3RFLE1BQU0sQ0FBQyx1QkFBK0IsS0FBSyxnQkFBZ0IsRUFBRTtnQkFFOUQsT0FBTyxDQUFDLEdBQUcsQ0FDUCxzSEFBc0gsQ0FBQyxDQUFBO2dCQUUzSCxNQUFNLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUE7YUFFcEQ7WUFFRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxNQUFNLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLENBQUE7U0FFekU7YUFDSTtZQUVELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFBO1NBRTNEO1FBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsRUFBRSxDQUFBO1FBQ3hDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQTtRQUd2QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUNqRCxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFDbkMsVUFBVSxNQUFNLEVBQUUsS0FBSztZQUVsQixRQUFRLENBQUMsYUFBNkIsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtRQUVsRCxDQUFDLENBQ0osQ0FBQTtRQUlELE1BQU0sZUFBZSxHQUFHO1lBRXBCLDhFQUE4RTtZQUM5RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtZQUM3QyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtZQUU1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtZQUM3Qyw4QkFBOEI7WUFFOUIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztnQkFFakQsSUFBSSxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlO2dCQUMvQyxVQUFVLEVBQUUsR0FBRzthQUVsQixDQUFDLENBQUE7UUFFTixDQUFDLENBQUE7UUFFRCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUU3RCxNQUFNLFNBQVMsR0FBRztZQUVkLE1BQU07WUFFTixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDO2dCQUVqRCxJQUFJLEVBQUUsTUFBTSxDQUFDLGtCQUFrQixDQUFDLGFBQWE7Z0JBQzdDLFVBQVUsRUFBRSxHQUFHO2FBRWxCLENBQUMsQ0FBQTtRQUlOLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFWixNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUVuRCxNQUFNLGFBQWEsR0FBRztZQUVsQixNQUFNO1lBRU4sSUFBSSxDQUFDLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUVwRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDO2dCQUVqRCxJQUFJLEVBQUUsTUFBTSxDQUFDLGtCQUFrQixDQUFDLGNBQWM7Z0JBQzlDLFVBQVUsRUFBRSxHQUFHO2FBRWxCLENBQUMsQ0FBQTtRQUdOLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFWixNQUFNLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUE7UUFFdEUsYUFBYSxFQUFFLENBQUE7SUFJbkIsQ0FBQztJQVdELE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBaUI7UUFFOUIsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDbkIsT0FBTTtTQUNUO1FBRUQsUUFBUSxDQUFDLE9BQU8sQ0FBQyxrREFBa0QsR0FBRyxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQTtJQUV2RyxDQUFDOztBQXJJTSxzQkFBZSxHQUFzQixHQUFHLENBQUE7QUFxSHhDLHlCQUFrQixHQUFHO0lBRXhCLGdCQUFnQixFQUFFLGdCQUFnQjtJQUNsQyxpQkFBaUIsRUFBRSxpQkFBaUI7Q0FFdkMsQ0FBQTtBQWlCTCxNQUFNLENBQUMsdUJBQXVCLEdBQUcsR0FBRyxDQUFBO0FBR3BDLE1BQU0sVUFBVSxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0FBQzVFLE1BQU0sU0FBUyxHQUFHLGdDQUFnQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUE7QUFHNUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUMsRUFBRSxDQUFDO0lBQ2hFLElBQUksQ0FBQyxDQUFBO0lBQ0wsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO1FBQ2QsTUFBTSxJQUFJLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFBO0tBQ3pEO0lBQ0QsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUNsQixDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUE7SUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO1FBQ1QsT0FBTyxDQUFDLENBQUMsQ0FBQTtLQUNaO0lBQ0QsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUNYLFFBQVEsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0lBQ25DLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtRQUNSLE9BQU8sQ0FBQyxDQUFDLENBQUE7S0FDWjtJQUNELEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHO1FBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3RCLE9BQU8sQ0FBQyxDQUFBO1NBQ1g7UUFDRCxDQUFDLEVBQUUsQ0FBQTtLQUNOO0lBQ0QsT0FBTyxDQUFDLENBQUMsQ0FBQTtBQUNiLENBQUMsQ0FBQyxDQUFBO0FDbE5GLGtDQUFrQztBQUNsQyxrQ0FBa0M7QUFNbEMsTUFBTSxVQUFXLFNBQVEsTUFBTTtJQWlDM0IsWUFBWSxTQUFrQixFQUFFLFlBQVksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxlQUFlLEdBQUcsSUFBSTtRQUU1RixLQUFLLENBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxZQUFZLENBQUMsQ0FBQTtRQWhDbkQsZUFBVSxHQUFZLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQTtRQUdqRCxrQkFBYSxHQUFHLEdBQUcsQ0FBQTtRQUVuQixlQUFVLEdBQUcsRUFBRSxDQUFBO1FBQ2YsZUFBVSxHQUFHLEVBQUUsQ0FBQTtRQUVmLHdCQUFtQixHQUFHLENBQUMsQ0FBQTtRQUV2QixpQkFBWSxHQUFXLEdBQUcsQ0FBQTtRQUMxQixpQkFBWSxHQUFXLEdBQUcsQ0FBQTtRQUUxQixnQ0FBMkIsR0FBRyxFQUFFLENBQUE7UUFFaEMsaUJBQVksR0FBRyxFQUFFLENBQUE7UUFtQmIsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUE7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUE7UUFFeEIsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUE7UUFFZCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUE7UUFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEdBQUcsVUFBVSxDQUFBO1FBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFBO1FBRXZCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQTtRQUUvQixJQUFJLENBQUMsc0JBQXNCLEdBQUcsR0FBRyxDQUFBO1FBRWpDLElBQUksWUFBWSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBRTFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxHQUFHLENBQUE7WUFFOUIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLFVBQVUsTUFBTSxFQUFFLEtBQUs7Z0JBRXRGLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtZQUVsQixDQUFDLENBQUMsQ0FBQTtTQUdMO0lBR0wsQ0FBQztJQU1ELE1BQU0sQ0FBQyx1QkFBdUI7UUFFMUIsTUFBTSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUN2QyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUE7UUFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDNUIsVUFBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtRQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUM1QixVQUFVLENBQUMsT0FBTyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFBO0lBRS9DLENBQUM7SUFpQ0QsSUFBSSxhQUFhO1FBQ2IsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUE7UUFDbkMsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELElBQUksYUFBYSxDQUFDLGFBQXFCO1FBQ25DLElBQUksQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFBO1FBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQTtJQUN4QyxDQUFDO0lBSUQsSUFBSSxTQUFTO1FBQ1QsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQTtRQUM5QixPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBRUQsSUFBSSxTQUFTLENBQUMsS0FBYztRQUV4QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssSUFBSSxVQUFVLENBQUMsZ0JBQWdCLENBQUE7UUFDdEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUE7SUFFbEQsQ0FBQztJQUdELElBQUksWUFBWTtRQUVaLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQTtJQUU3QixDQUFDO0lBRUQsSUFBSSxZQUFZLENBQUMsWUFBcUI7UUFFbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUE7UUFHakMsSUFBSSxZQUFZLEVBQUU7WUFFZCxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUE7WUFFN0IsT0FBTTtTQUVUO1FBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO0lBRXRDLENBQUM7SUFHRCxJQUFJLGtCQUFrQjtRQUVsQixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQTtJQUVuQyxDQUFDO0lBRUQsSUFBSSxrQkFBa0IsQ0FBQyxrQkFBMEI7UUFFN0MsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksa0JBQWtCLEVBQUU7WUFFaEQsT0FBTTtTQUVUO1FBRUQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGtCQUFrQixDQUFBO1FBRTdDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQTtRQUVyQixJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQTtRQUVqQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtJQUV4RCxDQUFDO0lBRUQsMkJBQTJCLENBQUMsa0JBQTBCO0lBTXRELENBQUM7SUFNRCxJQUFJLElBQUk7UUFFSixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxDQUFBO0lBRXpELENBQUM7SUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJO1FBRVQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7UUFFakIsSUFBSSxnQkFBZ0IsR0FBRyxFQUFFLENBQUE7UUFFekIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFFekIsZ0JBQWdCLEdBQUcsdUJBQXVCLEdBQUcsVUFBVSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsR0FBRyxNQUFNO2dCQUM5RixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEdBQUcsU0FBUyxDQUFBO1NBRWhFO1FBRUQsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLGdCQUFnQixFQUFFO1lBRS9GLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLGdCQUFnQixDQUFBO1NBRTFHO1FBRUQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBRTFCLENBQUM7SUFFRCxJQUFJLFNBQVMsQ0FBQyxTQUFpQjtRQUUzQixJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQTtJQUV6QixDQUFDO0lBRUQsSUFBSSxTQUFTO1FBRVQsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQTtJQUV6QyxDQUFDO0lBSUQsT0FBTyxDQUFDLEdBQVcsRUFBRSxhQUFxQixFQUFFLFVBQTREO1FBRXBHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLGFBQWEsRUFBRSxVQUFVLENBQUMsQ0FBQTtJQUVyRCxDQUFDO0lBR0QsSUFBSSxRQUFRO1FBRVIsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFBO1FBRTFFLE1BQU0sTUFBTSxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUV2RCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBRUQsSUFBSSxRQUFRLENBQUMsUUFBZ0I7UUFHekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsRUFBRSxHQUFHLFFBQVEsR0FBRyxJQUFJLENBQUE7SUFHOUMsQ0FBQztJQU1ELG9CQUFvQixDQUFDLGNBQXNCLEdBQUcsRUFBRSxjQUFzQixHQUFHO1FBR3JFLElBQUksQ0FBQywyQkFBMkIsR0FBRyxHQUFHLENBQUE7UUFHdEMsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUE7UUFFL0IsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUE7UUFFL0IsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO0lBR3pCLENBQUM7SUFNRCxNQUFNLENBQUMsK0JBQStCLENBQ2xDLE1BQW1CLEVBQ25CLFdBQXdCLEVBQ3hCLGVBQXVCLEVBQ3ZCLFdBQW9CLEVBQ3BCLFdBQW9CO1FBR3BCLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBRW5DLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFBO1FBRzlDLE1BQU0sZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFakUsTUFBTSxlQUFlLEdBQUcsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFLOUQsSUFBSSxVQUFVLEdBQUcsZ0JBQWdCLENBQUE7UUFFakMsSUFBSSxnQkFBZ0IsR0FBRyxlQUFlLEVBQUU7WUFFcEMsVUFBVSxHQUFHLGVBQWUsQ0FBQTtTQUkvQjtRQUtELE1BQU0sa0JBQWtCLEdBQUcsZUFBZSxHQUFHLFVBQVUsQ0FBQTtRQUd2RCxJQUFJLGtCQUFrQixHQUFHLFdBQVcsRUFBRTtZQUVsQyxPQUFPLFdBQVcsQ0FBQTtTQUVyQjtRQUVELElBQUksV0FBVyxHQUFHLGtCQUFrQixFQUFFO1lBRWxDLE9BQU8sV0FBVyxDQUFBO1NBRXJCO1FBR0QsT0FBTyxrQkFBa0IsQ0FBQTtJQUk3QixDQUFDO0lBTUQsd0JBQXdCLENBQUMsS0FBMkI7UUFFaEQsS0FBSyxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFBO0lBRXpDLENBQUM7SUFHRCxtQkFBbUIsQ0FBQyxTQUFpQjtRQUVqQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUE7SUFFeEMsQ0FBQztJQU1ELGNBQWM7UUFFVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUE7UUFHdEIsSUFBSSxJQUFJLENBQUMsMkJBQTJCLEVBQUU7WUFFbEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsK0JBQStCLENBQ3RELElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQztnQkFDbkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDcEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsRUFDckMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQzNCLElBQUksQ0FBQyxRQUFRLEVBQ2IsSUFBSSxDQUFDLFlBQVksRUFDakIsSUFBSSxDQUFDLFlBQVksQ0FDcEIsQ0FBQTtTQUtKO0lBTUwsQ0FBQztJQU1ELHNCQUFzQixDQUFDLGlCQUFpQixHQUFHLENBQUM7UUFFeEMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBRW5CLE9BQU8sS0FBSyxDQUFDLHNCQUFzQixDQUFDLGlCQUFpQixDQUFDLENBQUE7U0FFekQ7UUFFRCxNQUFNLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsR0FBRztZQUMzSCxDQUFDLEVBQUUsR0FBRyxpQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFFakUsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUV0RSxJQUFJLFlBQVksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUV0QixNQUFNLEdBQUcsS0FBSyxDQUFDLHNCQUFzQixDQUFDLGlCQUFpQixDQUFDLENBQUE7WUFFeEQsVUFBVSxDQUFDLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQTtTQUV2RTtRQUdELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxrQkFBa0IsR0FBRyxDQUFDO1FBRXhDLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUVuQixPQUFPLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1NBRXpEO1FBRUQsTUFBTSxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxHQUFHLEdBQUc7WUFDM0gsQ0FBQyxFQUFFLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBRWxFLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUE7UUFFckUsSUFBSSxZQUFZLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFFdEIsTUFBTSxHQUFHLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1lBRXhELFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUE7U0FFdEU7UUFHRCxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBS0Qsb0JBQW9CO1FBRWhCLHlCQUF5QjtRQUN6QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsbUNBQW1DLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBRWpFLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7O0FBcmJNLDJCQUFnQixHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUE7QUFDckMsZ0NBQXFCLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQTtBQUV4QyxnQ0FBcUIsR0FBMEQsSUFBSSxRQUFRLEVBQVMsQ0FBQTtBQUNwRywrQkFBb0IsR0FBMEQsSUFBSSxRQUFRLEVBQVMsQ0FBQTtBQTREbkcsZUFBSSxHQUFHO0lBRVYsV0FBVyxFQUFFLEdBQUc7SUFDaEIsU0FBUyxFQUFFLElBQUk7SUFDZixTQUFTLEVBQUUsSUFBSTtJQUNmLFNBQVMsRUFBRSxJQUFJO0lBQ2YsU0FBUyxFQUFFLElBQUk7SUFDZixTQUFTLEVBQUUsSUFBSTtJQUNmLFNBQVMsRUFBRSxJQUFJO0lBQ2YsVUFBVSxFQUFFLFVBQVU7SUFDdEIsV0FBVyxFQUFFLE9BQU87SUFDcEIsTUFBTSxFQUFFLE1BQU07SUFDZCxPQUFPLEVBQUUsT0FBTztDQUVuQixDQUFBO0FBSU0sd0JBQWEsR0FBRztJQUVuQixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSxRQUFRO0lBQ2xCLE9BQU8sRUFBRSxPQUFPO0lBQ2hCLFNBQVMsRUFBRSxTQUFTO0NBRXZCLENBQUE7QUF3V0wsVUFBVSxDQUFDLHVCQUF1QixFQUFFLENBQUE7QUFNcEMsTUFBTTtBQUNOLHdHQUF3RztBQUN4RyxNQUFNO0FBQ04sbURBQW1EO0FBQ25ELCtHQUErRztBQUMvRyxNQUFNO0FBQ04sNEdBQTRHO0FBQzVHLE1BQU07QUFDTix3Q0FBd0M7QUFDeEMscURBQXFEO0FBQ3JELHdHQUF3RztBQUN4Ryw2Q0FBNkM7QUFDN0MsMkJBQTJCO0FBQzNCLCtDQUErQztBQUMvQyxzQkFBc0I7QUFDdEIsSUFBSTtBQ2xmSixxREFBcUQ7QUFDckQsMkNBQTJDO0FBTTNDLE1BQU0scUJBQXNCLFNBQVEsZ0JBQWdCO0lBb0JoRCxZQUFZLElBQUk7UUFFWixnQkFBZ0I7UUFDaEIsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO1FBaEJQLGtCQUFhLEdBQUcsT0FBTyxDQUMzQjs7Ozs7Ozs7VUFRRSxDQUNMLENBQUE7UUFRRyxzQ0FBc0M7SUFFMUMsQ0FBQztJQUdELDBCQUEwQjtRQUV0QixLQUFLLENBQUMsMEJBQTBCLEVBQUUsQ0FBQTtRQUNsQyxJQUFJLENBQUMsVUFBVSxHQUFHLGdCQUFnQixDQUFBO0lBRXRDLENBQUM7SUFRRCxZQUFZO1FBRVIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQTtRQUU5QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQyxDQUFBO1FBQ25FLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQTtRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7UUFFcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUE7UUFHekQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxZQUFZLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUM3RixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQTtRQUM3RCxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQTtRQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUE7UUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBR3JDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDLENBQUE7UUFDekUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFBO1FBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUcxQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUE7UUFDcEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFBO1FBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQTtJQUV6QyxDQUFDO0lBR0ssV0FBVyxDQUFDLEtBQWM7OztZQUU1QixxQkFBaUIsWUFBQyxLQUFLLEVBQUM7WUFDeEIsTUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsMkJBQTJCLENBQUMscUJBQXFCLENBQUMsQ0FBQTtZQUVqRixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQTtZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7WUFFM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFBO1lBRWhDLElBQUk7Z0JBRUEsSUFBSSxJQUFJLEdBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUE7Z0JBRTNGLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLG9CQUFvQixDQUFDLENBQUE7Z0JBQ2xFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7Z0JBQ2pDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUE7Z0JBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUE7Z0JBRWpDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO2FBRTlDO1lBQUMsT0FBTyxTQUFTLEVBQUU7Z0JBRWhCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLGlDQUFpQyxDQUFBO2FBRTNEO1lBRUQsS0FBSyxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFFaEQsQ0FBQztLQUFBO0lBR0QsbUJBQW1CO1FBRWYsS0FBSyxDQUFDLG1CQUFtQixFQUFFLENBQUE7UUFFM0IsTUFBTSxPQUFPLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFBO1FBQ2hELE1BQU0sV0FBVyxHQUFHLE9BQU8sQ0FBQTtRQUUzQixjQUFjO1FBQ2QsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUE7UUFFN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQTtRQUV4RSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEdBQUcsR0FBRyxFQUFFLE9BQU8sR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNuRyxtQkFBbUIsQ0FDaEIsT0FBTyxFQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQ3JFLENBQUE7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FDbEUsT0FBTyxFQUNQLElBQUksQ0FBQyxlQUFlLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQzNFLENBQUE7UUFFRCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FDbEUsT0FBTyxFQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQzNFLENBQUE7SUFFTCxDQUFDO0lBR0Qsc0JBQXNCLENBQUMsb0JBQTRCLENBQUM7UUFFaEQsTUFBTSxPQUFPLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFBO1FBQ2hELE1BQU0sV0FBVyxHQUFHLE9BQU8sR0FBRyxHQUFHLENBQUE7UUFFakMsTUFBTSxNQUFNLEdBQUcsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsT0FBTyxHQUFHLENBQUM7WUFDckQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxpQkFBaUIsQ0FBQztZQUN6RCxJQUFJLENBQUMsZUFBZSxDQUFDLHNCQUFzQixDQUFDLGlCQUFpQixDQUFDO1lBQzlELElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtRQUU3RCxPQUFPLE1BQU0sQ0FBQTtJQUdqQixDQUFDOztBQWhIZSx3Q0FBa0IsR0FBRyxTQUFTLENBQUE7QUFFOUIsNkNBQXVCLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUE7QUM5QzVELG1DQUFtQztBQUduQyxvQkFBb0I7QUFFcEIsYUFBYTtBQUNiLE1BQU0sT0FBTyxHQUFRLE9BQU8sQ0FBQywrQ0FBK0MsRUFBRTtJQUMxRSxNQUFNLEVBQUUsTUFBTTtJQUNkLGNBQWM7SUFDZCxNQUFNLEVBQUUsSUFBSTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsMENBQTBDO0lBQzFDLHNEQUFzRDtJQUN0RCxLQUFLO0lBQ0wsZUFBZTtJQUNmLDJEQUEyRDtJQUMzRCxpQ0FBaUM7SUFDakMscUNBQXFDO0lBQ3JDLElBQUk7Q0FDUCxDQUFDLENBQUE7QUNyQkYsOENBQThDO0FBTTlDLE1BQU0sUUFBUyxTQUFRLFFBQVE7SUFNM0IsWUFBWSxTQUFrQixFQUFFLFdBQW9CO1FBRWhELEtBQUssQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLENBQUE7UUFFN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUE7UUFDdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7SUFLOUIsQ0FBQztJQU1ELFFBQVEsQ0FBQyxTQUFpQixFQUFFLGVBQTRCLEVBQUUsWUFBWTtRQUVsRSxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxlQUFlLEVBQUUsWUFBWSxDQUFDLENBQUE7UUFHeEQsaUNBQWlDO1FBRWpDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQTtRQUczQixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQTtRQUNsRCxJQUFJLENBQUMsa0NBQWtDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBRTFELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFBO0lBR3ZELENBQUM7SUFNRCxrQ0FBa0MsQ0FBQyxxQkFBOEI7UUFFN0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLHFCQUFxQixDQUFBO1FBRXJELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUEsQ0FBQyxvREFBb0Q7UUFFekgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxHQUFHLHFCQUFxQixDQUFBLENBQUMsbURBQW1EO1FBRTFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUEsQ0FBQyxtREFBbUQ7UUFFNUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUE7UUFFcEQsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUE7SUFFdkMsQ0FBQztJQUlELDJCQUEyQjtRQUV2QixLQUFLLENBQUMsMkJBQTJCLEVBQUUsQ0FBQTtRQUVuQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUVwQix1REFBdUQ7SUFFM0QsQ0FBQztJQUdELDRCQUE0QjtRQUV4QixLQUFLLENBQUMsNEJBQTRCLEVBQUUsQ0FBQTtRQUVwQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUVwQiw2RUFBNkU7UUFFN0UsdURBQXVEO0lBRTNELENBQUM7SUFFRCw0QkFBNEI7UUFFeEIsS0FBSyxDQUFDLDRCQUE0QixFQUFFLENBQUE7UUFFcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUV2Qyw2RUFBNkU7UUFFN0UsdURBQXVEO0lBRTNELENBQUM7SUFJRCxnQ0FBZ0M7UUFFNUIsS0FBSyxDQUFDLGdDQUFnQyxFQUFFLENBQUE7UUFFeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7UUFFcEIsNEVBQTRFO1FBRTVFLHVEQUF1RDtJQUUzRCxDQUFDO0lBTUQsbUNBQW1DO1FBRS9CLEtBQUssQ0FBQyxtQ0FBbUMsRUFBRSxDQUFBO1FBRTNDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUV0QixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQTtZQUVsRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1lBRTdDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFBO1NBRWpCO0lBRUwsQ0FBQztDQU1KO0FDN0lELHNDQUFzQztBQUl0QyxNQUFNLFlBQWEsU0FBUSxRQUFRO0lBNEIvQixZQUFZLFNBQWtCLEVBQUUsV0FBb0I7UUFFaEQsS0FBSyxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsQ0FBQTtRQUU3QixJQUFJLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQTtRQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtJQUs5QixDQUFDO0lBTUQsUUFBUSxDQUFDLFNBQWlCLEVBQUUsZUFBNEIsRUFBRSxZQUFZO1FBRWxFLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxZQUFZLENBQUMsQ0FBQTtRQUV4RCxJQUFJLENBQUMsTUFBTSxHQUFHO1lBRVYsVUFBVSxFQUFFO2dCQUVSLE1BQU0sRUFBRSxPQUFPLENBQUMsU0FBUztnQkFDekIsV0FBVyxFQUFFLE9BQU8sQ0FBQyxTQUFTO2dCQUM5QixRQUFRLEVBQUUsT0FBTyxDQUFDLFNBQVM7YUFFOUI7WUFDRCxVQUFVLEVBQUU7Z0JBRVIsTUFBTSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0I7Z0JBQ2hDLE9BQU8sRUFBRSxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUM7Z0JBQy9CLFdBQVcsRUFBRSxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUM7Z0JBQ25DLFFBQVEsRUFBRSxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUM7YUFFbkM7U0FFSixDQUFBO0lBSUwsQ0FBQztJQU1ELElBQUksZUFBZSxDQUFDLGVBQXdCO1FBR3hDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxlQUFlLENBQUE7UUFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FBQTtRQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsZUFBZSxDQUFBO1FBR2pELElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFBO0lBRXZDLENBQUM7SUFFRCxJQUFJLGVBQWU7UUFFZixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUE7UUFFNUMsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQU1ELDJCQUEyQjtRQUV2QixRQUFRLENBQUMsU0FBUyxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUV6RCx1REFBdUQ7SUFFM0QsQ0FBQztJQUdELDRCQUE0QjtRQUV4QixRQUFRLENBQUMsU0FBUyxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUkxRCw2RUFBNkU7UUFFN0Usd0RBQXdEO0lBRTVELENBQUM7SUFFRCw0QkFBNEI7UUFFeEIsUUFBUSxDQUFDLFNBQVMsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFJMUQsNkVBQTZFO1FBRTdFLHVEQUF1RDtJQUUzRCxDQUFDO0lBSUQsZ0NBQWdDO1FBRTVCLFFBQVEsQ0FBQyxTQUFTLENBQUMsZ0NBQWdDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBSTlELDRFQUE0RTtRQUU1RSx1REFBdUQ7SUFFM0QsQ0FBQzs7QUEzSU0sbUJBQU0sR0FBRztJQUVaLFVBQVUsRUFBRTtRQUVSLE1BQU0sRUFBRSxPQUFPLENBQUMsU0FBUztRQUN6QixXQUFXLEVBQUUsT0FBTyxDQUFDLFNBQVM7UUFDOUIsUUFBUSxFQUFFLE9BQU8sQ0FBQyxTQUFTO0tBRTlCO0lBQ0QsVUFBVSxFQUFFO1FBRVIsTUFBTSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0I7UUFDaEMsT0FBTyxFQUFFLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQztRQUMvQixXQUFXLEVBQUUsSUFBSSxPQUFPLENBQUMsU0FBUyxDQUFDO1FBQ25DLFFBQVEsRUFBRSxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUM7S0FFbkM7Q0FFSixDQUFBO0FDNUJMLGtEQUFrRDtBQU1sRCxNQUFNLFlBQWEsU0FBUSxZQUFZO0lBTW5DLFlBQVksU0FBa0IsRUFBRSxXQUFvQjtRQUVoRCxLQUFLLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQyxDQUFBO1FBRTdCLElBQUksQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFBO1FBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsWUFBWSxDQUFBO0lBS2xDLENBQUM7SUFNRCxRQUFRLENBQUMsU0FBaUIsRUFBRSxlQUE0QixFQUFFLFlBQVk7UUFFbEUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLFlBQVksQ0FBQyxDQUFBO1FBRXhELElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtRQUVqQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxFQUFFLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUUvRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtJQUtoQyxDQUFDO0NBTUo7QUMvQ0QsNENBQTRDO0FBTTVDLE1BQU0sUUFBUyxTQUFRLFFBQVE7SUFTM0IseUJBQXlCO0lBRXpCLFlBQVksU0FBa0IsRUFBRSxpQkFBeUIsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJO1FBRXpFLEtBQUssQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFBO1FBVi9DLGVBQVUsR0FBRyxFQUFFLENBQUE7UUFHZixjQUFTLEdBQUcsQ0FBQyxDQUFBO1FBQ2IsZUFBVSxHQUFHLENBQUMsQ0FBQTtRQVFWLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFBO1FBQ3RCLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFBO1FBRXhCLG1GQUFtRjtRQUNuRixvQ0FBb0M7UUFFcEMsSUFBSSxDQUFDLDhCQUE4QixFQUFFLENBQUE7SUFNekMsQ0FBQztJQU1ELElBQUksU0FBUyxDQUFDLFNBQWtCO1FBRTVCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFBO1FBRTNCLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFBO0lBRXpDLENBQUM7SUFFRCxJQUFJLFNBQVM7UUFFVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUE7SUFFMUIsQ0FBQztJQUdELDhCQUE4QjtRQUcxQixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFFakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFBO1lBRTdCLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFBO1lBRTNDLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFBO1lBRTNDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFBO1lBRS9ELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxFQUFFLENBQUE7WUFFaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFBO1lBRXZCLElBQUksQ0FBQyxNQUFNLEdBQUc7Z0JBRVYsVUFBVSxFQUFFO29CQUVSLE1BQU0sRUFBRSxPQUFPLENBQUMsU0FBUztvQkFDekIsV0FBVyxFQUFFLE9BQU8sQ0FBQyxTQUFTO29CQUM5QixRQUFRLEVBQUUsT0FBTyxDQUFDLFNBQVM7aUJBRTlCO2dCQUNELFVBQVUsRUFBRTtvQkFFUixNQUFNLEVBQUUsT0FBTyxDQUFDLGdCQUFnQjtvQkFDaEMsT0FBTyxFQUFFLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQztvQkFDL0IsV0FBVyxFQUFFLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQztvQkFDbkMsUUFBUSxFQUFFLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQztpQkFFbkM7YUFFSixDQUFBO1NBR0o7YUFDSTtZQUVELElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtZQUV0QixJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixHQUFHLEdBQUcsQ0FBQTtZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixHQUFHLEdBQUcsQ0FBQTtZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQTtZQUU3RCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsR0FBRyxDQUFBO1lBRWpDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQTtZQUUzQixJQUFJLENBQUMsTUFBTSxHQUFHO2dCQUVWLFVBQVUsRUFBRTtvQkFFUixNQUFNLEVBQUUsT0FBTyxDQUFDLFVBQVU7b0JBQzFCLFdBQVcsRUFBRSxPQUFPLENBQUMsVUFBVTtvQkFDL0IsUUFBUSxFQUFFLE9BQU8sQ0FBQyxVQUFVO2lCQUUvQjtnQkFDRCxVQUFVLEVBQUU7b0JBRVIsTUFBTSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0I7b0JBQ2hDLFdBQVcsRUFBRSxPQUFPLENBQUMsZ0JBQWdCO29CQUNyQyxRQUFRLEVBQUUsT0FBTyxDQUFDLGdCQUFnQjtpQkFFckM7YUFFSixDQUFBO1NBR0o7UUFFRCxJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQTtJQUl2QyxDQUFDO0lBTUQsMEJBQTBCO1FBRXRCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUV0QixPQUFNO1NBRVQ7UUFHRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQTtRQUV6RSxJQUFJLENBQUMsZUFBZSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQTtJQUtwRCxDQUFDO0lBR0QsSUFBSSxnQkFBZ0IsQ0FBQyxXQUFtQjtRQUlwQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUVqQixJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQTtZQUVqQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUE7WUFFOUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7U0FFeEM7YUFDSTtZQUVELElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtTQUU3QztJQUdMLENBQUM7SUFFRCxJQUFJLGdCQUFnQjtRQUVoQixJQUFJLE1BQU0sR0FBRyxHQUFHLENBQUE7UUFFaEIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBRXRCLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQTtTQUU1QztRQUVELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFNRCxjQUFjO1FBRVYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBRXRCLE1BQU0sT0FBTyxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQTtRQUNoRCxNQUFNLFdBQVcsR0FBRyxPQUFPLENBQUE7UUFFM0IsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUcxQixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUE7UUFFcEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEVBQUUsR0FBRyxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7UUFDdEYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsR0FBRyxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7UUFFeEYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQTtRQUV4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFBO1FBRXpDLDhDQUE4QztRQUc5QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO1lBRWhFLDZDQUE2QztZQUM3QyxxSUFBcUk7WUFFckksSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxHQUFHLEdBQUc7Z0JBQ2pGLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUE7WUFFakYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUU7Z0JBQzVCLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7U0FFL0Y7SUFNTCxDQUFDO0NBS0o7QUNsUEQsTUFBTSxpQkFBa0IsU0FBUSxNQUFNO0lBT2xDLFlBQVksU0FBa0I7UUFFMUIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBSnBCLFVBQUssR0FBVyxFQUFFLENBQUE7UUFNZCxJQUFJLENBQUMsTUFBTSxHQUFHLGlCQUFpQixDQUFBO1FBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFBO1FBRXhCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUMsQ0FBQTtRQUVqRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUVuQyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFFbEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUE7SUFHckIsQ0FBQztJQU1ELElBQUksSUFBSSxDQUFDLElBQVk7UUFFakIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7UUFFakIsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUE7SUFFckMsQ0FBQztJQUVELElBQUksSUFBSTtRQUVKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQTtJQUVyQixDQUFDO0lBSUQsSUFBSSxNQUFNLENBQUMsTUFBZTtRQUV0QixLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtRQUVyQixJQUFJLE1BQU0sRUFBRTtZQUVSLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtTQUUzQzthQUNJO1lBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7U0FFdEM7SUFFTCxDQUFDO0lBRUQsS0FBSztRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0lBRXBCLENBQUM7SUFFRCxJQUFJO1FBRUEsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUE7SUFFckIsQ0FBQztJQUlELGNBQWM7UUFFVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFdEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUUxQix5Q0FBeUM7UUFFekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7UUFDckUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7UUFFcEUsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRS9ELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUcsT0FBTyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7UUFDckUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEVBQUUsR0FBRyxPQUFPLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQTtRQUVwRSxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFFMUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQTtRQUMzRixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFBO0lBSy9GLENBQUM7Q0FNSjtBQzFHRCxNQUFNLGVBQWdCLFNBQVEsTUFBTTtJQU1oQyxZQUFZLFNBQWlCLEVBQUUsT0FBZSxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVE7UUFFdkUsS0FBSyxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFFOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxlQUFlLENBQUE7UUFDN0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUE7UUFHeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFBO1FBRS9DLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ2hGLENBQUMsQ0FBQTtJQU1MLENBQUM7SUFZRCxJQUFJLHFCQUFxQjtRQUVyQixPQUFPLEtBQUssQ0FBQyxxQkFBNEIsQ0FBQTtJQUU3QyxDQUFDO0lBNEJELElBQUksSUFBSTtRQUVKLE1BQU0sTUFBTSxHQUFHLElBQUksSUFBSSxDQUFFLElBQUksQ0FBQyxlQUFvQyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRXpFLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7O0FBNUNNLDRCQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsTUFBTSxDQUFDLFlBQVksRUFBRTtJQUV6RCxhQUFhLEVBQUUsYUFBYTtDQUUvQixDQUFDLENBQUE7QUFZSyxvQkFBSSxHQUFHO0lBRVYsTUFBTSxFQUFFLE1BQU07SUFDZCxNQUFNLEVBQUUsTUFBTTtJQUNkLFVBQVUsRUFBRSxVQUFVO0NBR3pCLENBQUE7QUFHTSxzQkFBTSxHQUFHO0lBRVosVUFBVSxFQUFFLFlBQVk7SUFDeEIsYUFBYSxFQUFFLFlBQVk7SUFDM0IsVUFBVSxFQUFFLFlBQVk7Q0FFM0IsQ0FBQTtBQ3JETCxNQUFNLFdBQVksU0FBUSxNQUFNO0lBWTVCLFlBQVksU0FBVSxFQUFFLGVBQWUsR0FBRyxJQUFJO1FBRTFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBTDVDLHFCQUFnQixHQUFHLEVBQUUsQ0FBQTtRQU9qQixJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQTtRQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQTtRQUV4Qiw4RUFBOEU7SUFHbEYsQ0FBQztJQW1CRCxJQUFJLGVBQWU7UUFFZixPQUFPLEtBQUssQ0FBQyxlQUFtQyxDQUFBO0lBRXBELENBQUM7SUFVRCxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxRQUFRO1FBQ3hCLE1BQU0sR0FBRyxHQUFHLElBQUksY0FBYyxFQUFFLENBQUE7UUFDaEMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFDcEIsR0FBRyxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUE7UUFDekIsR0FBRyxDQUFDLE1BQU0sR0FBRztZQUNULE1BQU0sRUFBRSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUE7WUFFM0IsRUFBRSxDQUFDLE1BQU0sR0FBRztnQkFDUixRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ3pCLENBQUMsQ0FBQTtZQUVELEVBQUUsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBLENBQUMsYUFBYTtRQUNoRCxDQUFDLENBQUE7UUFFRCxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUE7SUFDZCxDQUFDO0lBSUQsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFNBQWlCLEVBQUUsT0FBZSxFQUFFLFVBQTZDO1FBRXZHLE1BQU0sU0FBUyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUE7UUFDbkMsU0FBUyxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUE7UUFFakMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUc7WUFFL0IsTUFBTSxZQUFZLEdBQUcsU0FBUyxDQUFDLG9CQUFvQixFQUFFLENBQUE7WUFHckQsSUFBSSxVQUFVLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUE7WUFFNUUsVUFBVSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFBO1lBR3BDLE1BQU0sTUFBTSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxDQUFDLFlBQVksRUFBRSxDQUFDLFlBQVksQ0FBQyxLQUFLO2dCQUNwRyxVQUFVLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUU3QixVQUFVLENBQUMsTUFBTSxDQUFDLENBQUE7UUFFdEIsQ0FBQyxDQUFBO0lBRUwsQ0FBQztJQUdELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FDbkIsU0FBaUIsRUFDakIsTUFBYyxFQUNkLEtBQWEsRUFDYixVQUE2QztRQUc3QyxNQUFNLFNBQVMsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFBO1FBQ25DLFNBQVMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFBO1FBRWpDLFNBQVMsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHO1lBRS9CLE1BQU0sTUFBTSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFBO1lBQ2xELFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUV0QixDQUFDLENBQUE7SUFFTCxDQUFDO0lBR0QsVUFBVSxDQUFDLE1BQWUsRUFBRSxLQUFjO1FBRXRDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7UUFFaEMsaUNBQWlDO1FBQ2pDLE1BQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUE7UUFDL0MsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUE7UUFDcEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUE7UUFFdEIsd0NBQXdDO1FBQ3hDLE1BQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDbkMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFdkMsbUNBQW1DO1FBQ25DLDREQUE0RDtRQUM1RCxnRUFBZ0U7UUFDaEUsNEJBQTRCO1FBQzVCLE1BQU0sT0FBTyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFN0MsT0FBTyxPQUFPLENBQUE7UUFFZCwrREFBK0Q7SUFFbkUsQ0FBQztJQUtELElBQUksV0FBVztRQUVYLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUE7SUFFbkMsQ0FBQztJQUVELElBQUksV0FBVyxDQUFDLFlBQW9CO1FBRWhDLElBQUksTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ3RCLFlBQVksR0FBRyxFQUFFLENBQUE7U0FDcEI7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUE7UUFFdkMsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtTQUN6QztRQUVELElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1lBRXZDLDhCQUE4QjtZQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQTtZQUVqQixPQUFNO1NBRVQ7YUFDSTtZQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO1NBRW5CO1FBRUQsbURBQW1EO1FBQ25ELDJDQUEyQztRQUMzQyxnQ0FBZ0M7UUFDaEMsMERBQTBEO1FBRTFELGFBQWE7UUFDYixJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxVQUE2QixLQUFZO1lBRW5FLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUE7WUFFL0IsNkNBQTZDO1FBRWpELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7SUFFaEIsQ0FBQztJQUlELGNBQWMsQ0FBQyxHQUFXLEVBQUUsYUFBcUI7UUFFN0MsTUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQTtRQUM5RCxJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFBO0lBRWpHLENBQUM7SUFJRCx3QkFBd0IsQ0FBQyxLQUEyQjtRQUVoRCxLQUFLLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFckMsSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLElBQUksS0FBSyxDQUFDLElBQUk7WUFDckUsTUFBTSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRTtZQUUzQyxJQUFJLENBQUMsZ0NBQWdDLEVBQUUsQ0FBQTtTQUUxQztJQUVMLENBQUM7SUFHRCxtQkFBbUIsQ0FBQyxTQUFpQjtRQUVqQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUE7UUFFcEMsSUFBSSxDQUFDLGdDQUFnQyxFQUFFLENBQUE7SUFFM0MsQ0FBQztJQUVELGdDQUFnQztRQUU1QixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUV4QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1NBRTVEO0lBRUwsQ0FBQztJQUlELElBQUksUUFBUTtRQUdSLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQTtJQUV6QixDQUFDO0lBRUQsSUFBSSxRQUFRLENBQUMsUUFBUTtRQUVqQixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUV6QixJQUFJLENBQUMsS0FBYSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUE7SUFFNUMsQ0FBQztJQUdELElBQUksZUFBZTtRQUNmLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFBO0lBQ2hDLENBQUM7SUFFRCxJQUFJLGVBQWUsQ0FBQyxlQUF3QjtRQUN4QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFBO1FBQ3ZDLElBQUksZUFBZSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtTQUN6QztJQUNMLENBQUM7SUFNRCxrQkFBa0IsQ0FBQyxTQUFpQjtRQUVoQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLENBQUE7SUFNdkMsQ0FBQztJQU1ELGNBQWM7UUFFVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUE7SUFJMUIsQ0FBQztJQUtELG9CQUFvQjtRQUdoQixNQUFNLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUE7UUFFM0csT0FBTyxNQUFNLENBQUE7SUFHakIsQ0FBQztJQUVELG1DQUFtQyxDQUFDLGtCQUFrQixHQUFHLENBQUMsRUFBRSxpQkFBaUIsR0FBRyxDQUFDO1FBRTdFLE1BQU0sV0FBVyxHQUFHLGtCQUFrQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFBO1FBRTNFLE1BQU0sVUFBVSxHQUFHLGlCQUFpQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFBO1FBRXhFLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBRXBELE1BQU0sTUFBTSxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhO1lBQ25FLFVBQVUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxVQUFVLENBQUMsQ0FBQTtRQUUvRCxPQUFPLE1BQU0sQ0FBQTtJQUdqQixDQUFDOztBQXJTTSxvQkFBUSxHQUFHO0lBRWQsZUFBZSxFQUFFLE1BQU07SUFDdkIsV0FBVyxFQUFFLFNBQVM7SUFDdEIsWUFBWSxFQUFFLE9BQU87SUFDckIsUUFBUSxFQUFFLE1BQU07SUFDaEIsbUJBQW1CLEVBQUUsWUFBWTtDQUVwQyxDQUFBO0FDNUNMLE1BQU0sc0JBQXVCLFNBQVEsUUFBUTtJQWN6QyxZQUFZLG9CQUFvQixHQUFHLEVBQUU7UUFHakMsS0FBSyxFQUFFLENBQUE7UUFQWCxvQkFBZSxHQUFHLEVBQUUsQ0FBQTtRQUVwQixlQUFVLEdBQUcsc0JBQXNCLENBQUMsZ0JBQWdCLENBQUE7UUFPaEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQTtRQUNwQyxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQTtRQUcxQixJQUFJLG9CQUFvQixFQUFFO1lBRXRCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxNQUFNLENBQUMscURBQXFELENBQUMsQ0FBQTtTQUV0RjtRQUVELHNCQUFzQixDQUFDLGVBQWUsR0FBRyxzQkFBc0IsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFBO1FBQ25GLElBQUksQ0FBQyxlQUFlLEdBQUcsc0JBQXNCLENBQUMsZUFBZSxDQUFBO0lBR2pFLENBQUM7SUFNRCxJQUFJLGtCQUFrQjtRQUVsQixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUE7SUFFL0IsQ0FBQztJQU1ELFVBQVUsQ0FDTixlQUF1QixFQUN2QixJQUFXLEVBQ1gsWUFBc0IsRUFDdEIsV0FBbUIsRUFDbkIsVUFBZSxFQUNmLFVBQXdGO1FBSXhGLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUV0QixPQUFNO1NBRVQ7UUFFRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUE7UUFFNUIsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUE7UUFFbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsVUFBVSxPQUFPO1lBRXpDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxrQkFBa0IsRUFBRTtnQkFFdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxTQUFTLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQTtnQkFFbEUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7YUFFL0Y7UUFFTCxDQUFDLENBQUE7UUFHRCxJQUFJO1lBRUEsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7Z0JBRXhCLGlCQUFpQixFQUFFLGVBQWU7Z0JBQ2xDLE1BQU0sRUFBRSxJQUFJO2dCQUNaLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixhQUFhLEVBQUUsV0FBVztnQkFDMUIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLG9CQUFvQixFQUFFLGtCQUFrQjthQUUzQyxDQUFDLENBQUE7U0FFTDtRQUFDLE9BQU8sU0FBUyxFQUFFO1lBRWhCLFVBQVUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1NBRWpDO0lBTUwsQ0FBQztJQU1ELFdBQVc7UUFFUCxJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQTtRQUUxQixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksc0JBQXNCLENBQUMsZ0JBQWdCLEVBQUU7WUFFNUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsQ0FBQTtTQUU5QjtJQUlMLENBQUM7O0FBeEhNLHVDQUFnQixHQUFHLElBQUksTUFBTSxDQUFDLHFEQUFxRCxDQUFDLENBQUE7QUFFcEYsc0NBQWUsR0FBRyxDQUFDLENBQUMsQ0FBQTtBQ1UvQixNQUFNLHNCQUF1QixTQUFRLFFBQVE7SUFjekMsWUFBWSxvQkFBb0IsR0FBRyxFQUFFO1FBR2pDLEtBQUssRUFBRSxDQUFBO1FBUFgsb0JBQWUsR0FBRyxFQUFFLENBQUE7UUFFcEIsZUFBVSxHQUFHLHNCQUFzQixDQUFDLGdCQUFnQixDQUFBO1FBT2hELElBQUksQ0FBQyxNQUFNLEdBQUcsc0JBQXNCLENBQUE7UUFDcEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7UUFHMUIsSUFBSSxvQkFBb0IsRUFBRTtZQUV0QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksTUFBTSxDQUFDLHFEQUFxRCxDQUFDLENBQUE7U0FFdEY7UUFFRCxzQkFBc0IsQ0FBQyxlQUFlLEdBQUcsc0JBQXNCLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQTtRQUNuRixJQUFJLENBQUMsZUFBZSxHQUFHLHNCQUFzQixDQUFDLGVBQWUsQ0FBQTtJQUdqRSxDQUFDO0lBTUQsSUFBSSxrQkFBa0I7UUFFbEIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFBO0lBRS9CLENBQUM7SUF3QkQsUUFBUSxDQUNKLElBQVMsRUFDVCxtQkFBK0QsRUFDL0QsVUFBZSxFQUNmLFVBQStFO1FBSS9FLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUV0QixPQUFNO1NBRVQ7UUFFRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUE7UUFFNUIsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUE7UUFFbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsVUFBVSxPQUFPO1lBRXpDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxrQkFBa0IsRUFBRTtnQkFFdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsU0FBUyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUE7Z0JBRTFGLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO2FBRTNGO1FBRUwsQ0FBQyxDQUFBO1FBR0QsSUFBSTtZQUVBLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO2dCQUV4QixNQUFNLEVBQUUsSUFBSTtnQkFDWixxQkFBcUIsRUFBRSxtQkFBbUI7Z0JBQzFDLFlBQVksRUFBRSxVQUFVO2dCQUN4QixvQkFBb0IsRUFBRSxrQkFBa0I7YUFFM0MsQ0FBQyxDQUFBO1NBRUw7UUFBQyxPQUFPLFNBQVMsRUFBRTtZQUVoQixVQUFVLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQTtTQUVqQztJQU1MLENBQUM7SUFNRCxVQUFVLENBQ04sSUFBUyxFQUNULG1CQUErRCxFQUMvRCxhQUFrQixPQUFPLEVBQUU7UUFHM0IsTUFBTSxNQUFNLEdBTVAsSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFakMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLENBQUMsVUFBVSxFQUFFLGFBQWEsRUFBRSxnQkFBZ0IsRUFBRSxFQUFFO2dCQUVqRyxJQUFJLGdCQUFnQixJQUFJLFVBQVUsRUFBRTtvQkFFaEMsT0FBTyxDQUFDO3dCQUVKLFVBQVUsRUFBRSxVQUFVO3dCQUN0QixhQUFhLEVBQUUsYUFBYTt3QkFDNUIsVUFBVSxFQUFFLGdCQUFnQjtxQkFFL0IsQ0FBQyxDQUFBO2lCQUVMO1lBSUwsQ0FBQyxDQUFDLENBQUE7UUFJTixDQUFDLENBQUMsQ0FBQTtRQUVGLE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFNRCxXQUFXO1FBRVAsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUE7UUFFMUIsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLHNCQUFzQixDQUFDLGdCQUFnQixFQUFFO1lBRTVELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLENBQUE7U0FFOUI7SUFJTCxDQUFDOztBQWxMTSx1Q0FBZ0IsR0FBRyxJQUFJLE1BQU0sQ0FBQyxxREFBcUQsQ0FBQyxDQUFBO0FBRXBGLHNDQUFlLEdBQUcsQ0FBQyxDQUFDLENBQUE7QUE0Q3BCLCtCQUFRLEdBQUc7SUFFZCxRQUFRLEVBQUUsUUFBUTtDQUVyQixDQUFBO0FBR00sZ0NBQVMsR0FBRztJQUVmLFlBQVksRUFBRSxZQUFZO0lBQzFCLFdBQVcsRUFBRSxXQUFXO0NBRTNCLENBQUE7QUM1RUwsTUFBTSxZQUFhLFNBQVEsUUFBUTtJQVMvQixZQUFZLEtBQWtCO1FBRTFCLEtBQUssRUFBRSxDQUFBO1FBTFgsZUFBVSxHQUFtQixFQUFFLENBQUE7UUFPM0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUE7UUFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7UUFFMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUE7SUFFdkIsQ0FBQztJQU1ELFVBQVUsQ0FBQyxjQUFzQjtRQUk3QixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUk3QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUVyQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUE7YUFJaEI7U0FFSjtJQUtMLENBQUM7Q0FNSjtBQ25ERCxvQ0FBb0M7QUFNcEMsTUFBTSxrQkFBbUIsU0FBUSxNQUFNO0lBS25DLFlBQVksU0FBUyxFQUFFLGVBQWdCO1FBRW5DLEtBQUssQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUE7UUFKckMsc0JBQWlCLEdBQUcsQ0FBQyxDQUFBO1FBTWpCLElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUE7UUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUE7UUFHeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUE7UUFFOUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFBO1FBRTVCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFFdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsVUFBb0MsS0FBYztZQUc5RixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBO1lBRXRHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztnQkFFekIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhO2dCQUM3QyxVQUFVLEVBQUUsR0FBRzthQUVsQixDQUFDLENBQUE7UUFHTixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7SUFHakIsQ0FBQztJQU1ELG1CQUFtQixDQUFDLGNBQXVCO0lBSTNDLENBQUM7SUFJRCxJQUFJLFFBQVE7UUFDUixNQUFNLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxDQUFBO1FBQ2pELE9BQU8sTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFFRCxJQUFJLFFBQVEsQ0FBQyxPQUFnQjtRQUN6QixJQUFJLE9BQU8sRUFBRTtZQUNULElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQTtTQUNsQzthQUNJO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFBO1NBQ2xDO0lBQ0wsQ0FBQztJQUlELElBQUksUUFBUTtRQUNSLE1BQU0sTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLENBQUE7UUFDakQsT0FBTyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQUVELElBQUksUUFBUSxDQUFDLE9BQWdCO1FBQ3pCLElBQUksT0FBTyxFQUFFO1lBQ1QsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFBO1NBQ2xDO2FBQ0k7WUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUE7U0FDbEM7SUFDTCxDQUFDO0lBSUQsSUFBSSxhQUFhO1FBQ2IsTUFBTSxNQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUMzRixPQUFPLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBR0QsSUFBSSxhQUFhLENBQUMsV0FBb0I7UUFFbEMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFFeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUE7WUFDM0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUE7WUFFM0UsT0FBTTtTQUVUO1FBR0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQTtRQUMvQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFBO0lBRWxELENBQUM7SUFHRCxjQUFjO1FBRVYsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0lBRXRHLENBQUM7SUFFRCxXQUFXO1FBRVAsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtJQUU3RCxDQUFDO0lBRUQsSUFBSSxrQkFBa0I7UUFFbEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU07WUFDeEYsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFBO0lBRTNCLENBQUM7SUFFRCxJQUFJLGVBQWU7UUFFZixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFFN0UsQ0FBQztJQU1ELFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLFFBQVE7UUFFM0IsUUFBUSxHQUFHLFFBQVEsR0FBRyxJQUFJLENBQUE7UUFFMUIsTUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQTtRQUMvQixNQUFNLE1BQU0sR0FBRyxFQUFFLEdBQUcsS0FBSyxDQUFBO1FBQ3pCLE1BQU0sU0FBUyxHQUFHLEVBQUUsQ0FBQTtRQUVwQixNQUFNLGFBQWEsR0FBRyxVQUFVLFdBQVc7WUFDdkMsV0FBVyxJQUFJLFNBQVMsQ0FBQTtZQUN4QixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFBO1lBQ3JFLE9BQU8sQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFBO1lBQzVCLElBQUksV0FBVyxHQUFHLFFBQVEsRUFBRTtnQkFDeEIsVUFBVSxDQUFDO29CQUNQLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQTtnQkFDOUIsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFBO2FBQ2hCO1FBQ0wsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUVaLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNwQixDQUFDO0lBRUQsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUUsUUFBUTtRQUUzQixRQUFRLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQTtRQUUxQixNQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFBO1FBQy9CLE1BQU0sTUFBTSxHQUFHLEVBQUUsR0FBRyxLQUFLLENBQUE7UUFDekIsTUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFBO1FBRXBCLE1BQU0sYUFBYSxHQUFHLFVBQVUsV0FBVztZQUN2QyxXQUFXLElBQUksU0FBUyxDQUFBO1lBQ3hCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUE7WUFDckUsT0FBTyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7WUFDN0IsSUFBSSxXQUFXLEdBQUcsUUFBUSxFQUFFO2dCQUN4QixVQUFVLENBQUM7b0JBQ1AsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFBO2dCQUM5QixDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUE7YUFDaEI7UUFDTCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBRVosYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ3BCLENBQUM7SUFFRCxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUTtRQUMxQyxXQUFXLElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQTtRQUMzQixJQUFJLFdBQVcsR0FBRyxDQUFDLEVBQUU7WUFDakIsT0FBTyxNQUFNLEdBQUcsQ0FBQyxHQUFHLFdBQVcsR0FBRyxXQUFXLEdBQUcsS0FBSyxDQUFBO1NBQ3hEO1FBQ0QsV0FBVyxJQUFJLENBQUMsQ0FBQTtRQUNoQixPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUE7SUFDdEUsQ0FBQztDQU1KO0FDdE1ELG9DQUFvQztBQU1wQyxNQUFNLFlBQWEsU0FBUSxNQUFNO0lBZ0I3QixZQUFZLFNBQWlCLEVBQUUsZUFBNkI7UUFFeEQsS0FBSyxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQTtRQWZyQyxtQkFBYyxHQUFZLElBQUksT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUMzQyxrQkFBYSxHQUFXLENBQUMsQ0FBQTtRQU16QixtQkFBYyxHQUFZLEdBQUcsQ0FBQTtRQVV6QixJQUFJLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQTtRQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQTtRQUd4QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksTUFBTSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUMsQ0FBQTtRQUU1RCxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUdwQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUE7UUFFOUIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQSxDQUFDLE1BQU07UUFHcEMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFO1lBRTNELElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFBO1FBRTNCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUViLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRTtZQUV6RCxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQTtZQUV0QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFBO1lBRWhDLGFBQWEsRUFBRSxDQUFBO1FBRW5CLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUdiLFNBQVMsYUFBYTtZQUVsQiwwQkFBMEI7UUFFOUIsQ0FBQztRQUdELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLE1BQW9CLEVBQUUsS0FBWTtZQUV2RyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUU5RCxPQUFNO2FBRVQ7WUFFRCxNQUFNLGtCQUFrQixHQUFHLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQTtZQUVoRCxJQUFLLE1BQWMsQ0FBQyxVQUFVLElBQUksS0FBSyxZQUFZLFVBQVUsRUFBRTtnQkFFM0Qsa0JBQWtCLENBQUMsQ0FBQyxHQUFJLEtBQW9CLENBQUMsT0FBTyxDQUFBO2dCQUNwRCxrQkFBa0IsQ0FBQyxDQUFDLEdBQUksS0FBb0IsQ0FBQyxPQUFPLENBQUE7YUFFdkQ7WUFFRCxJQUFLLE1BQWMsQ0FBQyxVQUFVLElBQUksS0FBSyxZQUFZLFVBQVUsRUFBRTtnQkFFM0QsTUFBTSxVQUFVLEdBQWUsS0FBSyxDQUFBO2dCQUVwQyxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtvQkFFaEMsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUE7b0JBQ3RCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUE7b0JBRWhDLGFBQWEsRUFBRSxDQUFBO29CQUVmLE9BQU07aUJBRVQ7Z0JBRUQsa0JBQWtCLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFBO2dCQUNwRCxrQkFBa0IsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUE7YUFFdkQ7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUU1QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsa0JBQWtCLENBQUE7Z0JBRTlDLE9BQU07YUFFVDtZQUVELE1BQU0sV0FBVyxHQUFHLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtZQUdqRixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDdEQsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUE7YUFDcEI7WUFDRCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxFQUFFO2dCQUMxQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUE7YUFDeEM7WUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDM0QsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBO2FBQzVEO1lBRUQsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0JBQ3hELFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQ3BCO1lBQ0QsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsV0FBVyxDQUFDLENBQUMsRUFBRTtnQkFDMUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBO2FBQ3hDO1lBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0JBQzVELFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQTthQUM3RDtZQUVELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7WUFFeEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGtCQUFrQixDQUFBO1FBRWxELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtJQUtqQixDQUFDO0lBTUQsK0JBQStCO1FBRTNCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxHQUFHLENBQUE7SUFFckMsQ0FBQztJQU1ELElBQUksYUFBYTtRQUViLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQTtJQUU5QixDQUFDO0lBRUQsSUFBSSxhQUFhLENBQUMsTUFBZTtRQUU3QixJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQTtRQUM1QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7SUFFekIsQ0FBQztJQUlELGNBQWM7UUFFVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFdEIsMkRBQTJEO1FBQzNELG9DQUFvQztRQUNwQywyRkFBMkY7UUFDM0YsSUFBSTtRQUNKLDJEQUEyRDtRQUMzRCxrRkFBa0Y7UUFDbEYsZ0ZBQWdGO1FBQ2hGLG9EQUFvRDtRQUVwRCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0lBSzFGLENBQUM7SUFJRCxvQkFBb0I7SUFDcEIsNkJBQTZCO0lBQzdCLElBQUk7SUFFSixzQ0FBc0M7SUFDdEMsaUNBQWlDO0lBQ2pDLElBQUk7SUFFSixtQkFBbUI7SUFDbkIsMENBQTBDO0lBQzFDLElBQUk7SUFFSixxQ0FBcUM7SUFFckMsOENBQThDO0lBRTlDLDhDQUE4QztJQUc5QyxJQUFJO0lBR0osVUFBVSxDQUFDLElBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUM5QyxDQUFDO0lBRUQsVUFBVSxDQUFDLElBQVk7UUFFbkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFbkMsSUFBSSxDQUFDLCtCQUErQixFQUFFLENBQUE7SUFHMUMsQ0FBQztDQU1KO0FDek9ELE1BQU0sbUJBQW9CLFNBQVEsTUFBTTtJQW9CcEMsWUFBWSxTQUFpQixFQUFFLGVBQTZCO1FBRXhELEtBQUssQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUE7UUFqQnJDLGlCQUFZLEdBQVcsQ0FBQyxDQUFBO1FBRXhCLGlCQUFZLEdBQVksRUFBRSxDQUFBO1FBQzFCLHdCQUFtQixHQUFZLEVBQUUsQ0FBQTtRQUNqQyxvQkFBZSxHQUFZLEdBQUcsQ0FBQTtRQUU5QixnQkFBVyxHQUFhLEVBQUUsQ0FBQTtRQUUxQixlQUFVLEdBQVksR0FBRyxDQUFBO1FBRXpCLHNCQUFpQixHQUFXLElBQUksQ0FBQTtRQUNoQyxtQkFBYyxHQUFXLENBQUMsQ0FBQTtRQUUxQixzQkFBaUIsR0FBVyxDQUFDLENBQUE7UUFNekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUE7UUFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUE7UUFHeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLFlBQVksQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDLENBQUE7UUFFN0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7UUFHakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFBO1FBRXBDLElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQ3JDLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUMvQixVQUFVLE1BQWMsRUFBRSxLQUFZO1lBRWxDLElBQUksS0FBSyxZQUFZLFVBQVUsRUFBRTtnQkFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsQ0FBQTthQUNwQztRQUVMLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQ2YsQ0FBQTtRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUU7WUFFeEUsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssWUFBWSxVQUFVLEVBQUU7Z0JBQ2xELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTthQUN4QjtRQUVMLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUViLGVBQWU7UUFDZixJQUFJLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFVBQVUsTUFBTSxFQUFFLEtBQUs7WUFFOUYsSUFBSSxLQUFLLFlBQVksVUFBVSxFQUFFO2dCQUM3QixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxDQUFBO2FBQ3BDO1FBRUwsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO1FBRWIsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQztZQUN2QyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWE7U0FDbkUsRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLO1lBRXRCLElBQUksS0FBSyxZQUFZLFVBQVUsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUVsRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7YUFFeEI7UUFFTCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7UUFHYixpQkFBaUI7UUFFakIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksTUFBTSxDQUFDLFNBQVMsR0FBRyxvQkFBb0IsQ0FBQyxDQUFBO1FBQ3RFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUE7SUFNNUMsQ0FBQztJQU1ELCtCQUErQixDQUFDLEtBQWE7UUFFekMsTUFBTSxNQUFNLEdBQUcsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLEdBQUcscUJBQXFCLEdBQUcsS0FBSyxDQUFDLENBQUE7UUFFcEYsTUFBTSxDQUFDLHlCQUF5QixDQUFDO1lBQzdCLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTztTQUNuRSxFQUFFLFVBQVUsTUFBTSxFQUFFLEtBQUs7WUFFdEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQTtZQUV0QyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBRW5CLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTthQUV4QjtRQUlMLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUViLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUU3RCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxDQUFBO1FBRXJDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUdiLE1BQU0sQ0FBQywyQkFBMkIsR0FBRztZQUVqQyxNQUFNLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUE7WUFDMUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQTtRQUVwRCxDQUFDLENBQUE7UUFHRCxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBRWhELGdDQUFnQztRQUNoQywrQkFBK0I7UUFDL0IsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFBO1FBQ25DLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQTtRQUdsQyx5RUFBeUU7UUFDekUsc0RBQXNEO1FBQ3RELHNDQUFzQztRQUN0Qyx1Q0FBdUM7UUFDdkMsdUNBQXVDO1FBQ3ZDLDRDQUE0QztRQUU1QyxvQ0FBb0M7UUFHcEMsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUlELFlBQVksQ0FBQyxJQUFZO1FBRXJCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBRTFCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO0lBRTNCLENBQUM7SUFFRCxJQUFJLFVBQVUsQ0FBQyxLQUFlO1FBRTFCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFBO1FBRXhCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO0lBRTNCLENBQUM7SUFFRCxJQUFJLFVBQVU7UUFDVixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUE7SUFDM0IsQ0FBQztJQUlELElBQUksZ0JBQWdCO1FBRWhCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQTtRQUVyQyxPQUFPLE1BQU0sQ0FBQTtJQUVqQixDQUFDO0lBRUQsSUFBSSxnQkFBZ0IsQ0FBQyxLQUFhO1FBRTlCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUE7UUFFOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtRQUVwQyx3R0FBd0c7UUFDeEcsa0ZBQWtGO1FBRWxGLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUdoSCxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLE1BQWdCLEVBQUUsS0FBSyxFQUFFLEtBQUs7WUFFN0UsTUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUE7UUFFeEIsQ0FBQyxDQUFDLENBQUM7UUFJRixJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBYyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUE7SUFJeEUsQ0FBQztJQUlELG9CQUFvQixDQUFDLFFBQWlCO1FBRWxDLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQzdCLE9BQU07U0FDVDtRQUVELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQTtRQUV2QyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDakIsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUN2RTthQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRTtZQUN6RCxXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQTtTQUMxQzthQUNJO1lBQ0QsT0FBTTtTQUNUO1FBRUQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQTtJQUVyRCxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsUUFBaUI7UUFFOUIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDN0IsT0FBTTtTQUNUO1FBRUQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFBO1FBRXZDLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1NBQ3ZFO2FBQ0ksSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFO1lBQ3pELFdBQVcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFBO1NBQzFDO2FBQ0k7WUFDRCxPQUFNO1NBQ1Q7UUFFRCxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFBO0lBRXJELENBQUM7SUFJRCxxQkFBcUIsQ0FBQyxXQUFtQixFQUFFLFdBQW9CLEdBQUc7UUFFOUQsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUE7UUFFL0IsOERBQThEO1FBQzlELDJEQUEyRDtRQUUzRCxrREFBa0Q7UUFFbEQsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRTNDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxHQUFHLENBQUE7UUFFOUIsNkRBQTZEO1FBRTdELElBQUksUUFBUSxFQUFFO1lBSVYsTUFBTSxDQUFDLDhDQUE4QyxDQUNqRCxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFDOUIsSUFBSSxDQUFDLGlCQUFpQixFQUN0QixDQUFDLEVBQ0QsU0FBUyxFQUNUO2dCQUdJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxXQUFXLENBQUE7WUFJdkMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDWjtnQkFFSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLENBQUE7Z0JBRTFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUE7Z0JBRTdCLDRCQUE0QjtZQUVoQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUNmLENBQUE7U0FFSjthQUNJO1lBR0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQTtZQUNuQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLENBQUE7WUFFMUMsNEJBQTRCO1NBRS9CO0lBRUwsQ0FBQztJQUlELHlCQUF5QixDQUFDLEtBQWE7UUFFbkMsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUV6QyxJQUFJLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSyxVQUFrQixDQUFDLFVBQVUsSUFBSyxVQUFrQixDQUFDLFVBQVUsWUFBWSxRQUFRLEVBQUU7WUFFdkcsVUFBa0IsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtTQUVuQztJQUVMLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxLQUFhO0lBSXRDLENBQUM7SUFJRCxjQUFjO1FBRVYsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUE7UUFFdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtRQUVqQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtZQUVsRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUE7UUFFOUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO0lBRWpCLENBQUM7SUFFRCxhQUFhO1FBRVQsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUE7UUFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtJQUVyQyxDQUFDO0lBTUQsZ0JBQWdCO1FBRVosSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSztZQUVuRixPQUFPLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtRQUVqQyxDQUFDLENBQUMsQ0FBQTtRQUVGLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQVUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLO1lBRTVFLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO1FBRWpDLENBQUMsQ0FBQyxDQUFBO1FBRUYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUs7WUFFakQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7WUFFakMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtRQUVuRixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7SUFFakIsQ0FBQztJQUdELHdCQUF3QixDQUFDLEtBQTJCO1FBRWhELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUVyQyxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRTtZQUV6RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFBO1NBR2hEO0lBR0wsQ0FBQztJQUdELElBQUksS0FBSyxDQUFDLEtBQWtCO1FBRXhCLEtBQUssQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1FBRW5CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUE7SUFFakQsQ0FBQztJQUVELElBQUksS0FBSztRQUVMLE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQTtJQUV0QixDQUFDO0lBTUQsY0FBYztRQUVWLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUV0QixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO1lBQ25ELE9BQU07U0FDVDtRQUVELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFFMUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLE1BQU0sQ0FBQTtRQUVuQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUE7UUFJL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsS0FBSztZQUN6RSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLHVCQUF1QixDQUFDLFVBQXFDLElBQWlCO1lBRXRHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQTtZQUVsRCxPQUFPLElBQUksQ0FBQTtRQUVmLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUViLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLO1lBRWpELElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFBO1FBRXZFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUdiLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFBO0lBRy9CLENBQUM7SUFHRCxvQkFBb0I7UUFFaEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixFQUFFLENBQUE7UUFDNUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO1FBQzdDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtRQUM3QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUE7SUFFdkQsQ0FBQztJQUtELG1CQUFtQjtRQUdmLEtBQUssQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO1FBRTNCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtJQUd4QixDQUFDO0NBTUo7QUMvZEQsTUFBTSxjQUFlLFNBQVEsUUFBUTtJQWNqQyxZQUFZLG9CQUFvQixHQUFHLEVBQUU7UUFHakMsS0FBSyxFQUFFLENBQUE7UUFQWCxvQkFBZSxHQUFHLEVBQUUsQ0FBQTtRQUVwQixlQUFVLEdBQUcsY0FBYyxDQUFDLGdCQUFnQixDQUFBO1FBT3hDLElBQUksQ0FBQyxNQUFNLEdBQUcsY0FBYyxDQUFBO1FBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFBO1FBRzFCLElBQUksb0JBQW9CLEVBQUU7WUFFdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLE1BQU0sQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFBO1NBRTlFO1FBRUQsY0FBYyxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQTtRQUNuRSxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQyxlQUFlLENBQUE7SUFNekQsQ0FBQztJQU1ELElBQUksa0JBQWtCO1FBRWxCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQTtJQUUvQixDQUFDO0lBTUQsVUFBVSxDQUNOLGVBQXVCLEVBQ3ZCLElBQWMsRUFDZCxZQUFzQixFQUN0QixVQUFlLEVBQ2YsVUFBd0Y7UUFJeEYsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBRXRCLE9BQU07U0FFVDtRQUVELDZCQUE2QjtRQUU3QixNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQTtRQUVsRCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxVQUFVLE9BQU87WUFFekMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLGtCQUFrQixFQUFFO2dCQUV2RCxvRUFBb0U7Z0JBRXBFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO2FBRS9GO1FBRUwsQ0FBQyxDQUFBO1FBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7WUFFeEIsaUJBQWlCLEVBQUUsZUFBZTtZQUNsQyxNQUFNLEVBQUUsSUFBSTtZQUNaLGNBQWMsRUFBRSxZQUFZO1lBQzVCLFlBQVksRUFBRSxVQUFVO1lBQ3hCLG9CQUFvQixFQUFFLGtCQUFrQjtTQUUzQyxDQUFDLENBQUE7SUFHTixDQUFDO0lBR0QsWUFBWSxDQUNSLGVBQXVCLEVBQ3ZCLElBQWMsRUFDZCxlQUF5QixFQUFFLEVBQzNCLGFBQWtCLE9BQU8sRUFBRTtRQUkzQixNQUFNLE1BQU0sR0FNUCxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUVqQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFDM0QsQ0FBQyxZQUFZLEVBQUUsZUFBZSxFQUFFLGtCQUFrQixFQUFFLEVBQUU7Z0JBRWxELElBQUksa0JBQWtCLElBQUksVUFBVSxFQUFFO29CQUVsQyxPQUFPLENBQUM7d0JBRUosWUFBWSxFQUFFLFlBQVk7d0JBQzFCLGVBQWUsRUFBRSxlQUFlO3dCQUNoQyxVQUFVLEVBQUUsa0JBQWtCO3FCQUVqQyxDQUFDLENBQUE7aUJBRUw7WUFHTCxDQUFDLENBQ0osQ0FBQTtRQUdMLENBQUMsQ0FBQyxDQUFBO1FBRUYsT0FBTyxNQUFNLENBQUE7SUFHakIsQ0FBQztJQU1ELFdBQVc7UUFFUCxJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQTtRQUUxQixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksY0FBYyxDQUFDLGdCQUFnQixFQUFFO1lBRXBELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLENBQUE7U0FFOUI7SUFJTCxDQUFDOztBQXpKTSwrQkFBZ0IsR0FBRyxJQUFJLE1BQU0sQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFBO0FBRTVFLDhCQUFlLEdBQUcsQ0FBQyxDQUFDLENBQUE7QUNML0IsZ0RBQWdEO0FBd0JoRCxNQUFNLFdBQVksU0FBUSxrQkFBa0I7SUFpQ3hDLFlBQVksU0FBUztRQUVqQixLQUFLLENBQUMsU0FBUyxDQUFDLENBQUE7UUE5QnBCLDJCQUFzQixHQUFZLEVBQUUsQ0FBQTtRQUNwQyxpQkFBWSxHQUFhLEVBQUUsQ0FBQTtRQUMzQiw0QkFBdUIsR0FBYSxFQUFFLENBQUE7UUFFdEMsa0JBQWEsR0FBNEMsRUFBRSxDQUFBO1FBRTNELGtDQUE2QixHQUFXLENBQUMsQ0FBQTtRQUV6QyxtQkFBYyxHQUE0QyxFQUFFLENBQUE7UUFFNUQsMEJBQXFCLEdBQTRDLEVBQUUsQ0FBQTtRQUluRSxnQkFBVyxHQUFXLENBQUMsQ0FBQTtRQUV2Qiw0QkFBdUIsR0FBRyxHQUFHLENBQUE7UUFFN0IsZ0JBQVcsR0FBRyxDQUFDLENBQUE7UUFFZixtQkFBYyxHQUFVLEVBQUUsQ0FBQTtRQUMxQiwyQ0FBc0MsR0FBRyxFQUFFLENBQUE7UUFDM0MsZ0NBQTJCLEdBQUcsRUFBRSxDQUFBO1FBR2hDLHNCQUFpQixHQUFHLElBQUksQ0FBQTtRQU9wQixJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQTtRQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLGtCQUFrQixDQUFBO1FBRXBDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFBO0lBRXRCLENBQUM7SUFJRCxRQUFRLENBQUMsU0FBUyxFQUFFLGVBQWU7UUFFL0IsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUE7UUFFMUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFBO1FBQ25DLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQTtRQUNqQyxJQUFJLENBQUMsZUFBZSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQTtRQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtJQUV6QyxDQUFDO0lBSUQsUUFBUTtRQUVKLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFBO1FBRXhCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDM0QsSUFBSSxDQUFDLHNDQUFzQyxHQUFHLEdBQUcsQ0FBQTtRQUVqRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFckIsNkNBQTZDO1FBRTdDLDZCQUE2QjtRQUU3QixLQUFLO0lBRVQsQ0FBQztJQUVELFVBQVU7UUFFTixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtRQUN6QixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQTtRQUU3QixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQTtRQUN2QixJQUFJLENBQUMsNkJBQTZCLEdBQUcsQ0FBQyxDQUFBO1FBRXRDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtJQUVuQixDQUFDO0lBSUQsZ0JBQWdCLENBQUMsWUFBbUIsRUFBRSxPQUFjO1FBRWhELFlBQVksR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQVUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLO1lBRTdELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUVwQyxDQUFDLENBQUMsQ0FBQTtRQUVGLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLO1lBRW5ELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUVwQyxDQUFDLENBQUMsQ0FBQTtRQUdGLE1BQU0sVUFBVSxHQUFhLEVBQUUsQ0FBQTtRQUUvQixPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLO1lBRXpDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUUvQixVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO2FBRXpCO1FBRUwsQ0FBQyxDQUFDLENBQUE7UUFFRixVQUFVLENBQUMsT0FBTyxDQUFDLFVBQTZCLEtBQUs7WUFFakQsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBRW5DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTthQUUxRDtRQUVMLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtJQUVqQixDQUFDO0lBSUQsaUJBQWlCLENBQUMsR0FBVztJQUk3QixDQUFDO0lBTUQsNEJBQTRCLENBQUMsS0FBYSxFQUFFLGFBQWEsR0FBRyxFQUFFO1FBRTFELElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUE7U0FDekM7UUFFRCxJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLEVBQUUsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFBO1FBRTVGLG9CQUFvQjtRQUVwQiw4Q0FBOEM7UUFFOUMsK0JBQStCO1FBRS9CLElBQUk7UUFFSixJQUFJLENBQUMsc0NBQXNDLEdBQUcsR0FBRyxDQUFBO1FBRWpELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxhQUFhLENBQUE7SUFFakQsQ0FBQztJQUlELHNCQUFzQjtRQUNsQixJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFBO0lBQy9ELENBQUM7SUFFRCw2QkFBNkIsQ0FBQyxRQUFnQjtRQUUxQyxJQUFJLG1CQUFtQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUE7UUFDaEYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO1lBQzFCLG1CQUFtQixHQUFHO2dCQUNsQixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsQ0FBQztnQkFDUCxPQUFPLEVBQUUsR0FBRzthQUNmLENBQUE7U0FDSjtRQUVELElBQUksZUFBZSxHQUFHLG1CQUFtQixDQUFDLE9BQU8sQ0FBQTtRQUVqRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFFNUIsSUFBSSxDQUFDLDZCQUE2QixHQUFHLENBQUMsQ0FBQyxDQUFBO1NBRTFDO1FBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsNkJBQTZCLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFFckUsSUFBSSxNQUFjLENBQUE7WUFFbEIsTUFBTSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRS9DLElBQUksRUFBRSxDQUFDLENBQUMsaUJBQWlCLElBQUksR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBRXhDLE1BQU0sR0FBRyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxDQUFBO2FBRTlEO2lCQUNJO2dCQUVELE1BQU0sR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFFekM7WUFLRCxNQUFNLGNBQWMsR0FBMEM7Z0JBQzFELE9BQU8sRUFBRSxlQUFlLEdBQUcsTUFBTTtnQkFDakMsSUFBSSxFQUFFLGVBQWU7Z0JBQ3JCLE9BQU8sRUFBRSxHQUFHO2FBQ2YsQ0FBQTtZQUVELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO2dCQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxHQUFHLGNBQWMsQ0FBQTthQUN6QztpQkFDSTtnQkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQTthQUMxQztZQUNELElBQUksQ0FBQyw2QkFBNkIsR0FBRyxDQUFDLENBQUE7WUFDdEMsZUFBZSxHQUFHLGVBQWUsR0FBRyxNQUFNLENBQUE7U0FFN0M7SUFFTCxDQUFDO0lBSUQscUJBQXFCLENBQUMsWUFBWSxHQUFHLEdBQUc7UUFFcEMsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFBO1FBQzlFLE1BQU0sWUFBWSxHQUFHLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUMsQ0FBQTtRQUU1RSxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7UUFFeEMsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFFN0IsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRS9DLElBQUksVUFBVSxHQUFHLGFBQWEsR0FBRyxTQUFTLENBQUE7WUFDMUMsSUFBSSxTQUFTLEdBQUcsWUFBWSxHQUFHLFNBQVMsQ0FBQTtZQUV4QyxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQTtZQUNuQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUE7WUFFckMsVUFBVSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFBO1lBQ3BDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUE7WUFFakQsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFBO1lBQ2YsS0FBSyxJQUFJLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxHQUFHLFNBQVMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzdDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDakI7WUFDRCxPQUFPLE1BQU0sQ0FBQTtTQUNoQjtRQUVELElBQUksaUJBQWlCLEdBQUcsQ0FBQyxDQUFBO1FBQ3pCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQTtRQUVmLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFBO1FBRTdCLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUE7UUFFdkMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUVuQyxNQUFNLE1BQU0sR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUEsQ0FBRSxnQ0FBZ0M7WUFFL0YsaUJBQWlCLEdBQUcsaUJBQWlCLEdBQUcsTUFBTSxDQUFBO1lBQzlDLElBQUksaUJBQWlCLElBQUksYUFBYSxFQUFFO2dCQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQ2pCO1lBQ0QsSUFBSSxpQkFBaUIsSUFBSSxZQUFZLEVBQUU7Z0JBQ25DLE1BQUs7YUFDUjtTQUVKO1FBRUQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztJQUdELGtCQUFrQjtRQUVkLE1BQU0sV0FBVyxHQUFHLEVBQUUsQ0FBQTtRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUE2QixHQUFXLEVBQUUsS0FBYSxFQUFFLEtBQWU7WUFFOUYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsR0FBRyxJQUFJLENBQUMsa0NBQWtDLENBQ25GLEdBQUcsQ0FBQyxvQkFBb0IsRUFDeEIsR0FBRyxDQUNOLENBQUE7WUFDRCxHQUFHLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtZQUN6QixJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBRy9FLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUNSLElBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFBO0lBRW5DLENBQUM7SUFJRCxzQkFBc0I7UUFDbEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBNkIsSUFBYztZQUNuRSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQTZCLEdBQVcsRUFBRSxLQUFhLEVBQUUsS0FBZTtnQkFFakYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsR0FBRyxJQUFJLENBQUMsa0NBQWtDLENBQ25GLEdBQUcsQ0FBQyxvQkFBb0IsRUFDeEIsR0FBRyxDQUNOLENBQUE7Z0JBQ0QsR0FBRyxDQUFDLG1CQUFtQixFQUFFLENBQUE7Z0JBRXpCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUV2QyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7UUFDakIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO0lBQ2pCLENBQUM7SUFJRCx5QkFBeUIsQ0FBQyxHQUFXO1FBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2xGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7U0FDOUU7SUFDTCxDQUFDO0lBRUQsZ0JBQWdCO1FBRVosSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUMxQixPQUFNO1NBQ1Q7UUFFRCxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQTtRQUVuRCxNQUFNLFFBQVEsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDbEMsTUFBTSxRQUFRLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFMUQsTUFBTSxZQUFZLEdBQUcsRUFBRSxDQUFBO1FBRXZCLE1BQU0sV0FBVyxHQUFHLEVBQUUsQ0FBQTtRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUE2QixHQUFXLEVBQUUsS0FBYSxFQUFFLEtBQWU7WUFDOUYsSUFBSSxHQUFHLENBQUMsb0JBQW9CLEdBQUcsUUFBUSxJQUFJLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxRQUFRLEVBQUU7Z0JBRTVFLDRCQUE0QjtnQkFFNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsR0FBRyxJQUFJLENBQUMsa0NBQWtDLENBQ25GLEdBQUcsQ0FBQyxvQkFBb0IsRUFDeEIsR0FBRyxDQUNOLENBQUE7Z0JBRUQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtnQkFFM0UsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTthQUV6QjtpQkFDSTtnQkFDRCxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQ3hCO1FBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQ1IsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUE7UUFFL0IsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUE2QixRQUFnQixFQUFFLEtBQWEsRUFBRSxLQUFhO1lBRzlGLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN0QyxPQUFNO2FBQ1Q7WUFDRCxNQUFNLElBQUksR0FBVyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUE7WUFDdkQsdUNBQXVDO1lBQ3ZDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDdkMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDNUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUV6QixDQUFRLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFFZixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUUxQyxJQUFJLElBQUksR0FBVyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDbEMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtnQkFFdkMsNEhBQTRIO2dCQUM1SCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtnQkFFMUIsZ0ZBQWdGO2FBRW5GO1NBRUo7UUFFRCx3QkFBd0I7SUFFNUIsQ0FBQztJQUdELG1CQUFtQixDQUFDLFFBQWdCO1FBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ2hDLElBQUksR0FBRyxDQUFDLG9CQUFvQixJQUFJLFFBQVEsRUFBRTtnQkFDdEMsT0FBTyxHQUFHLENBQUE7YUFDYjtTQUNKO1FBQ0QsT0FBTyxHQUFHLENBQUE7SUFDZCxDQUFDO0lBR0QscUJBQXFCLENBQUMsUUFBZ0I7UUFDbEMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7SUFDakQsQ0FBQztJQUlELHlCQUF5QixDQUFDLFVBQWtCLEVBQUUsUUFBZ0I7UUFFMUQsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUN6QyxJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFBO1NBQzlDO1FBRUQsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sRUFBRTtZQUV6RixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUE7WUFFekQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFFBQVEsQ0FBQTtZQUVwQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFDLENBQUE7WUFFMUYsT0FBTyxJQUFJLENBQUE7U0FFZDtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFBO1NBQ3ZDO1FBRUQsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7UUFDL0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQTtRQUV2QyxJQUFJLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxFQUFFO1lBRXZCLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQTtTQUVoQjtRQUVELE9BQU8sQ0FBQyxpQ0FBaUMsR0FBRyxVQUFVLENBQUE7UUFFdEQsT0FBTyxDQUFDLG9CQUFvQixHQUFHLFFBQVEsQ0FBQTtRQUV2QyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFDLENBQUE7UUFDN0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7UUFFN0MsT0FBTyxPQUFPLENBQUE7SUFFbEIsQ0FBQztJQUdELHdFQUF3RTtJQUN4RSw0QkFBNEIsQ0FBQyxVQUFrQixFQUFFLFVBQWtCO1FBRS9ELE1BQU0sSUFBSSxHQUFHLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFBO1FBRTlELElBQUksQ0FBQyw0QkFBNEIsR0FBRyxFQUFFLENBQUE7UUFDdEMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQTtRQUU3QixPQUFPLElBQUksQ0FBQTtJQUVmLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxLQUFhO1FBQy9CLE9BQU8sRUFBRSxDQUFBO0lBQ2IsQ0FBQztJQUVELFlBQVk7UUFDUixPQUFPLEtBQUssQ0FBQTtJQUNoQixDQUFDO0lBRUQsNkJBQTZCO0lBSTdCLENBQUM7SUFFRCxrQ0FBa0MsQ0FBQyxRQUFnQixFQUFFLEdBQVc7SUFJaEUsQ0FBQztJQUVELG1CQUFtQixDQUFDLFFBQWdCO1FBRWhDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDM0QsR0FBZ0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLE1BQU0sR0FBRyxRQUFRLENBQUE7UUFFckQsT0FBTyxHQUFHLENBQUE7SUFFZCxDQUFDO0lBRUQsc0VBQXNFO0lBR3RFLGtEQUFrRDtJQUNsRCxtQkFBbUIsQ0FBQyxjQUF1QjtRQUV2QyxLQUFLLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLENBQUE7UUFFekMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsSUFBWTtZQUU1QyxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQTtRQUU3QixDQUFDLENBQUMsQ0FBQTtRQUVGLElBQUksQ0FBQyxJQUFJLENBQUMsMkJBQTJCLEVBQUU7WUFFbkMsSUFBSSxDQUFDLDJCQUEyQixHQUFHLEdBQUcsQ0FBQTtZQUV0QyxNQUFNLENBQUMsMEJBQTBCLENBQUM7Z0JBRTlCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFBO2dCQUU3QixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQTtnQkFFdkIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO2dCQUVyQixJQUFJLENBQUMsMkJBQTJCLEdBQUcsRUFBRSxDQUFBO1lBRXpDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtTQUVoQjtJQUVMLENBQUM7SUFFRCxrQkFBa0I7UUFDZCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7SUFDbkIsQ0FBQztJQUVELFFBQVEsQ0FBQyxTQUFzQixFQUFFLE1BQWUsRUFBRSxzQkFBZ0M7UUFFOUUsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQTtRQUN4QixLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsc0JBQXNCLENBQUMsQ0FBQTtRQUN6RCxJQUFJLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUN2RCxPQUFNO1NBQ1Q7UUFFRCxJQUFJLENBQUMsc0NBQXNDLEdBQUcsR0FBRyxDQUFBO0lBRXJELENBQUM7SUFNRCx3QkFBd0IsQ0FBQyxLQUEyQjtRQUVoRCxLQUFLLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFckMsSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBRXpGLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQTtTQUVwQjtJQUdMLENBQUM7SUFNTyxjQUFjLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhO1FBRWpELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7UUFFMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBNkIsR0FBVyxFQUFFLEtBQWEsRUFBRSxLQUFlO1lBRTlGLE1BQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtZQUUzQixNQUFNLGNBQWMsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUE7WUFDMUQsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQTtZQUNqQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFBO1lBQ3BDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1lBRWpCLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFBO1lBQ2hGLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7UUFHOUQsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBRVIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVc7WUFDMUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQTtRQUV4RCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsRUFBRSxDQUFBO0lBR3JDLENBQUM7SUFFTyxxQkFBcUI7UUFFekIsTUFBTSxDQUFDLDhDQUE4QyxDQUNqRCxJQUFJLENBQUMsWUFBWSxFQUNqQixJQUFJLENBQUMsaUJBQWlCLEVBQ3RCLENBQUMsRUFDRCxTQUFTLEVBQ1Q7WUFFSSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFekIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDWjtZQUVJLGdDQUFnQztZQUNoQyx3QkFBd0I7UUFFNUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDZixDQUFBO0lBRUwsQ0FBQztJQU1ELGNBQWM7UUFFVixNQUFNLGlCQUFpQixHQUE0QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUE7UUFFakgsTUFBTSx5QkFBeUIsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQTtRQUUxRCxJQUFJLElBQUksQ0FBQyxzQ0FBc0MsRUFBRTtZQUU3QywrQkFBK0I7WUFFL0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUE7WUFFdkIsSUFBSSxDQUFDLHNDQUFzQyxHQUFHLEVBQUUsQ0FBQTtTQUVuRDtRQUlELEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUd0QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBRWxELE9BQU07U0FFVDtRQU1ELElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBSS9CLGdEQUFnRDtZQUVoRCxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLENBQUE7WUFHdEMsSUFBSSx5QkFBeUIsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtnQkFHdEQsTUFBTSxDQUFDLDBCQUEwQixDQUFDO29CQUU5QixJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQTtnQkFFaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO2FBRWhCO2lCQUNJO2dCQUVELElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFBO2FBRS9CO1lBR0QsSUFBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQTtTQUVyQzthQUNJO1lBRUQscURBQXFEO1lBRXJELCtCQUErQjtZQUUvQix3REFBd0Q7WUFFeEQsSUFBSTtZQUVKLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFBO1lBRTdCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtTQUd4QjtJQU1MLENBQUM7SUFNRCxzQkFBc0IsQ0FBQyxpQkFBaUIsR0FBRyxDQUFDO1FBSXhDLElBQUksTUFBTSxHQUFHLDBCQUEwQixDQUFBO1FBRXZDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFFM0IsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFBO1NBRXJFO1FBRUQsT0FBTyxNQUFNLENBQUE7SUFFakIsQ0FBQztDQU1KO0FDOXVCRCxzQ0FBc0M7QUFLdEMsYUFBYTtBQUNiLE1BQU0sV0FBWSxTQUFRLFVBQVU7SUFRaEMsWUFBWSxTQUFrQixFQUFFLGVBQWUsR0FBRyxJQUFJLEVBQUUsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUztRQUVwRixLQUFLLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQTtRQUV2QyxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQTtRQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQTtRQUU1QixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFakQsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFBO1FBSXpDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLO1lBRXRGLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtRQUVsQixDQUFDLENBQUMsQ0FBQTtRQUdGLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDckMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQzNFLENBQUMsQ0FBQTtRQUdELElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFBO1FBRXBDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxHQUFHLENBQUE7UUFFakMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQTtJQUdqQyxDQUFDO0lBaUJELElBQUkscUJBQXFCO1FBRXJCLE9BQU8sS0FBSyxDQUFDLHFCQUE0QixDQUFDO0lBRTlDLENBQUM7SUFFRCxJQUFXLGVBQWU7UUFDdEIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUE7SUFDaEMsQ0FBQztJQUlELElBQVcsSUFBSSxDQUFDLElBQVk7UUFFeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO0lBRXJDLENBQUM7SUFHRCxJQUFXLElBQUk7UUFFWCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFBO0lBRXJDLENBQUM7SUFHRCxJQUFXLGVBQWUsQ0FBQyxJQUFZO1FBRW5DLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtJQUUzQyxDQUFDO0lBR0QsSUFBVyxlQUFlO1FBRXRCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUE7SUFFM0MsQ0FBQztJQUdELGtCQUFrQixDQUFDLEdBQVcsRUFBRSxhQUFxQjtRQUVqRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsR0FBRyxDQUFBO1FBQzlCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxhQUFhLENBQUE7UUFFNUMsTUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQTtRQUM5RCxJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFBO0lBRXJHLENBQUM7SUFJRCx3QkFBd0IsQ0FBQyxLQUEyQjtRQUVoRCxLQUFLLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFckMsSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLElBQUksS0FBSyxDQUFDLElBQUk7WUFDckUsTUFBTSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRTtZQUUzQyxJQUFJLENBQUMsZ0NBQWdDLEVBQUUsQ0FBQTtTQUUxQztJQUVMLENBQUM7SUFHRCxtQkFBbUIsQ0FBQyxTQUFpQjtRQUVqQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUE7UUFFcEMsSUFBSSxDQUFDLGdDQUFnQyxFQUFFLENBQUE7SUFFM0MsQ0FBQztJQUVELGdDQUFnQztRQUU1QixJQUFJLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFFMUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQTtTQUVsRjtJQUVMLENBQUM7SUFHRCxJQUFXLFFBQVE7UUFFZixNQUFNLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxJQUFJLFVBQVUsQ0FBQyxDQUFBO1FBRXhELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7SUFJRCxJQUFXLFFBQVEsQ0FBQyxNQUFlO1FBRS9CLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQTtRQUVqQixJQUFJLE1BQU0sRUFBRTtZQUVSLElBQUksR0FBRyxVQUFVLENBQUE7U0FFcEI7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7SUFFcEMsQ0FBQzs7QUF2SE0sd0JBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsWUFBWSxFQUFFO0lBR3pELFlBQVksRUFBRSxZQUFZO0NBRzdCLENBQUMsQ0FBQTtBQ3pETix1Q0FBdUM7QUFLdkMsTUFBTSxVQUFXLFNBQVEsV0FBVztJQU1oQyxZQUFZLFNBQVMsRUFBRSxlQUFlLEdBQUcsSUFBSTtRQUV6QyxLQUFLLENBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBRTNELElBQUksQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFBO1FBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFBO1FBRTdCLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBRTVDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQTtRQUU1QixJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQTtRQUVwQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFBO0lBRWpDLENBQUM7SUFHRCxJQUFJLHFCQUFxQjtRQUVyQixPQUFPLEtBQUssQ0FBQyxxQkFBNEIsQ0FBQztJQUU5QyxDQUFDO0lBRUQsYUFBYTtJQUNiLElBQUksZUFBZTtRQUVmLGFBQWE7UUFDYixPQUFPLEtBQUssQ0FBQyxlQUFlLENBQUE7SUFFaEMsQ0FBQztDQU1KO0FDL0NELE1BQU0sT0FBUSxTQUFRLFFBQVE7SUFXMUIsWUFBbUIsUUFBZ0IsRUFBUyxPQUFnQixFQUFTLE1BQWdCO1FBRWpGLEtBQUssRUFBRSxDQUFBO1FBRlEsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUFTLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFBUyxXQUFNLEdBQU4sTUFBTSxDQUFVO1FBSHJGLFlBQU8sR0FBWSxHQUFHLENBQUE7UUFPbEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUE7UUFHMUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO0lBSW5CLENBQUM7SUFJRCxRQUFRO1FBRUosTUFBTSxRQUFRLEdBQUc7WUFDYixJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxFQUFFO2dCQUNwQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7YUFDcEI7WUFDRCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7UUFDakIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUVaLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQTtJQUV6RSxDQUFDO0lBR0QsVUFBVTtRQUVOLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQTtRQUNqQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7SUFFbkIsQ0FBQztJQUlELElBQUk7UUFDQSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQTtTQUNwQjthQUNJO1lBQ0QsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFBO1NBQ3BCO1FBQ0QsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO0lBQ2pCLENBQUM7SUFFRCxVQUFVO1FBRU4sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBRWQsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUUvQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQTtTQUVwQjtJQUlMLENBQUM7Q0FNSiIsInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIE5pbEZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBuaWxcbn1cblxudmFyIG5pbDogYW55ID0gbmV3IFByb3h5KE9iamVjdC5hc3NpZ24oTmlsRnVuY3Rpb24sIHsgXCJjbGFzc1wiOiBuaWwsIFwiY2xhc3NOYW1lXCI6IFwiTmlsXCIgfSksIHtcbiAgICBcbiAgICBnZXQodGFyZ2V0LCBuYW1lKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAobmFtZSA9PSBTeW1ib2wudG9QcmltaXRpdmUpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChoaW50KSB7XG4gICAgICAgICAgICAgICAgaWYgKGhpbnQgPT0gXCJudW1iZXJcIikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gMFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoaGludCA9PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIlwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChuYW1lID09IFwidG9TdHJpbmdcIikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gTmlsRnVuY3Rpb24oKVxuICAgIH0sXG4gICAgXG4gICAgc2V0KHRhcmdldCwgbmFtZSwgdmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIE5pbEZ1bmN0aW9uKClcbiAgICB9XG4gICAgXG59KVxuXG5cbmZ1bmN0aW9uIHdyYXBJbk5pbDxUPihvYmplY3Q/OiBUKTogVCB7XG4gICAgXG4gICAgXG4gICAgdmFyIHJlc3VsdCA9IEZJUlNUX09SX05JTChvYmplY3QpXG4gICAgXG4gICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mIE9iamVjdCAmJiAhKG9iamVjdCBpbnN0YW5jZW9mIEZ1bmN0aW9uKSkge1xuICAgICAgICBcbiAgICAgICAgcmVzdWx0ID0gbmV3IFByb3h5KG9iamVjdCBhcyBPYmplY3QgJiBULCB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGdldCh0YXJnZXQsIG5hbWUpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAobmFtZSA9PSBcIndyYXBwZWRfbmlsX3RhcmdldFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGFyZ2V0XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IFJlZmxlY3QuZ2V0KHRhcmdldCwgbmFtZSlcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHZhbHVlID09PSBcIm9iamVjdFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gd3JhcEluTmlsKHZhbHVlKVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKElTX05PVF9MSUtFX05VTEwodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWVcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHJldHVybiBuaWxcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9KVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcmV0dXJuIHJlc3VsdFxuICAgIFxufVxuXG5cbmNvbnN0IFlFUyA9IHRydWVcbmNvbnN0IE5PID0gZmFsc2VcblxuZnVuY3Rpb24gSVMob2JqZWN0KSB7XG4gICAgXG4gICAgaWYgKG9iamVjdCAmJiBvYmplY3QgIT09IG5pbCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIFlFU1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcmV0dXJuIE5PXG4gICAgXG4gICAgLy9yZXR1cm4gKG9iamVjdCAhPSBuaWwgJiYgb2JqZWN0KTtcbiAgICBcbn1cblxuZnVuY3Rpb24gSVNfTk9UKG9iamVjdCkge1xuICAgIFxuICAgIHJldHVybiAhSVMob2JqZWN0KVxuICAgIFxufVxuXG5mdW5jdGlvbiBJU19ERUZJTkVEKG9iamVjdCkge1xuICAgIFxuICAgIGlmIChvYmplY3QgIT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gWUVTXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICByZXR1cm4gTk9cbiAgICBcbn1cblxuZnVuY3Rpb24gSVNfVU5ERUZJTkVEKG9iamVjdCkge1xuICAgIFxuICAgIHJldHVybiAhSVNfREVGSU5FRChvYmplY3QpXG4gICAgXG59XG5cbmZ1bmN0aW9uIElTX05JTChvYmplY3QpIHtcbiAgICBcbiAgICBpZiAob2JqZWN0ID09PSBuaWwpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHJldHVybiBOT1xuICAgIFxufVxuXG5mdW5jdGlvbiBJU19OT1RfTklMKG9iamVjdCkge1xuICAgIFxuICAgIHJldHVybiAhSVNfTklMKG9iamVjdClcbiAgICBcbn1cblxuXG5mdW5jdGlvbiBJU19MSUtFX05VTEwob2JqZWN0KSB7XG4gICAgXG4gICAgcmV0dXJuIChJU19VTkRFRklORUQob2JqZWN0KSB8fCBJU19OSUwob2JqZWN0KSB8fCBvYmplY3QgPT0gbnVsbClcbiAgICBcbn1cblxuZnVuY3Rpb24gSVNfTk9UX0xJS0VfTlVMTChvYmplY3QpIHtcbiAgICBcbiAgICByZXR1cm4gIUlTX0xJS0VfTlVMTChvYmplY3QpXG4gICAgXG59XG5cblxuZnVuY3Rpb24gSVNfQU5fRU1BSUxfQUREUkVTUyhlbWFpbDogc3RyaW5nKSB7XG4gICAgY29uc3QgcmUgPSAvXFxTK0BcXFMrXFwuXFxTKy9cbiAgICByZXR1cm4gcmUudGVzdChlbWFpbClcbn1cblxuXG5mdW5jdGlvbiBGSVJTVF9PUl9OSUw8VD4oLi4ub2JqZWN0czogVFtdKTogVCB7XG4gICAgXG4gICAgY29uc3QgcmVzdWx0ID0gb2JqZWN0cy5maW5kKGZ1bmN0aW9uIChvYmplY3QsIGluZGV4LCBhcnJheSkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIElTKG9iamVjdClcbiAgICAgICAgXG4gICAgfSlcbiAgICBcbiAgICByZXR1cm4gcmVzdWx0IHx8IG5pbFxuICAgIFxufVxuXG5mdW5jdGlvbiBGSVJTVDxUPiguLi5vYmplY3RzOiBUW10pOiBUIHtcbiAgICBcbiAgICBjb25zdCByZXN1bHQgPSBvYmplY3RzLmZpbmQoZnVuY3Rpb24gKG9iamVjdCwgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gSVMob2JqZWN0KVxuICAgICAgICBcbiAgICB9KVxuICAgIFxuICAgIHJldHVybiByZXN1bHQgfHwgSUYoSVNfREVGSU5FRChvYmplY3RzLmxhc3RFbGVtZW50KSkoUkVUVVJORVIob2JqZWN0cy5sYXN0RWxlbWVudCkpKClcbiAgICBcbn1cblxuXG5mdW5jdGlvbiBNQUtFX0lEKHJhbmRvbVBhcnRMZW5ndGggPSAxNSkge1xuICAgIFxuICAgIHZhciByZXN1bHQgPSBcIlwiXG4gICAgY29uc3QgY2hhcmFjdGVycyA9IFwiQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVphYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ejAxMjM0NTY3ODlcIlxuICAgIFxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcmFuZG9tUGFydExlbmd0aDsgaSsrKSB7XG4gICAgICAgIFxuICAgICAgICByZXN1bHQgPSByZXN1bHQgKyBjaGFyYWN0ZXJzLmNoYXJBdChNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBjaGFyYWN0ZXJzLmxlbmd0aCkpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICByZXN1bHQgPSByZXN1bHQgKyBEYXRlLm5vdygpXG4gICAgXG4gICAgcmV0dXJuIHJlc3VsdFxuICAgIFxufVxuXG5cbmZ1bmN0aW9uIFJFVFVSTkVSPFQ+KHZhbHVlOiBUKSB7XG4gICAgXG4gICAgcmV0dXJuIGZ1bmN0aW9uICguLi5vYmplY3RzOiBhbnlbXSkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHZhbHVlXG4gICAgICAgIFxuICAgIH1cbiAgICBcbn1cblxuXG5cblxuXG50eXBlIFVJSUZCbG9ja1JlY2VpdmVyPFQ+ID0gKGZ1bmN0aW9uVG9DYWxsOiAoKSA9PiBhbnkpID0+IFVJSUZFdmFsdWF0b3I8VD47XG5cbnR5cGUgVUlJRkV2YWx1YXRvckJhc2U8VD4gPSAoKSA9PiBUO1xuXG5cbmludGVyZmFjZSBVSUlGRXZhbHVhdG9yPFQ+IGV4dGVuZHMgVUlJRkV2YWx1YXRvckJhc2U8VD4ge1xuICAgIFxuICAgIEVMU0VfSUY6IChvdGhlclZhbHVlOiBhbnkpID0+IFVJSUZCbG9ja1JlY2VpdmVyPFQ+O1xuICAgIEVMU0U6IChmdW5jdGlvblRvQ2FsbDogKCkgPT4gYW55KSA9PiBUO1xuICAgIFxufVxuXG5cbmZ1bmN0aW9uIElGPFQgPSBhbnk+KHZhbHVlOiBhbnkpOiBVSUlGQmxvY2tSZWNlaXZlcjxUPiB7XG4gICAgXG4gICAgdmFyIHRoZW5GdW5jdGlvbiA9IG5pbFxuICAgIHZhciBlbHNlRnVuY3Rpb24gPSBuaWxcbiAgICBcbiAgICBjb25zdCByZXN1bHQ6IGFueSA9IGZ1bmN0aW9uIChmdW5jdGlvblRvQ2FsbDogKCkgPT4gVCkge1xuICAgICAgICB0aGVuRnVuY3Rpb24gPSBmdW5jdGlvblRvQ2FsbFxuICAgICAgICByZXR1cm4gcmVzdWx0LmV2YWx1YXRlQ29uZGl0aW9uc1xuICAgIH1cbiAgICBcbiAgICBcbiAgICByZXN1bHQuZXZhbHVhdGVDb25kaXRpb25zID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoSVModmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhlbkZ1bmN0aW9uKClcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZWxzZUZ1bmN0aW9uKClcbiAgICB9XG4gICAgXG4gICAgXG4gICAgcmVzdWx0LmV2YWx1YXRlQ29uZGl0aW9ucy5FTFNFX0lGID0gZnVuY3Rpb24gKG90aGVyVmFsdWU6IGFueSkge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgZnVuY3Rpb25SZXN1bHQgPSBJRihvdGhlclZhbHVlKSBhcyAoVUlJRkJsb2NrUmVjZWl2ZXI8VD4gJiB7IGV2YWx1YXRlQ29uZGl0aW9uczogVUlJRkV2YWx1YXRvcjxUPiB9KVxuICAgICAgICBlbHNlRnVuY3Rpb24gPSBmdW5jdGlvblJlc3VsdC5ldmFsdWF0ZUNvbmRpdGlvbnNcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGZ1bmN0aW9uUmVzdWx0RXZhbHVhdGVDb25kaXRpb25zRnVuY3Rpb246IGFueSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiByZXN1bHQuZXZhbHVhdGVDb25kaXRpb25zKClcbiAgICAgICAgfVxuICAgICAgICBmdW5jdGlvblJlc3VsdEV2YWx1YXRlQ29uZGl0aW9uc0Z1bmN0aW9uLkVMU0VfSUYgPSBmdW5jdGlvblJlc3VsdC5ldmFsdWF0ZUNvbmRpdGlvbnMuRUxTRV9JRlxuICAgICAgICBmdW5jdGlvblJlc3VsdEV2YWx1YXRlQ29uZGl0aW9uc0Z1bmN0aW9uLkVMU0UgPSBmdW5jdGlvblJlc3VsdC5ldmFsdWF0ZUNvbmRpdGlvbnMuRUxTRVxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb25SZXN1bHQuZXZhbHVhdGVDb25kaXRpb25zID0gZnVuY3Rpb25SZXN1bHRFdmFsdWF0ZUNvbmRpdGlvbnNGdW5jdGlvblxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uUmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICByZXN1bHQuZXZhbHVhdGVDb25kaXRpb25zLkVMU0UgPSBmdW5jdGlvbiAoZnVuY3Rpb25Ub0NhbGw6ICgpID0+IFQpIHtcbiAgICAgICAgZWxzZUZ1bmN0aW9uID0gZnVuY3Rpb25Ub0NhbGxcbiAgICAgICAgcmV0dXJuIHJlc3VsdC5ldmFsdWF0ZUNvbmRpdGlvbnMoKVxuICAgIH1cbiAgICBcbiAgICBcbiAgICByZXR1cm4gcmVzdWx0XG59XG5cblxuXG4vLyBAdHMtaWdub3JlXG5pZiAoIXdpbmRvdy5BdXRvTGF5b3V0KSB7XG4gICAgXG4gICAgLy8gQHRzLWlnbm9yZVxuICAgIHdpbmRvdy5BdXRvTGF5b3V0ID0gbmlsXG4gICAgXG59XG5cblxuY2xhc3MgVUlPYmplY3Qge1xuICAgIFxuICAgIF9jbGFzczogYW55XG4gICAgXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IFVJT2JqZWN0XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IG5pbC5jbGFzc1xuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgY2xhc3MoKTogYW55IHtcbiAgICAgICAgcmV0dXJuICh0aGlzLmNvbnN0cnVjdG9yIGFzIGFueSlcbiAgICB9XG4gICAgXG4gICAgXG4gICAgcHVibGljIGdldCBzdXBlcmNsYXNzKCk6IGFueSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gKHRoaXMuY29uc3RydWN0b3IgYXMgYW55KS5zdXBlcmNsYXNzXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgc2V0IHN1cGVyY2xhc3Moc3VwZXJjbGFzczogYW55KSB7XG4gICAgICAgICh0aGlzLmNvbnN0cnVjdG9yIGFzIGFueSkuc3VwZXJjbGFzcyA9IHN1cGVyY2xhc3NcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgcHVibGljIHN0YXRpYyB3cmFwT2JqZWN0PFQ+KG9iamVjdDogVCk6IFVJT2JqZWN0ICYgVCB7XG4gICAgICAgIFxuICAgICAgICBpZiAoSVNfTk9UKG9iamVjdCkpIHtcbiAgICAgICAgICAgIHJldHVybiBuaWxcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mIFVJT2JqZWN0KSB7XG4gICAgICAgICAgICByZXR1cm4gb2JqZWN0XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IE9iamVjdC5hc3NpZ24obmV3IFVJT2JqZWN0KCksIG9iamVjdClcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGlzS2luZE9mQ2xhc3MoY2xhc3NPYmplY3QpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNZW1iZXJPZkNsYXNzKGNsYXNzT2JqZWN0KSkge1xuICAgICAgICAgICAgcmV0dXJuIFlFU1xuICAgICAgICB9XG4gICAgICAgIGZvciAodmFyIHN1cGVyY2xhc3NPYmplY3QgPSB0aGlzLnN1cGVyY2xhc3M7IElTKHN1cGVyY2xhc3NPYmplY3QpOyBzdXBlcmNsYXNzT2JqZWN0ID0gc3VwZXJjbGFzc09iamVjdC5zdXBlcmNsYXNzKSB7XG4gICAgICAgICAgICBpZiAoc3VwZXJjbGFzc09iamVjdCA9PSBjbGFzc09iamVjdCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gTk9cbiAgICB9XG4gICAgXG4gICAgXG4gICAgaXNNZW1iZXJPZkNsYXNzKGNsYXNzT2JqZWN0OiBhbnkpIHtcbiAgICAgICAgcmV0dXJuICh0aGlzLmNsYXNzID09IGNsYXNzT2JqZWN0KVxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICB2YWx1ZUZvcktleShrZXk6IHN0cmluZykge1xuICAgICAgICByZXR1cm4gdGhpc1trZXldXG4gICAgfVxuICAgIFxuICAgIHZhbHVlRm9yS2V5UGF0aChrZXlQYXRoOiBzdHJpbmcpOiBhbnkge1xuICAgICAgICByZXR1cm4gVUlPYmplY3QudmFsdWVGb3JLZXlQYXRoKGtleVBhdGgsIHRoaXMpXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyB2YWx1ZUZvcktleVBhdGgoa2V5UGF0aDogc3RyaW5nLCBvYmplY3Q6IGFueSk6IGFueSB7XG4gICAgXG4gICAgICAgIGlmIChJU19OT1Qoa2V5UGF0aCkpIHtcbiAgICBcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGtleXMgPSBrZXlQYXRoLnNwbGl0KFwiLlwiKVxuICAgICAgICB2YXIgY3VycmVudE9iamVjdCA9IG9iamVjdFxuICAgICAgICBcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IGtleSA9IGtleXNbaV1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGtleS5zdWJzdHJpbmcoMCwgMikgPT0gXCJbXVwiKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgLy8gVGhpcyBuZXh0IG9iamVjdCB3aWxsIGJlIGFuIGFycmF5IGFuZCB0aGUgcmVzdCBvZiB0aGUga2V5cyBuZWVkIHRvIGJlIHJ1biBmb3IgZWFjaCBvZiB0aGUgZWxlbWVudHNcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBjdXJyZW50T2JqZWN0ID0gY3VycmVudE9iamVjdFtrZXkuc3Vic3RyaW5nKDIpXVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC8vIEN1cnJlbnRPYmplY3QgaXMgbm93IGFuIGFycmF5XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29uc3QgcmVtYWluaW5nS2V5UGF0aCA9IGtleXMuc2xpY2UoaSArIDEpLmpvaW4oXCIuXCIpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudEFycmF5ID0gY3VycmVudE9iamVjdCBhcyB1bmtub3duIGFzIGFueVtdXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY3VycmVudE9iamVjdCA9IGN1cnJlbnRBcnJheS5tYXAoZnVuY3Rpb24gKHN1Yk9iamVjdCwgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSBVSU9iamVjdC52YWx1ZUZvcktleVBhdGgocmVtYWluaW5nS2V5UGF0aCwgc3ViT2JqZWN0KVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGN1cnJlbnRPYmplY3QgPSBjdXJyZW50T2JqZWN0W2tleV1cbiAgICAgICAgICAgIGlmIChJU19OT1QoY3VycmVudE9iamVjdCkpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50T2JqZWN0ID0gbmlsXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIGN1cnJlbnRPYmplY3RcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNldFZhbHVlRm9yS2V5UGF0aChrZXlQYXRoOiBzdHJpbmcsIHZhbHVlOiBhbnksIGNyZWF0ZVBhdGggPSBZRVMpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiBVSU9iamVjdC5zZXRWYWx1ZUZvcktleVBhdGgoa2V5UGF0aCwgdmFsdWUsIHRoaXMsIGNyZWF0ZVBhdGgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgc2V0VmFsdWVGb3JLZXlQYXRoKGtleVBhdGg6IHN0cmluZywgdmFsdWU6IGFueSwgY3VycmVudE9iamVjdDogYW55LCBjcmVhdGVQYXRoKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBrZXlzID0ga2V5UGF0aC5zcGxpdChcIi5cIilcbiAgICAgICAgdmFyIGRpZFNldFZhbHVlID0gTk9cbiAgICAgICAgXG4gICAgICAgIGtleXMuZm9yRWFjaChmdW5jdGlvbiAoa2V5LCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIGlmIChpbmRleCA9PSBhcnJheS5sZW5ndGggLSAxICYmIElTX05PVF9MSUtFX05VTEwoY3VycmVudE9iamVjdCkpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50T2JqZWN0W2tleV0gPSB2YWx1ZVxuICAgICAgICAgICAgICAgIGRpZFNldFZhbHVlID0gWUVTXG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChJU19OT1QoY3VycmVudE9iamVjdCkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29uc3QgY3VycmVudE9iamVjdFZhbHVlID0gY3VycmVudE9iamVjdFtrZXldXG4gICAgICAgICAgICBpZiAoSVNfTElLRV9OVUxMKGN1cnJlbnRPYmplY3RWYWx1ZSkgJiYgY3JlYXRlUGF0aCkge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRPYmplY3Rba2V5XSA9IHt9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjdXJyZW50T2JqZWN0ID0gY3VycmVudE9iamVjdFtrZXldXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gZGlkU2V0VmFsdWVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHBlcmZvcm1GdW5jdGlvbldpdGhTZWxmKGZ1bmN0aW9uVG9QZXJmb3JtOiAoc2VsZjogdGhpcykgPT4gYW55KSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gZnVuY3Rpb25Ub1BlcmZvcm0odGhpcylcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHBlcmZvcm1GdW5jdGlvbldpdGhEZWxheShkZWxheTogbnVtYmVyLCBmdW5jdGlvblRvQ2FsbDogRnVuY3Rpb24pIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgbmV3IFVJVGltZXIoZGVsYXksIE5PLCBmdW5jdGlvblRvQ2FsbClcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCJpbnRlcmZhY2UgVUlDb2xvckRlc2NyaXB0b3Ige1xuICAgIFxuICAgIHJlZDogbnVtYmVyO1xuICAgIGdyZWVuOiBudW1iZXI7XG4gICAgYmx1ZTogbnVtYmVyO1xuICAgIGFscGhhPzogbnVtYmVyO1xuICAgIFxufVxuXG5cblxuXG5cbmNsYXNzIFVJQ29sb3IgZXh0ZW5kcyBVSU9iamVjdCB7XG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IocHVibGljIHN0cmluZ1ZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlDb2xvclxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSU9iamVjdFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICB0b1N0cmluZygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RyaW5nVmFsdWVcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIGdldCByZWRDb2xvcigpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBVSUNvbG9yKFwicmVkXCIpXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBnZXQgYmx1ZUNvbG9yKCkge1xuICAgICAgICByZXR1cm4gbmV3IFVJQ29sb3IoXCJibHVlXCIpXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBnZXQgZ3JlZW5Db2xvcigpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBVSUNvbG9yKFwiZ3JlZW5cIilcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIGdldCB5ZWxsb3dDb2xvcigpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBVSUNvbG9yKFwieWVsbG93XCIpXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBnZXQgYmxhY2tDb2xvcigpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBVSUNvbG9yKFwiYmxhY2tcIilcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIGdldCBicm93bkNvbG9yKCkge1xuICAgICAgICByZXR1cm4gbmV3IFVJQ29sb3IoXCJicm93blwiKVxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgZ2V0IHdoaXRlQ29sb3IoKSB7XG4gICAgICAgIHJldHVybiBuZXcgVUlDb2xvcihcIndoaXRlXCIpXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBnZXQgZ3JleUNvbG9yKCkge1xuICAgICAgICByZXR1cm4gbmV3IFVJQ29sb3IoXCJncmV5XCIpXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBnZXQgbGlnaHRHcmV5Q29sb3IoKSB7XG4gICAgICAgIHJldHVybiBuZXcgVUlDb2xvcihcImxpZ2h0Z3JleVwiKVxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgZ2V0IHRyYW5zcGFyZW50Q29sb3IoKSB7XG4gICAgICAgIHJldHVybiBuZXcgVUlDb2xvcihcInRyYW5zcGFyZW50XCIpXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBnZXQgdW5kZWZpbmVkQ29sb3IoKSB7XG4gICAgICAgIHJldHVybiBuZXcgVUlDb2xvcihcIlwiKVxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgZ2V0IG5pbENvbG9yKCkge1xuICAgICAgICByZXR1cm4gbmV3IFVJQ29sb3IoXCJcIilcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc3RhdGljIG5hbWVUb0hleChuYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIFwiYWxpY2VibHVlXCI6IFwiI2YwZjhmZlwiLFxuICAgICAgICAgICAgXCJhbnRpcXVld2hpdGVcIjogXCIjZmFlYmQ3XCIsXG4gICAgICAgICAgICBcImFxdWFcIjogXCIjMDBmZmZmXCIsXG4gICAgICAgICAgICBcImFxdWFtYXJpbmVcIjogXCIjN2ZmZmQ0XCIsXG4gICAgICAgICAgICBcImF6dXJlXCI6IFwiI2YwZmZmZlwiLFxuICAgICAgICAgICAgXCJiZWlnZVwiOiBcIiNmNWY1ZGNcIixcbiAgICAgICAgICAgIFwiYmlzcXVlXCI6IFwiI2ZmZTRjNFwiLFxuICAgICAgICAgICAgXCJibGFja1wiOiBcIiMwMDAwMDBcIixcbiAgICAgICAgICAgIFwiYmxhbmNoZWRhbG1vbmRcIjogXCIjZmZlYmNkXCIsXG4gICAgICAgICAgICBcImJsdWVcIjogXCIjMDAwMGZmXCIsXG4gICAgICAgICAgICBcImJsdWV2aW9sZXRcIjogXCIjOGEyYmUyXCIsXG4gICAgICAgICAgICBcImJyb3duXCI6IFwiI2E1MmEyYVwiLFxuICAgICAgICAgICAgXCJidXJseXdvb2RcIjogXCIjZGViODg3XCIsXG4gICAgICAgICAgICBcImNhZGV0Ymx1ZVwiOiBcIiM1ZjllYTBcIixcbiAgICAgICAgICAgIFwiY2hhcnRyZXVzZVwiOiBcIiM3ZmZmMDBcIixcbiAgICAgICAgICAgIFwiY2hvY29sYXRlXCI6IFwiI2QyNjkxZVwiLFxuICAgICAgICAgICAgXCJjb3JhbFwiOiBcIiNmZjdmNTBcIixcbiAgICAgICAgICAgIFwiY29ybmZsb3dlcmJsdWVcIjogXCIjNjQ5NWVkXCIsXG4gICAgICAgICAgICBcImNvcm5zaWxrXCI6IFwiI2ZmZjhkY1wiLFxuICAgICAgICAgICAgXCJjcmltc29uXCI6IFwiI2RjMTQzY1wiLFxuICAgICAgICAgICAgXCJjeWFuXCI6IFwiIzAwZmZmZlwiLFxuICAgICAgICAgICAgXCJkYXJrYmx1ZVwiOiBcIiMwMDAwOGJcIixcbiAgICAgICAgICAgIFwiZGFya2N5YW5cIjogXCIjMDA4YjhiXCIsXG4gICAgICAgICAgICBcImRhcmtnb2xkZW5yb2RcIjogXCIjYjg4NjBiXCIsXG4gICAgICAgICAgICBcImRhcmtncmF5XCI6IFwiI2E5YTlhOVwiLFxuICAgICAgICAgICAgXCJkYXJrZ3JlZW5cIjogXCIjMDA2NDAwXCIsXG4gICAgICAgICAgICBcImRhcmtraGFraVwiOiBcIiNiZGI3NmJcIixcbiAgICAgICAgICAgIFwiZGFya21hZ2VudGFcIjogXCIjOGIwMDhiXCIsXG4gICAgICAgICAgICBcImRhcmtvbGl2ZWdyZWVuXCI6IFwiIzU1NmIyZlwiLFxuICAgICAgICAgICAgXCJkYXJrb3JhbmdlXCI6IFwiI2ZmOGMwMFwiLFxuICAgICAgICAgICAgXCJkYXJrb3JjaGlkXCI6IFwiIzk5MzJjY1wiLFxuICAgICAgICAgICAgXCJkYXJrcmVkXCI6IFwiIzhiMDAwMFwiLFxuICAgICAgICAgICAgXCJkYXJrc2FsbW9uXCI6IFwiI2U5OTY3YVwiLFxuICAgICAgICAgICAgXCJkYXJrc2VhZ3JlZW5cIjogXCIjOGZiYzhmXCIsXG4gICAgICAgICAgICBcImRhcmtzbGF0ZWJsdWVcIjogXCIjNDgzZDhiXCIsXG4gICAgICAgICAgICBcImRhcmtzbGF0ZWdyYXlcIjogXCIjMmY0ZjRmXCIsXG4gICAgICAgICAgICBcImRhcmt0dXJxdW9pc2VcIjogXCIjMDBjZWQxXCIsXG4gICAgICAgICAgICBcImRhcmt2aW9sZXRcIjogXCIjOTQwMGQzXCIsXG4gICAgICAgICAgICBcImRlZXBwaW5rXCI6IFwiI2ZmMTQ5M1wiLFxuICAgICAgICAgICAgXCJkZWVwc2t5Ymx1ZVwiOiBcIiMwMGJmZmZcIixcbiAgICAgICAgICAgIFwiZGltZ3JheVwiOiBcIiM2OTY5NjlcIixcbiAgICAgICAgICAgIFwiZG9kZ2VyYmx1ZVwiOiBcIiMxZTkwZmZcIixcbiAgICAgICAgICAgIFwiZmlyZWJyaWNrXCI6IFwiI2IyMjIyMlwiLFxuICAgICAgICAgICAgXCJmbG9yYWx3aGl0ZVwiOiBcIiNmZmZhZjBcIixcbiAgICAgICAgICAgIFwiZm9yZXN0Z3JlZW5cIjogXCIjMjI4YjIyXCIsXG4gICAgICAgICAgICBcImZ1Y2hzaWFcIjogXCIjZmYwMGZmXCIsXG4gICAgICAgICAgICBcImdhaW5zYm9yb1wiOiBcIiNkY2RjZGNcIixcbiAgICAgICAgICAgIFwiZ2hvc3R3aGl0ZVwiOiBcIiNmOGY4ZmZcIixcbiAgICAgICAgICAgIFwiZ29sZFwiOiBcIiNmZmQ3MDBcIixcbiAgICAgICAgICAgIFwiZ29sZGVucm9kXCI6IFwiI2RhYTUyMFwiLFxuICAgICAgICAgICAgXCJncmF5XCI6IFwiIzgwODA4MFwiLFxuICAgICAgICAgICAgXCJncmVlblwiOiBcIiMwMDgwMDBcIixcbiAgICAgICAgICAgIFwiZ3JlZW55ZWxsb3dcIjogXCIjYWRmZjJmXCIsXG4gICAgICAgICAgICBcImhvbmV5ZGV3XCI6IFwiI2YwZmZmMFwiLFxuICAgICAgICAgICAgXCJob3RwaW5rXCI6IFwiI2ZmNjliNFwiLFxuICAgICAgICAgICAgXCJpbmRpYW5yZWQgXCI6IFwiI2NkNWM1Y1wiLFxuICAgICAgICAgICAgXCJpbmRpZ29cIjogXCIjNGIwMDgyXCIsXG4gICAgICAgICAgICBcIml2b3J5XCI6IFwiI2ZmZmZmMFwiLFxuICAgICAgICAgICAgXCJraGFraVwiOiBcIiNmMGU2OGNcIixcbiAgICAgICAgICAgIFwibGF2ZW5kZXJcIjogXCIjZTZlNmZhXCIsXG4gICAgICAgICAgICBcImxhdmVuZGVyYmx1c2hcIjogXCIjZmZmMGY1XCIsXG4gICAgICAgICAgICBcImxhd25ncmVlblwiOiBcIiM3Y2ZjMDBcIixcbiAgICAgICAgICAgIFwibGVtb25jaGlmZm9uXCI6IFwiI2ZmZmFjZFwiLFxuICAgICAgICAgICAgXCJsaWdodGJsdWVcIjogXCIjYWRkOGU2XCIsXG4gICAgICAgICAgICBcImxpZ2h0Y29yYWxcIjogXCIjZjA4MDgwXCIsXG4gICAgICAgICAgICBcImxpZ2h0Y3lhblwiOiBcIiNlMGZmZmZcIixcbiAgICAgICAgICAgIFwibGlnaHRnb2xkZW5yb2R5ZWxsb3dcIjogXCIjZmFmYWQyXCIsXG4gICAgICAgICAgICBcImxpZ2h0Z3JleVwiOiBcIiNkM2QzZDNcIixcbiAgICAgICAgICAgIFwibGlnaHRncmVlblwiOiBcIiM5MGVlOTBcIixcbiAgICAgICAgICAgIFwibGlnaHRwaW5rXCI6IFwiI2ZmYjZjMVwiLFxuICAgICAgICAgICAgXCJsaWdodHNhbG1vblwiOiBcIiNmZmEwN2FcIixcbiAgICAgICAgICAgIFwibGlnaHRzZWFncmVlblwiOiBcIiMyMGIyYWFcIixcbiAgICAgICAgICAgIFwibGlnaHRza3libHVlXCI6IFwiIzg3Y2VmYVwiLFxuICAgICAgICAgICAgXCJsaWdodHNsYXRlZ3JheVwiOiBcIiM3Nzg4OTlcIixcbiAgICAgICAgICAgIFwibGlnaHRzdGVlbGJsdWVcIjogXCIjYjBjNGRlXCIsXG4gICAgICAgICAgICBcImxpZ2h0eWVsbG93XCI6IFwiI2ZmZmZlMFwiLFxuICAgICAgICAgICAgXCJsaW1lXCI6IFwiIzAwZmYwMFwiLFxuICAgICAgICAgICAgXCJsaW1lZ3JlZW5cIjogXCIjMzJjZDMyXCIsXG4gICAgICAgICAgICBcImxpbmVuXCI6IFwiI2ZhZjBlNlwiLFxuICAgICAgICAgICAgXCJtYWdlbnRhXCI6IFwiI2ZmMDBmZlwiLFxuICAgICAgICAgICAgXCJtYXJvb25cIjogXCIjODAwMDAwXCIsXG4gICAgICAgICAgICBcIm1lZGl1bWFxdWFtYXJpbmVcIjogXCIjNjZjZGFhXCIsXG4gICAgICAgICAgICBcIm1lZGl1bWJsdWVcIjogXCIjMDAwMGNkXCIsXG4gICAgICAgICAgICBcIm1lZGl1bW9yY2hpZFwiOiBcIiNiYTU1ZDNcIixcbiAgICAgICAgICAgIFwibWVkaXVtcHVycGxlXCI6IFwiIzkzNzBkOFwiLFxuICAgICAgICAgICAgXCJtZWRpdW1zZWFncmVlblwiOiBcIiMzY2IzNzFcIixcbiAgICAgICAgICAgIFwibWVkaXVtc2xhdGVibHVlXCI6IFwiIzdiNjhlZVwiLFxuICAgICAgICAgICAgXCJtZWRpdW1zcHJpbmdncmVlblwiOiBcIiMwMGZhOWFcIixcbiAgICAgICAgICAgIFwibWVkaXVtdHVycXVvaXNlXCI6IFwiIzQ4ZDFjY1wiLFxuICAgICAgICAgICAgXCJtZWRpdW12aW9sZXRyZWRcIjogXCIjYzcxNTg1XCIsXG4gICAgICAgICAgICBcIm1pZG5pZ2h0Ymx1ZVwiOiBcIiMxOTE5NzBcIixcbiAgICAgICAgICAgIFwibWludGNyZWFtXCI6IFwiI2Y1ZmZmYVwiLFxuICAgICAgICAgICAgXCJtaXN0eXJvc2VcIjogXCIjZmZlNGUxXCIsXG4gICAgICAgICAgICBcIm1vY2Nhc2luXCI6IFwiI2ZmZTRiNVwiLFxuICAgICAgICAgICAgXCJuYXZham93aGl0ZVwiOiBcIiNmZmRlYWRcIixcbiAgICAgICAgICAgIFwibmF2eVwiOiBcIiMwMDAwODBcIixcbiAgICAgICAgICAgIFwib2xkbGFjZVwiOiBcIiNmZGY1ZTZcIixcbiAgICAgICAgICAgIFwib2xpdmVcIjogXCIjODA4MDAwXCIsXG4gICAgICAgICAgICBcIm9saXZlZHJhYlwiOiBcIiM2YjhlMjNcIixcbiAgICAgICAgICAgIFwib3JhbmdlXCI6IFwiI2ZmYTUwMFwiLFxuICAgICAgICAgICAgXCJvcmFuZ2VyZWRcIjogXCIjZmY0NTAwXCIsXG4gICAgICAgICAgICBcIm9yY2hpZFwiOiBcIiNkYTcwZDZcIixcbiAgICAgICAgICAgIFwicGFsZWdvbGRlbnJvZFwiOiBcIiNlZWU4YWFcIixcbiAgICAgICAgICAgIFwicGFsZWdyZWVuXCI6IFwiIzk4ZmI5OFwiLFxuICAgICAgICAgICAgXCJwYWxldHVycXVvaXNlXCI6IFwiI2FmZWVlZVwiLFxuICAgICAgICAgICAgXCJwYWxldmlvbGV0cmVkXCI6IFwiI2Q4NzA5M1wiLFxuICAgICAgICAgICAgXCJwYXBheWF3aGlwXCI6IFwiI2ZmZWZkNVwiLFxuICAgICAgICAgICAgXCJwZWFjaHB1ZmZcIjogXCIjZmZkYWI5XCIsXG4gICAgICAgICAgICBcInBlcnVcIjogXCIjY2Q4NTNmXCIsXG4gICAgICAgICAgICBcInBpbmtcIjogXCIjZmZjMGNiXCIsXG4gICAgICAgICAgICBcInBsdW1cIjogXCIjZGRhMGRkXCIsXG4gICAgICAgICAgICBcInBvd2RlcmJsdWVcIjogXCIjYjBlMGU2XCIsXG4gICAgICAgICAgICBcInB1cnBsZVwiOiBcIiM4MDAwODBcIixcbiAgICAgICAgICAgIFwicmVkXCI6IFwiI2ZmMDAwMFwiLFxuICAgICAgICAgICAgXCJyb3N5YnJvd25cIjogXCIjYmM4ZjhmXCIsXG4gICAgICAgICAgICBcInJveWFsYmx1ZVwiOiBcIiM0MTY5ZTFcIixcbiAgICAgICAgICAgIFwic2FkZGxlYnJvd25cIjogXCIjOGI0NTEzXCIsXG4gICAgICAgICAgICBcInNhbG1vblwiOiBcIiNmYTgwNzJcIixcbiAgICAgICAgICAgIFwic2FuZHlicm93blwiOiBcIiNmNGE0NjBcIixcbiAgICAgICAgICAgIFwic2VhZ3JlZW5cIjogXCIjMmU4YjU3XCIsXG4gICAgICAgICAgICBcInNlYXNoZWxsXCI6IFwiI2ZmZjVlZVwiLFxuICAgICAgICAgICAgXCJzaWVubmFcIjogXCIjYTA1MjJkXCIsXG4gICAgICAgICAgICBcInNpbHZlclwiOiBcIiNjMGMwYzBcIixcbiAgICAgICAgICAgIFwic2t5Ymx1ZVwiOiBcIiM4N2NlZWJcIixcbiAgICAgICAgICAgIFwic2xhdGVibHVlXCI6IFwiIzZhNWFjZFwiLFxuICAgICAgICAgICAgXCJzbGF0ZWdyYXlcIjogXCIjNzA4MDkwXCIsXG4gICAgICAgICAgICBcInNub3dcIjogXCIjZmZmYWZhXCIsXG4gICAgICAgICAgICBcInNwcmluZ2dyZWVuXCI6IFwiIzAwZmY3ZlwiLFxuICAgICAgICAgICAgXCJzdGVlbGJsdWVcIjogXCIjNDY4MmI0XCIsXG4gICAgICAgICAgICBcInRhblwiOiBcIiNkMmI0OGNcIixcbiAgICAgICAgICAgIFwidGVhbFwiOiBcIiMwMDgwODBcIixcbiAgICAgICAgICAgIFwidGhpc3RsZVwiOiBcIiNkOGJmZDhcIixcbiAgICAgICAgICAgIFwidG9tYXRvXCI6IFwiI2ZmNjM0N1wiLFxuICAgICAgICAgICAgXCJ0dXJxdW9pc2VcIjogXCIjNDBlMGQwXCIsXG4gICAgICAgICAgICBcInZpb2xldFwiOiBcIiNlZTgyZWVcIixcbiAgICAgICAgICAgIFwid2hlYXRcIjogXCIjZjVkZWIzXCIsXG4gICAgICAgICAgICBcIndoaXRlXCI6IFwiI2ZmZmZmZlwiLFxuICAgICAgICAgICAgXCJ3aGl0ZXNtb2tlXCI6IFwiI2Y1ZjVmNVwiLFxuICAgICAgICAgICAgXCJ5ZWxsb3dcIjogXCIjZmZmZjAwXCIsXG4gICAgICAgICAgICBcInllbGxvd2dyZWVuXCI6IFwiIzlhY2QzMlwiXG4gICAgICAgIH1bbmFtZS50b0xvd2VyQ2FzZSgpXVxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgaGV4VG9EZXNjcmlwdG9yKGM6IHN0cmluZyk6IFVJQ29sb3JEZXNjcmlwdG9yIHtcbiAgICAgICAgaWYgKGNbMF0gPT09IFwiI1wiKSB7XG4gICAgICAgICAgICBjID0gYy5zdWJzdHIoMSlcbiAgICAgICAgfVxuICAgICAgICBjb25zdCByID0gcGFyc2VJbnQoYy5zbGljZSgwLCAyKSwgMTYpXG4gICAgICAgIGNvbnN0IGcgPSBwYXJzZUludChjLnNsaWNlKDIsIDQpLCAxNilcbiAgICAgICAgY29uc3QgYiA9IHBhcnNlSW50KGMuc2xpY2UoNCwgNiksIDE2KVxuICAgICAgICBjb25zdCBhID0gcGFyc2VJbnQoYy5zbGljZSg2LCA4KSwgMTYpXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHsgXCJyZWRcIjogciwgXCJncmVlblwiOiBnLCBcImJsdWVcIjogYiwgXCJhbHBoYVwiOiBhIH1cbiAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICAgICAgLy9yZXR1cm4gJ3JnYignICsgciArICcsJyArIGcgKyAnLCcgKyBiICsgJyknO1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIHJnYlRvRGVzY3JpcHRvcihjb2xvclN0cmluZzogc3RyaW5nKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgaWYgKGNvbG9yU3RyaW5nLnN0YXJ0c1dpdGgoXCJyZ2JhKFwiKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb2xvclN0cmluZyA9IGNvbG9yU3RyaW5nLnNsaWNlKDUsIGNvbG9yU3RyaW5nLmxlbmd0aCAtIDEpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKGNvbG9yU3RyaW5nLnN0YXJ0c1dpdGgoXCJyZ2IoXCIpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbG9yU3RyaW5nID0gY29sb3JTdHJpbmcuc2xpY2UoNCwgY29sb3JTdHJpbmcubGVuZ3RoIC0gMSkgKyBcIiwgMFwiXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIFxuICAgIFxuICAgICAgICBjb25zdCBjb21wb25lbnRzID0gY29sb3JTdHJpbmcuc3BsaXQoXCIsXCIpXG4gICAgXG4gICAgXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHtcbiAgICAgICAgICAgIFwicmVkXCI6IE51bWJlcihjb21wb25lbnRzWzBdKSxcbiAgICAgICAgICAgIFwiZ3JlZW5cIjogTnVtYmVyKGNvbXBvbmVudHNbMV0pLFxuICAgICAgICAgICAgXCJibHVlXCI6IE51bWJlcihjb21wb25lbnRzWzJdKSxcbiAgICAgICAgICAgIFwiYWxwaGFcIjogTnVtYmVyKGNvbXBvbmVudHNbM10pXG4gICAgICAgIH1cbiAgICBcbiAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCBjb2xvckRlc2NyaXB0b3IoKTogVUlDb2xvckRlc2NyaXB0b3Ige1xuICAgIFxuICAgICAgICB2YXIgZGVzY3JpcHRvclxuICAgIFxuICAgICAgICBjb25zdCBjb2xvckhFWEZyb21OYW1lID0gVUlDb2xvci5uYW1lVG9IZXgodGhpcy5zdHJpbmdWYWx1ZSlcbiAgICBcbiAgICAgICAgaWYgKHRoaXMuc3RyaW5nVmFsdWUuc3RhcnRzV2l0aChcInJnYlwiKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBkZXNjcmlwdG9yID0gVUlDb2xvci5yZ2JUb0Rlc2NyaXB0b3IodGhpcy5zdHJpbmdWYWx1ZSlcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGNvbG9ySEVYRnJvbU5hbWUpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZGVzY3JpcHRvciA9IFVJQ29sb3IuaGV4VG9EZXNjcmlwdG9yKGNvbG9ySEVYRnJvbU5hbWUpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZGVzY3JpcHRvciA9IFVJQ29sb3IuaGV4VG9EZXNjcmlwdG9yKHRoaXMuc3RyaW5nVmFsdWUpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIGRlc2NyaXB0b3JcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGNvbG9yV2l0aFJlZChyZWQ6IG51bWJlcikge1xuICAgIFxuICAgIFxuICAgICAgICBjb25zdCBkZXNjcmlwdG9yID0gdGhpcy5jb2xvckRlc2NyaXB0b3JcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IFVJQ29sb3IoXCJyZ2JhKFwiICsgcmVkICsgXCIsXCIgKyBkZXNjcmlwdG9yLmdyZWVuICsgXCIsXCIgKyBkZXNjcmlwdG9yLmJsdWUgKyBcIixcIiArXG4gICAgICAgICAgICBkZXNjcmlwdG9yLmFscGhhICsgXCIpXCIpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGNvbG9yV2l0aEdyZWVuKGdyZWVuOiBudW1iZXIpIHtcbiAgICBcbiAgICBcbiAgICAgICAgY29uc3QgZGVzY3JpcHRvciA9IHRoaXMuY29sb3JEZXNjcmlwdG9yXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBVSUNvbG9yKFwicmdiYShcIiArIGRlc2NyaXB0b3IucmVkICsgXCIsXCIgKyBncmVlbiArIFwiLFwiICsgZGVzY3JpcHRvci5ibHVlICsgXCIsXCIgK1xuICAgICAgICAgICAgZGVzY3JpcHRvci5hbHBoYSArIFwiKVwiKVxuICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBjb2xvcldpdGhCbHVlKGJsdWU6IG51bWJlcikge1xuICAgIFxuICAgIFxuICAgICAgICBjb25zdCBkZXNjcmlwdG9yID0gdGhpcy5jb2xvckRlc2NyaXB0b3JcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IFVJQ29sb3IoXCJyZ2JhKFwiICsgZGVzY3JpcHRvci5yZWQgKyBcIixcIiArIGRlc2NyaXB0b3IuZ3JlZW4gKyBcIixcIiArIGJsdWUgKyBcIixcIiArXG4gICAgICAgICAgICBkZXNjcmlwdG9yLmFscGhhICsgXCIpXCIpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGNvbG9yV2l0aEFscGhhKGFscGhhOiBudW1iZXIpIHtcbiAgICBcbiAgICBcbiAgICAgICAgY29uc3QgZGVzY3JpcHRvciA9IHRoaXMuY29sb3JEZXNjcmlwdG9yXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBVSUNvbG9yKFwicmdiYShcIiArIGRlc2NyaXB0b3IucmVkICsgXCIsXCIgKyBkZXNjcmlwdG9yLmdyZWVuICsgXCIsXCIgKyBkZXNjcmlwdG9yLmJsdWUgKyBcIixcIiArXG4gICAgICAgICAgICBhbHBoYSArIFwiKVwiKVxuICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgY29sb3JXaXRoUkdCQShyZWQ6IG51bWJlciwgZ3JlZW46IG51bWJlciwgYmx1ZTogbnVtYmVyLCBhbHBoYTogbnVtYmVyID0gMSkge1xuICAgIFxuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSBuZXcgVUlDb2xvcihcInJnYmEoXCIgKyByZWQgKyBcIixcIiArIGdyZWVuICsgXCIsXCIgKyBibHVlICsgXCIsXCIgKyBhbHBoYSArIFwiKVwiKVxuICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIGNvbG9yV2l0aERlc2NyaXB0b3IoZGVzY3JpcHRvcjogVUlDb2xvckRlc2NyaXB0b3IpIHtcbiAgICBcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IFVJQ29sb3IoXCJyZ2JhKFwiICsgZGVzY3JpcHRvci5yZWQudG9GaXhlZCgwKSArIFwiLFwiICsgZGVzY3JpcHRvci5ncmVlbi50b0ZpeGVkKDApICsgXCIsXCIgK1xuICAgICAgICAgICAgZGVzY3JpcHRvci5ibHVlLnRvRml4ZWQoMCkgKyBcIixcIiArIHRoaXMuZGVmYXVsdEFscGhhVG9PbmUoZGVzY3JpcHRvci5hbHBoYSkgKyBcIilcIilcbiAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgcHJpdmF0ZSBzdGF0aWMgZGVmYXVsdEFscGhhVG9PbmUodmFsdWUgPSAxKSB7XG4gICAgICAgIGlmICh2YWx1ZSAhPSB2YWx1ZSkge1xuICAgICAgICAgICAgdmFsdWUgPSAxXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHZhbHVlXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGNvbG9yQnlNdWx0aXBseWluZ1JHQihtdWx0aXBsaWVyOiBudW1iZXIpIHtcbiAgICBcbiAgICAgICAgY29uc3QgZGVzY3JpcHRvciA9IHRoaXMuY29sb3JEZXNjcmlwdG9yXG4gICAgXG4gICAgICAgIGRlc2NyaXB0b3IucmVkID0gZGVzY3JpcHRvci5yZWQgKiBtdWx0aXBsaWVyXG4gICAgICAgIGRlc2NyaXB0b3IuZ3JlZW4gPSBkZXNjcmlwdG9yLmdyZWVuICogbXVsdGlwbGllclxuICAgICAgICBkZXNjcmlwdG9yLmJsdWUgPSBkZXNjcmlwdG9yLmJsdWUgKiBtdWx0aXBsaWVyXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IFVJQ29sb3IuY29sb3JXaXRoRGVzY3JpcHRvcihkZXNjcmlwdG9yKVxuICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9VSU9iamVjdC50c1wiIC8+XG5cblxuXG5cbmNsYXNzIFVJUG9pbnQgZXh0ZW5kcyBVSU9iamVjdCB7XG4gICAgXG4gICAgY29uc3RydWN0b3IocHVibGljIHg6IG51bWJlciwgcHVibGljIHk6IG51bWJlcikge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBVSVBvaW50XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJT2JqZWN0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBjb3B5KCkge1xuICAgICAgICByZXR1cm4gbmV3IFVJUG9pbnQodGhpcy54LCB0aGlzLnkpXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGlzRXF1YWxUbyhwb2ludDogVUlQb2ludCkge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSAodGhpcy54ID09IHBvaW50LnggJiYgdGhpcy55ID09IHBvaW50LnkpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNjYWxlKHpvb206IG51bWJlcikge1xuICAgICAgICBjb25zdCB4ID0gdGhpcy54XG4gICAgICAgIGNvbnN0IHkgPSB0aGlzLnlcbiAgICAgICAgdGhpcy54ID0geCAqIHpvb21cbiAgICAgICAgdGhpcy55ID0geSAqIHpvb21cbiAgICAgICAgcmV0dXJuIHRoaXNcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgYWRkKHY6IFVJUG9pbnQpIHtcbiAgICAgICAgdGhpcy54ID0gdGhpcy54ICsgdi54XG4gICAgICAgIHRoaXMueSA9IHRoaXMueSArIHYueVxuICAgICAgICByZXR1cm4gdGhpc1xuICAgIH1cbiAgICBcbiAgICBzdWJ0cmFjdCh2OiBVSVBvaW50KSB7XG4gICAgICAgIHRoaXMueCA9IHRoaXMueCAtIHYueFxuICAgICAgICB0aGlzLnkgPSB0aGlzLnkgLSB2LnlcbiAgICAgICAgcmV0dXJuIHRoaXNcbiAgICB9XG4gICAgXG4gICAgdG8oYjogVUlQb2ludCkge1xuICAgICAgICBjb25zdCBhID0gdGhpc1xuICAgICAgICBjb25zdCBhYiA9IGIuY29weSgpLmFkZChhLmNvcHkoKS5zY2FsZSgtMSkpXG4gICAgICAgIHJldHVybiBhYlxuICAgIH1cbiAgICBcbiAgICBwb2ludFdpdGhYKHg6IG51bWJlcikge1xuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICByZXN1bHQueCA9IHhcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBwb2ludFdpdGhZKHk6IG51bWJlcikge1xuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICByZXN1bHQueSA9IHlcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBwb2ludEJ5QWRkaW5nWCh4OiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucG9pbnRXaXRoWCh0aGlzLnggKyB4KVxuICAgIH1cbiAgICBcbiAgICBwb2ludEJ5QWRkaW5nWSh5OiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucG9pbnRXaXRoWSh0aGlzLnkgKyB5KVxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgbGVuZ3RoKCkge1xuICAgICAgICB2YXIgcmVzdWx0ID0gdGhpcy54ICogdGhpcy54ICsgdGhpcy55ICogdGhpcy55XG4gICAgICAgIHJlc3VsdCA9IE1hdGguc3FydChyZXN1bHQpXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgXG4gICAgZGlkQ2hhbmdlKGIpIHtcbiAgICAgICAgXG4gICAgICAgIC8vIENhbGxiYWNrIHRvIGJlIHNldCBieSBkZWxlZ2F0ZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuL1VJUG9pbnQudHNcIiAvPlxuXG5cblxuXG5cbmNsYXNzIFVJUmVjdGFuZ2xlIGV4dGVuZHMgVUlPYmplY3Qge1xuICAgIF9pc0JlaW5nVXBkYXRlZDogYm9vbGVhblxuICAgIHJlY3RhbmdsZVBvaW50RGlkQ2hhbmdlOiAoYjogYW55KSA9PiB2b2lkXG4gICAgbWF4OiBVSVBvaW50XG4gICAgbWluOiBVSVBvaW50XG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IoeDogbnVtYmVyID0gMCwgeTogbnVtYmVyID0gMCwgaGVpZ2h0OiBudW1iZXIgPSAwLCB3aWR0aDogbnVtYmVyID0gMCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBVSVJlY3RhbmdsZVxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSU9iamVjdFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMubWluID0gbmV3IFVJUG9pbnQoTnVtYmVyLlBPU0lUSVZFX0lORklOSVRZLCBOdW1iZXIuUE9TSVRJVkVfSU5GSU5JVFkpXG4gICAgICAgIHRoaXMubWF4ID0gbmV3IFVJUG9pbnQoTnVtYmVyLk5FR0FUSVZFX0lORklOSVRZLCBOdW1iZXIuTkVHQVRJVkVfSU5GSU5JVFkpXG4gICAgICAgIFxuICAgICAgICB0aGlzLm1pbi5kaWRDaGFuZ2UgPSB0aGlzLnJlY3RhbmdsZVBvaW50RGlkQ2hhbmdlXG4gICAgICAgIHRoaXMubWF4LmRpZENoYW5nZSA9IHRoaXMucmVjdGFuZ2xlUG9pbnREaWRDaGFuZ2VcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzQmVpbmdVcGRhdGVkID0gTk9cbiAgICAgICAgXG4gICAgICAgIHRoaXMubWluID0gbmV3IFVJUG9pbnQoeCwgeSlcbiAgICAgICAgdGhpcy5tYXggPSBuZXcgVUlQb2ludCh4ICsgd2lkdGgsIHkgKyBoZWlnaHQpXG4gICAgICAgIFxuICAgICAgICBpZiAoSVNfTklMKGhlaWdodCkpIHtcbiAgICAgICAgICAgIHRoaXMubWF4LnkgPSBoZWlnaHRcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKElTX05JTCh3aWR0aCkpIHtcbiAgICAgICAgICAgIHRoaXMubWF4LnggPSB3aWR0aFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgY29weSgpIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IFVJUmVjdGFuZ2xlKHRoaXMueCwgdGhpcy55LCB0aGlzLmhlaWdodCwgdGhpcy53aWR0aClcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBpc0VxdWFsVG8ocmVjdGFuZ2xlOiBVSVJlY3RhbmdsZSkge1xuICAgIFxuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSAoSVMocmVjdGFuZ2xlKSAmJiB0aGlzLm1pbi5pc0VxdWFsVG8ocmVjdGFuZ2xlLm1pbikgJiYgdGhpcy5tYXguaXNFcXVhbFRvKHJlY3RhbmdsZS5tYXgpKVxuICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgemVybygpIHtcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IFVJUmVjdGFuZ2xlKDAsIDAsIDAsIDApXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGNvbnRhaW5zUG9pbnQocG9pbnQ6IFVJUG9pbnQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubWluLnggPD0gcG9pbnQueCAmJiB0aGlzLm1pbi55IDw9IHBvaW50LnkgJiZcbiAgICAgICAgICAgIHBvaW50LnggPD0gdGhpcy5tYXgueCAmJiBwb2ludC55IDw9IHRoaXMubWF4LnlcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQnlBZGRpbmdQb2ludChwb2ludDogVUlQb2ludCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKCFwb2ludCkge1xuICAgICAgICAgICAgcG9pbnQgPSBuZXcgVUlQb2ludCgwLCAwKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLmJlZ2luVXBkYXRlcygpXG4gICAgXG4gICAgICAgIGNvbnN0IG1pbiA9IHRoaXMubWluLmNvcHkoKVxuICAgICAgICBpZiAobWluLnggPT09IG5pbCkge1xuICAgICAgICAgICAgbWluLnggPSB0aGlzLm1heC54XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1pbi55ID09PSBuaWwpIHtcbiAgICAgICAgICAgIG1pbi55ID0gdGhpcy5tYXgueVxuICAgICAgICB9XG4gICAgXG4gICAgICAgIGNvbnN0IG1heCA9IHRoaXMubWF4LmNvcHkoKVxuICAgICAgICBpZiAobWF4LnggPT09IG5pbCkge1xuICAgICAgICAgICAgbWF4LnggPSB0aGlzLm1pbi54XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1heC55ID09PSBuaWwpIHtcbiAgICAgICAgICAgIG1heC55ID0gdGhpcy5taW4ueVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLm1pbi54ID0gTWF0aC5taW4obWluLngsIHBvaW50LngpXG4gICAgICAgIHRoaXMubWluLnkgPSBNYXRoLm1pbihtaW4ueSwgcG9pbnQueSlcbiAgICAgICAgdGhpcy5tYXgueCA9IE1hdGgubWF4KG1heC54LCBwb2ludC54KVxuICAgICAgICB0aGlzLm1heC55ID0gTWF0aC5tYXgobWF4LnksIHBvaW50LnkpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmZpbmlzaFVwZGF0ZXMoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGhlaWdodCgpIHtcbiAgICAgICAgaWYgKHRoaXMubWF4LnkgPT09IG5pbCkge1xuICAgICAgICAgICAgcmV0dXJuIG5pbFxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLm1heC55IC0gdGhpcy5taW4ueVxuICAgIH1cbiAgICBcbiAgICBzZXQgaGVpZ2h0KGhlaWdodDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMubWF4LnkgPSB0aGlzLm1pbi55ICsgaGVpZ2h0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCB3aWR0aCgpIHtcbiAgICAgICAgaWYgKHRoaXMubWF4LnggPT09IG5pbCkge1xuICAgICAgICAgICAgcmV0dXJuIG5pbFxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLm1heC54IC0gdGhpcy5taW4ueFxuICAgIH1cbiAgICBcbiAgICBzZXQgd2lkdGgod2lkdGg6IG51bWJlcikge1xuICAgICAgICB0aGlzLm1heC54ID0gdGhpcy5taW4ueCArIHdpZHRoXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCB4KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5taW4ueFxuICAgIH1cbiAgICBcbiAgICBzZXQgeCh4OiBudW1iZXIpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYmVnaW5VcGRhdGVzKClcbiAgICBcbiAgICAgICAgY29uc3Qgd2lkdGggPSB0aGlzLndpZHRoXG4gICAgICAgIHRoaXMubWluLnggPSB4XG4gICAgICAgIHRoaXMubWF4LnggPSB0aGlzLm1pbi54ICsgd2lkdGhcbiAgICAgICAgXG4gICAgICAgIHRoaXMuZmluaXNoVXBkYXRlcygpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgeSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubWluLnlcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0IHkoeTogbnVtYmVyKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLmJlZ2luVXBkYXRlcygpXG4gICAgXG4gICAgICAgIGNvbnN0IGhlaWdodCA9IHRoaXMuaGVpZ2h0XG4gICAgICAgIHRoaXMubWluLnkgPSB5XG4gICAgICAgIHRoaXMubWF4LnkgPSB0aGlzLm1pbi55ICsgaGVpZ2h0XG4gICAgICAgIFxuICAgICAgICB0aGlzLmZpbmlzaFVwZGF0ZXMoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IHRvcExlZnQoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5taW4uY29weSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgdG9wUmlnaHQoKSB7XG4gICAgICAgIHJldHVybiBuZXcgVUlQb2ludCh0aGlzLm1heC54LCB0aGlzLnkpXG4gICAgfVxuICAgIFxuICAgIGdldCBib3R0b21MZWZ0KCkge1xuICAgICAgICByZXR1cm4gbmV3IFVJUG9pbnQodGhpcy54LCB0aGlzLm1heC55KVxuICAgIH1cbiAgICBcbiAgICBnZXQgYm90dG9tUmlnaHQoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5tYXguY29weSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgY2VudGVyKCkge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLm1pbi5jb3B5KCkuYWRkKHRoaXMubWluLnRvKHRoaXMubWF4KS5zY2FsZSgwLjUpKVxuICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXQgY2VudGVyKGNlbnRlcjogVUlQb2ludCkge1xuICAgIFxuICAgICAgICBjb25zdCBvZmZzZXQgPSB0aGlzLmNlbnRlci50byhjZW50ZXIpXG4gICAgICAgIHRoaXMub2Zmc2V0QnlQb2ludChvZmZzZXQpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBvZmZzZXRCeVBvaW50KG9mZnNldDogVUlQb2ludCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5taW4uYWRkKG9mZnNldClcbiAgICAgICAgdGhpcy5tYXguYWRkKG9mZnNldClcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBjb25jYXRlbmF0ZVdpdGhSZWN0YW5nbGUocmVjdGFuZ2xlOiBVSVJlY3RhbmdsZSkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy51cGRhdGVCeUFkZGluZ1BvaW50KHJlY3RhbmdsZS5ib3R0b21SaWdodClcbiAgICAgICAgdGhpcy51cGRhdGVCeUFkZGluZ1BvaW50KHJlY3RhbmdsZS50b3BMZWZ0KVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXNcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGludGVyc2VjdGlvblJlY3RhbmdsZVdpdGhSZWN0YW5nbGUocmVjdGFuZ2xlOiBVSVJlY3RhbmdsZSk6IFVJUmVjdGFuZ2xlIHtcbiAgICBcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5jb3B5KClcbiAgICBcbiAgICAgICAgcmVzdWx0LmJlZ2luVXBkYXRlcygpXG4gICAgXG4gICAgICAgIGNvbnN0IG1pbiA9IHJlc3VsdC5taW5cbiAgICAgICAgaWYgKG1pbi54ID09PSBuaWwpIHtcbiAgICAgICAgICAgIG1pbi54ID0gcmVjdGFuZ2xlLm1heC54IC0gTWF0aC5taW4ocmVzdWx0LndpZHRoLCByZWN0YW5nbGUud2lkdGgpXG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1pbi55ID09PSBuaWwpIHtcbiAgICAgICAgICAgIG1pbi55ID0gcmVjdGFuZ2xlLm1heC55IC0gTWF0aC5taW4ocmVzdWx0LmhlaWdodCwgcmVjdGFuZ2xlLmhlaWdodClcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBjb25zdCBtYXggPSByZXN1bHQubWF4XG4gICAgICAgIGlmIChtYXgueCA9PT0gbmlsKSB7XG4gICAgICAgICAgICBtYXgueCA9IHJlY3RhbmdsZS5taW4ueCArIE1hdGgubWluKHJlc3VsdC53aWR0aCwgcmVjdGFuZ2xlLndpZHRoKVxuICAgICAgICB9XG4gICAgICAgIGlmIChtYXgueSA9PT0gbmlsKSB7XG4gICAgICAgICAgICBtYXgueSA9IHJlY3RhbmdsZS5taW4ueSArIE1hdGgubWluKHJlc3VsdC5oZWlnaHQsIHJlY3RhbmdsZS5oZWlnaHQpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHJlc3VsdC5taW4ueCA9IE1hdGgubWF4KHJlc3VsdC5taW4ueCwgcmVjdGFuZ2xlLm1pbi54KVxuICAgICAgICByZXN1bHQubWluLnkgPSBNYXRoLm1heChyZXN1bHQubWluLnksIHJlY3RhbmdsZS5taW4ueSlcbiAgICAgICAgcmVzdWx0Lm1heC54ID0gTWF0aC5taW4ocmVzdWx0Lm1heC54LCByZWN0YW5nbGUubWF4LngpXG4gICAgICAgIHJlc3VsdC5tYXgueSA9IE1hdGgubWluKHJlc3VsdC5tYXgueSwgcmVjdGFuZ2xlLm1heC55KVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGlmIChyZXN1bHQuaGVpZ2h0IDwgMCkge1xuICAgIFxuICAgICAgICAgICAgY29uc3QgYXZlcmFnZVkgPSAodGhpcy5jZW50ZXIueSArIHJlY3RhbmdsZS5jZW50ZXIueSkgKiAwLjVcbiAgICAgICAgICAgIHJlc3VsdC5taW4ueSA9IGF2ZXJhZ2VZXG4gICAgICAgICAgICByZXN1bHQubWF4LnkgPSBhdmVyYWdlWVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChyZXN1bHQud2lkdGggPCAwKSB7XG4gICAgXG4gICAgICAgICAgICBjb25zdCBhdmVyYWdlWCA9ICh0aGlzLmNlbnRlci54ICsgcmVjdGFuZ2xlLmNlbnRlci54KSAqIDAuNVxuICAgICAgICAgICAgcmVzdWx0Lm1pbi54ID0gYXZlcmFnZVhcbiAgICAgICAgICAgIHJlc3VsdC5tYXgueCA9IGF2ZXJhZ2VYXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmVzdWx0LmZpbmlzaFVwZGF0ZXMoKVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGFyZWEoKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuaGVpZ2h0ICogdGhpcy53aWR0aFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIGludGVyc2VjdHNXaXRoUmVjdGFuZ2xlKHJlY3RhbmdsZTogVUlSZWN0YW5nbGUpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiAodGhpcy5pbnRlcnNlY3Rpb25SZWN0YW5nbGVXaXRoUmVjdGFuZ2xlKHJlY3RhbmdsZSkuYXJlYSAhPSAwKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgLy8gYWRkIHNvbWUgc3BhY2UgYXJvdW5kIHRoZSByZWN0YW5nbGVcbiAgICByZWN0YW5nbGVXaXRoSW5zZXRzKGxlZnQ6IG51bWJlciwgcmlnaHQ6IG51bWJlciwgYm90dG9tOiBudW1iZXIsIHRvcDogbnVtYmVyKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuY29weSgpXG4gICAgICAgIHJlc3VsdC5taW4ueCA9IHRoaXMubWluLnggKyBsZWZ0XG4gICAgICAgIHJlc3VsdC5tYXgueCA9IHRoaXMubWF4LnggLSByaWdodFxuICAgICAgICByZXN1bHQubWluLnkgPSB0aGlzLm1pbi55ICsgdG9wXG4gICAgICAgIHJlc3VsdC5tYXgueSA9IHRoaXMubWF4LnkgLSBib3R0b21cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICByZWN0YW5nbGVXaXRoSW5zZXQoaW5zZXQ6IG51bWJlcikge1xuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLnJlY3RhbmdsZVdpdGhJbnNldHMoaW5zZXQsIGluc2V0LCBpbnNldCwgaW5zZXQpXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgcmVjdGFuZ2xlV2l0aEhlaWdodChoZWlnaHQ6IG51bWJlciwgY2VudGVyZWRPblBvc2l0aW9uOiBudW1iZXIgPSBuaWwpIHtcbiAgICAgICAgXG4gICAgICAgIGlmIChpc05hTihjZW50ZXJlZE9uUG9zaXRpb24pKSB7XG4gICAgICAgICAgICBjZW50ZXJlZE9uUG9zaXRpb24gPSBuaWxcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICByZXN1bHQuaGVpZ2h0ID0gaGVpZ2h0XG4gICAgICAgIFxuICAgICAgICBpZiAoY2VudGVyZWRPblBvc2l0aW9uICE9IG5pbCkge1xuICAgICAgICAgICAgY29uc3QgY2hhbmdlID0gaGVpZ2h0IC0gdGhpcy5oZWlnaHRcbiAgICAgICAgICAgIHJlc3VsdC5vZmZzZXRCeVBvaW50KG5ldyBVSVBvaW50KDAsIGNoYW5nZSAqIGNlbnRlcmVkT25Qb3NpdGlvbikuc2NhbGUoLTEpKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICByZWN0YW5nbGVXaXRoV2lkdGgod2lkdGg6IG51bWJlciwgY2VudGVyZWRPblBvc2l0aW9uOiBudW1iZXIgPSBuaWwpIHtcbiAgICAgICAgXG4gICAgICAgIGlmIChpc05hTihjZW50ZXJlZE9uUG9zaXRpb24pKSB7XG4gICAgICAgICAgICBjZW50ZXJlZE9uUG9zaXRpb24gPSBuaWxcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICByZXN1bHQud2lkdGggPSB3aWR0aFxuICAgICAgICBcbiAgICAgICAgaWYgKGNlbnRlcmVkT25Qb3NpdGlvbiAhPSBuaWwpIHtcbiAgICAgICAgICAgIGNvbnN0IGNoYW5nZSA9IHdpZHRoIC0gdGhpcy53aWR0aFxuICAgICAgICAgICAgcmVzdWx0Lm9mZnNldEJ5UG9pbnQobmV3IFVJUG9pbnQoY2hhbmdlICogY2VudGVyZWRPblBvc2l0aW9uLCAwKS5zY2FsZSgtMSkpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHJlY3RhbmdsZVdpdGhIZWlnaHRSZWxhdGl2ZVRvV2lkdGgoaGVpZ2h0UmF0aW86IG51bWJlciA9IDEsIGNlbnRlcmVkT25Qb3NpdGlvbjogbnVtYmVyID0gbmlsKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMucmVjdGFuZ2xlV2l0aEhlaWdodCh0aGlzLndpZHRoICogaGVpZ2h0UmF0aW8sIGNlbnRlcmVkT25Qb3NpdGlvbilcbiAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcmVjdGFuZ2xlV2l0aFdpZHRoUmVsYXRpdmVUb0hlaWdodCh3aWR0aFJhdGlvOiBudW1iZXIgPSAxLCBjZW50ZXJlZE9uUG9zaXRpb246IG51bWJlciA9IG5pbCkge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLnJlY3RhbmdsZVdpdGhXaWR0aCh0aGlzLmhlaWdodCAqIHdpZHRoUmF0aW8sIGNlbnRlcmVkT25Qb3NpdGlvbilcbiAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcmVjdGFuZ2xlV2l0aFgoeDogbnVtYmVyLCBjZW50ZXJlZE9uUG9zaXRpb246IG51bWJlciA9IDApIHtcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5jb3B5KClcbiAgICAgICAgcmVzdWx0LnggPSB4IC0gcmVzdWx0LndpZHRoICogY2VudGVyZWRPblBvc2l0aW9uXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICByZWN0YW5nbGVXaXRoWSh5OiBudW1iZXIsIGNlbnRlcmVkT25Qb3NpdGlvbjogbnVtYmVyID0gMCkge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICByZXN1bHQueSA9IHkgLSByZXN1bHQuaGVpZ2h0ICogY2VudGVyZWRPblBvc2l0aW9uXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICByZWN0YW5nbGVCeUFkZGluZ1goeDogbnVtYmVyKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuY29weSgpXG4gICAgICAgIHJlc3VsdC54ID0gdGhpcy54ICsgeFxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcmVjdGFuZ2xlQnlBZGRpbmdZKHk6IG51bWJlcikge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICByZXN1bHQueSA9IHRoaXMueSArIHlcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHJlY3RhbmdsZXNCeVNwbGl0dGluZ1dpZHRoKFxuICAgICAgICB3ZWlnaHRzOiBudW1iZXJbXSxcbiAgICAgICAgcGFkZGluZ3M6IG51bWJlciB8IG51bWJlcltdID0gMCxcbiAgICAgICAgYWJzb2x1dGVXaWR0aHM6IG51bWJlciB8IG51bWJlcltdID0gbmlsXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoSVNfTklMKHBhZGRpbmdzKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBwYWRkaW5ncyA9IDFcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAoIShwYWRkaW5ncyBpbnN0YW5jZW9mIEFycmF5KSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBwYWRkaW5ncyA9IFtwYWRkaW5nc10uYXJyYXlCeVJlcGVhdGluZyh3ZWlnaHRzLmxlbmd0aCAtIDEpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcGFkZGluZ3MgPSBwYWRkaW5ncy5hcnJheUJ5VHJpbW1pbmdUb0xlbmd0aElmTG9uZ2VyKHdlaWdodHMubGVuZ3RoIC0gMSlcbiAgICAgICAgXG4gICAgICAgIGlmICghKGFic29sdXRlV2lkdGhzIGluc3RhbmNlb2YgQXJyYXkpICYmIElTX05PVF9OSUwoYWJzb2x1dGVXaWR0aHMpKSB7XG4gICAgICAgICAgICBhYnNvbHV0ZVdpZHRocyA9IFthYnNvbHV0ZVdpZHRoc10uYXJyYXlCeVJlcGVhdGluZyh3ZWlnaHRzLmxlbmd0aClcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQ6IFVJUmVjdGFuZ2xlW10gPSBbXVxuICAgICAgICBjb25zdCBzdW1PZldlaWdodHMgPSB3ZWlnaHRzLnJlZHVjZShmdW5jdGlvbiAoYSwgYiwgaW5kZXgpIHtcbiAgICAgICAgICAgIGlmIChJU19OT1RfTklMKGFic29sdXRlV2lkdGhzW2luZGV4XSkpIHtcbiAgICAgICAgICAgICAgICBiID0gMFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGEgKyBiXG4gICAgICAgIH0sIDApXG4gICAgICAgIGNvbnN0IHN1bU9mUGFkZGluZ3MgPSBwYWRkaW5ncy5zdW1tZWRWYWx1ZVxuICAgICAgICBjb25zdCBzdW1PZkFic29sdXRlV2lkdGhzID0gKGFic29sdXRlV2lkdGhzIGFzIG51bWJlcltdKS5zdW1tZWRWYWx1ZVxuICAgICAgICBjb25zdCB0b3RhbFJlbGF0aXZlV2lkdGggPSB0aGlzLndpZHRoIC0gc3VtT2ZQYWRkaW5ncyAtIHN1bU9mQWJzb2x1dGVXaWR0aHNcbiAgICAgICAgdmFyIHByZXZpb3VzQ2VsbE1heFggPSB0aGlzLnhcbiAgICBcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB3ZWlnaHRzLmxlbmd0aDsgaSsrKSB7XG4gICAgXG4gICAgICAgICAgICB2YXIgcmVzdWx0V2lkdGg6IG51bWJlclxuICAgICAgICAgICAgaWYgKElTX05PVF9OSUwoYWJzb2x1dGVXaWR0aHNbaV0pKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgcmVzdWx0V2lkdGggPSBhYnNvbHV0ZVdpZHRoc1tpXSB8fCAwXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICByZXN1bHRXaWR0aCA9IHRvdGFsUmVsYXRpdmVXaWR0aCAqICh3ZWlnaHRzW2ldIC8gc3VtT2ZXZWlnaHRzKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgIFxuICAgICAgICAgICAgY29uc3QgcmVjdGFuZ2xlID0gdGhpcy5yZWN0YW5nbGVXaXRoV2lkdGgocmVzdWx0V2lkdGgpXG4gICAgXG4gICAgICAgICAgICB2YXIgcGFkZGluZyA9IDBcbiAgICAgICAgICAgIGlmIChwYWRkaW5ncy5sZW5ndGggPiBpICYmIHBhZGRpbmdzW2ldKSB7XG4gICAgICAgICAgICAgICAgcGFkZGluZyA9IHBhZGRpbmdzW2ldXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJlY3RhbmdsZS54ID0gcHJldmlvdXNDZWxsTWF4WFxuICAgICAgICAgICAgcHJldmlvdXNDZWxsTWF4WCA9IHJlY3RhbmdsZS5tYXgueCArIHBhZGRpbmdcbiAgICAgICAgICAgIC8vcmVjdGFuZ2xlID0gcmVjdGFuZ2xlLnJlY3RhbmdsZVdpdGhJbnNldHMoMCwgcGFkZGluZywgMCwgMCk7XG4gICAgICAgICAgICByZXN1bHQucHVzaChyZWN0YW5nbGUpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcmVjdGFuZ2xlc0J5U3BsaXR0aW5nSGVpZ2h0KFxuICAgICAgICB3ZWlnaHRzOiBudW1iZXJbXSxcbiAgICAgICAgcGFkZGluZ3M6IG51bWJlciB8IG51bWJlcltdID0gMCxcbiAgICAgICAgYWJzb2x1dGVIZWlnaHRzOiBudW1iZXIgfCBudW1iZXJbXSA9IG5pbFxuICAgICkge1xuICAgICAgICBcbiAgICAgICAgaWYgKElTX05JTChwYWRkaW5ncykpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGFkZGluZ3MgPSAxXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKCEocGFkZGluZ3MgaW5zdGFuY2VvZiBBcnJheSkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGFkZGluZ3MgPSBbcGFkZGluZ3NdLmFycmF5QnlSZXBlYXRpbmcod2VpZ2h0cy5sZW5ndGggLSAxKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHBhZGRpbmdzID0gcGFkZGluZ3MuYXJyYXlCeVRyaW1taW5nVG9MZW5ndGhJZkxvbmdlcih3ZWlnaHRzLmxlbmd0aCAtIDEpXG4gICAgICAgIFxuICAgICAgICBpZiAoIShhYnNvbHV0ZUhlaWdodHMgaW5zdGFuY2VvZiBBcnJheSkgJiYgSVNfTk9UX05JTChhYnNvbHV0ZUhlaWdodHMpKSB7XG4gICAgICAgICAgICBhYnNvbHV0ZUhlaWdodHMgPSBbYWJzb2x1dGVIZWlnaHRzXS5hcnJheUJ5UmVwZWF0aW5nKHdlaWdodHMubGVuZ3RoKVxuICAgICAgICB9XG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdDogVUlSZWN0YW5nbGVbXSA9IFtdXG4gICAgICAgIGNvbnN0IHN1bU9mV2VpZ2h0cyA9IHdlaWdodHMucmVkdWNlKGZ1bmN0aW9uIChhLCBiLCBpbmRleCkge1xuICAgICAgICAgICAgaWYgKElTX05PVF9OSUwoYWJzb2x1dGVIZWlnaHRzW2luZGV4XSkpIHtcbiAgICAgICAgICAgICAgICBiID0gMFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGEgKyBiXG4gICAgICAgIH0sIDApXG4gICAgICAgIGNvbnN0IHN1bU9mUGFkZGluZ3MgPSBwYWRkaW5ncy5zdW1tZWRWYWx1ZVxuICAgICAgICBjb25zdCBzdW1PZkFic29sdXRlSGVpZ2h0cyA9IChhYnNvbHV0ZUhlaWdodHMgYXMgbnVtYmVyW10pLnN1bW1lZFZhbHVlXG4gICAgICAgIGNvbnN0IHRvdGFsUmVsYXRpdmVIZWlnaHQgPSB0aGlzLmhlaWdodCAtIHN1bU9mUGFkZGluZ3MgLSBzdW1PZkFic29sdXRlSGVpZ2h0c1xuICAgICAgICB2YXIgcHJldmlvdXNDZWxsTWF4WSA9IHRoaXMueVxuICAgIFxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHdlaWdodHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHZhciByZXN1bHRIZWlnaHQ6IG51bWJlclxuICAgICAgICAgICAgaWYgKElTX05PVF9OSUwoYWJzb2x1dGVIZWlnaHRzW2ldKSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHJlc3VsdEhlaWdodCA9IGFic29sdXRlSGVpZ2h0c1tpXSB8fCAwXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICByZXN1bHRIZWlnaHQgPSB0b3RhbFJlbGF0aXZlSGVpZ2h0ICogKHdlaWdodHNbaV0gLyBzdW1PZldlaWdodHMpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgXG4gICAgICAgICAgICBjb25zdCByZWN0YW5nbGUgPSB0aGlzLnJlY3RhbmdsZVdpdGhIZWlnaHQocmVzdWx0SGVpZ2h0KVxuICAgIFxuICAgICAgICAgICAgdmFyIHBhZGRpbmcgPSAwXG4gICAgICAgICAgICBpZiAocGFkZGluZ3MubGVuZ3RoID4gaSAmJiBwYWRkaW5nc1tpXSkge1xuICAgICAgICAgICAgICAgIHBhZGRpbmcgPSBwYWRkaW5nc1tpXVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICByZWN0YW5nbGUueSA9IHByZXZpb3VzQ2VsbE1heFlcbiAgICAgICAgICAgIHByZXZpb3VzQ2VsbE1heFkgPSByZWN0YW5nbGUubWF4LnkgKyBwYWRkaW5nXG4gICAgICAgICAgICAvL3JlY3RhbmdsZSA9IHJlY3RhbmdsZS5yZWN0YW5nbGVXaXRoSW5zZXRzKDAsIDAsIHBhZGRpbmcsIDApO1xuICAgICAgICAgICAgcmVzdWx0LnB1c2gocmVjdGFuZ2xlKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICByZWN0YW5nbGVzQnlFcXVhbGx5U3BsaXR0aW5nV2lkdGgobnVtYmVyT2ZGcmFtZXM6IG51bWJlciwgcGFkZGluZzogbnVtYmVyID0gMCkge1xuICAgICAgICBjb25zdCByZXN1bHQ6IFVJUmVjdGFuZ2xlW10gPSBbXVxuICAgICAgICBjb25zdCB0b3RhbFBhZGRpbmcgPSBwYWRkaW5nICogKG51bWJlck9mRnJhbWVzIC0gMSlcbiAgICAgICAgY29uc3QgcmVzdWx0V2lkdGggPSAodGhpcy53aWR0aCAtIHRvdGFsUGFkZGluZykgLyBudW1iZXJPZkZyYW1lc1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG51bWJlck9mRnJhbWVzOyBpKyspIHtcbiAgICAgICAgICAgIGNvbnN0IHJlY3RhbmdsZSA9IHRoaXMucmVjdGFuZ2xlV2l0aFdpZHRoKHJlc3VsdFdpZHRoLCBpIC8gKG51bWJlck9mRnJhbWVzIC0gMSkpXG4gICAgICAgICAgICByZXN1bHQucHVzaChyZWN0YW5nbGUpXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICByZWN0YW5nbGVzQnlFcXVhbGx5U3BsaXR0aW5nSGVpZ2h0KG51bWJlck9mRnJhbWVzOiBudW1iZXIsIHBhZGRpbmc6IG51bWJlciA9IDApIHtcbiAgICAgICAgY29uc3QgcmVzdWx0OiBVSVJlY3RhbmdsZVtdID0gW11cbiAgICAgICAgY29uc3QgdG90YWxQYWRkaW5nID0gcGFkZGluZyAqIChudW1iZXJPZkZyYW1lcyAtIDEpXG4gICAgICAgIGNvbnN0IHJlc3VsdEhlaWdodCA9ICh0aGlzLmhlaWdodCAtIHRvdGFsUGFkZGluZykgLyBudW1iZXJPZkZyYW1lc1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG51bWJlck9mRnJhbWVzOyBpKyspIHtcbiAgICAgICAgICAgIGNvbnN0IHJlY3RhbmdsZSA9IHRoaXMucmVjdGFuZ2xlV2l0aEhlaWdodChyZXN1bHRIZWlnaHQsIGkgLyAobnVtYmVyT2ZGcmFtZXMgLSAxKSlcbiAgICAgICAgICAgIHJlc3VsdC5wdXNoKHJlY3RhbmdsZSlcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGRpc3RyaWJ1dGVWaWV3c0Fsb25nV2lkdGgoXG4gICAgICAgIHZpZXdzOiBVSVZpZXdbXSxcbiAgICAgICAgd2VpZ2h0czogbnVtYmVyIHwgbnVtYmVyW10gPSAxLFxuICAgICAgICBwYWRkaW5ncz86IG51bWJlciB8IG51bWJlcltdLFxuICAgICAgICBhYnNvbHV0ZVdpZHRocz86IG51bWJlciB8IG51bWJlcltdXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoISh3ZWlnaHRzIGluc3RhbmNlb2YgQXJyYXkpKSB7XG4gICAgICAgICAgICB3ZWlnaHRzID0gW3dlaWdodHNdLmFycmF5QnlSZXBlYXRpbmcodmlld3MubGVuZ3RoKVxuICAgICAgICB9XG4gICAgXG4gICAgICAgIGNvbnN0IGZyYW1lcyA9IHRoaXMucmVjdGFuZ2xlc0J5U3BsaXR0aW5nV2lkdGgod2VpZ2h0cywgcGFkZGluZ3MsIGFic29sdXRlV2lkdGhzKVxuICAgIFxuICAgICAgICBmcmFtZXMuZm9yRWFjaChmdW5jdGlvbiAoZnJhbWUsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgdmlld3NbaW5kZXhdLmZyYW1lID0gZnJhbWVcbiAgICAgICAgfSlcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBkaXN0cmlidXRlVmlld3NBbG9uZ0hlaWdodChcbiAgICAgICAgdmlld3M6IFVJVmlld1tdLFxuICAgICAgICB3ZWlnaHRzOiBudW1iZXIgfCBudW1iZXJbXSA9IDEsXG4gICAgICAgIHBhZGRpbmdzPzogbnVtYmVyIHwgbnVtYmVyW10sXG4gICAgICAgIGFic29sdXRlSGVpZ2h0cz86IG51bWJlciB8IG51bWJlcltdXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoISh3ZWlnaHRzIGluc3RhbmNlb2YgQXJyYXkpKSB7XG4gICAgICAgICAgICB3ZWlnaHRzID0gW3dlaWdodHNdLmFycmF5QnlSZXBlYXRpbmcodmlld3MubGVuZ3RoKVxuICAgICAgICB9XG4gICAgXG4gICAgICAgIGNvbnN0IGZyYW1lcyA9IHRoaXMucmVjdGFuZ2xlc0J5U3BsaXR0aW5nSGVpZ2h0KHdlaWdodHMsIHBhZGRpbmdzLCBhYnNvbHV0ZUhlaWdodHMpXG4gICAgXG4gICAgICAgIGZyYW1lcy5mb3JFYWNoKGZ1bmN0aW9uIChmcmFtZSwgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgICAgICB2aWV3c1tpbmRleF0uZnJhbWUgPSBmcmFtZVxuICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXNcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGRpc3RyaWJ1dGVWaWV3c0VxdWFsbHlBbG9uZ1dpZHRoKHZpZXdzOiBVSVZpZXdbXSwgcGFkZGluZzogbnVtYmVyKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IGZyYW1lcyA9IHRoaXMucmVjdGFuZ2xlc0J5RXF1YWxseVNwbGl0dGluZ1dpZHRoKHZpZXdzLmxlbmd0aCwgcGFkZGluZylcbiAgICBcbiAgICAgICAgZnJhbWVzLmZvckVhY2goZnVuY3Rpb24gKGZyYW1lLCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIHZpZXdzW2luZGV4XS5mcmFtZSA9IGZyYW1lXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpc1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZGlzdHJpYnV0ZVZpZXdzRXF1YWxseUFsb25nSGVpZ2h0KHZpZXdzOiBVSVZpZXdbXSwgcGFkZGluZzogbnVtYmVyKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IGZyYW1lcyA9IHRoaXMucmVjdGFuZ2xlc0J5RXF1YWxseVNwbGl0dGluZ0hlaWdodCh2aWV3cy5sZW5ndGgsIHBhZGRpbmcpXG4gICAgXG4gICAgICAgIGZyYW1lcy5mb3JFYWNoKGZ1bmN0aW9uIChmcmFtZSwgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgICAgICB2aWV3c1tpbmRleF0uZnJhbWUgPSBmcmFtZVxuICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXNcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHJlY3RhbmdsZUZvck5leHRSb3cocGFkZGluZzogbnVtYmVyID0gMCwgaGVpZ2h0ID0gdGhpcy5oZWlnaHQpIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5yZWN0YW5nbGVXaXRoWSh0aGlzLm1heC55ICsgcGFkZGluZylcbiAgICAgICAgaWYgKGhlaWdodCAhPSB0aGlzLmhlaWdodCkge1xuICAgICAgICAgICAgcmVzdWx0LmhlaWdodCA9IGhlaWdodFxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgcmVjdGFuZ2xlRm9yTmV4dENvbHVtbihwYWRkaW5nOiBudW1iZXIgPSAwLCB3aWR0aCA9IHRoaXMud2lkdGgpIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5yZWN0YW5nbGVXaXRoWCh0aGlzLm1heC54ICsgcGFkZGluZylcbiAgICAgICAgaWYgKHdpZHRoICE9IHRoaXMud2lkdGgpIHtcbiAgICAgICAgICAgIHJlc3VsdC53aWR0aCA9IHdpZHRoXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICByZWN0YW5nbGVGb3JQcmV2aW91c1JvdyhwYWRkaW5nOiBudW1iZXIgPSAwKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMucmVjdGFuZ2xlV2l0aFkodGhpcy5taW4ueSAtIHRoaXMuaGVpZ2h0IC0gcGFkZGluZylcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICByZWN0YW5nbGVGb3JQcmV2aW91c0NvbHVtbihwYWRkaW5nOiBudW1iZXIgPSAwKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMucmVjdGFuZ2xlV2l0aFgodGhpcy5taW4ueCAtIHRoaXMud2lkdGggLSBwYWRkaW5nKVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIC8vIEJvdW5kaW5nIGJveFxuICAgIHN0YXRpYyBib3VuZGluZ0JveEZvclBvaW50cyhwb2ludHMpIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IFVJUmVjdGFuZ2xlKClcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwb2ludHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHJlc3VsdC51cGRhdGVCeUFkZGluZ1BvaW50KHBvaW50c1tpXSlcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIGJlZ2luVXBkYXRlcygpIHtcbiAgICAgICAgdGhpcy5faXNCZWluZ1VwZGF0ZWQgPSBZRVNcbiAgICB9XG4gICAgXG4gICAgZmluaXNoVXBkYXRlcygpIHtcbiAgICAgICAgdGhpcy5faXNCZWluZ1VwZGF0ZWQgPSBOT1xuICAgICAgICB0aGlzLmRpZENoYW5nZSgpXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGRpZENoYW5nZSgpIHtcbiAgICAgICAgXG4gICAgICAgIC8vIENhbGxiYWNrIHRvIGJlIHNldCBieSBkZWxlZ2F0ZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgX3JlY3RhbmdsZVBvaW50RGlkQ2hhbmdlKCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKCF0aGlzLl9pc0JlaW5nVXBkYXRlZCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmRpZENoYW5nZSgpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi4vYXV0b2xheW91dC9zcmMvQXV0b0xheW91dC5qc1wiIC8+XG4vLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9VSU9iamVjdC50c1wiIC8+XG4vLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9VSUNvbG9yLnRzXCIgLz5cbi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuL1VJUmVjdGFuZ2xlLnRzXCIgLz5cblxuLy9pbXBvcnQgQXR0cmlidXRlIGZyb20gXCIuLi9hdXRvbGF5b3V0L3NyYy9BdHRyaWJ1dGVcIjtcblxuLy9pbXBvcnQgQXV0b0xheW91dCBmcm9tIFwiLi4vYXV0b2xheW91dC9zcmMvQXV0b0xheW91dFwiO1xuXG5cblxuXG5cbmRlY2xhcmUgbW9kdWxlIEF1dG9MYXlvdXQge1xuICAgIFxuICAgIFxuICAgIGNsYXNzIENvbnN0cmFpbnQge1xuICAgICAgICBcbiAgICAgICAgW2tleTogc3RyaW5nXTogYW55XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBjbGFzcyBWaWV3IHtcbiAgICAgICAgXG4gICAgICAgIFtrZXk6IHN0cmluZ106IGFueVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgY2xhc3MgVmlzdWFsRm9ybWF0IHtcbiAgICAgICAgXG4gICAgICAgIHN0YXRpYyBwYXJzZShhcmcwOiBhbnksIGFyZzE6IGFueSk6IGFueTtcbiAgICAgICAgXG4gICAgICAgIFtrZXk6IHN0cmluZ106IGFueVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgZW51bSBBdHRyaWJ1dGUge1xuICAgICAgICBcbiAgICAgICAgTEVGVCwgUklHSFQsIEJPVFRPTSwgVE9QLCBDRU5URVJYLCBDRU5URVJZLCBXSURUSCwgSEVJR0hULCBaSU5ERVgsIFZBUklBQkxFLCBOT1RBTkFUVFJJQlVURVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgZW51bSBSZWxhdGlvbiB7XG4gICAgICAgIFxuICAgICAgICBFUVUsIExFUSwgR0VRXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5pbnRlcmZhY2UgTG9vc2VPYmplY3Qge1xuICAgIFtrZXk6IHN0cmluZ106IGFueVxufVxuXG5cbmludGVyZmFjZSBDb250cm9sRXZlbnRUYXJnZXRzT2JqZWN0IHtcbiAgICBcbiAgICBba2V5OiBzdHJpbmddOiBGdW5jdGlvbltdO1xuICAgIFxufVxuXG5cbmludGVyZmFjZSBVSVZpZXdCcm9hZGNhc3RFdmVudCB7XG4gICAgXG4gICAgbmFtZTogc3RyaW5nO1xuICAgIHBhcmFtZXRlcnM6IHtcbiAgICAgICAgW2tleTogc3RyaW5nXTogc3RyaW5nIHwgc3RyaW5nW107XG4gICAgfVxuICAgIFxufVxuXG5cblxudHlwZSBVSVZpZXdBZGRDb250cm9sRXZlbnRUYXJnZXRPYmplY3Q8VCBleHRlbmRzIHR5cGVvZiBVSVZpZXcuY29udHJvbEV2ZW50PiA9IHtcbiAgICBcbiAgICBbSyBpbiBrZXlvZiBUXTogKChzZW5kZXI6IFVJVmlldywgZXZlbnQ6IEV2ZW50KSA9PiB2b2lkKSAmIFBhcnRpYWw8VUlWaWV3QWRkQ29udHJvbEV2ZW50VGFyZ2V0T2JqZWN0PFQ+PlxuICAgIFxufVxuXG5cblxuXG5cbmNsYXNzIFVJVmlldyBleHRlbmRzIFVJT2JqZWN0IHtcbiAgICBcbiAgICBfbmF0aXZlU2VsZWN0aW9uRW5hYmxlZDogYm9vbGVhbiA9IFlFU1xuICAgIF9zaG91bGRMYXlvdXQ6IGJvb2xlYW5cbiAgICBfVUlUYWJsZVZpZXdSb3dJbmRleDogbnVtYmVyXG4gICAgX1VJVGFibGVWaWV3UmV1c2FiaWxpdHlJZGVudGlmaWVyOiBhbnlcbiAgICBfVUlWaWV3SW50cmluc2ljVGVtcG9yYXJ5V2lkdGg6IHN0cmluZ1xuICAgIF9VSVZpZXdJbnRyaW5zaWNUZW1wb3JhcnlIZWlnaHQ6IHN0cmluZ1xuICAgIF9lbmFibGVkOiBib29sZWFuID0gWUVTXG4gICAgX2ZyYW1lOiBhbnlcbiAgICBfYmFja2dyb3VuZENvbG9yOiBVSUNvbG9yID0gVUlDb2xvci50cmFuc3BhcmVudENvbG9yXG4gICAgXG4gICAgX3ZpZXdIVE1MRWxlbWVudDogSFRNTEVsZW1lbnQgJiBMb29zZU9iamVjdFxuICAgIFxuICAgIF9pbm5lckhUTUxLZXk6IHN0cmluZ1xuICAgIF9kZWZhdWx0SW5uZXJIVE1MOiBzdHJpbmdcbiAgICBfcGFyYW1ldGVyczogeyBbeDogc3RyaW5nXTogKHN0cmluZyB8IFVJTG9jYWxpemVkVGV4dE9iamVjdCkgfVxuICAgIFxuICAgIF9sb2NhbGl6ZWRUZXh0T2JqZWN0OiBVSUxvY2FsaXplZFRleHRPYmplY3QgPSBuaWxcbiAgICBcbiAgICBfY29udHJvbEV2ZW50VGFyZ2V0czogQ29udHJvbEV2ZW50VGFyZ2V0c09iamVjdCA9IHt9IC8veyBcIlBvaW50ZXJEb3duXCI6IEZ1bmN0aW9uW107IFwiUG9pbnRlck1vdmVcIjogRnVuY3Rpb25bXTsgXCJQb2ludGVyTGVhdmVcIjogRnVuY3Rpb25bXTsgXCJQb2ludGVyRW50ZXJcIjogRnVuY3Rpb25bXTsgXCJQb2ludGVyVXBJbnNpZGVcIjogRnVuY3Rpb25bXTsgXCJQb2ludGVyVXBcIjogRnVuY3Rpb25bXTsgXCJQb2ludGVySG92ZXJcIjogRnVuY3Rpb25bXTsgfTtcbiAgICBfZnJhbWVUcmFuc2Zvcm06IHN0cmluZ1xuICAgIF92aWV3Q29udHJvbGxlckxheW91dEZ1bmN0aW9uOiAoKSA9PiB2b2lkID0gbmlsXG4gICAgX2RpZExheW91dFN1YnZpZXdzRGVsZWdhdGVGdW5jdGlvbjogKCkgPT4gdm9pZFxuICAgIF9kaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnREZWxlZ2F0ZUZ1bmN0aW9uOiAoZXZlbnQ6IFVJVmlld0Jyb2FkY2FzdEV2ZW50KSA9PiB2b2lkXG4gICAgX3VwZGF0ZUxheW91dEZ1bmN0aW9uOiBhbnlcbiAgICAvLyBAdHMtaWdub3JlXG4gICAgX2NvbnN0cmFpbnRzOiBhbnlbXSAvL0F1dG9MYXlvdXQuQ29uc3RyYWludFtdO1xuICAgIHN1cGVydmlldzogVUlWaWV3XG4gICAgc3Vidmlld3M6IFVJVmlld1tdXG4gICAgX3N0eWxlQ2xhc3NlczogYW55W11cbiAgICBfaXNIaWRkZW46IGJvb2xlYW4gPSBOT1xuICAgIFxuICAgIHBhdXNlc1BvaW50ZXJFdmVudHM6IGJvb2xlYW4gPSBOT1xuICAgIHN0b3BzUG9pbnRlckV2ZW50UHJvcGFnYXRpb246IGJvb2xlYW4gPSBZRVNcbiAgICBfaXNQb2ludGVySW5zaWRlOiBib29sZWFuXG4gICAgX2lzUG9pbnRlclZhbGlkOiBib29sZWFuXG4gICAgX2luaXRpYWxQb2ludGVyUG9zaXRpb246IFVJUG9pbnRcbiAgICBfaGFzUG9pbnRlckRyYWdnZWQ6IGJvb2xlYW5cbiAgICBfcG9pbnRlckRyYWdUaHJlc2hvbGQgPSAyXG4gICAgXG4gICAgaWdub3Jlc1RvdWNoZXM6IGJvb2xlYW4gPSBOT1xuICAgIGlnbm9yZXNNb3VzZTogYm9vbGVhbiA9IE5PXG4gICAgXG4gICAgXG4gICAgc3RhdGljIF9VSVZpZXdJbmRleDogbnVtYmVyID0gLTFcbiAgICBfVUlWaWV3SW5kZXg6IG51bWJlclxuICAgIFxuICAgIHN0YXRpYyBfdmlld3NUb0xheW91dDogVUlWaWV3W10gPSBbXVxuICAgIFxuICAgIGZvcmNlSW50cmluc2ljU2l6ZVplcm86IGJvb2xlYW4gPSBOT1xuICAgIF90b3VjaEV2ZW50VGltZTogbnVtYmVyXG4gICAgXG4gICAgc3RhdGljIF9wYWdlU2NhbGUgPSAxXG4gICAgXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGVsZW1lbnRJRDogc3RyaW5nID0gKFwiVUlWaWV3XCIgK1xuICAgICAgICAgICAgVUlWaWV3Lm5leHRJbmRleCksXG4gICAgICAgIHZpZXdIVE1MRWxlbWVudDogSFRNTEVsZW1lbnQgJiBMb29zZU9iamVjdCA9IG51bGwsXG4gICAgICAgIGVsZW1lbnRUeXBlOiBzdHJpbmcgPSBudWxsLFxuICAgICAgICBpbml0Vmlld0RhdGE/OiBhbnlcbiAgICApIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlWaWV3XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJT2JqZWN0XG4gICAgICAgIFxuICAgICAgICAvLyBJbnN0YW5jZSB2YXJpYWJsZXNcbiAgICAgICAgXG4gICAgICAgIFVJVmlldy5fVUlWaWV3SW5kZXggPSBVSVZpZXcubmV4dEluZGV4XG4gICAgICAgIHRoaXMuX1VJVmlld0luZGV4ID0gVUlWaWV3Ll9VSVZpZXdJbmRleFxuICAgICAgICBcbiAgICAgICAgdGhpcy5fc3R5bGVDbGFzc2VzID0gW11cbiAgICAgICAgLy8gT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsIFwic3R5bGVDbGFzc2VzXCIsIHsgZ2V0OiB0aGlzLnN0eWxlQ2xhc3Nlcywgc2V0OiB0aGlzLnNldFN0eWxlQ2xhc3NlcyB9KTtcbiAgICAgICAgLy8gT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsIFwic3R5bGVDbGFzc05hbWVcIiwgeyBnZXQ6IHRoaXMuc3R5bGVDbGFzc05hbWUgfSk7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9pbml0Vmlld0hUTUxFbGVtZW50KGVsZW1lbnRJRCwgdmlld0hUTUxFbGVtZW50LCBlbGVtZW50VHlwZSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3Vidmlld3MgPSBbXVxuICAgICAgICB0aGlzLnN1cGVydmlldyA9IG5pbFxuICAgICAgICBcbiAgICAgICAgLy8gT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsIFwiZWxlbWVudElEXCIsIHsgZ2V0OiB0aGlzLmVsZW1lbnRJRCB9KTtcbiAgICAgICAgXG4gICAgICAgIC8vIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCBcImNvbnN0cmFpbnRzXCIsIHsgZ2V0OiB0aGlzLmNvbnN0cmFpbnRzLCBzZXQ6IHRoaXMuc2V0Q29uc3RyYWludHMgfSk7XG4gICAgICAgIHRoaXMuX2NvbnN0cmFpbnRzID0gW11cbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3VwZGF0ZUxheW91dEZ1bmN0aW9uID0gbmlsXG4gICAgICAgIFxuICAgICAgICAvL09iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCBcImJhY2tncm91bmRDb2xvclwiLCB7IGdldDogdGhpcy5iYWNrZ3JvdW5kQ29sb3IsIHNldDogdGhpcy5zZXRCYWNrZ3JvdW5kQ29sb3IgfSk7XG4gICAgICAgIC8vdGhpcy5iYWNrZ3JvdW5kQ29sb3IgPSBcInRyYW5zcGFyZW50XCI7XG4gICAgICAgIFxuICAgICAgICAvLyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgXCJhbHBoYVwiLCB7IGdldDogdGhpcy5hbHBoYSwgc2V0OiB0aGlzLnNldEFscGhhIH0pO1xuICAgICAgICBcbiAgICAgICAgLy8gT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsIFwiZnJhbWVcIiwgeyBnZXQ6IHRoaXMuZnJhbWUsIHNldDogdGhpcy5zZXRGcmFtZSB9KTtcbiAgICAgICAgLy8gT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsIFwiYm91bmRzXCIsIHsgZ2V0OiB0aGlzLmJvdW5kcywgc2V0OiB0aGlzLnNldEJvdW5kcyB9KTtcbiAgICAgICAgXG4gICAgICAgIC8vIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCBcInVzZXJJbnRlcmFjdGlvbkVuYWJsZWRcIiwgeyBnZXQ6IHRoaXMudXNlckludGVyYWN0aW9uRW5hYmxlZCwgc2V0OiB0aGlzLnNldFVzZXJJbnRlcmFjdGlvbkVuYWJsZWQgfSk7XG4gICAgICAgIFxuICAgICAgICAvLyB0aGlzLl9jb250cm9sRXZlbnRUYXJnZXRzID0ge1xuICAgICAgICAvLyAgICAgXCJQb2ludGVyRG93blwiOiBbXSxcbiAgICAgICAgLy8gICAgIFwiUG9pbnRlck1vdmVcIjogW10sXG4gICAgICAgIC8vICAgICBcIlBvaW50ZXJMZWF2ZVwiOiBbXSxcbiAgICAgICAgLy8gICAgIFwiUG9pbnRlckVudGVyXCI6IFtdLFxuICAgICAgICAvLyAgICAgXCJQb2ludGVyVXBJbnNpZGVcIjogW10sXG4gICAgICAgIC8vICAgICBcIlBvaW50ZXJVcFwiOiBbXSxcbiAgICAgICAgLy8gICAgIFwiUG9pbnRlckhvdmVyXCI6IFtdXG4gICAgICAgIC8vIH1cbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2RpZExheW91dFN1YnZpZXdzRGVsZWdhdGVGdW5jdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fZnJhbWVUcmFuc2Zvcm0gPSBcIlwiXG4gICAgICAgIFxuICAgICAgICB0aGlzLmluaXRWaWV3KHRoaXMudmlld0hUTUxFbGVtZW50LmlkLCB0aGlzLnZpZXdIVE1MRWxlbWVudCwgaW5pdFZpZXdEYXRhKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5faW5pdFZpZXdDU1NTZWxlY3RvcnNJZk5lZWRlZCgpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9sb2FkVUlFdmVudHMoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2V0TmVlZHNMYXlvdXQoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBnZXQgbmV4dEluZGV4KCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIFVJVmlldy5fVUlWaWV3SW5kZXggKyAxXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgZ2V0IHBhZ2VIZWlnaHQoKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBib2R5ID0gZG9jdW1lbnQuYm9keVxuICAgICAgICBjb25zdCBodG1sID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50XG4gICAgICAgIFxuICAgICAgICBjb25zdCBoZWlnaHQgPSBNYXRoLm1heChcbiAgICAgICAgICAgIGJvZHkuc2Nyb2xsSGVpZ2h0LFxuICAgICAgICAgICAgYm9keS5vZmZzZXRIZWlnaHQsXG4gICAgICAgICAgICBodG1sLmNsaWVudEhlaWdodCxcbiAgICAgICAgICAgIGh0bWwuc2Nyb2xsSGVpZ2h0LFxuICAgICAgICAgICAgaHRtbC5vZmZzZXRIZWlnaHRcbiAgICAgICAgKVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIGhlaWdodFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIGdldCBwYWdlV2lkdGgoKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBib2R5ID0gZG9jdW1lbnQuYm9keVxuICAgICAgICBjb25zdCBodG1sID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50XG4gICAgICAgIFxuICAgICAgICBjb25zdCB3aWR0aCA9IE1hdGgubWF4KGJvZHkuc2Nyb2xsV2lkdGgsIGJvZHkub2Zmc2V0V2lkdGgsIGh0bWwuY2xpZW50V2lkdGgsIGh0bWwuc2Nyb2xsV2lkdGgsIGh0bWwub2Zmc2V0V2lkdGgpXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gd2lkdGhcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGluaXRWaWV3KGVsZW1lbnRJRDogc3RyaW5nLCB2aWV3SFRNTEVsZW1lbnQ6IEhUTUxFbGVtZW50LCBpbml0Vmlld0RhdGE/OiBhbnkpIHtcbiAgICBcbiAgICBcbiAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgY2VudGVySW5Db250YWluZXIoKSB7XG4gICAgICAgIHRoaXMuc3R5bGUubGVmdCA9IFwiNTAlXCJcbiAgICAgICAgdGhpcy5zdHlsZS50b3AgPSBcIjUwJVwiXG4gICAgICAgIHRoaXMuc3R5bGUudHJhbnNmb3JtID0gXCJ0cmFuc2xhdGVYKC01MCUpIHRyYW5zbGF0ZVkoLTUwJSlcIlxuICAgIH1cbiAgICBcbiAgICBjZW50ZXJYSW5Db250YWluZXIoKSB7XG4gICAgICAgIHRoaXMuc3R5bGUubGVmdCA9IFwiNTAlXCJcbiAgICAgICAgdGhpcy5zdHlsZS50cmFuc2Zvcm0gPSBcInRyYW5zbGF0ZVgoLTUwJSlcIlxuICAgIH1cbiAgICBcbiAgICBjZW50ZXJZSW5Db250YWluZXIoKSB7XG4gICAgICAgIHRoaXMuc3R5bGUudG9wID0gXCI1MCVcIlxuICAgICAgICB0aGlzLnN0eWxlLnRyYW5zZm9ybSA9IFwidHJhbnNsYXRlWSgtNTAlKVwiXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIF9pbml0Vmlld0hUTUxFbGVtZW50KGVsZW1lbnRJRCwgdmlld0hUTUxFbGVtZW50LCBlbGVtZW50VHlwZSA9IFwiZGl2XCIpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAoIUlTKGVsZW1lbnRUeXBlKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBlbGVtZW50VHlwZSA9IFwiZGl2XCJcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAoIUlTKHZpZXdIVE1MRWxlbWVudCkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50ID0gdGhpcy5jcmVhdGVFbGVtZW50KGVsZW1lbnRJRCwgZWxlbWVudFR5cGUpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCJcbiAgICAgICAgICAgIHRoaXMuc3R5bGUubWFyZ2luID0gXCIwXCJcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl92aWV3SFRNTEVsZW1lbnQgPSB2aWV3SFRNTEVsZW1lbnRcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAoSVMoZWxlbWVudElEKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LmlkID0gZWxlbWVudElEXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQub2JleUF1dG9sYXlvdXQgPSBZRVNcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LlVJVmlldyA9IHRoaXNcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYWRkU3R5bGVDbGFzcyh0aGlzLnN0eWxlQ2xhc3NOYW1lKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc2V0IG5hdGl2ZVNlbGVjdGlvbkVuYWJsZWQoc2VsZWN0YWJsZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9uYXRpdmVTZWxlY3Rpb25FbmFibGVkID0gc2VsZWN0YWJsZVxuICAgICAgICBpZiAoIXNlbGVjdGFibGUpIHtcbiAgICAgICAgICAgIHRoaXMuc3R5bGUuY3NzVGV4dCA9IHRoaXMuc3R5bGUuY3NzVGV4dCArXG4gICAgICAgICAgICAgICAgXCIgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lOyAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lOyAta2h0bWwtdXNlci1zZWxlY3Q6IG5vbmU7IC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7IC1tcy11c2VyLXNlbGVjdDogbm9uZTsgdXNlci1zZWxlY3Q6IG5vbmU7XCJcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc3R5bGUuY3NzVGV4dCA9IHRoaXMuc3R5bGUuY3NzVGV4dCArXG4gICAgICAgICAgICAgICAgXCIgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiB0ZXh0OyAtd2Via2l0LXVzZXItc2VsZWN0OiB0ZXh0OyAta2h0bWwtdXNlci1zZWxlY3Q6IHRleHQ7IC1tb3otdXNlci1zZWxlY3Q6IHRleHQ7IC1tcy11c2VyLXNlbGVjdDogdGV4dDsgdXNlci1zZWxlY3Q6IHRleHQ7XCJcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgbmF0aXZlU2VsZWN0aW9uRW5hYmxlZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX25hdGl2ZVNlbGVjdGlvbkVuYWJsZWRcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IHN0eWxlQ2xhc3NOYW1lKCkge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gXCJVSUNvcmVfVUlWaWV3X1wiICsgdGhpcy5jbGFzcy5uYW1lXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBfaW5pdFZpZXdDU1NTZWxlY3RvcnNJZk5lZWRlZCgpIHtcbiAgICAgICAgXG4gICAgICAgIGlmICghdGhpcy5jbGFzcy5fYXJlVmlld0NTU1NlbGVjdG9yc0luaXRpYWxpemVkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuaW5pdFZpZXdTdHlsZVNlbGVjdG9ycygpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuY2xhc3MuX2FyZVZpZXdDU1NTZWxlY3RvcnNJbml0aWFsaXplZCA9IFlFU1xuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGluaXRWaWV3U3R5bGVTZWxlY3RvcnMoKSB7XG4gICAgICAgIFxuICAgICAgICAvLyBPdmVycmlkZSB0aGlzIGluIGEgc3ViY2xhc3NcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGluaXRTdHlsZVNlbGVjdG9yKHNlbGVjdG9yLCBzdHlsZSkge1xuICAgICAgICBcbiAgICAgICAgY29uc3Qgc3R5bGVSdWxlcyA9IFVJVmlldy5nZXRTdHlsZVJ1bGVzKHNlbGVjdG9yKVxuICAgICAgICBcbiAgICAgICAgaWYgKCFzdHlsZVJ1bGVzKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFVJVmlldy5jcmVhdGVTdHlsZVNlbGVjdG9yKHNlbGVjdG9yLCBzdHlsZSlcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBjcmVhdGVFbGVtZW50KGVsZW1lbnRJRCwgZWxlbWVudFR5cGUpIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGVsZW1lbnRJRClcbiAgICAgICAgaWYgKCFyZXN1bHQpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoZWxlbWVudFR5cGUpXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgZ2V0IHZpZXdIVE1MRWxlbWVudCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3ZpZXdIVE1MRWxlbWVudFxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgZ2V0IGVsZW1lbnRJRCgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLnZpZXdIVE1MRWxlbWVudC5pZFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0SW5uZXJIVE1MKGtleTogc3RyaW5nLCBkZWZhdWx0U3RyaW5nOiBzdHJpbmcsIHBhcmFtZXRlcnM/OiB7IFt4OiBzdHJpbmddOiBzdHJpbmcgfCBVSUxvY2FsaXplZFRleHRPYmplY3QgfSkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5faW5uZXJIVE1MS2V5ID0ga2V5XG4gICAgICAgIHRoaXMuX2RlZmF1bHRJbm5lckhUTUwgPSBkZWZhdWx0U3RyaW5nXG4gICAgICAgIHRoaXMuX3BhcmFtZXRlcnMgPSBwYXJhbWV0ZXJzXG4gICAgICAgIFxuICAgICAgICBjb25zdCBsYW5ndWFnZU5hbWUgPSBVSUNvcmUubGFuZ3VhZ2VTZXJ2aWNlLmN1cnJlbnRMYW5ndWFnZUtleVxuICAgICAgICBjb25zdCByZXN1bHQgPSBVSUNvcmUubGFuZ3VhZ2VTZXJ2aWNlLnN0cmluZ0ZvcktleShrZXksIGxhbmd1YWdlTmFtZSwgZGVmYXVsdFN0cmluZywgcGFyYW1ldGVycylcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaW5uZXJIVE1MID0gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBfc2V0SW5uZXJIVE1MRnJvbUtleUlmUG9zc2libGUoKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5faW5uZXJIVE1MS2V5ICYmIHRoaXMuX2RlZmF1bHRJbm5lckhUTUwpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zZXRJbm5lckhUTUwodGhpcy5faW5uZXJIVE1MS2V5LCB0aGlzLl9kZWZhdWx0SW5uZXJIVE1MLCB0aGlzLl9wYXJhbWV0ZXJzKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIF9zZXRJbm5lckhUTUxGcm9tTG9jYWxpemVkVGV4dE9iamVjdElmUG9zc2libGUoKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoSVModGhpcy5fbG9jYWxpemVkVGV4dE9iamVjdCkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5pbm5lckhUTUwgPSBVSUNvcmUubGFuZ3VhZ2VTZXJ2aWNlLnN0cmluZ0ZvckN1cnJlbnRMYW5ndWFnZSh0aGlzLl9sb2NhbGl6ZWRUZXh0T2JqZWN0KVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBsb2NhbGl6ZWRUZXh0T2JqZWN0KCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xvY2FsaXplZFRleHRPYmplY3RcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNldCBsb2NhbGl6ZWRUZXh0T2JqZWN0KGxvY2FsaXplZFRleHRPYmplY3Q6IFVJTG9jYWxpemVkVGV4dE9iamVjdCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fbG9jYWxpemVkVGV4dE9iamVjdCA9IGxvY2FsaXplZFRleHRPYmplY3RcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3NldElubmVySFRNTEZyb21Mb2NhbGl6ZWRUZXh0T2JqZWN0SWZQb3NzaWJsZSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgaW5uZXJIVE1MKCkge1xuICAgICAgICByZXR1cm4gdGhpcy52aWV3SFRNTEVsZW1lbnQuaW5uZXJIVE1MXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHNldCBpbm5lckhUTUwoaW5uZXJIVE1MKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5pbm5lckhUTUwgIT0gaW5uZXJIVE1MKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LmlubmVySFRNTCA9IEZJUlNUKGlubmVySFRNTCwgXCJcIilcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzZXQgaG92ZXJUZXh0KGhvdmVyVGV4dDogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LnNldEF0dHJpYnV0ZShcInRpdGxlXCIsIGhvdmVyVGV4dClcbiAgICB9XG4gICAgXG4gICAgZ2V0IGhvdmVyVGV4dCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudmlld0hUTUxFbGVtZW50LmdldEF0dHJpYnV0ZShcInRpdGxlXCIpXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBzY3JvbGxTaXplKCkge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IFVJUmVjdGFuZ2xlKDAsIDAsIHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbEhlaWdodCwgdGhpcy52aWV3SFRNTEVsZW1lbnQuc2Nyb2xsV2lkdGgpXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgZGlhbG9nVmlldygpOiBVSURpYWxvZ1ZpZXcge1xuICAgICAgICBpZiAoIUlTKHRoaXMuc3VwZXJ2aWV3KSkge1xuICAgICAgICAgICAgcmV0dXJuIG5pbFxuICAgICAgICB9XG4gICAgICAgIGlmICghKHRoaXMgaW5zdGFuY2VvZiBVSURpYWxvZ1ZpZXcpKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdXBlcnZpZXcuZGlhbG9nVmlld1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCByb290VmlldygpOiBVSVZpZXcge1xuICAgICAgICBpZiAoSVModGhpcy5zdXBlcnZpZXcpKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdXBlcnZpZXcucm9vdFZpZXdcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpc1xuICAgIH1cbiAgICBcbiAgICBcbiAgICBwdWJsaWMgc2V0IGVuYWJsZWQoZW5hYmxlZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9lbmFibGVkID0gZW5hYmxlZFxuICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50RW5hYmxlZFN0YXRlKClcbiAgICB9XG4gICAgXG4gICAgcHVibGljIGdldCBlbmFibGVkKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fZW5hYmxlZFxuICAgIH1cbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yQ3VycmVudEVuYWJsZWRTdGF0ZSgpIHtcbiAgICAgICAgdGhpcy5oaWRkZW4gPSAhdGhpcy5lbmFibGVkXG4gICAgICAgIHRoaXMudXNlckludGVyYWN0aW9uRW5hYmxlZCA9IHRoaXMuZW5hYmxlZFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBwdWJsaWMgZ2V0IHRhYkluZGV4KCk6IG51bWJlciB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gTnVtYmVyKHRoaXMudmlld0hUTUxFbGVtZW50LmdldEF0dHJpYnV0ZShcInRhYmluZGV4XCIpKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgcHVibGljIHNldCB0YWJJbmRleChpbmRleDogbnVtYmVyKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLnZpZXdIVE1MRWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJ0YWJpbmRleFwiLCBcIlwiICsgaW5kZXgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgc3R5bGVDbGFzc2VzKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0eWxlQ2xhc3Nlc1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IHN0eWxlQ2xhc3NlcyhzdHlsZUNsYXNzZXMpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3N0eWxlQ2xhc3NlcyA9IHN0eWxlQ2xhc3Nlc1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgaGFzU3R5bGVDbGFzcyhzdHlsZUNsYXNzKSB7XG4gICAgICAgIFxuICAgICAgICAvLyBUaGlzIGlzIGZvciBwZXJmb3JtYW5jZSByZWFzb25zXG4gICAgICAgIGlmICghSVMoc3R5bGVDbGFzcykpIHtcbiAgICAgICAgICAgIHJldHVybiBOT1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuc3R5bGVDbGFzc2VzLmluZGV4T2Yoc3R5bGVDbGFzcylcbiAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcbiAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gTk9cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGFkZFN0eWxlQ2xhc3Moc3R5bGVDbGFzczogc3RyaW5nKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoIUlTKHN0eWxlQ2xhc3MpKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKCF0aGlzLmhhc1N0eWxlQ2xhc3Moc3R5bGVDbGFzcykpIHtcbiAgICAgICAgICAgIHRoaXMuX3N0eWxlQ2xhc3Nlcy5wdXNoKHN0eWxlQ2xhc3MpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHJlbW92ZVN0eWxlQ2xhc3Moc3R5bGVDbGFzczogc3RyaW5nKSB7XG4gICAgICAgIFxuICAgICAgICAvLyBUaGlzIGlzIGZvciBwZXJmb3JtYW5jZSByZWFzb25zXG4gICAgICAgIGlmICghSVMoc3R5bGVDbGFzcykpIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuc3R5bGVDbGFzc2VzLmluZGV4T2Yoc3R5bGVDbGFzcylcbiAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zdHlsZUNsYXNzZXMuc3BsaWNlKGluZGV4LCAxKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgZmluZFZpZXdXaXRoRWxlbWVudElEKGVsZW1lbnRJRDogc3RyaW5nKTogVUlWaWV3IHtcbiAgICAgICAgY29uc3Qgdmlld0hUTUxFbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZWxlbWVudElEKVxuICAgICAgICBpZiAoSVNfTk9UKHZpZXdIVE1MRWxlbWVudCkpIHtcbiAgICAgICAgICAgIHJldHVybiBuaWxcbiAgICAgICAgfVxuICAgICAgICAvLyBAdHMtaWdub3JlXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHZpZXdIVE1MRWxlbWVudC5VSVZpZXdcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgY3JlYXRlU3R5bGVTZWxlY3RvcihzZWxlY3Rvciwgc3R5bGUpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVyblxuICAgICAgICBcbiAgICAgICAgLy8gQHRzLWlnbm9yZVxuICAgICAgICBpZiAoIWRvY3VtZW50LnN0eWxlU2hlZXRzKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJoZWFkXCIpLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdmFyIHN0eWxlU2hlZXRcbiAgICAgICAgdmFyIG1lZGlhVHlwZVxuICAgICAgICBcbiAgICAgICAgaWYgKGRvY3VtZW50LnN0eWxlU2hlZXRzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBsOiBhbnkgPSBkb2N1bWVudC5zdHlsZVNoZWV0cy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZiAoZG9jdW1lbnQuc3R5bGVTaGVldHNbaV0uZGlzYWJsZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgY29udGludWVcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY29uc3QgbWVkaWEgPSBkb2N1bWVudC5zdHlsZVNoZWV0c1tpXS5tZWRpYVxuICAgICAgICAgICAgICAgIG1lZGlhVHlwZSA9IHR5cGVvZiBtZWRpYVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGlmIChtZWRpYVR5cGUgPT09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKG1lZGlhIGFzIGFueSA9PT0gXCJcIiB8fCAoKG1lZGlhIGFzIGFueSkuaW5kZXhPZihcInNjcmVlblwiKSAhPT0gLTEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZVNoZWV0ID0gZG9jdW1lbnQuc3R5bGVTaGVldHNbaV1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmIChtZWRpYVR5cGUgPT0gXCJvYmplY3RcIikge1xuICAgICAgICAgICAgICAgICAgICBpZiAobWVkaWEubWVkaWFUZXh0ID09PSBcIlwiIHx8IChtZWRpYS5tZWRpYVRleHQuaW5kZXhPZihcInNjcmVlblwiKSAhPT0gLTEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZVNoZWV0ID0gZG9jdW1lbnQuc3R5bGVTaGVldHNbaV1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHN0eWxlU2hlZXQgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmICh0eXBlb2Ygc3R5bGVTaGVldCA9PT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgY29uc3Qgc3R5bGVTaGVldEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic3R5bGVcIilcbiAgICAgICAgICAgIHN0eWxlU2hlZXRFbGVtZW50LnR5cGUgPSBcInRleHQvY3NzXCJcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaGVhZFwiKVswXS5hcHBlbmRDaGlsZChzdHlsZVNoZWV0RWxlbWVudClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGRvY3VtZW50LnN0eWxlU2hlZXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYgKGRvY3VtZW50LnN0eWxlU2hlZXRzW2ldLmRpc2FibGVkKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHN0eWxlU2hlZXQgPSBkb2N1bWVudC5zdHlsZVNoZWV0c1tpXVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBtZWRpYVR5cGUgPSB0eXBlb2Ygc3R5bGVTaGVldC5tZWRpYVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAobWVkaWFUeXBlID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMCwgbCA9IHN0eWxlU2hlZXQucnVsZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0eWxlU2hlZXQucnVsZXNbaV0uc2VsZWN0b3JUZXh0ICYmIHN0eWxlU2hlZXQucnVsZXNbaV0uc2VsZWN0b3JUZXh0LnRvTG93ZXJDYXNlKCkgPT1cbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0b3IudG9Mb3dlckNhc2UoKSkge1xuICAgICAgICAgICAgICAgICAgICBzdHlsZVNoZWV0LnJ1bGVzW2ldLnN0eWxlLmNzc1RleHQgPSBzdHlsZVxuICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzdHlsZVNoZWV0LmFkZFJ1bGUoc2VsZWN0b3IsIHN0eWxlKVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKG1lZGlhVHlwZSA9PT0gXCJvYmplY3RcIikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB2YXIgc3R5bGVTaGVldExlbmd0aCA9IDBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBzdHlsZVNoZWV0TGVuZ3RoID0gKHN0eWxlU2hlZXQuY3NzUnVsZXMpID8gc3R5bGVTaGVldC5jc3NSdWxlcy5sZW5ndGggOiAwXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVTaGVldExlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0eWxlU2hlZXQuY3NzUnVsZXNbaV0uc2VsZWN0b3JUZXh0ICYmIHN0eWxlU2hlZXQuY3NzUnVsZXNbaV0uc2VsZWN0b3JUZXh0LnRvTG93ZXJDYXNlKCkgPT1cbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0b3IudG9Mb3dlckNhc2UoKSkge1xuICAgICAgICAgICAgICAgICAgICBzdHlsZVNoZWV0LmNzc1J1bGVzW2ldLnN0eWxlLmNzc1RleHQgPSBzdHlsZVxuICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzdHlsZVNoZWV0Lmluc2VydFJ1bGUoc2VsZWN0b3IgKyBcIntcIiArIHN0eWxlICsgXCJ9XCIsIHN0eWxlU2hlZXRMZW5ndGgpXG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgc3RhdGljIGdldFN0eWxlUnVsZXMoc2VsZWN0b3IpIHtcbiAgICAgICAgdmFyIHNlbGVjdG9yID0gc2VsZWN0b3IudG9Mb3dlckNhc2UoKVxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRvY3VtZW50LnN0eWxlU2hlZXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCBzdHlsZVNoZWV0ID0gZG9jdW1lbnQuc3R5bGVTaGVldHNbaV0gYXMgYW55XG4gICAgICAgICAgICB2YXIgc3R5bGVSdWxlc1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHN0eWxlUnVsZXMgPSBzdHlsZVNoZWV0LmNzc1J1bGVzID8gc3R5bGVTaGVldC5jc3NSdWxlcyA6IHN0eWxlU2hlZXQucnVsZXNcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVybiBzdHlsZVJ1bGVzXG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IHN0eWxlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy52aWV3SFRNTEVsZW1lbnQuc3R5bGVcbiAgICB9XG4gICAgXG4gICAgZ2V0IGNvbXB1dGVkU3R5bGUoKSB7XG4gICAgICAgIHJldHVybiBnZXRDb21wdXRlZFN0eWxlKHRoaXMudmlld0hUTUxFbGVtZW50KVxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgZ2V0IGhpZGRlbigpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzSGlkZGVuXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHB1YmxpYyBzZXQgaGlkZGVuKHY6IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzSGlkZGVuID0gdlxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX2lzSGlkZGVuKSB7XG4gICAgICAgICAgICB0aGlzLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnN0eWxlLnZpc2liaWxpdHkgPSBcInZpc2libGVcIlxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIHNldCBwYWdlU2NhbGUoc2NhbGU6IG51bWJlcikge1xuICAgICAgICBcbiAgICAgICAgVUlWaWV3Ll9wYWdlU2NhbGUgPSBzY2FsZVxuICAgICAgICBcbiAgICAgICAgY29uc3Qgem9vbSA9IHNjYWxlXG4gICAgICAgIGNvbnN0IHdpZHRoID0gMTAwIC8gem9vbVxuICAgICAgICBjb25zdCB2aWV3SFRNTEVsZW1lbnQgPSBVSUNvcmUubWFpbi5yb290Vmlld0NvbnRyb2xsZXIudmlldy52aWV3SFRNTEVsZW1lbnRcbiAgICAgICAgdmlld0hUTUxFbGVtZW50LnN0eWxlLnRyYW5zZm9ybU9yaWdpbiA9IFwibGVmdCB0b3BcIlxuICAgICAgICB2aWV3SFRNTEVsZW1lbnQuc3R5bGUudHJhbnNmb3JtID0gXCJzY2FsZShcIiArIHpvb20gKyBcIilcIlxuICAgICAgICB2aWV3SFRNTEVsZW1lbnQuc3R5bGUud2lkdGggPSB3aWR0aCArIFwiJVwiXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgZ2V0IHBhZ2VTY2FsZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiBVSVZpZXcuX3BhZ2VTY2FsZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgY2FsY3VsYXRlQW5kU2V0Vmlld0ZyYW1lKCkge1xuICAgICAgICBcbiAgICAgICAgLy8gVXNlIHRoaXMgbWV0aG9kIHRvIGNhbGN1bGF0ZSB0aGUgZnJhbWUgZm9yIHRoZSB2aWV3IGl0c2VsZlxuICAgICAgICBcbiAgICAgICAgLy8gVGhpcyBjYW4gYmUgdXNlZCB3aGVuIGFkZGluZyBzdWJ2aWV3cyB0byBleGlzdGluZyB2aWV3cyBsaWtlIGJ1dHRvbnNcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHB1YmxpYyBnZXQgZnJhbWUoKTogVUlSZWN0YW5nbGUge1xuICAgICAgICBcbiAgICAgICAgLy8gdmFyIHJlc3VsdCA9IG5ldyBVSVJlY3RhbmdsZSgxICogdGhpcy52aWV3SFRNTEVsZW1lbnQub2Zmc2V0TGVmdCwgMSAqIHRoaXMudmlld0hUTUxFbGVtZW50Lm9mZnNldFRvcCwgMSAqIHRoaXMudmlld0hUTUxFbGVtZW50Lm9mZnNldEhlaWdodCwgMSAqIHRoaXMudmlld0hUTUxFbGVtZW50Lm9mZnNldFdpZHRoKTtcbiAgICAgICAgXG4gICAgICAgIC8vIHJlc3VsdC56SW5kZXggPSAxICogdGhpcy5zdHlsZS56SW5kZXg7XG4gICAgICAgIFxuICAgICAgICB2YXIgcmVzdWx0ID0gdGhpcy5fZnJhbWVcbiAgICAgICAgXG4gICAgICAgIGlmICghcmVzdWx0KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJlc3VsdCA9IG5ldyBVSVJlY3RhbmdsZSgxICogdGhpcy52aWV3SFRNTEVsZW1lbnQub2Zmc2V0TGVmdCwgMSAqIHRoaXMudmlld0hUTUxFbGVtZW50Lm9mZnNldFRvcCwgMSAqXG4gICAgICAgICAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQub2Zmc2V0SGVpZ2h0LCAxICogdGhpcy52aWV3SFRNTEVsZW1lbnQub2Zmc2V0V2lkdGgpXG4gICAgICAgICAgICByZXN1bHQuekluZGV4ID0gMFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHQuY29weSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgc2V0IGZyYW1lKHJlY3RhbmdsZTogVUlSZWN0YW5nbGUpIHtcbiAgICAgICAgXG4gICAgICAgIGlmIChJUyhyZWN0YW5nbGUpKSB7XG4gICAgICAgICAgICB0aGlzLnNldEZyYW1lKHJlY3RhbmdsZSlcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0RnJhbWUocmVjdGFuZ2xlLCB6SW5kZXggPSAwLCBwZXJmb3JtVW5jaGVja2VkTGF5b3V0ID0gTk8pIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBjb25zdCBmcmFtZSA9IHRoaXMuX2ZyYW1lIHx8IG5ldyBVSVJlY3RhbmdsZShuaWwsIG5pbCwgbmlsLCBuaWwpXG4gICAgICAgIFxuICAgICAgICBpZiAoekluZGV4ICE9IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVjdGFuZ2xlLnpJbmRleCA9IHpJbmRleFxuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2ZyYW1lID0gcmVjdGFuZ2xlXG4gICAgICAgIFxuICAgICAgICAvLyBUaGlzIGlzIHVzZWxlc3MgYmVjYXVzZSBmcmFtZXMgYXJlIGNvcGllZFxuICAgICAgICAvLyBmcmFtZS5kaWRDaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vICAgICAvLyBEbyBub3RoaW5nXG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gcmVjdGFuZ2xlLmRpZENoYW5nZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy8gICAgIHRoaXMuZnJhbWUgPSByZWN0YW5nbGU7XG4gICAgICAgIC8vIH0uYmluZCh0aGlzKTtcbiAgICAgICAgXG4gICAgICAgIGlmIChmcmFtZSAmJiBmcmFtZS5pc0VxdWFsVG8ocmVjdGFuZ2xlKSAmJiAhcGVyZm9ybVVuY2hlY2tlZExheW91dCkge1xuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBVSVZpZXcuX3NldEFic29sdXRlU2l6ZUFuZFBvc2l0aW9uKFxuICAgICAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQsXG4gICAgICAgICAgICByZWN0YW5nbGUudG9wTGVmdC54LFxuICAgICAgICAgICAgcmVjdGFuZ2xlLnRvcExlZnQueSxcbiAgICAgICAgICAgIHJlY3RhbmdsZS53aWR0aCxcbiAgICAgICAgICAgIHJlY3RhbmdsZS5oZWlnaHQsXG4gICAgICAgICAgICByZWN0YW5nbGUuekluZGV4XG4gICAgICAgIClcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAoZnJhbWUuaGVpZ2h0ICE9IHJlY3RhbmdsZS5oZWlnaHQgfHwgZnJhbWUud2lkdGggIT0gcmVjdGFuZ2xlLndpZHRoIHx8IHBlcmZvcm1VbmNoZWNrZWRMYXlvdXQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dCgpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuYm91bmRzRGlkQ2hhbmdlKClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy90aGlzLmxheW91dFN1YnZpZXdzKCk7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgYm91bmRzKCkge1xuICAgICAgICBcbiAgICAgICAgdmFyIHJlc3VsdDogVUlSZWN0YW5nbGVcbiAgICAgICAgXG4gICAgICAgIC8vIGlmIChJU19OT1QodGhpcy5fZnJhbWUpICYmIHRoaXMuc3R5bGUuaGVpZ2h0ID09IFwiXCIgJiYgdGhpcy5zdHlsZS53aWR0aCAgPT0gXCJcIiAmJiB0aGlzLnN0eWxlLmxlZnQgPT0gXCJcIiAmJiB0aGlzLnN0eWxlLnJpZ2h0ID09IFwiXCIgJiYgdGhpcy5zdHlsZS5ib3R0b20gPT0gXCJcIiAmJiB0aGlzLnN0eWxlLnRvcCA9PSBcIlwiKSB7XG4gICAgICAgIFxuICAgICAgICAvLyAgICAgcmVzdWx0ID0gbmV3IFVJUmVjdGFuZ2xlKDAsIDAsIDAsIDApXG4gICAgICAgIFxuICAgICAgICAvLyB9XG4gICAgICAgIC8vIGVsc2VcbiAgICAgICAgaWYgKElTX05PVCh0aGlzLl9mcmFtZSkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmVzdWx0ID0gbmV3IFVJUmVjdGFuZ2xlKDAsIDAsIDEgKiB0aGlzLnZpZXdIVE1MRWxlbWVudC5vZmZzZXRIZWlnaHQsIDEgKiB0aGlzLnZpZXdIVE1MRWxlbWVudC5vZmZzZXRXaWR0aClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXN1bHQgPSB0aGlzLmZyYW1lLmNvcHkoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXN1bHQueCA9IDBcbiAgICAgICAgICAgIHJlc3VsdC55ID0gMFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNldCBib3VuZHMocmVjdGFuZ2xlKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBmcmFtZSA9IHRoaXMuZnJhbWVcbiAgICAgICAgXG4gICAgICAgIHRoaXMuZnJhbWUgPSBuZXcgVUlSZWN0YW5nbGUoZnJhbWUudG9wTGVmdC54LCBmcmFtZS50b3BMZWZ0LnksIHJlY3RhbmdsZS5oZWlnaHQsIHJlY3RhbmdsZS53aWR0aClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGJvdW5kc0RpZENoYW5nZSgpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0UG9zaXRpb24oXG4gICAgICAgIGxlZnQ6IG51bWJlciB8IHN0cmluZyA9IG5pbCxcbiAgICAgICAgcmlnaHQ6IG51bWJlciB8IHN0cmluZyA9IG5pbCxcbiAgICAgICAgYm90dG9tOiBudW1iZXIgfCBzdHJpbmcgPSBuaWwsXG4gICAgICAgIHRvcDogbnVtYmVyIHwgc3RyaW5nID0gbmlsLFxuICAgICAgICBoZWlnaHQ6IG51bWJlciB8IHN0cmluZyA9IG5pbCxcbiAgICAgICAgd2lkdGg6IG51bWJlciB8IHN0cmluZyA9IG5pbFxuICAgICkge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcHJldmlvdXNCb3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibGVmdFwiLCBsZWZ0KVxuICAgICAgICB0aGlzLnNldFN0eWxlUHJvcGVydHkoXCJyaWdodFwiLCByaWdodClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwiYm90dG9tXCIsIGJvdHRvbSlcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwidG9wXCIsIHRvcClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwiaGVpZ2h0XCIsIGhlaWdodClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwid2lkdGhcIiwgd2lkdGgpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBpZiAoYm91bmRzLmhlaWdodCAhPSBwcmV2aW91c0JvdW5kcy5oZWlnaHQgfHwgYm91bmRzLndpZHRoICE9IHByZXZpb3VzQm91bmRzLndpZHRoKSB7XG4gICAgICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgICAgIHRoaXMuYm91bmRzRGlkQ2hhbmdlKClcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0U2l6ZXMoaGVpZ2h0PzogbnVtYmVyIHwgc3RyaW5nLCB3aWR0aD86IG51bWJlciB8IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcHJldmlvdXNCb3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwiaGVpZ2h0XCIsIGhlaWdodClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwid2lkdGhcIiwgd2lkdGgpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBpZiAoYm91bmRzLmhlaWdodCAhPSBwcmV2aW91c0JvdW5kcy5oZWlnaHQgfHwgYm91bmRzLndpZHRoICE9IHByZXZpb3VzQm91bmRzLndpZHRoKSB7XG4gICAgICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgICAgIHRoaXMuYm91bmRzRGlkQ2hhbmdlKClcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0TWluU2l6ZXMoaGVpZ2h0PzogbnVtYmVyIHwgc3RyaW5nLCB3aWR0aD86IG51bWJlciB8IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcHJldmlvdXNCb3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWluSGVpZ2h0XCIsIGhlaWdodClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWluV2lkdGhcIiwgd2lkdGgpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBpZiAoYm91bmRzLmhlaWdodCAhPSBwcmV2aW91c0JvdW5kcy5oZWlnaHQgfHwgYm91bmRzLndpZHRoICE9IHByZXZpb3VzQm91bmRzLndpZHRoKSB7XG4gICAgICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgICAgIHRoaXMuYm91bmRzRGlkQ2hhbmdlKClcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0TWF4U2l6ZXMoaGVpZ2h0PzogbnVtYmVyIHwgc3RyaW5nLCB3aWR0aD86IG51bWJlciB8IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcHJldmlvdXNCb3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWF4SGVpZ2h0XCIsIGhlaWdodClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWF4V2lkdGhcIiwgd2lkdGgpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBpZiAoYm91bmRzLmhlaWdodCAhPSBwcmV2aW91c0JvdW5kcy5oZWlnaHQgfHwgYm91bmRzLndpZHRoICE9IHByZXZpb3VzQm91bmRzLndpZHRoKSB7XG4gICAgICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgICAgIHRoaXMuYm91bmRzRGlkQ2hhbmdlKClcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0TWFyZ2luKG1hcmdpbj86IG51bWJlciB8IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcHJldmlvdXNCb3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWFyZ2luXCIsIG1hcmdpbilcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGJvdW5kcyA9IHRoaXMuYm91bmRzXG4gICAgICAgIGlmIChib3VuZHMuaGVpZ2h0ICE9IHByZXZpb3VzQm91bmRzLmhlaWdodCB8fCBib3VuZHMud2lkdGggIT0gcHJldmlvdXNCb3VuZHMud2lkdGgpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0TmVlZHNMYXlvdXQoKVxuICAgICAgICAgICAgdGhpcy5ib3VuZHNEaWRDaGFuZ2UoKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXRNYXJnaW5zKGxlZnQ/OiBudW1iZXIgfCBzdHJpbmcsIHJpZ2h0PzogbnVtYmVyIHwgc3RyaW5nLCBib3R0b20/OiBudW1iZXIgfCBzdHJpbmcsIHRvcD86IG51bWJlciB8IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcHJldmlvdXNCb3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWFyZ2luTGVmdFwiLCBsZWZ0KVxuICAgICAgICB0aGlzLnNldFN0eWxlUHJvcGVydHkoXCJtYXJnaW5SaWdodFwiLCByaWdodClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWFyZ2luQm90dG9tXCIsIGJvdHRvbSlcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwibWFyZ2luVG9wXCIsIHRvcClcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGJvdW5kcyA9IHRoaXMuYm91bmRzXG4gICAgICAgIGlmIChib3VuZHMuaGVpZ2h0ICE9IHByZXZpb3VzQm91bmRzLmhlaWdodCB8fCBib3VuZHMud2lkdGggIT0gcHJldmlvdXNCb3VuZHMud2lkdGgpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0TmVlZHNMYXlvdXQoKVxuICAgICAgICAgICAgdGhpcy5ib3VuZHNEaWRDaGFuZ2UoKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXRQYWRkaW5nKHBhZGRpbmc/OiBudW1iZXIgfCBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHByZXZpb3VzQm91bmRzID0gdGhpcy5ib3VuZHNcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2V0U3R5bGVQcm9wZXJ0eShcInBhZGRpbmdcIiwgcGFkZGluZylcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGJvdW5kcyA9IHRoaXMuYm91bmRzXG4gICAgICAgIGlmIChib3VuZHMuaGVpZ2h0ICE9IHByZXZpb3VzQm91bmRzLmhlaWdodCB8fCBib3VuZHMud2lkdGggIT0gcHJldmlvdXNCb3VuZHMud2lkdGgpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0TmVlZHNMYXlvdXQoKVxuICAgICAgICAgICAgdGhpcy5ib3VuZHNEaWRDaGFuZ2UoKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXRQYWRkaW5ncyhsZWZ0PzogbnVtYmVyIHwgc3RyaW5nLCByaWdodD86IG51bWJlciB8IHN0cmluZywgYm90dG9tPzogbnVtYmVyIHwgc3RyaW5nLCB0b3A/OiBudW1iZXIgfCBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHByZXZpb3VzQm91bmRzID0gdGhpcy5ib3VuZHNcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2V0U3R5bGVQcm9wZXJ0eShcInBhZGRpbmdMZWZ0XCIsIGxlZnQpXG4gICAgICAgIHRoaXMuc2V0U3R5bGVQcm9wZXJ0eShcInBhZGRpbmdSaWdodFwiLCByaWdodClcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwicGFkZGluZ0JvdHRvbVwiLCBib3R0b20pXG4gICAgICAgIHRoaXMuc2V0U3R5bGVQcm9wZXJ0eShcInBhZGRpbmdUb3BcIiwgdG9wKVxuICAgICAgICBcbiAgICAgICAgY29uc3QgYm91bmRzID0gdGhpcy5ib3VuZHNcbiAgICAgICAgaWYgKGJvdW5kcy5oZWlnaHQgIT0gcHJldmlvdXNCb3VuZHMuaGVpZ2h0IHx8IGJvdW5kcy53aWR0aCAhPSBwcmV2aW91c0JvdW5kcy53aWR0aCkge1xuICAgICAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dCgpXG4gICAgICAgICAgICB0aGlzLmJvdW5kc0RpZENoYW5nZSgpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNldEJvcmRlcihcbiAgICAgICAgcmFkaXVzOiBudW1iZXIgfCBzdHJpbmcgPSBuaWwsXG4gICAgICAgIHdpZHRoOiBudW1iZXIgfCBzdHJpbmcgPSAxLFxuICAgICAgICBjb2xvcjogVUlDb2xvciA9IFVJQ29sb3IuYmxhY2tDb2xvcixcbiAgICAgICAgc3R5bGU6IHN0cmluZyA9IFwic29saWRcIlxuICAgICkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwiYm9yZGVyU3R5bGVcIiwgc3R5bGUpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnNldFN0eWxlUHJvcGVydHkoXCJib3JkZXJSYWRpdXNcIiwgcmFkaXVzKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRTdHlsZVByb3BlcnR5KFwiYm9yZGVyQ29sb3JcIiwgY29sb3Iuc3RyaW5nVmFsdWUpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnNldFN0eWxlUHJvcGVydHkoXCJib3JkZXJXaWR0aFwiLCB3aWR0aClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNldFN0eWxlUHJvcGVydHkocHJvcGVydHlOYW1lOiBzdHJpbmcsIHZhbHVlPzogbnVtYmVyIHwgc3RyaW5nKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKElTX05JTCh2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChJU19ERUZJTkVEKHZhbHVlKSAmJiAodmFsdWUgYXMgTnVtYmVyKS5pc0FOdW1iZXIpIHtcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IFwiXCIgKyAodmFsdWUgYXMgbnVtYmVyKS5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuc3R5bGVbcHJvcGVydHlOYW1lXSA9IHZhbHVlXG4gICAgICAgICAgICBcbiAgICAgICAgfSBjYXRjaCAoZXhjZXB0aW9uKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGV4Y2VwdGlvbilcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IHVzZXJJbnRlcmFjdGlvbkVuYWJsZWQoKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSAodGhpcy5zdHlsZS5wb2ludGVyRXZlbnRzICE9IFwibm9uZVwiKVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IHVzZXJJbnRlcmFjdGlvbkVuYWJsZWQodXNlckludGVyYWN0aW9uRW5hYmxlZCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKHVzZXJJbnRlcmFjdGlvbkVuYWJsZWQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zdHlsZS5wb2ludGVyRXZlbnRzID0gXCJcIlxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3R5bGUucG9pbnRlckV2ZW50cyA9IFwibm9uZVwiXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGJhY2tncm91bmRDb2xvcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2JhY2tncm91bmRDb2xvclxuICAgIH1cbiAgICBcbiAgICBzZXQgYmFja2dyb3VuZENvbG9yKGJhY2tncm91bmRDb2xvcjogVUlDb2xvcikge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fYmFja2dyb3VuZENvbG9yID0gYmFja2dyb3VuZENvbG9yXG4gICAgICAgIFxuICAgICAgICB0aGlzLnN0eWxlLmJhY2tncm91bmRDb2xvciA9IGJhY2tncm91bmRDb2xvci5zdHJpbmdWYWx1ZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGFscGhhKCkge1xuICAgICAgICByZXR1cm4gMSAqICh0aGlzLnN0eWxlLm9wYWNpdHkgYXMgYW55KVxuICAgIH1cbiAgICBcbiAgICBzZXQgYWxwaGEoYWxwaGEpIHtcbiAgICAgICAgdGhpcy5zdHlsZS5vcGFjaXR5ID0gXCJcIiArIGFscGhhXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBhbmltYXRlVmlld09yVmlld3NXaXRoRHVyYXRpb25EZWxheUFuZEZ1bmN0aW9uKFxuICAgICAgICB2aWV3T3JWaWV3czogVUlWaWV3IHwgSFRNTEVsZW1lbnQgfCBVSVZpZXdbXSB8IEhUTUxFbGVtZW50W10sXG4gICAgICAgIGR1cmF0aW9uOiBudW1iZXIsXG4gICAgICAgIGRlbGF5OiBudW1iZXIsXG4gICAgICAgIHRpbWluZ1N0eWxlID0gXCJjdWJpYy1iZXppZXIoMC4yNSwwLjEsMC4yNSwxKVwiLFxuICAgICAgICB0cmFuc2Zvcm1GdW5jdGlvbjogRnVuY3Rpb24sXG4gICAgICAgIHRyYW5zaXRpb25jb21wbGV0aW9uRnVuY3Rpb246IEZ1bmN0aW9uXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gY2FsbFRyYW5zaXRpb25jb21wbGV0aW9uRnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgICh0cmFuc2l0aW9uY29tcGxldGlvbkZ1bmN0aW9uIHx8IG5pbCkoKTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgKHZpZXdPclZpZXdzIGFzIFVJVmlld1tdKS5mb3JFYWNoKGZ1bmN0aW9uICh2aWV3LCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB2aWV3LmFuaW1hdGlvbkRpZEZpbmlzaCgpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAoSVNfRklSRUZPWCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICAvLyBGaXJlZm94IGRvZXMgbm90IGZpcmUgdGhlIHRyYW5zaXRpb24gY29tcGxldGlvbiBldmVudCBwcm9wZXJseVxuICAgICAgICAgICAgbmV3IFVJT2JqZWN0KCkucGVyZm9ybUZ1bmN0aW9uV2l0aERlbGF5KGRlbGF5ICsgZHVyYXRpb24sIGNhbGxUcmFuc2l0aW9uY29tcGxldGlvbkZ1bmN0aW9uKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgaWYgKCEodmlld09yVmlld3MgaW5zdGFuY2VvZiBBcnJheSkpIHtcbiAgICAgICAgICAgIHZpZXdPclZpZXdzID0gW3ZpZXdPclZpZXdzXSBhcyBhbnlcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgY29uc3QgdHJhbnNpdGlvblN0eWxlcyA9IFtdXG4gICAgICAgIGNvbnN0IHRyYW5zaXRpb25EdXJhdGlvbnMgPSBbXVxuICAgICAgICBjb25zdCB0cmFuc2l0aW9uRGVsYXlzID0gW11cbiAgICAgICAgY29uc3QgdHJhbnNpdGlvblRpbWluZ3MgPSBbXVxuICAgICAgICBcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAodmlld09yVmlld3MgYXMgYW55KS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB2YXIgdmlldyA9IHZpZXdPclZpZXdzW2ldXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh2aWV3LnZpZXdIVE1MRWxlbWVudCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHZpZXcgPSB2aWV3LnZpZXdIVE1MRWxlbWVudFxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB2aWV3LmFkZEV2ZW50TGlzdGVuZXIoXCJ0cmFuc2l0aW9uZW5kXCIsIHRyYW5zaXRpb25EaWRGaW5pc2gsIHRydWUpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRyYW5zaXRpb25TdHlsZXMucHVzaCh2aWV3LnN0eWxlLnRyYW5zaXRpb24pXG4gICAgICAgICAgICB0cmFuc2l0aW9uRHVyYXRpb25zLnB1c2godmlldy5zdHlsZS50cmFuc2l0aW9uRHVyYXRpb24pXG4gICAgICAgICAgICB0cmFuc2l0aW9uRGVsYXlzLnB1c2godmlldy5zdHlsZS50cmFuc2l0aW9uRGVsYXkpXG4gICAgICAgICAgICB0cmFuc2l0aW9uVGltaW5ncy5wdXNoKHZpZXcuc3R5bGUudHJhbnNpdGlvblRpbWluZ0Z1bmN0aW9uKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb24gPSBcImFsbFwiXG4gICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IFwiXCIgKyBkdXJhdGlvbiArIFwic1wiXG4gICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25EZWxheSA9IFwiXCIgKyBkZWxheSArIFwic1wiXG4gICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25UaW1pbmdGdW5jdGlvbiA9IHRpbWluZ1N0eWxlXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0cmFuc2Zvcm1GdW5jdGlvbigpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgY29uc3QgdHJhbnNpdGlvbk9iamVjdCA9IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXCJmaW5pc2hJbW1lZGlhdGVseVwiOiBmaW5pc2hUcmFuc2l0aW9uSW1tZWRpYXRlbHksXG4gICAgICAgICAgICBcImRpZEZpbmlzaFwiOiB0cmFuc2l0aW9uRGlkRmluaXNoTWFudWFsbHksXG4gICAgICAgICAgICBcInZpZXdzXCI6IHZpZXdPclZpZXdzLFxuICAgICAgICAgICAgXCJyZWdpc3RyYXRpb25UaW1lXCI6IERhdGUubm93KClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBmdW5jdGlvbiBmaW5pc2hUcmFuc2l0aW9uSW1tZWRpYXRlbHkoKSB7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8ICh2aWV3T3JWaWV3cyBhcyBhbnkpLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdmFyIHZpZXcgPSB2aWV3T3JWaWV3c1tpXVxuICAgICAgICAgICAgICAgIGlmICh2aWV3LnZpZXdIVE1MRWxlbWVudCkge1xuICAgICAgICAgICAgICAgICAgICB2aWV3ID0gdmlldy52aWV3SFRNTEVsZW1lbnRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmlldy5zdHlsZS50cmFuc2l0aW9uID0gXCJhbGxcIlxuICAgICAgICAgICAgICAgIHZpZXcuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gXCJcIiArIGR1cmF0aW9uICsgXCJzXCJcbiAgICAgICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25EZWxheSA9IFwiXCIgKyBkZWxheSArIFwic1wiXG4gICAgICAgICAgICAgICAgdmlldy5zdHlsZS50cmFuc2l0aW9uID0gdHJhbnNpdGlvblN0eWxlc1tpXVxuICAgICAgICAgICAgICAgIHZpZXcuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gdHJhbnNpdGlvbkR1cmF0aW9uc1tpXVxuICAgICAgICAgICAgICAgIHZpZXcuc3R5bGUudHJhbnNpdGlvbkRlbGF5ID0gdHJhbnNpdGlvbkRlbGF5c1tpXVxuICAgICAgICAgICAgICAgIHZpZXcuc3R5bGUudHJhbnNpdGlvblRpbWluZ0Z1bmN0aW9uID0gdHJhbnNpdGlvblRpbWluZ3NbaV1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gdHJhbnNpdGlvbkRpZEZpbmlzaChldmVudCkge1xuICAgICAgICAgICAgdmFyIHZpZXcgPSBldmVudC5zcmNFbGVtZW50XG4gICAgICAgICAgICBpZiAoIXZpZXcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh2aWV3LnZpZXdIVE1MRWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHZpZXcgPSB2aWV3LnZpZXdIVE1MRWxlbWVudFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmlldy5zdHlsZS50cmFuc2l0aW9uID0gdHJhbnNpdGlvblN0eWxlc1tpXVxuICAgICAgICAgICAgdmlldy5zdHlsZS50cmFuc2l0aW9uRHVyYXRpb24gPSB0cmFuc2l0aW9uRHVyYXRpb25zW2ldXG4gICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25EZWxheSA9IHRyYW5zaXRpb25EZWxheXNbaV1cbiAgICAgICAgICAgIHZpZXcuc3R5bGUudHJhbnNpdGlvblRpbWluZ0Z1bmN0aW9uID0gdHJhbnNpdGlvblRpbWluZ3NbaV1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY2FsbFRyYW5zaXRpb25jb21wbGV0aW9uRnVuY3Rpb24oKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB2aWV3LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJ0cmFuc2l0aW9uZW5kXCIsIHRyYW5zaXRpb25EaWRGaW5pc2gsIHRydWUpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gdHJhbnNpdGlvbkRpZEZpbmlzaE1hbnVhbGx5KCkge1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAodmlld09yVmlld3MgYXMgYW55KS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHZhciB2aWV3ID0gdmlld09yVmlld3NbaV1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAodmlldy52aWV3SFRNTEVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgdmlldyA9IHZpZXcudmlld0hUTUxFbGVtZW50XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHZpZXcuc3R5bGUudHJhbnNpdGlvbiA9IHRyYW5zaXRpb25TdHlsZXNbaV1cbiAgICAgICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IHRyYW5zaXRpb25EdXJhdGlvbnNbaV1cbiAgICAgICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25EZWxheSA9IHRyYW5zaXRpb25EZWxheXNbaV1cbiAgICAgICAgICAgICAgICB2aWV3LnN0eWxlLnRyYW5zaXRpb25UaW1pbmdGdW5jdGlvbiA9IHRyYW5zaXRpb25UaW1pbmdzW2ldXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmlldy5yZW1vdmVFdmVudExpc3RlbmVyKFwidHJhbnNpdGlvbmVuZFwiLCB0cmFuc2l0aW9uRGlkRmluaXNoLCB0cnVlKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdHJhbnNpdGlvbk9iamVjdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgYW5pbWF0aW9uRGlkRmluaXNoKCkge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgX3RyYW5zZm9ybUF0dHJpYnV0ZSA9ICgoXCJ0cmFuc2Zvcm1cIiBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUpID8gXCJ0cmFuc2Zvcm1cIiA6IHVuZGVmaW5lZCkgfHxcbiAgICAgICAgKChcIi13ZWJraXQtdHJhbnNmb3JtXCIgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlKSA/IFwiLXdlYmtpdC10cmFuc2Zvcm1cIiA6IFwidW5kZWZpbmVkXCIpIHx8XG4gICAgICAgICgoXCItbW96LXRyYW5zZm9ybVwiIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZSkgPyBcIi1tb3otdHJhbnNmb3JtXCIgOiBcInVuZGVmaW5lZFwiKSB8fFxuICAgICAgICAoKFwiLW1zLXRyYW5zZm9ybVwiIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZSkgPyBcIi1tcy10cmFuc2Zvcm1cIiA6IFwidW5kZWZpbmVkXCIpIHx8XG4gICAgICAgICgoXCItby10cmFuc2Zvcm1cIiBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUpID8gXCItby10cmFuc2Zvcm1cIiA6IFwidW5kZWZpbmVkXCIpXG4gICAgXG4gICAgc3RhdGljIF9zZXRBYnNvbHV0ZVNpemVBbmRQb3NpdGlvbihlbGVtZW50LCBsZWZ0LCB0b3AsIHdpZHRoLCBoZWlnaHQsIHpJbmRleCA9IDApIHtcbiAgICAgICAgXG4gICAgICAgIC8vIGlmICghVUlWaWV3Ll90cmFuc2Zvcm1BdHRyaWJ1dGUpIHtcbiAgICAgICAgXG4gICAgICAgIC8vICAgICBVSVZpZXcuX3RyYW5zZm9ybUF0dHJpYnV0ZSA9ICgoJ3RyYW5zZm9ybScgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlKSA/ICd0cmFuc2Zvcm0nIDogdW5kZWZpbmVkKTtcbiAgICAgICAgLy8gICAgIFVJVmlldy5fdHJhbnNmb3JtQXR0cmlidXRlID0gVUlWaWV3Ll90cmFuc2Zvcm1BdHRyaWJ1dGUgfHwgKCgnLXdlYmtpdC10cmFuc2Zvcm0nIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZSkgPyAnLXdlYmtpdC10cmFuc2Zvcm0nIDogJ3VuZGVmaW5lZCcpO1xuICAgICAgICAvLyAgICAgVUlWaWV3Ll90cmFuc2Zvcm1BdHRyaWJ1dGUgPSBVSVZpZXcuX3RyYW5zZm9ybUF0dHJpYnV0ZSB8fCAoKCctbW96LXRyYW5zZm9ybScgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlKSA/ICctbW96LXRyYW5zZm9ybScgOiAndW5kZWZpbmVkJyk7XG4gICAgICAgIC8vICAgICBVSVZpZXcuX3RyYW5zZm9ybUF0dHJpYnV0ZSA9IFVJVmlldy5fdHJhbnNmb3JtQXR0cmlidXRlIHx8ICgoJy1tcy10cmFuc2Zvcm0nIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZSkgPyAnLW1zLXRyYW5zZm9ybScgOiAndW5kZWZpbmVkJyk7XG4gICAgICAgIC8vICAgICBVSVZpZXcuX3RyYW5zZm9ybUF0dHJpYnV0ZSA9IFVJVmlldy5fdHJhbnNmb3JtQXR0cmlidXRlIHx8ICgoJy1vLXRyYW5zZm9ybScgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlKSA/ICctby10cmFuc2Zvcm0nIDogJ3VuZGVmaW5lZCcpO1xuICAgICAgICBcbiAgICAgICAgLy8gfVxuICAgICAgICBcbiAgICAgICAgaWYgKCFJUyhlbGVtZW50KSB8fCAhZWxlbWVudC5vYmV5QXV0b2xheW91dCAmJiAhZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJvYmV5QXV0b2xheW91dFwiKSkge1xuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChlbGVtZW50LmlkID09IFwibWFpblZpZXdcIikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciBhc2QgPSAxXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKElTKGhlaWdodCkpIHtcbiAgICAgICAgICAgIGhlaWdodCA9IGhlaWdodC5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKElTKHdpZHRoKSkge1xuICAgICAgICAgICAgd2lkdGggPSB3aWR0aC5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdmFyIHN0ciA9IGVsZW1lbnQuc3R5bGUuY3NzVGV4dFxuICAgICAgICBcbiAgICAgICAgY29uc3QgZnJhbWVUcmFuc2Zvcm0gPSBVSVZpZXcuX3RyYW5zZm9ybUF0dHJpYnV0ZSArIFwiOiB0cmFuc2xhdGUzZChcIiArICgxICogbGVmdCkuaW50ZWdlclZhbHVlICsgXCJweCwgXCIgK1xuICAgICAgICAgICAgKDEgKiB0b3ApLmludGVnZXJWYWx1ZSArIFwicHgsIFwiICsgekluZGV4LmludGVnZXJWYWx1ZSArIFwicHgpXCJcbiAgICAgICAgXG4gICAgICAgIGlmIChlbGVtZW50LlVJVmlldykge1xuICAgICAgICAgICAgc3RyID0gc3RyICsgZnJhbWVUcmFuc2Zvcm0gKyBcIjtcIlxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgZWxlbWVudC5VSVZpZXcuX2ZyYW1lVHJhbnNmb3JtID0gZnJhbWVUcmFuc2Zvcm1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKGhlaWdodCA9PSBuaWwpIHtcbiAgICAgICAgICAgIHN0ciA9IHN0ciArIFwiIGhlaWdodDogdW5zZXQ7XCJcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHN0ciA9IHN0ciArIFwiIGhlaWdodDpcIiArIGhlaWdodCArIFwiO1wiXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmICh3aWR0aCA9PSBuaWwpIHtcbiAgICAgICAgICAgIHN0ciA9IHN0ciArIFwiIHdpZHRoOiB1bnNldDtcIlxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgc3RyID0gc3RyICsgXCIgd2lkdGg6XCIgKyB3aWR0aCArIFwiO1wiXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChlbGVtZW50LmlkID09IFwibWFpblZpZXdcIikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciBhc2QgPSAxXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZWxlbWVudC5zdHlsZS5jc3NUZXh0ID0gZWxlbWVudC5zdHlsZS5jc3NUZXh0ICsgc3RyXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgcGVyZm9ybUF1dG9MYXlvdXQocGFyZW50RWxlbWVudCwgdmlzdWFsRm9ybWF0QXJyYXksIGNvbnN0cmFpbnRzQXJyYXkpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBjb25zdCB2aWV3ID0gbmV3IEF1dG9MYXlvdXQuVmlldygpXG4gICAgICAgIFxuICAgICAgICBpZiAoSVModmlzdWFsRm9ybWF0QXJyYXkpICYmIElTKHZpc3VhbEZvcm1hdEFycmF5Lmxlbmd0aCkpIHtcbiAgICAgICAgICAgIHZpZXcuYWRkQ29uc3RyYWludHMoQXV0b0xheW91dC5WaXN1YWxGb3JtYXQucGFyc2UodmlzdWFsRm9ybWF0QXJyYXksIHsgZXh0ZW5kZWQ6IHRydWUgfSkpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChJUyhjb25zdHJhaW50c0FycmF5KSAmJiBJUyhjb25zdHJhaW50c0FycmF5Lmxlbmd0aCkpIHtcbiAgICAgICAgICAgIHZpZXcuYWRkQ29uc3RyYWludHMoY29uc3RyYWludHNBcnJheSlcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgY29uc3QgZWxlbWVudHMgPSB7fVxuICAgICAgICBmb3IgKHZhciBrZXkgaW4gdmlldy5zdWJWaWV3cykge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoIXZpZXcuc3ViVmlld3MuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gbmlsXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgZWxlbWVudCA9IHBhcmVudEVsZW1lbnQucXVlcnlTZWxlY3RvcihcIiNcIiArIGtleSlcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkVycm9yIG9jY3VycmVkIFwiICsgZXJyb3IpO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoZWxlbWVudCAmJiAhZWxlbWVudC5vYmV5QXV0b2xheW91dCAmJiAhZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJvYmV5QXV0b2xheW91dFwiKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGVsZW1lbnQuY2xhc3NOYW1lICs9IGVsZW1lbnQuY2xhc3NOYW1lID8gXCIgYWJzXCIgOiBcImFic1wiXG4gICAgICAgICAgICAgICAgZWxlbWVudHNba2V5XSA9IGVsZW1lbnRcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB2YXIgcGFyZW50VUlWaWV3ID0gbmlsXG4gICAgICAgIFxuICAgICAgICBpZiAocGFyZW50RWxlbWVudC5VSVZpZXcpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGFyZW50VUlWaWV3ID0gcGFyZW50RWxlbWVudC5VSVZpZXdcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCB1cGRhdGVMYXlvdXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2aWV3LnNldFNpemUoXG4gICAgICAgICAgICAgICAgcGFyZW50RWxlbWVudCA/IHBhcmVudEVsZW1lbnQuY2xpZW50V2lkdGggOiB3aW5kb3cuaW5uZXJXaWR0aCxcbiAgICAgICAgICAgICAgICBwYXJlbnRFbGVtZW50ID8gcGFyZW50RWxlbWVudC5jbGllbnRIZWlnaHQgOiB3aW5kb3cuaW5uZXJIZWlnaHRcbiAgICAgICAgICAgIClcbiAgICAgICAgICAgIGZvciAoa2V5IGluIHZpZXcuc3ViVmlld3MpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAoIXZpZXcuc3ViVmlld3MuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgICAgICAgICBjb250aW51ZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBjb25zdCBzdWJWaWV3ID0gdmlldy5zdWJWaWV3c1trZXldXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnRzW2tleV0pIHtcbiAgICAgICAgICAgICAgICAgICAgVUlWaWV3Ll9zZXRBYnNvbHV0ZVNpemVBbmRQb3NpdGlvbihcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzW2tleV0sXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWJWaWV3LmxlZnQsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWJWaWV3LnRvcCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1YlZpZXcud2lkdGgsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWJWaWV3LmhlaWdodFxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBwYXJlbnRVSVZpZXcuZGlkTGF5b3V0U3Vidmlld3MoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHVwZGF0ZUxheW91dCgpXG4gICAgICAgIHJldHVybiB1cGRhdGVMYXlvdXRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBydW5GdW5jdGlvbkJlZm9yZU5leHRGcmFtZShzdGVwOiAoKSA9PiB2b2lkKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoSVNfU0FGQVJJKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIFRoaXMgY3JlYXRlcyBhIG1pY3JvdGFza1xuICAgICAgICAgICAgUHJvbWlzZS5yZXNvbHZlKCkudGhlbihzdGVwKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoc3RlcClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzdGF0aWMgc2NoZWR1bGVMYXlvdXRWaWV3c0lmTmVlZGVkKCkge1xuICAgICAgICBcbiAgICAgICAgVUlWaWV3LnJ1bkZ1bmN0aW9uQmVmb3JlTmV4dEZyYW1lKFVJVmlldy5sYXlvdXRWaWV3c0lmTmVlZGVkKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc3RhdGljIGxheW91dFZpZXdzSWZOZWVkZWQoKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgVUlWaWV3Ll92aWV3c1RvTGF5b3V0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCB2aWV3ID0gVUlWaWV3Ll92aWV3c1RvTGF5b3V0W2ldXG4gICAgICAgICAgICB2aWV3LmxheW91dElmTmVlZGVkKClcbiAgICAgICAgfVxuICAgICAgICBVSVZpZXcuX3ZpZXdzVG9MYXlvdXQgPSBbXVxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzZXROZWVkc0xheW91dCgpIHtcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl9zaG91bGRMYXlvdXQpIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zaG91bGRMYXlvdXQgPSBZRVNcbiAgICAgICAgXG4gICAgICAgIC8vIFJlZ2lzdGVyIHZpZXcgZm9yIGxheW91dCBiZWZvcmUgbmV4dCBmcmFtZVxuICAgICAgICBVSVZpZXcuX3ZpZXdzVG9MYXlvdXQucHVzaCh0aGlzKVxuICAgICAgICBcbiAgICAgICAgaWYgKFVJVmlldy5fdmlld3NUb0xheW91dC5sZW5ndGggPT0gMSkge1xuICAgICAgICAgICAgVUlWaWV3LnNjaGVkdWxlTGF5b3V0Vmlld3NJZk5lZWRlZCgpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBuZWVkc0xheW91dCgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLl9zaG91bGRMYXlvdXRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGxheW91dElmTmVlZGVkKCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKCF0aGlzLl9zaG91bGRMYXlvdXQpIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zaG91bGRMYXlvdXQgPSBOT1xuICAgICAgICBcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5sYXlvdXRTdWJ2aWV3cygpXG4gICAgICAgICAgICBcbiAgICAgICAgfSBjYXRjaCAoZXhjZXB0aW9uKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGV4Y2VwdGlvbilcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBsYXlvdXRTdWJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zaG91bGRMYXlvdXQgPSBOT1xuICAgICAgICBcbiAgICAgICAgLy8gQXV0b2xheW91dFxuICAgICAgICAvL3dpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLl91cGRhdGVMYXlvdXRGdW5jdGlvbik7XG4gICAgICAgIGlmICh0aGlzLmNvbnN0cmFpbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl91cGRhdGVMYXlvdXRGdW5jdGlvbiA9IFVJVmlldy5wZXJmb3JtQXV0b0xheW91dCh0aGlzLnZpZXdIVE1MRWxlbWVudCwgbnVsbCwgdGhpcy5jb25zdHJhaW50cylcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAvL3RoaXMuX3VwZGF0ZUxheW91dEZ1bmN0aW9uID0gdGhpcy5sYXlvdXRTdWJ2aWV3cy5iaW5kKHRoaXMpO1xuICAgICAgICBcbiAgICAgICAgLy93aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5fdXBkYXRlTGF5b3V0RnVuY3Rpb24pO1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fdmlld0NvbnRyb2xsZXJMYXlvdXRGdW5jdGlvbigpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmFwcGx5Q2xhc3Nlc0FuZFN0eWxlcygpXG4gICAgICAgIFxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuc3Vidmlld3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29uc3Qgc3VidmlldyA9IHRoaXMuc3Vidmlld3NbaV1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgc3Vidmlldy5jYWxjdWxhdGVBbmRTZXRWaWV3RnJhbWUoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICAvL3N1YnZpZXcubGF5b3V0U3Vidmlld3MoKTtcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLmRpZExheW91dFN1YnZpZXdzKClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGFwcGx5Q2xhc3Nlc0FuZFN0eWxlcygpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgLy92YXIgY2xhc3Nlc1N0cmluZyA9IFwiXCI7XG4gICAgICAgIFxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuc3R5bGVDbGFzc2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IHN0eWxlQ2xhc3MgPSB0aGlzLnN0eWxlQ2xhc3Nlc1tpXVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoc3R5bGVDbGFzcykge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LmNsYXNzTGlzdC5hZGQoc3R5bGVDbGFzcylcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vY2xhc3Nlc1N0cmluZyA9IGNsYXNzZXNTdHJpbmcgKyBcIiBcIiArIHN0eWxlQ2xhc3M7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy52aWV3SFRNTEVsZW1lbnQuY2xhc3NOYW1lID0gY2xhc3Nlc1N0cmluZztcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZGlkTGF5b3V0U3Vidmlld3MoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9kaWRMYXlvdXRTdWJ2aWV3c0RlbGVnYXRlRnVuY3Rpb24oKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGNvbnN0cmFpbnRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fY29uc3RyYWludHNcbiAgICB9XG4gICAgXG4gICAgc2V0IGNvbnN0cmFpbnRzKGNvbnN0cmFpbnRzKSB7XG4gICAgICAgIHRoaXMuX2NvbnN0cmFpbnRzID0gY29uc3RyYWludHNcbiAgICB9XG4gICAgXG4gICAgYWRkQ29uc3RyYWludChjb25zdHJhaW50KSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLmNvbnN0cmFpbnRzLnB1c2goY29uc3RyYWludClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGFkZENvbnN0cmFpbnRzV2l0aFZpc3VhbEZvcm1hdCh2aXN1YWxGb3JtYXRBcnJheSkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jb25zdHJhaW50cyA9IHRoaXMuY29uc3RyYWludHMuY29uY2F0KEF1dG9MYXlvdXQuVmlzdWFsRm9ybWF0LnBhcnNlKFxuICAgICAgICAgICAgdmlzdWFsRm9ybWF0QXJyYXksXG4gICAgICAgICAgICB7IGV4dGVuZGVkOiB0cnVlIH1cbiAgICAgICAgKSlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBjb25zdHJhaW50V2l0aFZpZXcodmlldywgYXR0cmlidXRlLCByZWxhdGlvbiwgdG9WaWV3LCB0b0F0dHJpYnV0ZSwgbXVsdGlwbGllciwgY29uc3RhbnQsIHByaW9yaXR5KSB7XG4gICAgICAgIFxuICAgICAgICB2YXIgVUlWaWV3T2JqZWN0ID0gbmlsXG4gICAgICAgIHZhciB2aWV3SUQgPSBudWxsXG4gICAgICAgIGlmICh2aWV3KSB7XG4gICAgICAgICAgICBpZiAodmlldy5pc0tpbmRPZkNsYXNzICYmIHZpZXcuaXNLaW5kT2ZDbGFzcyhVSVZpZXcpKSB7XG4gICAgICAgICAgICAgICAgVUlWaWV3T2JqZWN0ID0gdmlld1xuICAgICAgICAgICAgICAgIHZpZXcgPSB2aWV3LnZpZXdIVE1MRWxlbWVudFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmlld0lEID0gdmlldy5pZFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB2YXIgdG9VSVZpZXdPYmplY3QgPSBuaWxcbiAgICAgICAgdmFyIHRvVmlld0lEID0gbnVsbFxuICAgICAgICBpZiAodG9WaWV3KSB7XG4gICAgICAgICAgICBpZiAodG9WaWV3LmlzS2luZE9mQ2xhc3MgJiYgdmlldy5pc0tpbmRPZkNsYXNzKFVJVmlldykpIHtcbiAgICAgICAgICAgICAgICB0b1VJVmlld09iamVjdCA9IHRvVmlld1xuICAgICAgICAgICAgICAgIHRvVmlldyA9IHRvVmlldy52aWV3SFRNTEVsZW1lbnRcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRvVmlld0lEID0gdG9WaWV3LmlkXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGNvbnN0cmFpbnQgPSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZpZXcxOiB2aWV3SUQsXG4gICAgICAgICAgICBhdHRyMTogYXR0cmlidXRlLFxuICAgICAgICAgICAgcmVsYXRpb246IHJlbGF0aW9uLFxuICAgICAgICAgICAgdmlldzI6IHRvVmlld0lELFxuICAgICAgICAgICAgYXR0cjI6IHRvQXR0cmlidXRlLFxuICAgICAgICAgICAgbXVsdGlwbGllcjogbXVsdGlwbGllcixcbiAgICAgICAgICAgIGNvbnN0YW50OiBjb25zdGFudCxcbiAgICAgICAgICAgIHByaW9yaXR5OiBwcmlvcml0eVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHJldHVybiBjb25zdHJhaW50XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzdGF0aWMgY29uc3RyYWludEF0dHJpYnV0ZSA9IHtcbiAgICAgICAgXG4gICAgICAgIFwibGVmdFwiOiBBdXRvTGF5b3V0LkF0dHJpYnV0ZS5MRUZULFxuICAgICAgICBcInJpZ2h0XCI6IEF1dG9MYXlvdXQuQXR0cmlidXRlLlJJR0hULFxuICAgICAgICBcImJvdHRvbVwiOiBBdXRvTGF5b3V0LkF0dHJpYnV0ZS5CT1RUT00sXG4gICAgICAgIFwidG9wXCI6IEF1dG9MYXlvdXQuQXR0cmlidXRlLlRPUCxcbiAgICAgICAgXCJjZW50ZXJYXCI6IEF1dG9MYXlvdXQuQXR0cmlidXRlLkNFTlRFUlgsXG4gICAgICAgIFwiY2VudGVyWVwiOiBBdXRvTGF5b3V0LkF0dHJpYnV0ZS5DRU5URVJZLFxuICAgICAgICBcImhlaWdodFwiOiBBdXRvTGF5b3V0LkF0dHJpYnV0ZS5IRUlHSFQsXG4gICAgICAgIFwid2lkdGhcIjogQXV0b0xheW91dC5BdHRyaWJ1dGUuV0lEVEgsXG4gICAgICAgIFwiekluZGV4XCI6IEF1dG9MYXlvdXQuQXR0cmlidXRlLlpJTkRFWCxcbiAgICAgICAgLy8gTm90IHN1cmUgd2hhdCB0aGVzZSBhcmUgZm9yXG4gICAgICAgIFwiY29uc3RhbnRcIjogQXV0b0xheW91dC5BdHRyaWJ1dGUuTk9UQU5BVFRSSUJVVEUsXG4gICAgICAgIFwidmFyaWFibGVcIjogQXV0b0xheW91dC5BdHRyaWJ1dGUuVkFSSUFCTEVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBjb25zdHJhaW50UmVsYXRpb24gPSB7XG4gICAgICAgIFxuICAgICAgICBcImVxdWFsXCI6IEF1dG9MYXlvdXQuUmVsYXRpb24uRVFVLFxuICAgICAgICBcImxlc3NUaGFuT3JFcXVhbFwiOiBBdXRvTGF5b3V0LlJlbGF0aW9uLkxFUSxcbiAgICAgICAgXCJncmVhdGVyVGhhbk9yRXF1YWxcIjogQXV0b0xheW91dC5SZWxhdGlvbi5HRVFcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHN1YnZpZXdXaXRoSUQodmlld0lEKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdmFyIHJlc3VsdEhUTUxFbGVtZW50ID0gbmlsXG4gICAgICAgIFxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXN1bHRIVE1MRWxlbWVudCA9IHRoaXMudmlld0hUTUxFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjXCIgKyB2aWV3SUQpXG4gICAgICAgICAgICBcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChyZXN1bHRIVE1MRWxlbWVudCAmJiByZXN1bHRIVE1MRWxlbWVudC5VSVZpZXcpIHtcbiAgICAgICAgICAgIHJldHVybiByZXN1bHRIVE1MRWxlbWVudC5VSVZpZXdcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbmlsXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHJlY3RhbmdsZUNvbnRhaW5pbmdTdWJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGNlbnRlciA9IHRoaXMuYm91bmRzLmNlbnRlclxuICAgICAgICBcbiAgICAgICAgdmFyIHJlc3VsdCA9IG5ldyBVSVJlY3RhbmdsZShjZW50ZXIueCwgY2VudGVyLnksIDAsIDApXG4gICAgICAgIFxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuc3Vidmlld3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29uc3Qgc3VidmlldyA9IHRoaXMuc3Vidmlld3NbaV1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmFyIGZyYW1lID0gc3Vidmlldy5mcmFtZVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCByZWN0YW5nbGVDb250YWluaW5nU3Vidmlld3MgPSBzdWJ2aWV3LnJlY3RhbmdsZUNvbnRhaW5pbmdTdWJ2aWV3cygpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGZyYW1lID0gZnJhbWUuY29uY2F0ZW5hdGVXaXRoUmVjdGFuZ2xlKHJlY3RhbmdsZUNvbnRhaW5pbmdTdWJ2aWV3cylcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0LmNvbmNhdGVuYXRlV2l0aFJlY3RhbmdsZShmcmFtZSlcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBoYXNTdWJ2aWV3KHZpZXc6IFVJVmlldykge1xuICAgICAgICBcbiAgICAgICAgLy8gVGhpcyBpcyBmb3IgcGVyZm9ybWFuY2UgcmVhc29uc1xuICAgICAgICBpZiAoIUlTKHZpZXcpKSB7XG4gICAgICAgICAgICByZXR1cm4gTk9cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnN1YnZpZXdzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCBzdWJ2aWV3ID0gdGhpcy5zdWJ2aWV3c1tpXVxuICAgICAgICAgICAgaWYgKHN1YnZpZXcgPT0gdmlldykge1xuICAgICAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gTk9cbiAgICB9XG4gICAgXG4gICAgZ2V0IHZpZXdCZWxvd1RoaXNWaWV3KCkge1xuICAgICAgICBjb25zdCByZXN1bHQ6IFVJVmlldyA9ICh0aGlzLnZpZXdIVE1MRWxlbWVudC5wcmV2aW91c0VsZW1lbnRTaWJsaW5nIGFzIGFueSB8fCB7fSkuVUlWaWV3XG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgZ2V0IHZpZXdBYm92ZVRoaXNWaWV3KCkge1xuICAgICAgICBjb25zdCByZXN1bHQ6IFVJVmlldyA9ICh0aGlzLnZpZXdIVE1MRWxlbWVudC5uZXh0RWxlbWVudFNpYmxpbmcgYXMgYW55IHx8IHt9KS5VSVZpZXdcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBhZGRTdWJ2aWV3KHZpZXc6IFVJVmlldywgYWJvdmVWaWV3PzogVUlWaWV3KSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoIXRoaXMuaGFzU3Vidmlldyh2aWV3KSAmJiBJUyh2aWV3KSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB2aWV3LndpbGxNb3ZlVG9TdXBlcnZpZXcodGhpcylcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKElTKGFib3ZlVmlldykpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdIVE1MRWxlbWVudC5pbnNlcnRCZWZvcmUodmlldy52aWV3SFRNTEVsZW1lbnQsIGFib3ZlVmlldy52aWV3SFRNTEVsZW1lbnQubmV4dFNpYmxpbmcpXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJ2aWV3cy5pbnNlcnRFbGVtZW50QXRJbmRleCh0aGlzLnN1YnZpZXdzLmluZGV4T2YoYWJvdmVWaWV3KSwgdmlldylcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LmFwcGVuZENoaWxkKHZpZXcudmlld0hUTUxFbGVtZW50KVxuICAgICAgICAgICAgICAgIHRoaXMuc3Vidmlld3MucHVzaCh2aWV3KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmlldy5kaWRNb3ZlVG9TdXBlcnZpZXcodGhpcylcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKHRoaXMuc3VwZXJ2aWV3ICYmIHRoaXMuaXNNZW1iZXJPZlZpZXdUcmVlKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmlldy5icm9hZGNhc3RFdmVudEluU3VidHJlZSh7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBVSVZpZXcuYnJvYWRjYXN0RXZlbnROYW1lLkFkZGVkVG9WaWV3VHJlZSxcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1ldGVyczogbmlsXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc2V0TmVlZHNMYXlvdXQoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGFkZFN1YnZpZXdzKHZpZXdzOiBVSVZpZXdbXSkge1xuICAgICAgICB2aWV3cy5mb3JFYWNoKGZ1bmN0aW9uICh0aGlzOiBVSVZpZXcsIHZpZXc6IFVJVmlldywgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgICAgICB0aGlzLmFkZFN1YnZpZXcodmlldylcbiAgICAgICAgfSwgdGhpcylcbiAgICB9XG4gICAgXG4gICAgXG4gICAgbW92ZVRvQm90dG9tT2ZTdXBlcnZpZXcoKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoSVModGhpcy5zdXBlcnZpZXcpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IGJvdHRvbVZpZXcgPSB0aGlzLnN1cGVydmlldy5zdWJ2aWV3cy5maXJzdEVsZW1lbnRcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGJvdHRvbVZpZXcgPT0gdGhpcykge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnN1cGVydmlldy5zdWJ2aWV3cy5yZW1vdmVFbGVtZW50KHRoaXMpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3VwZXJ2aWV3LnN1YnZpZXdzLmluc2VydEVsZW1lbnRBdEluZGV4KDAsIHRoaXMpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3VwZXJ2aWV3LnZpZXdIVE1MRWxlbWVudC5pbnNlcnRCZWZvcmUodGhpcy52aWV3SFRNTEVsZW1lbnQsIGJvdHRvbVZpZXcudmlld0hUTUxFbGVtZW50KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBtb3ZlVG9Ub3BPZlN1cGVydmlldygpIHtcbiAgICAgICAgXG4gICAgICAgIGlmIChJUyh0aGlzLnN1cGVydmlldykpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29uc3QgdG9wVmlldyA9IHRoaXMuc3VwZXJ2aWV3LnN1YnZpZXdzLmxhc3RFbGVtZW50XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0b3BWaWV3ID09IHRoaXMpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zdXBlcnZpZXcuc3Vidmlld3MucmVtb3ZlRWxlbWVudCh0aGlzKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnN1cGVydmlldy5zdWJ2aWV3cy5wdXNoKHRoaXMpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3VwZXJ2aWV3LnZpZXdIVE1MRWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLnZpZXdIVE1MRWxlbWVudClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgcmVtb3ZlRnJvbVN1cGVydmlldygpIHtcbiAgICAgICAgaWYgKElTKHRoaXMuc3VwZXJ2aWV3KSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmZvckVhY2hWaWV3SW5TdWJ0cmVlKGZ1bmN0aW9uICh2aWV3KSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmlldy5ibHVyKClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5zdXBlcnZpZXcuc3Vidmlld3MuaW5kZXhPZih0aGlzKVxuICAgICAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnN1cGVydmlldy5zdWJ2aWV3cy5zcGxpY2UoaW5kZXgsIDEpXG4gICAgICAgICAgICAgICAgdGhpcy5zdXBlcnZpZXcudmlld0hUTUxFbGVtZW50LnJlbW92ZUNoaWxkKHRoaXMudmlld0hUTUxFbGVtZW50KVxuICAgICAgICAgICAgICAgIHRoaXMuc3VwZXJ2aWV3ID0gbmlsXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5icm9hZGNhc3RFdmVudEluU3VidHJlZSh7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBVSVZpZXcuYnJvYWRjYXN0RXZlbnROYW1lLlJlbW92ZWRGcm9tVmlld1RyZWUsXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtZXRlcnM6IG5pbFxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIFxuICAgIHdpbGxBcHBlYXIoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHdpbGxNb3ZlVG9TdXBlcnZpZXcoc3VwZXJ2aWV3OiBVSVZpZXcpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3NldElubmVySFRNTEZyb21LZXlJZlBvc3NpYmxlKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3NldElubmVySFRNTEZyb21Mb2NhbGl6ZWRUZXh0T2JqZWN0SWZQb3NzaWJsZSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBkaWRNb3ZlVG9TdXBlcnZpZXcoc3VwZXJ2aWV3OiBVSVZpZXcpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3VwZXJ2aWV3ID0gc3VwZXJ2aWV3XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB3YXNBZGRlZFRvVmlld1RyZWUoKSB7XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB3YXNSZW1vdmVkRnJvbVZpZXdUcmVlKCkge1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGlzTWVtYmVyT2ZWaWV3VHJlZSgpIHtcbiAgICAgICAgdmFyIGVsZW1lbnQgPSB0aGlzLnZpZXdIVE1MRWxlbWVudFxuICAgICAgICBmb3IgKHZhciBpID0gMDsgZWxlbWVudDsgaSA9IGkpIHtcbiAgICAgICAgICAgIGlmIChlbGVtZW50LnBhcmVudEVsZW1lbnQgJiYgZWxlbWVudC5wYXJlbnRFbGVtZW50ID09IGRvY3VtZW50LmJvZHkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gWUVTXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbGVtZW50ID0gZWxlbWVudC5wYXJlbnRFbGVtZW50XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIE5PXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBhbGxTdXBlcnZpZXdzKCkge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBbXVxuICAgICAgICB2YXIgdmlldzogVUlWaWV3ID0gdGhpc1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgSVModmlldyk7IGkgPSBpKSB7XG4gICAgICAgICAgICByZXN1bHQucHVzaCh2aWV3KVxuICAgICAgICAgICAgdmlldyA9IHZpZXcuc3VwZXJ2aWV3XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzZXROZWVkc0xheW91dE9uQWxsU3VwZXJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYWxsU3VwZXJ2aWV3cy5yZXZlcnNlKCkuZm9yRWFjaChmdW5jdGlvbiAodmlldzogVUlWaWV3LCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmlldy5zZXROZWVkc0xheW91dCgpXG4gICAgICAgICAgICBcbiAgICAgICAgfSlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHNldE5lZWRzTGF5b3V0VXBUb1Jvb3RWaWV3KCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dE9uQWxsU3VwZXJ2aWV3cygpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGZvY3VzKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQuZm9jdXMoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgYmx1cigpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LmJsdXIoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgX2xvYWRVSUV2ZW50cygpIHtcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy52aWV3SFRNTEVsZW1lbnQgPSBuaWw7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBpc1RvdWNoRXZlbnRDbGFzc0RlZmluZWQ6IGJvb2xlYW4gPSBOTyB8fCAod2luZG93IGFzIGFueSkuVG91Y2hFdmVudFxuICAgICAgICBcbiAgICAgICAgY29uc3QgcGF1c2VFdmVudCA9IChldmVudDogRXZlbnQsIGZvcmNlZCA9IE5PKSA9PiB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0aGlzLnBhdXNlc1BvaW50ZXJFdmVudHMgfHwgZm9yY2VkKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50LnN0b3BQcm9wYWdhdGlvbikge1xuICAgICAgICAgICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQucHJldmVudERlZmF1bHQpIHtcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBldmVudC5jYW5jZWxCdWJibGUgPSB0cnVlXG4gICAgICAgICAgICAgICAgZXZlbnQucmV0dXJuVmFsdWUgPSBmYWxzZVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoZXZlbnQuc3RvcFByb3BhZ2F0aW9uICYmIHRoaXMuc3RvcHNQb2ludGVyRXZlbnRQcm9wYWdhdGlvbikge1xuICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgY29uc3Qgb25Nb3VzZURvd24gPSAoZXZlbnQpID0+IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKCh0aGlzLmlnbm9yZXNUb3VjaGVzICYmIGlzVG91Y2hFdmVudENsYXNzRGVmaW5lZCAmJiBldmVudCBpbnN0YW5jZW9mIFRvdWNoRXZlbnQpIHx8XG4gICAgICAgICAgICAgICAgKCh0aGlzLmlnbm9yZXNNb3VzZSB8fCAoSVModGhpcy5fdG91Y2hFdmVudFRpbWUpICYmIChEYXRlLm5vdygpIC0gdGhpcy5fdG91Y2hFdmVudFRpbWUpID4gNTAwKSkgJiZcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQgaW5zdGFuY2VvZiBNb3VzZUV2ZW50KSkge1xuICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyRG93biwgZXZlbnQpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX2lzUG9pbnRlckluc2lkZSA9IFlFU1xuICAgICAgICAgICAgdGhpcy5faXNQb2ludGVyVmFsaWQgPSBZRVNcbiAgICAgICAgICAgIHRoaXMuX2luaXRpYWxQb2ludGVyUG9zaXRpb24gPSBuZXcgVUlQb2ludChldmVudC5jbGllbnRYLCBldmVudC5jbGllbnRZKVxuICAgICAgICAgICAgaWYgKGlzVG91Y2hFdmVudENsYXNzRGVmaW5lZCAmJiBldmVudCBpbnN0YW5jZW9mIFRvdWNoRXZlbnQpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl90b3VjaEV2ZW50VGltZSA9IERhdGUubm93KClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl9pbml0aWFsUG9pbnRlclBvc2l0aW9uID0gbmV3IFVJUG9pbnQoZXZlbnQudG91Y2hlc1swXS5jbGllbnRYLCBldmVudC50b3VjaGVzWzBdLmNsaWVudFkpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50LnRvdWNoZXMubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgb25Ub3VjaENhbmNlbChldmVudClcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl90b3VjaEV2ZW50VGltZSA9IG5pbFxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHBhdXNlRXZlbnQoZXZlbnQpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5faGFzUG9pbnRlckRyYWdnZWQgPSBOT1xuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGNvbnN0IG9uVG91Y2hTdGFydCA9IG9uTW91c2VEb3duIGFzIGFueVxuICAgICAgICBcbiAgICAgICAgY29uc3Qgb25tb3VzZXVwID0gKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICghdGhpcy5faXNQb2ludGVyVmFsaWQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKCh0aGlzLmlnbm9yZXNUb3VjaGVzICYmIGlzVG91Y2hFdmVudENsYXNzRGVmaW5lZCAmJiBldmVudCBpbnN0YW5jZW9mIFRvdWNoRXZlbnQpIHx8XG4gICAgICAgICAgICAgICAgKHRoaXMuaWdub3Jlc01vdXNlICYmIGV2ZW50IGluc3RhbmNlb2YgTW91c2VFdmVudCkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKHRoaXMuX2lzUG9pbnRlckluc2lkZSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIG9uUG9pbnRlclVwSW5zaWRlKGV2ZW50KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5faGFzUG9pbnRlckRyYWdnZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJUYXAsIGV2ZW50KVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIFRoaXMgaGFzIHRvIGJlIHNlbnQgYWZ0ZXIgdGhlIG1vcmUgc3BlY2lmaWMgZXZlbnQgc28gdGhhdCBVSUJ1dHRvbiBjYW4gaWdub3JlIGl0IHdoZW4gbm90IGhpZ2hsaWdodGVkXG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyVXAsIGV2ZW50KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBwYXVzZUV2ZW50KGV2ZW50KVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGNvbnN0IG9uVG91Y2hFbmQgPSBvbm1vdXNldXBcbiAgICAgICAgXG4gICAgICAgIC8vIGZ1bmN0aW9uIG9uTW91c2VFbnRlcihldmVudCkge1xuICAgICAgICBcbiAgICAgICAgLy8gICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJFbnRlciwgZXZlbnQpO1xuICAgICAgICBcbiAgICAgICAgLy8gICAgIHRoaXMuX2lzUG9pbnRlckluc2lkZSA9IFlFUztcbiAgICAgICAgXG4gICAgICAgIC8vICAgICBwYXVzZUV2ZW50KGV2ZW50KTtcbiAgICAgICAgXG4gICAgICAgIC8vIH1cbiAgICAgICAgXG4gICAgICAgIGNvbnN0IG9ubW91c2VvdXQgPSAoZXZlbnQpID0+IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKCh0aGlzLmlnbm9yZXNUb3VjaGVzICYmIGlzVG91Y2hFdmVudENsYXNzRGVmaW5lZCAmJiBldmVudCBpbnN0YW5jZW9mIFRvdWNoRXZlbnQpIHx8XG4gICAgICAgICAgICAgICAgKHRoaXMuaWdub3Jlc01vdXNlICYmIGV2ZW50IGluc3RhbmNlb2YgTW91c2VFdmVudCkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zZW5kQ29udHJvbEV2ZW50Rm9yS2V5KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlckxlYXZlLCBldmVudClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5faXNQb2ludGVySW5zaWRlID0gTk9cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGF1c2VFdmVudChldmVudClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCBvblRvdWNoTGVhdmUgPSBvbm1vdXNlb3V0XG4gICAgICAgIFxuICAgICAgICB2YXIgb25Ub3VjaENhbmNlbCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoIXRoaXMuX2lzUG9pbnRlclZhbGlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICgodGhpcy5pZ25vcmVzVG91Y2hlcyAmJiBpc1RvdWNoRXZlbnRDbGFzc0RlZmluZWQgJiYgZXZlbnQgaW5zdGFuY2VvZiBUb3VjaEV2ZW50KSB8fFxuICAgICAgICAgICAgICAgICh0aGlzLmlnbm9yZXNNb3VzZSAmJiBldmVudCBpbnN0YW5jZW9mIE1vdXNlRXZlbnQpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX2lzUG9pbnRlclZhbGlkID0gTk9cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zZW5kQ29udHJvbEV2ZW50Rm9yS2V5KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlckNhbmNlbCwgZXZlbnQpXG4gICAgICAgICAgICBcbiAgICAgICAgfS5iaW5kKHRoaXMpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBvbm1vdXNlb3ZlciA9IChldmVudCkgPT4ge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoKHRoaXMuaWdub3Jlc1RvdWNoZXMgJiYgaXNUb3VjaEV2ZW50Q2xhc3NEZWZpbmVkICYmIGV2ZW50IGluc3RhbmNlb2YgVG91Y2hFdmVudCkgfHxcbiAgICAgICAgICAgICAgICAodGhpcy5pZ25vcmVzTW91c2UgJiYgZXZlbnQgaW5zdGFuY2VvZiBNb3VzZUV2ZW50KSkge1xuICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVySG92ZXIsIGV2ZW50KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9pc1BvaW50ZXJJbnNpZGUgPSBZRVNcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5faXNQb2ludGVyVmFsaWQgPSBZRVNcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGF1c2VFdmVudChldmVudClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCBvbk1vdXNlTW92ZSA9IChldmVudCkgPT4ge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoIXRoaXMuX2lzUG9pbnRlclZhbGlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICgodGhpcy5pZ25vcmVzVG91Y2hlcyAmJiBpc1RvdWNoRXZlbnRDbGFzc0RlZmluZWQgJiYgZXZlbnQgaW5zdGFuY2VvZiBUb3VjaEV2ZW50KSB8fFxuICAgICAgICAgICAgICAgICh0aGlzLmlnbm9yZXNNb3VzZSAmJiBldmVudCBpbnN0YW5jZW9mIE1vdXNlRXZlbnQpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChJU19OT1QodGhpcy5faW5pdGlhbFBvaW50ZXJQb3NpdGlvbikpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl9pbml0aWFsUG9pbnRlclBvc2l0aW9uID0gbmV3IFVJUG9pbnQoZXZlbnQuY2xpZW50WCwgZXZlbnQuY2xpZW50WSlcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKG5ldyBVSVBvaW50KGV2ZW50LmNsaWVudFgsIGV2ZW50LmNsaWVudFkpLnRvKHRoaXMuX2luaXRpYWxQb2ludGVyUG9zaXRpb24pLmxlbmd0aCA+XG4gICAgICAgICAgICAgICAgdGhpcy5fcG9pbnRlckRyYWdUaHJlc2hvbGQpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl9oYXNQb2ludGVyRHJhZ2dlZCA9IFlFU1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJNb3ZlLCBldmVudClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGF1c2VFdmVudChldmVudClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCBvblRvdWNoTW92ZSA9IGZ1bmN0aW9uIChldmVudDogVG91Y2hFdmVudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoIXRoaXMuX2lzUG9pbnRlclZhbGlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICgodGhpcy5pZ25vcmVzVG91Y2hlcyAmJiBpc1RvdWNoRXZlbnRDbGFzc0RlZmluZWQgJiYgZXZlbnQgaW5zdGFuY2VvZiBUb3VjaEV2ZW50KSB8fFxuICAgICAgICAgICAgICAgICh0aGlzLmlnbm9yZXNNb3VzZSAmJiBldmVudCBpbnN0YW5jZW9mIE1vdXNlRXZlbnQpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChldmVudC50b3VjaGVzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBvblRvdWNoWm9vbShldmVudClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29uc3QgdG91Y2ggPSBldmVudC50b3VjaGVzWzBdXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChuZXcgVUlQb2ludCh0b3VjaC5jbGllbnRYLCB0b3VjaC5jbGllbnRZKS50byh0aGlzLl9pbml0aWFsUG9pbnRlclBvc2l0aW9uKS5sZW5ndGggPlxuICAgICAgICAgICAgICAgIHRoaXMuX3BvaW50ZXJEcmFnVGhyZXNob2xkKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5faGFzUG9pbnRlckRyYWdnZWQgPSBZRVNcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAodGhpcy5faXNQb2ludGVySW5zaWRlICYmIHRoaXMudmlld0hUTUxFbGVtZW50ICE9XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuZWxlbWVudEZyb21Qb2ludCh0b3VjaC5jbGllbnRYLCB0b3VjaC5jbGllbnRZKSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuX2lzUG9pbnRlckluc2lkZSA9IE5PXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5zZW5kQ29udHJvbEV2ZW50Rm9yS2V5KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlckxlYXZlLCBldmVudClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyTW92ZSwgZXZlbnQpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vcGF1c2VFdmVudChldmVudCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB2YXIgb25Ub3VjaFpvb20gPSBmdW5jdGlvbiBvblRvdWNoWm9vbShldmVudDogVG91Y2hFdmVudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlWaWV3LmNvbnRyb2xFdmVudC5NdWx0aXBsZVRvdWNoZXMsIGV2ZW50KVxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHZhciBvblBvaW50ZXJVcEluc2lkZSA9IChldmVudCkgPT4ge1xuICAgICAgICAgICAgcGF1c2VFdmVudChldmVudClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zZW5kQ29udHJvbEV2ZW50Rm9yS2V5KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlclVwSW5zaWRlLCBldmVudClcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gZXZlbnRLZXlJc0VudGVyKGV2ZW50KSB7XG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSAhPT0gMTMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gTk9cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gZXZlbnRLZXlJc1RhYihldmVudCkge1xuICAgICAgICAgICAgaWYgKGV2ZW50LmtleUNvZGUgIT09IDkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gTk9cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gZXZlbnRLZXlJc0VzYyhldmVudCkge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGZhbHNlXG4gICAgICAgICAgICBpZiAoXCJrZXlcIiBpbiBldmVudCkge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IChldmVudC5rZXkgPT0gXCJFc2NhcGVcIiB8fCBldmVudC5rZXkgPT0gXCJFc2NcIilcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IChldmVudC5rZXlDb2RlID09IDI3KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBmdW5jdGlvbiBldmVudEtleUlzTGVmdChldmVudCkge1xuICAgICAgICAgICAgaWYgKGV2ZW50LmtleUNvZGUgIT0gXCIzN1wiKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIE5PXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gWUVTXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGZ1bmN0aW9uIGV2ZW50S2V5SXNSaWdodChldmVudCkge1xuICAgICAgICAgICAgaWYgKGV2ZW50LmtleUNvZGUgIT0gXCIzOVwiKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIE5PXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gWUVTXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGZ1bmN0aW9uIGV2ZW50S2V5SXNEb3duKGV2ZW50KSB7XG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSAhPSBcIjQwXCIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gTk9cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gZXZlbnRLZXlJc1VwKGV2ZW50KSB7XG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSAhPSBcIjM4XCIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gTk9cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgY29uc3Qgb25LZXlEb3duID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChldmVudEtleUlzRW50ZXIoZXZlbnQpKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5zZW5kQ29udHJvbEV2ZW50Rm9yS2V5KFVJVmlldy5jb250cm9sRXZlbnQuRW50ZXJEb3duLCBldmVudClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGV2ZW50S2V5SXNFc2MoZXZlbnQpKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5zZW5kQ29udHJvbEV2ZW50Rm9yS2V5KFVJVmlldy5jb250cm9sRXZlbnQuRXNjRG93biwgZXZlbnQpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChldmVudEtleUlzVGFiKGV2ZW50KSAmJiB0aGlzLl9jb250cm9sRXZlbnRUYXJnZXRzLlRhYkRvd24gJiYgdGhpcy5fY29udHJvbEV2ZW50VGFyZ2V0cy5UYWJEb3duLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LlRhYkRvd24sIGV2ZW50KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHBhdXNlRXZlbnQoZXZlbnQsIFlFUylcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGV2ZW50S2V5SXNMZWZ0KGV2ZW50KSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LkxlZnRBcnJvd0Rvd24sIGV2ZW50KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoZXZlbnRLZXlJc1JpZ2h0KGV2ZW50KSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LlJpZ2h0QXJyb3dEb3duLCBldmVudClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGV2ZW50S2V5SXNEb3duKGV2ZW50KSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LkRvd25BcnJvd0Rvd24sIGV2ZW50KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoZXZlbnRLZXlJc1VwKGV2ZW50KSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LlVwQXJyb3dEb3duLCBldmVudClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcylcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IG9uS2V5VXAgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGV2ZW50S2V5SXNFbnRlcihldmVudCkpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlWaWV3LmNvbnRyb2xFdmVudC5FbnRlclVwLCBldmVudClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcylcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBjb25zdCBvbmZvY3VzID0gZnVuY3Rpb24gKGV2ZW50OiBFdmVudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlWaWV3LmNvbnRyb2xFdmVudC5Gb2N1cywgZXZlbnQpXG4gICAgICAgICAgICBcbiAgICAgICAgfS5iaW5kKHRoaXMpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBvbmJsdXIgPSBmdW5jdGlvbiAoZXZlbnQ6IEV2ZW50KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc2VuZENvbnRyb2xFdmVudEZvcktleShVSVZpZXcuY29udHJvbEV2ZW50LkJsdXIsIGV2ZW50KVxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIC8vIE1vdXNlIGFuZCB0b3VjaCBzdGFydCBldmVudHNcbiAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50Lm9ubW91c2Vkb3duID0gb25Nb3VzZURvd24uYmluZCh0aGlzKVxuICAgICAgICB0aGlzLl92aWV3SFRNTEVsZW1lbnQub250b3VjaHN0YXJ0ID0gb25Ub3VjaFN0YXJ0LmJpbmQodGhpcylcbiAgICAgICAgLy8gdGhpcy52aWV3SFRNTEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCBvbk1vdXNlRG93bi5iaW5kKHRoaXMpLCBmYWxzZSlcbiAgICAgICAgLy8gdGhpcy52aWV3SFRNTEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsIG9uVG91Y2hTdGFydC5iaW5kKHRoaXMpLCBmYWxzZSlcbiAgICAgICAgLy8gLy90aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2VlbnRlclwiLCBvbk1vdXNlRW50ZXIuYmluZCh0aGlzKSwgZmFsc2UpO1xuICAgICAgICBcbiAgICAgICAgLy8gTW91c2UgYW5kIHRvdWNoIG1vdmUgZXZlbnRzXG4gICAgICAgIHRoaXMuX3ZpZXdIVE1MRWxlbWVudC5vbm1vdXNlbW92ZSA9IG9uTW91c2VNb3ZlLmJpbmQodGhpcylcbiAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50Lm9udG91Y2htb3ZlID0gb25Ub3VjaE1vdmUuYmluZCh0aGlzKVxuICAgICAgICAvLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIG9uTW91c2VNb3ZlLmJpbmQodGhpcyksIGZhbHNlKVxuICAgICAgICAvLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCBvblRvdWNoTW92ZS5iaW5kKHRoaXMpLCBmYWxzZSlcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy52aWV3SFRNTEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNld2hlZWxcIiwgb25tb3VzZXdoZWVsLmJpbmQodGhpcyksIGZhbHNlKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50Lm9ubW91c2VvdmVyID0gb25tb3VzZW92ZXIuYmluZCh0aGlzKVxuICAgICAgICAvLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdmVyXCIsIG9ubW91c2VvdmVyLmJpbmQodGhpcyksIGZhbHNlKVxuICAgICAgICBcbiAgICAgICAgLy8gTW91c2UgYW5kIHRvdWNoIGVuZCBldmVudHNcbiAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50Lm9ubW91c2V1cCA9IG9ubW91c2V1cC5iaW5kKHRoaXMpXG4gICAgICAgIHRoaXMuX3ZpZXdIVE1MRWxlbWVudC5vbnRvdWNoZW5kID0gb25Ub3VjaEVuZC5iaW5kKHRoaXMpXG4gICAgICAgIHRoaXMuX3ZpZXdIVE1MRWxlbWVudC5vbnRvdWNoY2FuY2VsID0gb25Ub3VjaENhbmNlbC5iaW5kKHRoaXMpXG4gICAgICAgIC8vIHRoaXMudmlld0hUTUxFbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIG9ubW91c2V1cC5iaW5kKHRoaXMpLCBmYWxzZSlcbiAgICAgICAgLy8gdGhpcy52aWV3SFRNTEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBvblRvdWNoRW5kLmJpbmQodGhpcyksIGZhbHNlKVxuICAgICAgICAvLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaGNhbmNlbCcsIG9uVG91Y2hDYW5jZWwuYmluZCh0aGlzKSwgZmFsc2UpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl92aWV3SFRNTEVsZW1lbnQub25tb3VzZW91dCA9IG9ubW91c2VvdXQuYmluZCh0aGlzKVxuICAgICAgICAvLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdXRcIiwgb25tb3VzZW91dC5iaW5kKHRoaXMpLCBmYWxzZSlcbiAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaGxlYXZlXCIsIG9uVG91Y2hMZWF2ZS5iaW5kKHRoaXMpLCBmYWxzZSlcbiAgICAgICAgXG4gICAgICAgIC8vIHRoaXMudmlld0hUTUxFbGVtZW50Lm9ua2V5ZG93biA9IG9ua2V5ZG93blxuICAgICAgICAvLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5vbmtleXVwID0gb25rZXl1cFxuICAgICAgICB0aGlzLl92aWV3SFRNTEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgb25LZXlEb3duLCBmYWxzZSlcbiAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXl1cFwiLCBvbktleVVwLCBmYWxzZSlcbiAgICAgICAgXG4gICAgICAgIC8vIEZvY3VzIGV2ZW50c1xuICAgICAgICB0aGlzLl92aWV3SFRNTEVsZW1lbnQub25mb2N1cyA9IG9uZm9jdXNcbiAgICAgICAgdGhpcy5fdmlld0hUTUxFbGVtZW50Lm9uYmx1ciA9IG9uYmx1clxuICAgICAgICAvLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiZm9jdXNcIiwgb25mb2N1cywgdHJ1ZSlcbiAgICAgICAgLy8gdGhpcy52aWV3SFRNTEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImJsdXJcIiwgb25ibHVyLCB0cnVlKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHB1YmxpYyBzdGF0aWMgY29udHJvbEV2ZW50ID0ge1xuICAgICAgICBcbiAgICAgICAgXCJQb2ludGVyRG93blwiOiBcIlBvaW50ZXJEb3duXCIsXG4gICAgICAgIFwiUG9pbnRlck1vdmVcIjogXCJQb2ludGVyTW92ZVwiLFxuICAgICAgICBcIlBvaW50ZXJMZWF2ZVwiOiBcIlBvaW50ZXJMZWF2ZVwiLFxuICAgICAgICBcIlBvaW50ZXJFbnRlclwiOiBcIlBvaW50ZXJFbnRlclwiLFxuICAgICAgICBcIlBvaW50ZXJVcEluc2lkZVwiOiBcIlBvaW50ZXJVcEluc2lkZVwiLFxuICAgICAgICBcIlBvaW50ZXJUYXBcIjogXCJQb2ludGVyVGFwXCIsXG4gICAgICAgIFwiUG9pbnRlclVwXCI6IFwiUG9pbnRlclVwXCIsXG4gICAgICAgIFwiTXVsdGlwbGVUb3VjaGVzXCI6IFwiUG9pbnRlclpvb21cIixcbiAgICAgICAgXCJQb2ludGVyQ2FuY2VsXCI6IFwiUG9pbnRlckNhbmNlbFwiLFxuICAgICAgICBcIlBvaW50ZXJIb3ZlclwiOiBcIlBvaW50ZXJIb3ZlclwiLFxuICAgICAgICBcIkVudGVyRG93blwiOiBcIkVudGVyRG93blwiLFxuICAgICAgICBcIkVudGVyVXBcIjogXCJFbnRlclVwXCIsXG4gICAgICAgIFwiRXNjRG93blwiOiBcIkVzY0Rvd25cIixcbiAgICAgICAgXCJUYWJEb3duXCI6IFwiVGFiRG93blwiLFxuICAgICAgICBcIkxlZnRBcnJvd0Rvd25cIjogXCJMZWZ0QXJyb3dEb3duXCIsXG4gICAgICAgIFwiUmlnaHRBcnJvd0Rvd25cIjogXCJSaWdodEFycm93RG93blwiLFxuICAgICAgICBcIkRvd25BcnJvd0Rvd25cIjogXCJEb3duQXJyb3dEb3duXCIsXG4gICAgICAgIFwiVXBBcnJvd0Rvd25cIjogXCJVcEFycm93RG93blwiLFxuICAgICAgICBcIkZvY3VzXCI6IFwiRm9jdXNcIixcbiAgICAgICAgXCJCbHVyXCI6IFwiQmx1clwiXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBjb250cm9sRXZlbnQgPSBVSVZpZXcuY29udHJvbEV2ZW50XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGFkZENvbnRyb2xFdmVudFRhcmdldCgpIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGV2ZW50S2V5cyA9IFtdXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0OiBVSVZpZXdBZGRDb250cm9sRXZlbnRUYXJnZXRPYmplY3Q8dHlwZW9mIFVJVmlldy5jb250cm9sRXZlbnQ+ID0gbmV3IFByb3h5KFxuICAgICAgICAgICAgKHRoaXMuY29uc3RydWN0b3IgYXMgYW55KS5jb250cm9sRXZlbnQsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgZ2V0OiAodGFyZ2V0LCBrZXksIHJlY2VpdmVyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBldmVudEtleXMucHVzaChrZXkpXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgc2V0OiAodGFyZ2V0LCBrZXksIHZhbHVlLCByZWNlaXZlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgZXZlbnRLZXlzLnB1c2goa2V5KVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRUYXJnZXRGb3JDb250cm9sRXZlbnRzKGV2ZW50S2V5cywgdmFsdWUpXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgIClcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGFkZFRhcmdldEZvckNvbnRyb2xFdmVudHMoZXZlbnRLZXlzOiBzdHJpbmdbXSwgdGFyZ2V0RnVuY3Rpb246IChzZW5kZXI6IFVJVmlldywgZXZlbnQ6IEV2ZW50KSA9PiB2b2lkKSB7XG4gICAgICAgIFxuICAgICAgICBldmVudEtleXMuZm9yRWFjaChmdW5jdGlvbiAodGhpczogVUlWaWV3LCBrZXk6IHN0cmluZywgaW5kZXg6IG51bWJlciwgYXJyYXk6IHN0cmluZ1tdKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KGtleSwgdGFyZ2V0RnVuY3Rpb24pXG4gICAgICAgICAgICBcbiAgICAgICAgfSwgdGhpcylcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGFkZFRhcmdldEZvckNvbnRyb2xFdmVudChldmVudEtleTogc3RyaW5nLCB0YXJnZXRGdW5jdGlvbjogKHNlbmRlcjogVUlWaWV3LCBldmVudDogRXZlbnQpID0+IHZvaWQpIHtcbiAgICAgICAgXG4gICAgICAgIHZhciB0YXJnZXRzID0gdGhpcy5fY29udHJvbEV2ZW50VGFyZ2V0c1tldmVudEtleV1cbiAgICAgICAgXG4gICAgICAgIGlmICghdGFyZ2V0cykge1xuICAgICAgICAgICAgLy8gQHRzLWlnbm9yZVxuICAgICAgICAgICAgdGFyZ2V0cyA9IFtdXG4gICAgICAgICAgICB0aGlzLl9jb250cm9sRXZlbnRUYXJnZXRzW2V2ZW50S2V5XSA9IHRhcmdldHNcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKHRhcmdldHMuaW5kZXhPZih0YXJnZXRGdW5jdGlvbikgPT0gLTEpIHtcbiAgICAgICAgICAgIHRhcmdldHMucHVzaCh0YXJnZXRGdW5jdGlvbilcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcmVtb3ZlVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KGV2ZW50S2V5OiBzdHJpbmcsIHRhcmdldEZ1bmN0aW9uOiAoc2VuZGVyOiBVSVZpZXcsIGV2ZW50OiBFdmVudCkgPT4gdm9pZCkge1xuICAgICAgICBjb25zdCB0YXJnZXRzID0gdGhpcy5fY29udHJvbEV2ZW50VGFyZ2V0c1tldmVudEtleV1cbiAgICAgICAgaWYgKCF0YXJnZXRzKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBjb25zdCBpbmRleCA9IHRhcmdldHMuaW5kZXhPZih0YXJnZXRGdW5jdGlvbilcbiAgICAgICAgaWYgKGluZGV4ICE9IC0xKSB7XG4gICAgICAgICAgICB0YXJnZXRzLnNwbGljZShpbmRleCwgMSlcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbiAgICByZW1vdmVUYXJnZXRGb3JDb250cm9sRXZlbnRzKGV2ZW50S2V5czogc3RyaW5nW10sIHRhcmdldEZ1bmN0aW9uOiAoc2VuZGVyOiBVSVZpZXcsIGV2ZW50OiBFdmVudCkgPT4gdm9pZCkge1xuICAgICAgICBcbiAgICAgICAgZXZlbnRLZXlzLmZvckVhY2goZnVuY3Rpb24gKGtleSwgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KGtleSwgdGFyZ2V0RnVuY3Rpb24pXG4gICAgICAgICAgICBcbiAgICAgICAgfSwgdGhpcylcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNlbmRDb250cm9sRXZlbnRGb3JLZXkoZXZlbnRLZXk6IHN0cmluZywgbmF0aXZlRXZlbnQ6IEV2ZW50KSB7XG4gICAgICAgIHZhciB0YXJnZXRzID0gdGhpcy5fY29udHJvbEV2ZW50VGFyZ2V0c1tldmVudEtleV1cbiAgICAgICAgaWYgKCF0YXJnZXRzKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICB0YXJnZXRzID0gdGFyZ2V0cy5jb3B5KClcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0YXJnZXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCB0YXJnZXQgPSB0YXJnZXRzW2ldXG4gICAgICAgICAgICB0YXJnZXQodGhpcywgbmF0aXZlRXZlbnQpXG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgc3RhdGljIGJyb2FkY2FzdEV2ZW50TmFtZSA9IHtcbiAgICAgICAgXG4gICAgICAgIFwiTGFuZ3VhZ2VDaGFuZ2VkXCI6IFwiTGFuZ3VhZ2VDaGFuZ2VkXCIsXG4gICAgICAgIFwiUmVtb3ZlZEZyb21WaWV3VHJlZVwiOiBcIlJlbW92ZWRGcm9tVmlld1RyZWVcIixcbiAgICAgICAgXCJBZGRlZFRvVmlld1RyZWVcIjogXCJBZGRlZFRvVmlld1RyZWVcIixcbiAgICAgICAgXCJQYWdlRGlkU2Nyb2xsXCI6IFwiUGFnZURpZFNjcm9sbFwiXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBicm9hZGNhc3RFdmVudEluU3VidHJlZShldmVudDogVUlWaWV3QnJvYWRjYXN0RXZlbnQpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuZm9yRWFjaFZpZXdJblN1YnRyZWUoZnVuY3Rpb24gKHZpZXcpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmlldy5kaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnQoZXZlbnQpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh2aWV3Ll9kaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnREZWxlZ2F0ZUZ1bmN0aW9uKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmlldy5fZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50RGVsZWdhdGVGdW5jdGlvbihldmVudClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGRpZFJlY2VpdmVCcm9hZGNhc3RFdmVudChldmVudDogVUlWaWV3QnJvYWRjYXN0RXZlbnQpIHtcbiAgICAgICAgXG4gICAgICAgIGlmIChldmVudC5uYW1lID09IFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuUGFnZURpZFNjcm9sbCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9pc1BvaW50ZXJWYWxpZCA9IE5PXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKGV2ZW50Lm5hbWUgPT0gVUlWaWV3LmJyb2FkY2FzdEV2ZW50TmFtZS5BZGRlZFRvVmlld1RyZWUpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy53YXNBZGRlZFRvVmlld1RyZWUoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChldmVudC5uYW1lID09IFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuUmVtb3ZlZEZyb21WaWV3VHJlZSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLndhc1JlbW92ZWRGcm9tVmlld1RyZWUoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChldmVudC5uYW1lID09IFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuTGFuZ3VhZ2VDaGFuZ2VkIHx8IGV2ZW50Lm5hbWUgPT1cbiAgICAgICAgICAgIFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuQWRkZWRUb1ZpZXdUcmVlKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX3NldElubmVySFRNTEZyb21LZXlJZlBvc3NpYmxlKClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5fc2V0SW5uZXJIVE1MRnJvbUxvY2FsaXplZFRleHRPYmplY3RJZlBvc3NpYmxlKClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGZvckVhY2hWaWV3SW5TdWJ0cmVlKGZ1bmN0aW9uVG9DYWxsOiAodmlldzogVUlWaWV3KSA9PiB2b2lkKSB7XG4gICAgICAgIFxuICAgICAgICBmdW5jdGlvblRvQ2FsbCh0aGlzKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdWJ2aWV3cy5mb3JFYWNoKGZ1bmN0aW9uIChzdWJ2aWV3LCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgc3Vidmlldy5mb3JFYWNoVmlld0luU3VidHJlZShmdW5jdGlvblRvQ2FsbClcbiAgICAgICAgICAgIFxuICAgICAgICB9KVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgcmVjdGFuZ2xlSW5WaWV3KHJlY3RhbmdsZTogVUlSZWN0YW5nbGUsIHZpZXc6IFVJVmlldykge1xuICAgICAgICBpZiAoIXZpZXcuaXNNZW1iZXJPZlZpZXdUcmVlIHx8ICF0aGlzLmlzTWVtYmVyT2ZWaWV3VHJlZSkge1xuICAgICAgICAgICAgcmV0dXJuIG5pbFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCB2aWV3Q2xpZW50UmVjdGFuZ2xlID0gdmlldy52aWV3SFRNTEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClcbiAgICAgICAgY29uc3Qgdmlld0xvY2F0aW9uID0gbmV3IFVJUG9pbnQodmlld0NsaWVudFJlY3RhbmdsZS5sZWZ0LCB2aWV3Q2xpZW50UmVjdGFuZ2xlLnRvcClcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHNlbGZDbGllbnRSZWN0YW5nbGUgPSB0aGlzLnZpZXdIVE1MRWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKVxuICAgICAgICBjb25zdCBzZWxmTG9jYXRpb24gPSBuZXcgVUlQb2ludChzZWxmQ2xpZW50UmVjdGFuZ2xlLmxlZnQsIHNlbGZDbGllbnRSZWN0YW5nbGUudG9wKVxuICAgICAgICBcbiAgICAgICAgY29uc3Qgb2Zmc2V0UG9pbnQgPSBzZWxmTG9jYXRpb24uc3VidHJhY3Qodmlld0xvY2F0aW9uKVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlY3RhbmdsZS5jb3B5KCkub2Zmc2V0QnlQb2ludChvZmZzZXRQb2ludClcbiAgICB9XG4gICAgXG4gICAgcmVjdGFuZ2xlRnJvbVZpZXcocmVjdGFuZ2xlOiBVSVJlY3RhbmdsZSwgdmlldzogVUlWaWV3KSB7XG4gICAgICAgIHJldHVybiB2aWV3LnJlY3RhbmdsZUluVmlldyhyZWN0YW5nbGUsIHRoaXMpXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGludHJpbnNpY0NvbnRlbnRTaXplV2l0aENvbnN0cmFpbnRzKGNvbnN0cmFpbmluZ0hlaWdodDogbnVtYmVyID0gMCwgY29uc3RyYWluaW5nV2lkdGg6IG51bWJlciA9IDApIHtcbiAgICAgICAgXG4gICAgICAgIC8vIFRoaXMgd29ya3MgYnV0IGlzIHNsb3dcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBVSVJlY3RhbmdsZSgwLCAwLCAwLCAwKVxuICAgICAgICBpZiAodGhpcy5yb290Vmlldy5mb3JjZUludHJpbnNpY1NpemVaZXJvKSB7XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHZhciB0ZW1wb3JhcmlseUluVmlld1RyZWUgPSBOT1xuICAgICAgICB2YXIgbm9kZUFib3ZlVGhpc1ZpZXc6IE5vZGVcbiAgICAgICAgaWYgKCF0aGlzLmlzTWVtYmVyT2ZWaWV3VHJlZSkge1xuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0aGlzLnZpZXdIVE1MRWxlbWVudClcbiAgICAgICAgICAgIHRlbXBvcmFyaWx5SW5WaWV3VHJlZSA9IFlFU1xuICAgICAgICAgICAgbm9kZUFib3ZlVGhpc1ZpZXcgPSB0aGlzLnZpZXdIVE1MRWxlbWVudC5uZXh0U2libGluZ1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCBoZWlnaHQgPSB0aGlzLnN0eWxlLmhlaWdodFxuICAgICAgICBjb25zdCB3aWR0aCA9IHRoaXMuc3R5bGUud2lkdGhcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUuaGVpZ2h0ID0gXCJcIiArIGNvbnN0cmFpbmluZ0hlaWdodFxuICAgICAgICB0aGlzLnN0eWxlLndpZHRoID0gXCJcIiArIGNvbnN0cmFpbmluZ1dpZHRoXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgY29uc3QgbGVmdCA9IHRoaXMuc3R5bGUubGVmdFxuICAgICAgICBjb25zdCByaWdodCA9IHRoaXMuc3R5bGUucmlnaHRcbiAgICAgICAgY29uc3QgYm90dG9tID0gdGhpcy5zdHlsZS5ib3R0b21cbiAgICAgICAgY29uc3QgdG9wID0gdGhpcy5zdHlsZS50b3BcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUubGVmdCA9IFwiXCJcbiAgICAgICAgdGhpcy5zdHlsZS5yaWdodCA9IFwiXCJcbiAgICAgICAgdGhpcy5zdHlsZS5ib3R0b20gPSBcIlwiXG4gICAgICAgIHRoaXMuc3R5bGUudG9wID0gXCJcIlxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdEhlaWdodCA9IHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbEhlaWdodFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHdoaXRlU3BhY2UgPSB0aGlzLnN0eWxlLndoaXRlU3BhY2VcbiAgICAgICAgdGhpcy5zdHlsZS53aGl0ZVNwYWNlID0gXCJub3dyYXBcIlxuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0V2lkdGggPSB0aGlzLnZpZXdIVE1MRWxlbWVudC5zY3JvbGxXaWR0aFxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS53aGl0ZVNwYWNlID0gd2hpdGVTcGFjZVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLnN0eWxlLmhlaWdodCA9IGhlaWdodFxuICAgICAgICB0aGlzLnN0eWxlLndpZHRoID0gd2lkdGhcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUubGVmdCA9IGxlZnRcbiAgICAgICAgdGhpcy5zdHlsZS5yaWdodCA9IHJpZ2h0XG4gICAgICAgIHRoaXMuc3R5bGUuYm90dG9tID0gYm90dG9tXG4gICAgICAgIHRoaXMuc3R5bGUudG9wID0gdG9wXG4gICAgICAgIFxuICAgICAgICBpZiAodGVtcG9yYXJpbHlJblZpZXdUcmVlKSB7XG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHRoaXMudmlld0hUTUxFbGVtZW50KVxuICAgICAgICAgICAgaWYgKHRoaXMuc3VwZXJ2aWV3KSB7XG4gICAgICAgICAgICAgICAgaWYgKG5vZGVBYm92ZVRoaXNWaWV3KSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3VwZXJ2aWV3LnZpZXdIVE1MRWxlbWVudC5pbnNlcnRCZWZvcmUodGhpcy52aWV3SFRNTEVsZW1lbnQsIG5vZGVBYm92ZVRoaXNWaWV3KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdXBlcnZpZXcudmlld0hUTUxFbGVtZW50LmFwcGVuZENoaWxkKHRoaXMudmlld0hUTUxFbGVtZW50KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmVzdWx0LmhlaWdodCA9IHJlc3VsdEhlaWdodFxuICAgICAgICByZXN1bHQud2lkdGggPSByZXN1bHRXaWR0aFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGludHJpbnNpY0NvbnRlbnRXaWR0aChjb25zdHJhaW5pbmdIZWlnaHQ6IG51bWJlciA9IDApOiBudW1iZXIge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5pbnRyaW5zaWNDb250ZW50U2l6ZVdpdGhDb25zdHJhaW50cyhjb25zdHJhaW5pbmdIZWlnaHQpLndpZHRoXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBpbnRyaW5zaWNDb250ZW50SGVpZ2h0KGNvbnN0cmFpbmluZ1dpZHRoOiBudW1iZXIgPSAwKTogbnVtYmVyIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuaW50cmluc2ljQ29udGVudFNpemVXaXRoQ29uc3RyYWludHModW5kZWZpbmVkLCBjb25zdHJhaW5pbmdXaWR0aCkuaGVpZ2h0XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgaW50cmluc2ljQ29udGVudFNpemUoKTogVUlSZWN0YW5nbGUge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIG5pbFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9VSVZpZXcudHNcIiAvPlxuLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlPYmplY3QudHNcIiAvPlxuXG5cblxuXG5cbmNsYXNzIFVJVmlld0NvbnRyb2xsZXIgZXh0ZW5kcyBVSU9iamVjdCB7XG4gICAgXG4gICAgXG4gICAgcGFyZW50Vmlld0NvbnRyb2xsZXI6IFVJVmlld0NvbnRyb2xsZXJcbiAgICBjaGlsZFZpZXdDb250cm9sbGVyczogVUlWaWV3Q29udHJvbGxlcltdXG4gICAgX1VJVmlld0NvbnRyb2xsZXJfY29uc3RydWN0b3JBcmd1bWVudHM6IHsgXCJ2aWV3XCI6IFVJVmlldzsgfVxuICAgIHN0YXRpYyByZWFkb25seSByb3V0ZUNvbXBvbmVudE5hbWU6IHN0cmluZztcbiAgICBzdGF0aWMgcmVhZG9ubHkgUGFyYW1ldGVySWRlbnRpZmllck5hbWU6IGFueTtcbiAgICBcbiAgICBcbiAgICBcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdmlldzogVUlWaWV3KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcigpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5sb2FkSW50cm9zcGVjdGlvblZhcmlhYmxlcygpXG4gICAgICAgIHRoaXMuX1VJVmlld0NvbnRyb2xsZXJfY29uc3RydWN0b3JBcmd1bWVudHMgPSB7IFwidmlld1wiOiB2aWV3IH1cbiAgICAgICAgdGhpcy5faW5pdEluc3RhbmNlVmFyaWFibGVzKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMubG9hZFN1YnZpZXdzKClcbiAgICAgICAgdGhpcy51cGRhdGVWaWV3Q29uc3RyYWludHMoKVxuICAgICAgICB0aGlzLnVwZGF0ZVZpZXdTdHlsZXMoKVxuICAgICAgICB0aGlzLl9sYXlvdXRWaWV3U3Vidmlld3MoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBsb2FkSW50cm9zcGVjdGlvblZhcmlhYmxlcygpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlWaWV3Q29udHJvbGxlclxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSU9iamVjdFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIF9pbml0SW5zdGFuY2VWYXJpYWJsZXMoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlldyA9IHRoaXMuX1VJVmlld0NvbnRyb2xsZXJfY29uc3RydWN0b3JBcmd1bWVudHMudmlld1xuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3Ll92aWV3Q29udHJvbGxlckxheW91dEZ1bmN0aW9uID0gdGhpcy5sYXlvdXRWaWV3c01hbnVhbGx5LmJpbmQodGhpcylcbiAgICAgICAgdGhpcy52aWV3Ll9kaWRMYXlvdXRTdWJ2aWV3c0RlbGVnYXRlRnVuY3Rpb24gPSB0aGlzLnZpZXdEaWRMYXlvdXRTdWJ2aWV3cy5iaW5kKHRoaXMpXG4gICAgICAgIHRoaXMudmlldy5fZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50RGVsZWdhdGVGdW5jdGlvbiA9IHRoaXMudmlld0RpZFJlY2VpdmVCcm9hZGNhc3RFdmVudC5iaW5kKHRoaXMpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmNoaWxkVmlld0NvbnRyb2xsZXJzID0gW11cbiAgICAgICAgdGhpcy5wYXJlbnRWaWV3Q29udHJvbGxlciA9IG5pbFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBoYW5kbGVSb3V0ZVJlY3Vyc2l2ZWx5KHJvdXRlOiBVSVJvdXRlKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLmhhbmRsZVJvdXRlKHJvdXRlKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5jaGlsZFZpZXdDb250cm9sbGVycy5mb3JFYWNoKGZ1bmN0aW9uIChjb250cm9sbGVyLCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKCFyb3V0ZS5pc0hhbmRsZWQpIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyLmhhbmRsZVJvdXRlUmVjdXJzaXZlbHkocm91dGUpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfSlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGFzeW5jIGhhbmRsZVJvdXRlKHJvdXRlOiBVSVJvdXRlKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGxvYWRTdWJ2aWV3cygpIHtcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICB9XG4gICAgXG4gICAgYXN5bmMgdmlld1dpbGxBcHBlYXIoKSB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGFzeW5jIHZpZXdEaWRBcHBlYXIoKSB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGFzeW5jIHZpZXdXaWxsRGlzYXBwZWFyKCkge1xuICAgIFxuICAgIFxuICAgIFxuICAgIH1cbiAgICBcbiAgICBhc3luYyB2aWV3RGlkRGlzYXBwZWFyKCkge1xuICAgIFxuICAgIFxuICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICB1cGRhdGVWaWV3Q29uc3RyYWludHMoKSB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgfVxuICAgIFxuICAgIHVwZGF0ZVZpZXdTdHlsZXMoKSB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgfVxuICAgIFxuICAgIGxheW91dFZpZXdzTWFudWFsbHkoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIF9sYXlvdXRWaWV3U3Vidmlld3MoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLnZpZXcubGF5b3V0U3Vidmlld3MoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3RGlkTGF5b3V0U3Vidmlld3MoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgdmlld0RpZExheW91dFN1YnZpZXdzKCkge1xuICAgICAgICBcbiAgICAgICAgLy8gdGhpcy5jaGlsZFZpZXdDb250cm9sbGVycy5mb3JFYWNoKGZ1bmN0aW9uIChjb250cm9sbGVyLCBpbmRleCwgY29udHJvbGxlcnMpIHtcbiAgICAgICAgXG4gICAgICAgIC8vICAgICBjb250cm9sbGVyLl9sYXlvdXRWaWV3U3Vidmlld3MoKTtcbiAgICAgICAgXG4gICAgICAgIC8vIH0pXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHZpZXdEaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnQoZXZlbnQ6IFVJVmlld0Jyb2FkY2FzdEV2ZW50KSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGhhc0NoaWxkVmlld0NvbnRyb2xsZXIodmlld0NvbnRyb2xsZXIpIHtcbiAgICAgICAgXG4gICAgICAgIC8vIFRoaXMgaXMgZm9yIHBlcmZvcm1hbmNlIHJlYXNvbnNcbiAgICAgICAgaWYgKCFJUyh2aWV3Q29udHJvbGxlcikpIHtcbiAgICAgICAgICAgIHJldHVybiBOT1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuY2hpbGRWaWV3Q29udHJvbGxlcnMubGVuZ3RoOyBpKyspIHtcbiAgICBcbiAgICAgICAgICAgIGNvbnN0IGNoaWxkVmlld0NvbnRyb2xsZXIgPSB0aGlzLmNoaWxkVmlld0NvbnRyb2xsZXJzW2ldXG4gICAgXG4gICAgICAgICAgICBpZiAoY2hpbGRWaWV3Q29udHJvbGxlciA9PSB2aWV3Q29udHJvbGxlcikge1xuICAgICAgICAgICAgICAgIHJldHVybiBZRVNcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gTk9cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGFkZENoaWxkVmlld0NvbnRyb2xsZXIodmlld0NvbnRyb2xsZXIpIHtcbiAgICAgICAgaWYgKCF0aGlzLmhhc0NoaWxkVmlld0NvbnRyb2xsZXIodmlld0NvbnRyb2xsZXIpKSB7XG4gICAgICAgICAgICB2aWV3Q29udHJvbGxlci53aWxsTW92ZVRvUGFyZW50Vmlld0NvbnRyb2xsZXIodGhpcylcbiAgICAgICAgICAgIHRoaXMuY2hpbGRWaWV3Q29udHJvbGxlcnMucHVzaCh2aWV3Q29udHJvbGxlcilcbiAgICAgICAgICAgIC8vdGhpcy52aWV3LmFkZFN1YnZpZXcodmlld0NvbnRyb2xsZXIudmlldyk7XG4gICAgICAgICAgICAvL3ZpZXdDb250cm9sbGVyLmRpZE1vdmVUb1BhcmVudFZpZXdDb250cm9sbGVyKHRoaXMpO1xuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIFxuICAgIHJlbW92ZUZyb21QYXJlbnRWaWV3Q29udHJvbGxlcigpIHtcbiAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLnBhcmVudFZpZXdDb250cm9sbGVyLmNoaWxkVmlld0NvbnRyb2xsZXJzLmluZGV4T2YodGhpcylcbiAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcbiAgICAgICAgICAgIHRoaXMucGFyZW50Vmlld0NvbnRyb2xsZXIuY2hpbGRWaWV3Q29udHJvbGxlcnMuc3BsaWNlKGluZGV4LCAxKVxuICAgICAgICAgICAgLy90aGlzLnZpZXcucmVtb3ZlRnJvbVN1cGVydmlldygpO1xuICAgICAgICAgICAgdGhpcy5wYXJlbnRWaWV3Q29udHJvbGxlciA9IG5pbFxuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIHdpbGxNb3ZlVG9QYXJlbnRWaWV3Q29udHJvbGxlcihwYXJlbnRWaWV3Q29udHJvbGxlcikge1xuICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBkaWRNb3ZlVG9QYXJlbnRWaWV3Q29udHJvbGxlcihwYXJlbnRWaWV3Q29udHJvbGxlcikge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5wYXJlbnRWaWV3Q29udHJvbGxlciA9IHBhcmVudFZpZXdDb250cm9sbGVyXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICByZW1vdmVDaGlsZFZpZXdDb250cm9sbGVyKGNvbnRyb2xsZXI6IFVJVmlld0NvbnRyb2xsZXIpIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnRyb2xsZXIudmlld1dpbGxEaXNhcHBlYXIoKVxuICAgICAgICBpZiAoSVMoY29udHJvbGxlci5wYXJlbnRWaWV3Q29udHJvbGxlcikpIHtcbiAgICAgICAgICAgIGNvbnRyb2xsZXIucmVtb3ZlRnJvbVBhcmVudFZpZXdDb250cm9sbGVyKClcbiAgICAgICAgfVxuICAgICAgICBpZiAoSVMoY29udHJvbGxlci52aWV3KSkge1xuICAgICAgICAgICAgY29udHJvbGxlci52aWV3LnJlbW92ZUZyb21TdXBlcnZpZXcoKVxuICAgICAgICB9XG4gICAgICAgIGNvbnRyb2xsZXIudmlld0RpZERpc2FwcGVhcigpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBhZGRDaGlsZFZpZXdDb250cm9sbGVySW5Db250YWluZXIoY29udHJvbGxlcjogVUlWaWV3Q29udHJvbGxlciwgY29udGFpbmVyVmlldzogVUlWaWV3KSB7XG4gICAgICAgIFxuICAgICAgICBjb250cm9sbGVyLnZpZXdXaWxsQXBwZWFyKClcbiAgICAgICAgdGhpcy5hZGRDaGlsZFZpZXdDb250cm9sbGVyKGNvbnRyb2xsZXIpXG4gICAgICAgIGNvbnRhaW5lclZpZXcuYWRkU3Vidmlldyhjb250cm9sbGVyLnZpZXcpXG4gICAgICAgIGNvbnRyb2xsZXIuZGlkTW92ZVRvUGFyZW50Vmlld0NvbnRyb2xsZXIodGhpcylcbiAgICAgICAgY29udHJvbGxlci52aWV3RGlkQXBwZWFyKClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGFkZENoaWxkVmlld0NvbnRyb2xsZXJJbkRpYWxvZ1ZpZXcoY29udHJvbGxlcjogVUlWaWV3Q29udHJvbGxlciwgZGlhbG9nVmlldzogVUlEaWFsb2dWaWV3KSB7XG4gICAgICAgIFxuICAgICAgICBjb250cm9sbGVyLnZpZXdXaWxsQXBwZWFyKClcbiAgICAgICAgdGhpcy5hZGRDaGlsZFZpZXdDb250cm9sbGVyKGNvbnRyb2xsZXIpXG4gICAgICAgIGRpYWxvZ1ZpZXcudmlldyA9IGNvbnRyb2xsZXIudmlld1xuICAgICAgICBcbiAgICAgICAgdmFyIG9yaWdpbmFsRGlzbWlzc0Z1bmN0aW9uID0gZGlhbG9nVmlldy5kaXNtaXNzLmJpbmQoZGlhbG9nVmlldylcbiAgICAgICAgXG4gICAgICAgIGRpYWxvZ1ZpZXcuZGlzbWlzcyA9IGFuaW1hdGVkID0+IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgb3JpZ2luYWxEaXNtaXNzRnVuY3Rpb24oYW5pbWF0ZWQpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQ2hpbGRWaWV3Q29udHJvbGxlcihjb250cm9sbGVyKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGNvbnRyb2xsZXIuZGlkTW92ZVRvUGFyZW50Vmlld0NvbnRyb2xsZXIodGhpcylcbiAgICAgICAgY29udHJvbGxlci52aWV3RGlkQXBwZWFyKClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiY2xhc3MgVUlEaWFsb2dWaWV3PFZpZXdUeXBlIGV4dGVuZHMgVUlWaWV3ID0gVUlWaWV3PiBleHRlbmRzIFVJVmlldyB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgX3ZpZXc6IFZpZXdUeXBlID0gbmlsXG4gICAgXG4gICAgX2FwcGVhcmVkQW5pbWF0ZWQ6IGJvb2xlYW5cbiAgICBcbiAgICBhbmltYXRpb25EdXJhdGlvbjogbnVtYmVyID0gMC4yNVxuICAgIFxuICAgIF96SW5kZXg6IG51bWJlciA9IDEwMFxuICAgIFxuICAgIGlzVmlzaWJsZTogYm9vbGVhbiA9IE5PXG4gICAgXG4gICAgZGlzbWlzc2VzT25UYXBPdXRzaWRlID0gWUVTXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEPzogc3RyaW5nLCB2aWV3SFRNTEVsZW1lbnQ/OiBIVE1MRWxlbWVudCkge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgdmlld0hUTUxFbGVtZW50KVxuICAgICAgICBcbiAgICAgICAgdGhpcy5hZGRUYXJnZXRGb3JDb250cm9sRXZlbnQoXG4gICAgICAgICAgICBVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJUYXAsXG4gICAgICAgICAgICBmdW5jdGlvbiAodGhpczogVUlEaWFsb2dWaWV3LCBzZW5kZXI6IFVJVmlldywgZXZlbnQ6IEV2ZW50KSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5kaWREZXRlY3RUYXBPdXRzaWRlKHNlbmRlciwgZXZlbnQpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LmJpbmQodGhpcylcbiAgICAgICAgKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5iYWNrZ3JvdW5kQ29sb3IgPSBVSUNvbG9yLmNvbG9yV2l0aFJHQkEoMCwgMTAsIDI1KS5jb2xvcldpdGhBbHBoYSgwLjc1KSAvL0NCQ29sb3IucHJpbWFyeUNvbnRlbnRDb2xvci5jb2xvcldpdGhBbHBoYSgwLjc1KVxuICAgICAgICBcbiAgICAgICAgdGhpcy56SW5kZXggPSB0aGlzLl96SW5kZXhcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGRpZERldGVjdFRhcE91dHNpZGUoc2VuZGVyOiBVSVZpZXcsIGV2ZW50OiBFdmVudCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKGV2ZW50LnRhcmdldCA9PSB0aGlzLnZpZXdIVE1MRWxlbWVudCAmJiB0aGlzLmRpc21pc3Nlc09uVGFwT3V0c2lkZSkge1xuICAgICAgICAgICAgdGhpcy5kaXNtaXNzKHRoaXMuX2FwcGVhcmVkQW5pbWF0ZWQpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHNldCB6SW5kZXgoekluZGV4OiBudW1iZXIpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3pJbmRleCA9IHpJbmRleFxuICAgICAgICB0aGlzLnN0eWxlLnpJbmRleCA9IFwiXCIgKyB6SW5kZXhcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGdldCB6SW5kZXgoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5fekluZGV4XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzZXQgdmlldyh2aWV3OiBWaWV3VHlwZSkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fdmlldy5yZW1vdmVGcm9tU3VwZXJ2aWV3KClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3ZpZXcgPSB2aWV3XG4gICAgICAgIFxuICAgICAgICB0aGlzLmFkZFN1YnZpZXcodmlldylcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCB2aWV3KCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX3ZpZXdcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHdpbGxBcHBlYXIoYW5pbWF0ZWQ6IGJvb2xlYW4gPSBOTykge1xuICAgICAgICBcbiAgICAgICAgaWYgKGFuaW1hdGVkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3R5bGUub3BhY2l0eSA9IFwiMFwiXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS5oZWlnaHQgPSBcIlwiXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9mcmFtZSA9IG51bGxcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGFuaW1hdGVBcHBlYXJpbmcoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLnN0eWxlLm9wYWNpdHkgPSBcIjFcIlxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgYW5pbWF0ZURpc2FwcGVhcmluZygpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUub3BhY2l0eSA9IFwiMFwiXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzaG93SW5WaWV3KGNvbnRhaW5lclZpZXc6IFVJVmlldywgYW5pbWF0ZWQ6IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBhbmltYXRlZCA9IChhbmltYXRlZCAmJiAhSVNfRklSRUZPWClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2FwcGVhcmVkQW5pbWF0ZWQgPSBhbmltYXRlZFxuICAgICAgICBcbiAgICAgICAgdGhpcy53aWxsQXBwZWFyKGFuaW1hdGVkKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGNvbnRhaW5lclZpZXcuYWRkU3Vidmlldyh0aGlzKVxuICAgICAgICBcbiAgICAgICAgaWYgKGFuaW1hdGVkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMubGF5b3V0U3Vidmlld3MoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBVSVZpZXcuYW5pbWF0ZVZpZXdPclZpZXdzV2l0aER1cmF0aW9uRGVsYXlBbmRGdW5jdGlvbihcbiAgICAgICAgICAgICAgICB0aGlzLFxuICAgICAgICAgICAgICAgIHRoaXMuYW5pbWF0aW9uRHVyYXRpb24sXG4gICAgICAgICAgICAgICAgMCxcbiAgICAgICAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYW5pbWF0ZUFwcGVhcmluZygpXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksXG4gICAgICAgICAgICAgICAgbmlsXG4gICAgICAgICAgICApXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLmlzVmlzaWJsZSA9IFlFU1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc2hvd0luUm9vdFZpZXcoYW5pbWF0ZWQ6IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2hvd0luVmlldyhVSUNvcmUubWFpbi5yb290Vmlld0NvbnRyb2xsZXIudmlldywgYW5pbWF0ZWQpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBkaXNtaXNzKGFuaW1hdGVkPzogYm9vbGVhbikge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGFuaW1hdGVkID0gKGFuaW1hdGVkICYmICFJU19GSVJFRk9YKVxuICAgICAgICBcbiAgICAgICAgaWYgKGFuaW1hdGVkID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBhbmltYXRlZCA9IHRoaXMuX2FwcGVhcmVkQW5pbWF0ZWRcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAoYW5pbWF0ZWQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgVUlWaWV3LmFuaW1hdGVWaWV3T3JWaWV3c1dpdGhEdXJhdGlvbkRlbGF5QW5kRnVuY3Rpb24oXG4gICAgICAgICAgICAgICAgdGhpcyxcbiAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGlvbkR1cmF0aW9uLFxuICAgICAgICAgICAgICAgIDAsXG4gICAgICAgICAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYW5pbWF0ZURpc2FwcGVhcmluZygpXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH0uYmluZCh0aGlzKSxcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5pc1Zpc2libGUgPT0gTk8pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVGcm9tU3VwZXJ2aWV3KClcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICAgICAgKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlRnJvbVN1cGVydmlldygpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5pc1Zpc2libGUgPSBOT1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50KGV2ZW50OiBVSVZpZXdCcm9hZGNhc3RFdmVudCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50KGV2ZW50KVxuICAgICAgICBcbiAgICAgICAgaWYgKGV2ZW50Lm5hbWUgPT0gVUlDb3JlLmJyb2FkY2FzdEV2ZW50TmFtZS5XaW5kb3dEaWRSZXNpemUpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dCgpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgbGF5b3V0U3Vidmlld3MoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgaWYgKCFJUyh0aGlzLnZpZXcpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy5mcmFtZSA9IHRoaXMuc3VwZXJ2aWV3LmJvdW5kcztcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2V0UG9zaXRpb24oMCwgMCwgMCwgMCwgMCwgXCIxMDAlXCIpXG4gICAgICAgIHRoaXMuc2V0UG9zaXRpb24oMCwgMCwgMCwgMCwgVUlWaWV3LnBhZ2VIZWlnaHQsIFwiMTAwJVwiKVxuICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgIFxuICAgICAgICBjb25zdCBtYXJnaW4gPSAyMFxuICAgIFxuICAgICAgICAvL3RoaXMudmlldy5jZW50ZXJJbkNvbnRhaW5lcigpO1xuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3LnN0eWxlLnBvc2l0aW9uID0gXCJyZWxhdGl2ZVwiXG4gICAgICAgIFxuICAgICAgICAvLyB0aGlzLnZpZXcuc3R5bGUubWF4SGVpZ2h0ID0gXCJcIiArIChib3VuZHMuaGVpZ2h0IC0gbWFyZ2luICogMikuaW50ZWdlclZhbHVlICsgXCJweFwiO1xuICAgICAgICAvLyB0aGlzLnZpZXcuc3R5bGUubWF4V2lkdGggPSBcIlwiICsgKGJvdW5kcy53aWR0aCAtIG1hcmdpbiAqIDIpLmludGVnZXJWYWx1ZSArIFwicHhcIjtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIC8vIHZhciB2aWV3SW50cmluc2ljUmVjdGFuZ2xlID0gdGhpcy52aWV3LmludHJpbnNpY0NvbnRlbnRTaXplKCk7XG4gICAgICAgIC8vIHRoaXMudmlldy5mcmFtZSA9IG5ldyBVSVJlY3RhbmdsZSgoYm91bmRzLndpZHRoIC0gdmlld0ludHJpbnNpY1JlY3RhbmdsZS53aWR0aCkqMC41LCAgKVxuICAgICAgICBcbiAgICAgICAgc3VwZXIubGF5b3V0U3Vidmlld3MoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlWaWV3LnRzXCIgLz5cblxuXG5cblxuXG5jbGFzcyBVSUJhc2VCdXR0b24gZXh0ZW5kcyBVSVZpZXcge1xuICAgIFxuICAgIF9zZWxlY3RlZDogYm9vbGVhbiA9IE5PXG4gICAgX2hpZ2hsaWdodGVkOiBib29sZWFuID0gTk9cbiAgICBcbiAgICBfaXNQb2ludGVySW5zaWRlOiBib29sZWFuXG4gICAgXG4gICAgXG4gICAgX2lzVG9nZ2xlYWJsZTogYm9vbGVhbiA9IE5PXG4gICAgX2hvdmVyZWQ6IGJvb2xlYW5cbiAgICBfZm9jdXNlZDogYm9vbGVhblxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRDogc3RyaW5nLCBlbGVtZW50VHlwZT86IHN0cmluZywgaW5pdFZpZXdEYXRhPzogYW55KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQsIG5pbCwgZWxlbWVudFR5cGUsIGluaXRWaWV3RGF0YSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlCdXR0b25cbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlWaWV3XG4gICAgICAgIFxuICAgICAgICB0aGlzLmluaXRWaWV3U3RhdGVDb250cm9sKClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGluaXRWaWV3U3RhdGVDb250cm9sKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jbGFzcy5zdXBlcmNsYXNzID0gVUlWaWV3XG4gICAgICAgIFxuICAgICAgICAvLyBJbnN0YW5jZSB2YXJpYWJsZXNcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9pc1BvaW50ZXJJbnNpZGUgPSBOT1xuICAgIFxuICAgIFxuICAgICAgICBjb25zdCBzZXRIb3ZlcmVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdGhpcy5ob3ZlcmVkID0gWUVTXG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICB0aGlzLmFkZFRhcmdldEZvckNvbnRyb2xFdmVudChVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJIb3Zlciwgc2V0SG92ZXJlZClcbiAgICBcbiAgICAgICAgY29uc3Qgc2V0Tm90SG92ZXJlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmhvdmVyZWQgPSBOT1xuICAgICAgICBcbiAgICAgICAgfS5iaW5kKHRoaXMpXG4gICAgXG4gICAgICAgIHRoaXMuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50cyhbXG4gICAgICAgICAgICBVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJMZWF2ZSwgVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyQ2FuY2VsLCBVSVZpZXcuY29udHJvbEV2ZW50Lk11bHRpcGxlVG91Y2hlc1xuICAgICAgICBdLCBzZXROb3RIb3ZlcmVkKVxuICAgIFxuICAgIFxuICAgICAgICB2YXIgaGlnaGxpZ2h0aW5nVGltZVxuICAgICAgICBjb25zdCBzZXRIaWdobGlnaHRlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMuaGlnaGxpZ2h0ZWQgPSBZRVNcbiAgICAgICAgICAgIGhpZ2hsaWdodGluZ1RpbWUgPSBEYXRlLm5vdygpXG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICB0aGlzLmFkZFRhcmdldEZvckNvbnRyb2xFdmVudChVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJEb3duLCBzZXRIaWdobGlnaHRlZClcbiAgICAgICAgdGhpcy5hZGRUYXJnZXRGb3JDb250cm9sRXZlbnQoVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyRW50ZXIsIHNldEhpZ2hsaWdodGVkKVxuICAgIFxuICAgICAgICBjb25zdCBzZXROb3RIaWdobGlnaHRlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMuaGlnaGxpZ2h0ZWQgPSBOT1xuICAgICAgICB9LmJpbmQodGhpcylcbiAgICAgICAgY29uc3Qgc2V0Tm90SGlnaGxpZ2h0ZWRXaXRoTWluaW11bUR1cmF0aW9uID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY29uc3QgbWluaW11bUR1cmF0aW9uSW5NaWxsaXNlY29uZHMgPSA1MFxuICAgICAgICAgICAgY29uc3QgZWxhcHNlZFRpbWUgPSBEYXRlLm5vdygpIC0gaGlnaGxpZ2h0aW5nVGltZVxuICAgICAgICAgICAgaWYgKG1pbmltdW1EdXJhdGlvbkluTWlsbGlzZWNvbmRzIDwgZWxhcHNlZFRpbWUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmhpZ2hsaWdodGVkID0gTk9cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhpZ2hsaWdodGVkID0gTk9cbiAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIG1pbmltdW1EdXJhdGlvbkluTWlsbGlzZWNvbmRzIC0gZWxhcHNlZFRpbWUpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICB0aGlzLmFkZFRhcmdldEZvckNvbnRyb2xFdmVudHMoW1xuICAgICAgICAgICAgVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyTGVhdmUsIFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlckNhbmNlbCwgVUlWaWV3LmNvbnRyb2xFdmVudC5NdWx0aXBsZVRvdWNoZXNcbiAgICAgICAgXSwgc2V0Tm90SGlnaGxpZ2h0ZWQpXG4gICAgICAgIHRoaXMuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlclVwLCBzZXROb3RIaWdobGlnaHRlZFdpdGhNaW5pbXVtRHVyYXRpb24pXG4gICAgICAgIFxuICAgICAgICAvLyBIYW5kbGUgZW50ZXIga2V5IHByZXNzXG4gICAgICAgIHRoaXMuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KFVJVmlldy5jb250cm9sRXZlbnQuRW50ZXJEb3duLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHNldEhpZ2hsaWdodGVkKClcbiAgICAgICAgICAgIHNldE5vdEhpZ2hsaWdodGVkV2l0aE1pbmltdW1EdXJhdGlvbigpXG4gICAgICAgICAgICBcbiAgICAgICAgfSlcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLmFkZFRhcmdldEZvckNvbnRyb2xFdmVudChcbiAgICAgICAgICAgIFVJVmlldy5jb250cm9sRXZlbnQuRm9jdXMsXG4gICAgICAgICAgICBmdW5jdGlvbiAodGhpczogVUlCYXNlQnV0dG9uLCBzZW5kZXI6IFVJVmlldywgZXZlbnQ6IEV2ZW50KSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5mb2N1c2VkID0gWUVTXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LmJpbmQodGhpcylcbiAgICAgICAgKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5hZGRUYXJnZXRGb3JDb250cm9sRXZlbnQoXG4gICAgICAgICAgICBVSVZpZXcuY29udHJvbEV2ZW50LkJsdXIsXG4gICAgICAgICAgICBmdW5jdGlvbiAodGhpczogVUlCYXNlQnV0dG9uLCBzZW5kZXI6IFVJVmlldywgZXZlbnQ6IEV2ZW50KSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5mb2N1c2VkID0gTk9cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICApXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy51cGRhdGVDb250ZW50Rm9yQ3VycmVudFN0YXRlKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMucGF1c2VzUG9pbnRlckV2ZW50cyA9IFlFU1xuICAgICAgICB0aGlzLnRhYkluZGV4ID0gMVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS5jdXJzb3IgPSBcInBvaW50ZXJcIlxuICAgICAgICBcbiAgICAgICAgLy90aGlzLnN0eWxlLm91dGxpbmUgPSBcIm5vbmVcIjtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLm5hdGl2ZVNlbGVjdGlvbkVuYWJsZWQgPSBOT1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50cyhbXG4gICAgICAgICAgICBVSVZpZXcuY29udHJvbEV2ZW50LkVudGVyRG93biwgVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyVXBJbnNpZGVcbiAgICAgICAgXSwgZnVuY3Rpb24gKHRoaXM6IFVJQmFzZUJ1dHRvbiwgc2VuZGVyLCBldmVudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAodGhpcy5pc1RvZ2dsZWFibGUpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZVNlbGVjdGVkU3RhdGUoKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBwdWJsaWMgc2V0IGhvdmVyZWQoaG92ZXJlZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9ob3ZlcmVkID0gaG92ZXJlZFxuICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50U3RhdGUoKVxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgZ2V0IGhvdmVyZWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9ob3ZlcmVkXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBzZXQgaGlnaGxpZ2h0ZWQoaGlnaGxpZ2h0ZWQ6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5faGlnaGxpZ2h0ZWQgPSBoaWdobGlnaHRlZFxuICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50U3RhdGUoKVxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgZ2V0IGhpZ2hsaWdodGVkKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5faGlnaGxpZ2h0ZWRcbiAgICB9XG4gICAgXG4gICAgcHVibGljIHNldCBmb2N1c2VkKGZvY3VzZWQ6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fZm9jdXNlZCA9IGZvY3VzZWRcbiAgICAgICAgaWYgKGZvY3VzZWQpIHtcbiAgICAgICAgICAgIHRoaXMuZm9jdXMoKVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5ibHVyKClcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50U3RhdGUoKVxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgZ2V0IGZvY3VzZWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9mb2N1c2VkXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBzZXQgc2VsZWN0ZWQoc2VsZWN0ZWQ6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fc2VsZWN0ZWQgPSBzZWxlY3RlZFxuICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50U3RhdGUoKVxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgZ2V0IHNlbGVjdGVkKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fc2VsZWN0ZWRcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZSgpIHtcbiAgICBcbiAgICAgICAgdmFyIHVwZGF0ZUZ1bmN0aW9uOiBGdW5jdGlvbiA9IHRoaXMudXBkYXRlQ29udGVudEZvck5vcm1hbFN0YXRlXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkICYmIHRoaXMuaGlnaGxpZ2h0ZWQpIHtcbiAgICAgICAgICAgIHVwZGF0ZUZ1bmN0aW9uID0gdGhpcy51cGRhdGVDb250ZW50Rm9yU2VsZWN0ZWRBbmRIaWdobGlnaHRlZFN0YXRlXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5zZWxlY3RlZCkge1xuICAgICAgICAgICAgdXBkYXRlRnVuY3Rpb24gPSB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JTZWxlY3RlZFN0YXRlXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5mb2N1c2VkKSB7XG4gICAgICAgICAgICB1cGRhdGVGdW5jdGlvbiA9IHRoaXMudXBkYXRlQ29udGVudEZvckZvY3VzZWRTdGF0ZVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuaGlnaGxpZ2h0ZWQpIHtcbiAgICAgICAgICAgIHVwZGF0ZUZ1bmN0aW9uID0gdGhpcy51cGRhdGVDb250ZW50Rm9ySGlnaGxpZ2h0ZWRTdGF0ZVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuaG92ZXJlZCkge1xuICAgICAgICAgICAgdXBkYXRlRnVuY3Rpb24gPSB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JIb3ZlcmVkU3RhdGVcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKCFJUyh1cGRhdGVGdW5jdGlvbikpIHtcbiAgICAgICAgICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gVUlDb2xvci5uaWxDb2xvclxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdXBkYXRlRnVuY3Rpb24uY2FsbCh0aGlzKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yTm9ybWFsU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHVwZGF0ZUNvbnRlbnRGb3JIb3ZlcmVkU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JOb3JtYWxTdGF0ZSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yRm9jdXNlZFN0YXRlKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy51cGRhdGVDb250ZW50Rm9ySG92ZXJlZFN0YXRlKClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHVwZGF0ZUNvbnRlbnRGb3JIaWdobGlnaHRlZFN0YXRlKCkge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yU2VsZWN0ZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvclNlbGVjdGVkQW5kSGlnaGxpZ2h0ZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvclNlbGVjdGVkU3RhdGUoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0IGVuYWJsZWQoZW5hYmxlZDogYm9vbGVhbikge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuZW5hYmxlZCA9IGVuYWJsZWRcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRFbmFibGVkU3RhdGUoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGVuYWJsZWQoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gc3VwZXIuZW5hYmxlZFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckN1cnJlbnRFbmFibGVkU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5lbmFibGVkKSB7XG4gICAgICAgICAgICB0aGlzLmFscGhhID0gMVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5hbHBoYSA9IDAuNVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLnVzZXJJbnRlcmFjdGlvbkVuYWJsZWQgPSB0aGlzLmVuYWJsZWRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGFkZFN0eWxlQ2xhc3Moc3R5bGVDbGFzc05hbWU6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuYWRkU3R5bGVDbGFzcyhzdHlsZUNsYXNzTmFtZSlcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLnN0eWxlQ2xhc3NOYW1lICE9IHN0eWxlQ2xhc3NOYW1lKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZS5jYWxsKHRoaXMpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50KGV2ZW50OiBVSVZpZXdCcm9hZGNhc3RFdmVudCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50KGV2ZW50KVxuICAgICAgICBcbiAgICAgICAgaWYgKGV2ZW50Lm5hbWUgPT0gVUlWaWV3LmJyb2FkY2FzdEV2ZW50TmFtZS5QYWdlRGlkU2Nyb2xsIHx8IGV2ZW50Lm5hbWUgPT1cbiAgICAgICAgICAgIFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuQWRkZWRUb1ZpZXdUcmVlKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuaG92ZXJlZCA9IE5PXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuaGlnaGxpZ2h0ZWQgPSBOT1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHRvZ2dsZVNlbGVjdGVkU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5zZWxlY3RlZCA9ICF0aGlzLnNlbGVjdGVkXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IGlzVG9nZ2xlYWJsZShpc1RvZ2dsZWFibGU6IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzVG9nZ2xlYWJsZSA9IGlzVG9nZ2xlYWJsZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGlzVG9nZ2xlYWJsZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLl9pc1RvZ2dsZWFibGVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGxheW91dFN1YnZpZXdzKCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIubGF5b3V0U3Vidmlld3MoKVxuICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzZW5kQ29udHJvbEV2ZW50Rm9yS2V5KGV2ZW50S2V5OiBzdHJpbmcsIG5hdGl2ZUV2ZW50OiBFdmVudCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKGV2ZW50S2V5ID09IFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlclVwSW5zaWRlICYmICF0aGlzLmhpZ2hsaWdodGVkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIERvIG5vdCBzZW5kIHRoZSBldmVudCBpbiB0aGlzIGNhc2VcbiAgICAgICAgICAgIC8vc3VwZXIuc2VuZENvbnRyb2xFdmVudEZvcktleShldmVudEtleSwgbmF0aXZlRXZlbnQpO1xuICAgIFxuICAgICAgICAgICAgY29uc3QgYXNkID0gMVxuICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBzdXBlci5zZW5kQ29udHJvbEV2ZW50Rm9yS2V5KGV2ZW50S2V5LCBuYXRpdmVFdmVudClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgZ2V0RXZlbnRDb29yZGluYXRlc0luRG9jdW1lbnQodG91Y2hPck1vdXNlRXZlbnQpIHtcbiAgICAgICAgLy8gaHR0cDovL3d3dy5xdWlya3Ntb2RlLm9yZy9qcy9ldmVudHNfcHJvcGVydGllcy5odG1sXG4gICAgICAgIHZhciBwb3N4ID0gMFxuICAgICAgICB2YXIgcG9zeSA9IDBcbiAgICAgICAgdmFyIGUgPSB0b3VjaE9yTW91c2VFdmVudFxuICAgICAgICBpZiAoIWUpIHtcbiAgICAgICAgICAgIGUgPSB3aW5kb3cuZXZlbnRcbiAgICAgICAgfVxuICAgICAgICBpZiAoZS5wYWdlWCB8fCBlLnBhZ2VZKSB7XG4gICAgICAgICAgICBwb3N4ID0gZS5wYWdlWFxuICAgICAgICAgICAgcG9zeSA9IGUucGFnZVlcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChlLmNsaWVudFggfHwgZS5jbGllbnRZKSB7XG4gICAgICAgICAgICBwb3N4ID0gZS5jbGllbnRYICsgZG9jdW1lbnQuYm9keS5zY3JvbGxMZWZ0XG4gICAgICAgICAgICAgICAgKyBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsTGVmdFxuICAgICAgICAgICAgcG9zeSA9IGUuY2xpZW50WSArIGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wXG4gICAgICAgICAgICAgICAgKyBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wXG4gICAgICAgIH1cbiAgICAgICAgLy8gcG9zeCBhbmQgcG9zeSBjb250YWluIHRoZSBtb3VzZSBwb3NpdGlvbiByZWxhdGl2ZSB0byB0aGUgZG9jdW1lbnRcbiAgICBcbiAgICAgICAgY29uc3QgY29vcmRpbmF0ZXMgPSB7IFwieFwiOiBwb3N4LCBcInlcIjogcG9zeSB9XG4gICAgXG4gICAgICAgIHJldHVybiBjb29yZGluYXRlc1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc3RhdGljIGdldEVsZW1lbnRQb3NpdGlvbkluRG9jdW1lbnQoZWwpIHtcbiAgICAgICAgLy9odHRwczovL3d3dy5raXJ1cGEuY29tL2h0bWw1L2dldHRpbmdfbW91c2VfY2xpY2tfcG9zaXRpb24uaHRtXG4gICAgICAgIHZhciB4UG9zaXRpb24gPSAwXG4gICAgICAgIHZhciB5UG9zaXRpb24gPSAwXG4gICAgXG4gICAgICAgIHdoaWxlIChlbCkge1xuICAgICAgICAgICAgaWYgKGVsLnRhZ05hbWUgPT0gXCJCT0RZXCIpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAvLyBDb29yZGluYXRlcyBpbiBkb2N1bWVudCBhcmUgY29vcmRpbmF0ZXMgaW4gYm9keSwgdGhlcmVmb3JlIHN1YnRyYWN0aW5nIHRoZSBzY3JvbGwgcG9zaXRpb24gb2YgdGhlIGJvZHkgaXMgbm90IG5lZWRlZFxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC8vICAgICAgLy8gZGVhbCB3aXRoIGJyb3dzZXIgcXVpcmtzIHdpdGggYm9keS93aW5kb3cvZG9jdW1lbnQgYW5kIHBhZ2Ugc2Nyb2xsXG4gICAgICAgICAgICAgICAgLy8gICAgICB2YXIgeFNjcm9sbFBvcyA9IGVsLnNjcm9sbExlZnQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbExlZnQ7XG4gICAgICAgICAgICAgICAgLy8gICAgICB2YXIgeVNjcm9sbFBvcyA9IGVsLnNjcm9sbFRvcCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xuICAgICAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAgICAgLy8gICAgICB4UG9zaXRpb24gKz0gKGVsLm9mZnNldExlZnQgLSB4U2Nyb2xsUG9zICsgZWwuY2xpZW50TGVmdCk7XG4gICAgICAgICAgICAgICAgLy8gICAgICB5UG9zaXRpb24gKz0gKGVsLm9mZnNldFRvcCAtIHlTY3JvbGxQb3MgKyBlbC5jbGllbnRUb3ApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgeFBvc2l0aW9uICs9IChlbC5vZmZzZXRMZWZ0IC0gZWwuc2Nyb2xsTGVmdCArIGVsLmNsaWVudExlZnQpXG4gICAgICAgICAgICAgICAgeVBvc2l0aW9uICs9IChlbC5vZmZzZXRUb3AgLSBlbC5zY3JvbGxUb3AgKyBlbC5jbGllbnRUb3ApXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGVsID0gZWwub2Zmc2V0UGFyZW50XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHg6IHhQb3NpdGlvbixcbiAgICAgICAgICAgIHk6IHlQb3NpdGlvblxuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIHN0YXRpYyBjb252ZXJ0Q29vcmRpbmF0ZXNGcm9tRG9jdW1lbnRUb0VsZW1lbnQoeCwgeSwgZWxlbWVudCkge1xuICAgICAgICBjb25zdCBlbGVtZW50UG9zaXRpb25JbkRvY3VtZW50ID0gdGhpcy5nZXRFbGVtZW50UG9zaXRpb25JbkRvY3VtZW50KGVsZW1lbnQpXG4gICAgICAgIGNvbnN0IGNvb3JkaW5hdGVzSW5FbGVtZW50ID0geyBcInhcIjogeCAtIGVsZW1lbnRQb3NpdGlvbkluRG9jdW1lbnQueCwgXCJ5XCI6IHkgLSBlbGVtZW50UG9zaXRpb25JbkRvY3VtZW50LnkgfVxuICAgICAgICByZXR1cm4gY29vcmRpbmF0ZXNJbkVsZW1lbnRcbiAgICB9XG4gICAgXG4gICAgc3RhdGljIGdldEV2ZW50Q29vcmRpbmF0ZXNJbkVsZW1lbnQodG91Y2hPck1vdXNlRXZlbnQsIGVsZW1lbnQpIHtcbiAgICAgICAgY29uc3QgY29vcmRpbmF0ZXNJbkRvY3VtZW50ID0gdGhpcy5nZXRFdmVudENvb3JkaW5hdGVzSW5Eb2N1bWVudCh0b3VjaE9yTW91c2VFdmVudClcbiAgICAgICAgY29uc3QgY29vcmRpbmF0ZXNJbkVsZW1lbnQgPSB0aGlzLmNvbnZlcnRDb29yZGluYXRlc0Zyb21Eb2N1bWVudFRvRWxlbWVudChcbiAgICAgICAgICAgIGNvb3JkaW5hdGVzSW5Eb2N1bWVudC54LFxuICAgICAgICAgICAgY29vcmRpbmF0ZXNJbkRvY3VtZW50LnksXG4gICAgICAgICAgICBlbGVtZW50XG4gICAgICAgIClcbiAgICAgICAgcmV0dXJuIGNvb3JkaW5hdGVzSW5FbGVtZW50XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlCYXNlQnV0dG9uLnRzXCIgLz5cblxuXG5cbmNsYXNzIFVJTGluayBleHRlbmRzIFVJQmFzZUJ1dHRvbiB7XG4gICAgXG4gICAgY29sb3JzOiBVSUJ1dHRvbkNvbG9yU3BlY2lmaWVyXG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEOiBzdHJpbmcsIGluaXRWaWV3RGF0YSA9IG5pbCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIoZWxlbWVudElELCBcImFcIiwgaW5pdFZpZXdEYXRhKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBVSUxpbmtcbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlCYXNlQnV0dG9uXG4gICAgICAgIFxuICAgICAgICB0aGlzLnN0b3BzUG9pbnRlckV2ZW50UHJvcGFnYXRpb24gPSBOT1xuICAgICAgICBcbiAgICAgICAgdGhpcy5wYXVzZXNQb2ludGVyRXZlbnRzID0gTk9cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgaW5pdFZpZXcoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YTogeyB0aXRsZVR5cGU6IHN0cmluZyB9KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5pbml0VmlldyhlbGVtZW50SUQsIHZpZXdIVE1MRWxlbWVudCwgaW5pdFZpZXdEYXRhKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5jbGFzcy5zdXBlcmNsYXNzID0gVUlCYXNlQnV0dG9uXG4gICAgICAgIFxuICAgICAgICAvLyBJbnN0YW5jZSB2YXJpYWJsZXNcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICAvL3RoaXMuc3R5bGUucG9zaXRpb24gPSBcInJlbGF0aXZlXCJcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB2aWV3SFRNTEVsZW1lbnQub25jbGljayA9IHRoaXMuYmx1ci5iaW5kKHRoaXMpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgdmlld0hUTUxFbGVtZW50KCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHN1cGVyLnZpZXdIVE1MRWxlbWVudCBhcyBIVE1MTGlua0VsZW1lbnRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNldCB0ZXh0KHRleHQ6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQudGV4dENvbnRlbnQgPSB0ZXh0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgdGV4dCgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLnZpZXdIVE1MRWxlbWVudC50ZXh0Q29udGVudFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0IHRhcmdldCh0YXJnZXQ6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQuc2V0QXR0cmlidXRlKFwiaHJlZlwiLCB0YXJnZXQpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgdGFyZ2V0KCkge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLnZpZXdIVE1MRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJocmVmXCIpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNldCB0YXJnZXRSb3V0ZUZvckN1cnJlbnRTdGF0ZSh0YXJnZXRSb3V0ZUZvckN1cnJlbnRTdGF0ZTogKCkgPT4gKFVJUm91dGUgfCBzdHJpbmcpKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl90YXJnZXRSb3V0ZUZvckN1cnJlbnRTdGF0ZSA9IHRhcmdldFJvdXRlRm9yQ3VycmVudFN0YXRlXG4gICAgICAgIFxuICAgICAgICB0aGlzLnVwZGF0ZVRhcmdldCgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgdGFyZ2V0Um91dGVGb3JDdXJyZW50U3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5fdGFyZ2V0Um91dGVGb3JDdXJyZW50U3RhdGVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIF90YXJnZXRSb3V0ZUZvckN1cnJlbnRTdGF0ZSgpIHtcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gVUlSb3V0ZS5jdXJyZW50Um91dGUucm91dGVCeVJlbW92aW5nQ29tcG9uZW50c090aGVyVGhhbk9uZXNOYW1lZChbXCJzZXR0aW5nc1wiXSkgYXMgKFVJUm91dGUgfCBzdHJpbmcpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGRpZFJlY2VpdmVCcm9hZGNhc3RFdmVudChldmVudDogVUlWaWV3QnJvYWRjYXN0RXZlbnQpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmRpZFJlY2VpdmVCcm9hZGNhc3RFdmVudChldmVudClcbiAgICAgICAgXG4gICAgICAgIGlmIChldmVudC5uYW1lID09IFVJQ29yZS5icm9hZGNhc3RFdmVudE5hbWUuUm91dGVEaWRDaGFuZ2UpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy51cGRhdGVUYXJnZXQoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHdhc0FkZGVkVG9WaWV3VHJlZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLndhc0FkZGVkVG9WaWV3VHJlZSgpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnVwZGF0ZVRhcmdldCgpXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgdXBkYXRlVGFyZ2V0KCkge1xuICAgICAgICBcbiAgICAgICAgY29uc3Qgcm91dGUgPSB0aGlzLnRhcmdldFJvdXRlRm9yQ3VycmVudFN0YXRlKClcbiAgICAgICAgXG4gICAgICAgIGlmIChyb3V0ZSBpbnN0YW5jZW9mIFVJUm91dGUpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy50YXJnZXQgPSByb3V0ZS5saW5rUmVwcmVzZW50YXRpb25cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy50YXJnZXQgPSByb3V0ZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgbGF5b3V0U3Vidmlld3MoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5sYXlvdXRTdWJ2aWV3cygpXG4gICAgXG4gICAgICAgIGNvbnN0IGJvdW5kcyA9IHRoaXMuYm91bmRzXG4gICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlMaW5rLnRzXCIgLz5cblxuXG5cbmNsYXNzIFVJTGlua0J1dHRvbiBleHRlbmRzIFVJTGluayB7XG4gICAgXG4gICAgXG4gICAgYnV0dG9uOiBVSUJ1dHRvblxuICAgIFxuICAgIC8vbGluayA9IG5ldyBVSUxpbmsodGhpcy5lbGVtZW50SUQgKyBcIkxpbmtcIik7XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEOiBzdHJpbmcsIGVsZW1lbnRUeXBlPzogc3RyaW5nLCB0aXRsZVR5cGU/OiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgeyBcImVsZW1lbnRUeXBlXCI6IGVsZW1lbnRUeXBlLCBcInRpdGxlVHlwZVwiOiB0aXRsZVR5cGUgfSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlMaW5rQnV0dG9uXG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJTGlua1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYnV0dG9uLmFkZFRhcmdldEZvckNvbnRyb2xFdmVudHMoW1xuICAgICAgICAgICAgVUlCdXR0b24uY29udHJvbEV2ZW50LkVudGVyRG93biwgVUlCdXR0b24uY29udHJvbEV2ZW50LlBvaW50ZXJVcEluc2lkZVxuICAgICAgICBdLCBmdW5jdGlvbiAodGhpczogVUlMaW5rQnV0dG9uLCBzZW5kZXI6IFVJQnV0dG9uLCBldmVudDogRXZlbnQpIHtcbiAgICBcbiAgICAgICAgICAgIGNvbnN0IGFzZCA9IDFcbiAgICBcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbiA9IHRoaXMudGFyZ2V0IGFzIGFueVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfS5iaW5kKHRoaXMpKVxuICAgICAgICBcbiAgICAgICAgLy8gdGhpcy5saW5rLmhpZGRlbiA9IFlFUztcbiAgICAgICAgXG4gICAgICAgIC8vIHRoaXMuYWRkU3Vidmlldyh0aGlzLmxpbmspO1xuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGluaXRWaWV3KGVsZW1lbnRJRCwgdmlld0hUTUxFbGVtZW50LCBpbml0Vmlld0RhdGE6IHsgdGl0bGVUeXBlOiBzdHJpbmcsIGVsZW1lbnRUeXBlOiBzdHJpbmcgfSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaW5pdFZpZXcoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY2xhc3Muc3VwZXJjbGFzcyA9IFVJTGlua1xuICAgICAgICBcbiAgICAgICAgLy8gSW5zdGFuY2UgdmFyaWFibGVzXG4gICAgICAgIFxuICAgICAgICB0aGlzLmJ1dHRvbiA9IG5ldyBVSUJ1dHRvbih0aGlzLmVsZW1lbnRJRCArIFwiQnV0dG9uXCIsIGluaXRWaWV3RGF0YS5lbGVtZW50VHlwZSwgaW5pdFZpZXdEYXRhLnRpdGxlVHlwZSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYWRkU3Vidmlldyh0aGlzLmJ1dHRvbilcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCJcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgdGl0bGVMYWJlbCgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLmJ1dHRvbi50aXRsZUxhYmVsXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgaW1hZ2VWaWV3KCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuYnV0dG9uLmltYWdlVmlld1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0IGNvbG9ycyhjb2xvcnM6IFVJQnV0dG9uQ29sb3JTcGVjaWZpZXIpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYnV0dG9uLmNvbG9ycyA9IGNvbG9yc1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgZ2V0IGNvbG9ycygpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLmJ1dHRvbi5jb2xvcnNcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCB2aWV3SFRNTEVsZW1lbnQoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gc3VwZXIudmlld0hUTUxFbGVtZW50IGFzIEhUTUxMaW5rRWxlbWVudFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0IHRhcmdldCh0YXJnZXQ6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQuc2V0QXR0cmlidXRlKFwiaHJlZlwiLCB0YXJnZXQpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgdGFyZ2V0KCkge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLnZpZXdIVE1MRWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJocmVmXCIpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGxheW91dFN1YnZpZXdzKCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIubGF5b3V0U3Vidmlld3MoKVxuICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgIFxuICAgICAgICB0aGlzLmJ1dHRvbi5mcmFtZSA9IGJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5idXR0b24ubGF5b3V0U3Vidmlld3MoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlCYXNlQnV0dG9uLnRzXCIgLz5cblxuXG5cbmludGVyZmFjZSBVSUJ1dHRvbkNvbG9yU3BlY2lmaWVyIHtcbiAgICBcbiAgICB0aXRsZUxhYmVsOiBVSUJ1dHRvbkVsZW1lbnRDb2xvclNwZWNpZmllcjtcbiAgICBiYWNrZ3JvdW5kOiBVSUJ1dHRvbkVsZW1lbnRDb2xvclNwZWNpZmllcjtcbiAgICBcbn1cblxuXG5pbnRlcmZhY2UgVUlCdXR0b25FbGVtZW50Q29sb3JTcGVjaWZpZXIge1xuICAgIFxuICAgIG5vcm1hbDogVUlDb2xvcjtcbiAgICBob3ZlcmVkPzogVUlDb2xvcjtcbiAgICBoaWdobGlnaHRlZDogVUlDb2xvcjtcbiAgICBmb2N1c2VkPzogVUlDb2xvcjtcbiAgICBzZWxlY3RlZDogVUlDb2xvcjtcbiAgICBzZWxlY3RlZEFuZEhpZ2hsaWdodGVkPzogVUlDb2xvcjtcbiAgICBcbn1cblxuXG5cbmNsYXNzIFVJQnV0dG9uIGV4dGVuZHMgVUlCYXNlQnV0dG9uIHtcbiAgICBcbiAgICBfY29udGVudFBhZGRpbmc6IGFueVxuICAgIF90aXRsZUxhYmVsOiBVSVRleHRWaWV3XG4gICAgX2ltYWdlVmlldzogVUlJbWFnZVZpZXdcbiAgICBcbiAgICB1c2VzQXV0b21hdGljVGl0bGVGb250U2l6ZSA9IE5PXG4gICAgbWluQXV0b21hdGljRm9udFNpemU6IG51bWJlciA9IG5pbFxuICAgIG1heEF1dG9tYXRpY0ZvbnRTaXplOiBudW1iZXIgPSAyNVxuICAgIFxuICAgIGNvbG9yczogVUlCdXR0b25Db2xvclNwZWNpZmllclxuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRDogc3RyaW5nLCBlbGVtZW50VHlwZT86IHN0cmluZywgdGl0bGVUeXBlID0gVUlUZXh0Vmlldy50eXBlLnNwYW4pIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgZWxlbWVudFR5cGUsIHsgXCJ0aXRsZVR5cGVcIjogdGl0bGVUeXBlIH0pXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IFVJQnV0dG9uXG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJQmFzZUJ1dHRvblxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGluaXRWaWV3KGVsZW1lbnRJRCwgdmlld0hUTUxFbGVtZW50LCBpbml0Vmlld0RhdGE6IHsgdGl0bGVUeXBlOiBzdHJpbmcgfSkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jbGFzcy5zdXBlcmNsYXNzID0gVUlCYXNlQnV0dG9uXG4gICAgICAgIFxuICAgICAgICAvLyBJbnN0YW5jZSB2YXJpYWJsZXNcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29sb3JzID0ge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aXRsZUxhYmVsOiB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLndoaXRlQ29sb3IsXG4gICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IFVJQ29sb3Iud2hpdGVDb2xvcixcbiAgICAgICAgICAgICAgICBzZWxlY3RlZDogVUlDb2xvci53aGl0ZUNvbG9yXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLmJsdWVDb2xvcixcbiAgICAgICAgICAgICAgICBoaWdobGlnaHRlZDogVUlDb2xvci5ncmVlbkNvbG9yLFxuICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBVSUNvbG9yLnJlZENvbG9yXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2ltYWdlVmlldyA9IG5ldyBVSUltYWdlVmlldyhlbGVtZW50SUQgKyBcIkltYWdlVmlld1wiKVxuICAgICAgICB0aGlzLl9pbWFnZVZpZXcuaGlkZGVuID0gWUVTXG4gICAgICAgIHRoaXMuYWRkU3Vidmlldyh0aGlzLmltYWdlVmlldylcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaW1hZ2VWaWV3LmZpbGxNb2RlID0gVUlJbWFnZVZpZXcuZmlsbE1vZGUuYXNwZWN0Rml0SWZMYXJnZXJcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAoSVNfTk9UX05JTChpbml0Vmlld0RhdGEudGl0bGVUeXBlKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl90aXRsZUxhYmVsID0gbmV3IFVJVGV4dFZpZXcoZWxlbWVudElEICsgXCJUaXRsZUxhYmVsXCIsIGluaXRWaWV3RGF0YS50aXRsZVR5cGUpXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuc3R5bGUud2hpdGVTcGFjZSA9IFwibm93cmFwXCJcbiAgICAgICAgICAgIHRoaXMuYWRkU3Vidmlldyh0aGlzLnRpdGxlTGFiZWwpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC51c2VySW50ZXJhY3Rpb25FbmFibGVkID0gTk9cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLmNvbnRlbnRQYWRkaW5nID0gMTBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaW1hZ2VWaWV3LnVzZXJJbnRlcmFjdGlvbkVuYWJsZWQgPSBOT1xuICAgICAgICB0aGlzLnRpdGxlTGFiZWwudGV4dEFsaWdubWVudCA9IFVJVGV4dFZpZXcudGV4dEFsaWdubWVudC5jZW50ZXJcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLm5hdGl2ZVNlbGVjdGlvbkVuYWJsZWQgPSBOT1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGNvbnRlbnRQYWRkaW5nKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbnRlbnRQYWRkaW5nLmludGVnZXJWYWx1ZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IGNvbnRlbnRQYWRkaW5nKGNvbnRlbnRQYWRkaW5nKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jb250ZW50UGFkZGluZyA9IGNvbnRlbnRQYWRkaW5nXG4gICAgICAgIFxuICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHB1YmxpYyBzZXQgaG92ZXJlZChob3ZlcmVkOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2hvdmVyZWQgPSBob3ZlcmVkXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZSgpXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgaG92ZXJlZCgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2hvdmVyZWRcbiAgICB9XG4gICAgXG4gICAgcHVibGljIHNldCBoaWdobGlnaHRlZChoaWdobGlnaHRlZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9oaWdobGlnaHRlZCA9IGhpZ2hsaWdodGVkXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZSgpXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgaGlnaGxpZ2h0ZWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9oaWdobGlnaHRlZFxuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgc2V0IGZvY3VzZWQoZm9jdXNlZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9mb2N1c2VkID0gZm9jdXNlZFxuICAgICAgICBpZiAoZm9jdXNlZCkge1xuICAgICAgICAgICAgdGhpcy5mb2N1cygpXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmJsdXIoKVxuICAgICAgICB9XG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZSgpXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgZm9jdXNlZCgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ZvY3VzZWRcbiAgICB9XG4gICAgXG4gICAgcHVibGljIHNldCBzZWxlY3RlZChzZWxlY3RlZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9zZWxlY3RlZCA9IHNlbGVjdGVkXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZSgpXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgc2VsZWN0ZWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9zZWxlY3RlZFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yQ3VycmVudFN0YXRlKCkge1xuICAgIFxuICAgICAgICB2YXIgdXBkYXRlRnVuY3Rpb246IEZ1bmN0aW9uID0gdGhpcy51cGRhdGVDb250ZW50Rm9yTm9ybWFsU3RhdGVcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWQgJiYgdGhpcy5oaWdobGlnaHRlZCkge1xuICAgICAgICAgICAgdXBkYXRlRnVuY3Rpb24gPSB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JTZWxlY3RlZEFuZEhpZ2hsaWdodGVkU3RhdGVcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLnNlbGVjdGVkKSB7XG4gICAgICAgICAgICB1cGRhdGVGdW5jdGlvbiA9IHRoaXMudXBkYXRlQ29udGVudEZvclNlbGVjdGVkU3RhdGVcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmZvY3VzZWQpIHtcbiAgICAgICAgICAgIHVwZGF0ZUZ1bmN0aW9uID0gdGhpcy51cGRhdGVDb250ZW50Rm9yRm9jdXNlZFN0YXRlXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5oaWdobGlnaHRlZCkge1xuICAgICAgICAgICAgdXBkYXRlRnVuY3Rpb24gPSB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JIaWdobGlnaHRlZFN0YXRlXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5ob3ZlcmVkKSB7XG4gICAgICAgICAgICB1cGRhdGVGdW5jdGlvbiA9IHRoaXMudXBkYXRlQ29udGVudEZvckhvdmVyZWRTdGF0ZVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAoIUlTKHVwZGF0ZUZ1bmN0aW9uKSkge1xuICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLnRleHRDb2xvciA9IFVJQ29sb3IubmlsQ29sb3JcbiAgICAgICAgICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gVUlDb2xvci5uaWxDb2xvclxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdXBkYXRlRnVuY3Rpb24uY2FsbCh0aGlzKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50RW5hYmxlZFN0YXRlKCk7XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yTm9ybWFsU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLmJhY2tncm91bmRDb2xvciA9IHRoaXMuY29sb3JzLmJhY2tncm91bmQubm9ybWFsXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC50ZXh0Q29sb3IgPSB0aGlzLmNvbG9ycy50aXRsZUxhYmVsLm5vcm1hbFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckhvdmVyZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvck5vcm1hbFN0YXRlKClcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLmNvbG9ycy5iYWNrZ3JvdW5kLmhvdmVyZWQpIHtcbiAgICAgICAgICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gdGhpcy5jb2xvcnMuYmFja2dyb3VuZC5ob3ZlcmVkXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLmNvbG9ycy50aXRsZUxhYmVsLmhvdmVyZWQpIHtcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC50ZXh0Q29sb3IgPSB0aGlzLmNvbG9ycy50aXRsZUxhYmVsLmhvdmVyZWRcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckZvY3VzZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckhvdmVyZWRTdGF0ZSgpXG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5jb2xvcnMuYmFja2dyb3VuZC5mb2N1c2VkKSB7XG4gICAgICAgICAgICB0aGlzLmJhY2tncm91bmRDb2xvciA9IHRoaXMuY29sb3JzLmJhY2tncm91bmQuZm9jdXNlZFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5jb2xvcnMudGl0bGVMYWJlbC5mb2N1c2VkKSB7XG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwudGV4dENvbG9yID0gdGhpcy5jb2xvcnMudGl0bGVMYWJlbC5mb2N1c2VkXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHVwZGF0ZUNvbnRlbnRGb3JIaWdobGlnaHRlZFN0YXRlKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5iYWNrZ3JvdW5kQ29sb3IgPSB0aGlzLmNvbG9ycy5iYWNrZ3JvdW5kLmhpZ2hsaWdodGVkXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC50ZXh0Q29sb3IgPSB0aGlzLmNvbG9ycy50aXRsZUxhYmVsLmhpZ2hsaWdodGVkXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yU2VsZWN0ZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gdGhpcy5jb2xvcnMuYmFja2dyb3VuZC5zZWxlY3RlZFxuICAgICAgICB0aGlzLnRpdGxlTGFiZWwudGV4dENvbG9yID0gdGhpcy5jb2xvcnMudGl0bGVMYWJlbC5zZWxlY3RlZFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvclNlbGVjdGVkQW5kSGlnaGxpZ2h0ZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvclNlbGVjdGVkU3RhdGUoKVxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuY29sb3JzLmJhY2tncm91bmQuc2VsZWN0ZWRBbmRIaWdobGlnaHRlZCkge1xuICAgICAgICAgICAgdGhpcy5iYWNrZ3JvdW5kQ29sb3IgPSB0aGlzLmNvbG9ycy5iYWNrZ3JvdW5kLnNlbGVjdGVkQW5kSGlnaGxpZ2h0ZWRcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuY29sb3JzLnRpdGxlTGFiZWwuc2VsZWN0ZWRBbmRIaWdobGlnaHRlZCkge1xuICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLnRleHRDb2xvciA9IHRoaXMuY29sb3JzLnRpdGxlTGFiZWwuc2VsZWN0ZWRBbmRIaWdobGlnaHRlZFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzZXQgZW5hYmxlZChlbmFibGVkOiBib29sZWFuKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5lbmFibGVkID0gZW5hYmxlZFxuICAgICAgICBcbiAgICAgICAgdGhpcy51cGRhdGVDb250ZW50Rm9yQ3VycmVudFN0YXRlKClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGdldCBlbmFibGVkKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHN1cGVyLmVuYWJsZWRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50RW5hYmxlZFN0YXRlKCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlZCkge1xuICAgICAgICAgICAgdGhpcy5hbHBoYSA9IDFcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuYWxwaGEgPSAwLjVcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy51c2VySW50ZXJhY3Rpb25FbmFibGVkID0gdGhpcy5lbmFibGVkXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBhZGRTdHlsZUNsYXNzKHN0eWxlQ2xhc3NOYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmFkZFN0eWxlQ2xhc3Moc3R5bGVDbGFzc05hbWUpXG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5zdHlsZUNsYXNzTmFtZSAhPSBzdHlsZUNsYXNzTmFtZSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50U3RhdGUuY2FsbCh0aGlzKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCB0aXRsZUxhYmVsKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RpdGxlTGFiZWxcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGdldCBpbWFnZVZpZXcoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5faW1hZ2VWaWV3XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBsYXlvdXRTdWJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmxheW91dFN1YnZpZXdzKClcbiAgICBcbiAgICAgICAgdmFyIGJvdW5kcyA9IHRoaXMuYm91bmRzXG4gICAgXG4gICAgICAgIHRoaXMuaG92ZXJUZXh0ID0gdGhpcy50aXRsZUxhYmVsLnRleHRcbiAgICAgICAgXG4gICAgICAgIC8vIEltYWdlIG9ubHkgaWYgdGV4dCBpcyBub3QgcHJlc2VudFxuICAgICAgICBpZiAoSVNfTk9UKHRoaXMuaW1hZ2VWaWV3LmhpZGRlbikgJiYgIUlTKHRoaXMudGl0bGVMYWJlbC50ZXh0KSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuaW1hZ2VWaWV3LmZyYW1lID0gYm91bmRzXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAvLyBUZXh0IG9ubHkgaWYgaW1hZ2UgaXMgbm90IHByZXNlbnRcbiAgICAgICAgaWYgKElTKHRoaXMuaW1hZ2VWaWV3LmhpZGRlbikgJiYgSVModGhpcy50aXRsZUxhYmVsLnRleHQpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciB0aXRsZUVsZW1lbnQgPSB0aGlzLnRpdGxlTGFiZWwudmlld0hUTUxFbGVtZW50XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLnN0eWxlLmxlZnQgPSB0aGlzLmNvbnRlbnRQYWRkaW5nXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuc3R5bGUucmlnaHQgPSB0aGlzLmNvbnRlbnRQYWRkaW5nXG4gICAgICAgICAgICAvLyB0aGlzLnRpdGxlTGFiZWwuc3R5bGUubWFyZ2luTGVmdCA9IFwiXCJcbiAgICAgICAgICAgIC8vIHRoaXMudGl0bGVMYWJlbC5zdHlsZS5yaWdodCA9IHRoaXMuY29udGVudFBhZGRpbmdcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5zdHlsZS50b3AgPSBcIjUwJVwiXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuc3R5bGUudHJhbnNmb3JtID0gXCJ0cmFuc2xhdGVZKC01MCUpXCJcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5mcmFtZSA9IG5ldyBVSVJlY3RhbmdsZShuaWwsIG5pbCwgbmlsLCBuaWwpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0aGlzLnVzZXNBdXRvbWF0aWNUaXRsZUZvbnRTaXplKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmFyIGhpZGRlbiA9IHRoaXMudGl0bGVMYWJlbC5oaWRkZW5cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuaGlkZGVuID0gWUVTXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLmZvbnRTaXplID0gMTVcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuZm9udFNpemUgPSBVSVRleHRWaWV3LmF1dG9tYXRpY2FsbHlDYWxjdWxhdGVkRm9udFNpemUoXG4gICAgICAgICAgICAgICAgICAgIG5ldyBVSVJlY3RhbmdsZShcbiAgICAgICAgICAgICAgICAgICAgICAgIDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ib3VuZHMuaGVpZ2h0LFxuICAgICAgICAgICAgICAgICAgICAgICAgMSAqXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwudmlld0hUTUxFbGVtZW50Lm9mZnNldFdpZHRoXG4gICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5pbnRyaW5zaWNDb250ZW50U2l6ZSgpLFxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuZm9udFNpemUsXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWluQXV0b21hdGljRm9udFNpemUsXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWF4QXV0b21hdGljRm9udFNpemVcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLmhpZGRlbiA9IGhpZGRlblxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgLy8gSW1hZ2UgYW5kIHRleHQgYm90aCBwcmVzZW50XG4gICAgICAgIGlmIChJU19OT1QodGhpcy5pbWFnZVZpZXcuaGlkZGVuKSAmJiBJUyh0aGlzLnRpdGxlTGFiZWwudGV4dCkpIHtcbiAgICBcbiAgICAgICAgICAgIGNvbnN0IGltYWdlU2hhcmVPZldpZHRoID0gMC4yNVxuICAgIFxuICAgICAgICAgICAgYm91bmRzID0gYm91bmRzLnJlY3RhbmdsZVdpdGhJbnNldCh0aGlzLmNvbnRlbnRQYWRkaW5nKVxuICAgIFxuICAgICAgICAgICAgY29uc3QgaW1hZ2VGcmFtZSA9IGJvdW5kcy5jb3B5KClcbiAgICAgICAgICAgIGltYWdlRnJhbWUud2lkdGggPSBib3VuZHMuaGVpZ2h0IC0gdGhpcy5jb250ZW50UGFkZGluZyAqIDAuNVxuICAgICAgICAgICAgdGhpcy5pbWFnZVZpZXcuZnJhbWUgPSBpbWFnZUZyYW1lXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciB0aXRsZUVsZW1lbnQgPSB0aGlzLnRpdGxlTGFiZWwudmlld0hUTUxFbGVtZW50XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5zdHlsZS5sZWZ0ID0gaW1hZ2VGcmFtZS5tYXgueCArIHRoaXMuY29udGVudFBhZGRpbmdcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5zdHlsZS5yaWdodCA9IHRoaXMuY29udGVudFBhZGRpbmdcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5zdHlsZS50b3AgPSBcIjUwJVwiXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuc3R5bGUudHJhbnNmb3JtID0gXCJ0cmFuc2xhdGVZKC01MCUpXCJcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKHRoaXMudXNlc0F1dG9tYXRpY1RpdGxlRm9udFNpemUpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB2YXIgaGlkZGVuID0gdGhpcy50aXRsZUxhYmVsLmhpZGRlblxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5oaWRkZW4gPSBZRVNcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuZm9udFNpemUgPSAxNVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5mb250U2l6ZSA9IFVJVGV4dFZpZXcuYXV0b21hdGljYWxseUNhbGN1bGF0ZWRGb250U2l6ZShcbiAgICAgICAgICAgICAgICAgICAgbmV3IFVJUmVjdGFuZ2xlKFxuICAgICAgICAgICAgICAgICAgICAgICAgMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIDAsXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJvdW5kcy5oZWlnaHQsXG4gICAgICAgICAgICAgICAgICAgICAgICAxICpcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC52aWV3SFRNTEVsZW1lbnQub2Zmc2V0V2lkdGhcbiAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLmludHJpbnNpY0NvbnRlbnRTaXplKCksXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5mb250U2l6ZSxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5taW5BdXRvbWF0aWNGb250U2l6ZSxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXhBdXRvbWF0aWNGb250U2l6ZVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuaGlkZGVuID0gaGlkZGVuXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5hcHBseUNsYXNzZXNBbmRTdHlsZXMoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgaW5pdFZpZXdTdHlsZVNlbGVjdG9ycygpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaW5pdFN0eWxlU2VsZWN0b3IoXCIuXCIgKyB0aGlzLnN0eWxlQ2xhc3NOYW1lLCBcImJhY2tncm91bmQtY29sb3I6IGxpZ2h0Ymx1ZTtcIilcbiAgICAgICAgXG4gICAgICAgIC8vIHZhciBzZWxlY3RvcldpdGhvdXRJbWFnZSA9IFwiLlwiICsgdGhpcy5zdHlsZUNsYXNzTmFtZSArIFwiIC5cIiArIHRoaXMuaW1hZ2VWaWV3LnN0eWxlQ2xhc3NOYW1lICsgXCIgKyAuXCIgKyB0aGlzLnRpdGxlTGFiZWwuc3R5bGVDbGFzc05hbWU7XG4gICAgICAgIFxuICAgICAgICAvLyB0aGlzLmluaXRTdHlsZVNlbGVjdG9yKFxuICAgICAgICAvLyAgICAgc2VsZWN0b3JXaXRob3V0SW1hZ2UsXG4gICAgICAgIC8vICAgICBcImxlZnQ6IFwiICsgdGhpcy5jb250ZW50UGFkZGluZyArIFwiO1wiICtcbiAgICAgICAgLy8gICAgIFwicmlnaHQ6IFwiICsgdGhpcy5jb250ZW50UGFkZGluZyArIFwiO1wiICtcbiAgICAgICAgLy8gICAgIFwidG9wOiA1MCU7XCIgK1xuICAgICAgICAvLyAgICAgXCJ0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XCIpO1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCIvLy88cmVmZXJlbmNlIHBhdGg9XCJVSUNvcmUvVUlCdXR0b24udHNcIi8+XG5cblxuY2xhc3MgSXRlbVZpZXcgZXh0ZW5kcyBVSUJ1dHRvbiB7XG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEOiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgbmlsLCBVSVRleHRWaWV3LnR5cGUuaGVhZGVyMylcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gSXRlbVZpZXdcbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlCdXR0b25cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHVwZGF0ZUNvbnRlbnRGb3JOb3JtYWxTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gVUlDb2xvci50cmFuc3BhcmVudENvbG9yXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC50ZXh0Q29sb3IgPSBVSUNvbG9yLmJsYWNrQ29sb3JcbiAgICAgICAgXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC5hbHBoYSA9IDFcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9ySG92ZXJlZFN0YXRlKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLmFscGhhID0gMC43NVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckhpZ2hsaWdodGVkU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLnRpdGxlTGFiZWwuYWxwaGEgPSAwLjVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGxheW91dFN1YnZpZXdzKCkge1xuICAgICAgICBcbiAgICAgICAgLy8gTm90IGNhbGxpbmcgc3VwZXIgb24gcHVycG9zZVxuICAgICAgICAvL3N1cGVyLmxheW91dFN1YnZpZXdzKCk7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBwYWRkaW5nID0gUm9vdFZpZXdDb250cm9sbGVyLnBhZGRpbmdMZW5ndGhcbiAgICAgICAgY29uc3QgbGFiZWxIZWlnaHQgPSBwYWRkaW5nICogMS41XG4gICAgICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLmlzU2luZ2xlTGluZSA9IE5PXG4gICAgICAgIFxuICAgICAgICB0aGlzLmltYWdlVmlldy5maWxsTW9kZSA9IFVJSW1hZ2VWaWV3LmZpbGxNb2RlLmFzcGVjdEZpbGxcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLnRleHRBbGlnbm1lbnQgPSBVSVRleHRWaWV3LnRleHRBbGlnbm1lbnQubGVmdFxuICAgICAgICBcbiAgICAgICAgdGhpcy5pbWFnZVZpZXcuZnJhbWUgPSBib3VuZHMucmVjdGFuZ2xlV2l0aEhlaWdodFJlbGF0aXZlVG9XaWR0aCg5IC8gMTYpXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC5mcmFtZSA9IHRoaXMuaW1hZ2VWaWV3LmZyYW1lLnJlY3RhbmdsZUZvck5leHRSb3coXG4gICAgICAgICAgICBwYWRkaW5nLFxuICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLmludHJpbnNpY0NvbnRlbnRIZWlnaHQoYm91bmRzLndpZHRoKVxuICAgICAgICApXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBpbnRyaW5zaWNDb250ZW50SGVpZ2h0KGNvbnN0cmFpbmluZ1dpZHRoOiBudW1iZXIgPSAwKTogbnVtYmVyIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHBhZGRpbmcgPSBSb290Vmlld0NvbnRyb2xsZXIucGFkZGluZ0xlbmd0aFxuICAgICAgICBjb25zdCBsYWJlbEhlaWdodCA9IHBhZGRpbmcgKiAxLjVcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGNvbnN0cmFpbmluZ1dpZHRoICogKDkgLyAxNikgKyBwYWRkaW5nICogMiArXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuaW50cmluc2ljQ29udGVudEhlaWdodChjb25zdHJhaW5pbmdXaWR0aClcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCJpZiAoXCJyZW1vdmVFbGVtZW50QXRJbmRleFwiIGluIEFycmF5LnByb3RvdHlwZSA9PSBOTykge1xuICAgIFxuICAgIChBcnJheS5wcm90b3R5cGUgYXMgYW55KS5yZW1vdmVFbGVtZW50QXRJbmRleCA9IGZ1bmN0aW9uICh0aGlzOiBBcnJheTxhbnk+LCBpbmRleDogbnVtYmVyKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoaW5kZXggPj0gMCAmJiBpbmRleCA8IHRoaXMubGVuZ3RoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3BsaWNlKGluZGV4LCAxKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxufVxuXG5cbmludGVyZmFjZSBBcnJheTxUPiB7XG4gICAgXG4gICAgcmVtb3ZlRWxlbWVudEF0SW5kZXgoaW5kZXg6IG51bWJlcik7XG4gICAgXG59XG5cblxuaWYgKFwicmVtb3ZlRWxlbWVudFwiIGluIEFycmF5LnByb3RvdHlwZSA9PSBOTykge1xuICAgIFxuICAgIChBcnJheS5wcm90b3R5cGUgYXMgYW55KS5yZW1vdmVFbGVtZW50ID0gZnVuY3Rpb24gKHRoaXM6IEFycmF5PGFueT4sIGVsZW1lbnQpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMucmVtb3ZlRWxlbWVudEF0SW5kZXgodGhpcy5pbmRleE9mKGVsZW1lbnQpKVxuICAgICAgICBcbiAgICB9XG4gICAgXG59XG5cblxuaW50ZXJmYWNlIEFycmF5PFQ+IHtcbiAgICBcbiAgICByZW1vdmVFbGVtZW50KGVsZW1lbnQ6IFQpO1xuICAgIFxufVxuXG5cbmlmIChcImluc2VydEVsZW1lbnRBdEluZGV4XCIgaW4gQXJyYXkucHJvdG90eXBlID09IE5PKSB7XG4gICAgXG4gICAgKEFycmF5LnByb3RvdHlwZSBhcyBhbnkpLmluc2VydEVsZW1lbnRBdEluZGV4ID0gZnVuY3Rpb24gKHRoaXM6IEFycmF5PGFueT4sIGluZGV4OiBudW1iZXIsIGVsZW1lbnQ6IGFueSkge1xuICAgICAgICBcbiAgICAgICAgaWYgKGluZGV4ID49IDAgJiYgaW5kZXggPD0gdGhpcy5sZW5ndGgpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zcGxpY2UoaW5kZXgsIDAsIGVsZW1lbnQpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG59XG5cblxuaW50ZXJmYWNlIEFycmF5PFQ+IHtcbiAgICBcbiAgICBpbnNlcnRFbGVtZW50QXRJbmRleChpbmRleDogbnVtYmVyLCBlbGVtZW50OiBUKTtcbiAgICBcbn1cblxuXG5pZiAoXCJyZXBsYWNlRWxlbWVudEF0SW5kZXhcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICAoQXJyYXkucHJvdG90eXBlIGFzIGFueSkucmVwbGFjZUVsZW1lbnRBdEluZGV4ID0gZnVuY3Rpb24gKHRoaXM6IEFycmF5PGFueT4sIGluZGV4OiBudW1iZXIsIGVsZW1lbnQ6IGFueSkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5yZW1vdmVFbGVtZW50QXRJbmRleChpbmRleClcbiAgICAgICAgdGhpcy5pbnNlcnRFbGVtZW50QXRJbmRleChpbmRleCwgZWxlbWVudClcbiAgICAgICAgXG4gICAgfVxuICAgIFxufVxuXG5cbmludGVyZmFjZSBBcnJheTxUPiB7XG4gICAgXG4gICAgcmVwbGFjZUVsZW1lbnRBdEluZGV4KGluZGV4OiBudW1iZXIsIGVsZW1lbnQ6IFQpO1xuICAgIFxufVxuXG5cbmlmIChcImNvbnRhaW5zXCIgaW4gQXJyYXkucHJvdG90eXBlID09IE5PKSB7XG4gICAgXG4gICAgKEFycmF5LnByb3RvdHlwZSBhcyBhbnkpLmNvbnRhaW5zID0gZnVuY3Rpb24gKHRoaXM6IEFycmF5PGFueT4sIGVsZW1lbnQpIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9ICh0aGlzLmluZGV4T2YoZWxlbWVudCkgIT0gLTEpXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxufVxuXG5pZiAoXCJjb250YWluc0FueVwiIGluIEFycmF5LnByb3RvdHlwZSA9PSBOTykge1xuICAgIFxuICAgIChBcnJheS5wcm90b3R5cGUgYXMgYW55KS5jb250YWluc0FueSA9IGZ1bmN0aW9uICh0aGlzOiBBcnJheTxhbnk+LCBlbGVtZW50czogYW55W10pIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuYW55TWF0Y2goZnVuY3Rpb24gKGVsZW1lbnQsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnRzLmNvbnRhaW5zKGVsZW1lbnQpXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbn1cblxuXG5pbnRlcmZhY2UgQXJyYXk8VD4ge1xuICAgIFxuICAgIGNvbnRhaW5zKGVsZW1lbnQ6IFQpOiBib29sZWFuO1xuICAgIFxuICAgIGNvbnRhaW5zQW55KGVsZW1lbnQ6IFRbXSk6IGJvb2xlYW47XG4gICAgXG59XG5cblxuaWYgKFwiYW55TWF0Y2hcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICAoQXJyYXkucHJvdG90eXBlIGFzIGFueSkuYW55TWF0Y2ggPSBmdW5jdGlvbiAoXG4gICAgICAgIHRoaXM6IEFycmF5PGFueT4sXG4gICAgICAgIGZ1bmN0aW9uVG9DYWxsOiAodmFsdWU6IGFueSwgaW5kZXg6IG51bWJlciwgYXJyYXk6IGFueVtdKSA9PiBib29sZWFuXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSAodGhpcy5maW5kSW5kZXgoZnVuY3Rpb25Ub0NhbGwpID4gLTEpXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbn1cblxuaWYgKFwibm9uZU1hdGNoXCIgaW4gQXJyYXkucHJvdG90eXBlID09IE5PKSB7XG4gICAgXG4gICAgKEFycmF5LnByb3RvdHlwZSBhcyBhbnkpLm5vbmVNYXRjaCA9IGZ1bmN0aW9uIChcbiAgICAgICAgdGhpczogQXJyYXk8YW55PixcbiAgICAgICAgZnVuY3Rpb25Ub0NhbGw6ICh2YWx1ZTogYW55LCBpbmRleDogbnVtYmVyLCBhcnJheTogYW55W10pID0+IGJvb2xlYW5cbiAgICApIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9ICh0aGlzLmZpbmRJbmRleChmdW5jdGlvblRvQ2FsbCkgPT0gLTEpXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbn1cblxuaWYgKFwiYWxsTWF0Y2hcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICAoQXJyYXkucHJvdG90eXBlIGFzIGFueSkuYWxsTWF0Y2ggPSBmdW5jdGlvbiAoXG4gICAgICAgIHRoaXM6IEFycmF5PGFueT4sXG4gICAgICAgIGZ1bmN0aW9uVG9DYWxsOiAodmFsdWU6IGFueSwgaW5kZXg6IG51bWJlciwgYXJyYXk6IGFueVtdKSA9PiBib29sZWFuXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBmdW5jdGlvbiByZXZlcnNlZEZ1bmN0aW9uKHZhbHVlOiBhbnksIGluZGV4OiBudW1iZXIsIGFycmF5OiBhbnlbXSkge1xuICAgICAgICAgICAgcmV0dXJuICFmdW5jdGlvblRvQ2FsbCh2YWx1ZSwgaW5kZXgsIGFycmF5KVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSAodGhpcy5maW5kSW5kZXgocmV2ZXJzZWRGdW5jdGlvbikgPT0gLTEpXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbn1cblxuXG5pbnRlcmZhY2UgQXJyYXk8VD4ge1xuICAgIFxuICAgIGFueU1hdGNoKHByZWRpY2F0ZTogKHZhbHVlOiBULCBpbmRleDogbnVtYmVyLCBvYmo6IFRbXSkgPT4gYm9vbGVhbik6IGJvb2xlYW5cbiAgICBcbiAgICBub25lTWF0Y2gocHJlZGljYXRlOiAodmFsdWU6IFQsIGluZGV4OiBudW1iZXIsIG9iajogVFtdKSA9PiBib29sZWFuKTogYm9vbGVhblxuICAgIFxuICAgIGFsbE1hdGNoKHByZWRpY2F0ZTogKHZhbHVlOiBULCBpbmRleDogbnVtYmVyLCBvYmo6IFRbXSkgPT4gYm9vbGVhbik6IGJvb2xlYW5cbiAgICBcbn1cblxuXG5pZiAoXCJncm91cGVkQnlcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICBBcnJheS5wcm90b3R5cGUuZ3JvdXBlZEJ5ID0gZnVuY3Rpb24gKHRoaXM6IEFycmF5PGFueT4sIGZ1bmNQcm9wKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlZHVjZShmdW5jdGlvbiAoYWNjLCB2YWwpIHtcbiAgICAgICAgICAgIChhY2NbZnVuY1Byb3AodmFsKV0gPSBhY2NbZnVuY1Byb3AodmFsKV0gfHwgW10pLnB1c2godmFsKVxuICAgICAgICAgICAgcmV0dXJuIGFjY1xuICAgICAgICB9LCB7fSlcbiAgICB9XG4gICAgXG59XG5cblxuaW50ZXJmYWNlIEFycmF5PFQ+IHtcbiAgICBcbiAgICBncm91cGVkQnkoa2V5RnVuY3Rpb246IChpdGVtOiBUKSA9PiBhbnkpOiB7IFtrZXk6IHN0cmluZ106IEFycmF5PFQ+IH07XG4gICAgXG59XG5cblxuaWYgKFwiZmlyc3RFbGVtZW50XCIgaW4gQXJyYXkucHJvdG90eXBlID09IE5PKSB7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KEFycmF5LnByb3RvdHlwZSwgXCJmaXJzdEVsZW1lbnRcIiwge1xuICAgICAgICBnZXQ6IGZ1bmN0aW9uIGZpcnN0RWxlbWVudCh0aGlzOiBBcnJheTxhbnk+KSB7XG4gICAgICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzWzBdXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIH0sXG4gICAgICAgIHNldDogZnVuY3Rpb24gKHRoaXM6IEFycmF5PGFueT4sIGVsZW1lbnQ6IGFueSkge1xuICAgICAgICAgICAgaWYgKHRoaXMubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgICAgICB0aGlzLnB1c2goZWxlbWVudClcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXNbMF0gPSBlbGVtZW50XG4gICAgICAgIH1cbiAgICB9KVxufVxuXG5pZiAoXCJsYXN0RWxlbWVudFwiIGluIEFycmF5LnByb3RvdHlwZSA9PSBOTykge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShBcnJheS5wcm90b3R5cGUsIFwibGFzdEVsZW1lbnRcIiwge1xuICAgICAgICBnZXQ6IGZ1bmN0aW9uIGxhc3RFbGVtZW50KHRoaXM6IEFycmF5PGFueT4pIHtcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXNbdGhpcy5sZW5ndGggLSAxXVxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9LFxuICAgICAgICBzZXQ6IGZ1bmN0aW9uICh0aGlzOiBBcnJheTxhbnk+LCBlbGVtZW50OiBhbnkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wdXNoKGVsZW1lbnQpXG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzW3RoaXMubGVuZ3RoIC0gMV0gPSBlbGVtZW50XG4gICAgICAgIH1cbiAgICB9KVxufVxuXG5pZiAoXCJldmVyeUVsZW1lbnRcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoQXJyYXkucHJvdG90eXBlLCBcImV2ZXJ5RWxlbWVudFwiLCB7XG4gICAgICAgIFxuICAgICAgICBnZXQ6IGZ1bmN0aW9uIGV2ZXJ5RWxlbWVudCh0aGlzOiBBcnJheTxhbnk+KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciB2YWx1ZUtleXMgPSBbXVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCB0YXJnZXRGdW5jdGlvbiA9IChvYmplY3RzKSA9PiB7XG4gICAgXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubWFwKChlbGVtZW50LCBpbmRleCwgYXJyYXkpID0+IHtcbiAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50RnVuY3Rpb24gPSAoVUlPYmplY3QudmFsdWVGb3JLZXlQYXRoKHZhbHVlS2V5cy5qb2luKFwiLlwiKSwgZWxlbWVudCkgYXMgRnVuY3Rpb24pLmJpbmQoXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgb2JqZWN0c1xuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgIFxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZWxlbWVudEZ1bmN0aW9uKClcbiAgICAgICAgXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBQcm94eShcbiAgICAgICAgICAgICAgICB0YXJnZXRGdW5jdGlvbixcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBnZXQ6ICh0YXJnZXQsIGtleSwgcmVjZWl2ZXIpID0+IHtcbiAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChrZXkgPT0gXCJVSV9lbGVtZW50VmFsdWVzXCIpIHtcbiAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubWFwKChlbGVtZW50LCBpbmRleCwgYXJyYXkpID0+IFVJT2JqZWN0LnZhbHVlRm9yS2V5UGF0aChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVLZXlzLmpvaW4oXCIuXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlS2V5cy5wdXNoKGtleSlcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHNldDogKHRhcmdldCwga2V5LCB2YWx1ZSwgcmVjZWl2ZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVLZXlzLnB1c2goa2V5KVxuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvckVhY2goKGVsZW1lbnQsIGluZGV4LCBhcnJheSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFVJT2JqZWN0LnNldFZhbHVlRm9yS2V5UGF0aCh2YWx1ZUtleXMuam9pbihcIi5cIiksIHZhbHVlLCBlbGVtZW50LCBZRVMpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgICAgICBcbiAgICAgICAgfSxcbiAgICAgICAgc2V0OiBmdW5jdGlvbiAodGhpczogQXJyYXk8YW55PiwgZWxlbWVudDogYW55KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7ICsraSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXNbaV0gPSBlbGVtZW50XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9KVxuICAgIFxufVxuXG5cbnR5cGUgVUlFdmVyeUVsZW1lbnRJdGVtPFQ+ID0ge1xuICAgIFxuICAgIFtQIGluIGtleW9mIFRdOiBVSUV2ZXJ5RWxlbWVudEl0ZW08VFtQXT5cbiAgICBcbn0gJiB7XG4gICAgXG4gICAgVUlfZWxlbWVudFZhbHVlcz86IFRbXTtcbiAgICBcbn0gJiBUXG5cbmludGVyZmFjZSBBcnJheTxUPiB7XG4gICAgXG4gICAgZmlyc3RFbGVtZW50OiBUO1xuICAgIGxhc3RFbGVtZW50OiBUO1xuICAgIFxuICAgIGV2ZXJ5RWxlbWVudDogVUlFdmVyeUVsZW1lbnRJdGVtPFQ+O1xuICAgIFxufVxuXG5cbmlmIChcImNvcHlcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICAoQXJyYXkucHJvdG90eXBlIGFzIGFueSkuY29weSA9IGZ1bmN0aW9uICh0aGlzOiBBcnJheTxhbnk+KSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLnNsaWNlKDApXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxufVxuXG5cbmludGVyZmFjZSBBcnJheTxUPiB7XG4gICAgXG4gICAgY29weSgpOiBBcnJheTxUPjtcbiAgICBcbn1cblxuXG5pZiAoXCJhcnJheUJ5UmVwZWF0aW5nXCIgaW4gQXJyYXkucHJvdG90eXBlID09IE5PKSB7XG4gICAgXG4gICAgKEFycmF5LnByb3RvdHlwZSBhcyBhbnkpLmFycmF5QnlSZXBlYXRpbmcgPSBmdW5jdGlvbiAodGhpczogQXJyYXk8YW55PiwgbnVtYmVyT2ZSZXBldGl0aW9uczogbnVtYmVyKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IFtdXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbnVtYmVyT2ZSZXBldGl0aW9uczsgaSsrKSB7XG4gICAgICAgICAgICB0aGlzLmZvckVhY2goZnVuY3Rpb24gKGVsZW1lbnQsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKGVsZW1lbnQpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG59XG5cblxuaW50ZXJmYWNlIEFycmF5PFQ+IHtcbiAgICBcbiAgICBhcnJheUJ5UmVwZWF0aW5nKG51bWJlck9mUmVwZXRpdGlvbnM6IG51bWJlcik6IEFycmF5PFQ+O1xuICAgIFxufVxuXG5cbmlmIChcImFycmF5QnlUcmltbWluZ1RvTGVuZ3RoSWZMb25nZXJcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICAoQXJyYXkucHJvdG90eXBlIGFzIGFueSkuYXJyYXlCeVRyaW1taW5nVG9MZW5ndGhJZkxvbmdlciA9IGZ1bmN0aW9uICh0aGlzOiBBcnJheTxhbnk+LCBtYXhMZW5ndGg6IG51bWJlcikge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBbXVxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG1heExlbmd0aCAmJiBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgcmVzdWx0LnB1c2godGhpc1tpXSlcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxufVxuXG5cbmludGVyZmFjZSBBcnJheTxUPiB7XG4gICAgXG4gICAgYXJyYXlCeVRyaW1taW5nVG9MZW5ndGhJZkxvbmdlcihtYXhMZW5ndGg6IG51bWJlcik6IEFycmF5PFQ+O1xuICAgIFxufVxuXG5cbmlmIChcInN1bW1lZFZhbHVlXCIgaW4gQXJyYXkucHJvdG90eXBlID09IE5PKSB7XG4gICAgXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KEFycmF5LnByb3RvdHlwZSwgXCJzdW1tZWRWYWx1ZVwiLCB7XG4gICAgICAgIGdldDogZnVuY3Rpb24gc3VtbWVkVmFsdWUodGhpczogQXJyYXk8YW55Pikge1xuICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5yZWR1Y2UoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gYSArIGJcbiAgICAgICAgICAgIH0sIDApXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIH1cbiAgICB9KVxuICAgIFxufVxuXG5cbmludGVyZmFjZSBBcnJheTxUPiB7XG4gICAgXG4gICAgcmVhZG9ubHkgc3VtbWVkVmFsdWU6IFQ7XG4gICAgXG59XG5cblxuLy8gV2FybiBpZiBvdmVycmlkaW5nIGV4aXN0aW5nIG1ldGhvZFxuaWYgKFwiaXNFcXVhbFRvQXJyYXlcIiBpbiBBcnJheS5wcm90b3R5cGUgPT0gWUVTKSB7XG4gICAgY29uc29sZS53YXJuKFxuICAgICAgICBcIk92ZXJyaWRpbmcgZXhpc3RpbmcgQXJyYXkucHJvdG90eXBlLmlzRXF1YWxUb0FycmF5LiBQb3NzaWJsZSBjYXVzZXM6IE5ldyBBUEkgZGVmaW5lcyB0aGUgbWV0aG9kLCB0aGVyZSdzIGEgZnJhbWV3b3JrIGNvbmZsaWN0IG9yIHlvdSd2ZSBnb3QgZG91YmxlIGluY2x1c2lvbnMgaW4geW91ciBjb2RlLlwiKVxufVxuLy8gYXR0YWNoIHRoZSAuZXF1YWxzIG1ldGhvZCB0byBBcnJheSdzIHByb3RvdHlwZSB0byBjYWxsIGl0IG9uIGFueSBhcnJheVxuQXJyYXkucHJvdG90eXBlLmlzRXF1YWxUb0FycmF5ID0gZnVuY3Rpb24gKGFycmF5OiBhbnlbXSwga2V5UGF0aD86IHN0cmluZykge1xuICAgIFxuICAgIC8vIGlmIHRoZSBvdGhlciBhcnJheSBpcyBhIGZhbHN5IHZhbHVlLCByZXR1cm5cbiAgICBpZiAoIWFycmF5KSB7XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgICBcbiAgICAvLyBjb21wYXJlIGxlbmd0aHMgLSBjYW4gc2F2ZSBhIGxvdCBvZiB0aW1lIFxuICAgIGlmICh0aGlzLmxlbmd0aCAhPSBhcnJheS5sZW5ndGgpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfVxuICAgIFxuICAgIHZhciBpID0gMFxuICAgIGNvbnN0IGwgPSB0aGlzLmxlbmd0aFxuICAgIGZvciAoOyBpIDwgbDsgaSsrKSB7XG4gICAgICAgIFxuICAgICAgICAvLyBDaGVjayBpZiB3ZSBoYXZlIG5lc3RlZCBhcnJheXNcbiAgICAgICAgaWYgKHRoaXNbaV0gaW5zdGFuY2VvZiBBcnJheSAmJiBhcnJheVtpXSBpbnN0YW5jZW9mIEFycmF5ICYmICFrZXlQYXRoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIHJlY3Vyc2UgaW50byB0aGUgbmVzdGVkIGFycmF5c1xuICAgICAgICAgICAgaWYgKCF0aGlzW2ldLmlzRXF1YWxUb0FycmF5KGFycmF5W2ldKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoa2V5UGF0aCAmJiBVSU9iamVjdC52YWx1ZUZvcktleVBhdGgoa2V5UGF0aCwgdGhpc1tpXSkgIT0gVUlPYmplY3QudmFsdWVGb3JLZXlQYXRoKGtleVBhdGgsIGFycmF5W2ldKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXNbaV0gIT0gYXJyYXlbaV0pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy8gV2FybmluZyAtIHR3byBkaWZmZXJlbnQgb2JqZWN0IGluc3RhbmNlcyB3aWxsIG5ldmVyIGJlIGVxdWFsOiB7eDoyMH0gIT0ge3g6MjB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICByZXR1cm4gdHJ1ZVxuICAgIFxufVxuXG4vLyBIaWRlIG1ldGhvZCBmcm9tIGZvci1pbiBsb29wc1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KEFycmF5LnByb3RvdHlwZSwgXCJpc0VxdWFsVG9BcnJheVwiLCB7IGVudW1lcmFibGU6IGZhbHNlIH0pXG5cblxuaW50ZXJmYWNlIEFycmF5PFQ+IHtcbiAgICBcbiAgICBpc0VxdWFsVG9BcnJheShhcnJheTogQXJyYXk8VD4sIGtleVBhdGg/OiBzdHJpbmcpOiBib29sZWFuO1xuICAgIFxufVxuXG5cblxuaWYgKFwiZm9yRWFjaFwiIGluIE9iamVjdC5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICAoT2JqZWN0LnByb3RvdHlwZSBhcyBhbnkpLmZvckVhY2ggPSBmdW5jdGlvbiAodGhpczogT2JqZWN0LCBjYWxsYmFja0Z1bmN0aW9uOiAodmFsdWU6IGFueSwga2V5OiBzdHJpbmcpID0+IHZvaWQpIHtcbiAgICAgICAgY29uc3Qga2V5cyA9IE9iamVjdC5rZXlzKHRoaXMpXG4gICAgICAgIGtleXMuZm9yRWFjaChmdW5jdGlvbiAoa2V5LCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrRnVuY3Rpb24odGhpc1trZXldLCBrZXkpXG4gICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICB9XG4gICAgXG4gICAgLy8gSGlkZSBtZXRob2QgZnJvbSBmb3ItaW4gbG9vcHNcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoT2JqZWN0LnByb3RvdHlwZSwgXCJmb3JFYWNoXCIsIHsgZW51bWVyYWJsZTogZmFsc2UgfSlcbiAgICBcbn1cblxuXG5pbnRlcmZhY2UgT2JqZWN0IHtcbiAgICBcbiAgICBmb3JFYWNoKGNhbGxiYWNrRnVuY3Rpb246ICh2YWx1ZTogYW55LCBrZXk6IHN0cmluZykgPT4gdm9pZCk6IHZvaWQ7XG4gICAgXG59XG5cblxuaWYgKFwiYWxsVmFsdWVzXCIgaW4gT2JqZWN0LnByb3RvdHlwZSA9PSBOTykge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShPYmplY3QucHJvdG90eXBlLCBcImFsbFZhbHVlc1wiLCB7XG4gICAgICAgIGdldDogZnVuY3Rpb24gKHRoaXM6IE9iamVjdCkge1xuICAgICAgICAgICAgY29uc3QgdmFsdWVzID0gW11cbiAgICAgICAgICAgIHRoaXMuZm9yRWFjaChmdW5jdGlvbiAodmFsdWU6IGFueSkge1xuICAgICAgICAgICAgICAgIHZhbHVlcy5wdXNoKHZhbHVlKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZXNcbiAgICAgICAgfVxuICAgIH0pXG59XG5cblxuaW50ZXJmYWNlIE9iamVjdCB7XG4gICAgXG4gICAgcmVhZG9ubHkgYWxsVmFsdWVzOiBBcnJheTxhbnk+O1xuICAgIFxufVxuXG5cbmlmIChcImFsbEtleXNcIiBpbiBPYmplY3QucHJvdG90eXBlID09IE5PKSB7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KE9iamVjdC5wcm90b3R5cGUsIFwiYWxsS2V5c1wiLCB7XG4gICAgICAgIGdldDogZnVuY3Rpb24gKHRoaXM6IE9iamVjdCkge1xuICAgICAgICAgICAgY29uc3QgdmFsdWVzID0gT2JqZWN0LmtleXModGhpcylcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZXNcbiAgICAgICAgfVxuICAgIH0pXG59XG5cblxuaW50ZXJmYWNlIE9iamVjdCB7XG4gICAgXG4gICAgcmVhZG9ubHkgYWxsS2V5czogc3RyaW5nW107XG4gICAgXG59XG5cblxuaWYgKFwib2JqZWN0QnlDb3B5aW5nVmFsdWVzUmVjdXJzaXZlbHlGcm9tT2JqZWN0XCIgaW4gT2JqZWN0LnByb3RvdHlwZSA9PSBOTykge1xuICAgIFxuICAgIChPYmplY3QucHJvdG90eXBlIGFzIGFueSkub2JqZWN0QnlDb3B5aW5nVmFsdWVzUmVjdXJzaXZlbHlGcm9tT2JqZWN0ID0gZnVuY3Rpb24gKHRoaXM6IE9iamVjdCwgb2JqZWN0OiBhbnkpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBmdW5jdGlvbiBpc0FuT2JqZWN0KGl0ZW06IGFueSkge1xuICAgICAgICAgICAgcmV0dXJuIChpdGVtICYmIHR5cGVvZiBpdGVtID09PSBcIm9iamVjdFwiICYmICFBcnJheS5pc0FycmF5KGl0ZW0pKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBmdW5jdGlvbiBtZXJnZVJlY3Vyc2l2ZWx5KHRhcmdldDogYW55LCBzb3VyY2U6IGFueSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBvdXRwdXQgPSBPYmplY3QuYXNzaWduKHt9LCB0YXJnZXQpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChpc0FuT2JqZWN0KHRhcmdldCkgJiYgaXNBbk9iamVjdChzb3VyY2UpKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoc291cmNlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc0FuT2JqZWN0KHNvdXJjZVtrZXldKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiAoIShrZXkgaW4gdGFyZ2V0KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgT2JqZWN0LmFzc2lnbihvdXRwdXQsIHsgW2tleV06IHNvdXJjZVtrZXldIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgb3V0cHV0W2tleV0gPSBtZXJnZVJlY3Vyc2l2ZWx5KHRhcmdldFtrZXldLCBzb3VyY2Vba2V5XSlcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgLy99XG4gICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmFzc2lnbihvdXRwdXQsIHsgW2tleV06IHNvdXJjZVtrZXldIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gb3V0cHV0XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbWVyZ2VSZWN1cnNpdmVseSh0aGlzLCBvYmplY3QpXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICAvLyBIaWRlIG1ldGhvZCBmcm9tIGZvci1pbiBsb29wc1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShPYmplY3QucHJvdG90eXBlLCBcIm9iamVjdEJ5Q29weWluZ1ZhbHVlc1JlY3Vyc2l2ZWx5RnJvbU9iamVjdFwiLCB7IGVudW1lcmFibGU6IGZhbHNlIH0pXG4gICAgXG59XG5cblxuaW50ZXJmYWNlIE9iamVjdCB7XG4gICAgXG4gICAgb2JqZWN0QnlDb3B5aW5nVmFsdWVzUmVjdXJzaXZlbHlGcm9tT2JqZWN0PFQ+KG9iamVjdDogVCk6IHRoaXMgJiBUO1xuICAgIFxufVxuXG5cblxuaWYgKFwiY29udGFpbnNcIiBpbiBTdHJpbmcucHJvdG90eXBlID09IE5PKSB7XG4gICAgXG4gICAgKFN0cmluZy5wcm90b3R5cGUgYXMgYW55KS5jb250YWlucyA9IGZ1bmN0aW9uICh0aGlzOiBTdHJpbmcsIHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gKHRoaXMuaW5kZXhPZihzdHJpbmcpICE9IC0xKVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbn1cblxuXG5pbnRlcmZhY2UgU3RyaW5nIHtcbiAgICBcbiAgICBjb250YWlucyhzdHJpbmcpOiBib29sZWFuO1xuICAgIFxufVxuXG5cbmlmIChcImNhcGl0YWxpemVkU3RyaW5nXCIgaW4gU3RyaW5nLnByb3RvdHlwZSA9PSBOTykge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShPYmplY3QucHJvdG90eXBlLCBcImNhcGl0YWxpemVkU3RyaW5nXCIsIHtcbiAgICAgICAgZ2V0OiBmdW5jdGlvbiAodGhpczogU3RyaW5nKSB7XG4gICAgICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgdGhpcy5zbGljZSgxKS50b0xvd2VyQ2FzZSgpXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIH1cbiAgICB9KVxufVxuXG5cbmludGVyZmFjZSBTdHJpbmcge1xuICAgIFxuICAgIHJlYWRvbmx5IGNhcGl0YWxpemVkU3RyaW5nOiBzdHJpbmc7XG4gICAgXG59XG5cblxuaWYgKFwibnVtZXJpY2FsVmFsdWVcIiBpbiBTdHJpbmcucHJvdG90eXBlID09IE5PKSB7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwibnVtZXJpY2FsVmFsdWVcIiwge1xuICAgICAgICBnZXQ6IGZ1bmN0aW9uIG51bWVyaWNhbFZhbHVlKHRoaXM6IHN0cmluZykge1xuICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gTnVtYmVyKHRoaXMpXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIH1cbiAgICB9KVxufVxuXG5cbmludGVyZmFjZSBTdHJpbmcge1xuICAgIFxuICAgIHJlYWRvbmx5IG51bWVyaWNhbFZhbHVlOiBudW1iZXI7XG4gICAgXG59XG5cblxuaWYgKFwiaXNBU3RyaW5nXCIgaW4gU3RyaW5nLnByb3RvdHlwZSA9PSBOTykge1xuICAgIFxuICAgIChTdHJpbmcucHJvdG90eXBlIGFzIGFueSkuaXNBU3RyaW5nID0gWUVTXG4gICAgXG59XG5cblxuaW50ZXJmYWNlIFN0cmluZyB7XG4gICAgXG4gICAgaXNBU3RyaW5nOiBib29sZWFuO1xuICAgIFxufVxuXG5cbmlmIChcImlzQU51bWJlclwiIGluIE51bWJlci5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICAoTnVtYmVyLnByb3RvdHlwZSBhcyBhbnkpLmlzQU51bWJlciA9IFlFU1xuICAgIFxufVxuXG5cbmludGVyZmFjZSBOdW1iZXIge1xuICAgIFxuICAgIGlzQU51bWJlcjogYm9vbGVhbjtcbiAgICBcbn1cblxuXG5cblxuXG5pZiAoXCJpbnRlZ2VyVmFsdWVcIiBpbiBOdW1iZXIucHJvdG90eXBlID09IE5PKSB7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KE51bWJlci5wcm90b3R5cGUsIFwiaW50ZWdlclZhbHVlXCIsIHtcbiAgICAgICAgZ2V0OiBmdW5jdGlvbiAodGhpczogbnVtYmVyKSB7XG4gICAgICAgICAgICBjb25zdCByZXN1bHQgPSBwYXJzZUludChcIlwiICsgKE1hdGgucm91bmQodGhpcykgKyAwLjUpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9XG4gICAgfSlcbn1cblxuXG5pbnRlcmZhY2UgTnVtYmVyIHtcbiAgICBcbiAgICByZWFkb25seSBpbnRlZ2VyVmFsdWU6IG51bWJlcjtcbiAgICBcbn1cblxuXG5cbmNsYXNzIFByaW1pdGl2ZU51bWJlciB7XG4gICAgXG4gICAgLy8gQHRzLWlnbm9yZVxuICAgIHN0YXRpYyBbU3ltYm9sLmhhc0luc3RhbmNlXSh4KSB7XG4gICAgICAgIHJldHVyblxuICAgIH1cbiAgICBcbn1cblxuXG5pZiAoXCJpbnRlZ2VyVmFsdWVcIiBpbiBCb29sZWFuLnByb3RvdHlwZSA9PSBOTykge1xuICAgIFxuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShCb29sZWFuLnByb3RvdHlwZSwgXCJpbnRlZ2VyVmFsdWVcIiwge1xuICAgICAgICBnZXQ6IGZ1bmN0aW9uICh0aGlzOiBib29sZWFuKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0aGlzID09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICByZXR1cm4gMVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gMFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9KVxuICAgIFxufVxuXG5cbmludGVyZmFjZSBCb29sZWFuIHtcbiAgICBcbiAgICByZWFkb25seSBpbnRlZ2VyVmFsdWU6IG51bWJlcjtcbiAgICBcbn1cblxuXG5pZiAoXCJkYXRlU3RyaW5nXCIgaW4gRGF0ZS5wcm90b3R5cGUgPT0gTk8pIHtcbiAgICBcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoRGF0ZS5wcm90b3R5cGUsIFwiZGF0ZVN0cmluZ1wiLCB7XG4gICAgICAgIGdldDogZnVuY3Rpb24gZGF0ZVN0cmluZyh0aGlzOiBEYXRlKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IChcIjBcIiArIHRoaXMuZ2V0RGF0ZSgpKS5zbGljZSgtMikgKyBcIi1cIiArIChcIjBcIiArICh0aGlzLmdldE1vbnRoKCkgKyAxKSkuc2xpY2UoLTIpICsgXCItXCIgK1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0RnVsbFllYXIoKSArIFwiIFwiICsgKFwiMFwiICsgdGhpcy5nZXRIb3VycygpKS5zbGljZSgtMikgKyBcIjpcIiArXG4gICAgICAgICAgICAgICAgKFwiMFwiICsgdGhpcy5nZXRNaW51dGVzKCkpLnNsaWNlKC0yKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIH0pXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuaW50ZXJmYWNlIERhdGUge1xuICAgIFxuICAgIHJlYWRvbmx5IGRhdGVTdHJpbmc6IHN0cmluZztcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG4iLCIvLy88cmVmZXJlbmNlIHBhdGg9XCJVSUNvcmUvVUlWaWV3LnRzXCIvPlxuLy8vPHJlZmVyZW5jZSBwYXRoPVwiUm9vdFZpZXdDb250cm9sbGVyLnRzXCIvPlxuLy8vPHJlZmVyZW5jZSBwYXRoPVwiSXRlbVZpZXcudHNcIi8+XG4vLy88cmVmZXJlbmNlIHBhdGg9XCJVSUNvcmUvVUlDb3JlRXh0ZW5zaW9ucy50c1wiLz5cblxuXG5jbGFzcyBUYWJsZVZpZXcgZXh0ZW5kcyBVSVZpZXcge1xuICAgIFxuICAgIHByaXZhdGUgaXRlbVZpZXdzOiBJdGVtVmlld1tdID0gW11cbiAgICBwcml2YXRlIF9pdGVtczogeyBpZDogc3RyaW5nOyB0aXRsZTogc3RyaW5nOyBpbWc6IHN0cmluZyB9W10gPSBbXVxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRDogc3RyaW5nKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IFRhYmxlVmlld1xuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSVZpZXdcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBpdGVtcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2l0ZW1zXG4gICAgfVxuICAgIFxuICAgIHNldCBpdGVtcyhpdGVtczoge1xuICAgICAgICBpZDogc3RyaW5nXG4gICAgICAgIHRpdGxlOiBzdHJpbmcsIGltZzogc3RyaW5nXG4gICAgfVtdKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9pdGVtcyA9IGl0ZW1zXG4gICAgICAgIFxuICAgICAgICB0aGlzLml0ZW1WaWV3cy5ldmVyeUVsZW1lbnQucmVtb3ZlRnJvbVN1cGVydmlldygpXG4gICAgICAgIFxuICAgICAgICB0aGlzLml0ZW1WaWV3cyA9IFtdXG4gICAgICAgIFxuICAgICAgICBpdGVtcy5mb3JFYWNoKCh2YWx1ZSwgaW5kZXgsIGFycmF5KSA9PiB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IGl0ZW1WaWV3ID0gbmV3IEl0ZW1WaWV3KFwiSXRlbVZpZXdcIiArIGluZGV4KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpdGVtVmlldy50aXRsZUxhYmVsLnRleHQgPSB2YWx1ZS50aXRsZVxuICAgICAgICAgICAgaXRlbVZpZXcuaW1hZ2VWaWV3LmltYWdlU291cmNlID0gRklSU1QodmFsdWUuaW1nLCBcImltYWdlcy9sb2dvMTkyLnBuZ1wiKVxuICAgICAgICAgICAgaXRlbVZpZXcuYWRkQ29udHJvbEV2ZW50VGFyZ2V0LkVudGVyRG93bi5Qb2ludGVyVXBJbnNpZGUgPSAoXG4gICAgICAgICAgICAgICAgc2VuZGVyLFxuICAgICAgICAgICAgICAgIGV2ZW50XG4gICAgICAgICAgICApID0+IFVJUm91dGUuY3VycmVudFJvdXRlLnJvdXRlV2l0aFZpZXdDb250cm9sbGVyQ29tcG9uZW50KEFydGljbGVWaWV3Q29udHJvbGxlciwgeyBJRDogdmFsdWUuaWQgfSkuYXBwbHkoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLml0ZW1WaWV3cy5wdXNoKGl0ZW1WaWV3KVxuICAgICAgICAgICAgdGhpcy5hZGRTdWJ2aWV3KGl0ZW1WaWV3KVxuICAgICAgICAgICAgXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgbGF5b3V0U3Vidmlld3MoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5sYXlvdXRTdWJ2aWV3cygpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBwYWRkaW5nID0gUm9vdFZpZXdDb250cm9sbGVyLnBhZGRpbmdMZW5ndGhcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGJvdW5kcyA9IHRoaXMuYm91bmRzXG4gICAgICAgIFxuICAgICAgICAvLyBUaGlzIHNvbHV0aW9uIGlzIG9idmlvdXNseSBub3QgdmVyeSBkeW5hbWljLCBidXQgSSBkZWNpZGVkIHRvIG5vdCBtYWtlIHRoZSBwYWdlIGhhdmUgbW9yZSBmZWF0dXJlcyB0aGFuIHRoZVxuICAgICAgICAvLyBSZWFjdCB2ZXJzaW9uXG4gICAgICAgIFxuICAgICAgICB2YXIgZnJhbWUgPSBib3VuZHMucmVjdGFuZ2xlV2l0aEhlaWdodCgwKVxuICAgICAgICBcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLml0ZW1WaWV3cy5sZW5ndGg7IGkgPSBpICsgMykge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBpdGVtVmlldyA9IEZJUlNUX09SX05JTCh0aGlzLml0ZW1WaWV3c1tpXSlcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZnJhbWUgPSBmcmFtZS5yZWN0YW5nbGVGb3JOZXh0Um93KDAsIGl0ZW1WaWV3LmludHJpbnNpY0NvbnRlbnRIZWlnaHQoZnJhbWUud2lkdGgpKVxuICAgICAgICAgICAgaXRlbVZpZXcuZnJhbWUgPSBmcmFtZVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBzZWNvbmRJdGVtVmlldyA9IEZJUlNUX09SX05JTCh0aGlzLml0ZW1WaWV3c1tpICsgMV0pXG4gICAgICAgICAgICBjb25zdCB0aGlyZEl0ZW1WaWV3ID0gRklSU1RfT1JfTklMKHRoaXMuaXRlbVZpZXdzW2kgKyAyXSlcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBjb25zdHJhaW5pbmdXaWR0aCA9IChmcmFtZS53aWR0aCAtIHBhZGRpbmcgKiAwLjUpIC8gMjtcbiAgICAgICAgICAgIGNvbnN0IGhlaWdodCA9IE1hdGgubWF4KFxuICAgICAgICAgICAgICAgIHNlY29uZEl0ZW1WaWV3LmludHJpbnNpY0NvbnRlbnRIZWlnaHQoY29uc3RyYWluaW5nV2lkdGgpLFxuICAgICAgICAgICAgICAgIHRoaXJkSXRlbVZpZXcuaW50cmluc2ljQ29udGVudEhlaWdodChjb25zdHJhaW5pbmdXaWR0aClcbiAgICAgICAgICAgIClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZnJhbWUgPSBmcmFtZS5yZWN0YW5nbGVGb3JOZXh0Um93KDAsIGhlaWdodClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZnJhbWUuZGlzdHJpYnV0ZVZpZXdzRXF1YWxseUFsb25nV2lkdGgoW3NlY29uZEl0ZW1WaWV3LCB0aGlyZEl0ZW1WaWV3XSwgcGFkZGluZyAqIDAuNSlcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlDb3JlL1VJVmlld0NvbnRyb2xsZXIudHNcIiAvPlxuLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlDb3JlL1VJRGlhbG9nVmlldy50c1wiIC8+XG4vLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9VSUNvcmUvVUlMaW5rQnV0dG9uLnRzXCIgLz5cbi8vLzxyZWZlcmVuY2UgcGF0aD1cIkFydGljbGVWaWV3Q29udHJvbGxlci50c1wiLz5cbi8vLzxyZWZlcmVuY2UgcGF0aD1cIlRhYmxlVmlldy50c1wiLz5cblxuXG5cblxuXG5jbGFzcyBSb290Vmlld0NvbnRyb2xsZXIgZXh0ZW5kcyBVSVZpZXdDb250cm9sbGVyIHtcbiAgICBcbiAgICBwcml2YXRlIGFydGljbGVWaWV3Q29udHJvbGxlcjogQXJ0aWNsZVZpZXdDb250cm9sbGVyXG4gICAgcHJpdmF0ZSBtYWluVmlld0NvbnRyb2xsZXI6IGFueVxuICAgIHByaXZhdGUgX2NvbnRlbnRWaWV3Q29udHJvbGxlcjogVUlWaWV3Q29udHJvbGxlclxuICAgIHByaXZhdGUgdG9wQmFyVmlldzogQ2VsbFZpZXdcbiAgICBwcml2YXRlIF90YWJsZVZpZXc6IFRhYmxlVmlld1xuICAgIHByaXZhdGUgYmFja1RvTWFpbkJ1dHRvbjogQ0JGbGF0QnV0dG9uXG4gICAgcHJpdmF0ZSBfcmV0cmlldmVEYXRhID0gR3JhcGhRTChcbiAgICAgICAgYHF1ZXJ5ICgkc2tpcDogSW50ISwgJGxpbWl0OiBJbnQhKSB7XG4gICAgICAgICAgICBuZXdzTGlzdChza2lwOiAkc2tpcCwgbGltaXQ6ICRsaW1pdCkge1xuICAgICAgICAgICAgICAgIHJvd3Mge1xuICAgICAgICAgICAgICAgICAgaWRcbiAgICAgICAgICAgICAgICAgIHRpdGxlXG4gICAgICAgICAgICAgICAgICBpbWdcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1gXG4gICAgKVxuICAgIHByaXZhdGUgX2RhdGE6IGFueVxuICAgIFxuICAgIGNvbnN0cnVjdG9yKHZpZXcpIHtcbiAgICAgICAgXG4gICAgICAgIC8vIENhbGxpbmcgc3VwZXJcbiAgICAgICAgc3VwZXIodmlldylcbiAgICAgICAgXG4gICAgICAgIC8vIEhlcmUgYXJlIHNvbWUgc3VnZ2VzdGVkIGNvbnZlbnRpb25zIHRoYXQgYXJlIHVzZWQgaW4gVUlDb3JlXG4gICAgICAgIFxuICAgICAgICAvLyBJbnN0YW5jZSB2YXJpYWJsZXMsIGl0IGlzIGdvb2QgdG8gaW5pdGlhbGl6ZSB0byBuaWwgb3IgZW1wdHkgZnVuY3Rpb24sIG5vdCBsZWF2ZSBhcyB1bmRlZmluZWQgdG8gYXZvaWRcbiAgICAgICAgLy8gaWYgYmxvY2tzXG4gICAgICAgIC8vIHRoaXMuX2ZpcnN0VmlldyA9IG5pbDtcbiAgICAgICAgLy8gdGhpcy5fc2Vjb25kVmlldyA9IG5pbDtcbiAgICAgICAgLy8gdGhpcy5fdGVzdFZpZXcgPSBuaWw7XG4gICAgICAgIC8vIHRoaXMuX2J1dHRvbiA9IG5pbDtcbiAgICAgICAgXG4gICAgICAgIC8vIFRoZSBuaWwgb2JqZWN0IGF2b2lkcyB1bm5lY2Nlc3NhcnkgY3Jhc2hlcyBieSBhbGxvd2luZyB5b3UgdG8gY2FsbCBhbnkgZnVuY3Rpb24gb3IgYWNjZXNzIGFueSB2YXJpYWJsZSBvbiBpdCwgcmV0dXJuaW5nIG5pbFxuICAgICAgICBcbiAgICAgICAgLy8gRGVmaW5lIHByb3BlcnRpZXMgd2l0aCBnZXQgYW5kIHNldCBmdW5jdGlvbnMgc28gdGhleSBjYW4gYmUgYWNjZXNzZWQgYW5kIHNldCBsaWtlIHZhcmlhYmxlc1xuICAgICAgICBcbiAgICAgICAgLy8gTmFtZSB2YXJpYWJsZXMgdGhhdCBzaG91bGQgYmUgcHJpdmF0ZSwgbGlrZSBwcm9wZXJ0eSB2YXJpYWJsZXMsIHdpdGggYSBfIHNpZ24sIHRoaXMgYWxzbyBob2xkcyBmb3IgcHJpdmF0ZSBmdW5jdGlvbnNcbiAgICAgICAgLy8gQXZvaWQgYWNjZXNzaW5nIHZhcmlhYmxlcyBhbmQgZnVuY3Rpb25zIG5hbWVkIHdpdGggXyBmcm9tIG91dHNpZGUgYXMgdGhpcyBjcmVhdGVzIHN0cm9uZyBjb3VwbGluZyBhbmQgaGluZGVycyBzdGFiaWxpdHlcbiAgICAgICAgXG4gICAgICAgIC8vIENvZGUgZm9yIGZ1cnRoZXIgc2V0dXAgaWYgbmVjZXNzYXJ5XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBsb2FkSW50cm9zcGVjdGlvblZhcmlhYmxlcygpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmxvYWRJbnRyb3NwZWN0aW9uVmFyaWFibGVzKClcbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlWaWV3Q29udHJvbGxlclxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgbG9hZFN1YnZpZXdzKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy50b3BCYXJWaWV3ID0gbmV3IENlbGxWaWV3KFwiVG9wQmFyVmlld1wiKVxuICAgICAgICB0aGlzLnRvcEJhclZpZXcudGl0bGVMYWJlbC50ZXh0ID0gXCJORVdTIFJFQURFUlwiXG4gICAgICAgIHRoaXMudG9wQmFyVmlldy5jb2xvcnMuYmFja2dyb3VuZC5ub3JtYWwgPSBuZXcgVUlDb2xvcihcIiMyODJjMzRcIilcbiAgICAgICAgdGhpcy50b3BCYXJWaWV3LmNvbG9ycy50aXRsZUxhYmVsLm5vcm1hbCA9IFVJQ29sb3Iud2hpdGVDb2xvclxuICAgICAgICB0aGlzLnRvcEJhclZpZXcudGl0bGVMYWJlbC50ZXh0QWxpZ25tZW50ID0gVUlUZXh0Vmlldy50ZXh0QWxpZ25tZW50LmNlbnRlclxuICAgICAgICB0aGlzLnZpZXcuYWRkU3Vidmlldyh0aGlzLnRvcEJhclZpZXcpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmJhY2tUb01haW5CdXR0b24gPSBuZXcgQ0JGbGF0QnV0dG9uKClcbiAgICAgICAgdGhpcy5iYWNrVG9NYWluQnV0dG9uLnRpdGxlTGFiZWwudGV4dCA9IFwiJiM4NTkyO1wiXG4gICAgICAgIHRoaXMuYmFja1RvTWFpbkJ1dHRvbi5jb2xvcnMgPSB7XG4gICAgICAgICAgICB0aXRsZUxhYmVsOiB7XG4gICAgICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLndoaXRlQ29sb3IsXG4gICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IFVJQ29sb3Iud2hpdGVDb2xvci5jb2xvcldpdGhBbHBoYSgwLjc1KSxcbiAgICAgICAgICAgICAgICBzZWxlY3RlZDogVUlDb2xvci53aGl0ZUNvbG9yLmNvbG9yV2l0aEFscGhhKDAuNSlcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB7XG4gICAgICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLnRyYW5zcGFyZW50Q29sb3IsXG4gICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IFVJQ29sb3IudHJhbnNwYXJlbnRDb2xvcixcbiAgICAgICAgICAgICAgICBzZWxlY3RlZDogVUlDb2xvci50cmFuc3BhcmVudENvbG9yXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHRoaXMuYmFja1RvTWFpbkJ1dHRvbi5jYWxjdWxhdGVBbmRTZXRWaWV3RnJhbWUgPSBmdW5jdGlvbiAodGhpczogQ0JGbGF0QnV0dG9uKSB7XG4gICAgICAgICAgICB0aGlzLnNldFBvc2l0aW9uKDAsIG5pbCwgMCwgMCwgbmlsLCA1MClcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy50b3BCYXJWaWV3LmFkZFN1YnZpZXcodGhpcy5iYWNrVG9NYWluQnV0dG9uKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5iYWNrVG9NYWluQnV0dG9uLmFkZENvbnRyb2xFdmVudFRhcmdldC5FbnRlckRvd24uUG9pbnRlclVwSW5zaWRlID0gKFxuICAgICAgICAgICAgc2VuZGVyLFxuICAgICAgICAgICAgZXZlbnRcbiAgICAgICAgKSA9PiBVSVJvdXRlLmN1cnJlbnRSb3V0ZS5yb3V0ZUJ5UmVtb3ZpbmdDb21wb25lbnROYW1lZChBcnRpY2xlVmlld0NvbnRyb2xsZXIucm91dGVDb21wb25lbnROYW1lKS5hcHBseSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBhc3luYyBoYW5kbGVSb3V0ZShyb3V0ZTogVUlSb3V0ZSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaGFuZGxlUm91dGUocm91dGUpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmJhY2tUb01haW5CdXR0b24uaGlkZGVuID0gTk9cbiAgICAgICAgXG4gICAgICAgIGlmIChJUyhyb3V0ZS5jb21wb25lbnRXaXRoTmFtZShBcnRpY2xlVmlld0NvbnRyb2xsZXIucm91dGVDb21wb25lbnROYW1lKSkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy8gU2hvdyBhcnRpY2xlIHZpZXdcbiAgICAgICAgICAgIGlmICghSVModGhpcy5hcnRpY2xlVmlld0NvbnRyb2xsZXIpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5hcnRpY2xlVmlld0NvbnRyb2xsZXIgPSBuZXcgQXJ0aWNsZVZpZXdDb250cm9sbGVyKG5ldyBVSVZpZXcoXCJBcnRpY2xlVmlld1wiKSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5jb250ZW50Vmlld0NvbnRyb2xsZXIgPSB0aGlzLmFydGljbGVWaWV3Q29udHJvbGxlclxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIE1haW4gdmlldyBjb250cm9sbGVyXG4gICAgICAgICAgICBpZiAoIUlTKHRoaXMubWFpblZpZXdDb250cm9sbGVyKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuX3RhYmxlVmlldyA9IG5ldyBUYWJsZVZpZXcoXCJtYWluVmlld1wiKVxuICAgICAgICAgICAgICAgIHRoaXMubWFpblZpZXdDb250cm9sbGVyID0gbmV3IFVJVmlld0NvbnRyb2xsZXIodGhpcy5fdGFibGVWaWV3KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRWaWV3Q29udHJvbGxlciA9IHRoaXMubWFpblZpZXdDb250cm9sbGVyXG4gICAgICAgICAgICB0aGlzLmJhY2tUb01haW5CdXR0b24uaGlkZGVuID0gWUVTXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIFVwZGF0ZSBfdGFibGVWaWV3IHdpdGggZGF0YVxuICAgICAgICAgICAgdGhpcy5fZGF0YSA9IHRoaXMuX2RhdGEgfHwgYXdhaXQgRklSU1RfT1JfTklMKHRoaXMuX3JldHJpZXZlRGF0YSkoeyBza2lwOiAwLCBsaW1pdDogMjAwIH0pXG4gICAgICAgICAgICB0aGlzLl90YWJsZVZpZXcuaXRlbXMgPSB0aGlzLl9kYXRhLm5ld3NMaXN0LnJvd3NcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzZXQgY29udGVudFZpZXdDb250cm9sbGVyKGNvbnRyb2xsZXI6IFVJVmlld0NvbnRyb2xsZXIpIHtcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnRWaWV3Q29udHJvbGxlciA9PSBjb250cm9sbGVyKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuY29udGVudFZpZXdDb250cm9sbGVyKSB7XG4gICAgICAgICAgICB0aGlzLnJlbW92ZUNoaWxkVmlld0NvbnRyb2xsZXIodGhpcy5jb250ZW50Vmlld0NvbnRyb2xsZXIpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NvbnRlbnRWaWV3Q29udHJvbGxlciA9IGNvbnRyb2xsZXJcbiAgICAgICAgdGhpcy5hZGRDaGlsZFZpZXdDb250cm9sbGVySW5Db250YWluZXIoY29udHJvbGxlciwgdGhpcy52aWV3KVxuICAgICAgICB0aGlzLl9sYXlvdXRWaWV3U3Vidmlld3MoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3LnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGdldCBjb250ZW50Vmlld0NvbnRyb2xsZXIoKTogVUlWaWV3Q29udHJvbGxlciB7XG4gICAgICAgIHJldHVybiB0aGlzLl9jb250ZW50Vmlld0NvbnRyb2xsZXIgfHwgbmlsXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBnZXQgcGFkZGluZ0xlbmd0aCgpIHtcbiAgICAgICAgcmV0dXJuIDIwXG4gICAgfVxuICAgIFxuICAgIGdldCBwYWRkaW5nTGVuZ3RoKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jbGFzcy5wYWRkaW5nTGVuZ3RoXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGxheW91dFZpZXdzTWFudWFsbHkoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5sYXlvdXRWaWV3c01hbnVhbGx5KClcbiAgICAgICAgXG4gICAgICAgIC8vIFZpZXcgYm91bmRzXG4gICAgICAgIHZhciBib3VuZHMgPSB0aGlzLnZpZXcuYm91bmRzXG4gICAgICAgIFxuICAgICAgICB0aGlzLnRvcEJhclZpZXcuZnJhbWUgPSBib3VuZHMucmVjdGFuZ2xlV2l0aEhlaWdodCg1MClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29udGVudFZpZXdDb250cm9sbGVyLnZpZXcuZnJhbWUgPSB0aGlzLnRvcEJhclZpZXcuZnJhbWUucmVjdGFuZ2xlRm9yTmV4dFJvdyhcbiAgICAgICAgICAgIDAsXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRWaWV3Q29udHJvbGxlci52aWV3LmludHJpbnNpY0NvbnRlbnRIZWlnaHQoYm91bmRzLndpZHRoKVxuICAgICAgICApXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsInR5cGUgVmFsdWVPZjxUPiA9IFRba2V5b2YgVF07XG50eXBlIFByb3BUeXBlPFRPYmosIFRQcm9wIGV4dGVuZHMga2V5b2YgVE9iaj4gPSBUT2JqW1RQcm9wXTtcblxudHlwZSBVSVJvdXRlUGFyYW1ldGVyczxUID0gYW55PiA9IHtcbiAgICBcbiAgICBba2V5OiBzdHJpbmddOiBzdHJpbmc7XG4gICAgXG59IHwgVDtcblxuXG5pbnRlcmZhY2UgVUlSb3V0ZUNvbXBvbmVudDxUID0gYW55PiB7XG4gICAgXG4gICAgbmFtZTogc3RyaW5nO1xuICAgIHBhcmFtZXRlcnM6IFVJUm91dGVQYXJhbWV0ZXJzPFQ+O1xuICAgIFxufVxuXG5cbi8vIEB0cy1pZ25vcmVcbmNsYXNzIFVJUm91dGUgZXh0ZW5kcyBBcnJheTxVSVJvdXRlQ29tcG9uZW50PiB7XG4gICAgXG4gICAgX2lzSGFuZGxlZDogYm9vbGVhbiA9IE5PXG4gICAgY29tcGxldGVkQ29tcG9uZW50czogVUlSb3V0ZUNvbXBvbmVudFtdID0gW11cbiAgICBcbiAgICBwYXJlbnRSb3V0ZTogVUlSb3V0ZVxuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGhhc2g/OiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgXG4gICAgICAgIGlmICghaGFzaCB8fCAhaGFzaC5zdGFydHNXaXRoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChoYXNoLnN0YXJ0c1dpdGgoXCIjXCIpKSB7XG4gICAgICAgICAgICBoYXNoID0gaGFzaC5zbGljZSgxKVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBoYXNoID0gZGVjb2RlVVJJQ29tcG9uZW50KGhhc2gpXG4gICAgICAgIFxuICAgICAgICBjb25zdCBjb21wb25lbnRzID0gaGFzaC5zcGxpdChcIl1cIilcbiAgICAgICAgY29tcG9uZW50cy5mb3JFYWNoKGZ1bmN0aW9uICh0aGlzOiBVSVJvdXRlLCBjb21wb25lbnQ6IHN0cmluZywgaW5kZXg6IG51bWJlciwgYXJyYXk6IHN0cmluZ1tdKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IGNvbXBvbmVudE5hbWUgPSBjb21wb25lbnQuc3BsaXQoXCJbXCIpWzBdXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge31cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKCFjb21wb25lbnROYW1lKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnNTdHJpbmcgPSBjb21wb25lbnQuc3BsaXQoXCJbXCIpWzFdIHx8IFwiXCJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlclBhaXJTdHJpbmdzID0gcGFyYW1ldGVyc1N0cmluZy5zcGxpdChcIixcIikgfHwgW11cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGFyYW1ldGVyUGFpclN0cmluZ3MuZm9yRWFjaChmdW5jdGlvbiAocGFpclN0cmluZywgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29uc3Qga2V5QW5kVmFsdWVBcnJheSA9IHBhaXJTdHJpbmcuc3BsaXQoXCI6XCIpXG4gICAgICAgICAgICAgICAgY29uc3Qga2V5ID0gZGVjb2RlVVJJQ29tcG9uZW50KGtleUFuZFZhbHVlQXJyYXlbMF0pXG4gICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBkZWNvZGVVUklDb21wb25lbnQoa2V5QW5kVmFsdWVBcnJheVsxXSlcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAoa2V5KSB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBwYXJhbWV0ZXJzW2tleV0gPSB2YWx1ZVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5wdXNoKHtcbiAgICAgICAgICAgICAgICBuYW1lOiBjb21wb25lbnROYW1lLFxuICAgICAgICAgICAgICAgIHBhcmFtZXRlcnM6IHBhcmFtZXRlcnNcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9LCB0aGlzKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBnZXQgY3VycmVudFJvdXRlKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIG5ldyBVSVJvdXRlKHdpbmRvdy5sb2NhdGlvbi5oYXNoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgYXBwbHkoKSB7XG4gICAgICAgIFxuICAgICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IHRoaXMuc3RyaW5nUmVwcmVzZW50YXRpb25cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGFwcGx5QnlSZXBsYWNpbmdDdXJyZW50Um91dGVJbkhpc3RvcnkoKSB7XG4gICAgICAgIFxuICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVwbGFjZSh0aGlzLmxpbmtSZXByZXNlbnRhdGlvbilcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGNvcHkoKSB7XG4gICAgICAgIHZhciByZXN1bHQgPSBuZXcgVUlSb3V0ZSgpXG4gICAgICAgIHJlc3VsdCA9IE9iamVjdC5hc3NpZ24ocmVzdWx0LCB0aGlzKVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIGNoaWxkUm91dGUoKSB7XG4gICAgICAgIFxuICAgICAgICB2YXIgcmVzdWx0ID0gdGhpcy5jb3B5KClcbiAgICAgICAgXG4gICAgICAgIHJlc3VsdC5jb21wbGV0ZWRDb21wb25lbnRzLmZvckVhY2goZnVuY3Rpb24gKGNvbXBvbmVudCwgaW5kZXgsIGNvbXBsZXRlZENvbXBvbmVudHMpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmFyIGluZGV4SW5SZXN1bHQgPSByZXN1bHQuaW5kZXhPZihjb21wb25lbnQpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChpbmRleEluUmVzdWx0ID4gLTEpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICByZXN1bHQuc3BsaWNlKGluZGV4SW5SZXN1bHQsIDEpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfSlcbiAgICAgICAgXG4gICAgICAgIHJlc3VsdC5jb21wbGV0ZWRDb21wb25lbnRzID0gW11cbiAgICAgICAgXG4gICAgICAgIHJlc3VsdC5wYXJlbnRSb3V0ZSA9IHRoaXNcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgcm91dGVCeVJlbW92aW5nQ29tcG9uZW50c090aGVyVGhhbk9uZXNOYW1lZChjb21wb25lbnROYW1lczogc3RyaW5nW10pIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5jb3B5KClcbiAgICAgICAgY29uc3QgaW5kZXhlc1RvUmVtb3ZlOiBudW1iZXJbXSA9IFtdXG4gICAgICAgIHJlc3VsdC5mb3JFYWNoKGZ1bmN0aW9uIChjb21wb25lbnQsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgaWYgKCFjb21wb25lbnROYW1lcy5jb250YWlucyhjb21wb25lbnQubmFtZSkpIHtcbiAgICAgICAgICAgICAgICBpbmRleGVzVG9SZW1vdmUucHVzaChpbmRleClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgICAgaW5kZXhlc1RvUmVtb3ZlLmZvckVhY2goZnVuY3Rpb24gKGluZGV4VG9SZW1vdmUsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgcmVzdWx0LnJlbW92ZUVsZW1lbnRBdEluZGV4KGluZGV4VG9SZW1vdmUpXG4gICAgICAgIH0pXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgcm91dGVCeVJlbW92aW5nQ29tcG9uZW50TmFtZWQoY29tcG9uZW50TmFtZTogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuY29weSgpXG4gICAgICAgIGNvbnN0IGNvbXBvbmVudEluZGV4ID0gcmVzdWx0LmZpbmRJbmRleChmdW5jdGlvbiAoY29tcG9uZW50LCBpbmRleCkge1xuICAgICAgICAgICAgcmV0dXJuIChjb21wb25lbnQubmFtZSA9PSBjb21wb25lbnROYW1lKVxuICAgICAgICB9KVxuICAgICAgICBpZiAoY29tcG9uZW50SW5kZXggIT0gLTEpIHtcbiAgICAgICAgICAgIHJlc3VsdC5zcGxpY2UoY29tcG9uZW50SW5kZXgsIDEpXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBcbiAgICByb3V0ZUJ5UmVtb3ZpbmdQYXJhbWV0ZXJJbkNvbXBvbmVudChjb21wb25lbnROYW1lOiBzdHJpbmcsIHBhcmFtZXRlck5hbWU6IHN0cmluZywgcmVtb3ZlQ29tcG9uZW50SWZFbXB0eSA9IE5PKSB7XG4gICAgICAgIHZhciByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICB2YXIgcGFyYW1ldGVycyA9IHJlc3VsdC5jb21wb25lbnRXaXRoTmFtZShjb21wb25lbnROYW1lKS5wYXJhbWV0ZXJzXG4gICAgICAgIGlmIChJU19OT1QocGFyYW1ldGVycykpIHtcbiAgICAgICAgICAgIHBhcmFtZXRlcnMgPSB7fVxuICAgICAgICB9XG4gICAgICAgIGRlbGV0ZSBwYXJhbWV0ZXJzW3BhcmFtZXRlck5hbWVdXG4gICAgICAgIHJlc3VsdCA9IHJlc3VsdC5yb3V0ZVdpdGhDb21wb25lbnQoY29tcG9uZW50TmFtZSwgcGFyYW1ldGVycylcbiAgICAgICAgaWYgKHJlbW92ZUNvbXBvbmVudElmRW1wdHkgJiYgT2JqZWN0LmtleXMocGFyYW1ldGVycykubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdC5yb3V0ZUJ5UmVtb3ZpbmdDb21wb25lbnROYW1lZChjb21wb25lbnROYW1lKVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgcm91dGVCeVNldHRpbmdQYXJhbWV0ZXJJbkNvbXBvbmVudChjb21wb25lbnROYW1lOiBzdHJpbmcsIHBhcmFtZXRlck5hbWU6IHN0cmluZywgdmFsdWVUb1NldDogc3RyaW5nKSB7XG4gICAgICAgIHZhciByZXN1bHQgPSB0aGlzLmNvcHkoKVxuICAgICAgICBpZiAoSVNfTklMKHZhbHVlVG9TZXQpIHx8IElTX05JTChwYXJhbWV0ZXJOYW1lKSkge1xuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9XG4gICAgICAgIHZhciBwYXJhbWV0ZXJzID0gcmVzdWx0LmNvbXBvbmVudFdpdGhOYW1lKGNvbXBvbmVudE5hbWUpLnBhcmFtZXRlcnNcbiAgICAgICAgaWYgKElTX05PVChwYXJhbWV0ZXJzKSkge1xuICAgICAgICAgICAgcGFyYW1ldGVycyA9IHt9XG4gICAgICAgIH1cbiAgICAgICAgcGFyYW1ldGVyc1twYXJhbWV0ZXJOYW1lXSA9IHZhbHVlVG9TZXRcbiAgICAgICAgcmVzdWx0ID0gcmVzdWx0LnJvdXRlV2l0aENvbXBvbmVudChjb21wb25lbnROYW1lLCBwYXJhbWV0ZXJzKVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIHJvdXRlV2l0aFZpZXdDb250cm9sbGVyQ29tcG9uZW50PFQgZXh0ZW5kcyB0eXBlb2YgVUlWaWV3Q29udHJvbGxlcj4oXG4gICAgICAgIHZpZXdDb250cm9sbGVyOiBULFxuICAgICAgICBwYXJhbWV0ZXJzOiBVSVJvdXRlUGFyYW1ldGVyczx7IFtQIGluIGtleW9mIFRbXCJQYXJhbWV0ZXJJZGVudGlmaWVyTmFtZVwiXV06IHN0cmluZyB9PixcbiAgICAgICAgZXh0ZW5kUGFyYW1ldGVyczogYm9vbGVhbiA9IE5PXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5yb3V0ZVdpdGhDb21wb25lbnQodmlld0NvbnRyb2xsZXIucm91dGVDb21wb25lbnROYW1lLCBwYXJhbWV0ZXJzLCBleHRlbmRQYXJhbWV0ZXJzKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcm91dGVXaXRoQ29tcG9uZW50KG5hbWU6IHN0cmluZywgcGFyYW1ldGVyczogVUlSb3V0ZVBhcmFtZXRlcnMsIGV4dGVuZFBhcmFtZXRlcnM6IGJvb2xlYW4gPSBOTykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5jb3B5KClcbiAgICAgICAgdmFyIGNvbXBvbmVudCA9IHJlc3VsdC5jb21wb25lbnRXaXRoTmFtZShuYW1lKVxuICAgICAgICBpZiAoSVNfTk9UKGNvbXBvbmVudCkpIHtcbiAgICAgICAgICAgIGNvbXBvbmVudCA9IHtcbiAgICAgICAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgICAgICAgIHBhcmFtZXRlcnM6IHt9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXN1bHQucHVzaChjb21wb25lbnQpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChJU19OT1QocGFyYW1ldGVycykpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcGFyYW1ldGVycyA9IHt9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKGV4dGVuZFBhcmFtZXRlcnMpIHtcbiAgICAgICAgICAgIGNvbXBvbmVudC5wYXJhbWV0ZXJzID0gT2JqZWN0LmFzc2lnbihjb21wb25lbnQucGFyYW1ldGVycywgcGFyYW1ldGVycylcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbXBvbmVudC5wYXJhbWV0ZXJzID0gcGFyYW1ldGVyc1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBuYXZpZ2F0ZUJ5U2V0dGluZ0NvbXBvbmVudChuYW1lOiBzdHJpbmcsIHBhcmFtZXRlcnM6IFVJUm91dGVQYXJhbWV0ZXJzLCBleHRlbmRQYXJhbWV0ZXJzOiBib29sZWFuID0gTk8pIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMucm91dGVXaXRoQ29tcG9uZW50KG5hbWUsIHBhcmFtZXRlcnMsIGV4dGVuZFBhcmFtZXRlcnMpLmFwcGx5KClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGNvbXBvbmVudFdpdGhWaWV3Q29udHJvbGxlcjxUIGV4dGVuZHMgdHlwZW9mIFVJVmlld0NvbnRyb2xsZXI+KHZpZXdDb250cm9sbGVyOiBUKTogVUlSb3V0ZUNvbXBvbmVudDx7IFtQIGluIGtleW9mIFRbXCJQYXJhbWV0ZXJJZGVudGlmaWVyTmFtZVwiXV06IHN0cmluZyB9PiB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5jb21wb25lbnRXaXRoTmFtZSh2aWV3Q29udHJvbGxlci5yb3V0ZUNvbXBvbmVudE5hbWUpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBjb21wb25lbnRXaXRoTmFtZShuYW1lOiBzdHJpbmcpOiBVSVJvdXRlQ29tcG9uZW50IHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IG5pbFxuICAgICAgICB0aGlzLmZvckVhY2goZnVuY3Rpb24gKGNvbXBvbmVudCwgaW5kZXgsIHNlbGYpIHtcbiAgICAgICAgICAgIGlmIChjb21wb25lbnQubmFtZSA9PSBuYW1lKSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gY29tcG9uZW50XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZGlkY29tcGxldGVDb21wb25lbnQoY29tcG9uZW50OiBVSVJvdXRlQ29tcG9uZW50KSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBzZWxmOiBVSVJvdXRlID0gdGhpc1xuICAgICAgICBjb25zdCBpbmRleCA9IHNlbGYuaW5kZXhPZihjb21wb25lbnQsIDApXG4gICAgICAgIGlmIChpbmRleCA+IC0xKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgc2VsZi5jb21wbGV0ZWRDb21wb25lbnRzLnB1c2goc2VsZi5zcGxpY2UoaW5kZXgsIDEpWzBdKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICAvL3NlbGYuY29tcGxldGVkQ29tcG9uZW50cy5wdXNoKGNvbXBvbmVudCk7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IGlzSGFuZGxlZChpc0hhbmRsZWQ6IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzSGFuZGxlZCA9IGlzSGFuZGxlZFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGlzSGFuZGxlZCgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiAodGhpcy5faXNIYW5kbGVkIHx8ICh0aGlzLmxlbmd0aCA9PSAwICYmIHRoaXMuY29tcGxldGVkQ29tcG9uZW50cy5sZW5ndGggIT0gMCkpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgbGlua1JlcHJlc2VudGF0aW9uKCkge1xuICAgICAgICByZXR1cm4gXCIjXCIgKyB0aGlzLnN0cmluZ1JlcHJlc2VudGF0aW9uXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBzdHJpbmdSZXByZXNlbnRhdGlvbigpIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IFwiXCJcbiAgICAgICAgdGhpcy5jb21wbGV0ZWRDb21wb25lbnRzLmZvckVhY2goZnVuY3Rpb24gKGNvbXBvbmVudCwgaW5kZXgsIHNlbGYpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdCArIGNvbXBvbmVudC5uYW1lXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0gY29tcG9uZW50LnBhcmFtZXRlcnNcbiAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdCArIFwiW1wiXG4gICAgICAgICAgICBPYmplY3Qua2V5cyhwYXJhbWV0ZXJzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXksIGluZGV4LCBrZXlzKSB7XG4gICAgICAgICAgICAgICAgaWYgKGluZGV4KSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdCArIFwiLFwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdCArIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgXCI6XCIgKyBlbmNvZGVVUklDb21wb25lbnQocGFyYW1ldGVyc1trZXldKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdCArIFwiXVwiXG4gICAgICAgIH0pXG4gICAgICAgIHRoaXMuZm9yRWFjaChmdW5jdGlvbiAoY29tcG9uZW50LCBpbmRleCwgc2VsZikge1xuICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICsgY29tcG9uZW50Lm5hbWVcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSBjb21wb25lbnQucGFyYW1ldGVyc1xuICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICsgXCJbXCJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHBhcmFtZXRlcnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSwgaW5kZXgsIGtleXMpIHtcbiAgICAgICAgICAgICAgICBpZiAoaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICsgXCIsXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICsgZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyBcIjpcIiArIGVuY29kZVVSSUNvbXBvbmVudChwYXJhbWV0ZXJzW2tleV0pXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICsgXCJdXCJcbiAgICAgICAgfSlcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIlVJVmlldy50c1wiIC8+XG4vLy8gPHJlZmVyZW5jZSBwYXRoPVwiVUlWaWV3Q29udHJvbGxlci50c1wiIC8+XG4vLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi4vUm9vdFZpZXdDb250cm9sbGVyLnRzXCIgLz5cbi8vLyA8cmVmZXJlbmNlIHBhdGg9XCJVSVJvdXRlLnRzXCIgLz5cblxuXG5cblxuaW50ZXJmYWNlIFVJTGFuZ3VhZ2VTZXJ2aWNlIHtcbiAgICBcbiAgICBjdXJyZW50TGFuZ3VhZ2VLZXk6IHN0cmluZztcbiAgICBcbiAgICBzdHJpbmdGb3JLZXkoXG4gICAgICAgIGtleTogc3RyaW5nLFxuICAgICAgICBsYW5ndWFnZU5hbWU6IHN0cmluZyxcbiAgICAgICAgZGVmYXVsdFN0cmluZzogc3RyaW5nLFxuICAgICAgICBwYXJhbWV0ZXJzOiB7IFt4OiBzdHJpbmddOiBzdHJpbmcgfCBVSUxvY2FsaXplZFRleHRPYmplY3Q7IH1cbiAgICApOiBzdHJpbmcgfCB1bmRlZmluZWQ7XG4gICAgXG4gICAgc3RyaW5nRm9yQ3VycmVudExhbmd1YWdlKGxvY2FsaXplZFRleHRPYmplY3Q6IFVJTG9jYWxpemVkVGV4dE9iamVjdCk7XG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuaW50ZXJmYWNlIFVJTG9jYWxpemVkVGV4dE9iamVjdCB7XG4gICAgXG4gICAgW2tleTogc3RyaW5nXTogc3RyaW5nO1xuICAgIFxufVxuXG5cblxuXG5cbmNsYXNzIFVJQ29yZSBleHRlbmRzIFVJT2JqZWN0IHtcbiAgICBcbiAgICByb290Vmlld0NvbnRyb2xsZXI6IFVJVmlld0NvbnRyb2xsZXIgPSBuaWxcbiAgICBzdGF0aWMgUm9vdFZpZXdDb250cm9sbGVyQ2xhc3M6IGFueVxuICAgIHN0YXRpYyBtYWluOiBVSUNvcmVcbiAgICBcbiAgICBzdGF0aWMgbGFuZ3VhZ2VTZXJ2aWNlOiBVSUxhbmd1YWdlU2VydmljZSA9IG5pbFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKHJvb3REaXZFbGVtZW50SUQ6IHN0cmluZywgcm9vdFZpZXdDb250cm9sbGVyQ2xhc3M6IHR5cGVvZiBVSVZpZXdDb250cm9sbGVyKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcigpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IFVJQ29yZVxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSU9iamVjdFxuICAgICAgICBcbiAgICAgICAgVUlDb3JlLlJvb3RWaWV3Q29udHJvbGxlckNsYXNzID0gcm9vdFZpZXdDb250cm9sbGVyQ2xhc3NcbiAgICAgICAgXG4gICAgICAgIFVJQ29yZS5tYWluID0gdGhpc1xuICAgIFxuICAgICAgICBjb25zdCByb290Vmlld0VsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChyb290RGl2RWxlbWVudElEKVxuICAgIFxuICAgICAgICBjb25zdCByb290VmlldyA9IG5ldyBVSVZpZXcocm9vdERpdkVsZW1lbnRJRCwgcm9vdFZpZXdFbGVtZW50KVxuICAgIFxuICAgICAgICByb290Vmlldy5wYXVzZXNQb2ludGVyRXZlbnRzID0gTk8gLy9ZRVM7XG4gICAgICAgIFxuICAgICAgICBpZiAoVUlDb3JlLlJvb3RWaWV3Q29udHJvbGxlckNsYXNzKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICghKFVJQ29yZS5Sb290Vmlld0NvbnRyb2xsZXJDbGFzcy5wcm90b3R5cGUgaW5zdGFuY2VvZiBVSVZpZXdDb250cm9sbGVyKSB8fFxuICAgICAgICAgICAgICAgIChVSUNvcmUuUm9vdFZpZXdDb250cm9sbGVyQ2xhc3MgYXMgYW55KSA9PT0gVUlWaWV3Q29udHJvbGxlcikge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFxuICAgICAgICAgICAgICAgICAgICBcIkVycm9yLCBVSUNvcmUuUm9vdFZpZXdDb250cm9sbGVyQ2xhc3MgbXVzdCBiZSBhIG9yIGEgc3ViY2xhc3Mgb2YgVUlWaWV3Q29udHJvbGxlciwgZmFsbGluZyBiYWNrIHRvIFVJVmlld0NvbnRyb2xsZXIuXCIpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgVUlDb3JlLlJvb3RWaWV3Q29udHJvbGxlckNsYXNzID0gVUlWaWV3Q29udHJvbGxlclxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnJvb3RWaWV3Q29udHJvbGxlciA9IG5ldyBVSUNvcmUuUm9vdFZpZXdDb250cm9sbGVyQ2xhc3Mocm9vdFZpZXcpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5yb290Vmlld0NvbnRyb2xsZXIgPSBuZXcgVUlWaWV3Q29udHJvbGxlcihyb290VmlldylcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLnJvb3RWaWV3Q29udHJvbGxlci52aWV3V2lsbEFwcGVhcigpXG4gICAgICAgIHRoaXMucm9vdFZpZXdDb250cm9sbGVyLnZpZXdEaWRBcHBlYXIoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMucm9vdFZpZXdDb250cm9sbGVyLnZpZXcuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KFxuICAgICAgICAgICAgVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyVXBJbnNpZGUsXG4gICAgICAgICAgICBmdW5jdGlvbiAoc2VuZGVyLCBldmVudCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIChkb2N1bWVudC5hY3RpdmVFbGVtZW50IGFzIEhUTUxFbGVtZW50KS5ibHVyKClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgKVxuICAgIFxuICAgIFxuICAgIFxuICAgICAgICBjb25zdCB3aW5kb3dEaWRSZXNpemUgPSBmdW5jdGlvbiAodGhpczogVUlDb3JlKSB7XG4gICAgICAgIFxuICAgICAgICAgICAgLy8gRG9pbmcgbGF5b3V0IHR3byB0aW1lcyB0byBwcmV2ZW50IHBhZ2Ugc2Nyb2xsYmFycyBmcm9tIGNvbmZ1c2luZyB0aGUgbGF5b3V0XG4gICAgICAgICAgICB0aGlzLnJvb3RWaWV3Q29udHJvbGxlci5fbGF5b3V0Vmlld1N1YnZpZXdzKClcbiAgICAgICAgICAgIFVJVmlldy5sYXlvdXRWaWV3c0lmTmVlZGVkKClcbiAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnJvb3RWaWV3Q29udHJvbGxlci5fbGF5b3V0Vmlld1N1YnZpZXdzKClcbiAgICAgICAgICAgIC8vVUlWaWV3LmxheW91dFZpZXdzSWZOZWVkZWQoKVxuICAgICAgICBcbiAgICAgICAgICAgIHRoaXMucm9vdFZpZXdDb250cm9sbGVyLnZpZXcuYnJvYWRjYXN0RXZlbnRJblN1YnRyZWUoe1xuICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgbmFtZTogVUlDb3JlLmJyb2FkY2FzdEV2ZW50TmFtZS5XaW5kb3dEaWRSZXNpemUsXG4gICAgICAgICAgICAgICAgcGFyYW1ldGVyczogbmlsXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICB9XG4gICAgXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIHdpbmRvd0RpZFJlc2l6ZS5iaW5kKHRoaXMpKVxuICAgIFxuICAgICAgICBjb25zdCBkaWRTY3JvbGwgPSBmdW5jdGlvbiAodGhpczogVUlDb3JlKSB7XG4gICAgICAgIFxuICAgICAgICAgICAgLy9jb2RlXG4gICAgICAgIFxuICAgICAgICAgICAgdGhpcy5yb290Vmlld0NvbnRyb2xsZXIudmlldy5icm9hZGNhc3RFdmVudEluU3VidHJlZSh7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBuYW1lOiBVSVZpZXcuYnJvYWRjYXN0RXZlbnROYW1lLlBhZ2VEaWRTY3JvbGwsXG4gICAgICAgICAgICAgICAgcGFyYW1ldGVyczogbmlsXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgIFxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLCBkaWRTY3JvbGwsIGZhbHNlKVxuICAgIFxuICAgICAgICBjb25zdCBoYXNoRGlkQ2hhbmdlID0gZnVuY3Rpb24gKHRoaXM6IFVJQ29yZSkge1xuICAgICAgICBcbiAgICAgICAgICAgIC8vY29kZVxuICAgICAgICBcbiAgICAgICAgICAgIHRoaXMucm9vdFZpZXdDb250cm9sbGVyLmhhbmRsZVJvdXRlUmVjdXJzaXZlbHkoVUlSb3V0ZS5jdXJyZW50Um91dGUpXG4gICAgICAgIFxuICAgICAgICAgICAgdGhpcy5yb290Vmlld0NvbnRyb2xsZXIudmlldy5icm9hZGNhc3RFdmVudEluU3VidHJlZSh7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBuYW1lOiBVSUNvcmUuYnJvYWRjYXN0RXZlbnROYW1lLlJvdXRlRGlkQ2hhbmdlLFxuICAgICAgICAgICAgICAgIHBhcmFtZXRlcnM6IG5pbFxuICAgICAgICAgICAgXG4gICAgICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgIFxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImhhc2hjaGFuZ2VcIiwgaGFzaERpZENoYW5nZS5iaW5kKHRoaXMpLCBmYWxzZSlcbiAgICAgICAgXG4gICAgICAgIGhhc2hEaWRDaGFuZ2UoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzdGF0aWMgYnJvYWRjYXN0RXZlbnROYW1lID0ge1xuICAgICAgICBcbiAgICAgICAgXCJSb3V0ZURpZENoYW5nZVwiOiBcIlJvdXRlRGlkQ2hhbmdlXCIsXG4gICAgICAgIFwiV2luZG93RGlkUmVzaXplXCI6IFwiV2luZG93RGlkUmVzaXplXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBsb2FkQ2xhc3MoY2xhc3NOYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIGlmICh3aW5kb3dbY2xhc3NOYW1lXSkge1xuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGRvY3VtZW50LndyaXRlbG4oXCI8c2NyaXB0IHR5cGU9J3RleHQvamF2YXNjcmlwdCcgc3JjPSdkaXN0L1VJQ29yZS9cIiArIGNsYXNzTmFtZSArIFwiLmpzJz48L3NjcmlwdD5cIilcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxufVxuXG5cblVJQ29yZS5Sb290Vmlld0NvbnRyb2xsZXJDbGFzcyA9IG5pbFxuXG5cbmNvbnN0IElTX0ZJUkVGT1ggPSBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZihcImZpcmVmb3hcIikgPiAtMVxuY29uc3QgSVNfU0FGQVJJID0gL14oKD8hY2hyb21lfGFuZHJvaWQpLikqc2FmYXJpL2kudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KVxuXG5cbkFycmF5LnByb3RvdHlwZS5pbmRleE9mIHx8IChBcnJheS5wcm90b3R5cGUuaW5kZXhPZiA9IGZ1bmN0aW9uIChkLCBlKSB7XG4gICAgdmFyIGFcbiAgICBpZiAobnVsbCA9PSB0aGlzKSB7XG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJcXFwidGhpc1xcXCIgaXMgbnVsbCBvciBub3QgZGVmaW5lZFwiKVxuICAgIH1cbiAgICBjb25zdCBjID0gT2JqZWN0KHRoaXMpLFxuICAgICAgICBiID0gYy5sZW5ndGggPj4+IDBcbiAgICBpZiAoMCA9PT0gYikge1xuICAgICAgICByZXR1cm4gLTFcbiAgICB9XG4gICAgYSA9ICtlIHx8IDBcbiAgICBJbmZpbml0eSA9PT0gTWF0aC5hYnMoYSkgJiYgKGEgPSAwKVxuICAgIGlmIChhID49IGIpIHtcbiAgICAgICAgcmV0dXJuIC0xXG4gICAgfVxuICAgIGZvciAoYSA9IE1hdGgubWF4KDAgPD0gYSA/IGEgOiBiIC0gTWF0aC5hYnMoYSksIDApOyBhIDwgYjspIHtcbiAgICAgICAgaWYgKGEgaW4gYyAmJiBjW2FdID09PSBkKSB7XG4gICAgICAgICAgICByZXR1cm4gYVxuICAgICAgICB9XG4gICAgICAgIGErK1xuICAgIH1cbiAgICByZXR1cm4gLTFcbn0pXG5cblxuXG5cblxuXG5cblxuXG5cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGg9XCJVSVZpZXcudHNcIiAvPlxuLy8vIDxyZWZlcmVuY2UgcGF0aD1cIlVJQ29yZS50c1wiIC8+XG5cblxuXG5cblxuY2xhc3MgVUlUZXh0VmlldyBleHRlbmRzIFVJVmlldyB7XG4gICAgXG4gICAgXG4gICAgX3RleHRDb2xvcjogVUlDb2xvciA9IFVJVGV4dFZpZXcuZGVmYXVsdFRleHRDb2xvclxuICAgIF90ZXh0QWxpZ25tZW50OiBzdHJpbmdcbiAgICBcbiAgICBfaXNTaW5nbGVMaW5lID0gWUVTXG4gICAgXG4gICAgdGV4dFByZWZpeCA9IFwiXCJcbiAgICB0ZXh0U3VmZml4ID0gXCJcIlxuICAgIFxuICAgIF9ub3RpZmljYXRpb25BbW91bnQgPSAwXG4gICAgXG4gICAgX21pbkZvbnRTaXplOiBudW1iZXIgPSBuaWxcbiAgICBfbWF4Rm9udFNpemU6IG51bWJlciA9IG5pbFxuICAgIFxuICAgIF9hdXRvbWF0aWNGb250U2l6ZVNlbGVjdGlvbiA9IE5PXG4gICAgXG4gICAgY2hhbmdlc09mdGVuID0gTk9cbiAgICBcbiAgICBcbiAgICBzdGF0aWMgZGVmYXVsdFRleHRDb2xvciA9IFVJQ29sb3IuYmxhY2tDb2xvclxuICAgIHN0YXRpYyBub3RpZmljYXRpb25UZXh0Q29sb3IgPSBVSUNvbG9yLnJlZENvbG9yXG4gICAgXG4gICAgc3RhdGljIF9pbnRyaW5zaWNIZWlnaHRDYWNoZTogeyBbeDogc3RyaW5nXTogeyBbeDogc3RyaW5nXTogbnVtYmVyOyB9OyB9ICYgVUlPYmplY3QgPSBuZXcgVUlPYmplY3QoKSBhcyBhbnlcbiAgICBzdGF0aWMgX2ludHJpbnNpY1dpZHRoQ2FjaGU6IHsgW3g6IHN0cmluZ106IHsgW3g6IHN0cmluZ106IG51bWJlcjsgfTsgfSAmIFVJT2JqZWN0ID0gbmV3IFVJT2JqZWN0KCkgYXMgYW55XG4gICAgXG4gICAgXG4gICAgc3RhdGljIF9wdFRvUHg6IG51bWJlclxuICAgIHN0YXRpYyBfcHhUb1B0OiBudW1iZXJcbiAgICBfdGV4dDogc3RyaW5nXG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEPzogc3RyaW5nLCB0ZXh0Vmlld1R5cGUgPSBVSVRleHRWaWV3LnR5cGUucGFyYWdyYXBoLCB2aWV3SFRNTEVsZW1lbnQgPSBudWxsKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQsIHZpZXdIVE1MRWxlbWVudCwgdGV4dFZpZXdUeXBlKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBVSVRleHRWaWV3XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJVmlld1xuICAgICAgICBcbiAgICAgICAgdGhpcy50ZXh0ID0gXCJcIlxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS5vdmVyZmxvdyA9IFwiaGlkZGVuXCJcbiAgICAgICAgdGhpcy5zdHlsZS50ZXh0T3ZlcmZsb3cgPSBcImVsbGlwc2lzXCJcbiAgICAgICAgdGhpcy5pc1NpbmdsZUxpbmUgPSBZRVNcbiAgICAgICAgXG4gICAgICAgIHRoaXMudGV4dENvbG9yID0gdGhpcy50ZXh0Q29sb3JcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXNlckludGVyYWN0aW9uRW5hYmxlZCA9IFlFU1xuICAgICAgICBcbiAgICAgICAgaWYgKHRleHRWaWV3VHlwZSA9PSBVSVRleHRWaWV3LnR5cGUudGV4dEFyZWEpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5wYXVzZXNQb2ludGVyRXZlbnRzID0gWUVTXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlclVwSW5zaWRlLCBmdW5jdGlvbiAoc2VuZGVyLCBldmVudCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHNlbmRlci5mb2N1cygpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBfZGV0ZXJtaW5lUFhBbmRQVFJhdGlvcygpIHtcbiAgICBcbiAgICAgICAgY29uc3QgbyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIilcbiAgICAgICAgby5zdHlsZS53aWR0aCA9IFwiMTAwMHB0XCJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChvKVxuICAgICAgICBVSVRleHRWaWV3Ll9wdFRvUHggPSBvLmNsaWVudFdpZHRoIC8gMTAwMFxuICAgICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKG8pXG4gICAgICAgIFVJVGV4dFZpZXcuX3B4VG9QdCA9IDEgLyBVSVRleHRWaWV3Ll9wdFRvUHhcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyB0eXBlID0ge1xuICAgICAgICBcbiAgICAgICAgXCJwYXJhZ3JhcGhcIjogXCJwXCIsXG4gICAgICAgIFwiaGVhZGVyMVwiOiBcImgxXCIsXG4gICAgICAgIFwiaGVhZGVyMlwiOiBcImgyXCIsXG4gICAgICAgIFwiaGVhZGVyM1wiOiBcImgzXCIsXG4gICAgICAgIFwiaGVhZGVyNFwiOiBcImg0XCIsXG4gICAgICAgIFwiaGVhZGVyNVwiOiBcImg1XCIsXG4gICAgICAgIFwiaGVhZGVyNlwiOiBcImg2XCIsXG4gICAgICAgIFwidGV4dEFyZWFcIjogXCJ0ZXh0YXJlYVwiLFxuICAgICAgICBcInRleHRGaWVsZFwiOiBcImlucHV0XCIsXG4gICAgICAgIFwic3BhblwiOiBcInNwYW5cIixcbiAgICAgICAgXCJsYWJlbFwiOiBcImxhYmVsXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyB0ZXh0QWxpZ25tZW50ID0ge1xuICAgICAgICBcbiAgICAgICAgXCJsZWZ0XCI6IFwibGVmdFwiLFxuICAgICAgICBcImNlbnRlclwiOiBcImNlbnRlclwiLFxuICAgICAgICBcInJpZ2h0XCI6IFwicmlnaHRcIixcbiAgICAgICAgXCJqdXN0aWZ5XCI6IFwianVzdGlmeVwiXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgdGV4dEFsaWdubWVudCgpIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5zdHlsZS50ZXh0QWxpZ25cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBzZXQgdGV4dEFsaWdubWVudCh0ZXh0QWxpZ25tZW50OiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fdGV4dEFsaWdubWVudCA9IHRleHRBbGlnbm1lbnRcbiAgICAgICAgdGhpcy5zdHlsZS50ZXh0QWxpZ24gPSB0ZXh0QWxpZ25tZW50XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCB0ZXh0Q29sb3IoKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuX3RleHRDb2xvclxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIHNldCB0ZXh0Q29sb3IoY29sb3I6IFVJQ29sb3IpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3RleHRDb2xvciA9IGNvbG9yIHx8IFVJVGV4dFZpZXcuZGVmYXVsdFRleHRDb2xvclxuICAgICAgICB0aGlzLnN0eWxlLmNvbG9yID0gdGhpcy5fdGV4dENvbG9yLnN0cmluZ1ZhbHVlXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgaXNTaW5nbGVMaW5lKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzU2luZ2xlTGluZVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IGlzU2luZ2xlTGluZShpc1NpbmdsZUxpbmU6IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzU2luZ2xlTGluZSA9IGlzU2luZ2xlTGluZVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGlmIChpc1NpbmdsZUxpbmUpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zdHlsZS53aGl0ZVNwYWNlID0gXCJwcmVcIlxuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLnN0eWxlLndoaXRlU3BhY2UgPSBcInByZS13cmFwXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBub3RpZmljYXRpb25BbW91bnQoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5fbm90aWZpY2F0aW9uQW1vdW50XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXQgbm90aWZpY2F0aW9uQW1vdW50KG5vdGlmaWNhdGlvbkFtb3VudDogbnVtYmVyKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5fbm90aWZpY2F0aW9uQW1vdW50ID09IG5vdGlmaWNhdGlvbkFtb3VudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9ub3RpZmljYXRpb25BbW91bnQgPSBub3RpZmljYXRpb25BbW91bnRcbiAgICAgICAgXG4gICAgICAgIHRoaXMudGV4dCA9IHRoaXMudGV4dFxuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dFVwVG9Sb290VmlldygpXG4gICAgICAgIFxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkFtb3VudERpZENoYW5nZShub3RpZmljYXRpb25BbW91bnQpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBub3RpZmljYXRpb25BbW91bnREaWRDaGFuZ2Uobm90aWZpY2F0aW9uQW1vdW50OiBudW1iZXIpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgdGV4dCgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiAodGhpcy5fdGV4dCB8fCB0aGlzLnZpZXdIVE1MRWxlbWVudC5pbm5lckhUTUwpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXQgdGV4dCh0ZXh0KSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl90ZXh0ID0gdGV4dFxuICAgIFxuICAgICAgICB2YXIgbm90aWZpY2F0aW9uVGV4dCA9IFwiXCJcbiAgICBcbiAgICAgICAgaWYgKHRoaXMubm90aWZpY2F0aW9uQW1vdW50KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIG5vdGlmaWNhdGlvblRleHQgPSBcIjxzcGFuIHN0eWxlPVxcXCJjb2xvcjogXCIgKyBVSVRleHRWaWV3Lm5vdGlmaWNhdGlvblRleHRDb2xvci5zdHJpbmdWYWx1ZSArIFwiO1xcXCI+XCIgK1xuICAgICAgICAgICAgICAgIChcIiAoXCIgKyB0aGlzLm5vdGlmaWNhdGlvbkFtb3VudCArIFwiKVwiKS5ib2xkKCkgKyBcIjwvc3Bhbj5cIlxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLnZpZXdIVE1MRWxlbWVudC5pbm5lckhUTUwgIT0gdGhpcy50ZXh0UHJlZml4ICsgdGV4dCArIHRoaXMudGV4dFN1ZmZpeCArIG5vdGlmaWNhdGlvblRleHQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQuaW5uZXJIVE1MID0gdGhpcy50ZXh0UHJlZml4ICsgRklSU1QodGV4dCwgXCJcIikgKyB0aGlzLnRleHRTdWZmaXggKyBub3RpZmljYXRpb25UZXh0XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dCgpO1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IGlubmVySFRNTChpbm5lckhUTUw6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgdGhpcy50ZXh0ID0gaW5uZXJIVE1MXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgaW5uZXJIVE1MKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMudmlld0hUTUxFbGVtZW50LmlubmVySFRNTFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc2V0VGV4dChrZXk6IHN0cmluZywgZGVmYXVsdFN0cmluZzogc3RyaW5nLCBwYXJhbWV0ZXJzPzogeyBbeDogc3RyaW5nXTogc3RyaW5nIHwgVUlMb2NhbGl6ZWRUZXh0T2JqZWN0IH0pIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2V0SW5uZXJIVE1MKGtleSwgZGVmYXVsdFN0cmluZywgcGFyYW1ldGVycylcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGdldCBmb250U2l6ZSgpIHtcbiAgICBcbiAgICAgICAgY29uc3Qgc3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLnZpZXdIVE1MRWxlbWVudCwgbnVsbCkuZm9udFNpemVcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gKHBhcnNlRmxvYXQoc3R5bGUpICogVUlUZXh0Vmlldy5fcHhUb1B0KVxuICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXQgZm9udFNpemUoZm9udFNpemU6IG51bWJlcikge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUuZm9udFNpemUgPSBcIlwiICsgZm9udFNpemUgKyBcInB0XCJcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICB1c2VBdXRvbWF0aWNGb250U2l6ZShtaW5Gb250U2l6ZTogbnVtYmVyID0gbmlsLCBtYXhGb250U2l6ZTogbnVtYmVyID0gbmlsKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5fYXV0b21hdGljRm9udFNpemVTZWxlY3Rpb24gPSBZRVNcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9taW5Gb250U2l6ZSA9IG1pbkZvbnRTaXplXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9tYXhGb250U2l6ZSA9IG1heEZvbnRTaXplXG4gICAgICAgIFxuICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgYXV0b21hdGljYWxseUNhbGN1bGF0ZWRGb250U2l6ZShcbiAgICAgICAgYm91bmRzOiBVSVJlY3RhbmdsZSxcbiAgICAgICAgY3VycmVudFNpemU6IFVJUmVjdGFuZ2xlLFxuICAgICAgICBjdXJyZW50Rm9udFNpemU6IG51bWJlcixcbiAgICAgICAgbWluRm9udFNpemU/OiBudW1iZXIsXG4gICAgICAgIG1heEZvbnRTaXplPzogbnVtYmVyXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBtaW5Gb250U2l6ZSA9IEZJUlNUKG1pbkZvbnRTaXplLCAxKVxuICAgICAgICBcbiAgICAgICAgbWF4Rm9udFNpemUgPSBGSVJTVChtYXhGb250U2l6ZSwgMTAwMDAwMDAwMDAwKVxuICAgIFxuICAgIFxuICAgICAgICBjb25zdCBoZWlnaHRNdWx0aXBsaWVyID0gYm91bmRzLmhlaWdodCAvIChjdXJyZW50U2l6ZS5oZWlnaHQgKyAxKVxuICAgIFxuICAgICAgICBjb25zdCB3aWR0aE11bHRpcGxpZXIgPSBib3VuZHMud2lkdGggLyAoY3VycmVudFNpemUud2lkdGggKyAxKVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgICAgICB2YXIgbXVsdGlwbGllciA9IGhlaWdodE11bHRpcGxpZXJcbiAgICBcbiAgICAgICAgaWYgKGhlaWdodE11bHRpcGxpZXIgPiB3aWR0aE11bHRpcGxpZXIpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbXVsdGlwbGllciA9IHdpZHRoTXVsdGlwbGllclxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgICAgIGNvbnN0IG1heEZpdHRpbmdGb250U2l6ZSA9IGN1cnJlbnRGb250U2l6ZSAqIG11bHRpcGxpZXJcbiAgICBcbiAgICBcbiAgICAgICAgaWYgKG1heEZpdHRpbmdGb250U2l6ZSA+IG1heEZvbnRTaXplKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVybiBtYXhGb250U2l6ZVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmIChtaW5Gb250U2l6ZSA+IG1heEZpdHRpbmdGb250U2l6ZSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gbWluRm9udFNpemVcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIG1heEZpdHRpbmdGb250U2l6ZVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBkaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnQoZXZlbnQ6IFVJVmlld0Jyb2FkY2FzdEV2ZW50KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5kaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnQoZXZlbnQpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICB3aWxsTW92ZVRvU3VwZXJ2aWV3KHN1cGVydmlldzogVUlWaWV3KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci53aWxsTW92ZVRvU3VwZXJ2aWV3KHN1cGVydmlldylcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGxheW91dFN1YnZpZXdzKCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIubGF5b3V0U3Vidmlld3MoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl9hdXRvbWF0aWNGb250U2l6ZVNlbGVjdGlvbikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmZvbnRTaXplID0gVUlUZXh0Vmlldy5hdXRvbWF0aWNhbGx5Q2FsY3VsYXRlZEZvbnRTaXplKFxuICAgICAgICAgICAgICAgIG5ldyBVSVJlY3RhbmdsZSgwLCAwLCAxICpcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQub2Zmc2V0SGVpZ2h0LCAxICpcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQub2Zmc2V0V2lkdGgpLFxuICAgICAgICAgICAgICAgIHRoaXMuaW50cmluc2ljQ29udGVudFNpemUoKSxcbiAgICAgICAgICAgICAgICB0aGlzLmZvbnRTaXplLFxuICAgICAgICAgICAgICAgIHRoaXMuX21pbkZvbnRTaXplLFxuICAgICAgICAgICAgICAgIHRoaXMuX21heEZvbnRTaXplXG4gICAgICAgICAgICApXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGludHJpbnNpY0NvbnRlbnRIZWlnaHQoY29uc3RyYWluaW5nV2lkdGggPSAwKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5jaGFuZ2VzT2Z0ZW4pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuIHN1cGVyLmludHJpbnNpY0NvbnRlbnRIZWlnaHQoY29uc3RyYWluaW5nV2lkdGgpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBjb25zdCBrZXlQYXRoID0gKHRoaXMudmlld0hUTUxFbGVtZW50LmlubmVySFRNTCArIFwiX2NzZl9cIiArIHRoaXMuY29tcHV0ZWRTdHlsZS5mb250KS5yZXBsYWNlKG5ldyBSZWdFeHAoXCJcXFxcLlwiLCBcImdcIiksIFwiX1wiKSArIFwiLlwiICtcbiAgICAgICAgICAgIChcIlwiICsgY29uc3RyYWluaW5nV2lkdGgpLnJlcGxhY2UobmV3IFJlZ0V4cChcIlxcXFwuXCIsIFwiZ1wiKSwgXCJfXCIpXG4gICAgXG4gICAgICAgIHZhciByZXN1bHQgPSBVSVRleHRWaWV3Ll9pbnRyaW5zaWNIZWlnaHRDYWNoZS52YWx1ZUZvcktleVBhdGgoa2V5UGF0aClcbiAgICBcbiAgICAgICAgaWYgKElTX0xJS0VfTlVMTChyZXN1bHQpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJlc3VsdCA9IHN1cGVyLmludHJpbnNpY0NvbnRlbnRIZWlnaHQoY29uc3RyYWluaW5nV2lkdGgpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFVJVGV4dFZpZXcuX2ludHJpbnNpY0hlaWdodENhY2hlLnNldFZhbHVlRm9yS2V5UGF0aChrZXlQYXRoLCByZXN1bHQpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGludHJpbnNpY0NvbnRlbnRXaWR0aChjb25zdHJhaW5pbmdIZWlnaHQgPSAwKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5jaGFuZ2VzT2Z0ZW4pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuIHN1cGVyLmludHJpbnNpY0NvbnRlbnRXaWR0aChjb25zdHJhaW5pbmdIZWlnaHQpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBjb25zdCBrZXlQYXRoID0gKHRoaXMudmlld0hUTUxFbGVtZW50LmlubmVySFRNTCArIFwiX2NzZl9cIiArIHRoaXMuY29tcHV0ZWRTdHlsZS5mb250KS5yZXBsYWNlKG5ldyBSZWdFeHAoXCJcXFxcLlwiLCBcImdcIiksIFwiX1wiKSArIFwiLlwiICtcbiAgICAgICAgICAgIChcIlwiICsgY29uc3RyYWluaW5nSGVpZ2h0KS5yZXBsYWNlKG5ldyBSZWdFeHAoXCJcXFxcLlwiLCBcImdcIiksIFwiX1wiKVxuICAgIFxuICAgICAgICB2YXIgcmVzdWx0ID0gVUlUZXh0Vmlldy5faW50cmluc2ljV2lkdGhDYWNoZS52YWx1ZUZvcktleVBhdGgoa2V5UGF0aClcbiAgICBcbiAgICAgICAgaWYgKElTX0xJS0VfTlVMTChyZXN1bHQpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJlc3VsdCA9IHN1cGVyLmludHJpbnNpY0NvbnRlbnRXaWR0aChjb25zdHJhaW5pbmdIZWlnaHQpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFVJVGV4dFZpZXcuX2ludHJpbnNpY1dpZHRoQ2FjaGUuc2V0VmFsdWVGb3JLZXlQYXRoKGtleVBhdGgsIHJlc3VsdClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgaW50cmluc2ljQ29udGVudFNpemUoKSB7XG4gICAgICAgIFxuICAgICAgICAvLyBUaGlzIHdvcmtzIGJ1dCBpcyBzbG93XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuaW50cmluc2ljQ29udGVudFNpemVXaXRoQ29uc3RyYWludHMobmlsLCBuaWwpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblVJVGV4dFZpZXcuX2RldGVybWluZVBYQW5kUFRSYXRpb3MoKVxuXG5cblxuXG5cbi8vIC8qKlxuLy8gICogVXNlcyBjYW52YXMubWVhc3VyZVRleHQgdG8gY29tcHV0ZSBhbmQgcmV0dXJuIHRoZSB3aWR0aCBvZiB0aGUgZ2l2ZW4gdGV4dCBvZiBnaXZlbiBmb250IGluIHBpeGVscy5cbi8vICAqIFxuLy8gICogQHBhcmFtIHtTdHJpbmd9IHRleHQgVGhlIHRleHQgdG8gYmUgcmVuZGVyZWQuXG4vLyAgKiBAcGFyYW0ge1N0cmluZ30gZm9udCBUaGUgY3NzIGZvbnQgZGVzY3JpcHRvciB0aGF0IHRleHQgaXMgdG8gYmUgcmVuZGVyZWQgd2l0aCAoZS5nLiBcImJvbGQgMTRweCB2ZXJkYW5hXCIpLlxuLy8gICogXG4vLyAgKiBAc2VlIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzExODI0MS9jYWxjdWxhdGUtdGV4dC13aWR0aC13aXRoLWphdmFzY3JpcHQvMjEwMTUzOTMjMjEwMTUzOTNcbi8vICAqL1xuLy8gZnVuY3Rpb24gZ2V0VGV4dE1ldHJpY3ModGV4dCwgZm9udCkge1xuLy8gICAgIC8vIHJlLXVzZSBjYW52YXMgb2JqZWN0IGZvciBiZXR0ZXIgcGVyZm9ybWFuY2Vcbi8vICAgICB2YXIgY2FudmFzID0gZ2V0VGV4dE1ldHJpY3MuY2FudmFzIHx8IChnZXRUZXh0TWV0cmljcy5jYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiY2FudmFzXCIpKTtcbi8vICAgICB2YXIgY29udGV4dCA9IGNhbnZhcy5nZXRDb250ZXh0KFwiMmRcIik7XG4vLyAgICAgY29udGV4dC5mb250ID0gZm9udDtcbi8vICAgICB2YXIgbWV0cmljcyA9IGNvbnRleHQubWVhc3VyZVRleHQodGV4dCk7XG4vLyAgICAgcmV0dXJuIG1ldHJpY3M7XG4vLyB9XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4vVUlDb3JlL1VJVmlld0NvbnRyb2xsZXIudHNcIiAvPlxuLy8vPHJlZmVyZW5jZSBwYXRoPVwiVUlDb3JlL1VJVGV4dFZpZXcudHNcIi8+XG5cblxuXG5cblxuY2xhc3MgQXJ0aWNsZVZpZXdDb250cm9sbGVyIGV4dGVuZHMgVUlWaWV3Q29udHJvbGxlciB7XG4gICAgXG4gICAgY29udGVudFRleHRWaWV3OiBVSVRleHRWaWV3XG4gICAgcHJpdmF0ZSBpbWFnZVZpZXc6IFVJSW1hZ2VWaWV3XG4gICAgcHJpdmF0ZSB0aXRsZUxhYmVsOiBVSVRleHRWaWV3XG4gICAgcHJpdmF0ZSBsaW5rQnV0dG9uOiBDQkxpbmtCdXR0b25cbiAgICBcbiAgICBwcml2YXRlIF9yZXRyaWV2ZURhdGEgPSBHcmFwaFFMKFxuICAgICAgICBgcXVlcnkoJGlkOiBJRCEpIHtcbiAgICAgICAgICBuZXdzSXRlbShpZDogJGlkKSB7XG4gICAgICAgICAgICBpZFxuICAgICAgICAgICAgdGl0bGVcbiAgICAgICAgICAgIGNvbnRlbnRcbiAgICAgICAgICAgIHVybFxuICAgICAgICAgICAgaW1nXG4gICAgICAgICAgfVxuICAgICAgICB9YFxuICAgIClcbiAgICBcbiAgICBcbiAgICBjb25zdHJ1Y3Rvcih2aWV3KSB7XG4gICAgICAgIFxuICAgICAgICAvLyBDYWxsaW5nIHN1cGVyXG4gICAgICAgIHN1cGVyKHZpZXcpXG4gICAgICAgIFxuICAgICAgICAvLyBDb2RlIGZvciBmdXJ0aGVyIHNldHVwIGlmIG5lY2Vzc2FyeVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgbG9hZEludHJvc3BlY3Rpb25WYXJpYWJsZXMoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5sb2FkSW50cm9zcGVjdGlvblZhcmlhYmxlcygpXG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJVmlld0NvbnRyb2xsZXJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHN0YXRpYyByZWFkb25seSByb3V0ZUNvbXBvbmVudE5hbWUgPSBcImFydGljbGVcIlxuICAgIFxuICAgIHN0YXRpYyByZWFkb25seSBQYXJhbWV0ZXJJZGVudGlmaWVyTmFtZSA9IHsgXCJJRFwiOiBcIklEXCIgfVxuICAgIFxuICAgIFxuICAgIGxvYWRTdWJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlldy5iYWNrZ3JvdW5kQ29sb3IgPSBVSUNvbG9yLndoaXRlQ29sb3JcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaW1hZ2VWaWV3ID0gbmV3IFVJSW1hZ2VWaWV3KHRoaXMudmlldy5lbGVtZW50SUQgKyBcIkltYWdlVmlld1wiKVxuICAgICAgICB0aGlzLmltYWdlVmlldy5oaWRkZW4gPSBZRVNcbiAgICAgICAgdGhpcy52aWV3LmFkZFN1YnZpZXcodGhpcy5pbWFnZVZpZXcpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmltYWdlVmlldy5maWxsTW9kZSA9IFVJSW1hZ2VWaWV3LmZpbGxNb2RlLmFzcGVjdEZpbGxcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLnRpdGxlTGFiZWwgPSBuZXcgVUlUZXh0Vmlldyh0aGlzLnZpZXcuZWxlbWVudElEICsgXCJUaXRsZUxhYmVsXCIsIFVJVGV4dFZpZXcudHlwZS5oZWFkZXIzKVxuICAgICAgICB0aGlzLnRpdGxlTGFiZWwudGV4dEFsaWdubWVudCA9IFVJVGV4dFZpZXcudGV4dEFsaWdubWVudC5sZWZ0XG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC5uYXRpdmVTZWxlY3Rpb25FbmFibGVkID0gTk9cbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLmlzU2luZ2xlTGluZSA9IE5PXG4gICAgICAgIHRoaXMudmlldy5hZGRTdWJ2aWV3KHRoaXMudGl0bGVMYWJlbClcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLmNvbnRlbnRUZXh0VmlldyA9IG5ldyBVSVRleHRWaWV3KHRoaXMudmlldy5lbGVtZW50SUQgKyBcIlRpdGxlTGFiZWxcIilcbiAgICAgICAgdGhpcy5jb250ZW50VGV4dFZpZXcudGV4dCA9IFwiU29tZSBjb250ZW50XCJcbiAgICAgICAgdGhpcy52aWV3LmFkZFN1YnZpZXcodGhpcy5jb250ZW50VGV4dFZpZXcpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5saW5rQnV0dG9uID0gbmV3IENCTGlua0J1dHRvbigpXG4gICAgICAgIHRoaXMubGlua0J1dHRvbi50ZXh0ID0gXCJSZWFkIG1vcmVcIlxuICAgICAgICB0aGlzLnZpZXcuYWRkU3Vidmlldyh0aGlzLmxpbmtCdXR0b24pXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBhc3luYyBoYW5kbGVSb3V0ZShyb3V0ZTogVUlSb3V0ZSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaGFuZGxlUm91dGUocm91dGUpXG4gICAgICAgIGNvbnN0IGlucXVpcnlDb21wb25lbnQgPSByb3V0ZS5jb21wb25lbnRXaXRoVmlld0NvbnRyb2xsZXIoQXJ0aWNsZVZpZXdDb250cm9sbGVyKVxuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3LnN1YnZpZXdzLmV2ZXJ5RWxlbWVudC5oaWRkZW4gPSBZRVNcbiAgICAgICAgXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC5oaWRkZW4gPSBOT1xuICAgICAgICBcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLnRleHQgPSBcIkxvYWRpbmdcIlxuICAgICAgICBcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbGV0IGRhdGE6IGFueSA9IChhd2FpdCB0aGlzLl9yZXRyaWV2ZURhdGEoeyBpZDogaW5xdWlyeUNvbXBvbmVudC5wYXJhbWV0ZXJzLklEIH0pKS5uZXdzSXRlbVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmltYWdlVmlldy5pbWFnZVNvdXJjZSA9IEZJUlNUKGRhdGEuaW1nLCBcImltYWdlcy9sb2dvMTkyLnBuZ1wiKVxuICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLnRleHQgPSBkYXRhLnRpdGxlXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRUZXh0Vmlldy50ZXh0ID0gZGF0YS5jb250ZW50XG4gICAgICAgICAgICB0aGlzLmxpbmtCdXR0b24udGFyZ2V0ID0gZGF0YS51cmxcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy52aWV3LnN1YnZpZXdzLmV2ZXJ5RWxlbWVudC5oaWRkZW4gPSBOT1xuICAgICAgICAgICAgXG4gICAgICAgIH0gY2F0Y2ggKGV4Y2VwdGlvbikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwudGV4dCA9IFwiUmV0cmlldmluZyBhcnRpY2xlIGRhdGEgZmFpbGVkLlwiXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcm91dGUuZGlkY29tcGxldGVDb21wb25lbnQoaW5xdWlyeUNvbXBvbmVudClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGxheW91dFZpZXdzTWFudWFsbHkoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5sYXlvdXRWaWV3c01hbnVhbGx5KClcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHBhZGRpbmcgPSBSb290Vmlld0NvbnRyb2xsZXIucGFkZGluZ0xlbmd0aFxuICAgICAgICBjb25zdCBsYWJlbEhlaWdodCA9IHBhZGRpbmdcbiAgICAgICAgXG4gICAgICAgIC8vIFZpZXcgYm91bmRzXG4gICAgICAgIHZhciBib3VuZHMgPSB0aGlzLnZpZXcuYm91bmRzXG4gICAgICAgIFxuICAgICAgICB0aGlzLmltYWdlVmlldy5mcmFtZSA9IGJvdW5kcy5yZWN0YW5nbGVXaXRoSGVpZ2h0UmVsYXRpdmVUb1dpZHRoKDkgLyAxNilcbiAgICAgICAgXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC5mcmFtZSA9IHRoaXMuaW1hZ2VWaWV3LmZyYW1lLnJlY3RhbmdsZVdpdGhJbnNldHMocGFkZGluZyAqIDAuNSwgcGFkZGluZyAqIDAuNSwgMCwgMClcbiAgICAgICAgLnJlY3RhbmdsZUZvck5leHRSb3coXG4gICAgICAgICAgICBwYWRkaW5nLFxuICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLmludHJpbnNpY0NvbnRlbnRIZWlnaHQodGhpcy5pbWFnZVZpZXcuZnJhbWUud2lkdGgpXG4gICAgICAgIClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29udGVudFRleHRWaWV3LmZyYW1lID0gdGhpcy50aXRsZUxhYmVsLmZyYW1lLnJlY3RhbmdsZUZvck5leHRSb3coXG4gICAgICAgICAgICBwYWRkaW5nLFxuICAgICAgICAgICAgdGhpcy5jb250ZW50VGV4dFZpZXcuaW50cmluc2ljQ29udGVudEhlaWdodCh0aGlzLnRpdGxlTGFiZWwuZnJhbWUud2lkdGgpXG4gICAgICAgIClcbiAgICAgICAgXG4gICAgICAgIHRoaXMubGlua0J1dHRvbi5mcmFtZSA9IHRoaXMuY29udGVudFRleHRWaWV3LmZyYW1lLnJlY3RhbmdsZUZvck5leHRSb3coXG4gICAgICAgICAgICBwYWRkaW5nLFxuICAgICAgICAgICAgdGhpcy5saW5rQnV0dG9uLmludHJpbnNpY0NvbnRlbnRIZWlnaHQodGhpcy5jb250ZW50VGV4dFZpZXcuZnJhbWUud2lkdGgpXG4gICAgICAgIClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGludHJpbnNpY0NvbnRlbnRIZWlnaHQoY29uc3RyYWluaW5nV2lkdGg6IG51bWJlciA9IDApOiBudW1iZXIge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcGFkZGluZyA9IFJvb3RWaWV3Q29udHJvbGxlci5wYWRkaW5nTGVuZ3RoXG4gICAgICAgIGNvbnN0IGxhYmVsSGVpZ2h0ID0gcGFkZGluZyAqIDEuNVxuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gY29uc3RyYWluaW5nV2lkdGggKiAoOSAvIDE2KSArIHBhZGRpbmcgKiA0ICtcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5pbnRyaW5zaWNDb250ZW50SGVpZ2h0KGNvbnN0cmFpbmluZ1dpZHRoKSArXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRUZXh0Vmlldy5pbnRyaW5zaWNDb250ZW50SGVpZ2h0KGNvbnN0cmFpbmluZ1dpZHRoKSArXG4gICAgICAgICAgICB0aGlzLmxpbmtCdXR0b24uaW50cmluc2ljQ29udGVudEhlaWdodChjb25zdHJhaW5pbmdXaWR0aClcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsIlxuLy8gaHR0cHM6Ly9naXRodWIuY29tL2YvZ3JhcGhxbC5qcy9cblxuXG4vLyBDb25uZWN0IHRvIHNlcnZlclxuXG4vLyBAdHMtaWdub3JlXG5jb25zdCBHcmFwaFFMOiBhbnkgPSBncmFwaHFsKFwiaHR0cHM6Ly9uZXdzLXJlYWRlci5zdGFnbmF0aW9ubGFiLmRldi9ncmFwaHFsXCIsIHtcbiAgICBtZXRob2Q6IFwiUE9TVFwiLCAvLyBQT1NUIGJ5IGRlZmF1bHQuXG4gICAgLy9kZWJ1ZzogdHJ1ZSxcbiAgICBhc0pTT046IHRydWVcbiAgICAvLyBoZWFkZXJzOiB7XG4gICAgLy8gICAgIC8vIGhlYWRlcnNcbiAgICAvLyAgICAgXCJBY2Nlc3MtVG9rZW5cIjogXCJzb21lLWFjY2Vzcy10b2tlblwiXG4gICAgLy8gICAgIC8vIE9SIFwiQWNjZXNzLVRva2VuXCI6ICgpID0+IFwic29tZS1hY2Nlc3MtdG9rZW5cIlxuICAgIC8vIH0sXG4gICAgLy8gZnJhZ21lbnRzOiB7XG4gICAgLy8gICAgIC8vIGZyYWdtZW50cywgeW91IGRvbid0IG5lZWQgdG8gc2F5IGBmcmFnbWVudCBuYW1lYC5cbiAgICAvLyAgICAgYXV0aDogXCJvbiBVc2VyIHsgdG9rZW4gfVwiLFxuICAgIC8vICAgICBlcnJvcjogXCJvbiBFcnJvciB7IG1lc3NhZ2VzIH1cIlxuICAgIC8vIH1cbn0pXG5cblxuXG5cblxuXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi4vVUlDb3JlL1VJQnV0dG9uLnRzXCIgLz5cblxuXG5cblxuXG5jbGFzcyBDQkJ1dHRvbiBleHRlbmRzIFVJQnV0dG9uIHtcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50SUQ/OiBzdHJpbmcsIGVsZW1lbnRUeXBlPzogc3RyaW5nKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQsIGVsZW1lbnRUeXBlKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBDQkJ1dHRvblxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSUJ1dHRvblxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgaW5pdFZpZXcoZWxlbWVudElEOiBzdHJpbmcsIHZpZXdIVE1MRWxlbWVudDogSFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaW5pdFZpZXcoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YSlcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICAvL3RoaXMuc3R5bGUuYm9yZGVyUmFkaXVzID0gXCIycHhcIlxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS5vdXRsaW5lID0gXCJub25lXCJcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLmNvbG9ycy50aXRsZUxhYmVsLm5vcm1hbCA9IFVJQ29sb3Iud2hpdGVDb2xvclxuICAgICAgICB0aGlzLnNldEJhY2tncm91bmRDb2xvcnNXaXRoTm9ybWFsQ29sb3IoVUlDb2xvci5ibHVlQ29sb3IpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmNvbG9ycy50aXRsZUxhYmVsLnNlbGVjdGVkID0gVUlDb2xvci5ibHVlQ29sb3JcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzZXRCYWNrZ3JvdW5kQ29sb3JzV2l0aE5vcm1hbENvbG9yKG5vcm1hbEJhY2tncm91bmRDb2xvcjogVUlDb2xvcikge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jb2xvcnMuYmFja2dyb3VuZC5ub3JtYWwgPSBub3JtYWxCYWNrZ3JvdW5kQ29sb3JcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29sb3JzLmJhY2tncm91bmQuaG92ZXJlZCA9IFVJQ29sb3IuY29sb3JXaXRoUkdCQSg0MCwgMTY4LCAxODMpIC8vIG5vcm1hbEJhY2tncm91bmRDb2xvci5jb2xvckJ5TXVsdGlwbHlpbmdSR0IoMC44NSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29sb3JzLmJhY2tncm91bmQuZm9jdXNlZCA9IG5vcm1hbEJhY2tncm91bmRDb2xvciAvLyBub3JtYWxCYWNrZ3JvdW5kQ29sb3IuY29sb3JCeU11bHRpcGx5aW5nUkdCKDAuNSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29sb3JzLmJhY2tncm91bmQuaGlnaGxpZ2h0ZWQgPSBVSUNvbG9yLmNvbG9yV2l0aFJHQkEoNDgsIDE5NiwgMjEyKSAvLyBub3JtYWxCYWNrZ3JvdW5kQ29sb3IuY29sb3JCeU11bHRpcGx5aW5nUkdCKDAuNylcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29sb3JzLmJhY2tncm91bmQuc2VsZWN0ZWQgPSBVSUNvbG9yLndoaXRlQ29sb3JcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yTm9ybWFsU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci51cGRhdGVDb250ZW50Rm9yTm9ybWFsU3RhdGUoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRCb3JkZXIoMCwgMClcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy5zdHlsZS5ib3hTaGFkb3cgPSBcIjAgMnB4IDJweCAwIHJnYmEoMCwwLDAsMC4yNClcIlxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckhvdmVyZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLnVwZGF0ZUNvbnRlbnRGb3JIb3ZlcmVkU3RhdGUoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXRCb3JkZXIoMCwgMClcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy50aXRsZUxhYmVsLnRleHRDb2xvciA9IFVJQ29sb3Iud2hpdGVDb2xvci5jb2xvckJ5TXVsdGlwbHlpbmdSR0IoMC44NSk7XG4gICAgICAgIFxuICAgICAgICAvL3RoaXMuc3R5bGUuYm94U2hhZG93ID0gXCIwIDJweCAycHggMCByZ2JhKDAsMCwwLDAuMTgpXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHVwZGF0ZUNvbnRlbnRGb3JGb2N1c2VkU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci51cGRhdGVDb250ZW50Rm9yRm9jdXNlZFN0YXRlKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2V0Qm9yZGVyKDAsIDEsIFVJQ29sb3IuYmx1ZUNvbG9yKVxuICAgICAgICBcbiAgICAgICAgLy90aGlzLnRpdGxlTGFiZWwudGV4dENvbG9yID0gVUlDb2xvci53aGl0ZUNvbG9yLmNvbG9yQnlNdWx0aXBseWluZ1JHQigwLjg1KTtcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy5zdHlsZS5ib3hTaGFkb3cgPSBcIjAgMnB4IDJweCAwIHJnYmEoMCwwLDAsMC4xOClcIlxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckhpZ2hsaWdodGVkU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci51cGRhdGVDb250ZW50Rm9ySGlnaGxpZ2h0ZWRTdGF0ZSgpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnNldEJvcmRlcigwLCAwKVxuICAgICAgICBcbiAgICAgICAgLy90aGlzLnRpdGxlTGFiZWwudGV4dENvbG9yID0gVUlDb2xvci53aGl0ZUNvbG9yLmNvbG9yQnlNdWx0aXBseWluZ1JHQigwLjcpO1xuICAgICAgICBcbiAgICAgICAgLy90aGlzLnN0eWxlLmJveFNoYWRvdyA9IFwiMCAycHggMnB4IDAgcmdiYSgwLDAsMCwwLjEyKVwiXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9yQ3VycmVudEVuYWJsZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLnVwZGF0ZUNvbnRlbnRGb3JDdXJyZW50RW5hYmxlZFN0YXRlKClcbiAgICAgICAgXG4gICAgICAgIGlmIChJU19OT1QodGhpcy5lbmFibGVkKSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwudGV4dENvbG9yID0gbmV3IFVJQ29sb3IoXCIjYWRhZGFkXCIpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gbmV3IFVJQ29sb3IoXCIjZTVlNWU1XCIpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuYWxwaGEgPSAxXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9DQkJ1dHRvbi50c1wiIC8+XG5cblxuXG5jbGFzcyBDQkZsYXRCdXR0b24gZXh0ZW5kcyBDQkJ1dHRvbiB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgc3RhdGljIGNvbG9ycyA9IHtcbiAgICAgICAgXG4gICAgICAgIHRpdGxlTGFiZWw6IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLmJsdWVDb2xvcixcbiAgICAgICAgICAgIGhpZ2hsaWdodGVkOiBVSUNvbG9yLmJsdWVDb2xvcixcbiAgICAgICAgICAgIHNlbGVjdGVkOiBVSUNvbG9yLmJsdWVDb2xvclxuICAgICAgICAgICAgXG4gICAgICAgIH0sXG4gICAgICAgIGJhY2tncm91bmQ6IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLnRyYW5zcGFyZW50Q29sb3IsXG4gICAgICAgICAgICBob3ZlcmVkOiBuZXcgVUlDb2xvcihcIiNGOEY4RjhcIiksXG4gICAgICAgICAgICBoaWdobGlnaHRlZDogbmV3IFVJQ29sb3IoXCIjZWJlYmViXCIpLFxuICAgICAgICAgICAgc2VsZWN0ZWQ6IG5ldyBVSUNvbG9yKFwiI2ViZWJlYlwiKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRD86IHN0cmluZywgZWxlbWVudFR5cGU/OiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgZWxlbWVudFR5cGUpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IENCRmxhdEJ1dHRvblxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBDQkJ1dHRvblxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgaW5pdFZpZXcoZWxlbWVudElEOiBzdHJpbmcsIHZpZXdIVE1MRWxlbWVudDogSFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaW5pdFZpZXcoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29sb3JzID0ge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aXRsZUxhYmVsOiB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLmJsdWVDb2xvcixcbiAgICAgICAgICAgICAgICBoaWdobGlnaHRlZDogVUlDb2xvci5ibHVlQ29sb3IsXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWQ6IFVJQ29sb3IuYmx1ZUNvbG9yXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYmFja2dyb3VuZDoge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIG5vcm1hbDogVUlDb2xvci50cmFuc3BhcmVudENvbG9yLFxuICAgICAgICAgICAgICAgIGhvdmVyZWQ6IG5ldyBVSUNvbG9yKFwiI0Y4RjhGOFwiKSxcbiAgICAgICAgICAgICAgICBoaWdobGlnaHRlZDogbmV3IFVJQ29sb3IoXCIjZWJlYmViXCIpLFxuICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBuZXcgVUlDb2xvcihcIiNlYmViZWJcIilcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNldCB0aXRsZUxhYmVsQ29sb3IodGl0bGVMYWJlbENvbG9yOiBVSUNvbG9yKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5jb2xvcnMudGl0bGVMYWJlbC5ub3JtYWwgPSB0aXRsZUxhYmVsQ29sb3JcbiAgICAgICAgdGhpcy5jb2xvcnMudGl0bGVMYWJlbC5oaWdobGlnaHRlZCA9IHRpdGxlTGFiZWxDb2xvclxuICAgICAgICB0aGlzLmNvbG9ycy50aXRsZUxhYmVsLnNlbGVjdGVkID0gdGl0bGVMYWJlbENvbG9yXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy51cGRhdGVDb250ZW50Rm9yQ3VycmVudFN0YXRlKClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGdldCB0aXRsZUxhYmVsQ29sb3IoKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuY29sb3JzLnRpdGxlTGFiZWwubm9ybWFsXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHVwZGF0ZUNvbnRlbnRGb3JOb3JtYWxTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIFVJQnV0dG9uLnByb3RvdHlwZS51cGRhdGVDb250ZW50Rm9yTm9ybWFsU3RhdGUuY2FsbCh0aGlzKVxuICAgICAgICBcbiAgICAgICAgLy90aGlzLnN0eWxlLmJveFNoYWRvdyA9IFwiMCAycHggMnB4IDAgcmdiYSgwLDAsMCwwLjI0KVwiXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICB1cGRhdGVDb250ZW50Rm9ySG92ZXJlZFN0YXRlKCkge1xuICAgICAgICBcbiAgICAgICAgVUlCdXR0b24ucHJvdG90eXBlLnVwZGF0ZUNvbnRlbnRGb3JIb3ZlcmVkU3RhdGUuY2FsbCh0aGlzKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICAvL3RoaXMudGl0bGVMYWJlbC50ZXh0Q29sb3IgPSBVSUNvbG9yLndoaXRlQ29sb3IuY29sb3JCeU11bHRpcGx5aW5nUkdCKDAuODUpO1xuICAgICAgICBcbiAgICAgICAgLy90aGlzLnN0eWxlLmJveFNoYWRvdyA9IFwiMCAycHggMnB4IDAgcmdiYSgwLDAsMCwwLjE4KVwiO1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckZvY3VzZWRTdGF0ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIFVJQnV0dG9uLnByb3RvdHlwZS51cGRhdGVDb250ZW50Rm9yRm9jdXNlZFN0YXRlLmNhbGwodGhpcylcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgLy90aGlzLnRpdGxlTGFiZWwudGV4dENvbG9yID0gVUlDb2xvci53aGl0ZUNvbG9yLmNvbG9yQnlNdWx0aXBseWluZ1JHQigwLjg1KTtcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy5zdHlsZS5ib3hTaGFkb3cgPSBcIjAgMnB4IDJweCAwIHJnYmEoMCwwLDAsMC4xOClcIlxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgdXBkYXRlQ29udGVudEZvckhpZ2hsaWdodGVkU3RhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBVSUJ1dHRvbi5wcm90b3R5cGUudXBkYXRlQ29udGVudEZvckhpZ2hsaWdodGVkU3RhdGUuY2FsbCh0aGlzKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICAvL3RoaXMudGl0bGVMYWJlbC50ZXh0Q29sb3IgPSBVSUNvbG9yLndoaXRlQ29sb3IuY29sb3JCeU11bHRpcGx5aW5nUkdCKDAuNyk7XG4gICAgICAgIFxuICAgICAgICAvL3RoaXMuc3R5bGUuYm94U2hhZG93ID0gXCIwIDJweCAycHggMCByZ2JhKDAsMCwwLDAuMTIpXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4uL1VJQ29yZS9VSUxpbmtCdXR0b24udHNcIiAvPlxuXG5cblxuXG5cbmNsYXNzIENCTGlua0J1dHRvbiBleHRlbmRzIFVJTGlua0J1dHRvbiB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEPzogc3RyaW5nLCBlbGVtZW50VHlwZT86IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIoZWxlbWVudElELCBlbGVtZW50VHlwZSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gQ0JMaW5rQnV0dG9uXG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJTGlua0J1dHRvblxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgaW5pdFZpZXcoZWxlbWVudElEOiBzdHJpbmcsIHZpZXdIVE1MRWxlbWVudDogSFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaW5pdFZpZXcoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQsIGluaXRWaWV3RGF0YSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYnV0dG9uLnJlbW92ZUZyb21TdXBlcnZpZXcoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5idXR0b24gPSBuZXcgQ0JCdXR0b24odGhpcy5lbGVtZW50SUQgKyBcIkJ1dHRvblwiLCBpbml0Vmlld0RhdGEuZWxlbWVudFR5cGUpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmFkZFN1YnZpZXcodGhpcy5idXR0b24pXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuLi9VSUNvcmUvVUlWaWV3LnRzXCIgLz5cblxuXG5cblxuXG5jbGFzcyBDZWxsVmlldyBleHRlbmRzIFVJQnV0dG9uIHtcbiAgICBcbiAgICBcbiAgICBfaXNBQnV0dG9uID0gTk9cbiAgICBfcmlnaHRJbWFnZVZpZXc6IFVJSW1hZ2VWaWV3XG4gICAgXG4gICAgbGVmdEluc2V0ID0gMFxuICAgIHJpZ2h0SW5zZXQgPSAwXG4gICAgXG4gICAgLy90aXRsZUxhYmVsOiBVSVRleHRWaWV3O1xuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRD86IHN0cmluZywgdGl0bGVMYWJlbFR5cGU6IHN0cmluZyA9IFVJVGV4dFZpZXcudHlwZS5zcGFuKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQsIHVuZGVmaW5lZCwgdGl0bGVMYWJlbFR5cGUpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IENlbGxWaWV3XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJVmlld1xuICAgICAgICBcbiAgICAgICAgLy8gdGhpcy50aXRsZUxhYmVsID0gbmV3IFVJVGV4dFZpZXcodGhpcy5lbGVtZW50SUQgKyBcIlRpdGxlTGFiZWxcIiwgdGl0bGVMYWJlbFR5cGUpO1xuICAgICAgICAvLyB0aGlzLmFkZFN1YnZpZXcodGhpcy50aXRsZUxhYmVsKTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlRm9yQ3VycmVudElzQUJ1dHRvblN0YXRlKClcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzZXQgaXNBQnV0dG9uKGlzQUJ1dHRvbjogYm9vbGVhbikge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5faXNBQnV0dG9uID0gaXNBQnV0dG9uXG4gICAgICAgIFxuICAgICAgICB0aGlzLnVwZGF0ZUZvckN1cnJlbnRJc0FCdXR0b25TdGF0ZSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgaXNBQnV0dG9uKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzQUJ1dHRvblxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgdXBkYXRlRm9yQ3VycmVudElzQUJ1dHRvblN0YXRlKCkge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl9pc0FCdXR0b24pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zdHlsZS5jdXJzb3IgPSBcInBvaW50ZXJcIlxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwudXNlckludGVyYWN0aW9uRW5hYmxlZCA9IE5PXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMudGl0bGVMYWJlbC5uYXRpdmVTZWxlY3Rpb25FbmFibGVkID0gTk9cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy50aXRsZUxhYmVsLnRleHRBbGlnbm1lbnQgPSBVSVRleHRWaWV3LnRleHRBbGlnbm1lbnQuY2VudGVyXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMubmF0aXZlU2VsZWN0aW9uRW5hYmxlZCA9IE5PXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3R5bGUub3V0bGluZSA9IFwiXCJcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5jb2xvcnMgPSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGl0bGVMYWJlbDoge1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgbm9ybWFsOiBVSUNvbG9yLmJsdWVDb2xvcixcbiAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IFVJQ29sb3IuYmx1ZUNvbG9yLFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZDogVUlDb2xvci5ibHVlQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBub3JtYWw6IFVJQ29sb3IudHJhbnNwYXJlbnRDb2xvcixcbiAgICAgICAgICAgICAgICAgICAgaG92ZXJlZDogbmV3IFVJQ29sb3IoXCIjRjhGOEY4XCIpLFxuICAgICAgICAgICAgICAgICAgICBoaWdobGlnaHRlZDogbmV3IFVJQ29sb3IoXCIjZWJlYmViXCIpLFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZDogbmV3IFVJQ29sb3IoXCIjZWJlYmViXCIpXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc3R5bGUuY3Vyc29yID0gXCJcIlxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwudXNlckludGVyYWN0aW9uRW5hYmxlZCA9IFlFU1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwubmF0aXZlU2VsZWN0aW9uRW5hYmxlZCA9IFlFU1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwudGV4dEFsaWdubWVudCA9IFVJVGV4dFZpZXcudGV4dEFsaWdubWVudC5sZWZ0XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMubmF0aXZlU2VsZWN0aW9uRW5hYmxlZCA9IFlFU1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnN0eWxlLm91dGxpbmUgPSBcIm5vbmVcIlxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmNvbG9ycyA9IHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aXRsZUxhYmVsOiB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBub3JtYWw6IFVJQ29sb3IuYmxhY2tDb2xvcixcbiAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IFVJQ29sb3IuYmxhY2tDb2xvcixcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWQ6IFVJQ29sb3IuYmxhY2tDb2xvclxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHtcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIG5vcm1hbDogVUlDb2xvci50cmFuc3BhcmVudENvbG9yLFxuICAgICAgICAgICAgICAgICAgICBoaWdobGlnaHRlZDogVUlDb2xvci50cmFuc3BhcmVudENvbG9yLFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZDogVUlDb2xvci50cmFuc3BhcmVudENvbG9yXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlQ29udGVudEZvckN1cnJlbnRTdGF0ZSgpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGluaXRSaWdodEltYWdlVmlld0lmTmVlZGVkKCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX3JpZ2h0SW1hZ2VWaWV3KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9yaWdodEltYWdlVmlldyA9IG5ldyBVSUltYWdlVmlldyh0aGlzLmVsZW1lbnRJRCArIFwiUmlnaHRJbWFnZVZpZXdcIilcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3JpZ2h0SW1hZ2VWaWV3LnVzZXJJbnRlcmFjdGlvbkVuYWJsZWQgPSBOT1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2V0IHJpZ2h0SW1hZ2VTb3VyY2UoaW1hZ2VTb3VyY2U6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAoSVMoaW1hZ2VTb3VyY2UpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuaW5pdFJpZ2h0SW1hZ2VWaWV3SWZOZWVkZWQoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9yaWdodEltYWdlVmlldy5pbWFnZVNvdXJjZSA9IGltYWdlU291cmNlXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuYWRkU3Vidmlldyh0aGlzLl9yaWdodEltYWdlVmlldylcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9yaWdodEltYWdlVmlldy5yZW1vdmVGcm9tU3VwZXJ2aWV3KClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IHJpZ2h0SW1hZ2VTb3VyY2UoKSB7XG4gICAgXG4gICAgICAgIHZhciByZXN1bHQgPSBuaWxcbiAgICBcbiAgICAgICAgaWYgKHRoaXMuX3JpZ2h0SW1hZ2VWaWV3KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuX3JpZ2h0SW1hZ2VWaWV3LmltYWdlU291cmNlXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgbGF5b3V0U3Vidmlld3MoKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5sYXlvdXRTdWJ2aWV3cygpXG4gICAgXG4gICAgICAgIGNvbnN0IHBhZGRpbmcgPSBSb290Vmlld0NvbnRyb2xsZXIucGFkZGluZ0xlbmd0aFxuICAgICAgICBjb25zdCBsYWJlbEhlaWdodCA9IHBhZGRpbmdcbiAgICBcbiAgICAgICAgY29uc3QgYm91bmRzID0gdGhpcy5ib3VuZHNcbiAgICBcbiAgICBcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLmNlbnRlcllJbkNvbnRhaW5lcigpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnRpdGxlTGFiZWwuc3R5bGUubGVmdCA9IFwiXCIgKyAocGFkZGluZyAqIDAuNSArIHRoaXMubGVmdEluc2V0KS5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLnN0eWxlLnJpZ2h0ID0gXCJcIiArIChwYWRkaW5nICogMC41ICsgdGhpcy5yaWdodEluc2V0KS5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgXG4gICAgICAgIHRoaXMudGl0bGVMYWJlbC5zdHlsZS5tYXhIZWlnaHQgPSBcIjEwMCVcIlxuICAgICAgICBcbiAgICAgICAgdGhpcy50aXRsZUxhYmVsLnN0eWxlLm92ZXJmbG93ID0gXCJoaWRkZW5cIlxuICAgICAgICBcbiAgICAgICAgLy90aGlzLnRpdGxlTGFiZWwuc3R5bGUud2hpdGVTcGFjZSA9IFwibm93cmFwXCI7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX3JpZ2h0SW1hZ2VWaWV3ICYmIHRoaXMuX3JpZ2h0SW1hZ2VWaWV3LnN1cGVydmlldyA9PSB0aGlzKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIHZhciBpbWFnZUhlaWdodCA9IGJvdW5kcy5oZWlnaHQgLSBwYWRkaW5nO1xuICAgICAgICAgICAgLy8gdGhpcy5fcmlnaHRJbWFnZVZpZXcuZnJhbWUgPSBuZXcgVUlSZWN0YW5nbGUoYm91bmRzLndpZHRoIC0gaW1hZ2VIZWlnaHQgLSBwYWRkaW5nICogMC41LCBwYWRkaW5nICogMC41LCBpbWFnZUhlaWdodCwgaW1hZ2VIZWlnaHQpO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9yaWdodEltYWdlVmlldy5mcmFtZSA9IGJvdW5kcy5yZWN0YW5nbGVXaXRoSW5zZXRzKHRoaXMubGVmdEluc2V0LCBwYWRkaW5nICogMC41ICtcbiAgICAgICAgICAgICAgICB0aGlzLnJpZ2h0SW5zZXQsIDAsIDApLnJlY3RhbmdsZVdpdGhXaWR0aCgyNCwgMSkucmVjdGFuZ2xlV2l0aEhlaWdodCgyNCwgMC41KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnRpdGxlTGFiZWwuc3R5bGUucmlnaHQgPSBcIlwiICtcbiAgICAgICAgICAgICAgICAocGFkZGluZyAqIDAuNSArIHRoaXMucmlnaHRJbnNldCArIHRoaXMuX3JpZ2h0SW1hZ2VWaWV3LmZyYW1lLndpZHRoKS5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsImNsYXNzIFVJQWN0aW9uSW5kaWNhdG9yIGV4dGVuZHMgVUlWaWV3IHtcbiAgICBcbiAgICBcbiAgICBpbmRpY2F0b3JWaWV3OiBVSVZpZXdcbiAgICBcbiAgICBfc2l6ZTogbnVtYmVyID0gNTBcbiAgICBcbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50SUQ/OiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlBY3Rpb25JbmRpY2F0b3JcbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlWaWV3XG4gICAgICAgIFxuICAgICAgICB0aGlzLmluZGljYXRvclZpZXcgPSBuZXcgVUlWaWV3KHRoaXMuZWxlbWVudElEICsgXCJJbmRpY2F0b3JWaWV3XCIpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmFkZFN1YnZpZXcodGhpcy5pbmRpY2F0b3JWaWV3KVxuICAgICAgICBcbiAgICAgICAgdGhpcy5pbmRpY2F0b3JWaWV3LnZpZXdIVE1MRWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiTHVrZUhhYXNMb2FkZXJcIilcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaGlkZGVuID0gWUVTXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgc2V0IHNpemUoc2l6ZTogbnVtYmVyKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zaXplID0gc2l6ZVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dFVwVG9Sb290VmlldygpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgc2l6ZSgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLl9zaXplXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzZXQgaGlkZGVuKGhpZGRlbjogYm9vbGVhbikge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaGlkZGVuID0gaGlkZGVuXG4gICAgICAgIFxuICAgICAgICBpZiAoaGlkZGVuKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuaW5kaWNhdG9yVmlldy5yZW1vdmVGcm9tU3VwZXJ2aWV3KClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmFkZFN1YnZpZXcodGhpcy5pbmRpY2F0b3JWaWV3KVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHN0YXJ0KCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5oaWRkZW4gPSBOT1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc3RvcCgpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaGlkZGVuID0gWUVTXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBsYXlvdXRTdWJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmxheW91dFN1YnZpZXdzKClcbiAgICBcbiAgICAgICAgY29uc3QgYm91bmRzID0gdGhpcy5ib3VuZHNcbiAgICBcbiAgICAgICAgLy90aGlzLmluZGljYXRvclZpZXcuY2VudGVySW5Db250YWluZXIoKTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaW5kaWNhdG9yVmlldy5zdHlsZS5oZWlnaHQgPSBcIlwiICsgdGhpcy5fc2l6ZS5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgdGhpcy5pbmRpY2F0b3JWaWV3LnN0eWxlLndpZHRoID0gXCJcIiArIHRoaXMuX3NpemUuaW50ZWdlclZhbHVlICsgXCJweFwiXG4gICAgXG4gICAgICAgIGNvbnN0IG1pblNpemUgPSBNYXRoLm1pbih0aGlzLmJvdW5kcy5oZWlnaHQsIHRoaXMuYm91bmRzLndpZHRoKVxuICAgIFxuICAgICAgICB0aGlzLmluZGljYXRvclZpZXcuc3R5bGUubWF4SGVpZ2h0ID0gXCJcIiArIG1pblNpemUuaW50ZWdlclZhbHVlICsgXCJweFwiXG4gICAgICAgIHRoaXMuaW5kaWNhdG9yVmlldy5zdHlsZS5tYXhXaWR0aCA9IFwiXCIgKyBtaW5TaXplLmludGVnZXJWYWx1ZSArIFwicHhcIlxuICAgIFxuICAgICAgICBjb25zdCBzaXplID0gTWF0aC5taW4odGhpcy5fc2l6ZSwgbWluU2l6ZSlcbiAgICBcbiAgICAgICAgdGhpcy5pbmRpY2F0b3JWaWV3LnN0eWxlLmxlZnQgPSBcIlwiICsgKChib3VuZHMud2lkdGggLSBzaXplKSAqIDAuNSAtIDExKS5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgdGhpcy5pbmRpY2F0b3JWaWV3LnN0eWxlLnRvcCA9IFwiXCIgKyAoKGJvdW5kcy5oZWlnaHQgLSBzaXplKSAqIDAuNSAtIDExKS5pbnRlZ2VyVmFsdWUgKyBcInB4XCJcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCJjbGFzcyBVSURhdGVUaW1lSW5wdXQgZXh0ZW5kcyBVSVZpZXcge1xuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRDogc3RyaW5nLCB0eXBlOiBzdHJpbmcgPSBVSURhdGVUaW1lSW5wdXQudHlwZS5EYXRlVGltZSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIoZWxlbWVudElELCBuaWwsIFwiaW5wdXRcIilcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlEYXRlVGltZUlucHV0XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJVmlld1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LnNldEF0dHJpYnV0ZShcInR5cGVcIiwgdHlwZSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50Lm9uY2hhbmdlID0gKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlEYXRlVGltZUlucHV0LmNvbnRyb2xFdmVudC5WYWx1ZUNoYW5nZSwgZXZlbnQpXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgY29udHJvbEV2ZW50ID0gT2JqZWN0LmFzc2lnbih7fSwgVUlWaWV3LmNvbnRyb2xFdmVudCwge1xuICAgICAgICBcbiAgICAgICAgXCJWYWx1ZUNoYW5nZVwiOiBcIlZhbHVlQ2hhbmdlXCJcbiAgICAgICAgXG4gICAgfSlcbiAgICBcbiAgICBnZXQgYWRkQ29udHJvbEV2ZW50VGFyZ2V0KCk6IFVJVmlld0FkZENvbnRyb2xFdmVudFRhcmdldE9iamVjdDx0eXBlb2YgVUlEYXRlVGltZUlucHV0LmNvbnRyb2xFdmVudD4ge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHN1cGVyLmFkZENvbnRyb2xFdmVudFRhcmdldCBhcyBhbnlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyB0eXBlID0ge1xuICAgICAgICBcbiAgICAgICAgXCJEYXRlXCI6IFwiZGF0ZVwiLFxuICAgICAgICBcIlRpbWVcIjogXCJ0aW1lXCIsXG4gICAgICAgIFwiRGF0ZVRpbWVcIjogXCJkYXRldGltZVwiXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc3RhdGljIGZvcm1hdCA9IHtcbiAgICAgICAgXG4gICAgICAgIFwiRXVyb3BlYW5cIjogXCJERC1NTS1ZWVlZXCIsXG4gICAgICAgIFwiSVNPQ29tcHV0ZXJcIjogXCJZWVlZLU1NLUREXCIsXG4gICAgICAgIFwiQW1lcmljYW5cIjogXCJNTS9ERC9ZWVlZXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCBkYXRlKCkge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IERhdGUoKHRoaXMudmlld0hUTUxFbGVtZW50IGFzIEhUTUxJbnB1dEVsZW1lbnQpLnZhbHVlKVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCJcblxuXG5cblxuXG5cblxuXG5jbGFzcyBVSUltYWdlVmlldyBleHRlbmRzIFVJVmlldyB7XG4gICAgXG4gICAgXG4gICAgLy9hY3Rpb25JbmRpY2F0b3I6IFVJQWN0aW9uSW5kaWNhdG9yO1xuICAgIF9zb3VyY2VLZXk6IHN0cmluZ1xuICAgIF9kZWZhdWx0U291cmNlOiBzdHJpbmdcbiAgICBcbiAgICBfZmlsbE1vZGU6IGFueVxuICAgIFxuICAgIF9oaWRkZW5XaGVuRW1wdHkgPSBOT1xuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRD8sIHZpZXdIVE1MRWxlbWVudCA9IG51bGwpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgdmlld0hUTUxFbGVtZW50LCBcImltZ1wiKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBVSUltYWdlVmlld1xuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSVZpZXdcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy5hY3Rpb25JbmRpY2F0b3IgPSBuZXcgVUlBY3Rpb25JbmRpY2F0b3IoZWxlbWVudElEICsgXCJBY3Rpb25JbmRpY2F0b3JcIik7XG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgc3RhdGljIGZpbGxNb2RlID0ge1xuICAgICAgICBcbiAgICAgICAgXCJzdHJldGNoVG9GaWxsXCI6IFwiZmlsbFwiLFxuICAgICAgICBcImFzcGVjdEZpdFwiOiBcImNvbnRhaW5cIixcbiAgICAgICAgXCJhc3BlY3RGaWxsXCI6IFwiY292ZXJcIixcbiAgICAgICAgXCJjZW50ZXJcIjogXCJub25lXCIsXG4gICAgICAgIFwiYXNwZWN0Rml0SWZMYXJnZXJcIjogXCJzY2FsZS1kb3duXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCB2aWV3SFRNTEVsZW1lbnQoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gc3VwZXIudmlld0hUTUxFbGVtZW50IGFzIEhUTUxJbWFnZUVsZW1lbnRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBkYXRhVVJMKHVybCwgY2FsbGJhY2spIHtcbiAgICAgICAgY29uc3QgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KClcbiAgICAgICAgeGhyLm9wZW4oXCJnZXRcIiwgdXJsKVxuICAgICAgICB4aHIucmVzcG9uc2VUeXBlID0gXCJibG9iXCJcbiAgICAgICAgeGhyLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGNvbnN0IGZyID0gbmV3IEZpbGVSZWFkZXIoKVxuICAgIFxuICAgICAgICAgICAgZnIub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKHRoaXMucmVzdWx0KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBmci5yZWFkQXNEYXRhVVJMKHhoci5yZXNwb25zZSkgLy8gYXN5bmMgY2FsbFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB4aHIuc2VuZCgpXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBkYXRhVVJMV2l0aE1heFNpemUoVVJMU3RyaW5nOiBzdHJpbmcsIG1heFNpemU6IG51bWJlciwgY29tcGxldGlvbjogKHJlc3VsdFVSTFN0cmluZzogc3RyaW5nKSA9PiB2b2lkKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IGltYWdlVmlldyA9IG5ldyBVSUltYWdlVmlldygpXG4gICAgICAgIGltYWdlVmlldy5pbWFnZVNvdXJjZSA9IFVSTFN0cmluZ1xuICAgICAgICBcbiAgICAgICAgaW1hZ2VWaWV3LnZpZXdIVE1MRWxlbWVudC5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgXG4gICAgICAgICAgICBjb25zdCBvcmlnaW5hbFNpemUgPSBpbWFnZVZpZXcuaW50cmluc2ljQ29udGVudFNpemUoKVxuICAgIFxuICAgIFxuICAgICAgICAgICAgdmFyIG11bHRpcGxpZXIgPSBtYXhTaXplIC8gTWF0aC5tYXgob3JpZ2luYWxTaXplLmhlaWdodCwgb3JpZ2luYWxTaXplLndpZHRoKVxuICAgIFxuICAgICAgICAgICAgbXVsdGlwbGllciA9IE1hdGgubWluKDEsIG11bHRpcGxpZXIpXG4gICAgXG4gICAgXG4gICAgICAgICAgICBjb25zdCByZXN1bHQgPSBpbWFnZVZpZXcuZ2V0RGF0YVVSTCgob3JpZ2luYWxTaXplLmhlaWdodCAqIG11bHRpcGxpZXIpLmludGVnZXJWYWx1ZSwgKG9yaWdpbmFsU2l6ZS53aWR0aCAqXG4gICAgICAgICAgICAgICAgbXVsdGlwbGllcikuaW50ZWdlclZhbHVlKVxuICAgIFxuICAgICAgICAgICAgY29tcGxldGlvbihyZXN1bHQpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc3RhdGljIGRhdGFVUkxXaXRoU2l6ZXMoXG4gICAgICAgIFVSTFN0cmluZzogc3RyaW5nLFxuICAgICAgICBoZWlnaHQ6IG51bWJlcixcbiAgICAgICAgd2lkdGg6IG51bWJlcixcbiAgICAgICAgY29tcGxldGlvbjogKHJlc3VsdFVSTFN0cmluZzogc3RyaW5nKSA9PiB2b2lkXG4gICAgKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IGltYWdlVmlldyA9IG5ldyBVSUltYWdlVmlldygpXG4gICAgICAgIGltYWdlVmlldy5pbWFnZVNvdXJjZSA9IFVSTFN0cmluZ1xuICAgICAgICBcbiAgICAgICAgaW1hZ2VWaWV3LnZpZXdIVE1MRWxlbWVudC5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgXG4gICAgICAgICAgICBjb25zdCByZXN1bHQgPSBpbWFnZVZpZXcuZ2V0RGF0YVVSTChoZWlnaHQsIHdpZHRoKVxuICAgICAgICAgICAgY29tcGxldGlvbihyZXN1bHQpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgZ2V0RGF0YVVSTChoZWlnaHQ/OiBudW1iZXIsIHdpZHRoPzogbnVtYmVyKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IGltZyA9IHRoaXMudmlld0hUTUxFbGVtZW50XG4gICAgXG4gICAgICAgIC8vIENyZWF0ZSBhbiBlbXB0eSBjYW52YXMgZWxlbWVudFxuICAgICAgICBjb25zdCBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiY2FudmFzXCIpXG4gICAgICAgIGNhbnZhcy53aWR0aCA9IHdpZHRoXG4gICAgICAgIGNhbnZhcy5oZWlnaHQgPSBoZWlnaHRcbiAgICAgICAgXG4gICAgICAgIC8vIENvcHkgdGhlIGltYWdlIGNvbnRlbnRzIHRvIHRoZSBjYW52YXNcbiAgICAgICAgY29uc3QgY3R4ID0gY2FudmFzLmdldENvbnRleHQoXCIyZFwiKVxuICAgICAgICBjdHguZHJhd0ltYWdlKGltZywgMCwgMCwgd2lkdGgsIGhlaWdodClcbiAgICAgICAgXG4gICAgICAgIC8vIEdldCB0aGUgZGF0YS1VUkwgZm9ybWF0dGVkIGltYWdlXG4gICAgICAgIC8vIEZpcmVmb3ggc3VwcG9ydHMgUE5HIGFuZCBKUEVHLiBZb3UgY291bGQgY2hlY2sgaW1nLnNyYyB0b1xuICAgICAgICAvLyBndWVzcyB0aGUgb3JpZ2luYWwgZm9ybWF0LCBidXQgYmUgYXdhcmUgdGhlIHVzaW5nIFwiaW1hZ2UvanBnXCJcbiAgICAgICAgLy8gd2lsbCByZS1lbmNvZGUgdGhlIGltYWdlLlxuICAgICAgICBjb25zdCBkYXRhVVJMID0gY2FudmFzLnRvRGF0YVVSTChcImltYWdlL3BuZ1wiKVxuICAgIFxuICAgICAgICByZXR1cm4gZGF0YVVSTFxuICAgICAgICBcbiAgICAgICAgLy9yZXR1cm4gZGF0YVVSTC5yZXBsYWNlKC9eZGF0YTppbWFnZVxcLyhwbmd8anBnKTtiYXNlNjQsLywgXCJcIik7XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgaW1hZ2VTb3VyY2UoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy52aWV3SFRNTEVsZW1lbnQuc3JjXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXQgaW1hZ2VTb3VyY2Uoc291cmNlU3RyaW5nOiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIGlmIChJU19OT1Qoc291cmNlU3RyaW5nKSkge1xuICAgICAgICAgICAgc291cmNlU3RyaW5nID0gXCJcIlxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLnZpZXdIVE1MRWxlbWVudC5zcmMgPSBzb3VyY2VTdHJpbmdcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLmhpZGRlbldoZW5FbXB0eSkge1xuICAgICAgICAgICAgdGhpcy5oaWRkZW4gPSBJU19OT1QodGhpcy5pbWFnZVNvdXJjZSlcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKCFzb3VyY2VTdHJpbmcgfHwgIXNvdXJjZVN0cmluZy5sZW5ndGgpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy90aGlzLmFjdGlvbkluZGljYXRvci5zdG9wKCk7XG4gICAgICAgICAgICB0aGlzLmhpZGRlbiA9IFlFU1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmhpZGRlbiA9IE5PXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgLy8gdGhpcy5zdXBlcnZpZXcuYWRkU3Vidmlldyh0aGlzLmFjdGlvbkluZGljYXRvcik7XG4gICAgICAgIC8vIHRoaXMuYWN0aW9uSW5kaWNhdG9yLmZyYW1lID0gdGhpcy5mcmFtZTtcbiAgICAgICAgLy8gdGhpcy5hY3Rpb25JbmRpY2F0b3Iuc3RhcnQoKTtcbiAgICAgICAgLy8gdGhpcy5hY3Rpb25JbmRpY2F0b3IuYmFja2dyb3VuZENvbG9yID0gVUlDb2xvci5yZWRDb2xvclxuICAgICAgICBcbiAgICAgICAgLy8gQHRzLWlnbm9yZVxuICAgICAgICB0aGlzLnZpZXdIVE1MRWxlbWVudC5vbmxvYWQgPSBmdW5jdGlvbiAodGhpczogVUlJbWFnZVZpZXcsIGV2ZW50OiBFdmVudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnN1cGVydmlldy5zZXROZWVkc0xheW91dCgpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vdGhpcy5hY3Rpb25JbmRpY2F0b3IucmVtb3ZlRnJvbVN1cGVydmlldygpO1xuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc2V0SW1hZ2VTb3VyY2Uoa2V5OiBzdHJpbmcsIGRlZmF1bHRTdHJpbmc6IHN0cmluZykge1xuICAgIFxuICAgICAgICBjb25zdCBsYW5ndWFnZU5hbWUgPSBVSUNvcmUubGFuZ3VhZ2VTZXJ2aWNlLmN1cnJlbnRMYW5ndWFnZUtleVxuICAgICAgICB0aGlzLmltYWdlU291cmNlID0gVUlDb3JlLmxhbmd1YWdlU2VydmljZS5zdHJpbmdGb3JLZXkoa2V5LCBsYW5ndWFnZU5hbWUsIGRlZmF1bHRTdHJpbmcsIG5pbClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGRpZFJlY2VpdmVCcm9hZGNhc3RFdmVudChldmVudDogVUlWaWV3QnJvYWRjYXN0RXZlbnQpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmRpZFJlY2VpdmVCcm9hZGNhc3RFdmVudChldmVudClcbiAgICAgICAgXG4gICAgICAgIGlmIChldmVudC5uYW1lID09IFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuTGFuZ3VhZ2VDaGFuZ2VkIHx8IGV2ZW50Lm5hbWUgPT1cbiAgICAgICAgICAgIFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuQWRkZWRUb1ZpZXdUcmVlKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX3NldEltYWdlU291cmNlRnJvbUtleUlmUG9zc2libGUoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHdpbGxNb3ZlVG9TdXBlcnZpZXcoc3VwZXJ2aWV3OiBVSVZpZXcpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLndpbGxNb3ZlVG9TdXBlcnZpZXcoc3VwZXJ2aWV3KVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fc2V0SW1hZ2VTb3VyY2VGcm9tS2V5SWZQb3NzaWJsZSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBfc2V0SW1hZ2VTb3VyY2VGcm9tS2V5SWZQb3NzaWJsZSgpIHtcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl9zb3VyY2VLZXkgJiYgdGhpcy5fZGVmYXVsdFNvdXJjZSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNldEltYWdlU291cmNlKHRoaXMuX3NvdXJjZUtleSwgdGhpcy5fZGVmYXVsdFNvdXJjZSlcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgZmlsbE1vZGUoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ZpbGxNb2RlXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzZXQgZmlsbE1vZGUoZmlsbE1vZGUpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2ZpbGxNb2RlID0gZmlsbE1vZGU7XG4gICAgICAgIFxuICAgICAgICAodGhpcy5zdHlsZSBhcyBhbnkpLm9iamVjdEZpdCA9IGZpbGxNb2RlXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgaGlkZGVuV2hlbkVtcHR5KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faGlkZGVuV2hlbkVtcHR5XG4gICAgfVxuICAgIFxuICAgIHNldCBoaWRkZW5XaGVuRW1wdHkoaGlkZGVuV2hlbkVtcHR5OiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2hpZGRlbldoZW5FbXB0eSA9IGhpZGRlbldoZW5FbXB0eVxuICAgICAgICBpZiAoaGlkZGVuV2hlbkVtcHR5KSB7XG4gICAgICAgICAgICB0aGlzLmhpZGRlbiA9IElTX05PVCh0aGlzLmltYWdlU291cmNlKVxuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGRpZE1vdmVUb1N1cGVydmlldyhzdXBlcnZpZXc6IFVJVmlldykge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuZGlkTW92ZVRvU3VwZXJ2aWV3KHN1cGVydmlldylcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBsYXlvdXRTdWJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmxheW91dFN1YnZpZXdzKClcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgaW50cmluc2ljQ29udGVudFNpemUoKSB7XG4gICAgXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBVSVJlY3RhbmdsZSgwLCAwLCB0aGlzLnZpZXdIVE1MRWxlbWVudC5uYXR1cmFsSGVpZ2h0LCB0aGlzLnZpZXdIVE1MRWxlbWVudC5uYXR1cmFsV2lkdGgpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBpbnRyaW5zaWNDb250ZW50U2l6ZVdpdGhDb25zdHJhaW50cyhjb25zdHJhaW5pbmdIZWlnaHQgPSAwLCBjb25zdHJhaW5pbmdXaWR0aCA9IDApIHtcbiAgICBcbiAgICAgICAgY29uc3QgaGVpZ2h0UmF0aW8gPSBjb25zdHJhaW5pbmdIZWlnaHQgLyB0aGlzLnZpZXdIVE1MRWxlbWVudC5uYXR1cmFsSGVpZ2h0XG4gICAgXG4gICAgICAgIGNvbnN0IHdpZHRoUmF0aW8gPSBjb25zdHJhaW5pbmdXaWR0aCAvIHRoaXMudmlld0hUTUxFbGVtZW50Lm5hdHVyYWxXaWR0aFxuICAgIFxuICAgICAgICBjb25zdCBtdWx0aXBsaWVyID0gTWF0aC5tYXgoaGVpZ2h0UmF0aW8sIHdpZHRoUmF0aW8pXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBVSVJlY3RhbmdsZSgwLCAwLCB0aGlzLnZpZXdIVE1MRWxlbWVudC5uYXR1cmFsSGVpZ2h0ICpcbiAgICAgICAgICAgIG11bHRpcGxpZXIsIHRoaXMudmlld0hUTUxFbGVtZW50Lm5hdHVyYWxXaWR0aCAqIG11bHRpcGxpZXIpXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCJjbGFzcyBVSUtleVZhbHVlU3RyaW5nRmlsdGVyIGV4dGVuZHMgVUlPYmplY3Qge1xuICAgIFxuICAgIFxuICAgIHN0YXRpYyBfc2hhcmVkV2ViV29ya2VyID0gbmV3IFdvcmtlcihcImNvbXBpbGVkU2NyaXB0cy8vVUlLZXlWYWx1ZVN0cmluZ0ZpbHRlcldlYldvcmtlci5qc1wiKVxuICAgIFxuICAgIHN0YXRpYyBfaW5zdGFuY2VOdW1iZXIgPSAtMVxuICAgIFxuICAgIFxuICAgIF9pbnN0YW5jZU51bWJlcjogbnVtYmVyXG4gICAgXG4gICAgX2lzVGhyZWFkQ2xvc2VkID0gTk9cbiAgICBcbiAgICBfd2ViV29ya2VyID0gVUlLZXlWYWx1ZVN0cmluZ0ZpbHRlci5fc2hhcmVkV2ViV29ya2VyXG4gICAgXG4gICAgY29uc3RydWN0b3IodXNlU2VwYXJhdGVXZWJXb3JrZXIgPSBOTykge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlLZXlWYWx1ZVN0cmluZ0ZpbHRlclxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSU9iamVjdFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGlmICh1c2VTZXBhcmF0ZVdlYldvcmtlcikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl93ZWJXb3JrZXIgPSBuZXcgV29ya2VyKFwiY29tcGlsZWRTY3JpcHRzLy9VSUtleVZhbHVlU3RyaW5nRmlsdGVyV2ViV29ya2VyLmpzXCIpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgVUlLZXlWYWx1ZVN0cmluZ0ZpbHRlci5faW5zdGFuY2VOdW1iZXIgPSBVSUtleVZhbHVlU3RyaW5nRmlsdGVyLl9pbnN0YW5jZU51bWJlciArIDFcbiAgICAgICAgdGhpcy5faW5zdGFuY2VOdW1iZXIgPSBVSUtleVZhbHVlU3RyaW5nRmlsdGVyLl9pbnN0YW5jZU51bWJlclxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCBpbnN0YW5jZUlkZW50aWZpZXIoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5faW5zdGFuY2VOdW1iZXJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGZpbHRlckRhdGEoXG4gICAgICAgIGZpbHRlcmluZ1N0cmluZzogc3RyaW5nLFxuICAgICAgICBkYXRhOiBhbnlbXSxcbiAgICAgICAgZXhjbHVkZWREYXRhOiBzdHJpbmdbXSxcbiAgICAgICAgZGF0YUtleVBhdGg6IHN0cmluZyxcbiAgICAgICAgaWRlbnRpZmllcjogYW55LFxuICAgICAgICBjb21wbGV0aW9uOiAoZmlsdGVyZWREYXRhOiBzdHJpbmdbXSwgZmlsdGVyZWRJbmRleGVzOiBzdHJpbmdbXSwgaWRlbnRpZmllcjogYW55KSA9PiB2b2lkXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX2lzVGhyZWFkQ2xvc2VkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICBcbiAgICAgICAgY29uc3Qgc3RhcnRUaW1lID0gRGF0ZS5ub3coKVxuICAgIFxuICAgICAgICBjb25zdCBpbnN0YW5jZUlkZW50aWZpZXIgPSB0aGlzLmluc3RhbmNlSWRlbnRpZmllclxuICAgIFxuICAgICAgICB0aGlzLl93ZWJXb3JrZXIub25tZXNzYWdlID0gZnVuY3Rpb24gKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKG1lc3NhZ2UuZGF0YS5pbnN0YW5jZUlkZW50aWZpZXIgPT0gaW5zdGFuY2VJZGVudGlmaWVyKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJGaWx0ZXJpbmcgdG9vayBcIiArIChEYXRlLm5vdygpIC0gc3RhcnRUaW1lKSArIFwiIG1zLlwiKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGNvbXBsZXRpb24obWVzc2FnZS5kYXRhLmZpbHRlcmVkRGF0YSwgbWVzc2FnZS5kYXRhLmZpbHRlcmVkSW5kZXhlcywgbWVzc2FnZS5kYXRhLmlkZW50aWZpZXIpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX3dlYldvcmtlci5wb3N0TWVzc2FnZSh7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXCJmaWx0ZXJpbmdTdHJpbmdcIjogZmlsdGVyaW5nU3RyaW5nLFxuICAgICAgICAgICAgICAgIFwiZGF0YVwiOiBkYXRhLFxuICAgICAgICAgICAgICAgIFwiZXhjbHVkZWREYXRhXCI6IGV4Y2x1ZGVkRGF0YSxcbiAgICAgICAgICAgICAgICBcImRhdGFLZXlQYXRoXCI6IGRhdGFLZXlQYXRoLFxuICAgICAgICAgICAgICAgIFwiaWRlbnRpZmllclwiOiBpZGVudGlmaWVyLFxuICAgICAgICAgICAgICAgIFwiaW5zdGFuY2VJZGVudGlmaWVyXCI6IGluc3RhbmNlSWRlbnRpZmllclxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIFxuICAgICAgICB9IGNhdGNoIChleGNlcHRpb24pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29tcGxldGlvbihbXSwgW10sIGlkZW50aWZpZXIpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGNsb3NlVGhyZWFkKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5faXNUaHJlYWRDbG9zZWQgPSBZRVNcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl93ZWJXb3JrZXIgIT0gVUlLZXlWYWx1ZVN0cmluZ0ZpbHRlci5fc2hhcmVkV2ViV29ya2VyKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX3dlYldvcmtlci50ZXJtaW5hdGUoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsImludGVyZmFjZSBVSUtleVZhbHVlU3RyaW5nU29ydGVyU29ydGluZ0luc3RydWN0aW9uIHtcbiAgICBcbiAgICBrZXlQYXRoOiBzdHJpbmc7XG4gICAgXG4gICAgZGF0YVR5cGU6IHN0cmluZztcbiAgICBcbiAgICBkaXJlY3Rpb246IHN0cmluZztcbiAgICBcbiAgICBcbn1cblxuXG5cblxuXG5jbGFzcyBVSUtleVZhbHVlU3RyaW5nU29ydGVyIGV4dGVuZHMgVUlPYmplY3Qge1xuICAgIFxuICAgIFxuICAgIHN0YXRpYyBfc2hhcmVkV2ViV29ya2VyID0gbmV3IFdvcmtlcihcImNvbXBpbGVkU2NyaXB0cy8vVUlLZXlWYWx1ZVN0cmluZ1NvcnRlcldlYldvcmtlci5qc1wiKVxuICAgIFxuICAgIHN0YXRpYyBfaW5zdGFuY2VOdW1iZXIgPSAtMVxuICAgIFxuICAgIFxuICAgIF9pbnN0YW5jZU51bWJlcjogbnVtYmVyXG4gICAgXG4gICAgX2lzVGhyZWFkQ2xvc2VkID0gTk9cbiAgICBcbiAgICBfd2ViV29ya2VyID0gVUlLZXlWYWx1ZVN0cmluZ1NvcnRlci5fc2hhcmVkV2ViV29ya2VyXG4gICAgXG4gICAgY29uc3RydWN0b3IodXNlU2VwYXJhdGVXZWJXb3JrZXIgPSBOTykge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlLZXlWYWx1ZVN0cmluZ1NvcnRlclxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSU9iamVjdFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGlmICh1c2VTZXBhcmF0ZVdlYldvcmtlcikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl93ZWJXb3JrZXIgPSBuZXcgV29ya2VyKFwiY29tcGlsZWRTY3JpcHRzLy9VSUtleVZhbHVlU3RyaW5nU29ydGVyV2ViV29ya2VyLmpzXCIpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgVUlLZXlWYWx1ZVN0cmluZ1NvcnRlci5faW5zdGFuY2VOdW1iZXIgPSBVSUtleVZhbHVlU3RyaW5nU29ydGVyLl9pbnN0YW5jZU51bWJlciArIDFcbiAgICAgICAgdGhpcy5faW5zdGFuY2VOdW1iZXIgPSBVSUtleVZhbHVlU3RyaW5nU29ydGVyLl9pbnN0YW5jZU51bWJlclxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCBpbnN0YW5jZUlkZW50aWZpZXIoKSB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy5faW5zdGFuY2VOdW1iZXJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBkYXRhVHlwZSA9IHtcbiAgICAgICAgXG4gICAgICAgIFwic3RyaW5nXCI6IFwic3RyaW5nXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHN0YXRpYyBkaXJlY3Rpb24gPSB7XG4gICAgICAgIFxuICAgICAgICBcImRlc2NlbmRpbmdcIjogXCJkZXNjZW5kaW5nXCIsXG4gICAgICAgIFwiYXNjZW5kaW5nXCI6IFwiYXNjZW5kaW5nXCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNvcnREYXRhPFQ+KFxuICAgICAgICBkYXRhOiBUW10sXG4gICAgICAgIHNvcnRpbmdJbnN0cnVjdGlvbnM6IFVJS2V5VmFsdWVTdHJpbmdTb3J0ZXJTb3J0aW5nSW5zdHJ1Y3Rpb25bXSxcbiAgICAgICAgaWRlbnRpZmllcjogYW55LFxuICAgICAgICBjb21wbGV0aW9uOiAoc29ydGVkRGF0YTogVFtdLCBzb3J0ZWRJbmRleGVzOiBudW1iZXJbXSwgaWRlbnRpZmllcjogYW55KSA9PiB2b2lkXG4gICAgKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX2lzVGhyZWFkQ2xvc2VkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICBcbiAgICAgICAgY29uc3Qgc3RhcnRUaW1lID0gRGF0ZS5ub3coKVxuICAgIFxuICAgICAgICBjb25zdCBpbnN0YW5jZUlkZW50aWZpZXIgPSB0aGlzLmluc3RhbmNlSWRlbnRpZmllclxuICAgIFxuICAgICAgICB0aGlzLl93ZWJXb3JrZXIub25tZXNzYWdlID0gZnVuY3Rpb24gKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKG1lc3NhZ2UuZGF0YS5pbnN0YW5jZUlkZW50aWZpZXIgPT0gaW5zdGFuY2VJZGVudGlmaWVyKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJTb3J0aW5nIFwiICsgZGF0YS5sZW5ndGggKyBcIiBpdGVtcyB0b29rIFwiICsgKERhdGUubm93KCkgLSBzdGFydFRpbWUpICsgXCIgbXMuXCIpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29tcGxldGlvbihtZXNzYWdlLmRhdGEuc29ydGVkRGF0YSwgbWVzc2FnZS5kYXRhLnNvcnRlZEluZGV4ZXMsIG1lc3NhZ2UuZGF0YS5pZGVudGlmaWVyKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl93ZWJXb3JrZXIucG9zdE1lc3NhZ2Uoe1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIFwiZGF0YVwiOiBkYXRhLFxuICAgICAgICAgICAgICAgIFwic29ydGluZ0luc3RydWN0aW9uc1wiOiBzb3J0aW5nSW5zdHJ1Y3Rpb25zLFxuICAgICAgICAgICAgICAgIFwiaWRlbnRpZmllclwiOiBpZGVudGlmaWVyLFxuICAgICAgICAgICAgICAgIFwiaW5zdGFuY2VJZGVudGlmaWVyXCI6IGluc3RhbmNlSWRlbnRpZmllclxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIFxuICAgICAgICB9IGNhdGNoIChleGNlcHRpb24pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29tcGxldGlvbihbXSwgW10sIGlkZW50aWZpZXIpXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNvcnRlZERhdGE8VD4oXG4gICAgICAgIGRhdGE6IFRbXSxcbiAgICAgICAgc29ydGluZ0luc3RydWN0aW9uczogVUlLZXlWYWx1ZVN0cmluZ1NvcnRlclNvcnRpbmdJbnN0cnVjdGlvbltdLFxuICAgICAgICBpZGVudGlmaWVyOiBhbnkgPSBNQUtFX0lEKClcbiAgICApIHtcbiAgICBcbiAgICAgICAgY29uc3QgcmVzdWx0OiBQcm9taXNlPHtcbiAgICAgICAgXG4gICAgICAgICAgICBzb3J0ZWREYXRhOiBUW10sXG4gICAgICAgICAgICBzb3J0ZWRJbmRleGVzOiBudW1iZXJbXSxcbiAgICAgICAgICAgIGlkZW50aWZpZXI6IGFueVxuICAgICAgICBcbiAgICAgICAgfT4gPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zb3J0RGF0YShkYXRhLCBzb3J0aW5nSW5zdHJ1Y3Rpb25zLCBpZGVudGlmaWVyLCAoc29ydGVkRGF0YSwgc29ydGVkSW5kZXhlcywgc29ydGVkSWRlbnRpZmllcikgPT4ge1xuICAgIFxuICAgICAgICAgICAgICAgIGlmIChzb3J0ZWRJZGVudGlmaWVyID09IGlkZW50aWZpZXIpIHtcbiAgICBcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh7XG4gICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGVkRGF0YTogc29ydGVkRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRlZEluZGV4ZXM6IHNvcnRlZEluZGV4ZXMsXG4gICAgICAgICAgICAgICAgICAgICAgICBpZGVudGlmaWVyOiBzb3J0ZWRJZGVudGlmaWVyXG4gICAgICAgIFxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIH0pXG4gICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGNsb3NlVGhyZWFkKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5faXNUaHJlYWRDbG9zZWQgPSBZRVNcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl93ZWJXb3JrZXIgIT0gVUlLZXlWYWx1ZVN0cmluZ1NvcnRlci5fc2hhcmVkV2ViV29ya2VyKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX3dlYldvcmtlci50ZXJtaW5hdGUoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsImNsYXNzIFVJTGF5b3V0R3JpZCBleHRlbmRzIFVJT2JqZWN0IHtcbiAgICBcbiAgICBcbiAgICBcbiAgICBfZnJhbWU6IFVJUmVjdGFuZ2xlXG4gICAgXG4gICAgX3N1YmZyYW1lczogVUlMYXlvdXRHcmlkW10gPSBbXVxuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGZyYW1lOiBVSVJlY3RhbmdsZSkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBVSUxheW91dEdyaWRcbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlPYmplY3RcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2ZyYW1lID0gZnJhbWVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNwbGl0WEludG8obnVtYmVyT2ZGcmFtZXM6IG51bWJlcikge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5fc3ViZnJhbWVzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG51bWJlck9mRnJhbWVzOyBpKyspIHtcbiAgICBcbiAgICAgICAgICAgICAgICBjb25zdCBhc2QgPSAxXG4gICAgXG4gICAgXG4gICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9VSVZpZXcudHNcIiAvPlxuXG5cblxuXG5cbmNsYXNzIFVJTmF0aXZlU2Nyb2xsVmlldyBleHRlbmRzIFVJVmlldyB7XG4gICAgXG4gICAgXG4gICAgYW5pbWF0aW9uRHVyYXRpb24gPSAwXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQ/KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQsIHZpZXdIVE1MRWxlbWVudClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlOYXRpdmVTY3JvbGxWaWV3XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJVmlld1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUuY3NzVGV4dCA9IHRoaXMuc3R5bGUuY3NzVGV4dCArIFwiLXdlYmtpdC1vdmVyZmxvdy1zY3JvbGxpbmc6IHRvdWNoO1wiXG4gICAgICAgIFxuICAgICAgICB0aGlzLnN0eWxlLm92ZXJmbG93ID0gXCJhdXRvXCJcbiAgICAgICAgXG4gICAgICAgIC8vIHRoaXMuc2Nyb2xsc1ggPSBZRVM7XG4gICAgICAgIC8vIHRoaXMuc2Nyb2xsc1kgPSBZRVM7XG4gICAgICAgIFxuICAgICAgICB0aGlzLnZpZXdIVE1MRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsIGZ1bmN0aW9uICh0aGlzOiBVSU5hdGl2ZVNjcm9sbFZpZXcsIGV2ZW50OiBVSUV2ZW50KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5kaWRTY3JvbGxUb1Bvc2l0aW9uKG5ldyBVSVBvaW50KHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbExlZnQsIHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbFRvcCkpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuYnJvYWRjYXN0RXZlbnRJblN1YnRyZWUoe1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIG5hbWU6IFVJVmlldy5icm9hZGNhc3RFdmVudE5hbWUuUGFnZURpZFNjcm9sbCxcbiAgICAgICAgICAgICAgICBwYXJhbWV0ZXJzOiBuaWxcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZGlkU2Nyb2xsVG9Qb3NpdGlvbihvZmZzZXRQb3NpdGlvbjogVUlQb2ludCkge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgc2Nyb2xsc1goKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9ICh0aGlzLnN0eWxlLm92ZXJmbG93WCA9PSBcInNjcm9sbFwiKVxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuICAgIFxuICAgIHNldCBzY3JvbGxzWChzY3JvbGxzOiBib29sZWFuKSB7XG4gICAgICAgIGlmIChzY3JvbGxzKSB7XG4gICAgICAgICAgICB0aGlzLnN0eWxlLm92ZXJmbG93WCA9IFwic2Nyb2xsXCJcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc3R5bGUub3ZlcmZsb3dYID0gXCJoaWRkZW5cIlxuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCBzY3JvbGxzWSgpIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gKHRoaXMuc3R5bGUub3ZlcmZsb3dZID09IFwic2Nyb2xsXCIpXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG4gICAgXG4gICAgc2V0IHNjcm9sbHNZKHNjcm9sbHM6IGJvb2xlYW4pIHtcbiAgICAgICAgaWYgKHNjcm9sbHMpIHtcbiAgICAgICAgICAgIHRoaXMuc3R5bGUub3ZlcmZsb3dZID0gXCJzY3JvbGxcIlxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zdHlsZS5vdmVyZmxvd1kgPSBcImhpZGRlblwiXG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGNvbnRlbnRPZmZzZXQoKSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBVSVBvaW50KHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbExlZnQsIHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbFRvcClcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzZXQgY29udGVudE9mZnNldChvZmZzZXRQb2ludDogVUlQb2ludCkge1xuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuYW5pbWF0aW9uRHVyYXRpb24pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zY3JvbGxYVG8odGhpcy52aWV3SFRNTEVsZW1lbnQsIG9mZnNldFBvaW50LngsIHRoaXMuYW5pbWF0aW9uRHVyYXRpb24pXG4gICAgICAgICAgICB0aGlzLnNjcm9sbFlUbyh0aGlzLnZpZXdIVE1MRWxlbWVudCwgb2Zmc2V0UG9pbnQueSwgdGhpcy5hbmltYXRpb25EdXJhdGlvbilcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbExlZnQgPSBvZmZzZXRQb2ludC54XG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LnNjcm9sbFRvcCA9IG9mZnNldFBvaW50LnlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHNjcm9sbFRvQm90dG9tKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jb250ZW50T2Zmc2V0ID0gbmV3IFVJUG9pbnQodGhpcy5jb250ZW50T2Zmc2V0LngsIHRoaXMuc2Nyb2xsU2l6ZS5oZWlnaHQgLSB0aGlzLmZyYW1lLmhlaWdodClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNjcm9sbFRvVG9wKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jb250ZW50T2Zmc2V0ID0gbmV3IFVJUG9pbnQodGhpcy5jb250ZW50T2Zmc2V0LngsIDApXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgaXNTY3JvbGxlZFRvQm90dG9tKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuY29udGVudE9mZnNldC5pc0VxdWFsVG8obmV3IFVJUG9pbnQodGhpcy5jb250ZW50T2Zmc2V0LngsIHRoaXMuc2Nyb2xsU2l6ZS5oZWlnaHQgLVxuICAgICAgICAgICAgdGhpcy5mcmFtZS5oZWlnaHQpKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGlzU2Nyb2xsZWRUb1RvcCgpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLmNvbnRlbnRPZmZzZXQuaXNFcXVhbFRvKG5ldyBVSVBvaW50KHRoaXMuY29udGVudE9mZnNldC54LCAwKSlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHNjcm9sbFlUbyhlbGVtZW50LCB0bywgZHVyYXRpb24pIHtcbiAgICAgICAgXG4gICAgICAgIGR1cmF0aW9uID0gZHVyYXRpb24gKiAxMDAwXG4gICAgXG4gICAgICAgIGNvbnN0IHN0YXJ0ID0gZWxlbWVudC5zY3JvbGxUb3BcbiAgICAgICAgY29uc3QgY2hhbmdlID0gdG8gLSBzdGFydFxuICAgICAgICBjb25zdCBpbmNyZW1lbnQgPSAxMFxuICAgIFxuICAgICAgICBjb25zdCBhbmltYXRlU2Nyb2xsID0gZnVuY3Rpb24gKGVsYXBzZWRUaW1lKSB7XG4gICAgICAgICAgICBlbGFwc2VkVGltZSArPSBpbmNyZW1lbnRcbiAgICAgICAgICAgIGNvbnN0IHBvc2l0aW9uID0gdGhpcy5lYXNlSW5PdXQoZWxhcHNlZFRpbWUsIHN0YXJ0LCBjaGFuZ2UsIGR1cmF0aW9uKVxuICAgICAgICAgICAgZWxlbWVudC5zY3JvbGxUb3AgPSBwb3NpdGlvblxuICAgICAgICAgICAgaWYgKGVsYXBzZWRUaW1lIDwgZHVyYXRpb24pIHtcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZVNjcm9sbChlbGFwc2VkVGltZSlcbiAgICAgICAgICAgICAgICB9LCBpbmNyZW1lbnQpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0uYmluZCh0aGlzKVxuICAgIFxuICAgICAgICBhbmltYXRlU2Nyb2xsKDApXG4gICAgfVxuICAgIFxuICAgIHNjcm9sbFhUbyhlbGVtZW50LCB0bywgZHVyYXRpb24pIHtcbiAgICAgICAgXG4gICAgICAgIGR1cmF0aW9uID0gZHVyYXRpb24gKiAxMDAwXG4gICAgXG4gICAgICAgIGNvbnN0IHN0YXJ0ID0gZWxlbWVudC5zY3JvbGxUb3BcbiAgICAgICAgY29uc3QgY2hhbmdlID0gdG8gLSBzdGFydFxuICAgICAgICBjb25zdCBpbmNyZW1lbnQgPSAxMFxuICAgIFxuICAgICAgICBjb25zdCBhbmltYXRlU2Nyb2xsID0gZnVuY3Rpb24gKGVsYXBzZWRUaW1lKSB7XG4gICAgICAgICAgICBlbGFwc2VkVGltZSArPSBpbmNyZW1lbnRcbiAgICAgICAgICAgIGNvbnN0IHBvc2l0aW9uID0gdGhpcy5lYXNlSW5PdXQoZWxhcHNlZFRpbWUsIHN0YXJ0LCBjaGFuZ2UsIGR1cmF0aW9uKVxuICAgICAgICAgICAgZWxlbWVudC5zY3JvbGxMZWZ0ID0gcG9zaXRpb25cbiAgICAgICAgICAgIGlmIChlbGFwc2VkVGltZSA8IGR1cmF0aW9uKSB7XG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGFuaW1hdGVTY3JvbGwoZWxhcHNlZFRpbWUpXG4gICAgICAgICAgICAgICAgfSwgaW5jcmVtZW50KVxuICAgICAgICAgICAgfVxuICAgICAgICB9LmJpbmQodGhpcylcbiAgICBcbiAgICAgICAgYW5pbWF0ZVNjcm9sbCgwKVxuICAgIH1cbiAgICBcbiAgICBlYXNlSW5PdXQoY3VycmVudFRpbWUsIHN0YXJ0LCBjaGFuZ2UsIGR1cmF0aW9uKSB7XG4gICAgICAgIGN1cnJlbnRUaW1lIC89IGR1cmF0aW9uIC8gMlxuICAgICAgICBpZiAoY3VycmVudFRpbWUgPCAxKSB7XG4gICAgICAgICAgICByZXR1cm4gY2hhbmdlIC8gMiAqIGN1cnJlbnRUaW1lICogY3VycmVudFRpbWUgKyBzdGFydFxuICAgICAgICB9XG4gICAgICAgIGN1cnJlbnRUaW1lIC09IDFcbiAgICAgICAgcmV0dXJuIC1jaGFuZ2UgLyAyICogKGN1cnJlbnRUaW1lICogKGN1cnJlbnRUaW1lIC0gMikgLSAxKSArIHN0YXJ0XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuL1VJVmlldy50c1wiIC8+XG5cblxuXG5cblxuY2xhc3MgVUlTY3JvbGxWaWV3IGV4dGVuZHMgVUlWaWV3IHtcbiAgICBcbiAgICBcbiAgICBfY29udGVudE9mZnNldDogVUlQb2ludCA9IG5ldyBVSVBvaW50KDAsIDApXG4gICAgX2NvbnRlbnRTY2FsZTogbnVtYmVyID0gMVxuICAgIFxuICAgIGNvbnRhaW5lclZpZXc6IFVJVmlld1xuICAgIFxuICAgIF9wb2ludGVyRG93bjogYm9vbGVhblxuICAgIFxuICAgIF9zY3JvbGxFbmFibGVkOiBib29sZWFuID0gWUVTXG4gICAgXG4gICAgX3ByZXZpb3VzQ2xpZW50UG9pbnQ6IFVJUG9pbnRcbiAgICBcbiAgICBfaW50cmluc2ljQ29udGVudEZyYW1lOiBVSVJlY3RhbmdsZVxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRDogc3RyaW5nLCB2aWV3SFRNTEVsZW1lbnQ/OiBIVE1MRWxlbWVudCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IFVJU2Nyb2xsVmlld1xuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSVZpZXdcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLmNvbnRhaW5lclZpZXcgPSBuZXcgVUlWaWV3KGVsZW1lbnRJRCArIFwiQ29udGFpbmVyVmlld1wiKVxuICAgICAgICBcbiAgICAgICAgc3VwZXIuYWRkU3Vidmlldyh0aGlzLmNvbnRhaW5lclZpZXcpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS5vdmVyZmxvdyA9IFwiaGlkZGVuXCJcbiAgICAgICAgXG4gICAgICAgIHRoaXMucGF1c2VzUG9pbnRlckV2ZW50cyA9IE5PIC8vWUVTO1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlckRvd24sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5fcG9pbnRlckRvd24gPSBZRVNcbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgIFxuICAgICAgICB0aGlzLmFkZFRhcmdldEZvckNvbnRyb2xFdmVudChVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJVcCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9wb2ludGVyRG93biA9IE5PXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX3ByZXZpb3VzQ2xpZW50UG9pbnQgPSBudWxsXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHNjcm9sbFN0b3BwZWQoKVxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBmdW5jdGlvbiBzY3JvbGxTdG9wcGVkKCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICAvLyBIYW5kbGUgcGFnaW5nIGlmIG5lZWRlZFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLmFkZFRhcmdldEZvckNvbnRyb2xFdmVudChVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJNb3ZlLCBmdW5jdGlvbiAoc2VuZGVyOiBVSVNjcm9sbFZpZXcsIGV2ZW50OiBFdmVudCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoISh0aGlzLl9wb2ludGVyRG93biAmJiB0aGlzLl9zY3JvbGxFbmFibGVkICYmIHRoaXMuX2VuYWJsZWQpKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgXG4gICAgICAgICAgICBjb25zdCBjdXJyZW50Q2xpZW50UG9pbnQgPSBuZXcgVUlQb2ludChuaWwsIG5pbClcbiAgICBcbiAgICAgICAgICAgIGlmICgod2luZG93IGFzIGFueSkuTW91c2VFdmVudCAmJiBldmVudCBpbnN0YW5jZW9mIE1vdXNlRXZlbnQpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBjdXJyZW50Q2xpZW50UG9pbnQueCA9IChldmVudCBhcyBNb3VzZUV2ZW50KS5jbGllbnRYXG4gICAgICAgICAgICAgICAgY3VycmVudENsaWVudFBvaW50LnkgPSAoZXZlbnQgYXMgTW91c2VFdmVudCkuY2xpZW50WVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoKHdpbmRvdyBhcyBhbnkpLlRvdWNoRXZlbnQgJiYgZXZlbnQgaW5zdGFuY2VvZiBUb3VjaEV2ZW50KSB7XG4gICAgXG4gICAgICAgICAgICAgICAgY29uc3QgdG91Y2hFdmVudDogVG91Y2hFdmVudCA9IGV2ZW50XG4gICAgXG4gICAgICAgICAgICAgICAgaWYgKHRvdWNoRXZlbnQudG91Y2hlcy5sZW5ndGggIT0gMSkge1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fcG9pbnRlckRvd24gPSBOT1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9wcmV2aW91c0NsaWVudFBvaW50ID0gbnVsbFxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsU3RvcHBlZCgpXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGN1cnJlbnRDbGllbnRQb2ludC54ID0gdG91Y2hFdmVudC50b3VjaGVzWzBdLmNsaWVudFhcbiAgICAgICAgICAgICAgICBjdXJyZW50Q2xpZW50UG9pbnQueSA9IHRvdWNoRXZlbnQudG91Y2hlc1swXS5jbGllbnRZXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICghdGhpcy5fcHJldmlvdXNDbGllbnRQb2ludCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuX3ByZXZpb3VzQ2xpZW50UG9pbnQgPSBjdXJyZW50Q2xpZW50UG9pbnRcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICBcbiAgICAgICAgICAgIGNvbnN0IGNoYW5nZVBvaW50ID0gY3VycmVudENsaWVudFBvaW50LmNvcHkoKS5zdWJ0cmFjdCh0aGlzLl9wcmV2aW91c0NsaWVudFBvaW50KVxuICAgIFxuICAgIFxuICAgICAgICAgICAgaWYgKHRoaXMuY29udGFpbmVyVmlldy5ib3VuZHMud2lkdGggPD0gdGhpcy5ib3VuZHMud2lkdGgpIHtcbiAgICAgICAgICAgICAgICBjaGFuZ2VQb2ludC54ID0gMFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKDAgPCB0aGlzLmNvbnRlbnRPZmZzZXQueCArIGNoYW5nZVBvaW50LngpIHtcbiAgICAgICAgICAgICAgICBjaGFuZ2VQb2ludC54ID0gLXRoaXMuY29udGVudE9mZnNldC54XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5jb250ZW50T2Zmc2V0LnggKyBjaGFuZ2VQb2ludC54IDwgLXRoaXMuYm91bmRzLndpZHRoKSB7XG4gICAgICAgICAgICAgICAgY2hhbmdlUG9pbnQueCA9IC10aGlzLmJvdW5kcy53aWR0aCAtIHRoaXMuY29udGVudE9mZnNldC54XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0aGlzLmNvbnRhaW5lclZpZXcuYm91bmRzLmhlaWdodCA8PSB0aGlzLmJvdW5kcy5oZWlnaHQpIHtcbiAgICAgICAgICAgICAgICBjaGFuZ2VQb2ludC55ID0gMFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKDAgPCB0aGlzLmNvbnRlbnRPZmZzZXQueSArIGNoYW5nZVBvaW50LnkpIHtcbiAgICAgICAgICAgICAgICBjaGFuZ2VQb2ludC55ID0gLXRoaXMuY29udGVudE9mZnNldC55XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5jb250ZW50T2Zmc2V0LnkgKyBjaGFuZ2VQb2ludC55IDwgLXRoaXMuYm91bmRzLmhlaWdodCkge1xuICAgICAgICAgICAgICAgIGNoYW5nZVBvaW50LnkgPSAtdGhpcy5ib3VuZHMuaGVpZ2h0IC0gdGhpcy5jb250ZW50T2Zmc2V0LnlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5jb250ZW50T2Zmc2V0ID0gdGhpcy5jb250ZW50T2Zmc2V0LmFkZChjaGFuZ2VQb2ludClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5fcHJldmlvdXNDbGllbnRQb2ludCA9IGN1cnJlbnRDbGllbnRQb2ludFxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGludmFsaWRhdGVJbnRyaW5zaWNDb250ZW50RnJhbWUoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9pbnRyaW5zaWNDb250ZW50RnJhbWUgPSBuaWxcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGdldCBjb250ZW50T2Zmc2V0KCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbnRlbnRPZmZzZXRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHNldCBjb250ZW50T2Zmc2V0KG9mZnNldDogVUlQb2ludCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fY29udGVudE9mZnNldCA9IG9mZnNldFxuICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGxheW91dFN1YnZpZXdzKCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIubGF5b3V0U3Vidmlld3MoKVxuICAgICAgICBcbiAgICAgICAgLy8gdmFyIGludHJpbnNpY0NvbnRlbnRGcmFtZSA9IHRoaXMuX2ludHJpbnNpY0NvbnRlbnRGcmFtZTtcbiAgICAgICAgLy8gaWYgKCFJUyhpbnRyaW5zaWNDb250ZW50RnJhbWUpKSB7XG4gICAgICAgIC8vICAgICBpbnRyaW5zaWNDb250ZW50RnJhbWUgPSB0aGlzLmNvbnRhaW5lclZpZXcuaW50cmluc2ljQ29udGVudFNpemVXaXRoQ29uc3RyYWludHMoKTsgICBcbiAgICAgICAgLy8gfVxuICAgICAgICAvLyBpbnRyaW5zaWNDb250ZW50RnJhbWUub2Zmc2V0QnlQb2ludCh0aGlzLmNvbnRlbnRPZmZzZXQpO1xuICAgICAgICAvLyBpbnRyaW5zaWNDb250ZW50RnJhbWUuaGVpZ2h0ID0gdGhpcy5jb250YWluZXJWaWV3LnZpZXdIVE1MRWxlbWVudC5zY3JvbGxIZWlnaHQ7XG4gICAgICAgIC8vIGludHJpbnNpY0NvbnRlbnRGcmFtZS53aWR0aCA9IHRoaXMuY29udGFpbmVyVmlldy52aWV3SFRNTEVsZW1lbnQuc2Nyb2xsV2lkdGg7XG4gICAgICAgIC8vIHRoaXMuY29udGFpbmVyVmlldy5mcmFtZSA9IGludHJpbnNpY0NvbnRlbnRGcmFtZTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29udGFpbmVyVmlldy5mcmFtZSA9IHRoaXMuY29udGFpbmVyVmlldy5ib3VuZHMub2Zmc2V0QnlQb2ludCh0aGlzLmNvbnRlbnRPZmZzZXQpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICAvLyBnZXQgX3N1YnZpZXdzKCkge1xuICAgIC8vICAgICByZXR1cm4gc3VwZXIuc3Vidmlld3M7XG4gICAgLy8gfVxuICAgIFxuICAgIC8vIHNldCBfc3Vidmlld3Moc3Vidmlld3M6IFVJVmlld1tdKSB7XG4gICAgLy8gICAgIHN1cGVyLnN1YnZpZXdzID0gc3Vidmlld3M7XG4gICAgLy8gfVxuICAgIFxuICAgIC8vIGdldCBzdWJ2aWV3cygpIHtcbiAgICAvLyAgICAgcmV0dXJuIHRoaXMuY29udGFpbmVyVmlldy5zdWJ2aWV3cztcbiAgICAvLyB9XG4gICAgXG4gICAgLy8gc2V0IHN1YnZpZXdzKHN1YnZpZXdzOiBVSVZpZXdbXSkge1xuICAgIFxuICAgIC8vICAgICB0aGlzLmNvbnRhaW5lclZpZXcuc3Vidmlld3MgPSBzdWJ2aWV3cztcbiAgICBcbiAgICAvLyAgICAgdGhpcy5pbnZhbGlkYXRlSW50cmluc2ljQ29udGVudEZyYW1lKCk7XG4gICAgXG4gICAgXG4gICAgLy8gfVxuICAgIFxuICAgIFxuICAgIGhhc1N1YnZpZXcodmlldzogVUlWaWV3KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnRhaW5lclZpZXcuaGFzU3Vidmlldyh2aWV3KVxuICAgIH1cbiAgICBcbiAgICBhZGRTdWJ2aWV3KHZpZXc6IFVJVmlldykge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jb250YWluZXJWaWV3LmFkZFN1YnZpZXcodmlldylcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaW52YWxpZGF0ZUludHJpbnNpY0NvbnRlbnRGcmFtZSgpXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG59XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiY2xhc3MgVUlTbGlkZVNjcm9sbGVyVmlldyBleHRlbmRzIFVJVmlldyB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgX3ByZXZpb3VzTGF5b3V0Qm91bmRzOiBVSVJlY3RhbmdsZVxuICAgIF90YXJnZXRJbmRleDogbnVtYmVyID0gMFxuICAgIHBhZ2VJbmRpY2F0b3JzVmlldzogVUlWaWV3XG4gICAgX2lzQW5pbWF0aW5nOiBib29sZWFuID0gTk9cbiAgICBfaXNBbmltYXRpb25PbmdvaW5nOiBib29sZWFuID0gTk9cbiAgICBfYW5pbWF0aW9uVGltZXI6IFVJVGltZXIgPSBuaWxcbiAgICBfc2Nyb2xsVmlldzogVUlTY3JvbGxWaWV3XG4gICAgX3NsaWRlVmlld3M6IFVJVmlld1tdID0gW11cbiAgICBcbiAgICB3cmFwQXJvdW5kOiBib29sZWFuID0gWUVTXG4gICAgXG4gICAgYW5pbWF0aW9uRHVyYXRpb246IG51bWJlciA9IDAuMzVcbiAgICBhbmltYXRpb25EZWxheTogbnVtYmVyID0gMlxuICAgIFxuICAgIF9jdXJyZW50UGFnZUluZGV4OiBudW1iZXIgPSAwXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEOiBzdHJpbmcsIHZpZXdIVE1MRWxlbWVudD86IEhUTUxFbGVtZW50KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQsIHZpZXdIVE1MRWxlbWVudClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlTY3JvbGxWaWV3XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJVmlld1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3Njcm9sbFZpZXcgPSBuZXcgVUlTY3JvbGxWaWV3KGVsZW1lbnRJRCArIFwiU2Nyb2xsVmlld1wiKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5hZGRTdWJ2aWV3KHRoaXMuX3Njcm9sbFZpZXcpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5fc2Nyb2xsVmlldy5fc2Nyb2xsRW5hYmxlZCA9IE5PXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zY3JvbGxWaWV3LmFkZFRhcmdldEZvckNvbnRyb2xFdmVudChcbiAgICAgICAgICAgIFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlck1vdmUsXG4gICAgICAgICAgICBmdW5jdGlvbiAoc2VuZGVyOiBVSVZpZXcsIGV2ZW50OiBFdmVudCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGlmIChldmVudCBpbnN0YW5jZW9mIE1vdXNlRXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fYW5pbWF0aW9uVGltZXIuaW52YWxpZGF0ZSgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpXG4gICAgICAgIClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3Njcm9sbFZpZXcuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlckxlYXZlLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc0FuaW1hdGluZyAmJiBldmVudCBpbnN0YW5jZW9mIE1vdXNlRXZlbnQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0QW5pbWF0aW5nKClcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgIFxuICAgICAgICAvLyBUb3VjaCBldmVudHNcbiAgICAgICAgdGhpcy5fc2Nyb2xsVmlldy5hZGRUYXJnZXRGb3JDb250cm9sRXZlbnQoVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyRG93biwgZnVuY3Rpb24gKHNlbmRlciwgZXZlbnQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgVG91Y2hFdmVudCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2FuaW1hdGlvblRpbWVyLmludmFsaWRhdGUoKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3Njcm9sbFZpZXcuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50cyhbXG4gICAgICAgICAgICBVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJVcCwgVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyQ2FuY2VsXG4gICAgICAgIF0sIGZ1bmN0aW9uIChzZW5kZXIsIGV2ZW50KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChldmVudCBpbnN0YW5jZW9mIFRvdWNoRXZlbnQgJiYgdGhpcy5faXNBbmltYXRpbmcpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0QW5pbWF0aW5nKClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgLy8gUGFnZSBpbmRpY2F0b3JcbiAgICAgICAgXG4gICAgICAgIHRoaXMucGFnZUluZGljYXRvcnNWaWV3ID0gbmV3IFVJVmlldyhlbGVtZW50SUQgKyBcIlBhZ2VJbmRpY2F0b3JzVmlld1wiKVxuICAgICAgICB0aGlzLmFkZFN1YnZpZXcodGhpcy5wYWdlSW5kaWNhdG9yc1ZpZXcpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgYnV0dG9uRm9yUGFnZUluZGljYXRvcldpdGhJbmRleChpbmRleDogbnVtYmVyKTogVUlCdXR0b24ge1xuICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSBuZXcgVUlCdXR0b24odGhpcy52aWV3SFRNTEVsZW1lbnQuaWQgKyBcIlBhZ2VJbmRpY2F0b3JCdXR0b25cIiArIGluZGV4KVxuICAgIFxuICAgICAgICByZXN1bHQuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50cyhbXG4gICAgICAgICAgICBVSVZpZXcuY29udHJvbEV2ZW50LlBvaW50ZXJVcEluc2lkZSwgVUlWaWV3LmNvbnRyb2xFdmVudC5FbnRlclVwXG4gICAgICAgIF0sIGZ1bmN0aW9uIChzZW5kZXIsIGV2ZW50KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsVG9QYWdlV2l0aEluZGV4KGluZGV4LCBZRVMpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc0FuaW1hdGluZykge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuc3RhcnRBbmltYXRpbmcoKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgIFxuICAgICAgICByZXN1bHQuYWRkVGFyZ2V0Rm9yQ29udHJvbEV2ZW50KFVJVmlldy5jb250cm9sRXZlbnQuUG9pbnRlck1vdmUsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5fYW5pbWF0aW9uVGltZXIuaW52YWxpZGF0ZSgpXG4gICAgICAgICAgICBcbiAgICAgICAgfS5iaW5kKHRoaXMpKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHJlc3VsdC51cGRhdGVDb250ZW50Rm9yTm9ybWFsU3RhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJlc3VsdC5iYWNrZ3JvdW5kQ29sb3IgPSBVSUNvbG9yLmJsdWVDb2xvclxuICAgICAgICAgICAgcmVzdWx0LnRpdGxlTGFiZWwudGV4dENvbG9yID0gVUlDb2xvci53aGl0ZUNvbG9yXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHJlc3VsdC5mcmFtZSA9IG5ldyBVSVJlY3RhbmdsZShuaWwsIG5pbCwgMjAsIDUwKVxuICAgICAgICBcbiAgICAgICAgLy8gcmVzdWx0LnN0eWxlLmhlaWdodCA9IFwiMjBweFwiO1xuICAgICAgICAvLyByZXN1bHQuc3R5bGUud2lkdGggPSBcIjUwcHhcIjtcbiAgICAgICAgcmVzdWx0LnN0eWxlLmRpc3BsYXkgPSBcInRhYmxlLWNlbGxcIlxuICAgICAgICByZXN1bHQuc3R5bGUucG9zaXRpb24gPSBcInJlbGF0aXZlXCJcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICAvLyB2YXIgcmVzdWx0Q29udGVudCA9IG5ldyBVSVZpZXcocmVzdWx0LnZpZXdIVE1MRWxlbWVudC5pZCArIFwiQ29udGVudFwiKTtcbiAgICAgICAgLy8gcmVzdWx0Q29udGVudC5iYWNrZ3JvdW5kQ29sb3IgPSBVSUNvbG9yLndoaXRlQ29sb3I7XG4gICAgICAgIC8vIHJlc3VsdENvbnRlbnQuY2VudGVyWUluQ29udGFpbmVyKCk7XG4gICAgICAgIC8vIHJlc3VsdENvbnRlbnQuc3R5bGUuaGVpZ2h0ID0gXCIxMHB4XCI7XG4gICAgICAgIC8vIHJlc3VsdENvbnRlbnQuc3R5bGUuaGVpZ2h0ID0gXCIxMDAlXCI7XG4gICAgICAgIC8vIHJlc3VsdENvbnRlbnQuc3R5bGUuYm9yZGVyUmFkaXVzID0gXCI1cHhcIjtcbiAgICAgICAgXG4gICAgICAgIC8vIHJlc3VsdC5hZGRTdWJ2aWV3KHJlc3VsdENvbnRlbnQpO1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGFkZFNsaWRlVmlldyh2aWV3OiBVSVZpZXcpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc2xpZGVWaWV3cy5wdXNoKHZpZXcpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnVwZGF0ZVNsaWRlVmlld3MoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IHNsaWRlVmlld3Modmlld3M6IFVJVmlld1tdKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zbGlkZVZpZXdzID0gdmlld3NcbiAgICAgICAgXG4gICAgICAgIHRoaXMudXBkYXRlU2xpZGVWaWV3cygpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBnZXQgc2xpZGVWaWV3cygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NsaWRlVmlld3NcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGN1cnJlbnRQYWdlSW5kZXgoKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuX2N1cnJlbnRQYWdlSW5kZXhcbiAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgc2V0IGN1cnJlbnRQYWdlSW5kZXgoaW5kZXg6IG51bWJlcikge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fY3VycmVudFBhZ2VJbmRleCA9IGluZGV4XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zbGlkZVZpZXdzW2luZGV4XS53aWxsQXBwZWFyKClcbiAgICAgICAgXG4gICAgICAgIC8vdGhpcy5fc2Nyb2xsVmlldy5jb250ZW50T2Zmc2V0LnggPSAtdGhpcy5fc2xpZGVWaWV3c1tpbmRleF0uZnJhbWUubWluLng7IC8vLXRoaXMuYm91bmRzLndpZHRoICogaW5kZXg7XG4gICAgICAgIC8vdGhpcy5fc2Nyb2xsVmlldy5jb250ZW50T2Zmc2V0LnggPSBNYXRoLnJvdW5kKHRoaXMuX3Njcm9sbFZpZXcuY29udGVudE9mZnNldC54KTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3Njcm9sbFZpZXcuY29udGVudE9mZnNldCA9IHRoaXMuX3Njcm9sbFZpZXcuY29udGVudE9mZnNldC5wb2ludFdpdGhYKC10aGlzLl9zbGlkZVZpZXdzW2luZGV4XS5mcmFtZS5taW4ueClcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLnBhZ2VJbmRpY2F0b3JzVmlldy5zdWJ2aWV3cy5mb3JFYWNoKGZ1bmN0aW9uIChidXR0b246IFVJQnV0dG9uLCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgYnV0dG9uLnNlbGVjdGVkID0gTk9cbiAgICAgICAgICAgIFxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgKHRoaXMucGFnZUluZGljYXRvcnNWaWV3LnN1YnZpZXdzW2luZGV4XSBhcyBVSUJ1dHRvbikuc2VsZWN0ZWQgPSBZRVNcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc2Nyb2xsVG9QcmV2aW91c1BhZ2UoYW5pbWF0ZWQ6IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLnNsaWRlVmlld3MubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgXG4gICAgICAgIHZhciB0YXJnZXRJbmRleCA9IHRoaXMuY3VycmVudFBhZ2VJbmRleFxuICAgIFxuICAgICAgICBpZiAodGhpcy53cmFwQXJvdW5kKSB7XG4gICAgICAgICAgICB0YXJnZXRJbmRleCA9ICh0aGlzLmN1cnJlbnRQYWdlSW5kZXggLSAxKSAlICh0aGlzLnNsaWRlVmlld3MubGVuZ3RoKVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuY3VycmVudFBhZ2VJbmRleCAtIDEgPCB0aGlzLnNsaWRlVmlld3MubGVuZ3RoKSB7XG4gICAgICAgICAgICB0YXJnZXRJbmRleCA9IHRoaXMuY3VycmVudFBhZ2VJbmRleCAtIDFcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB0aGlzLnNjcm9sbFRvUGFnZVdpdGhJbmRleCh0YXJnZXRJbmRleCwgYW5pbWF0ZWQpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBzY3JvbGxUb05leHRQYWdlKGFuaW1hdGVkOiBib29sZWFuKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5zbGlkZVZpZXdzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgIFxuICAgICAgICB2YXIgdGFyZ2V0SW5kZXggPSB0aGlzLmN1cnJlbnRQYWdlSW5kZXhcbiAgICBcbiAgICAgICAgaWYgKHRoaXMud3JhcEFyb3VuZCkge1xuICAgICAgICAgICAgdGFyZ2V0SW5kZXggPSAodGhpcy5jdXJyZW50UGFnZUluZGV4ICsgMSkgJSAodGhpcy5zbGlkZVZpZXdzLmxlbmd0aClcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmN1cnJlbnRQYWdlSW5kZXggKyAxIDwgdGhpcy5zbGlkZVZpZXdzLmxlbmd0aCkge1xuICAgICAgICAgICAgdGFyZ2V0SW5kZXggPSB0aGlzLmN1cnJlbnRQYWdlSW5kZXggKyAxXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5zY3JvbGxUb1BhZ2VXaXRoSW5kZXgodGFyZ2V0SW5kZXgsIGFuaW1hdGVkKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgc2Nyb2xsVG9QYWdlV2l0aEluZGV4KHRhcmdldEluZGV4OiBudW1iZXIsIGFuaW1hdGVkOiBib29sZWFuID0gWUVTKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl90YXJnZXRJbmRleCA9IHRhcmdldEluZGV4XG4gICAgICAgIFxuICAgICAgICAvLyB0aGlzLl9zbGlkZVZpZXdzW3RoaXMuY3VycmVudFBhZ2VJbmRleF0uX3Nob3VsZExheW91dCA9IE5PO1xuICAgICAgICAvLyB0aGlzLl9zbGlkZVZpZXdzW3RoaXMuX3RhcmdldEluZGV4XS5fc2hvdWxkTGF5b3V0ID0gWUVTO1xuICAgICAgICBcbiAgICAgICAgLy90aGlzLl9zbGlkZVZpZXdzW3RoaXMuX3RhcmdldEluZGV4XS5oaWRkZW4gPSBOTztcbiAgICAgICAgXG4gICAgICAgIHRoaXMud2lsbFNjcm9sbFRvUGFnZVdpdGhJbmRleCh0YXJnZXRJbmRleClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzQW5pbWF0aW9uT25nb2luZyA9IFlFU1xuICAgICAgICBcbiAgICAgICAgLy92YXIgcHJldmlvdXNWaWV3ID0gdGhpcy5fc2xpZGVWaWV3c1t0aGlzLmN1cnJlbnRQYWdlSW5kZXhdO1xuICAgICAgICBcbiAgICAgICAgaWYgKGFuaW1hdGVkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBVSVZpZXcuYW5pbWF0ZVZpZXdPclZpZXdzV2l0aER1cmF0aW9uRGVsYXlBbmRGdW5jdGlvbihcbiAgICAgICAgICAgICAgICB0aGlzLl9zY3JvbGxWaWV3LmNvbnRhaW5lclZpZXcsXG4gICAgICAgICAgICAgICAgdGhpcy5hbmltYXRpb25EdXJhdGlvbixcbiAgICAgICAgICAgICAgICAwLFxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAodGhpczogVUlTbGlkZVNjcm9sbGVyVmlldykge1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2VJbmRleCA9IHRhcmdldEluZGV4XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uICh0aGlzOiBVSVNsaWRlU2Nyb2xsZXJWaWV3KSB7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpZFNjcm9sbFRvUGFnZVdpdGhJbmRleCh0YXJnZXRJbmRleClcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2lzQW5pbWF0aW9uT25nb2luZyA9IE5PXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAvL3ByZXZpb3VzVmlldy5oaWRkZW4gPSBZRVM7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH0uYmluZCh0aGlzKVxuICAgICAgICAgICAgKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZUluZGV4ID0gdGFyZ2V0SW5kZXhcbiAgICAgICAgICAgIHRoaXMuZGlkU2Nyb2xsVG9QYWdlV2l0aEluZGV4KHRhcmdldEluZGV4KVxuICAgICAgICAgICAgXG4gICAgICAgICAgICAvL3ByZXZpb3VzVmlldy5oaWRkZW4gPSBZRVM7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgd2lsbFNjcm9sbFRvUGFnZVdpdGhJbmRleChpbmRleDogbnVtYmVyKSB7XG4gICAgXG4gICAgICAgIGNvbnN0IHRhcmdldFZpZXcgPSB0aGlzLnNsaWRlVmlld3NbaW5kZXhdXG4gICAgXG4gICAgICAgIGlmIChJUyh0YXJnZXRWaWV3KSAmJiAodGFyZ2V0VmlldyBhcyBhbnkpLndpbGxBcHBlYXIgJiYgKHRhcmdldFZpZXcgYXMgYW55KS53aWxsQXBwZWFyIGluc3RhbmNlb2YgRnVuY3Rpb24pIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgKHRhcmdldFZpZXcgYXMgYW55KS53aWxsQXBwZWFyKClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBkaWRTY3JvbGxUb1BhZ2VXaXRoSW5kZXgoaW5kZXg6IG51bWJlcikge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGFydEFuaW1hdGluZygpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzQW5pbWF0aW5nID0gWUVTXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9hbmltYXRpb25UaW1lci5pbnZhbGlkYXRlKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2FuaW1hdGlvblRpbWVyID0gbmV3IFVJVGltZXIodGhpcy5hbmltYXRpb25EZWxheSArIHRoaXMuYW5pbWF0aW9uRHVyYXRpb24sIFlFUywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnNjcm9sbFRvTmV4dFBhZ2UoWUVTKVxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHN0b3BBbmltYXRpbmcoKSB7XG4gICAgICAgIFxuICAgICAgICB0aGlzLl9pc0FuaW1hdGluZyA9IE5PXG4gICAgICAgIHRoaXMuX2FuaW1hdGlvblRpbWVyLmludmFsaWRhdGUoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgdXBkYXRlU2xpZGVWaWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3Njcm9sbFZpZXcuY29udGFpbmVyVmlldy5zdWJ2aWV3cy5zbGljZSgpLmZvckVhY2goZnVuY3Rpb24gKHN1YnZpZXcsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBzdWJ2aWV3LnJlbW92ZUZyb21TdXBlcnZpZXcoKVxuICAgICAgICAgICAgXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICB0aGlzLnBhZ2VJbmRpY2F0b3JzVmlldy5zdWJ2aWV3cy5zbGljZSgpLmZvckVhY2goZnVuY3Rpb24gKHN1YnZpZXcsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBzdWJ2aWV3LnJlbW92ZUZyb21TdXBlcnZpZXcoKVxuICAgICAgICAgICAgXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zbGlkZVZpZXdzLmZvckVhY2goZnVuY3Rpb24gKHZpZXcsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9zY3JvbGxWaWV3LmFkZFN1YnZpZXcodmlldylcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5wYWdlSW5kaWNhdG9yc1ZpZXcuYWRkU3Vidmlldyh0aGlzLmJ1dHRvbkZvclBhZ2VJbmRpY2F0b3JXaXRoSW5kZXgoaW5kZXgpKVxuICAgICAgICAgICAgXG4gICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGRpZFJlY2VpdmVCcm9hZGNhc3RFdmVudChldmVudDogVUlWaWV3QnJvYWRjYXN0RXZlbnQpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmRpZFJlY2VpdmVCcm9hZGNhc3RFdmVudChldmVudClcbiAgICAgICAgXG4gICAgICAgIGlmIChldmVudC5uYW1lID09IFVJQ29yZS5icm9hZGNhc3RFdmVudE5hbWUuV2luZG93RGlkUmVzaXplKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2VJbmRleCA9IHRoaXMuY3VycmVudFBhZ2VJbmRleFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHNldCBmcmFtZShmcmFtZTogVUlSZWN0YW5nbGUpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmZyYW1lID0gZnJhbWVcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY3VycmVudFBhZ2VJbmRleCA9IHRoaXMuY3VycmVudFBhZ2VJbmRleFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgZ2V0IGZyYW1lKCkge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHN1cGVyLmZyYW1lXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBsYXlvdXRTdWJ2aWV3cygpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmxheW91dFN1YnZpZXdzKClcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLmJvdW5kcy5pc0VxdWFsVG8odGhpcy5fcHJldmlvdXNMYXlvdXRCb3VuZHMpKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgIFxuICAgICAgICBjb25zdCBib3VuZHMgPSB0aGlzLmJvdW5kc1xuICAgIFxuICAgICAgICB0aGlzLl9wcmV2aW91c0xheW91dEJvdW5kcyA9IGJvdW5kc1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fc2Nyb2xsVmlldy5mcmFtZSA9IGJvdW5kc1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zY3JvbGxWaWV3LmNvbnRhaW5lclZpZXcuZnJhbWUgPSBib3VuZHMucmVjdGFuZ2xlV2l0aFdpZHRoKGJvdW5kcy53aWR0aCAqXG4gICAgICAgICAgICB0aGlzLnNsaWRlVmlld3MubGVuZ3RoKS5wZXJmb3JtRnVuY3Rpb25XaXRoU2VsZihmdW5jdGlvbiAodGhpczogVUlTbGlkZVNjcm9sbGVyVmlldywgc2VsZjogVUlSZWN0YW5nbGUpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgc2VsZi5vZmZzZXRCeVBvaW50KHRoaXMuX3Njcm9sbFZpZXcuY29udGVudE9mZnNldClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuIHNlbGZcbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9zbGlkZVZpZXdzLmZvckVhY2goZnVuY3Rpb24gKHZpZXcsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB2aWV3LmZyYW1lID0gYm91bmRzLnJlY3RhbmdsZVdpdGhYKCh0aGlzLmJvdW5kcy53aWR0aCArIDEpICogaW5kZXgpXG4gICAgICAgICAgICBcbiAgICAgICAgfS5iaW5kKHRoaXMpKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMubGF5b3V0UGFnZUluZGljYXRvcnMoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIGxheW91dFBhZ2VJbmRpY2F0b3JzKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5wYWdlSW5kaWNhdG9yc1ZpZXcuY2VudGVyWEluQ29udGFpbmVyKClcbiAgICAgICAgdGhpcy5wYWdlSW5kaWNhdG9yc1ZpZXcuc3R5bGUuYm90dG9tID0gXCIyMHB4XCJcbiAgICAgICAgdGhpcy5wYWdlSW5kaWNhdG9yc1ZpZXcuc3R5bGUuaGVpZ2h0ID0gXCIyMHB4XCJcbiAgICAgICAgdGhpcy5wYWdlSW5kaWNhdG9yc1ZpZXcuc3R5bGUuZGlzcGxheSA9IFwidGFibGUtcm93XCJcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIHJlbW92ZUZyb21TdXBlcnZpZXcoKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgc3VwZXIucmVtb3ZlRnJvbVN1cGVydmlldygpXG4gICAgICAgIFxuICAgICAgICB0aGlzLnN0b3BBbmltYXRpbmcoKVxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCJjbGFzcyBVSVN0cmluZ0ZpbHRlciBleHRlbmRzIFVJT2JqZWN0IHtcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgX3NoYXJlZFdlYldvcmtlciA9IG5ldyBXb3JrZXIoXCJjb21waWxlZFNjcmlwdHMvL1VJU3RyaW5nRmlsdGVyV2ViV29ya2VyLmpzXCIpXG4gICAgXG4gICAgc3RhdGljIF9pbnN0YW5jZU51bWJlciA9IC0xXG4gICAgXG4gICAgXG4gICAgX2luc3RhbmNlTnVtYmVyOiBudW1iZXJcbiAgICBcbiAgICBfaXNUaHJlYWRDbG9zZWQgPSBOT1xuICAgIFxuICAgIF93ZWJXb3JrZXIgPSBVSVN0cmluZ0ZpbHRlci5fc2hhcmVkV2ViV29ya2VyXG4gICAgXG4gICAgY29uc3RydWN0b3IodXNlU2VwYXJhdGVXZWJXb3JrZXIgPSBOTykge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2NsYXNzID0gVUlTdHJpbmdGaWx0ZXJcbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlPYmplY3RcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAodXNlU2VwYXJhdGVXZWJXb3JrZXIpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5fd2ViV29ya2VyID0gbmV3IFdvcmtlcihcImNvbXBpbGVkU2NyaXB0cy8vVUlTdHJpbmdGaWx0ZXJXZWJXb3JrZXIuanNcIilcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBVSVN0cmluZ0ZpbHRlci5faW5zdGFuY2VOdW1iZXIgPSBVSVN0cmluZ0ZpbHRlci5faW5zdGFuY2VOdW1iZXIgKyAxXG4gICAgICAgIHRoaXMuX2luc3RhbmNlTnVtYmVyID0gVUlTdHJpbmdGaWx0ZXIuX2luc3RhbmNlTnVtYmVyXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZ2V0IGluc3RhbmNlSWRlbnRpZmllcigpIHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiB0aGlzLl9pbnN0YW5jZU51bWJlclxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZmlsdGVyRGF0YShcbiAgICAgICAgZmlsdGVyaW5nU3RyaW5nOiBzdHJpbmcsXG4gICAgICAgIGRhdGE6IHN0cmluZ1tdLFxuICAgICAgICBleGNsdWRlZERhdGE6IHN0cmluZ1tdLFxuICAgICAgICBpZGVudGlmaWVyOiBhbnksXG4gICAgICAgIGNvbXBsZXRpb246IChmaWx0ZXJlZERhdGE6IHN0cmluZ1tdLCBmaWx0ZXJlZEluZGV4ZXM6IG51bWJlcltdLCBpZGVudGlmaWVyOiBhbnkpID0+IHZvaWRcbiAgICApIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5faXNUaHJlYWRDbG9zZWQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgLy92YXIgc3RhcnRUaW1lID0gRGF0ZS5ub3coKTtcbiAgICBcbiAgICAgICAgY29uc3QgaW5zdGFuY2VJZGVudGlmaWVyID0gdGhpcy5pbnN0YW5jZUlkZW50aWZpZXJcbiAgICBcbiAgICAgICAgdGhpcy5fd2ViV29ya2VyLm9ubWVzc2FnZSA9IGZ1bmN0aW9uIChtZXNzYWdlKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmRhdGEuaW5zdGFuY2VJZGVudGlmaWVyID09IGluc3RhbmNlSWRlbnRpZmllcikge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJGaWx0ZXJpbmcgdG9vayBcIiArIChEYXRlLm5vdygpIC0gc3RhcnRUaW1lKSArIFwiIG1zXCIpO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGNvbXBsZXRpb24obWVzc2FnZS5kYXRhLmZpbHRlcmVkRGF0YSwgbWVzc2FnZS5kYXRhLmZpbHRlcmVkSW5kZXhlcywgbWVzc2FnZS5kYXRhLmlkZW50aWZpZXIpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fd2ViV29ya2VyLnBvc3RNZXNzYWdlKHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXCJmaWx0ZXJpbmdTdHJpbmdcIjogZmlsdGVyaW5nU3RyaW5nLFxuICAgICAgICAgICAgXCJkYXRhXCI6IGRhdGEsXG4gICAgICAgICAgICBcImV4Y2x1ZGVkRGF0YVwiOiBleGNsdWRlZERhdGEsXG4gICAgICAgICAgICBcImlkZW50aWZpZXJcIjogaWRlbnRpZmllcixcbiAgICAgICAgICAgIFwiaW5zdGFuY2VJZGVudGlmaWVyXCI6IGluc3RhbmNlSWRlbnRpZmllclxuICAgICAgICAgICAgXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgZmlsdGVyZWREYXRhKFxuICAgICAgICBmaWx0ZXJpbmdTdHJpbmc6IHN0cmluZyxcbiAgICAgICAgZGF0YTogc3RyaW5nW10sXG4gICAgICAgIGV4Y2x1ZGVkRGF0YTogc3RyaW5nW10gPSBbXSxcbiAgICAgICAgaWRlbnRpZmllcjogYW55ID0gTUFLRV9JRCgpXG4gICAgKSB7XG4gICAgXG4gICAgXG4gICAgICAgIGNvbnN0IHJlc3VsdDogUHJvbWlzZTx7XG4gICAgICAgIFxuICAgICAgICAgICAgZmlsdGVyZWREYXRhOiBzdHJpbmdbXSxcbiAgICAgICAgICAgIGZpbHRlcmVkSW5kZXhlczogbnVtYmVyW10sXG4gICAgICAgICAgICBpZGVudGlmaWVyOiBhbnlcbiAgICAgICAgXG4gICAgICAgIH0+ID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuZmlsdGVyRGF0YShmaWx0ZXJpbmdTdHJpbmcsIGRhdGEsIGV4Y2x1ZGVkRGF0YSwgaWRlbnRpZmllcixcbiAgICAgICAgICAgICAgICAoZmlsdGVyZWREYXRhLCBmaWx0ZXJlZEluZGV4ZXMsIGZpbHRlcmVkSWRlbnRpZmllcikgPT4ge1xuICAgIFxuICAgICAgICAgICAgICAgICAgICBpZiAoZmlsdGVyZWRJZGVudGlmaWVyID09IGlkZW50aWZpZXIpIHtcbiAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoe1xuICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWx0ZXJlZERhdGE6IGZpbHRlcmVkRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWx0ZXJlZEluZGV4ZXM6IGZpbHRlcmVkSW5kZXhlcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZGVudGlmaWVyOiBmaWx0ZXJlZElkZW50aWZpZXJcbiAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgfSlcbiAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGNsb3NlVGhyZWFkKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5faXNUaHJlYWRDbG9zZWQgPSBZRVNcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl93ZWJXb3JrZXIgIT0gVUlTdHJpbmdGaWx0ZXIuX3NoYXJlZFdlYldvcmtlcikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl93ZWJXb3JrZXIudGVybWluYXRlKClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi9VSU5hdGl2ZVNjcm9sbFZpZXcudHNcIiAvPlxuXG5cblxuaW50ZXJmYWNlIFVJVGFibGVWaWV3UmV1c2FibGVWaWV3c0NvbnRhaW5lck9iamVjdCB7XG4gICAgXG4gICAgW2tleTogc3RyaW5nXTogVUlWaWV3W107XG4gICAgXG59XG5cblxuaW50ZXJmYWNlIFVJVGFibGVWaWV3UmV1c2FibGVWaWV3UG9zaXRpb25PYmplY3Qge1xuICAgIFxuICAgIGJvdHRvbVk6IG51bWJlcjtcbiAgICB0b3BZOiBudW1iZXI7XG4gICAgXG4gICAgaXNWYWxpZDogYm9vbGVhbjtcbiAgICBcbn1cblxuXG5cblxuXG5jbGFzcyBVSVRhYmxlVmlldyBleHRlbmRzIFVJTmF0aXZlU2Nyb2xsVmlldyB7XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgYWxsUm93c0hhdmVFcXVhbEhlaWdodDogYm9vbGVhbiA9IE5PXG4gICAgX3Zpc2libGVSb3dzOiBVSVZpZXdbXSA9IFtdXG4gICAgX2ZpcnN0TGF5b3V0VmlzaWJsZVJvd3M6IFVJVmlld1tdID0gW11cbiAgICBcbiAgICBfcm93UG9zaXRpb25zOiBVSVRhYmxlVmlld1JldXNhYmxlVmlld1Bvc2l0aW9uT2JqZWN0W10gPSBbXVxuICAgIFxuICAgIF9oaWdoZXN0VmFsaWRSb3dQb3NpdGlvbkluZGV4OiBudW1iZXIgPSAwXG4gICAgXG4gICAgX3JldXNhYmxlVmlld3M6IFVJVGFibGVWaWV3UmV1c2FibGVWaWV3c0NvbnRhaW5lck9iamVjdCA9IHt9XG4gICAgXG4gICAgX3JlbW92ZWRSZXVzYWJsZVZpZXdzOiBVSVRhYmxlVmlld1JldXNhYmxlVmlld3NDb250YWluZXJPYmplY3QgPSB7fVxuICAgIFxuICAgIF9mdWxsSGVpZ2h0VmlldzogVUlWaWV3XG4gICAgXG4gICAgX3Jvd0lESW5kZXg6IG51bWJlciA9IDBcbiAgICBcbiAgICByZWxvYWRzT25MYW5ndWFnZUNoYW5nZSA9IFlFU1xuICAgIFxuICAgIHNpZGVQYWRkaW5nID0gMFxuICAgIFxuICAgIF9wZXJzaXN0ZWREYXRhOiBhbnlbXSA9IFtdXG4gICAgX25lZWRzRHJhd2luZ09mVmlzaWJsZVJvd3NCZWZvcmVMYXlvdXQgPSBOT1xuICAgIF9pc0RyYXdWaXNpYmxlUm93c1NjaGVkdWxlZCA9IE5PXG4gICAgX3Nob3VsZEFuaW1hdGVOZXh0TGF5b3V0OiBib29sZWFuXG4gICAgXG4gICAgYW5pbWF0aW9uRHVyYXRpb24gPSAwLjI1XG4gICAgXG4gICAgXG4gICAgY29uc3RydWN0b3IoZWxlbWVudElEKSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlcihlbGVtZW50SUQpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IFVJVGFibGVWaWV3XG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJTmF0aXZlU2Nyb2xsVmlld1xuICAgICAgICBcbiAgICAgICAgdGhpcy5zY3JvbGxzWCA9IE5PXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBpbml0VmlldyhlbGVtZW50SUQsIHZpZXdIVE1MRWxlbWVudCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuaW5pdFZpZXcoZWxlbWVudElELCB2aWV3SFRNTEVsZW1lbnQpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9mdWxsSGVpZ2h0VmlldyA9IG5ldyBVSVZpZXcoKVxuICAgICAgICB0aGlzLl9mdWxsSGVpZ2h0Vmlldy5oaWRkZW4gPSBZRVNcbiAgICAgICAgdGhpcy5fZnVsbEhlaWdodFZpZXcudXNlckludGVyYWN0aW9uRW5hYmxlZCA9IE5PXG4gICAgICAgIHRoaXMuYWRkU3Vidmlldyh0aGlzLl9mdWxsSGVpZ2h0VmlldylcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGxvYWREYXRhKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fcGVyc2lzdGVkRGF0YSA9IFtdXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jYWxjdWxhdGVQb3NpdGlvbnNVbnRpbEluZGV4KHRoaXMubnVtYmVyT2ZSb3dzKCkgLSAxKVxuICAgICAgICB0aGlzLl9uZWVkc0RyYXdpbmdPZlZpc2libGVSb3dzQmVmb3JlTGF5b3V0ID0gWUVTXG4gICAgICAgIFxuICAgICAgICB0aGlzLnNldE5lZWRzTGF5b3V0KClcbiAgICAgICAgXG4gICAgICAgIC8vIHRoaXMuZm9yRWFjaFZpZXdJblN1YnRyZWUoZnVuY3Rpb24odmlldykge1xuICAgICAgICBcbiAgICAgICAgLy8gICAgIHZpZXcuc2V0TmVlZHNMYXlvdXQoKTtcbiAgICAgICAgXG4gICAgICAgIC8vIH0pXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICByZWxvYWREYXRhKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fcmVtb3ZlVmlzaWJsZVJvd3MoKVxuICAgICAgICB0aGlzLl9yZW1vdmVBbGxSZXVzYWJsZVJvd3MoKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fcm93UG9zaXRpb25zID0gW11cbiAgICAgICAgdGhpcy5faGlnaGVzdFZhbGlkUm93UG9zaXRpb25JbmRleCA9IDBcbiAgICAgICAgXG4gICAgICAgIHRoaXMubG9hZERhdGEoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgaGlnaGxpZ2h0Q2hhbmdlcyhwcmV2aW91c0RhdGE6IGFueVtdLCBuZXdEYXRhOiBhbnlbXSkge1xuICAgICAgICBcbiAgICAgICAgcHJldmlvdXNEYXRhID0gcHJldmlvdXNEYXRhLm1hcChmdW5jdGlvbiAoZGF0YVBvaW50LCBpbmRleCwgYXJyYXkpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGRhdGFQb2ludClcbiAgICAgICAgICAgIFxuICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgbmV3RGF0YSA9IG5ld0RhdGEubWFwKGZ1bmN0aW9uIChkYXRhUG9pbnQsIGluZGV4LCBhcnJheSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkoZGF0YVBvaW50KVxuICAgICAgICAgICAgXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgY29uc3QgbmV3SW5kZXhlczogbnVtYmVyW10gPSBbXVxuICAgICAgICBcbiAgICAgICAgbmV3RGF0YS5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSwgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICghcHJldmlvdXNEYXRhLmNvbnRhaW5zKHZhbHVlKSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIG5ld0luZGV4ZXMucHVzaChpbmRleClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9KVxuICAgICAgICBcbiAgICAgICAgbmV3SW5kZXhlcy5mb3JFYWNoKGZ1bmN0aW9uICh0aGlzOiBVSVRhYmxlVmlldywgaW5kZXgpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKHRoaXMuaXNSb3dXaXRoSW5kZXhWaXNpYmxlKGluZGV4KSkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuaGlnaGxpZ2h0Um93QXNOZXcodGhpcy52aWV3Rm9yUm93V2l0aEluZGV4KGluZGV4KSlcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBoaWdobGlnaHRSb3dBc05ldyhyb3c6IFVJVmlldykge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBpbnZhbGlkYXRlU2l6ZU9mUm93V2l0aEluZGV4KGluZGV4OiBudW1iZXIsIGFuaW1hdGVDaGFuZ2UgPSBOTykge1xuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX3Jvd1Bvc2l0aW9uc1tpbmRleF0pIHtcbiAgICAgICAgICAgIHRoaXMuX3Jvd1Bvc2l0aW9uc1tpbmRleF0uaXNWYWxpZCA9IE5PXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2hpZ2hlc3RWYWxpZFJvd1Bvc2l0aW9uSW5kZXggPSBNYXRoLm1pbih0aGlzLl9oaWdoZXN0VmFsaWRSb3dQb3NpdGlvbkluZGV4LCBpbmRleCAtIDEpXG4gICAgICAgIFxuICAgICAgICAvLyBpZiAoaW5kZXggPT0gMCkge1xuICAgICAgICBcbiAgICAgICAgLy8gICAgIHRoaXMuX2hpZ2hlc3RWYWxpZFJvd1Bvc2l0aW9uSW5kZXggPSAwO1xuICAgICAgICBcbiAgICAgICAgLy8gICAgIHRoaXMuX3Jvd1Bvc2l0aW9ucyA9IFtdO1xuICAgICAgICBcbiAgICAgICAgLy8gfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fbmVlZHNEcmF3aW5nT2ZWaXNpYmxlUm93c0JlZm9yZUxheW91dCA9IFlFU1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fc2hvdWxkQW5pbWF0ZU5leHRMYXlvdXQgPSBhbmltYXRlQ2hhbmdlXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBfY2FsY3VsYXRlQWxsUG9zaXRpb25zKCkge1xuICAgICAgICB0aGlzLl9jYWxjdWxhdGVQb3NpdGlvbnNVbnRpbEluZGV4KHRoaXMubnVtYmVyT2ZSb3dzKCkgLSAxKVxuICAgIH1cbiAgICBcbiAgICBfY2FsY3VsYXRlUG9zaXRpb25zVW50aWxJbmRleChtYXhJbmRleDogbnVtYmVyKSB7XG4gICAgICAgIFxuICAgICAgICB2YXIgdmFsaWRQb3NpdGlvbk9iamVjdCA9IHRoaXMuX3Jvd1Bvc2l0aW9uc1t0aGlzLl9oaWdoZXN0VmFsaWRSb3dQb3NpdGlvbkluZGV4XVxuICAgICAgICBpZiAoIUlTKHZhbGlkUG9zaXRpb25PYmplY3QpKSB7XG4gICAgICAgICAgICB2YWxpZFBvc2l0aW9uT2JqZWN0ID0ge1xuICAgICAgICAgICAgICAgIGJvdHRvbVk6IDAsXG4gICAgICAgICAgICAgICAgdG9wWTogMCxcbiAgICAgICAgICAgICAgICBpc1ZhbGlkOiBZRVNcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdmFyIHByZXZpb3VzQm90dG9tWSA9IHZhbGlkUG9zaXRpb25PYmplY3QuYm90dG9tWVxuICAgICAgICBcbiAgICAgICAgaWYgKCF0aGlzLl9yb3dQb3NpdGlvbnMubGVuZ3RoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX2hpZ2hlc3RWYWxpZFJvd1Bvc2l0aW9uSW5kZXggPSAtMVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGZvciAodmFyIGkgPSB0aGlzLl9oaWdoZXN0VmFsaWRSb3dQb3NpdGlvbkluZGV4ICsgMTsgaSA8PSBtYXhJbmRleDsgaSsrKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciBoZWlnaHQ6IG51bWJlclxuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCByb3dQb3NpdGlvbk9iamVjdCA9IHRoaXMuX3Jvd1Bvc2l0aW9uc1tpXVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoSVMoKHJvd1Bvc2l0aW9uT2JqZWN0IHx8IG5pbCkuaXNWYWxpZCkpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBoZWlnaHQgPSByb3dQb3NpdGlvbk9iamVjdC5ib3R0b21ZIC0gcm93UG9zaXRpb25PYmplY3QudG9wWVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaGVpZ2h0ID0gdGhpcy5oZWlnaHRGb3JSb3dXaXRoSW5kZXgoaSlcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29uc3QgcG9zaXRpb25PYmplY3Q6IFVJVGFibGVWaWV3UmV1c2FibGVWaWV3UG9zaXRpb25PYmplY3QgPSB7XG4gICAgICAgICAgICAgICAgYm90dG9tWTogcHJldmlvdXNCb3R0b21ZICsgaGVpZ2h0LFxuICAgICAgICAgICAgICAgIHRvcFk6IHByZXZpb3VzQm90dG9tWSxcbiAgICAgICAgICAgICAgICBpc1ZhbGlkOiBZRVNcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGkgPCB0aGlzLl9yb3dQb3NpdGlvbnMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fcm93UG9zaXRpb25zW2ldID0gcG9zaXRpb25PYmplY3RcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuX3Jvd1Bvc2l0aW9ucy5wdXNoKHBvc2l0aW9uT2JqZWN0KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5faGlnaGVzdFZhbGlkUm93UG9zaXRpb25JbmRleCA9IGlcbiAgICAgICAgICAgIHByZXZpb3VzQm90dG9tWSA9IHByZXZpb3VzQm90dG9tWSArIGhlaWdodFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIGluZGV4ZXNGb3JWaXNpYmxlUm93cyhwYWRkaW5nUmF0aW8gPSAwLjUpOiBudW1iZXJbXSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBmaXJzdFZpc2libGVZID0gdGhpcy5jb250ZW50T2Zmc2V0LnkgLSB0aGlzLmJvdW5kcy5oZWlnaHQgKiBwYWRkaW5nUmF0aW9cbiAgICAgICAgY29uc3QgbGFzdFZpc2libGVZID0gZmlyc3RWaXNpYmxlWSArIHRoaXMuYm91bmRzLmhlaWdodCAqICgxICsgcGFkZGluZ1JhdGlvKVxuICAgICAgICBcbiAgICAgICAgY29uc3QgbnVtYmVyT2ZSb3dzID0gdGhpcy5udW1iZXJPZlJvd3MoKVxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuYWxsUm93c0hhdmVFcXVhbEhlaWdodCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCByb3dIZWlnaHQgPSB0aGlzLmhlaWdodEZvclJvd1dpdGhJbmRleCgwKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB2YXIgZmlyc3RJbmRleCA9IGZpcnN0VmlzaWJsZVkgLyByb3dIZWlnaHRcbiAgICAgICAgICAgIHZhciBsYXN0SW5kZXggPSBsYXN0VmlzaWJsZVkgLyByb3dIZWlnaHRcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZmlyc3RJbmRleCA9IE1hdGgudHJ1bmMoZmlyc3RJbmRleClcbiAgICAgICAgICAgIGxhc3RJbmRleCA9IE1hdGgudHJ1bmMobGFzdEluZGV4KSArIDFcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZmlyc3RJbmRleCA9IE1hdGgubWF4KGZpcnN0SW5kZXgsIDApXG4gICAgICAgICAgICBsYXN0SW5kZXggPSBNYXRoLm1pbihsYXN0SW5kZXgsIG51bWJlck9mUm93cyAtIDEpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciByZXN1bHQgPSBbXVxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IGZpcnN0SW5kZXg7IGkgPCBsYXN0SW5kZXggKyAxOyBpKyspIHtcbiAgICAgICAgICAgICAgICByZXN1bHQucHVzaChpKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICB2YXIgYWNjdW11bGF0ZWRIZWlnaHQgPSAwXG4gICAgICAgIHZhciByZXN1bHQgPSBbXVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2FsY3VsYXRlQWxsUG9zaXRpb25zKClcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJvd1Bvc2l0aW9ucyA9IHRoaXMuX3Jvd1Bvc2l0aW9uc1xuICAgICAgICBcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBudW1iZXJPZlJvd3M7IGkrKykge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBoZWlnaHQgPSByb3dQb3NpdGlvbnNbaV0uYm90dG9tWSAtIHJvd1Bvc2l0aW9uc1tpXS50b3BZICAvLyB0aGlzLmhlaWdodEZvclJvd1dpdGhJbmRleChpKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBhY2N1bXVsYXRlZEhlaWdodCA9IGFjY3VtdWxhdGVkSGVpZ2h0ICsgaGVpZ2h0XG4gICAgICAgICAgICBpZiAoYWNjdW11bGF0ZWRIZWlnaHQgPj0gZmlyc3RWaXNpYmxlWSkge1xuICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKGkpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoYWNjdW11bGF0ZWRIZWlnaHQgPj0gbGFzdFZpc2libGVZKSB7XG4gICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBfcmVtb3ZlVmlzaWJsZVJvd3MoKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCB2aXNpYmxlUm93cyA9IFtdXG4gICAgICAgIHRoaXMuX3Zpc2libGVSb3dzLmZvckVhY2goZnVuY3Rpb24gKHRoaXM6IFVJVGFibGVWaWV3LCByb3c6IFVJVmlldywgaW5kZXg6IG51bWJlciwgYXJyYXk6IFVJVmlld1tdKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX3BlcnNpc3RlZERhdGFbcm93Ll9VSVRhYmxlVmlld1Jvd0luZGV4XSA9IHRoaXMucGVyc2lzdGVuY2VEYXRhSXRlbUZvclJvd1dpdGhJbmRleChcbiAgICAgICAgICAgICAgICByb3cuX1VJVGFibGVWaWV3Um93SW5kZXgsXG4gICAgICAgICAgICAgICAgcm93XG4gICAgICAgICAgICApXG4gICAgICAgICAgICByb3cucmVtb3ZlRnJvbVN1cGVydmlldygpXG4gICAgICAgICAgICB0aGlzLl9yZW1vdmVkUmV1c2FibGVWaWV3c1tyb3cuX1VJVGFibGVWaWV3UmV1c2FiaWxpdHlJZGVudGlmaWVyXS5wdXNoKHJvdylcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgIH0sIHRoaXMpXG4gICAgICAgIHRoaXMuX3Zpc2libGVSb3dzID0gdmlzaWJsZVJvd3NcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIF9yZW1vdmVBbGxSZXVzYWJsZVJvd3MoKSB7XG4gICAgICAgIHRoaXMuX3JldXNhYmxlVmlld3MuZm9yRWFjaChmdW5jdGlvbiAodGhpczogVUlUYWJsZVZpZXcsIHJvd3M6IFVJVmlld1tdKSB7XG4gICAgICAgICAgICByb3dzLmZvckVhY2goZnVuY3Rpb24gKHRoaXM6IFVJVGFibGVWaWV3LCByb3c6IFVJVmlldywgaW5kZXg6IG51bWJlciwgYXJyYXk6IFVJVmlld1tdKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5fcGVyc2lzdGVkRGF0YVtyb3cuX1VJVGFibGVWaWV3Um93SW5kZXhdID0gdGhpcy5wZXJzaXN0ZW5jZURhdGFJdGVtRm9yUm93V2l0aEluZGV4KFxuICAgICAgICAgICAgICAgICAgICByb3cuX1VJVGFibGVWaWV3Um93SW5kZXgsXG4gICAgICAgICAgICAgICAgICAgIHJvd1xuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICByb3cucmVtb3ZlRnJvbVN1cGVydmlldygpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5fbWFya1JldXNhYmxlVmlld0FzVW51c2VkKHJvdylcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSlcbiAgICAgICAgfS5iaW5kKHRoaXMpKVxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBfbWFya1JldXNhYmxlVmlld0FzVW51c2VkKHJvdzogVUlWaWV3KSB7XG4gICAgICAgIGlmICghdGhpcy5fcmVtb3ZlZFJldXNhYmxlVmlld3Nbcm93Ll9VSVRhYmxlVmlld1JldXNhYmlsaXR5SWRlbnRpZmllcl0uY29udGFpbnMocm93KSkge1xuICAgICAgICAgICAgdGhpcy5fcmVtb3ZlZFJldXNhYmxlVmlld3Nbcm93Ll9VSVRhYmxlVmlld1JldXNhYmlsaXR5SWRlbnRpZmllcl0ucHVzaChyb3cpXG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgX2RyYXdWaXNpYmxlUm93cygpIHtcbiAgICAgICAgXG4gICAgICAgIGlmICghdGhpcy5pc01lbWJlck9mVmlld1RyZWUpIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCB2aXNpYmxlSW5kZXhlcyA9IHRoaXMuaW5kZXhlc0ZvclZpc2libGVSb3dzKClcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IG1pbkluZGV4ID0gdmlzaWJsZUluZGV4ZXNbMF1cbiAgICAgICAgY29uc3QgbWF4SW5kZXggPSB2aXNpYmxlSW5kZXhlc1t2aXNpYmxlSW5kZXhlcy5sZW5ndGggLSAxXVxuICAgICAgICBcbiAgICAgICAgY29uc3QgcmVtb3ZlZFZpZXdzID0gW11cbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHZpc2libGVSb3dzID0gW11cbiAgICAgICAgdGhpcy5fdmlzaWJsZVJvd3MuZm9yRWFjaChmdW5jdGlvbiAodGhpczogVUlUYWJsZVZpZXcsIHJvdzogVUlWaWV3LCBpbmRleDogbnVtYmVyLCBhcnJheTogVUlWaWV3W10pIHtcbiAgICAgICAgICAgIGlmIChyb3cuX1VJVGFibGVWaWV3Um93SW5kZXggPCBtaW5JbmRleCB8fCByb3cuX1VJVGFibGVWaWV3Um93SW5kZXggPiBtYXhJbmRleCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC8vcm93LnJlbW92ZUZyb21TdXBlcnZpZXcoKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl9wZXJzaXN0ZWREYXRhW3Jvdy5fVUlUYWJsZVZpZXdSb3dJbmRleF0gPSB0aGlzLnBlcnNpc3RlbmNlRGF0YUl0ZW1Gb3JSb3dXaXRoSW5kZXgoXG4gICAgICAgICAgICAgICAgICAgIHJvdy5fVUlUYWJsZVZpZXdSb3dJbmRleCxcbiAgICAgICAgICAgICAgICAgICAgcm93XG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuX3JlbW92ZWRSZXVzYWJsZVZpZXdzW3Jvdy5fVUlUYWJsZVZpZXdSZXVzYWJpbGl0eUlkZW50aWZpZXJdLnB1c2gocm93KVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHJlbW92ZWRWaWV3cy5wdXNoKHJvdylcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHZpc2libGVSb3dzLnB1c2gocm93KVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB0aGlzKVxuICAgICAgICB0aGlzLl92aXNpYmxlUm93cyA9IHZpc2libGVSb3dzXG4gICAgICAgIFxuICAgICAgICB2aXNpYmxlSW5kZXhlcy5mb3JFYWNoKGZ1bmN0aW9uICh0aGlzOiBVSVRhYmxlVmlldywgcm93SW5kZXg6IG51bWJlciwgaW5kZXg6IG51bWJlciwgYXJyYXk6IG51bWJlcikge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh0aGlzLmlzUm93V2l0aEluZGV4VmlzaWJsZShyb3dJbmRleCkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHZpZXc6IFVJVmlldyA9IHRoaXMudmlld0ZvclJvd1dpdGhJbmRleChyb3dJbmRleClcbiAgICAgICAgICAgIC8vdmlldy5fVUlUYWJsZVZpZXdSb3dJbmRleCA9IHJvd0luZGV4O1xuICAgICAgICAgICAgdGhpcy5fZmlyc3RMYXlvdXRWaXNpYmxlUm93cy5wdXNoKHZpZXcpXG4gICAgICAgICAgICB0aGlzLl92aXNpYmxlUm93cy5wdXNoKHZpZXcpXG4gICAgICAgICAgICB0aGlzLmFkZFN1YnZpZXcodmlldylcbiAgICAgICAgICAgIFxuICAgICAgICB9IGFzIGFueSwgdGhpcylcbiAgICAgICAgXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcmVtb3ZlZFZpZXdzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciB2aWV3OiBVSVZpZXcgPSByZW1vdmVkVmlld3NbaV1cbiAgICAgICAgICAgIGlmICh0aGlzLl92aXNpYmxlUm93cy5pbmRleE9mKHZpZXcpID09IC0xKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgLy90aGlzLl9wZXJzaXN0ZWREYXRhW3ZpZXcuX1VJVGFibGVWaWV3Um93SW5kZXhdID0gdGhpcy5wZXJzaXN0ZW5jZURhdGFJdGVtRm9yUm93V2l0aEluZGV4KHZpZXcuX1VJVGFibGVWaWV3Um93SW5kZXgsIHZpZXcpO1xuICAgICAgICAgICAgICAgIHZpZXcucmVtb3ZlRnJvbVN1cGVydmlldygpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgLy90aGlzLl9yZW1vdmVkUmV1c2FibGVWaWV3c1t2aWV3Ll9VSVRhYmxlVmlld1JldXNhYmlsaXR5SWRlbnRpZmllcl0ucHVzaCh2aWV3KTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAvL3RoaXMuc2V0TmVlZHNMYXlvdXQoKTtcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHZpc2libGVSb3dXaXRoSW5kZXgocm93SW5kZXg6IG51bWJlcik6IFVJVmlldyB7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fdmlzaWJsZVJvd3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGNvbnN0IHJvdyA9IHRoaXMuX3Zpc2libGVSb3dzW2ldXG4gICAgICAgICAgICBpZiAocm93Ll9VSVRhYmxlVmlld1Jvd0luZGV4ID09IHJvd0luZGV4KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJvd1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBuaWxcbiAgICB9XG4gICAgXG4gICAgXG4gICAgaXNSb3dXaXRoSW5kZXhWaXNpYmxlKHJvd0luZGV4OiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIElTKHRoaXMudmlzaWJsZVJvd1dpdGhJbmRleChyb3dJbmRleCkpXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHJldXNhYmxlVmlld0ZvcklkZW50aWZpZXIoaWRlbnRpZmllcjogc3RyaW5nLCByb3dJbmRleDogbnVtYmVyKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAoIXRoaXMuX3JlbW92ZWRSZXVzYWJsZVZpZXdzW2lkZW50aWZpZXJdKSB7XG4gICAgICAgICAgICB0aGlzLl9yZW1vdmVkUmV1c2FibGVWaWV3c1tpZGVudGlmaWVyXSA9IFtdXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl9yZW1vdmVkUmV1c2FibGVWaWV3c1tpZGVudGlmaWVyXSAmJiB0aGlzLl9yZW1vdmVkUmV1c2FibGVWaWV3c1tpZGVudGlmaWVyXS5sZW5ndGgpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29uc3QgdmlldyA9IHRoaXMuX3JlbW92ZWRSZXVzYWJsZVZpZXdzW2lkZW50aWZpZXJdLnBvcCgpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZpZXcuX1VJVGFibGVWaWV3Um93SW5kZXggPSByb3dJbmRleFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBPYmplY3QuYXNzaWduKHZpZXcsIHRoaXMuX3BlcnNpc3RlZERhdGFbcm93SW5kZXhdIHx8IHRoaXMuZGVmYXVsdFJvd1BlcnNpc3RlbmNlRGF0YUl0ZW0oKSlcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcmV0dXJuIHZpZXdcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAoIXRoaXMuX3JldXNhYmxlVmlld3NbaWRlbnRpZmllcl0pIHtcbiAgICAgICAgICAgIHRoaXMuX3JldXNhYmxlVmlld3NbaWRlbnRpZmllcl0gPSBbXVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBjb25zdCBuZXdWaWV3ID0gdGhpcy5uZXdSZXVzYWJsZVZpZXdGb3JJZGVudGlmaWVyKGlkZW50aWZpZXIsIHRoaXMuX3Jvd0lESW5kZXgpXG4gICAgICAgIHRoaXMuX3Jvd0lESW5kZXggPSB0aGlzLl9yb3dJREluZGV4ICsgMVxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX3Jvd0lESW5kZXggPiA0MCkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBhc2QgPSAxXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgbmV3Vmlldy5fVUlUYWJsZVZpZXdSZXVzYWJpbGl0eUlkZW50aWZpZXIgPSBpZGVudGlmaWVyXG4gICAgICAgIFxuICAgICAgICBuZXdWaWV3Ll9VSVRhYmxlVmlld1Jvd0luZGV4ID0gcm93SW5kZXhcbiAgICAgICAgXG4gICAgICAgIE9iamVjdC5hc3NpZ24obmV3VmlldywgdGhpcy5fcGVyc2lzdGVkRGF0YVtyb3dJbmRleF0gfHwgdGhpcy5kZWZhdWx0Um93UGVyc2lzdGVuY2VEYXRhSXRlbSgpKVxuICAgICAgICB0aGlzLl9yZXVzYWJsZVZpZXdzW2lkZW50aWZpZXJdLnB1c2gobmV3VmlldylcbiAgICAgICAgXG4gICAgICAgIHJldHVybiBuZXdWaWV3XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICAvLyBGdW5jdGlvbnMgdGhhdCBzaG91bGQgYmUgb3ZlcnJpZGRlbiB0byBkcmF3IHRoZSBjb3JyZWN0IGNvbnRlbnQgU1RBUlRcbiAgICBuZXdSZXVzYWJsZVZpZXdGb3JJZGVudGlmaWVyKGlkZW50aWZpZXI6IHN0cmluZywgcm93SURJbmRleDogbnVtYmVyKTogVUlWaWV3IHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHZpZXcgPSBuZXcgVUlCdXR0b24odGhpcy5lbGVtZW50SUQgKyBcIlJvd1wiICsgcm93SURJbmRleClcbiAgICAgICAgXG4gICAgICAgIHZpZXcuc3RvcHNQb2ludGVyRXZlbnRQcm9wYWdhdGlvbiA9IE5PXG4gICAgICAgIHZpZXcucGF1c2VzUG9pbnRlckV2ZW50cyA9IE5PXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdmlld1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgaGVpZ2h0Rm9yUm93V2l0aEluZGV4KGluZGV4OiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gNTBcbiAgICB9XG4gICAgXG4gICAgbnVtYmVyT2ZSb3dzKCkge1xuICAgICAgICByZXR1cm4gMTAwMDBcbiAgICB9XG4gICAgXG4gICAgZGVmYXVsdFJvd1BlcnNpc3RlbmNlRGF0YUl0ZW0oKSB7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHBlcnNpc3RlbmNlRGF0YUl0ZW1Gb3JSb3dXaXRoSW5kZXgocm93SW5kZXg6IG51bWJlciwgcm93OiBVSVZpZXcpIHtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgdmlld0ZvclJvd1dpdGhJbmRleChyb3dJbmRleDogbnVtYmVyKTogVUlWaWV3IHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IHJvdyA9IHRoaXMucmV1c2FibGVWaWV3Rm9ySWRlbnRpZmllcihcIlJvd1wiLCByb3dJbmRleCk7XG4gICAgICAgIChyb3cgYXMgVUlCdXR0b24pLnRpdGxlTGFiZWwudGV4dCA9IFwiUm93IFwiICsgcm93SW5kZXhcbiAgICAgICAgXG4gICAgICAgIHJldHVybiByb3dcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIC8vIEZ1bmN0aW9ucyB0aGF0IHNob3VsZCBiZSBvdmVycmlkZGVuIHRvIGRyYXcgdGhlIGNvcnJlY3QgY29udGVudCBFTkRcbiAgICBcbiAgICBcbiAgICAvLyBGdW5jdGlvbnMgdGhhdCB0cmlnZ2VyIHJlZHJhd2luZyBvZiB0aGUgY29udGVudFxuICAgIGRpZFNjcm9sbFRvUG9zaXRpb24ob2Zmc2V0UG9zaXRpb246IFVJUG9pbnQpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmRpZFNjcm9sbFRvUG9zaXRpb24ob2Zmc2V0UG9zaXRpb24pXG4gICAgICAgIFxuICAgICAgICB0aGlzLmZvckVhY2hWaWV3SW5TdWJ0cmVlKGZ1bmN0aW9uICh2aWV3OiBVSVZpZXcpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmlldy5faXNQb2ludGVyVmFsaWQgPSBOT1xuICAgICAgICAgICAgXG4gICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICBpZiAoIXRoaXMuX2lzRHJhd1Zpc2libGVSb3dzU2NoZWR1bGVkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX2lzRHJhd1Zpc2libGVSb3dzU2NoZWR1bGVkID0gWUVTXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFVJVmlldy5ydW5GdW5jdGlvbkJlZm9yZU5leHRGcmFtZShmdW5jdGlvbiAodGhpczogVUlUYWJsZVZpZXcpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl9jYWxjdWxhdGVBbGxQb3NpdGlvbnMoKVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMuX2RyYXdWaXNpYmxlUm93cygpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5zZXROZWVkc0xheW91dCgpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5faXNEcmF3VmlzaWJsZVJvd3NTY2hlZHVsZWQgPSBOT1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHdhc0FkZGVkVG9WaWV3VHJlZSgpIHtcbiAgICAgICAgdGhpcy5sb2FkRGF0YSgpXG4gICAgfVxuICAgIFxuICAgIHNldEZyYW1lKHJlY3RhbmdsZTogVUlSZWN0YW5nbGUsIHpJbmRleD86IG51bWJlciwgcGVyZm9ybVVuY2hlY2tlZExheW91dD86IGJvb2xlYW4pIHtcbiAgICAgICAgXG4gICAgICAgIGNvbnN0IGZyYW1lID0gdGhpcy5mcmFtZVxuICAgICAgICBzdXBlci5zZXRGcmFtZShyZWN0YW5nbGUsIHpJbmRleCwgcGVyZm9ybVVuY2hlY2tlZExheW91dClcbiAgICAgICAgaWYgKGZyYW1lLmlzRXF1YWxUbyhyZWN0YW5nbGUpICYmICFwZXJmb3JtVW5jaGVja2VkTGF5b3V0KSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fbmVlZHNEcmF3aW5nT2ZWaXNpYmxlUm93c0JlZm9yZUxheW91dCA9IFlFU1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50KGV2ZW50OiBVSVZpZXdCcm9hZGNhc3RFdmVudCkge1xuICAgICAgICBcbiAgICAgICAgc3VwZXIuZGlkUmVjZWl2ZUJyb2FkY2FzdEV2ZW50KGV2ZW50KVxuICAgICAgICBcbiAgICAgICAgaWYgKGV2ZW50Lm5hbWUgPT0gVUlWaWV3LmJyb2FkY2FzdEV2ZW50TmFtZS5MYW5ndWFnZUNoYW5nZWQgJiYgdGhpcy5yZWxvYWRzT25MYW5ndWFnZUNoYW5nZSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLnJlbG9hZERhdGEoKVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBwcml2YXRlIF9sYXlvdXRBbGxSb3dzKHBvc2l0aW9ucyA9IHRoaXMuX3Jvd1Bvc2l0aW9ucykge1xuICAgICAgICBcbiAgICAgICAgY29uc3QgYm91bmRzID0gdGhpcy5ib3VuZHNcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3Zpc2libGVSb3dzLmZvckVhY2goZnVuY3Rpb24gKHRoaXM6IFVJVGFibGVWaWV3LCByb3c6IFVJVmlldywgaW5kZXg6IG51bWJlciwgYXJyYXk6IFVJVmlld1tdKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IGZyYW1lID0gYm91bmRzLmNvcHkoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBwb3NpdGlvbk9iamVjdCA9IHBvc2l0aW9uc1tyb3cuX1VJVGFibGVWaWV3Um93SW5kZXhdXG4gICAgICAgICAgICBmcmFtZS5taW4ueSA9IHBvc2l0aW9uT2JqZWN0LnRvcFlcbiAgICAgICAgICAgIGZyYW1lLm1heC55ID0gcG9zaXRpb25PYmplY3QuYm90dG9tWVxuICAgICAgICAgICAgcm93LmZyYW1lID0gZnJhbWVcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcm93LnN0eWxlLndpZHRoID0gXCJcIiArIChib3VuZHMud2lkdGggLSB0aGlzLnNpZGVQYWRkaW5nICogMikuaW50ZWdlclZhbHVlICsgXCJweFwiXG4gICAgICAgICAgICByb3cuc3R5bGUubGVmdCA9IFwiXCIgKyB0aGlzLnNpZGVQYWRkaW5nLmludGVnZXJWYWx1ZSArIFwicHhcIlxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfSwgdGhpcylcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2Z1bGxIZWlnaHRWaWV3LmZyYW1lID0gYm91bmRzLnJlY3RhbmdsZVdpdGhIZWlnaHQoKHBvc2l0aW9ucy5sYXN0RWxlbWVudCB8fFxuICAgICAgICAgICAgbmlsKS5ib3R0b21ZKS5yZWN0YW5nbGVXaXRoV2lkdGgoYm91bmRzLndpZHRoICogMC41KVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fZmlyc3RMYXlvdXRWaXNpYmxlUm93cyA9IFtdXG4gICAgICAgIFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgcHJpdmF0ZSBfYW5pbWF0ZUxheW91dEFsbFJvd3MoKSB7XG4gICAgICAgIFxuICAgICAgICBVSVZpZXcuYW5pbWF0ZVZpZXdPclZpZXdzV2l0aER1cmF0aW9uRGVsYXlBbmRGdW5jdGlvbihcbiAgICAgICAgICAgIHRoaXMuX3Zpc2libGVSb3dzLFxuICAgICAgICAgICAgdGhpcy5hbmltYXRpb25EdXJhdGlvbixcbiAgICAgICAgICAgIDAsXG4gICAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgICBmdW5jdGlvbiAodGhpczogVUlUYWJsZVZpZXcpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl9sYXlvdXRBbGxSb3dzKClcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSxcbiAgICAgICAgICAgIGZ1bmN0aW9uICh0aGlzOiBVSVRhYmxlVmlldykge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC8vIHRoaXMuX2NhbGN1bGF0ZUFsbFBvc2l0aW9ucygpXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5fbGF5b3V0QWxsUm93cygpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LmJpbmQodGhpcylcbiAgICAgICAgKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgbGF5b3V0U3Vidmlld3MoKSB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCBwcmV2aW91c1Bvc2l0aW9uczogVUlUYWJsZVZpZXdSZXVzYWJsZVZpZXdQb3NpdGlvbk9iamVjdFtdID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh0aGlzLl9yb3dQb3NpdGlvbnMpKVxuICAgICAgICBcbiAgICAgICAgY29uc3QgcHJldmlvdXNWaXNpYmxlUm93c0xlbmd0aCA9IHRoaXMuX3Zpc2libGVSb3dzLmxlbmd0aFxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuX25lZWRzRHJhd2luZ09mVmlzaWJsZVJvd3NCZWZvcmVMYXlvdXQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy90aGlzLl9jYWxjdWxhdGVBbGxQb3NpdGlvbnMoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9kcmF3VmlzaWJsZVJvd3MoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9uZWVkc0RyYXdpbmdPZlZpc2libGVSb3dzQmVmb3JlTGF5b3V0ID0gTk9cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHN1cGVyLmxheW91dFN1YnZpZXdzKClcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAoIXRoaXMubnVtYmVyT2ZSb3dzKCkgfHwgIXRoaXMuaXNNZW1iZXJPZlZpZXdUcmVlKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5fc2hvdWxkQW5pbWF0ZU5leHRMYXlvdXQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIE5lZWQgdG8gZG8gbGF5b3V0IHdpdGggdGhlIHByZXZpb3VzIHBvc2l0aW9uc1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9sYXlvdXRBbGxSb3dzKHByZXZpb3VzUG9zaXRpb25zKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChwcmV2aW91c1Zpc2libGVSb3dzTGVuZ3RoIDwgdGhpcy5fdmlzaWJsZVJvd3MubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgVUlWaWV3LnJ1bkZ1bmN0aW9uQmVmb3JlTmV4dEZyYW1lKGZ1bmN0aW9uICh0aGlzOiBVSVRhYmxlVmlldykge1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fYW5pbWF0ZUxheW91dEFsbFJvd3MoKVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9LmJpbmQodGhpcykpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLl9hbmltYXRlTGF5b3V0QWxsUm93cygpXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5fc2hvdWxkQW5pbWF0ZU5leHRMYXlvdXQgPSBOT1xuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIGlmICh0aGlzLl9uZWVkc0RyYXdpbmdPZlZpc2libGVSb3dzQmVmb3JlTGF5b3V0KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vICAgICB0aGlzLl9kcmF3VmlzaWJsZVJvd3MoKTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy8gICAgIHRoaXMuX25lZWRzRHJhd2luZ09mVmlzaWJsZVJvd3NCZWZvcmVMYXlvdXQgPSBOTztcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy8gfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9jYWxjdWxhdGVBbGxQb3NpdGlvbnMoKVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9sYXlvdXRBbGxSb3dzKClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBpbnRyaW5zaWNDb250ZW50SGVpZ2h0KGNvbnN0cmFpbmluZ1dpZHRoID0gMCkge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB2YXIgcmVzdWx0ID0gMTAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDBcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLl9yb3dQb3NpdGlvbnMubGVuZ3RoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuX3Jvd1Bvc2l0aW9uc1t0aGlzLl9yb3dQb3NpdGlvbnMubGVuZ3RoIC0gMV0uYm90dG9tWVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGg9XCJVSVRleHRWaWV3LnRzXCIgLz5cblxuXG5cblxuLy8gQHRzLWlnbm9yZVxuY2xhc3MgVUlUZXh0RmllbGQgZXh0ZW5kcyBVSVRleHRWaWV3IHtcbiAgICBcbiAgICBcbiAgICBfcGxhY2Vob2xkZXJUZXh0S2V5OiBzdHJpbmdcbiAgICBfZGVmYXVsdFBsYWNlaG9sZGVyVGV4dDogc3RyaW5nXG4gICAgXG4gICAgX3ZpZXdIVE1MRWxlbWVudDogSFRNTElucHV0RWxlbWVudFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRJRD86IHN0cmluZywgdmlld0hUTUxFbGVtZW50ID0gbnVsbCwgdHlwZSA9IFVJVGV4dFZpZXcudHlwZS50ZXh0RmllbGQpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgdHlwZSwgdmlld0hUTUxFbGVtZW50KVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fY2xhc3MgPSBVSVRleHRGaWVsZFxuICAgICAgICB0aGlzLnN1cGVyY2xhc3MgPSBVSVRleHRWaWV3XG4gICAgICAgIFxuICAgICAgICB0aGlzLnZpZXdIVE1MRWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJ0eXBlXCIsIFwidGV4dFwiKVxuICAgICAgICBcbiAgICAgICAgdGhpcy5iYWNrZ3JvdW5kQ29sb3IgPSBVSUNvbG9yLndoaXRlQ29sb3JcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5hZGRUYXJnZXRGb3JDb250cm9sRXZlbnQoVUlWaWV3LmNvbnRyb2xFdmVudC5Qb2ludGVyVXBJbnNpZGUsIGZ1bmN0aW9uIChzZW5kZXIsIGV2ZW50KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHNlbmRlci5mb2N1cygpXG4gICAgICAgICAgICBcbiAgICAgICAgfSlcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLnZpZXdIVE1MRWxlbWVudC5vbmlucHV0ID0gKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlbmRDb250cm9sRXZlbnRGb3JLZXkoVUlUZXh0RmllbGQuY29udHJvbEV2ZW50LlRleHRDaGFuZ2UsIGV2ZW50KVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS53ZWJraXRVc2VyU2VsZWN0ID0gXCJ0ZXh0XCJcbiAgICAgICAgXG4gICAgICAgIHRoaXMubmF0aXZlU2VsZWN0aW9uRW5hYmxlZCA9IFlFU1xuICAgICAgICBcbiAgICAgICAgdGhpcy5wYXVzZXNQb2ludGVyRXZlbnRzID0gTk9cbiAgICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBzdGF0aWMgY29udHJvbEV2ZW50ID0gT2JqZWN0LmFzc2lnbih7fSwgVUlWaWV3LmNvbnRyb2xFdmVudCwge1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIFwiVGV4dENoYW5nZVwiOiBcIlRleHRDaGFuZ2VcIlxuICAgICAgICBcbiAgICAgICAgXG4gICAgfSlcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBnZXQgYWRkQ29udHJvbEV2ZW50VGFyZ2V0KCk6IFVJVmlld0FkZENvbnRyb2xFdmVudFRhcmdldE9iamVjdDx0eXBlb2YgVUlUZXh0RmllbGQuY29udHJvbEV2ZW50PiB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gc3VwZXIuYWRkQ29udHJvbEV2ZW50VGFyZ2V0IGFzIGFueTtcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgdmlld0hUTUxFbGVtZW50KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fdmlld0hUTUxFbGVtZW50XG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHB1YmxpYyBzZXQgdGV4dCh0ZXh0OiBzdHJpbmcpIHtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LnZhbHVlID0gdGV4dFxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgXG4gICAgcHVibGljIGdldCB0ZXh0KCk6IHN0cmluZyB7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy52aWV3SFRNTEVsZW1lbnQudmFsdWVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHB1YmxpYyBzZXQgcGxhY2Vob2xkZXJUZXh0KHRleHQ6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQucGxhY2Vob2xkZXIgPSB0ZXh0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBwdWJsaWMgZ2V0IHBsYWNlaG9sZGVyVGV4dCgpOiBzdHJpbmcge1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHRoaXMudmlld0hUTUxFbGVtZW50LnBsYWNlaG9sZGVyXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBzZXRQbGFjZWhvbGRlclRleHQoa2V5OiBzdHJpbmcsIGRlZmF1bHRTdHJpbmc6IHN0cmluZykge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5fcGxhY2Vob2xkZXJUZXh0S2V5ID0ga2V5XG4gICAgICAgIHRoaXMuX2RlZmF1bHRQbGFjZWhvbGRlclRleHQgPSBkZWZhdWx0U3RyaW5nXG4gICAgICAgIFxuICAgICAgICBjb25zdCBsYW5ndWFnZU5hbWUgPSBVSUNvcmUubGFuZ3VhZ2VTZXJ2aWNlLmN1cnJlbnRMYW5ndWFnZUtleVxuICAgICAgICB0aGlzLnBsYWNlaG9sZGVyVGV4dCA9IFVJQ29yZS5sYW5ndWFnZVNlcnZpY2Uuc3RyaW5nRm9yS2V5KGtleSwgbGFuZ3VhZ2VOYW1lLCBkZWZhdWx0U3RyaW5nLCBuaWwpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBkaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnQoZXZlbnQ6IFVJVmlld0Jyb2FkY2FzdEV2ZW50KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci5kaWRSZWNlaXZlQnJvYWRjYXN0RXZlbnQoZXZlbnQpXG4gICAgICAgIFxuICAgICAgICBpZiAoZXZlbnQubmFtZSA9PSBVSVZpZXcuYnJvYWRjYXN0RXZlbnROYW1lLkxhbmd1YWdlQ2hhbmdlZCB8fCBldmVudC5uYW1lID09XG4gICAgICAgICAgICBVSVZpZXcuYnJvYWRjYXN0RXZlbnROYW1lLkFkZGVkVG9WaWV3VHJlZSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLl9zZXRQbGFjZWhvbGRlckZyb21LZXlJZlBvc3NpYmxlKClcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICB3aWxsTW92ZVRvU3VwZXJ2aWV3KHN1cGVydmlldzogVUlWaWV3KSB7XG4gICAgICAgIFxuICAgICAgICBzdXBlci53aWxsTW92ZVRvU3VwZXJ2aWV3KHN1cGVydmlldylcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3NldFBsYWNlaG9sZGVyRnJvbUtleUlmUG9zc2libGUoKVxuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgX3NldFBsYWNlaG9sZGVyRnJvbUtleUlmUG9zc2libGUoKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5fcGxhY2Vob2xkZXJUZXh0S2V5ICYmIHRoaXMuX2RlZmF1bHRQbGFjZWhvbGRlclRleHQpIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5zZXRQbGFjZWhvbGRlclRleHQodGhpcy5fcGxhY2Vob2xkZXJUZXh0S2V5LCB0aGlzLl9kZWZhdWx0UGxhY2Vob2xkZXJUZXh0KVxuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIHB1YmxpYyBnZXQgaXNTZWN1cmUoKTogYm9vbGVhbiB7XG4gICAgICAgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSAodGhpcy52aWV3SFRNTEVsZW1lbnQudHlwZSA9PSBcInBhc3N3b3JkXCIpXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBwdWJsaWMgc2V0IGlzU2VjdXJlKHNlY3VyZTogYm9vbGVhbikge1xuICAgICAgICBcbiAgICAgICAgdmFyIHR5cGUgPSBcInRleHRcIlxuICAgICAgICBcbiAgICAgICAgaWYgKHNlY3VyZSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0eXBlID0gXCJwYXNzd29yZFwiXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy52aWV3SFRNTEVsZW1lbnQudHlwZSA9IHR5cGVcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIlVJVGV4dEZpZWxkLnRzXCIgLz5cblxuXG5cblxuY2xhc3MgVUlUZXh0QXJlYSBleHRlbmRzIFVJVGV4dEZpZWxkIHtcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50SUQsIHZpZXdIVE1MRWxlbWVudCA9IG51bGwpIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKGVsZW1lbnRJRCwgdmlld0hUTUxFbGVtZW50LCBVSVRleHRWaWV3LnR5cGUudGV4dEFyZWEpXG4gICAgICAgIFxuICAgICAgICB0aGlzLl9jbGFzcyA9IFVJVGV4dEFyZWFcbiAgICAgICAgdGhpcy5zdXBlcmNsYXNzID0gVUlUZXh0RmllbGRcbiAgICAgICAgXG4gICAgICAgIHRoaXMudmlld0hUTUxFbGVtZW50LnJlbW92ZUF0dHJpYnV0ZShcInR5cGVcIilcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3R5bGUub3ZlcmZsb3cgPSBcImF1dG9cIlxuICAgICAgICBcbiAgICAgICAgdGhpcy5zdHlsZS53ZWJraXRVc2VyU2VsZWN0ID0gXCJ0ZXh0XCJcbiAgICAgICAgXG4gICAgICAgIHRoaXMucGF1c2VzUG9pbnRlckV2ZW50cyA9IE5PXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBnZXQgYWRkQ29udHJvbEV2ZW50VGFyZ2V0KCk6IFVJVmlld0FkZENvbnRyb2xFdmVudFRhcmdldE9iamVjdDx0eXBlb2YgVUlUZXh0QXJlYS5jb250cm9sRXZlbnQ+IHtcbiAgICAgICAgXG4gICAgICAgIHJldHVybiBzdXBlci5hZGRDb250cm9sRXZlbnRUYXJnZXQgYXMgYW55O1xuICAgICAgICBcbiAgICB9XG4gICAgXG4gICAgLy8gQHRzLWlnbm9yZVxuICAgIGdldCB2aWV3SFRNTEVsZW1lbnQoKTogSFRNTFRleHRBcmVhRWxlbWVudCB7XG4gICAgICAgIFxuICAgICAgICAvLyBAdHMtaWdub3JlXG4gICAgICAgIHJldHVybiBzdXBlci52aWV3SFRNTEVsZW1lbnRcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuIiwiY2xhc3MgVUlUaW1lciBleHRlbmRzIFVJT2JqZWN0IHtcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBcbiAgICBfaW50ZXJ2YWxJRDogbnVtYmVyXG4gICAgXG4gICAgaXNWYWxpZDogYm9vbGVhbiA9IFlFU1xuICAgIFxuICAgIFxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBpbnRlcnZhbDogbnVtYmVyLCBwdWJsaWMgcmVwZWF0czogYm9vbGVhbiwgcHVibGljIHRhcmdldDogRnVuY3Rpb24pIHtcbiAgICAgICAgXG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgXG4gICAgICAgIHRoaXMuc3VwZXJjbGFzcyA9IFVJT2JqZWN0XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgdGhpcy5zY2hlZHVsZSgpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIHNjaGVkdWxlKCkge1xuICAgIFxuICAgICAgICBjb25zdCBjYWxsYmFjayA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnJlcGVhdHMgPT0gTk8pIHtcbiAgICAgICAgICAgICAgICB0aGlzLmludmFsaWRhdGUoKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy50YXJnZXQoKVxuICAgICAgICB9LmJpbmQodGhpcylcbiAgICBcbiAgICAgICAgdGhpcy5faW50ZXJ2YWxJRCA9IHdpbmRvdy5zZXRJbnRlcnZhbChjYWxsYmFjaywgdGhpcy5pbnRlcnZhbCAqIDEwMDApXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICByZXNjaGVkdWxlKCkge1xuICAgICAgICBcbiAgICAgICAgdGhpcy5pbnZhbGlkYXRlKClcbiAgICAgICAgdGhpcy5zY2hlZHVsZSgpXG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcbiAgICBcbiAgICBmaXJlKCkge1xuICAgICAgICBpZiAodGhpcy5yZXBlYXRzID09IE5PKSB7XG4gICAgICAgICAgICB0aGlzLmludmFsaWRhdGUoKVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5yZXNjaGVkdWxlKClcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnRhcmdldCgpXG4gICAgfVxuICAgIFxuICAgIGludmFsaWRhdGUoKSB7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5pc1ZhbGlkKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5faW50ZXJ2YWxJRClcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5pc1ZhbGlkID0gTk9cbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiJdfQ==